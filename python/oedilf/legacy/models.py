# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class DilfAuthors(models.Model):
    authorid = models.AutoField(
        db_column="AuthorId", primary_key=True
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=50
    )  # Field name made lowercase.
    password = models.CharField(
        db_column="Password", max_length=32
    )  # Field name made lowercase.
    token = models.CharField(
        db_column="Token", max_length=32
    )  # Field name made lowercase.
    accesstime = models.CharField(
        db_column="AccessTime", max_length=16, blank=True, null=True
    )  # Field name made lowercase.
    usertype = models.CharField(
        db_column="UserType", max_length=18
    )  # Field name made lowercase.
    rank = models.CharField(
        db_column="Rank", max_length=5
    )  # Field name made lowercase.
    userip = models.CharField(
        db_column="UserIP", max_length=20
    )  # Field name made lowercase.
    email = models.CharField(
        db_column="Email", max_length=80, blank=True, null=True
    )  # Field name made lowercase.
    realname = models.CharField(
        db_column="RealName", max_length=50
    )  # Field name made lowercase.
    lastupdate = models.CharField(
        db_column="LastUpdate", max_length=16
    )  # Field name made lowercase.
    workshoptime = models.DateTimeField(
        db_column="WorkshopTime"
    )  # Field name made lowercase.
    biography = models.TextField(
        db_column="Biography", blank=True, null=True
    )  # Field name made lowercase.
    notifychanges = models.CharField(
        db_column="NotifyChanges", max_length=3
    )  # Field name made lowercase.
    monitornew = models.CharField(
        db_column="MonitorNew", max_length=4
    )  # Field name made lowercase.
    monitorcomments = models.CharField(
        db_column="MonitorComments", max_length=11
    )  # Field name made lowercase.
    monitorattention = models.CharField(
        db_column="MonitorAttention", max_length=3
    )  # Field name made lowercase.
    useemail = models.CharField(
        db_column="UseEmail", max_length=3
    )  # Field name made lowercase.
    feedbackstyle = models.TextField(
        db_column="FeedbackStyle"
    )  # Field name made lowercase.
    timezone = models.IntegerField(db_column="TimeZone")  # Field name made lowercase.
    addprivileges = models.TextField(
        db_column="AddPrivileges"
    )  # Field name made lowercase.
    monitorrfas = models.CharField(
        db_column="MonitorRFAs", max_length=8
    )  # Field name made lowercase.
    juniormode = models.CharField(
        db_column="JuniorMode", max_length=3
    )  # Field name made lowercase.
    curtainfiltering = models.CharField(
        db_column="CurtainFiltering", max_length=3
    )  # Field name made lowercase.
    textmacrobuttons = models.CharField(
        db_column="TextMacroButtons", max_length=3
    )  # Field name made lowercase.
    wscommentdisplaylimit = models.CharField(
        db_column="WSCommentDisplayLimit", max_length=9
    )  # Field name made lowercase.
    lat = models.FloatField(db_column="Lat")  # Field name made lowercase.
    lng = models.FloatField(db_column="Lng")  # Field name made lowercase.
    bornin = models.CharField(
        db_column="BornIn", max_length=2
    )  # Field name made lowercase.
    livingin = models.CharField(
        db_column="LivingIn", max_length=2
    )  # Field name made lowercase.
    heldcount = models.FloatField(db_column="HeldCount")  # Field name made lowercase.
    newcount = models.FloatField(db_column="NewCount")  # Field name made lowercase.
    approvedcount = models.FloatField(
        db_column="ApprovedCount"
    )  # Field name made lowercase.
    confirmingcount = models.FloatField(
        db_column="ConfirmingCount"
    )  # Field name made lowercase.
    revisedcount = models.FloatField(
        db_column="RevisedCount"
    )  # Field name made lowercase.
    tentativecount = models.FloatField(
        db_column="TentativeCount"
    )  # Field name made lowercase.
    untendedcount = models.FloatField(
        db_column="UntendedCount"
    )  # Field name made lowercase.
    firstlimdate = models.DateTimeField(
        db_column="FirstLimDate"
    )  # Field name made lowercase.
    approvalemails = models.CharField(
        db_column="ApprovalEmails", max_length=3
    )  # Field name made lowercase.
    milestoneemails = models.CharField(
        db_column="MilestoneEmails", max_length=3
    )  # Field name made lowercase.
    newsemails = models.CharField(
        db_column="NewsEmails", max_length=3
    )  # Field name made lowercase.
    twitterhandle = models.CharField(
        db_column="TwitterHandle", max_length=15
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Authors"


class DilfCountries(models.Model):
    code = models.CharField(
        db_column="Code", max_length=2
    )  # Field name made lowercase.
    name = models.TextField(db_column="Name")  # Field name made lowercase.
    flag = models.TextField(db_column="Flag")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Countries"


class DilfDocuments(models.Model):
    title = models.CharField(
        db_column="Title", primary_key=True, max_length=80
    )  # Field name made lowercase.
    text = models.TextField(
        db_column="Text", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Documents"


class DilfLastvisit(models.Model):
    authorid = models.IntegerField(db_column="AuthorId")  # Field name made lowercase.
    originalid = models.IntegerField(
        db_column="OriginalId"
    )  # Field name made lowercase.
    workshopid = models.IntegerField(
        db_column="WorkshopId"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_LastVisit"


class DilfLimericks(models.Model):
    verseid = models.AutoField(
        db_column="VerseId", primary_key=True
    )  # Field name made lowercase.
    verse = models.TextField(db_column="Verse")  # Field name made lowercase.
    datetime = models.DateTimeField(db_column="DateTime")  # Field name made lowercase.
    primaryauthorid = models.IntegerField(
        db_column="PrimaryAuthorId"
    )  # Field name made lowercase.
    secondaryauthorid = models.IntegerField(
        db_column="SecondaryAuthorId"
    )  # Field name made lowercase.
    originalid = models.IntegerField(
        db_column="OriginalId"
    )  # Field name made lowercase.
    limericknumber = models.IntegerField(
        db_column="LimerickNumber"
    )  # Field name made lowercase.
    version = models.IntegerField(db_column="Version")  # Field name made lowercase.
    state = models.CharField(
        db_column="State", max_length=10
    )  # Field name made lowercase.
    title = models.TextField(db_column="Title")  # Field name made lowercase.
    authornotes = models.TextField(
        db_column="AuthorNotes"
    )  # Field name made lowercase.
    editornotes = models.TextField(
        db_column="EditorNotes"
    )  # Field name made lowercase.
    showcasepriority = models.IntegerField(
        db_column="ShowcasePriority"
    )  # Field name made lowercase.
    updatedatetime = models.DateTimeField(
        db_column="UpdateDateTime"
    )  # Field name made lowercase.
    workshopstate = models.CharField(
        db_column="WorkshopState", max_length=9
    )  # Field name made lowercase.
    approvingeditor = models.IntegerField(
        db_column="ApprovingEditor"
    )  # Field name made lowercase.
    linkcode = models.IntegerField(db_column="LinkCode")  # Field name made lowercase.
    statedatetime = models.DateTimeField(
        db_column="StateDateTime"
    )  # Field name made lowercase.
    category = models.CharField(
        db_column="Category", max_length=14
    )  # Field name made lowercase.
    heldbyid = models.IntegerField(db_column="HeldById")  # Field name made lowercase.
    rfas = models.IntegerField(db_column="RFAs")  # Field name made lowercase.
    primaryrfa = models.IntegerField(
        db_column="PrimaryRFA"
    )  # Field name made lowercase.
    secondaryrfa = models.IntegerField(
        db_column="SecondaryRFA"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Limericks"


class DilfLog(models.Model):
    # logid = models.AutoField(db_column='LogId')  # Field name made lowercase.
    datetime = models.DateTimeField(db_column="DateTime")  # Field name made lowercase.
    authorid = models.IntegerField(db_column="AuthorId")  # Field name made lowercase.
    userip = models.CharField(
        db_column="UserIP", max_length=20
    )  # Field name made lowercase.
    verseid = models.IntegerField(db_column="VerseId")  # Field name made lowercase.
    action = models.CharField(
        db_column="Action", max_length=200
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Log"


class DilfMessages(models.Model):
    msgid = models.AutoField(
        db_column="MsgId", primary_key=True
    )  # Field name made lowercase.
    srcid = models.IntegerField(db_column="SrcId")  # Field name made lowercase.
    dstid = models.IntegerField(db_column="DstId")  # Field name made lowercase.
    verseid = models.IntegerField(db_column="VerseId")  # Field name made lowercase.
    authorid = models.IntegerField(db_column="AuthorId")  # Field name made lowercase.
    msgtype = models.CharField(
        db_column="MsgType", max_length=12
    )  # Field name made lowercase.
    datetime = models.DateTimeField(db_column="DateTime")  # Field name made lowercase.
    msgdata = models.TextField(db_column="MsgData")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Messages"


class DilfMonitors(models.Model):
    authorid = models.IntegerField(db_column="AuthorId")  # Field name made lowercase.
    originalid = models.IntegerField(
        db_column="OriginalId"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Monitors"


class DilfRfas(models.Model):
    originalid = models.IntegerField(
        db_column="OriginalId"
    )  # Field name made lowercase.
    authorid = models.IntegerField(db_column="AuthorId")  # Field name made lowercase.
    new = models.IntegerField(db_column="New")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_RFAs"


class DilfRandom(models.Model):
    catid = models.IntegerField(
        db_column="CatId", primary_key=True
    )  # Field name made lowercase.
    timestamp = models.CharField(
        db_column="TimeStamp", max_length=16
    )  # Field name made lowercase.
    text = models.TextField(db_column="Text")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Random"


class DilfRanks(models.Model):
    rank = models.CharField(
        db_column="Rank", primary_key=True, max_length=5
    )  # Field name made lowercase.
    title = models.CharField(
        db_column="Title", max_length=80
    )  # Field name made lowercase.
    showcaselimit = models.IntegerField(
        db_column="ShowcaseLimit"
    )  # Field name made lowercase.
    privilege = models.CharField(
        db_column="Privilege", max_length=18
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Ranks"


class DilfSettings(models.Model):
    alphabetend = models.CharField(
        db_column="AlphabetEnd", primary_key=True, max_length=10
    )  # Field name made lowercase.
    adminemail = models.CharField(
        db_column="AdminEmail", max_length=80, blank=True, null=True
    )  # Field name made lowercase.
    eicemail = models.CharField(
        db_column="EiCEmail", max_length=80, blank=True, null=True
    )  # Field name made lowercase.
    shutdown = models.TextField(
        db_column="Shutdown", blank=True, null=True
    )  # Field name made lowercase.
    copyright = models.TextField(db_column="Copyright")  # Field name made lowercase.
    lastlacecheck = models.CharField(
        db_column="LastLACECheck", max_length=16
    )  # Field name made lowercase.
    lacespercheck = models.IntegerField(
        db_column="LACEsPerCheck"
    )  # Field name made lowercase.
    limericknumber = models.IntegerField(
        db_column="LimerickNumber"
    )  # Field name made lowercase.
    wpsisubmission = models.CharField(
        db_column="WPSISubmission", max_length=3
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Settings"


class DilfStats(models.Model):
    total = models.IntegerField(db_column="Total")  # Field name made lowercase.
    approved = models.IntegerField(db_column="Approved")  # Field name made lowercase.
    approvedshowcase = models.IntegerField(
        db_column="ApprovedShowcase"
    )  # Field name made lowercase.
    approvedgeneral = models.IntegerField(
        db_column="ApprovedGeneral"
    )  # Field name made lowercase.
    approvedgeneralshowcase = models.IntegerField(
        db_column="ApprovedGeneralShowcase"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Stats"


class DilfSubtopics(models.Model):
    topicid = models.IntegerField(db_column="TopicId")  # Field name made lowercase.
    subid = models.IntegerField(db_column="SubId")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_SubTopics"


class DilfTags(models.Model):
    tagindex = models.AutoField(
        db_column="TagIndex", primary_key=True
    )  # Field name made lowercase.
    authorid = models.IntegerField(db_column="AuthorId")  # Field name made lowercase.
    originalid = models.IntegerField(
        db_column="OriginalId"
    )  # Field name made lowercase.
    tagtext = models.TextField(db_column="TagText")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Tags"


class DilfTokens(models.Model):
    token = models.CharField(primary_key=True, max_length=10)
    expires = models.IntegerField()
    handler = models.TextField()
    description = models.TextField()

    class Meta:
        managed = False
        db_table = "DILF_Tokens"


class DilfTopiclims(models.Model):
    topicid = models.IntegerField(db_column="TopicId")  # Field name made lowercase.
    originalid = models.IntegerField(
        db_column="OriginalId"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_TopicLims"


class DilfTopics(models.Model):
    topicid = models.AutoField(
        db_column="TopicId", unique=True, primary_key=True
    )  # Field name made lowercase.
    name = models.TextField(db_column="Name")  # Field name made lowercase.
    description = models.TextField(
        db_column="Description"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Topics"


class DilfWeap(models.Model):
    authorid = models.IntegerField(db_column="AuthorId")  # Field name made lowercase.
    originalid = models.IntegerField(
        db_column="OriginalId", primary_key=True
    )  # Field name made lowercase.
    type = models.CharField(
        db_column="Type", max_length=4
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_WEAP"


class DilfWordlist(models.Model):
    wordtext = models.CharField(
        db_column="WordText", unique=True, max_length=54
    )  # Field name made lowercase.
    status = models.CharField(
        db_column="Status", max_length=8
    )  # Field name made lowercase.
    wordtype = models.CharField(
        db_column="WordType", max_length=6
    )  # Field name made lowercase.
    neededdefs = models.TextField(db_column="NeededDefs")  # Field name made lowercase.
    review = models.CharField(
        db_column="Review", max_length=8
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_WordList"


class DilfWords(models.Model):
    wordid = models.AutoField(
        db_column="WordId", primary_key=True
    )  # Field name made lowercase.
    wordtext = models.CharField(
        db_column="WordText", max_length=54
    )  # Field name made lowercase.
    verseid = models.IntegerField(db_column="VerseId")  # Field name made lowercase.
    type = models.CharField(
        db_column="Type", max_length=8
    )  # Field name made lowercase.
    sequence = models.SmallIntegerField(
        db_column="Sequence"
    )  # Field name made lowercase.
    searchtext = models.CharField(
        db_column="SearchText", max_length=54
    )  # Field name made lowercase.
    sound = models.CharField(
        db_column="Sound", max_length=30
    )  # Field name made lowercase.
    sortorder = models.CharField(
        db_column="SortOrder", max_length=54
    )  # Field name made lowercase.
    isphraseword = models.IntegerField(
        db_column="IsPhraseWord"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Words"


class DilfWorkshop(models.Model):
    workshopid = models.AutoField(
        db_column="WorkshopId", primary_key=True
    )  # Field name made lowercase.
    verseid = models.IntegerField(db_column="VerseId")  # Field name made lowercase.
    authorid = models.IntegerField(db_column="AuthorId")  # Field name made lowercase.
    datetime = models.DateTimeField(db_column="DateTime")  # Field name made lowercase.
    message = models.TextField(
        db_column="Message", blank=True, null=True
    )  # Field name made lowercase.
    posterip = models.CharField(
        db_column="PosterIP", max_length=20
    )  # Field name made lowercase.
    originalid = models.IntegerField(
        db_column="OriginalId"
    )  # Field name made lowercase.
    editdatetime = models.DateTimeField(
        db_column="EditDateTime", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "DILF_Workshop"


class BannerSets(models.Model):
    setid = models.AutoField(
        db_column="SetId", primary_key=True
    )  # Field name made lowercase.
    filename = models.CharField(
        db_column="FileName", max_length=50
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "banner_sets"


class BannerVotes(models.Model):
    voteid = models.AutoField(
        db_column="VoteId", primary_key=True
    )  # Field name made lowercase.
    authorid = models.IntegerField(db_column="AuthorId")  # Field name made lowercase.
    set1 = models.IntegerField(db_column="Set1")  # Field name made lowercase.
    set2 = models.IntegerField(db_column="Set2")  # Field name made lowercase.
    vote = models.IntegerField(db_column="Vote")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "banner_votes"
