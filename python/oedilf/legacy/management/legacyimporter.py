import sys

import progressbar
from django.core.management.color import no_style
from django.db import connection

import lim.signals
from legacy.models import *
from lim.models import *


class LegacyImporterException(Exception):
    pass


class LegacyImporter:
    def __init__(self):
        self.user_cache = {}

    @staticmethod
    def _deverge(authorid):
        # Virge was author 1 in the old system, which clashes with Django's admin user.
        # There was no authorid 2 in the old system, so we bump Virge's author ID.
        if authorid == 1:
            return 2

        else:
            return authorid

    def get_user(self, author_id):
        if author_id in self.user_cache:
            return self.user_cache[author_id]

        if author_id == 0:
            return None

        elif author_id == 1:
            self.user_cache[1] = User.objects.get(username="Virge")
            return self.user_cache[1]

        author_id = self._deverge(author_id)
        users = User.objects.filter(id=author_id)
        if users.exists():
            self.user_cache[author_id] = users[0]
            return self.user_cache[author_id]

        else:
            sys.stderr.write(f"Warning: could not find author {author_id}\n")
            return None

    def import_authors(self):
        fct = progressbar.FormatCustomText(
            "%(id)4d %(username)30s", dict(id=0, username="")
        )
        widgets = [
            "User: ",
            progressbar.Percentage(),
            " ",
            fct,
            " ",
            progressbar.Timer(),
            " ",
            progressbar.ETA(),
        ]
        with progressbar.ProgressBar(
            max_value=DilfAuthors.objects.all().count(), widgets=widgets
        ) as bar:
            for i, legacy_author in enumerate(DilfAuthors.objects.all(), 1):
                fct.update_mapping(
                    id=legacy_author.authorid, username=legacy_author.name
                )
                bar.update(i)
                self.import_author(legacy_author)
        self.sequence_reset(Author, User)

    def import_author(self, legacy_author):
        legacy_author = self._deverge(legacy_author)
        if legacy_author.authorid == 3215:
            # User 'pseudonomical' created a second user 'Pseudonomical '
            # and it is the latter which actually did anything.
            return

        if User.objects.filter(pk=self._deverge(legacy_author.authorid)).exists():
            return
        user = User.objects.create(id=self._deverge(legacy_author.authorid))
        user.save()
        author = user.author
        user.username = legacy_author.name
        user.password = legacy_author.password
        user.email = legacy_author.email
        user.is_staff = legacy_author.authorid == 2357  # neilski
        user.last_name = legacy_author.realname
        author.real_name = legacy_author.realname
        author.author_type = AuthorTypes[
            legacy_author.usertype.replace(" ", "_").upper()
        ]
        author.author_rank = AuthorRanks[legacy_author.rank.upper()]
        if legacy_author.biography is None:
            author.biography = ""
        else:
            author.biography = legacy_author.biography
        author.notify_changes = legacy_author.notifychanges == "On"
        author.monitor_new = MonitorNew[legacy_author.monitornew.upper()]
        author.monitor_comments = MonitorComments[legacy_author.monitorcomments.upper()]
        author.monitor_attention = legacy_author.monitorattention == "On"
        author.use_email = legacy_author.useemail == "On"
        try:
            author.feedback_style_standard = PredefinedFeedbackStyles(
                int(legacy_author.feedbackstyle)
            )
            author.feedback_style = ""
        except ValueError:
            author.feedback_style_standard = PredefinedFeedbackStyles.CUSTOM
            author.feedback_style = legacy_author.feedbackstyle
        author.time_zone = legacy_author.timezone
        author.monitor_rfas = MonitorRFAs[legacy_author.monitorrfas.upper()]
        author.junior_mode = legacy_author.juniormode == "On"
        author.curtain_filtering = legacy_author.curtainfiltering == "On"
        author.text_macro_buttons = legacy_author.textmacrobuttons == "On"
        if legacy_author.wscommentdisplaylimit == "Unlimited":
            author.ws_comment_display_limit = 0
        else:
            author.ws_comment_display_limit = legacy_author.wscommentdisplaylimit
        author.lat = legacy_author.lat
        author.lng = legacy_author.lng
        author.born_in = legacy_author.bornin
        author.living_in = legacy_author.livingin
        if (
            legacy_author.firstlimdate is not None
            and legacy_author.firstlimdate != "0000-00-00 00:00:00"
        ):
            user.date_joined = legacy_author.firstlimdate
        author.approval_emails = legacy_author.approvalemails == "On"
        author.milestone_emails = legacy_author.milestoneemails == "On"
        author.news_emails = legacy_author.newsemails == "On"
        author.twitter_handle = legacy_author.twitterhandle
        author.new_count = legacy_author.newcount
        author.held_count = legacy_author.heldcount
        author.revised_count = legacy_author.revisedcount
        author.tentative_count = legacy_author.tentativecount
        author.confirming_count = legacy_author.confirmingcount
        author.approved_count = legacy_author.approvedcount
        author.untended_count = legacy_author.untendedcount
        user.last_login = datetime.datetime.fromtimestamp(int(legacy_author.accesstime))
        user.save()
        author.save()

    def import_limericks(self, not_before=None):
        q = Q(originalid__gt=(Limerick.objects.last().pk if Limerick.objects.count() > 0 else -1))
        if not_before:
            q &= Q(datetime__gte=not_before)
        originalids = (
            DilfLimericks.objects.filter(q)
            .values_list("originalid", flat=True)
            .order_by()
            .distinct()
        )
        fct = progressbar.FormatCustomText("%(id)8d", dict(id=0))
        widgets = [
            "Limerick: ",
            progressbar.Percentage(),
            " ",
            fct,
            " ",
            progressbar.Timer(),
            " ",
            progressbar.ETA(),
        ]
        with progressbar.ProgressBar(
            max_value=len(originalids), widgets=widgets
        ) as bar:
            for i, originalid in enumerate(originalids, 1):
                fct.update_mapping(id=originalid)
                bar.update(i)
                self.import_limerick(originalid)
        self.sequence_reset(Limerick)

        # catch the stragglers - make sure every latest revision is marked as such
        rev_first_ids = Limerick.objects.annotate(first_rev=Max('revision')).values_list('first_rev', flat=True)
        Revision.objects.filter(id__in=rev_first_ids).update(is_latest_revision=True)

    def import_limerick(self, originalid):
        """
        Every limerick in the original limerictionary has an originalid,
        which refers back to the first revision of that limerick.
        We use this to make separate Revision and Limerick objects.
        """
        if Limerick.objects.filter(original_id=originalid).exists():
            return
        legacy_versions = DilfLimericks.objects.filter(originalid=originalid).order_by(
            "-version"
        )
        limerick = Limerick.objects.create(original_id=originalid, pk=originalid)
        limerick.limerick_number = legacy_versions[0].limericknumber
        limerick.state = LimStates[legacy_versions[0].state.upper()].value
        limerick.curtained = legacy_versions[0].category == "curtained room"
        self.set_approving_editor(limerick, legacy_versions)

        self.set_held_by(limerick, legacy_versions)
        limerick.save()

        # the above save() triggers an update to state_date via signals
        # because the state is updated.
        # So we need to re-save with the original. :(
        limerick.state_date = legacy_versions.last().statedatetime
        limerick.save()

        revisions = []
        for legacy_revision in reversed(legacy_versions):
            r = self.get_revision(limerick, legacy_revision)
            r.order = len(revisions)
            revisions.append(r)
        revisions[0].is_latest_revision = True
        Revision.objects.bulk_create(revisions)
        lim.signals.do_denormalise_revision(revisions[0])

    def get_revision(self, limerick, legacy_revision):
        primary_author = self.get_user(legacy_revision.primaryauthorid)
        secondary_author = self.get_user(legacy_revision.secondaryauthorid)
        if primary_author is None:
            print(
                f"Warning: could not find primary author {legacy_revision.primaryauthorid} "
                f"for revision {legacy_revision.verseid} - using 'Workshop' instead"
            )
            primary_author = User.objects.get(username="Workshop")
        workshop_state = WorkshopStates[legacy_revision.workshopstate.upper()]
        rev = Revision(
            limerick=limerick,
            primary_author=primary_author,
            secondary_author=secondary_author,
            workshop_state=workshop_state,
        )
        rev.verse = legacy_revision.verse.replace('\0', '')
        rev.datetime = legacy_revision.datetime
        rev.version = legacy_revision.version
        rev.author_notes = legacy_revision.authornotes.replace('\0', '')
        rev.editor_notes = legacy_revision.editornotes.replace('\0', '')
        if legacy_revision.updatedatetime != "0000-00-00 00:00:00":
            rev.updated_at = legacy_revision.updatedatetime
        rev.legacy_verse_id = legacy_revision.verseid
        return rev

    def set_held_by(self, lim, legacy_versions):
        if lim.state == LimStates.HELD:
            he = None
            try:
                for rev in legacy_versions:
                    he = self._deverge(rev.heldbyid)
                    if he is not None and he != 0:
                        lim.held_by = User.objects.get(id=he).author
            except User.DoesNotExist:
                print(
                    f"warning: limerick {lim.pk} held by user ID {he} which has no author"
                )
                lim.held_by = User.objects.get(pk=2)

    def set_approving_editor(self, lim, legacy_versions):
        for rev in legacy_versions:
            ae = self._deverge(rev.approvingeditor)
            if ae is not None and ae != 0:
                try:
                    lim.approving_editor = User.objects.get(id=ae)
                    break
                except User.DoesNotExist:
                    print(f"Warning: lim {lim.original_id}: AE {ae} not found")
                    continue

    def import_defined_words(self):
        print("Removing all DefinedWords ...", end="")
        DefinedWord.objects.all().delete()
        print("done")
        legacy_words = DilfWords.objects.order_by('sequence', 'wordid').filter(
            isphraseword=False
        )
        fct = progressbar.FormatCustomText("%(id)8d %(text)30s", dict(id=0, text=""))
        widgets = [
            "Defined Word: ",
            progressbar.Percentage(),
            " ",
            fct,
            " ",
            progressbar.Timer(),
            " ",
            progressbar.ETA(),
        ]
        legacy_word_texts = set(legacy_words.values_list("wordtext", flat=True).distinct())
        print(f"Bulk creating {len(legacy_word_texts)} DefinedWords ...", end="")
        DefinedWord.objects.bulk_create(
            map(
                lambda x: DefinedWord(word_text=x),
                legacy_word_texts
            )
        )
        print("done")
        legacy_words = legacy_words.filter(
            verseid__in=self.get_legacy_verse_ids()
        )
        with progressbar.ProgressBar(
            max_value=legacy_words.count(), widgets=widgets
        ) as bar:
            for i, word in enumerate(legacy_words, 1):
                fct.update_mapping(id=word.wordid, text=word.wordtext)
                bar.update(i)
                self.import_defined_word(word)
        self.sequence_reset(DefinedWord)

    @staticmethod
    def import_defined_word(legacy_word):
        dws = DefinedWord.objects.filter(word_text=legacy_word.wordtext)
        if dws.count() > 1:
            raise LegacyImporterException(
                "More than one DefinedWord exists for legacy_word "
                + legacy_word.wordtext
            )
        elif dws.count() == 1:
            dw = dws.first()
        else:
            dw = DefinedWord.objects.create(word_text=legacy_word.wordtext)
        revisions = Revision.objects.filter(legacy_verse_id=legacy_word.verseid)
        if revisions.exists():
            revision = revisions.first()
        else:
            return None
        RevisionDefinedWordsThroughModel.objects.create(
            revision=revision,
            defined_word=dw,
            order=revision.definedword_set.count(),
        )
        if revision.is_latest_revision:
            LimerickDefinedWordsThroughModel.objects.create(
                limerick=revision.limerick,
                defined_word=dw,
                order=revision.limerick.definedword_set.count(),
            )

    def import_workshop_comments(self, not_before=None):
        q = (
            Q(workshopid__gt=Comment.objects.all().aggregate(maxid=Max('id'))['maxid'] or -1) &
            Q(verseid__in=self.get_legacy_verse_ids())
        )
        if not_before is not None:
            q &= Q(datetime__gte=not_before)
        workshops = DilfWorkshop.objects.order_by('workshopid').filter(q)
        fct = progressbar.FormatCustomText("id: %(id)8d", dict(id=0))
        widgets = [
            "Comment: ",
            progressbar.Percentage(),
            " ",
            fct,
            " ",
            progressbar.Timer(),
            " ",
            progressbar.ETA(),
        ]
        # Cannot use bulk_create with Comment as it is an ordered model
        with progressbar.ProgressBar(
            max_value=workshops.count(), widgets=widgets
        ) as bar:
            for i, workshop in enumerate(
                workshops.order_by("workshopid").iterator(chunk_size=100), 1
            ):
                fct.update_mapping(id=workshop.workshopid)
                bar.update(i)
                self.create_workshop_comment(workshop)
        self.sequence_reset(Comment)

    def get_legacy_verse_ids(self):
        return list(Revision.objects.all().values_list('legacy_verse_id', flat=True))

    def get_legacy_original_ids(self):
        return list(Limerick.objects.all().values_list("original_id", flat=True))

    def create_workshop_comment(self, workshop):
        revision = Revision.objects.filter(legacy_verse_id=workshop.verseid)
        if not revision.exists():
            print(f"\n{workshop.workshopid}: no associated Revision - skipping")
            return

        return Comment.objects.create(
            revision=revision.first(),
            date_time=workshop.datetime,
            message=workshop.message.replace('\0', ''),
            poster_ip=workshop.posterip,
            edit_date_time=workshop.editdatetime,
            pk=workshop.workshopid,
            author=self.get_user(workshop.authorid) if workshop.authorid > 0 else None,
        )

    def import_rfas(self):
        self.sequence_reset(RFA)
        self.import_all_rfas()
        self.set_rfa_counts()

    def import_all_rfas(self):
        qs = DilfRfas.objects.order_by('id').filter(
            Q(id__gt=RFA.objects.all().aggregate(maxid=Max('id'))['maxid'] or -1) &
            Q(originalid__in=self.get_legacy_verse_ids())
        )
        fct = progressbar.FormatCustomText(
            "%(id)8d lim=%(limerick_id)8d author=%(author_id)8d imported=%(imported)8d",
            dict(id=0, limerick_id=0, author_id=0, imported=0),
        )
        widgets = [
            "RFA: ",
            progressbar.Percentage(),
            " ",
            fct,
            " ",
            progressbar.Timer(),
            " ",
            progressbar.ETA(),
        ]
        with progressbar.ProgressBar(max_value=qs.count(), widgets=widgets) as bar:
            to_import = []
            total_imported = 0
            for i, rfa in enumerate(qs, 1):
                fct.update_mapping(
                    id=rfa.id,
                    limerick_id=rfa.originalid,
                    author_id=rfa.authorid,
                    imported=total_imported
                )
                bar.update(i)
                rfa = self.import_rfa(rfa)
                if rfa is not None:
                    to_import.append(rfa)
                if len(to_import) >= 1000:
                    total_imported += len(to_import)
                    fct.update_mapping(imported=total_imported)
                    RFA.objects.bulk_create(to_import)
                    to_import = []
            total_imported += len(to_import)
            fct.update_mapping(imported=total_imported)
            RFA.objects.bulk_create(to_import)

    def set_rfa_counts(self):
        fct = progressbar.FormatCustomText(
            "%(id)8d count=%(count)2d", dict(id=0, count=0)
        )
        widgets = [
            "Setting RFA count: ",
            progressbar.Percentage(),
            " ",
            fct,
            " ",
            progressbar.Timer(),
            " ",
            progressbar.ETA(),
        ]
        with progressbar.ProgressBar(
            max_value=Revision.objects.count(), widgets=widgets
        ) as bar:
            for i, revision in enumerate(Revision.objects.all(), 1):
                legacy_revision = DilfLimericks.objects.get(pk=revision.legacy_verse_id)
                revision.rfa_count = legacy_revision.rfas
                fct.update_mapping(id=legacy_revision.verseid, count=revision.rfa_count)
                bar.update(i)
                revision.save()
        self.sequence_reset(RFA)

    def import_rfa(self, legacy_rfa):
        author = self.get_user(legacy_rfa.authorid)
        if author is None:
            # legacy author no longer exists in the database
            return None

        limerick = Limerick.objects.filter(original_id=legacy_rfa.originalid)
        if not limerick.exists():
            # legacy limerick no longer exists in the database
            return None

        limerick = limerick.first()

        rfa = RFA(author=author, limerick=limerick, new=(legacy_rfa.new != 0))
        return rfa

    def import_activity_messages(self, not_before=None):
        qs = DilfMessages.objects.filter(verseid__in=self.get_legacy_verse_ids())
        if not_before is not None:
            qs = qs.filter(datetime__gte=not_before)
        fct = progressbar.FormatCustomText("%(id)8d", dict(id=0))
        widgets = [
            "ActivityMessage: ",
            progressbar.Percentage(),
            " ",
            fct,
            " ",
            progressbar.Timer(),
            " ",
            progressbar.ETA(),
        ]
        with progressbar.ProgressBar(max_value=qs.count(), widgets=widgets) as bar:
            for i, message in enumerate(qs.all(), 1):
                fct.update_mapping(id=message.msgid)
                bar.update(i)
                self.import_activity_message(message)
        self.sequence_reset(ActivityMessage)

    def import_activity_message(self, message):
        if ActivityMessage.objects.filter(legacy_message_id=message.msgid).exists():
            return
        source = self.get_user(message.srcid)
        dest = self.get_user(message.dstid)
        author = self.get_user(message.authorid)
        if source is None or dest is None or author is None:
            print(
                f"Warning: no such person: source {source}, dest {dest}, author {author} - skipping"
            )
            return

        revision = Revision.objects.filter(legacy_verse_id=message.verseid)
        if not revision.exists():
            print(f"Warning: no such revision {message.verseid} - skipping")
            return

        ActivityMessage.objects.create(
            source=source,
            dest=dest,
            author=author,
            revision=revision.first(),
            message_type=ActivityMessageType[message.msgtype.replace(" ", "_").upper()],
            date_time=message.datetime,
            data=message.msgdata,
            legacy_message_id=message.msgid,
        )

    def import_single_rows(self):
        self.import_settings()
        # self.import_stats()

    def import_settings(self):
        legacy_settings = DilfSettings.objects.first()
        settings = SiteSettings.get_solo()
        settings.alphabet_end = legacy_settings.alphabetend
        settings.admin_email = legacy_settings.adminemail
        settings.eic_email = legacy_settings.eicemail
        settings.shutdown = legacy_settings.shutdown
        settings.copyright = legacy_settings.copyright
        settings.last_lace_check = datetime.datetime.fromtimestamp(
            int(legacy_settings.lastlacecheck)
        ).isoformat()
        settings.laces_per_check = legacy_settings.lacespercheck
        settings.limerick_number = legacy_settings.limericknumber
        settings.wpsi_submission = legacy_settings.wpsisubmission == "On"
        settings.save()
        self.sequence_reset(SiteSettings)

    def import_stats(self):
        legacy_stats = DilfStats.objects.first()
        stats = SiteStatistics.get_solo()
        stats.total = legacy_stats.total
        stats.approved = legacy_stats.approved
        stats.approved_showcase = legacy_stats.approvedshowcase
        stats.approved_general = legacy_stats.approvedgeneral
        stats.approved_general_showcase = legacy_stats.approvedgeneralshowcase
        stats.save()
        self.sequence_reset(SiteStatistics)

    def import_documents(self):
        SiteDocument.objects.all().delete()
        fct = progressbar.FormatCustomText("%(id)30s", dict(id=""))
        widgets = [
            "SiteDocument: ",
            progressbar.Percentage(),
            " ",
            fct,
            " ",
            progressbar.Timer(),
            " ",
            progressbar.ETA(),
        ]
        with progressbar.ProgressBar(
            max_value=DilfDocuments.objects.count(), widgets=widgets
        ) as bar:
            for i, legacy_document in enumerate(DilfDocuments.objects.all(), 1):
                fct.update_mapping(id=legacy_document.pk)
                bar.update(i)
                doc = SiteDocument.objects.create()
                doc.title = legacy_document.title
                if legacy_document.text is None:
                    doc.text = ""
                else:
                    doc.text = legacy_document.text
                doc.save()
        self.sequence_reset(SiteDocument)

    def import_showcases(self):
        # Showcases need to be imported in the correct order,
        # so that this ordering is preserved in the new OrderedModel.
        # For this reason, we can't use bulk_create.
        Showcase.objects.all().delete()
        fct = progressbar.FormatCustomText(
            "#%(limid)d: author %(authorid)d, pri %(pri)d",
            dict(limid=0, authorid=0, pri=0),
        )
        widgets = [
            "Showcase: ",
            progressbar.Percentage(),
            " ",
            progressbar.ETA(),
            " ",
            fct,
            " ",
            progressbar.Bar(),
        ]
        legacy_showcases = DilfLimericks.objects.filter(
            Q(showcasepriority__gt=0) & Q(state="approved") & Q(originalid__in=self.get_legacy_original_ids())
        ).order_by("primaryauthorid", "-showcasepriority", "-statedatetime")
        i = 0
        with progressbar.ProgressBar(
            max_value=legacy_showcases.count(), widgets=widgets
        ) as bar:
            for legacy_lim in legacy_showcases:
                # print("#{}: author {}, priority {}".format(
                #     legacy_lim.originalid, legacy_lim.primaryauthorid, legacy_lim.showcasepriority
                # ))
                qs = Limerick.objects.filter(original_id=legacy_lim.originalid)
                if qs.exists():
                    limerick = qs.first()
                    user = self.get_user(legacy_lim.primaryauthorid)
                    Showcase.objects.create(user=user, limerick=limerick)
                i += 1
                fct.update_mapping(
                    limid=legacy_lim.originalid,
                    authorid=legacy_lim.primaryauthorid,
                    pri=legacy_lim.showcasepriority,
                )
                bar.update(i)
        self.sequence_reset(Showcase)

    def import_buttons(self):
        TextMacro.objects.all().delete()
        fct = progressbar.FormatCustomText("%(title)30s", dict(id=0, title=""))
        widgets = [
            "Document: ",
            progressbar.Percentage(),
            " ",
            fct,
            " ",
            progressbar.Timer(),
            " ",
            progressbar.ETA(),
        ]
        documents = DilfDocuments.objects.filter(title__startswith="Buttons")
        with progressbar.ProgressBar(max_value=documents.count(), widgets=widgets):
            for document in documents:
                fct.update_mapping(title=document.title)
                if document.title.endswith("EiC"):
                    author_type = AuthorTypes.ADMINISTRATOR
                else:
                    author_type = AuthorTypes.WORKSHOPPER
                buttons = document.text.split("@@Button=")
                for button in buttons[1:]:
                    # print(button)
                    title, body = button.split("@@", 1)
                    TextMacro.objects.create(
                        title=title, body=body, author_type=author_type
                    )
        self.sequence_reset(TextMacro)

    def import_topics(self):
        print("Removing old topics ... ", end="")
        print(Topic.objects.all().delete())
        print("done")
        # self.import_topic_names()
        self.import_topic_relationships()
        self.import_topic_lims()
        self.sequence_reset(Topic)

    def import_topic(self, parent):
        print(f"Importing {parent} ...")
        for subtopic in DilfSubtopics.objects.filter(topicid=parent.id):
            print(f"  Looking at subtopic: {subtopic}")
            if not Topic.objects.filter(pk=subtopic.subid).exists():
                legacy_topic = DilfTopics.objects.get(pk=subtopic.subid)
                topic = parent.add_child(
                    pk=legacy_topic.topicid,
                    name=legacy_topic.name,
                    description=legacy_topic.description,
                )
                self.import_topic(topic)
        print(f"Finished importing {parent}")

    def import_topic_relationships(self):
        legacy_root = DilfTopics.objects.get(pk=1)
        root = Topic.add_root(
            pk=1, name=legacy_root.name, description=legacy_root.description
        )
        self.import_topic(root)

    def import_topic_lims(self):
        fct = progressbar.FormatCustomText(
            "#%(topicid)8d: %(name)30s lim=%(lim)8d", dict(topicid=0, name="", lim=0)
        )
        widgets = [
            "Topic limerick: ",
            progressbar.Percentage(),
            " ",
            progressbar.ETA(),
            " ",
            fct,
            " ",
            progressbar.Bar(),
        ]
        legacy_original_ids = self.get_legacy_original_ids()
        legacy_topiclims = DilfTopiclims.objects.filter(topicid=topic.id, originalid__in=legacy_original_ids)
        with progressbar.ProgressBar(
            max_value=legacy_topiclims.count(), widgets=widgets
        ) as bar:
            i = 0
            for topic in Topic.objects.all():
                for mapping in legacy_topiclims:
                    fct.update_mapping(
                        topicid=topic.id, name=topic.name, lim=mapping.originalid
                    )
                    bar.update(i)
                    i += 1
                    limerick = Limerick.objects.get(original_id=mapping.originalid)
                    topic.limerick_set.add(limerick)
                topic.save()

    def import_limerick_watchers(self):
        LimerickWatcher.objects.all().delete()
        fct = progressbar.FormatCustomText(
            "ID: %(id)8d, imported: %(total)8d", dict(id=0, total=0)
        )
        widgets = [
            "Limerick watchers: ",
            progressbar.Percentage(),
            " ",
            progressbar.Timer(),
            " ",
            fct,
            " ",
            progressbar.ETA(),
        ]
        oeds = list(Limerick.objects.all().values_list('original_id', flat=True))
        monitors = DilfMonitors.objects.filter(originalid__in=oeds)
        with progressbar.ProgressBar(
            max_value=monitors.count(), widgets=widgets
        ) as bar:
            total_imported = 0
            for i, legacy_monitor in enumerate(monitors, 1):
                fct.update_mapping(id=legacy_monitor.pk, total=total_imported)
                bar.update(i)
                try:
                    author = self.get_user(legacy_monitor.authorid)
                    limerick = Limerick.objects.get(
                        original_id=legacy_monitor.originalid
                    )
                    if author is not None:
                        LimerickWatcher.objects.get_or_create(author=author, limerick=limerick)
                    total_imported += 1
                except (User.DoesNotExist, Limerick.DoesNotExist):
                    pass

    @staticmethod
    def sequence_reset(*args):
        sequence_sql = connection.ops.sequence_reset_sql(no_style(), [*args])
        with connection.cursor() as cursor:
            for sql in sequence_sql:
                cursor.execute(sql)

    @staticmethod
    def import_wordlist():
        fct = progressbar.FormatCustomText(
            "ID: %(id)8d, %(word_text)30s imported: %(total)8d",
            dict(id=0, word_text="", total=0),
        )
        widgets = [
            "WordList: ",
            progressbar.Percentage(),
            " ",
            progressbar.Timer(),
            " ",
            fct,
            " ",
            progressbar.ETA(),
        ]
        with progressbar.ProgressBar(
            max_value=DilfWordlist.objects.count(), widgets=widgets
        ) as bar:
            to_import = []
            total_imported = 0
            for i, legacy_wordlist in enumerate(DilfWordlist.objects.all(), 1):
                fct.update_mapping(
                    id=legacy_wordlist.pk, word_text=legacy_wordlist.wordtext
                )
                bar.update(i)
                if not DefinedWord.objects.filter(
                    word_text=legacy_wordlist.wordtext
                ).exists():
                    to_import.append(DefinedWord(word_text=legacy_wordlist.wordtext))
                if i % 1000 == 0:
                    total_imported += len(to_import)
                    fct.update_mapping(total=total_imported)
                    DefinedWord.objects.bulk_create(to_import)
                    to_import = []
            total_imported += len(to_import)
            fct.update_mapping(total=total_imported)
            DefinedWord.objects.bulk_create(to_import)

    def import_tags(self):
        Tag.objects.all().delete()
        self.sequence_reset(Tag)
        fct = progressbar.FormatCustomText(
            "ID: %(id)8d, %(text)30s",
            dict(id=0, text="")
        )
        widgets = [
            "Tag: ",
            progressbar.Percentage(),
            " ",
            progressbar.Timer(),
            " ",
            fct,
            " ",
            progressbar.ETA()
        ]
        with progressbar.ProgressBar(
            max_value=DilfTags.objects.count(), widgets=widgets
        ) as bar:
            for i, legacy_tag in enumerate(DilfTags.objects.all(), 1):
                fct.update_mapping(
                    id=legacy_tag.pk, text=legacy_tag.tagtext
                )
                bar.update(i)
                author = self.get_user(legacy_tag.authorid)
                if author is not None:
                    tag, created = Tag.objects.get_or_create(
                        author=author,
                        text=legacy_tag.tagtext
                    )
                    limericks = Limerick.objects.filter(original_id=legacy_tag.originalid)
                    tag.limericks.add(*limericks)
