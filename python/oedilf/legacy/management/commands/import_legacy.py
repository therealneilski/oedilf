from dateutil.parser import parse
from django.core.management.base import BaseCommand, CommandError

from legacy.management.legacyimporter import LegacyImporter


class Command(BaseCommand):
    help = "Migrate from the old OEDILF PHP database to the new Django database"

    import_types = (
        "authors",
        "limericks",
        "dw",
        "workshop",
        "rfa",
        "activity",
        "documents",
        "single",
        "showcase",
        "tags",
        "topic",
        "limerick_watchers",
        "wordlist"
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "-i",
            "--import_types",
            nargs="+",
            choices=sorted(self.import_types),
            dest="imports",
            help="Only import the specified types of object",
        )
        parser.add_argument(
            "--not-before",
            type=parse,
            dest="not-before",
            default=None,
            help="Do not import objects dated before this date"
        )

    def handle(self, *args, **options):
        importer = LegacyImporter()
        try:
            if not options["imports"]:
                options["imports"] = self.import_types
            if "authors" in options["imports"]:
                importer.import_authors()
            if "limericks" in options["imports"]:
                importer.import_limericks(options["not-before"])
                importer.import_limerick_watchers()
            elif "limerick_watchers" in options["imports"]:
                importer.import_limerick_watchers()
            if "workshop" in options["imports"]:
                importer.import_workshop_comments(options["not-before"])
            if "dw" in options["imports"]:
                importer.import_defined_words()
            if "rfa" in options["imports"]:
                importer.import_rfas()
            if "activity" in options["imports"]:
                importer.import_activity_messages(options["not-before"])
            if "documents" in options["imports"]:
                importer.import_documents()
                importer.import_buttons()
            if "single" in options["imports"]:
                importer.import_single_rows()
            if "showcase" in options["imports"]:
                importer.import_showcases()
            if "tags" in options["imports"]:
                importer.import_tags()
            if "topic" in options["imports"]:
                importer.import_topics()
            if "wordlist" in options["imports"]:
                importer.import_wordlist()
        except Exception as e:
            raise CommandError(e)
