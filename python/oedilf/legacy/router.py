class LegacyRouter(object):

    LABEL = "legacy"

    """
    This router forces all models in the 'legacy' application
    to use the 'legacy' database.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read blog models go to my_blog.
        """
        if model._meta.app_label == self.LABEL:
            return 'legacy'
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        return app_label != self.LABEL
