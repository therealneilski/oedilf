# Generated by Django 2.1.5 on 2019-01-30 17:31
# Need to apply this with `python manage.py migrate --database legacy legacy`

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('legacy', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(
            "ALTER TABLE DILF_Monitors ADD COLUMN id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT",
            "ALTER TABLE DILF_Monitors DROP COLUMN id",
            state_operations=[
                migrations.AddField(
                    'DilfMonitors',
                    'id',
                    models.IntegerField(),
                ),
            ],

        )
    ]
