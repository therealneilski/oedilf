if [ -f .env ] ; then
	autostash $(grep -v '^#' .env |sed 's/^export \([^=]*\)=.*/\1/')
	. ./.env
	echo "Loaded variables from .env"
fi
