--
-- This file contains things which need to be done to the legacy database
-- before migration can be attempted.
--

-- Convert to InnoDB so that we can introduce foreign keys

ALTER TABLE DILF_Authors ENGINE = InnoDB;
ALTER TABLE DILF_Countries ENGINE = InnoDB;
ALTER TABLE DILF_Documents ENGINE = InnoDB;
ALTER TABLE DILF_LastVisit ENGINE = InnoDB;
ALTER TABLE DILF_Limericks ENGINE = InnoDB;
ALTER TABLE DILF_Log ENGINE = InnoDB;
ALTER TABLE DILF_Messages ENGINE = InnoDB;
ALTER TABLE DILF_Monitors ENGINE = InnoDB;
ALTER TABLE DILF_RFAs ENGINE = InnoDB;
ALTER TABLE DILF_Random ENGINE = InnoDB;
ALTER TABLE DILF_Ranks ENGINE = InnoDB;
ALTER TABLE DILF_Settings ENGINE = InnoDB;
ALTER TABLE DILF_Stats ENGINE = InnoDB;
ALTER TABLE DILF_SubTopics ENGINE = InnoDB;
ALTER TABLE DILF_Tags ENGINE = InnoDB;
ALTER TABLE DILF_Tokens ENGINE = InnoDB;
ALTER TABLE DILF_TopicLims ENGINE = InnoDB;
ALTER TABLE DILF_Topics ENGINE = InnoDB;
ALTER TABLE DILF_WEAP ENGINE = InnoDB;
ALTER TABLE DILF_WordList ENGINE = InnoDB;
ALTER TABLE DILF_Words ENGINE = InnoDB;
ALTER TABLE DILF_Workshop ENGINE = InnoDB;
ALTER TABLE banner_sets ENGINE = InnoDB;
ALTER TABLE banner_votes ENGINE = InnoDB;


--
-- RFAs table
--
-- Clean up data
DELETE FROM DILF_RFAs WHERE OriginalId NOT IN (SELECT OriginalId FROM DILF_Limericks);
DELETE FROM DILF_RFAs where AuthorId NOT IN (SELECT AuthorId FROM DILF_Authors);
-- Ensure there is a PK for Django to access
ALTER TABLE `DILF_RFAs` ADD COLUMN `id` INT AUTO_INCREMENT PRIMARY KEY UNIQUE FIRST;

--
-- Topics
--
-- Ensure there is a PK for Django to access
ALTER TABLE `DILF_SubTopics` ADD COLUMN `id` INT AUTO_INCREMENT PRIMARY KEY UNIQUE FIRST;
DELETE FROM `DILF_TopicLims` WHERE OriginalId NOT IN (SELECT OriginalId FROM DILF_Limericks);
ALTER TABLE `DILF_TopicLims` ADD COLUMN `id` INT AUTO_INCREMENT PRIMARY KEY UNIQUE FIRST;
