from django import template

from lim.utils import oxford_join as do_oxford_join

register = template.Library()


@register.simple_tag
def oxford_join(list_to_join, sep=", ", final_sep=", and ", strip_empty=False):
    return do_oxford_join(list_to_join, sep, final_sep, strip_empty)
