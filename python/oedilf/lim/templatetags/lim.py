import datetime
import re

from bs4 import BeautifulSoup
from django import template
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import (
    urlize,
    stringfilter,
    linebreaks,
    escape,
    pluralize,
)
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.functional import keep_lazy_text
from django.utils.html import format_html, mark_safe
from django.utils.timezone import now
from lim.models import (
    LimStates,
    Limerick,
    Comment,
    Revision,
    CFA,
    CFAResponse,
    SiteSettings,
)
from rules.contrib.views import permission_required

register = template.Library()


@register.simple_tag
def lim_curtained_badge(limerick):
    return format_html(
        """
        <span id="curtain-{}" class="{} badge badge-warning"><i class="fa fa-exclamation-triangle"></i> Curtained</span>
    """.format(
            limerick.latest_revision.version,
            "d-inline" if limerick.curtained else "d-none",
        )
    )


@register.simple_tag()
def lim_state_badge(limerick):
    # enum('tentative','obsolete','approved','held','excluded','stored','revised','confirming','new','untended')
    # badges: default success warning error info inverse
    return state_badge(limerick.state)


def state_badge(state):
    state_classes = {
        LimStates.TENTATIVE: "badge-secondary",
        LimStates.OBSOLETE: "badge-warning",
        LimStates.APPROVED: "badge-success",
        LimStates.HELD: "badge-warning",
        LimStates.EXCLUDED: "badge-danger",
        LimStates.STORED: "badge-danger",
        LimStates.REVISED: "badge-default",
        LimStates.CONFIRMING: "badge-info",
        LimStates.NEW: "badge-default",
        LimStates.UNTENDED: "badge-warning",
    }
    if isinstance(state, LimStates):
        lim_state = state
    else:
        lim_state = LimStates(state)
    return format_html(
        '<span class="badge {}">{}</span>', state_classes[lim_state], lim_state.label
    )


@register.simple_tag()
def revision_state_badge(revision):
    if revision.limerick.latest_revision == revision:
        return lim_state_badge(revision.limerick)

    else:
        return state_badge(LimStates.OBSOLETE)


@register.simple_tag()
def lim_rfas_badge(limerick):
    self_rfa = limerick.rfa_set.filter(author=limerick.primary_author).exists()
    if limerick.secondary_author is not None:
        self_rfa &= limerick.rfa_set.filter(author=limerick.secondary_author).exists()
    tooltip = ", ".join(limerick.rfa_set.values_list("author__username", flat=True))
    return revision_rfas_badge(
        limerick.latest_revision, check=self_rfa, tooltip=tooltip
    )


@register.simple_tag()
def revision_rfas_badge(revision, check=False, tooltip=None):
    authors = revision.limerick.authors
    if revision.is_latest_revision and revision.limerick.rfa_set.filter(
        author__in=authors
    ).count() == len(authors):
        check = True
    if check:
        badge_class = "badge-success"
    elif revision.rfa_count >= 5:
        badge_class = "badge-primary"
    elif revision.rfa_count > 0:
        badge_class = "badge-secondary"
    else:
        badge_class = "badge-warning"
    if tooltip is None:
        tooltip = "{} author{} consider{} this limerick Ready for Approval".format(
            revision.rfa_count,
            pluralize(revision.rfa_count),
            pluralize(revision.rfa_count, "s,"),
        )
    result = format_html(
        '<span id="rfa-badge-{}">'
        '<span class="badge {}" data-toggle="tooltip" title="{}">RFAs '
        '<span class="badge badge-light black-text">{}</span>',
        revision.version,
        badge_class,
        tooltip,
        revision.rfa_count,
    )
    if check:
        result += format_html(' <i class="fa fa-check"></i>')
    result += format_html("</span></span>")

    return result


@register.inclusion_tag("lim/inclusion/limerick-metadata.html", takes_context=True)
def lim_display_metadata(context, limerick):
    context["limerick"] = limerick
    return context


@register.inclusion_tag("lim/inclusion/limerick-home-metadata.html", takes_context=True)
def lim_display_homepage_metadata(context, limerick):
    context["limerick"] = limerick
    return context


@register.inclusion_tag("lim/inclusion/revision-metadata.html", takes_context=True)
def revision_display_metadata(context, limerick, include_revision_number=True):
    context["limerick"] = limerick
    context["include_revision_number"] = include_revision_number
    return context


@register.simple_tag()
def lim_workshop_button(limerick):
    return format_html(
        '<a href="{}" class="btn btn-primary btn-sm card-link">Workshop</a>',
        reverse("workshop", args=[limerick.id]),
    )


@register.simple_tag()
def revision_display_limerick(revision):
    html = format_html("")
    if "@@ENDPLAN@@" in revision.author_notes:
        html += format_html(
            '<div class="lim-notes">{}</div><br/>',
            mark_safe(
                linebreaks(
                    revision.author_notes[: revision.author_notes.index("@@ENDPLAN@@")]
                )
            ),
        )
    html += format_html(
        '<div class="lim-verse">{}</div><br/>',
        mark_safe(linebreaksdiv(revision.verse, css_class="hanging-indent")),
    )
    if "@@ENDPLAN@@" in revision.author_notes:
        html += format_html(
            '<div class="lim-notes">{}</div><br/>',
            mark_safe(
                linebreaks(
                    revision.author_notes[
                        revision.author_notes.index("@@ENDPLAN@@") + 11 :
                    ]
                )
            ),
        )
    else:
        html += format_html(
            '<div class="lim_notes">{}</div><br/>',
            mark_safe(linebreaks(revision.author_notes)),
        )
    if revision.editor_notes:
        html += format_html(
            '<div class="lim_notes"><strong>Editor Notes:</strong><br/><br/>{}</div><br/>',
            mark_safe(linebreaks(revision.editor_notes)),
        )
    return lim_links(urlize(html))


@permission_required("lim.view_limerick")
@register.inclusion_tag("lim/cards/limerick-card.html", takes_context=True)
def limerick_card(context, limerick):
    if isinstance(limerick, Revision):
        context["limerick"] = limerick.limerick
    else:
        context["limerick"] = limerick
    if hasattr(limerick, "tags") and context["request"].user.is_authenticated:
        context["tags"] = limerick.tags.filter(author=context["request"].user)
    return context


@register.inclusion_tag("lim/cards/limerick-waypoint-card.html", takes_context=True)
def limerick_waypoint_card(context, limerick):
    if isinstance(limerick, Revision):
        context["limerick"] = limerick.limerick
    else:
        context["limerick"] = limerick
    return context


@permission_required("lim.view_limerick")
@register.inclusion_tag("lim/cards/limerick-home-card.html", takes_context=True)
def limerick_home_card(context, limerick):
    context["limerick"] = limerick
    return context


@register.inclusion_tag("lim/cards/revision-card.html", takes_context=True)
def revision_card(context, revision):
    context["revision"] = revision
    return context


@register.simple_tag()
def lim_display_limerick(limerick):
    html = format_html("")
    if "@@ENDPLAN@@" in limerick.latest_revision.author_notes:
        html += format_html(
            '<div class="lim-notes">{}</div><br/>',
            mark_safe(
                linebreaks(
                    limerick.latest_revision.author_notes[
                        : limerick.latest_revision.author_notes.index("@@ENDPLAN@@")
                    ]
                )
            ),
        )
    html += format_html(
        '<div class="lim-verse">{}</div><br/>',
        mark_safe(
            linebreaksdiv_filter(
                limerick.latest_revision.verse, css_class="hanging-indent"
            )
        ),
    )
    if "@@ENDPLAN@@" in limerick.latest_revision.author_notes:
        html += format_html(
            '<div class="lim-notes">{}</div><br/>',
            mark_safe(
                linebreaks(
                    limerick.latest_revision.author_notes[
                        limerick.latest_revision.author_notes.index("@@ENDPLAN@@")
                        + 11 :
                    ]
                )
            ),
        )
    else:
        html += format_html(
            '<div class="lim_notes">{}</div><br/>',
            mark_safe(linebreaks(limerick.latest_revision.author_notes)),
        )
    if limerick.latest_revision.editor_notes:
        html += format_html(
            '<div class="lim_notes"><strong>Editor Notes:</strong><br/><br/>{}</div><br/>',
            mark_safe(linebreaks(limerick.latest_revision.editor_notes)),
        )
    return lim_links(urlize(html))


@register.simple_tag()
def lim_display_limerick_only(limerick):
    html = format_html(
        '<div class="lim-verse">{}</div>',
        mark_safe(linebreaks(limerick.latest_revision.verse)),
    )
    return lim_links(urlize(html))


@register.inclusion_tag("lim/cards/random-card.html", takes_context=True)
def random_limerick_card(context):
    return context


@register.simple_tag()
def lim_rev_display(revision):
    html = format_html(
        """
        <span class="h2">
            Revision {} of Limerick #{}
            by {}
            """,
        revision.version,
        revision.limerick.id,
        revision.author_names,
    )
    html += format_html(
        """
            </span>
        <small class="text-muted">{}</small>
        <br/>
    """,
        revision.updated_at,
    )
    if "@@ENDPLAN@@" in revision.author_notes:
        html += format_html(
            '<div class="lim-notes">{}</div><br/>',
            mark_safe(
                linebreaks(
                    revision.author_notes[: revision.author_notes.index("@@ENDPLAN@@")]
                )
            ),
        )
    html += format_html(
        '<div class="lim-verse">{}</div><br/>', mark_safe(linebreaks(revision.verse))
    )
    if "@@ENDPLAN@@" in revision.author_notes:
        html += format_html(
            '<div class="lim-notes">{}</div><br/>',
            mark_safe(
                linebreaks(
                    revision.author_notes[
                        revision.author_notes.index("@@ENDPLAN@@") + 11 :
                    ]
                )
            ),
        )
    else:
        html += format_html(
            '<div class="lim_notes">{}<div><br/>',
            mark_safe(linebreaks(revision.author_notes)),
        )
    if revision.editor_notes:
        html += format_html(
            '<div class="lim_notes"><strong>Editor Notes:</strong><br/><br/>{}<div><br/>',
            mark_safe(linebreaks(revision.editor_notes)),
        )
    return lim_links(urlize(html))


# Return the Gravatar image for a user, if there is one.
# Cache the result for 24 hours, or until there is a change in an Author


@register.simple_tag()
def lim_gravatar_image(user, size=150, scale=False):
    from django_gravatar.helpers import get_gravatar_url
    from django.conf import settings

    if user is None:
        return format_html('<i class="fas fa-info-circle"></i>')

    elif settings.OEDILF_USE_GRAVATAR:
        return format_html(
            '<img class="gravatar" src="{}" width="{}" height="{}" alt=""></img>',
            get_gravatar_url(user.email, size=size),
            size,
            size,
        )

    elif scale:
        return format_html(f"<i style='font-size: {size}px' class='fas fa-user'></i>")

    else:
        return format_html("<i class='fas fa-user'></i>")


@register.simple_tag()
def verbose_order_by(sort_by):
    if isinstance(sort_by, tuple):
        return "; ".join(verbose_order_by(s) for s in sort_by)

    if sort_by is None:
        return None

    elif sort_by.startswith("-"):
        direction = "descending"
        sort_by = sort_by[1:]
    else:
        direction = "ascending"
    sort_by = sort_by.replace("_count", "")
    return f"{sort_by}, {direction}"


# https://stackoverflow.com/a/35568978


@register.filter(name="range")
def _range(_min, args=None):
    _max, _step = None, None
    if args:
        if not isinstance(args, int):
            _max, _step = map(int, args.split(","))
        else:
            _max = args
    args = filter(None, (_min, _max, _step))
    return range(*args)


def _get_page_numbers(current, total):
    if current < 4:
        result = list(range(1, min(6, total + 1)))
        if total >= 6:
            result += [None]
    elif current > total - 3:
        result = [None] + list(range(total - 4, total + 1))
    else:
        result = [None] + list(range(current - 2, current + 3)) + [None]
    return result


def get_page_link(context, page):
    request = context["request"]
    get_copy = request.GET.copy()
    get_copy["page"] = page
    return f"{request.path}?{get_copy.urlencode()}"


@register.simple_tag(takes_context=True)
def lim_table_pagination(context, size="md"):
    """
        Replace django-tables2 pagination with Bootstrap-style pagination including more than just previous/next.
    :param context: Template context
    :param size: Bootstrap size (sm, md, lg) to display the pagination (default: md)
    :return:
    """
    table = context["table"]
    current, total = table.page.number, table.paginator.num_pages
    return bootstrap_style_pagination(context, current, size, total)


@register.simple_tag(takes_context=True)
def lim_el_pagination(context, current, total, size="md"):
    """
    Replace django-el-pagination with Bootstrap-style pagination.
    :param context:
    :param current:
    :param total:
    :param size:
    :return:
    """
    return bootstrap_style_pagination(context, current.number, size, total.number)


@register.simple_tag(takes_context=True)
def lim_django_pagination(context, page, size="md"):
    return bootstrap_style_pagination(
        context, page.number, size, page.paginator.num_pages
    )


def bootstrap_style_pagination(context, current, size, total):
    if total == 1:
        result = format_html(
            """
                <nav class="nav-center">
                    <ul class="pagination pagination-{} pg-blue">
                        <li class="page-item disabled">
                        <span class="page-link" aria-label="First">
                            <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                            <span class="sr-only">Previous</span>
                        </span>
                    </li>
                    <li class="page-item disabled">
                        <span class="page-link" aria-label="Previous">
                            <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            <span class="sr-only">Previous</span>
                        </span>
                    </li>
                    <li class="page-item active">
                        <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="page-item disabled">
                        <span class="page-link" aria-label="Next">
                            <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            <span class="sr-only">Next</span>
                        </span>
                    </li>
                    <li class="page-item disabled">
                        <span class="page-link" aria-label="Last">
                            <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                            <span class="sr-only">Last</span>
                        </span>
                    </li>
                </ul>
            </nav>
            """,
            size,
        )
        return result

    pages = _get_page_numbers(current, total)
    if current == 1:
        result = format_html(
            """
            <nav class="nav-center">
                <ul class="pagination pagination-{} pg-blue">
                    <li class="page-item disabled">
                        <span class="page-link" aria-label="First">
                            <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                            <span class="sr-only">Previous</span>
                        </span>
                    </li>
                    <li class="page-item disabled">
                        <span class="page-link" aria-label="Previous">
                            <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            <span class="sr-only">Previous</span>
                        </span>
                    </li>
            """,
            size,
        )
    else:
        result = format_html(
            """
            <nav>
                <ul class="pagination pagination-{} pg-blue">
                    <li class="page-item ">
                        <a href="{}" class="page-link" aria-label="First">
                            <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                            <span class="sr-only">First</span>
                        </a>
                    </li>
                    <li class="page-item ">
                        <a href="{}" class="page-link" aria-label="Previous">
                            <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
            """,
            size,
            get_page_link(context, 1),
            get_page_link(context, current - 1),
        )
    for page in pages:
        if page is None:
            result += format_html(
                """
                    <li class="page-link">
                        <span class="page-item disabled"><i class="fa fa-ellipsis-h"></i></span>
                    </li>
                """
            )
        elif page == current:
            result += format_html(
                """
                    <li class="page-item active">
                        <a class="page-link" href="#">{} <span class="sr-only">(current)</span></a>
                    </li>
                """,
                page,
            )
        elif page > 0:
            result += format_html(
                """
                    <li class="page-item">
                        <a class="page-link" href="{}">{}</span></a>
                    </li>
                """,
                get_page_link(context, page),
                page,
            )
    if current == total:
        result += format_html(
            """
                    <li class="page-item disabled">
                        <span class="page-link" aria-label="Next">
                            <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            <span class="sr-only">Next</span>
                        </span>
                    </li>
                    <li class="page-item disabled">
                        <span class="page-link" aria-label="Last">
                            <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                            <span class="sr-only">Last</span>
                        </span>
                    </li>
                </ul>
            </nav>
            """
        )
    else:
        result += format_html(
            """
                    <li class="page-item">
                        <a href="{}" class="page-link" aria-label="Next">
                            <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a href="{}" class="page-link" aria-label="Last">
                            <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                            <span class="sr-only">Last</span>
                        </a>
                    </li>
                </ul>
            </nav>
            """,
            get_page_link(context, current + 1),
            get_page_link(context, total),
        )
    return result


def expand_links(m):
    t = m.groups()
    limerick = None
    if re.compile(r'^T\d+$').match(t[0]):
        limerick = Limerick.objects.filter(original_id=t[0][1:])
    elif re.compile(r'^\d+$').match(t[0]):
        limerick = Limerick.objects.filter(limerick_number=t[0])
    if limerick is None or not limerick.exists():
        limerick = Limerick.objects.filter(pk=t[0])
    if not limerick.exists():
        return "[[#{}{}]]".format(t[0], "" if t[1] is None else t[1])
    url = limerick.first().get_absolute_url()
    if t[2] is None:
        text = f"#{t[0]}"
    else:
        text = t[2]
    return mark_safe('<a href="{}">{}</a>'.format(url, text))


# noinspection PyUnusedLocal


@register.filter(needs_autoescape=True)
@stringfilter
def lim_links(text, autoescape=True):
    pattern = re.compile(r"\[\[#(\w+)(:(.*?))?\]\]")
    try:
        return mark_safe(pattern.sub(lambda x: expand_links(x), text))
    except Limerick.DoesNotExist:
        return text


@register.inclusion_tag("lim/inclusion/word-with-definitions.html", takes_context=True)
def word_with_definitions(context, word):
    context["word"] = word
    return context


@register.inclusion_tag("lim/inclusion/workshop-comment.html", takes_context=True)
def workshop_comment(context, comment):
    if isinstance(comment, Comment):
        context["comment"] = comment
    else:
        context["comment"] = get_object_or_404(Comment, pk=comment)
    return context


@register.filter()
def strip(s):
    return mark_safe(s.strip())


@keep_lazy_text
def linebreaksdiv(value, autoescape=False, css_class=None):
    """Surround each line in a <div></div>"""
    lines = str(value).split("\n")
    class_attr = "" if css_class is None else ' class="{}"'.format(css_class)
    if autoescape:
        lines = ["<div{}>{}</div>".format(class_attr, escape(p)) for p in lines]
    else:
        lines = ["<div{}>{}</div>".format(class_attr, p) for p in lines]
    return "\n".join(lines)


@register.filter(name="linebreaksdiv")
def linebreaksdiv_filter(value, css_class=None):
    return linebreaksdiv(value, css_class=css_class)


@keep_lazy_text
@register.filter
def linebreaks_for_trumbowyg(value):
    soup = BeautifulSoup(value, "html.parser")
    for br in soup.find_all("br"):
        br.decompose()
    for div in soup.find_all("div"):
        if len(div.contents) == 0:
            div.append(soup.new_tag("br"))
    return str(soup)


@register.simple_tag(takes_context=True)
def confirmation_header(context, limerick):
    if not limerick.is_confirming:
        return format_html("")

    result = format_html(
        """
            <br/>
            <i class="fa fa-check"></i>
            Approval started by {}.
            <br/>
            <i class="fa fa-stopwatch"></i>
            Confirmation delay {} {}
        """,
        limerick.approving_editor.username,
        "ended" if limerick.confirmation_countdown_end_date < now() else "ends",
        limerick.confirmation_countdown_end_date.strftime("%c"),
    )
    if context["request"].user.has_perm("lim.send_lace", limerick):
        laces = limerick.lace_set.count()
        if laces > 0:
            result += format_html(
                """
                    <br/>
                    <span class="badge badge-info">
                        <i class="fa fa-envelope"></i>
                        {} LACE{} sent
                    </span>
                """,
                laces,
                "" if laces == 1 else "s",
            )
            most_recent_lace_date = limerick.lace_set.first().date
            if most_recent_lace_date is not None:
                result += format_html(
                    "<small>(last {})</small>", most_recent_lace_date.strftime("%c")
                )
        else:
            result += format_html(
                """
                    <br/>
                    <span class="badge badge-warning">
                        <i class="fa fa-envelope"></i>
                        LACE NOT sent
                    </span>
                """
            )
    return result


@register.simple_tag()
def topic_badge(topic, style="info"):
    result = format_html(
        "<span>"
        '<a href="{}" class="badge badge-{} text-wrap" data-toggle="tooltip" title="{}">'
        "{}</a></span>",
        reverse("limericks-by-topic", kwargs=dict(topic_pk=topic.pk)),
        style,
        topic.description,
        topic.name,
    )
    return result


@register.simple_tag()
def tag_badge(tag, style="light"):
    result = format_html(
        """
        <span class="badge badge-{} text-wrap" data-toggle="tooltip" data-title="Personal tag: {}">
            {}
        </span>
        """,
        style,
        tag.text,
        tag.text,
    )
    return result


@register.simple_tag()
def active_cfa_messages(user):
    CFAResponse.objects.filter(
        dismissed=True,
        date_time__lt=datetime.datetime.now()
        - datetime.timedelta(seconds=SiteSettings.get_solo().cfa_dismissal_timeout),
    ).delete()
    cfas = CFA.objects.filter(active=True).exclude(cfaresponse__author=user)
    if user.author.curtain_filtering:
        cfas = cfas.exclude(limerick__curtained=True)
    return render_to_string(
        "lim/inclusion/cfa-messages.html", context=dict(cfas=cfas, user=user)
    )


@register.simple_tag()
def limerick_pk(limerick_or_revision):
    if isinstance(limerick_or_revision, Revision):
        return limerick_or_revision.limerick.pk
    else:
        return limerick_or_revision.pk


@register.filter()
@stringfilter
def first_paragraph(text, autoescape=True):
    soup = BeautifulSoup(text, "html.parser")
    if soup.find("p"):
        return soup.find("p").text
    else:
        return text


@register.filter()
def has_more_paragraphs(text):
    soup = BeautifulSoup(text, "html.parser")
    return len(soup.find_all("p")) > 1
