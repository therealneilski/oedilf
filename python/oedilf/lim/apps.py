from django.apps import AppConfig


class LimConfig(AppConfig):
    name = "lim"

    def ready(self):
        import lim.signals

        assert lim.signals is not None
