import urllib.parse

from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.urls import resolve
from django_filters.rest_framework import DjangoFilterBackend, FilterSet, Filter
from drf_enum_field.serializers import EnumFieldSerializerMixin
from hashid_field.rest import HashidSerializerCharField
from rest_framework import permissions
from rest_framework import serializers, viewsets, filters, pagination
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework_bulk import (
    BulkListSerializer,
    BulkSerializerMixin,
    BulkModelViewSet,
)
from rest_framework_bulk.routes import BulkRouter
from rest_framework_tricks.serializers import HyperlinkedModelSerializer

from .models import *


def get_object_from_hyperlink(url):
    path = urllib.parse.urlparse(url).path
    func, _, kwargs = resolve(path)
    return func.cls.serializer_class.Meta.model.objects.get(**kwargs)


class Select2Pagination(pagination.PageNumberPagination):
    def get_paginated_response(self, data):
        return Response(
            {
                'metadata': {
                    "links": {
                        'next': self.get_next_link(),
                        'previous': self.get_previous_link(),
                    },
                    'total': self.page.paginator.count,
                    "page": self.page.number,
                    "pages": self.page.end_index(),
                },
                "paginate": {"more": self.get_next_link() is not None},
                'results': data,
            }
        )


class Select2SearchFilter(filters.SearchFilter):

    search_param = "q"


class Select2TermFilter(filters.SearchFilter):

    search_param = "term"


class ListFilter(Filter):
    def filter(self, queryset, value):
        if value:
            value_list = value.split(",")
            return queryset.filter(pk__in=value_list)
        else:
            return queryset


class ActivityMessageHashidFilterSet(FilterSet):
    id = ListFilter(field_name="id")

    class Meta:
        model = ActivityMessage
        fields = ["id"]


class ActivityMessageSerializer(
    BulkSerializerMixin,
    EnumFieldSerializerMixin,
    serializers.HyperlinkedModelSerializer,
):
    id = HashidSerializerCharField(
        source_field="lim.ActivityMessage.id", read_only=True
    )

    class Meta:
        model = ActivityMessage
        fields = (
            "id",
            "href",
            "data",
            "date_time",
            "seen",
            "source",
            "dest",
            "author",
            "message_type",
        )
        list_serializer_class = BulkListSerializer


class ActivityMessageUserFilterBackend(filters.BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    """

    def filter_queryset(self, request, queryset, view):
        return queryset.filter(dest=request.user)


# class ActivityMessageViewSet(viewsets.ReadOnlyModelViewSet):
class ActivityMessageViewSet(BulkModelViewSet):
    serializer_class = ActivityMessageSerializer
    queryset = ActivityMessage.objects.all().order_by("-date_time")
    filter_backends = (DjangoFilterBackend, ActivityMessageUserFilterBackend)
    filterset_class = ActivityMessageHashidFilterSet
    pagination_class = pagination.PageNumberPagination

    def allow_bulk_destroy(self, qs, filtered):
        return (
            super(ActivityMessageViewSet, self).allow_bulk_destroy(qs, filtered)
            or self.request.query_params.get("id", "") != ""
        )


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ("id", "href", "username")


class UserDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "href",
            "username",
            "email",
            "is_staff",
            "tag_set",
            "showcase_set",
        )


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ("username",)
    ordering_fields = ["username"]
    pagination_class = pagination.PageNumberPagination

    def get_queryset(self):
        return User.objects.all().order_by("pk")

    def retrieve(self, request, pk=None, *args, **kwargs):
        user = get_object_or_404(self.get_queryset(), pk=pk)
        serializer = UserDetailSerializer(user, context={"request": request})
        return Response(serializer.data)


class AuthorStatsSerializer(serializers.ModelSerializer):
    id = HashidSerializerCharField(source_field="lim.Author.id", read_only=True)
    user = serializers.SlugRelatedField(read_only=True, slug_field="username")

    class Meta:
        model = Author
        fields = (
            "user",
            "submitted",
            "backlog",
            "approved_count",
            "new_count",
            "revised_count",
            "tentative_count",
            "confirming_count",
            "untended_count",
        )


class AuthorStatsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorStatsSerializer
    pagination_class = pagination.PageNumberPagination


class ShowcaseSerializer(serializers.HyperlinkedModelSerializer):
    limerick = serializers.HyperlinkedRelatedField(
        view_name="limerick-detail",
        queryset=Limerick.objects.all(),
        style={"base_template": "input.html"},
    )
    href = serializers.HyperlinkedIdentityField(view_name="showcase-detail")
    user = serializers.HyperlinkedRelatedField(
        view_name="user-detail",
        style={"base_template": "input.html"},
        required=False,
        read_only=True,
    )

    class Meta:
        model = Showcase
        fields = ("pk", "href", "user", "limerick")


class ShowcaseViewSet(viewsets.ModelViewSet):
    queryset = Showcase.objects.all()
    serializer_class = ShowcaseSerializer
    pagination_class = pagination.PageNumberPagination

    def perform_update(self, serializer):
        if "order" in self.request.data:
            order = int(self.request.data["order"][0])
            showcase = self.get_object()
            showcase.to(order)

    def perform_create(self, serializer):
        limerick = get_object_from_hyperlink(self.request.data["limerick"])
        if self.request.user != limerick.primary_author:
            raise PermissionDenied(
                "You may not add another author's limerick to your showcase"
            )
        serializer.validated_data["user"] = limerick.primary_author
        showcase = serializer.save()
        showcase.bottom()

    def perform_delete(self):
        showcase = self.get_object()
        if showcase.user != self.request.user:
            return PermissionDenied(
                "You may not remove another author's limerick from your showcase"
            )

        showcase.delete()


class TextMacroSerializer(serializers.ModelSerializer):
    id = HashidSerializerCharField(source_field="lim.TextMacro.id", read_only=True)

    class Meta:
        model = TextMacro
        fields = "__all__"


class TextMacroViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = TextMacro.objects.all()
    serializer_class = TextMacroSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ("title", "body")
    pagination_class = pagination.PageNumberPagination


class RevisionSerializer(serializers.ModelSerializer):
    id = HashidSerializerCharField(source_field="lim.Revision.id", read_only=True)

    primary_author = serializers.HyperlinkedRelatedField(
        read_only=True, view_name="user-detail"
    )
    secondary_author = serializers.HyperlinkedRelatedField(
        read_only=True, view_name="user-detail"
    )

    class Meta:
        model = Revision
        fields = [
            "pk",
            "verse",
            "author_notes",
            "href",
            "primary_author",
            "secondary_author",
        ]


class RevisionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Revision.objects.all()
    serializer_class = RevisionSerializer
    pagination_class = pagination.PageNumberPagination
    permission_required = "lim.view_revision"


class LimerickSerializer(HyperlinkedModelSerializer):

    id = HashidSerializerCharField(source_field="lim.Limerick.id", read_only=True)

    revision_set = serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name="revision-detail"
    )
    primary_author = serializers.HyperlinkedRelatedField(
        read_only=True, view_name="user-detail"
    )
    secondary_author = serializers.HyperlinkedRelatedField(
        read_only=True, view_name="user-detail"
    )
    tags = serializers.HyperlinkedRelatedField(
        read_only=True, view_name="tag-detail", many=True
    )

    class Meta:
        model = Limerick
        fields = [
            "id",
            "href",
            "last_workshop_date",
            "last_revision_date",
            "created_date",
            "state_date",
            "state",
            "formatted_defined_words",
            "revision_set",
            "primary_author",
            "secondary_author",
            "curtained",
            "topics",
            "tags",
        ]

    def get_tags(self):
        return Tag.objects.filter(
            author=self.context["request"].user, limerick=self.instance
        )


class LimerickViewSet(viewsets.ModelViewSet):
    serializer_class = LimerickSerializer
    pagination_class = pagination.PageNumberPagination
    permission_required = "lim.view_limerick"

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Limerick.objects.filter(
                Q(state=LimStates.APPROVED) & Q(curtained=False)
            ).order_by("pk")
        elif self.request.user.author.curtain_filtering:
            return Limerick.objects.filter(curtained=False).order_by("pk")
        else:
            return Limerick.objects.all().order_by("pk")

    def get_object(self):
        obj = super(LimerickViewSet, self).get_object()
        if (
            self.request.method in permissions.SAFE_METHODS
            or self.request.user.has_perm("lim.manage_topics", obj)
        ):
            return obj
        else:
            raise PermissionDenied

    def get_tag_objects(self, tag_urls):
        tags = []
        for url in tag_urls:
            try:
                tag = get_object_from_hyperlink(url)
                tags.append(tag)
            except Tag.DoesNotExist as e:
                raise ValidationError(f"'{url}' is not a valid Tag API endpoint", e)
        return tags

    @action(detail=True, methods=["patch"])
    def remove_tags(self, request, *args, **kwargs):
        limerick = self.get_object()
        serializer = LimerickSerializer(data=request.data, context={"request": request})
        if serializer.is_valid():
            tags = self.get_tag_objects(serializer.initial_data["tags"])
            limerick.tags.remove(*tags)
            return Response({"status": f"{len(tags)} tags removed from limerick"})
        else:
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=["patch"])
    def add_tags(self, request, *args, **kwargs):
        limerick = self.get_object()
        serializer = LimerickSerializer(data=request.data, context={"request": request})
        if serializer.is_valid():
            tags = self.get_tag_objects(serializer.initial_data["tags"])
            limerick.tags.add(*tags)
            return Response({"status": f"{len(tags)} tags added to limerick"})
        else:
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


class HyperlinkedRelatedLimerickField(serializers.HyperlinkedRelatedField):
    def get_queryset(self):
        instance = self.get_object()
        if hasattr(instance, "limericks"):
            return self.get_object().getattr("limericks").all()
        elif hasattr(instance, "limerick_set"):
            return self.get_object().getattr("limerick_set").all()
        else:
            return QuerySet()


class TopicSerializer(serializers.ModelSerializer):

    id = HashidSerializerCharField(source_field="lim.Topic.id", read_only=True)
    text = serializers.SerializerMethodField()
    parent_name = serializers.SerializerMethodField()
    disabled = serializers.SerializerMethodField()

    class Meta:
        model = Topic
        fields = [
            "id",
            "name",
            "description",
            "text",
            "parent_name",
            "disabled",
            "href",
        ]
        nested_proxy_field = True

    def get_text(self, topic):
        # Being for the benefit of select2
        return topic.name

    def get_parent_name(self, topic):
        if topic.pk == 1 or topic.get_parent().pk == 1:
            return None
        else:
            return topic.get_parent().name

    def get_disabled(self, topic):
        return topic.pk == 1


class TopicDetailSerializer(TopicSerializer):

    id = HashidSerializerCharField(source_field="lim.Topic.id", read_only=True)
    limerick_set = HyperlinkedRelatedLimerickField(
        many=True, view_name="limerick-detail"
    )

    class Meta:
        model = Topic
        fields = [
            "id",
            "name",
            "description",
            "text",
            "parent_name",
            "disabled",
            "href",
            "limerick_set",
        ]


class TopicViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
    filter_backends = (Select2SearchFilter,)
    search_fields = ("name",)
    pagination_class = Select2Pagination

    def retrieve(self, request, pk=None, *args, **kwargs):
        topic = get_object_or_404(self.queryset, pk=pk)
        serializer = TopicDetailSerializer(topic, context={"request": request})
        return Response(serializer.data)


class TagSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        super(TagSerializer, self).__init__(*args, **kwargs)
        self.limerick_objects = []
        self.remove_limerick_objects = []

    id = HashidSerializerCharField(source_field="lim.Tag.id", read_only=True)
    author = serializers.HyperlinkedRelatedField(
        view_name="user-detail", read_only=True
    )
    limericks = serializers.HyperlinkedRelatedField(
        view_name="limerick-detail",
        many=True,
        queryset=Limerick.objects.all(),
        style={"base_template": "input.html"},
    )
    text = serializers.CharField(required=False)

    class Meta:
        model = Tag
        fields = ["id", "text", "author", "limericks", "href"]

    def get_limerick_objects(self, limerick_urls):
        limerick_objects = []
        for url in limerick_urls:
            try:
                limerick = get_object_from_hyperlink(url)
                limerick_objects.append(limerick)
            except Limerick.DoesNotExist as e:
                raise ValidationError(
                    f"'{url}' is not a valid Limerick API endpoint", e
                )
        return limerick_objects

    def validate_limericks(self, _):
        limericks = self.initial_data["limericks"]
        self.limerick_objects = self.get_limerick_objects(limericks)
        return limericks

    def update(self, instance, validated_data):
        if "limericks" in validated_data:
            if self.partial:
                current_limericks = set(instance.limericks.all())
            else:
                current_limericks = set([])

            current_limericks.update(self.limerick_objects)
            instance.limericks.set(current_limericks)

        if "text" in validated_data:
            instance.text = validated_data["text"]
        return instance

    def create(self, validated_data):
        tag = self.__class__.Meta.model.objects.create(
            text=validated_data["text"], author=self.context["request"].user
        )
        tag.limericks.set(self.limerick_objects)
        return tag


class TagViewSet(viewsets.ModelViewSet):
    serializer_class = TagSerializer
    pagination_class = pagination.PageNumberPagination
    permission_required = "lim.view_tag"
    filter_backends = (Select2TermFilter,)
    search_fields = ("text",)

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Tag.objects.all().order_by("pk")
        if self.request.user.is_authenticated:
            return self.request.user.tag_set.all().order_by("pk")
        else:
            raise PermissionDenied

    @action(detail=True, methods=["patch"])
    def remove_limericks(self, request, *args, **kwargs):
        tag = self.get_object()
        serializer = TagSerializer(data=request.data, context={"request": request})
        if serializer.is_valid():
            limericks_to_remove = serializer.limerick_objects
            tag.limericks.remove(*limericks_to_remove)
            return Response(
                {"status": f"{len(limericks_to_remove)} limericks removed from tag"}
            )
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=["patch"])
    def add_limericks(self, request, *args, **kwargs):
        tag = self.get_object()
        serializer = TagSerializer(data=request.data, context={"request": request})
        if serializer.is_valid():
            limericks_to_add = serializer.limerick_objects
            tag.limericks.add(*limericks_to_add)
            return Response(
                {"status": f"{len(limericks_to_add)} limericks added to tag"}
            )
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CFASerializer(serializers.HyperlinkedModelSerializer):

    cfaresponse_set = serializers.HyperlinkedRelatedField(
        view_name="cfaresponse-detail", many=True, queryset=CFAResponse.objects.all()
    )

    class Meta:
        model = CFA
        fields = ("href", "active", "creator", "limerick", "cfaresponse_set")


class CFAViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = CFA.objects.all().order_by("pk")
    serializer_class = CFASerializer
    pagination_class = pagination.PageNumberPagination
    permission_required = "lim.is_workshopping_editor"


class CFAResponseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CFAResponse
        fields = "__all__"


class CFAResponseViewSet(viewsets.ModelViewSet):
    queryset = CFAResponse.objects.all().order_by("pk")
    serializer_class = CFAResponseSerializer
    pagination_class = pagination.PageNumberPagination
    permission_required = "lim.is_workshopping_editor"


# Routers provide an easy way of automatically determining the URL conf.
# router = routers.DefaultRouter()
router = BulkRouter()
router.register("activity-message", ActivityMessageViewSet)
router.register("author-stats", AuthorStatsViewSet)
router.register("limericks", LimerickViewSet, basename="limerick")
router.register("showcase", ShowcaseViewSet)
router.register("revisions", RevisionViewSet)
router.register("tags", TagViewSet, basename="tag")
router.register("text-macros", TextMacroViewSet)
router.register("topics", TopicViewSet)
router.register("users", UserViewSet, basename="user")
router.register("cfas", CFAViewSet)
router.register("cfa-responses", CFAResponseViewSet)
