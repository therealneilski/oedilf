import re

from behave import given, when, then
from django.shortcuts import reverse

from lim.features.environment import log_in_as_user
from lim.models import AuthorRanks, SiteSettings, Limerick


@given("I am logged in as WE")
def log_in_as_we(context):
    context.we = log_in_as_user(context, AuthorRanks.WE)


@given('the alphabet end is "zz"')
def alphabet_end_is_zz(context):
    site_settings = SiteSettings.get_solo()
    site_settings.alphabet_end = "zz"
    site_settings.save()


@given('I am on the "add-limerick" page')
def visit_add_limerick_page(context):
    context.browser.visit(context.config.server_url + reverse("add-limerick"))


def strip_quotes(text):
    if text[0] == text[-1] and text.startswith(('"', "'")):
        return text[1:-1]
    else:
        return text


@when('I enter {text} into the "{field}" rich field')
def enter_rich_text(context, text, field):
    text = strip_quotes(text)
    script = """$('#{}').trumbowyg('html', '{}');""".format(field, text)
    context.browser.execute_script(script)


@when('I add the limerick\'s defined word to the "{field}" rich field')
def add_defined_word_as_rich_text(context, field):
    dw = context.limerick.definedword_set.first().word_text
    return enter_rich_text(context, f"<b>{dw}</b>", field)


@when('I enter {text} into the "{field}" field')
def enter_text(context, text, field):
    context.browser.fill(field, text)


@given('I click the "{button}" button')
@when('I click the "{button}" button')
def click_button(context, button):
    context.browser.find_by_id(button).first.click()


@when('I am on a workshop page')
def wait_for_workshop_page(context):
    assert context.browser.is_text_present("Workshop:", wait_time=5)


def get_workshopping_limerick(context):
    assert (
        "workshop" in context.browser.url
    ), f'"workshop" not found in {context.browser.url}'
    url_re = re.compile(r"/limericks/(.*?)/workshop/")  # NOSONAR
    matches = url_re.findall(context.browser.url)
    assert matches != []
    pk = matches[0]
    context.limerick = Limerick.objects.get(pk=pk)


@then("the latest limerick version of the workshop limerick is {version}")
def latest_version_of_workshop_is(context, version):
    if not hasattr(context, "limerick"):
        get_workshopping_limerick(context)
    assert context.limerick.latest_revision.version == int(version)


@then("the verse of the workshop limerick reads {verse}")
def verse_of_added_limerick_reads(context, verse):
    if not hasattr(context, "limerick"):
        get_workshopping_limerick(context)
    assert (
        context.limerick.latest_revision.verse.strip()
        == verse.replace("\\n", "\n").strip()
    )


@then("the limerick's {field} reads {text}")
def limerick_field_reads_text(context, field, text):
    field = strip_quotes(field)
    text = strip_quotes(text).replace("\\n", "\n")
    if hasattr(context.limerick.latest_revision, field):
        assert (
            getattr(context.limerick.latest_revision, field) == text
        ), f"{field} contents \"{getattr(context.limerick.latest_revision, field)}\" != \"{text}\""
    else:
        assert False, f"No such field: {field}"
