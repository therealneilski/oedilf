import datetime

from behave import given, when, then
from django.contrib.auth.hashers import make_password
from django.shortcuts import reverse

from lim.models import LimStates, AuthorRanks
from lim.tests import factories


@given('I am on the "add-revision" page of the limerick')
def visit_add_revision_page(context):
    context.browser.visit(
        context.config.server_url
        + reverse("add-revision", kwargs=dict(limerick_id=context.limerick.pk.id))
    )


@given("the limerick status is {state}")
def limerick_status_is_state(context, state):
    context.limerick.state = LimStates[state]
    if context.limerick.state == LimStates.HELD:
        context.limerick.held_by = context.limerick.primary_author.author
    if context.limerick.state == LimStates.APPROVED:
        context.ae = factories.UserFactory(
            password=make_password("password"), author__author_rank=AuthorRanks.AE
        )
        context.limerick.approving_editor = context.ae
        context.limerick.state_date = datetime.datetime.now()
        context.limerick.save()
    context.limerick.save()


@when('I set the following defined words')
def set_defined_words(context):
    context.browser.fill("all_defined_words", context.text)


@when('I wait for the "{urlname}" page to be loaded')
def wait_for_workshop_page(context, urlname):
    assert context.browser.is_element_not_present_by_id("id_all_defined_words")


@then("the latest limerick version is {n}")
def latest_limerick_version_is_n(context, n):
    assert context.limerick.latest_revision.version == int(n)


@given('I see the text "{text}"')
@then('I see the text "{text}"')
def i_see_text(context, text):
    assert context.browser.is_text_present(text)


@then('the latest comment reads "{text}"')
def latest_comment_reads(context, text):
    assert (
        context.limerick.latest_revision.comment_set.first().message == text
    ), f"Comment \"{context.limerick.latest_revision.comment_set.first().message}\" != \"{text}\""


@then("the limerick's formatted defined words are \"{formatted_defined_words}\"")
def formatted_defined_words_are(context, formatted_defined_words):
    assert (
        context.limerick.latest_revision.formatted_defined_words
        == formatted_defined_words
    ), (
        f"Defined words \"{context.limerick.latest_revision.formatted_defined_words}\""
        f" != \"{formatted_defined_words}\""
    )
