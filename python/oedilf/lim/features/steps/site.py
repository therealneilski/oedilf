from behave import given, when, then
from django.shortcuts import reverse


@given("I am not logged in")
def not_logged_in(context):
    context.browser.visit(context.config.server_url + reverse("account_logout"))


@when("I visit the home page")
def visit_home_page(context):
    context.browser.visit(context.config.server_url)


@then('I see the expected text')
def see_text(context):
    assert context.text is not None
    assert context.browser.find_by_text(context.text)
