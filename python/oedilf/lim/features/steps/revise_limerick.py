from behave import then


@then("the limerick has an approving editor")
def limerick_has_approving_editor(context):
    assert context.limerick.approving_editor is not None
