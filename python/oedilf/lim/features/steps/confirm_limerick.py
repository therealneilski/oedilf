from behave import given, when, then
from django.db.utils import IntegrityError

from lim.features.environment import wait_for_condition, log_in_as_user
from lim.models import AuthorRanks, LimStates
from lim.tests import factories


@given("I am logged in as AE")
def log_in_as_ae(context):
    context.ae = log_in_as_user(context, AuthorRanks.AE)


@given("someone has written a limerick")
def someone_has_written_limerick(context):
    for _ in range(3):
        try:
            revision = factories.RevisionWithDefinedWordFactory(
                verse=factories.WORKSHOP_VERSE, limerick__state=LimStates.TENTATIVE
            )
            context.limerick = revision.limerick
            break
        except IntegrityError:
            # have tried to create a limerick using a DW that's already been created
            pass


@when('I acknowledge the "stop countdown" modal')
def ack_stop_countdown_modal(context):
    wait_for_condition(
        lambda: context.browser.find_by_id("modal-stop-countdown-button").first.visible
    )
    context.browser.find_by_id("modal-stop-countdown-button").first.click()
    wait_for_condition(
        lambda: not context.browser.is_element_present_by_id(
            "modal-stop-countdown-button"
        )
    )


@then("the limerick is confirming")
def limerick_is_confirming(context):
    assert context.limerick.is_confirming
