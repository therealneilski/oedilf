from behave import given, when, then


@given("I pause for debugging")
@when("I pause for debugging")
@then("I pause for debugging")
def pause_for_debugging(context):
    """
    This is a deliberate no-op function.
    When running in pycharm, you can put a breakpoint on the `pass` statement below
    so that execution will pause.
    :param context:
    :return:
    """
    pass
