from behave import given, when, then
from bs4 import BeautifulSoup
from django.db.utils import IntegrityError
from django.shortcuts import reverse
from str2bool import str2bool

from lim.features.environment import wait_for_condition
from lim.models import CommentActions, Limerick, LimStates
from lim.tests import factories


@given("I have written a limerick")
def have_written_limerick(context):
    for _ in range(3):
        try:
            revision = factories.RevisionWithDefinedWordFactory(
                primary_author=context.we, verse=factories.WORKSHOP_VERSE
            )
            context.limerick = revision.limerick
            break
        except IntegrityError:
            # have tried to create a limerick using a DW that's already been created
            pass


@given('I am on the "workshop" page of the limerick')
def visit_add_limerick_page(context):
    context.browser.visit(
        context.config.server_url
        + reverse("workshop", kwargs=dict(limerick_id=context.limerick.pk))
    )


@given('the limerick has {rfas} RFAs')
def limerick_has_n_rfas(context, rfas):
    for _ in range(int(rfas)):
        rfa = factories.RFAFactory(limerick=context.limerick)
        rfa.save()


@given("the limerick's self-RFA is {self_rfa}")
def limerick_has_self_rfa(context, self_rfa):
    self_rfa = str2bool(self_rfa)
    if self_rfa:
        rfa = factories.RFAFactory(
            author=context.limerick.primary_author, limerick=context.limerick
        )
        rfa.save()


@given("the limerick has been held")
def limerick_has_been_held(context):
    context.limerick.state = LimStates.HELD
    context.limerick.held_by = context.we.author
    context.limerick.save()


@given("the limerick is curtained")
def limerick_is_curtained(context):
    context.limerick.curtained = True
    context.limerick.save()


@when("I enter {type_comment} in the comment form")
def enter_comment_in_form(context, type_comment):
    assert context.browser.is_element_present_by_id("id_message")
    context.browser.execute_script(
        f"$('#id_message').trumbowyg('html', '{type_comment}');"
    )
    context.type_comment = type_comment


@when("I click the button with label {comment_action_label}")
def select_comment_option(context, comment_action_label):
    try:
        action = CommentActions.by_label(comment_action_label)
        context.browser.execute_script(
            "$('#action-{}').click()".format(action.name.lower())
        )
        context.comment_action = action
    except KeyError:
        context.comment_action = CommentActions.NONE


@when('I click "Add comment"')
def i_click_add_comment(context):
    context.browser.find_by_id("comment-submit").first.click()


@when("I wait for a new comment to be loaded")
def i_wait_for_a_new_comment_to_be_loaded(context):
    assert wait_for_condition(
        lambda: Limerick.objects.get(
            pk=context.limerick.pk
        ).latest_revision.comment_set.count()
        > 0
    )
    comment = Limerick.objects.get(
        pk=context.limerick.pk
    ).latest_revision.comment_set.first()
    assert comment is not None
    assert context.browser.is_element_present_by_id(
        f"div_comment_{comment.pk}", wait_time=2
    )


@when("I acknowledge any modals")
def ack_modals(context):
    if context.comment_action == CommentActions.CLEAR_RFAS:
        wait_for_condition(
            lambda: context.browser.find_by_id("clear-rfas-modal-button").first.visible
        )
        context.browser.find_by_id("clear-rfas-modal-button").first.click()
        wait_for_condition(
            lambda: not context.browser.is_element_present_by_id(
                "clear_rfas-modal-button"
            )
        )


@then('I see the first line of the workshop limerick')
def see_first_line_of_limerick(context):
    verse = BeautifulSoup(factories.WORKSHOP_VERSE)
    assert context.browser.is_text_present(
        verse.get_text().strip().split("\n")[0], wait_time=2
    )


@then("I see the comment text in the comment")
def i_see_comment_text_in_comment(context):
    assert context.limerick.latest_revision.comment_set.count() >= 1
    comment = context.limerick.latest_revision.comment_set.filter(
        author=context.limerick.primary_author
    ).first()
    assert context.type_comment == comment.message
    assert context.comment_action == comment.action
    assert context.browser.is_element_present_by_id(
        f"div_comment_{comment.pk}", wait_time=2
    )
    assert (
        context.type_comment
        in context.browser.find_by_id(f"div_comment_{comment.pk}").text
    )


@then("the limerick status is {state}")
def limerick_status_is_state(context, state):
    context.limerick.refresh_from_db()
    state = LimStates[state]
    lim_state = context.limerick.state
    assert state == lim_state


@then("the limerick curtaining is {is_curtained}")
def limerick_curtaining_is(context, is_curtained):
    context.limerick.refresh_from_db()
    assert context.limerick.curtained == str2bool(is_curtained)


@then("the limerick's self-RFA is {end_self_rfa}")
def limerick_has_self_rfa(context, end_self_rfa):
    end_self_rfa = str2bool(end_self_rfa)
    context.limerick.refresh_from_db()
    assert context.limerick.is_eligible_for_stc == end_self_rfa


@then("the limerick has {end_rfas} RFAs")
def limerick_has_n_rfas(context, end_rfas):
    assert int(end_rfas) == context.limerick.rfa_set.count()
