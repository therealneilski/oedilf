Feature: Workshop
  # Workshopping

  Background:
    Given I am logged in as WE
    And I have written a limerick

  Scenario: Workshop page is displayed
    Given I am on the "workshop" page of the limerick
    Then I see the first line of the workshop limerick


  Scenario Outline: Set limerick to various states
    Given I am on the "workshop" page of the limerick
    When I enter <type_comment> in the comment form
    And I click the button with label <comment_action_label>
    And I wait for a new comment to be loaded
    Then I see the comment text in the comment
    And the limerick status is <state>
    And the limerick curtaining is <is_curtained>

    Examples:
    | type_comment    | comment_action_label | state | is_curtained |
    | Hello again     | Just comment         | NEW   | False        |
    | Putting on hold | Hold                 | HELD  | False        |
    | Curtaining      | Add curtain          | NEW   | True         |

  Scenario Outline: Self-RFA
    Given the limerick has <rfas> RFAs
    And the limerick's self-RFA is <self_rfa>
    And I am on the "workshop" page of the limerick
    When I enter <type_comment> in the comment form
    And I click the button with label <comment_action_label>
    And I acknowledge any modals
    And I wait for a new comment to be loaded
    Then I see the comment text in the comment
    And the limerick status is <state>
    And the limerick's self-RFA is <end_self_rfa>
    And the limerick has <end_rfas> RFAs

    Examples:
    | rfas | self_rfa | type_comment    | comment_action_label | state | end_rfas | end_self_rfa |
    | 4    | False    | Add my RFA      | Add RFA              | NEW   | 5        | True         |
    | 4    | True     | Remove my RFA   | Remove RFA           | NEW   | 4        | False        |
    | 4    | True     | Clearing RFAs   | Clear ALL RFAs       | NEW   | 0        | False        |

  Scenario Outline: Unholding a limerick
    Given the limerick has been held
    And I am on the "workshop" page of the limerick
    When I enter <type_comment> in the comment form
    And I click the button with label <comment_action_label>
    And I wait for a new comment to be loaded
    Then I see the comment text in the comment
    And the limerick status is <state>
    And the limerick curtaining is <is_curtained>

    Examples:
    | type_comment    | comment_action_label | state      | is_curtained |
    | Taking off hold | Unhold               | TENTATIVE  | False        |

  Scenario Outline: Removing the curtain
    Given the limerick is curtained
    And I am on the "workshop" page of the limerick
    When I enter <type_comment> in the comment form
    And I click the button with label <comment_action_label>
    And I wait for a new comment to be loaded
    Then I see the comment text in the comment
    And the limerick status is <state>
    And the limerick curtaining is <is_curtained>

    Examples:
    | type_comment     | comment_action_label | state      | is_curtained |
    | Removing curtain | Remove curtain       | NEW        | False        |

