import os
import shutil
import time

from allauth.account.models import EmailAddress
from django.contrib.auth.hashers import make_password, check_password
from django.shortcuts import reverse
from splinter.browser import Browser

from lim.models import LimStates
from lim.tests import factories

SCREENSHOT_DIR = "/Users/neilp/src/oedilf/oedilf/python/oedilf/behave-screenshots"


def before_all(context):
    if os.path.exists(SCREENSHOT_DIR):
        shutil.rmtree(SCREENSHOT_DIR)
    if context.config.browser:
        context.browser = Browser(context.config.browser)
    else:
        context.browser = Browser('chrome')
    rev = factories.RevisionWithDefinedWordFactory(
        limerick__state=LimStates.APPROVED,
        limerick__original_id=100,
        limerick__limerick_number=100,
    )
    context.approved_limerick = rev.limerick


def after_all(context):
    context.browser.quit()
    context.browser = None


def wait_for_condition(f, wait_time=2):
    end_time = time.time() + wait_time

    while time.time() < end_time:
        if f():
            return True

    return False


def after_step(context, step):
    if step.status.name == "failed" and hasattr(context, "browser"):
        d = os.path.join(SCREENSHOT_DIR, os.path.splitext(step.filename)[0])
        os.makedirs(d, exist_ok=True)
        context.browser.screenshot(os.path.join(d, f"{step.line}.png"), full=True)


def log_in_as_user(context, author_rank):
    user = factories.UserFactory(
        password=make_password("password"), author__author_rank=author_rank
    )
    user.refresh_from_db()
    EmailAddress.objects.create(user=user, email=user.email, verified=True)
    assert check_password("password", user.password)
    context.browser.visit(context.config.server_url + reverse("account_login"))
    assert context.browser.is_text_present("Login")
    context.browser.fill("login", user.username)
    context.browser.fill("password", "password")
    context.browser.find_by_id("login-native").first.click()
    assert context.browser.is_text_present("Activity for " + user.username, wait_time=2)
    return user


def after_scenario(context, scenario):
    context.browser.visit(context.config.server_url + reverse("account_logout"))
    if context.browser.is_text_present("Are you sure you want to sign out?"):
        context.browser.find_by_id("btn-sign-out").first.click()
        assert context.browser.is_text_present("Register", wait_time=2)
