Feature: Limerick confirmation
  # Confirming limericks, approving limericks, and cancelling countdowns

  Background:
    Given I am logged in as AE
    And someone has written a limerick
#    And the limerick is eligible for STC
    And the limerick has 4 RFAs
    And the limerick's self-RFA is True
    And I am on the "workshop" page of the limerick

  Scenario: Check limerick is STC
    Then I see the text "This limerick is eligible to be Set To Confirming."

  Scenario Outline: Set limerick to confirming
    When I click the "<button>" button
    Then I see the text "Confirmation delay ends"
    And the limerick status is <state>
    And the limerick is confirming

    Examples:
    | button                   | state      |
    | button-set-to-confirming | CONFIRMING |

  Scenario Outline: Stop countdown
    Given I click the "<first_button>" button
    And I see the text "Confirmation delay ends"
    When I click the "<button>" button
    And I acknowledge the "stop countdown" modal
    Then I see the text "Countdown stopped"
    And the limerick status is <state>

  Examples:
    | first_button             | button                  | state     |
    | button-set-to-confirming | button-stop-countdown   | TENTATIVE |
