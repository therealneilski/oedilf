Feature: Add limerick

  Background:
    Given I am logged in as WE
    And the alphabet end is "zz"
    And I am on the "add-limerick" page

  Scenario: Add limerick page is displayed
    Then I see the text "Add limerick"

  Scenario Outline: Add a limerick
    When I enter <verse_with_div> into the "id_verse" rich field
    And I enter <defined_word> into the "all_defined_words" field
    And I click the "limerick-submit" button
    And I am on a workshop page
    Then the latest limerick version of the workshop limerick is 0
    And the verse of the workshop limerick reads <verse>

  Examples: Test
    |verse_with_div|defined_word|verse|
    |<div><b>Test</b> 1</div><div><b>Test</b> 2</div><div><b>Test</b> 3</div><div><b>Test</b> 4</div><div><b>Test</b> 5</div>|test|<b>Test</b> 1\n<b>Test</b> 2\n<b>Test</b> 3\n<b>Test</b> 4\n<b>Test</b> 5|
