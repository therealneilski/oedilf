Feature: Site
  Basic operation of the site.

Scenario: Home page
  Given I am not logged in
  When I visit the home page
  Then I see the expected text
    """
    The Omnificent English Dictionary In Limerick Form
    """
