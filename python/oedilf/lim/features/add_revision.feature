Feature: Add revision

  Background:
    Given I am logged in as WE
    And the alphabet end is "zz"
    And I have written a limerick
    And I am on the "add-revision" page of the limerick

  Scenario: Sanity check
    Then the latest limerick version is 0

  Scenario: Add revision page is displayed
    Then I see the first line of the workshop limerick
    And I see the text "All defined words"

  Scenario: Add a comment with no changes
    When I enter "no change" into the "id_workshop_comment" rich field
    And I set the following defined words
      """
      workshop
      test
      """
    And I click the "revision-submit" button
    And I wait for the "workshop" page to be loaded
    Then the latest limerick version is 1
    And the latest comment reads "no change"

  Scenario: Add an author note
    When I enter "This is the note" into the "id_author_notes" rich field
    And I enter "AN" into the "id_workshop_comment" rich field
    And I set the following defined words
      """
      workshop
      test
      """
    And I click the "revision-submit" button
    And I wait for the "workshop" page to be loaded
    Then the latest limerick version is 1
    And the latest comment reads "AN"
    And the limerick's "author_notes" reads "This is the note"

  Scenario: Change the limerick
    When I enter "<div><b>Test</b> 1</div><div><b>Test</b> 2</div><div><b>Test</b> 3</div><div><b>Test</b> 4</div><div><b>Test</b> 5</div>" into the "id_verse" rich field
    And I enter "Verse" into the "id_workshop_comment" rich field
    And I set the following defined words
      """
      test
      workshop
      """
    And I click the "revision-submit" button
    And I wait for the "workshop" page to be loaded
    Then the latest limerick version is 1
    And the latest comment reads "Verse"
    And the limerick's "verse" reads "<b>Test</b> 1\n<b>Test</b> 2\n<b>Test</b> 3\n<b>Test</b> 4\n<b>Test</b> 5"

  Scenario Outline: Revise limerick in various states
    Given the limerick status is <initial_state>
    When I enter "This is the note" into the "id_author_notes" rich field
    And I enter "AN" into the "id_workshop_comment" rich field
    And I set the following defined words
      """
      workshop
      test
      """
    And I click the "revision-submit" button
    And I wait for the "workshop" page to be loaded
    Then the latest limerick version is 1
    And the latest comment reads "AN"
    And the limerick's "author_notes" reads "This is the note"
    And the limerick status is <state>

    Examples:
    | initial_state | state      |
    | NEW           | REVISED    |
    | REVISED       | REVISED    |
    | TENTATIVE     | REVISED    |
    | HELD          | HELD       |

  Scenario: Preserve ordering of DWs
    When I enter "testing DWs order" into the "id_workshop_comment" rich field
    And I set the following defined words
      """
      workshop
      test
      """
    And I click the "revision-submit" button
    And I wait for the "workshop" page to be loaded
    Then the latest limerick version is 1
    And the limerick's formatted defined words are "workshop, test"

