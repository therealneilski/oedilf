# https://trello.com/c/yxbHiOxR

Feature: Revising my limerick does not affect RFAs

  Background:
    Given I am logged in as WE
    And I have written a limerick

  Scenario: Add a revision to a tentative limerick does not clear RFAs
    Given the limerick has 4 RFAs
    And the limerick's self-RFA is False
    And the limerick status is TENTATIVE
    And I am on the "add-revision" page of the limerick
    And I pause for debugging
    When I enter "no change" into the "id_workshop_comment" rich field
    And I add the limerick's defined word to the "id_author_notes" rich field
    And I click the "revision-submit" button
    And I wait for the "workshop" page to be loaded
    And I pause for debugging
    Then the limerick has 4 RFAs
