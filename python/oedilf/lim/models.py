import datetime
import html
import re

from django.contrib.auth.models import User
from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVectorField
from django.db.models import *
from django.shortcuts import reverse
from django.utils.functional import cached_property
from django.utils.html import format_html_join, format_html
from django.utils.timezone import now
from django_fsm import FSMIntegerField, transition
from enumfields import IntEnum, EnumIntegerField
from hashid_field import HashidAutoField
from ordered_model.models import OrderedModel
from solo.models import SingletonModel
from templated_email import get_templated_mail
from treebeard.mp_tree import MP_Node


class AuthorTypes(IntEnum):
    APPLICANT = 0
    MEMBER = 1
    WORKSHOPPER = 2
    EDITOR = 3
    ADMINISTRATOR = 4
    BANNED = 5
    SENIOR_WORKSHOPPER = 6


class AuthorRanks(IntEnum):
    C = 0
    CE = 1
    WE = 2
    AE = 3
    EIC = 4
    R = 5
    W50 = 6
    W100 = 7
    W200 = 8
    W300 = 9
    W400 = 10
    W500 = 11
    W750 = 12
    W1000 = 13

    class Labels:
        C = "Contributor"
        CE = "Contributing Editor"
        WE = "Workshopping Editor"
        AE = "Associate Editor"
        EIC = "Editor-in-Chief"
        R = "Rookie"
        W50 = "Senior Limericist"
        W100 = "Centurion"
        W200 = "Double Centurion"
        W300 = "MeterMaestro"
        W500 = "RhymeMaster"
        W750 = "Senior RhymeMaster"
        W1000 = "Anapestarian Supreme"

    def verbose_name(self):
        return self.label

    def position(self):
        return [
            self.R,
            self.C,
            self.CE,
            self.WE,
            self.W50,
            self.W100,
            self.W200,
            self.W300,
            self.W400,
            self.W500,
            self.W750,
            self.W1000,
            self.AE,
            self.EIC,
        ].index(self)

    @classmethod
    def get_from_approved_count(cls, approved_count) -> "AuthorRanks":
        if approved_count < 10:
            return cls["R"]
        elif approved_count < 20:
            return cls["CE"]
        elif approved_count < 50:
            return cls["WE"]
        elif approved_count < 100:
            return cls["W50"]
        elif approved_count < 200:
            return cls["W100"]
        elif approved_count < 300:
            return cls["W200"]
        elif approved_count < 500:
            return cls["W300"]
        elif approved_count < 750:
            return cls["W500"]
        elif approved_count < 1000:
            return cls["W750"]
        else:
            return cls["W1000"]


class MonitorComments(IntEnum):
    OFF = 0
    WORKSHOPPED = 1
    ALL = 2

    class Labels:
        OFF = "Only on my limericks"
        WORKSHOPPED = "On limericks I've workshopped"
        ALL = "All"


class MonitorNew(IntEnum):
    OFF = 0
    HELD = 1
    ALL = 2


class MonitorRFAs(IntEnum):
    OFF = 0
    ALL = 1
    REVISION = 2
    STATE = 3

    class Labels:
        OFF = "No special post-RFA notifications"
        ALL = "Notify me on any activity"
        REVISION = "Notify me when the limerick is revised"
        STATE = "Notify me when my RFA is cleared or the author self-RFAs"


class PredefinedFeedbackStyles(IntEnum):
    NO_PREF = 0
    ABSENT = 1
    NO_HELP = 2
    NO_FIXES = 3
    HINTS_ONLY = 4
    PARTIAL_REWRITES = 5
    ANYTHING_GOES = 6
    CUSTOM = 7

    class Labels:
        NO_PREF = "No preference yet. Let's see how we go."
        ABSENT = (
            "Feel free to devote your WSing efforts elsewhere since I will be unable to respond "
            "to your comments in the near future."
        )
        NO_HELP = "Just tell me what's wrong. I'll ask for help whenever I need it."
        NO_FIXES = (
            "Tell me what's wrong. Explain how to fix it, but don't provide fixes."
        )
        HINTS_ONLY = (
            "Point out what needs to improve and offer hints -- a word here or there."
        )
        PARTIAL_REWRITES = "Offer me criticisms, suggestions, or even partial rewrites."
        ANYTHING_GOES = "I'd really appreciate any guidance, explanations, edits or rewrites you'd care to give."
        CUSTOM = "Something else ..."


class Author(Model):
    id = HashidAutoField(primary_key=True)
    user = OneToOneField(User, on_delete=CASCADE)
    author_type = EnumIntegerField(AuthorTypes, default=AuthorTypes.APPLICANT)
    author_rank = EnumIntegerField(AuthorRanks, default=AuthorRanks.R)
    real_name = CharField(max_length=50)
    biography = TextField(blank=True)
    notify_changes = BooleanField(
        default=True, verbose_name="Monitor changes and comments on my limericks"
    )
    monitor_new = EnumIntegerField(
        MonitorNew,
        default=MonitorNew.OFF,
        verbose_name="Notify me when new limericks are posted",
    )
    monitor_comments = EnumIntegerField(MonitorComments, default=MonitorComments.OFF)
    monitor_attention = BooleanField(
        default=False, verbose_name="Monitor Calls for Attention"
    )
    use_email = BooleanField(default=True)
    feedback_style_standard = EnumIntegerField(
        PredefinedFeedbackStyles,
        default=PredefinedFeedbackStyles.CUSTOM,
        verbose_name="Feedback style",
    )
    feedback_style = CharField(max_length=1500, blank=True)
    time_zone = SmallIntegerField(default=0)
    monitor_rfas = EnumIntegerField(
        MonitorRFAs, default=MonitorRFAs.ALL, verbose_name="Monitor RFAs"
    )
    junior_mode = BooleanField(default=False)
    curtain_filtering = BooleanField(
        default=False, verbose_name="Hide curtained limericks"
    )
    text_macro_buttons = BooleanField(
        default=True, verbose_name="Show the text macro buttons when workshopping"
    )
    ws_comment_display_limit = PositiveSmallIntegerField(
        default=50, verbose_name="Display at most this many comments on a revision"
    )
    # following is the lat/lng of Limerick
    lat = FloatField(default=52.668_020_4, verbose_name="Latitude", blank=True)
    lng = FloatField(
        default=-8.630_497_600_000_012, verbose_name="Longitude", blank=True
    )
    born_in = CharField(max_length=2, blank=True)
    living_in = CharField(max_length=2, blank=True)
    approval_emails = BooleanField(
        default=True, verbose_name="Email me when a limerick needs my approval"
    )
    milestone_emails = BooleanField(
        default=True, verbose_name="Email me when I gain access to new site features"
    )
    news_emails = BooleanField(
        default=True, verbose_name="Email occasional newsletters and programs"
    )
    twitter_handle = CharField(max_length=15, blank=True)
    held_count = DecimalField(
        default=0.0, decimal_places=1, max_digits=7, verbose_name="Held"
    )
    new_count = DecimalField(
        default=0.0, decimal_places=1, max_digits=7, verbose_name="New"
    )
    revised_count = DecimalField(
        default=0.0, decimal_places=1, max_digits=7, verbose_name="Revised"
    )
    tentative_count = DecimalField(
        default=0.0, decimal_places=1, max_digits=7, verbose_name="Tentative"
    )
    confirming_count = DecimalField(
        default=0.0, decimal_places=1, max_digits=7, verbose_name="Confirming"
    )
    approved_count = DecimalField(
        default=0.0, decimal_places=1, max_digits=7, verbose_name="Approved"
    )
    untended_count = DecimalField(
        default=0.0, decimal_places=1, max_digits=7, verbose_name="Untended"
    )
    stored_count = DecimalField(
        default=0.0, decimal_places=1, max_digits=7, verbose_name="Stored"
    )
    excluded_count = DecimalField(
        default=0.0, decimal_places=1, max_digits=7, verbose_name="Excluded"
    )
    submitted_count = DecimalField(
        default=0.0, decimal_places=1, max_digits=7, verbose_name="Submitted"
    )
    backlog = DecimalField(
        default=0.0, decimal_places=1, max_digits=4, verbose_name="Backlog"
    )

    def update_stats(self):
        self.held_count = self.live_held_count()
        self.new_count = self.live_new_count()
        self.tentative_count = self.live_tentative_count()
        self.revised_count = self.live_revised_count()
        self.confirming_count = self.live_confirming_count()
        self.approved_count = self.live_approved_count()
        self.untended_count = self.live_untended_count()
        self.stored_count = self.live_stored_count()
        self.excluded_count = self.live_excluded_count()
        self.submitted_count = self._calculate_submitted()
        self.backlog = self._calculate_backlog()

    def _calculate_submitted(self):
        return sum(
            (
                self.approved_count,
                self.confirming_count,
                self.tentative_count,
                self.revised_count,
                self.held_count,
                self.new_count,
                self.untended_count,
                self.stored_count,
                self.excluded_count,
            )
        )

    def _calculate_backlog(self):
        try:
            num = float(self.approved_count + self.confirming_count)
            den = float(
                self.approved_count
                + self.confirming_count
                + self.tentative_count
                + self.revised_count
                + self.new_count
            )
            return 1.0 - (num / den)

        except ZeroDivisionError:
            return 0.0

    @property
    def is_active(self):
        return self.user.last_login >= datetime.datetime.now() - datetime.timedelta(
            SiteSettings.get_solo().inactive_days
        )

    @property
    def verbose_feedback_style(self):
        if self.feedback_style_standard == PredefinedFeedbackStyles.CUSTOM:
            return self.feedback_style

        else:
            return self.feedback_style_standard.label

    @cached_property
    def totals(self):
        """
        This could potentially obviate the needs for the *_count members of Author as it is efficiently
        calculated live
        :return:
        """
        qs = (
            Limerick.objects.filter(
                Q(primary_author=self.user) | Q(secondary_author=self.user)
            )
            .annotate(
                score=Case(
                    When(secondary_author=self.user, then=Value(0.5)),
                    When(secondary_author__isnull=False, then=Value(0.5)),
                    When(secondary_author__isnull=True, then=Value(1.0)),
                    default=Value(0),
                    output_field=DecimalField(),
                )
            )
            .annotate(_score=Sum("score"))
            .values("state", "_score")
            .annotate(sum_of_scores=F("_score"))
            .values("state", "sum_of_scores")
            .order_by("state")
        )
        totals = dict(
            (LimStates(x["state"]), float(x["sum_of_scores"])) for x in qs.all()
        )
        return totals

    def _live_state_count(self, state):
        return self.totals.get(state, 0)

    def live_submitted(self):
        return sum(self.totals.values())

    def live_new_count(self):
        return self._live_state_count(LimStates.NEW)

    def live_revised_count(self):
        return self._live_state_count(LimStates.REVISED)

    def live_tentative_count(self):
        return self._live_state_count(LimStates.TENTATIVE)

    def live_approved_count(self):
        return self._live_state_count(LimStates.APPROVED)

    def live_confirming_count(self):
        return self._live_state_count(LimStates.CONFIRMING)

    def live_held_count(self):
        return self._live_state_count(LimStates.HELD)

    def live_stored_count(self):
        return self._live_state_count(LimStates.STORED)

    def live_excluded_count(self):
        return self._live_state_count(LimStates.EXCLUDED)

    def live_untended_count(self):
        return self._live_state_count(LimStates.UNTENDED)

    def showcase_percentage(self):
        approved_count = self.approved_count
        if approved_count >= 20000:
            return 0.2

        elif approved_count >= 15000:
            return 0.19

        elif approved_count >= 10000:
            return 0.18

        elif approved_count >= 6000:
            return 0.17

        elif approved_count >= 3000:
            return 0.16

        elif approved_count >= 1000:
            return 0.15

        elif approved_count >= 500:
            return 0.14

        elif approved_count >= 250:
            return 0.13

        elif approved_count >= 100:
            return 0.12

        elif approved_count >= 50:
            return 0.11

        elif approved_count >= 10:
            return 0.1

        else:
            return 0

    def showcase_quantity(self):
        return int(self.showcase_percentage() * self.live_approved_count())

    def get_absolute_url(self):
        return reverse("author-profile", kwargs={"author": self.pk})

    def as_link(self):
        if self.user.username == "Workshop":
            return "Workshop"
        else:
            return format_html(
                "<a href=\"{}\">{}</a>", self.get_absolute_url(), self.user.username
            )

    def update_rank(self):
        """
        Update the author's rank based on the number of approved limericks
        :return: 
        """
        if self.author_rank in (AuthorRanks.AE, AuthorRanks.EIC):
            return
        new_rank = AuthorRanks.get_from_approved_count(self.approved_count)
        if new_rank != self.author_rank:
            message = "{} to {} ({})".format(
                "Promoted"
                if new_rank.position() > self.author_rank.position()
                else "Demoted",
                new_rank.name,
                new_rank.label,
            )
            messages = []
            for ae in Author.objects.filter(
                author_rank__in=(AuthorRanks.AE, AuthorRanks.EIC)
            ):
                messages.append(
                    ActivityMessage(
                        message_type=ActivityMessageType.PROMOTION,
                        source=None,
                        dest=ae.user,
                        author=self.user,
                        data=message,
                        date_time=datetime.datetime.now(),
                    )
                )
            ActivityMessage.objects.bulk_create(messages)
            self.author_rank = new_rank
            self.save()

    def __str__(self):
        return self.user.username


class WordStatus(IntEnum):
    NOT_DONE = 0
    PARTIAL = 1
    COMPLETE = 2


class DefinedWord(Model):
    status = EnumIntegerField(WordStatus, null=True)
    word_text = CharField(max_length=60, unique=True)
    untended = BooleanField(default=False, null=False)

    def is_untended(self):
        limerick_set_count = self.limerick_set.count()
        return (
            limerick_set_count > 0
            and limerick_set_count
            == self.limerick_set.filter(state=LimStates.UNTENDED).count()
        )

    def __str__(self):
        return self.word_text


class LimStates(IntEnum):
    # enum('tentative','obsolete','approved','held','excluded','stored','revised','confirming','new','untended')
    TENTATIVE = 0
    OBSOLETE = 1
    APPROVED = 2
    HELD = 3
    EXCLUDED = 4
    STORED = 5
    REVISED = 6
    CONFIRMING = 7
    NEW = 8
    UNTENDED = 9

    @classmethod
    def choices(cls):
        for state in (  # type: LimStates
            cls.NEW,
            cls.REVISED,
            cls.TENTATIVE,
            cls.CONFIRMING,
            cls.APPROVED,
            cls.HELD,
            cls.EXCLUDED,
            cls.STORED,
            cls.UNTENDED,
        ):
            yield (state.value, state.name.title())


class WorkshopStates(IntEnum):
    NONE = 0
    STARTED = 1
    ATTENTION = 2
    RFA = 3


class Limerick(Model):
    id = HashidAutoField(primary_key=True, allow_int_lookup=True)
    original_id = IntegerField(db_index=True, null=True)  # legacy
    limerick_number = IntegerField(db_index=True, null=True)  # legacy
    state = FSMIntegerField(
        default=LimStates.TENTATIVE,
        verbose_name="State",
        choices=LimStates.choices(),
        db_index=True,
    )
    curtained = BooleanField(default=False)
    approving_editor = ForeignKey(
        User, on_delete=CASCADE, null=True, related_name="approving_editor"
    )
    held_by = ForeignKey(Author, on_delete=CASCADE, null=True, related_name="held_by")
    link_code = IntegerField(null=True)  # link embedded in LACE - legacy
    primary_author_approved = BooleanField(default=False)
    secondary_author_approved = BooleanField(default=False)

    topics = ManyToManyField("Topic", blank=True)

    eligible_for_stc = BooleanField(default=False)

    # Following fields are denormalised for performance reasons
    primary_author = ForeignKey(
        User, on_delete=CASCADE, null=True, related_name="lim_primary_author"
    )
    secondary_author = ForeignKey(
        User, on_delete=CASCADE, null=True, related_name="lim_secondary_author"
    )
    last_workshop_date = DateTimeField(null=True)
    last_revision_date = DateTimeField(null=True)
    created_date = DateTimeField(null=True)
    state_date = DateTimeField(null=True, blank=True)

    definedword_set = ManyToManyField(
        DefinedWord,
        blank=True,
        related_name="limerick_set",
        through='LimerickDefinedWordsThroughModel',
    )

    @property
    def latest_revision(self):
        return self.revision_set.first()

    @property
    def confirmation_countdown_end_date(self):
        if self.state != LimStates.CONFIRMING:
            return None

        if self.state_date is None:
            return now() + datetime.timedelta(
                days=SiteSettings.get_solo().countdown_days
            )

        return self.state_date + datetime.timedelta(
            days=SiteSettings.get_solo().countdown_days
        )

    @property
    def is_confirming(self):
        return self.state == LimStates.CONFIRMING

    @property
    def confirmation_countdown_ended(self):
        return self._confirmation_countdown_ended()

    def _confirmation_countdown_ended(self):
        if self.is_confirming:
            return self.confirmation_countdown_end_date < now()

        else:
            return False

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("limerick", args=[str(self.pk)])

    @property
    def formatted_defined_words(self):
        revision = self.latest_revision
        if revision is None:
            return ""

        else:
            return revision.formatted_defined_words

    @property
    def all_defined_words(self):
        revision = self.latest_revision
        if revision is None:
            return ""

        else:
            return revision.all_defined_words

    @property
    def is_approved(self):
        return self.state == LimStates.APPROVED

    @property
    def authors(self):
        if self.secondary_author is None:
            return (self.primary_author,)

        else:
            return self.primary_author, self.secondary_author

    @property
    def is_eligible_for_self_rfa(self):
        return (
            self.rfa_set.exclude(
                author_id__in=(author.id for author in self.authors)
            ).count()
            >= 4
        )

    @property
    def is_eligible_for_stc(self):
        author_rfas = self.rfa_set.filter(
            author_id__in=(author.id for author in self.authors)
        ).count()
        return (
            self.is_eligible_for_self_rfa
            and self.state
            not in (
                LimStates.CONFIRMING,
                LimStates.APPROVED,
                LimStates.EXCLUDED,
                LimStates.STORED,
            )
            and (
                (self.secondary_author is None and author_rfas == 1)
                or (self.secondary_author is not None and author_rfas == 2)
            )
        )

    def add_author_approval(self, user):
        if user == self.primary_author:
            self.primary_author_approved = True
        elif user == self.secondary_author:
            self.secondary_author_approved = True

    # Transitions

    @transition(
        field=state,
        source=(
            LimStates.NEW,
            LimStates.REVISED,
            LimStates.TENTATIVE,
            LimStates.UNTENDED,
        ),
        target=LimStates.HELD,
        permission=lambda instance, user: user.has_perm("lim.can_hold", instance),
    )
    def on_hold(self, user):
        self.held_by = user.author
        return True

    # noinspection PyUnusedLocal

    @transition(
        field=state,
        source=LimStates.HELD,
        target=LimStates.TENTATIVE,
        permission=lambda instance, user: user.has_perm("lim.can_unhold", instance),
    )
    def off_hold(self, user):
        self.held_by = None
        return True

    # noinspection PyUnusedLocal

    @transition(
        field=state,
        source=(LimStates.NEW, LimStates.REVISED, LimStates.UNTENDED),
        target=LimStates.TENTATIVE,
        permission=lambda instance, user: user.has_perm("lim.is_workshopping_editor"),
    )
    def set_to_tentative(self, user):
        return True

    # noinspection PyUnusedLocal

    @transition(
        field=state,
        source=(LimStates.NEW, LimStates.TENTATIVE),
        target=LimStates.REVISED,
        permission=lambda instance, user: user.has_perm("lim.is_author", instance),
    )
    def set_to_revised(self, user):
        return True

    @property
    def all_authors_have_approved(self):
        return self._all_authors_have_approved()

    def _all_authors_have_approved(self):
        """
        Note that a limerick with two authors must be approved by both of them before it can become APPROVED.
        """
        if (
            self.secondary_author is None
            or self.secondary_author.username == "Workshop"
        ):
            return self.primary_author_approved

        else:
            return self.primary_author_approved and self.secondary_author_approved

    # noinspection PyUnusedLocal

    @transition(
        field=state,
        source=LimStates.CONFIRMING,
        target=LimStates.APPROVED,
        conditions=[_all_authors_have_approved],
        permission=lambda instance, user: user.has_perm(
            "lim.set_to_approved", instance
        ),
    )
    def set_to_approved(self, user):
        """
        Note that a limerick with two authors must be approved by both of them before it can become APPROVED.
        :param user:
        :return:
        """
        for author in self.authors:
            author.author.update_rank()
        return True

    # noinspection PyUnusedLocal

    @transition(
        field=state,
        source=LimStates.CONFIRMING,
        target=LimStates.APPROVED,
        conditions=[_confirmation_countdown_ended],
        permission=lambda instance, user: user.has_perm(
            "lim.force_final_approval", instance
        ),
    )
    def force_final_approval(self, user):
        return True

    # noinspection PyUnusedLocal

    @transition(
        field=state,
        source=(LimStates.APPROVED, LimStates.STORED, LimStates.EXCLUDED),
        target=LimStates.TENTATIVE,
        permission=lambda instance, user: user.has_perm("lim.is_approving_editor"),
    )
    def unapprove(self, user):
        return True

    # noinspection PyUnusedLocal

    @transition(
        field=state,
        source=(LimStates.NEW, LimStates.REVISED, LimStates.TENTATIVE),
        target=LimStates.CONFIRMING,
        conditions=[lambda instance: instance.is_eligible_for_stc],
        permission=lambda instance, user: user.has_perm("is_approving_editor"),
    )
    def set_to_confirming(self, user):
        return True

    # noinspection PyUnusedLocal

    @transition(
        field=state,
        source=LimStates.APPROVED,
        target=LimStates.STORED,
        permission=lambda instance, user: user.has_perm("lim.is_approving_editor"),
    )
    def store(self, user):
        """
        Put an approved limerick aside because its DW falls outside the current range.
        Also known as the Bottom Drawer.
        :param user:
        :return:
        """
        return True

    # noinspection PyUnusedLocal

    @transition(
        field=state,
        source="*",
        target=LimStates.EXCLUDED,
        permission=lambda _, user: user.has_perm("lim.is_approving_editor"),
    )
    def exclude(self, user):
        """
        Put a limerick aside because it does not meet site standards.
        Also known as the BoneYard.
        :param user:
        :return:
        """
        return True

    def get_lace_mails(self, request):

        recipients = []
        if not self.primary_author_approved:
            recipients.append(self.primary_author)
        if not self.secondary_author_approved:
            recipients.append(self.secondary_author)
        lace_mails = []
        for author in recipients:
            if author is not None and author.username != "Workshop":
                lace_mails.append(
                    get_templated_mail(
                        template_name="lace",
                        from_email="oedilf@oedilf.com",
                        to=[author.email],
                        context={
                            "author": author,
                            "limerick": self,
                            "workshop_uri": request.build_absolute_uri(
                                reverse("workshop", kwargs={"limerick_id": self.id})
                            ),
                        },
                    )
                )
        LACE(limerick=self).save()
        return lace_mails

    @property
    def is_eligible_for_first_lace(self):
        return (
            self.is_confirming
            and self.confirmation_countdown_ended
            and self.lace_set.count() == 0
        )

    @property
    def is_eligible_for_followup_lace(self):
        """
        If a limerick is still not APPROVED after this many days,
        a followup LACE may be sent.
        :return:
        """
        if not self.is_confirming or self.lace_set.count() == 0:
            return False

        d = self.lace_set.latest().date
        return d is None or d < now() - datetime.timedelta(
            days=SiteSettings.get_solo().lace_resend_days
        )

    def __str__(self):
        return "{} ({}): {} by {}".format(
            self.pk,
            LimStates(self.state).label,
            self.formatted_defined_words,
            ", ".join(("(none)" if x is None else x.username) for x in self.authors),
        )


class LACE(Model):
    limerick = ForeignKey(Limerick, on_delete=CASCADE)
    date = DateTimeField(null=True, default=now)

    class Meta:
        get_latest_by = "date"
        ordering = ("-date",)


class Revision(OrderedModel):
    id = HashidAutoField(primary_key=True)
    limerick = ForeignKey(Limerick, on_delete=CASCADE)
    verse = TextField()
    primary_author = ForeignKey(
        User, on_delete=CASCADE, null=True, related_name="primary_author"
    )
    secondary_author = ForeignKey(
        User, on_delete=CASCADE, null=True, related_name="secondary_author"
    )
    legacy_verse_id = IntegerField(default=0, null=True, db_index=True)
    author_notes = TextField(blank=True)
    editor_notes = TextField(blank=True)
    title = TextField(blank=True)
    updated_at = DateTimeField(null=True)
    version = PositiveIntegerField(default=0)
    workshop_state = EnumIntegerField(WorkshopStates)
    rfa_count = SmallIntegerField(default=0)
    order_with_respect_to = "version"
    is_latest_revision = BooleanField(default=False)

    definedword_set = ManyToManyField(
        DefinedWord,
        blank=True,
        related_name="revision_set",
        through='RevisionDefinedWordsThroughModel',
    )
    search_vector = SearchVectorField(null=True)

    class Meta:
        get_latest_by = "version"
        ordering = ("-version",)
        indexes = [GinIndex(fields=['search_vector'])]

    def __str__(self):
        return f"{self.pk}, limerick {self.limerick.id}, version {self.version}, lvid={self.legacy_verse_id}"

    @property
    def author_names(self):
        if self.primary_author is None:
            return "(none)"

        return " and ".join(a.username for a in self.authors)

    @property
    def author_name_links(self):
        return format_html_join(
            " and ", "{}", [(a.author.as_link(),) for a in self.authors]
        )

    def _format_defined_words(self, separator=", "):
        # django-orderedmodel ought to do this ordering for me, but I can't work out why it isn't
        queryset = self.definedword_set.all().order_by(
            'revisiondefinedwordsthroughmodel__order'
        )
        words = separator.join(queryset.values_list("word_text", flat=True))
        if words:
            return html.unescape(words)

        else:
            return "(none)"

    @property
    def all_defined_words(self):
        return self._format_defined_words("\n")

    @property
    def formatted_defined_words(self):
        return self._format_defined_words(", ")

    @property
    def f_is_latest_revision(self):
        return hasattr(self.limerick.latest_revision, 'pk') and (
            self.limerick.revision_set.count() == 1
            or self.limerick.latest_revision.pk == self.pk
        )

    @property
    def authors(self):
        if self.secondary_author is None:
            return [self.primary_author]

        else:
            return [self.primary_author, self.secondary_author]

    def denormalise_to_limerick(self):
        if not self.is_latest_revision:
            return
        limerick = self.limerick
        limerick.primary_author = self.primary_author
        limerick.secondary_author = self.secondary_author
        limerick.last_revision_date = self.updated_at
        comments = self.comment_set.order_by("pk")
        if comments.exists():
            comment = comments.first()
            if comment.edit_date_time is not None:
                limerick.last_workshop_date = comment.edit_date_time
            else:
                limerick.last_workshop_date = comment.date_time
        if limerick.created_date is None:
            limerick.created_date = datetime.datetime(1970, 1, 1)
            for thisrev in limerick.revision_set.all().order_by("version"):
                limerick.created_date = thisrev.updated_at
                break
        limerick.eligible_for_self_rfa = limerick.is_eligible_for_self_rfa

        limerick.primary_author_approved = limerick.rfa_set.filter(
            author=limerick.primary_author
        ).exists()
        limerick.secondary_author_approved = (
            limerick.rfa_set.filter(author=limerick.secondary_author).exists()
            if limerick.secondary_author is not None
            else False
        )

        limerick.save()

        lim_dws = []
        for dw in self.definedword_set.all():
            lim_dws.append(
                LimerickDefinedWordsThroughModel(
                    limerick=limerick, defined_word=dw, order=len(lim_dws)
                )
            )
        LimerickDefinedWordsThroughModel.objects.bulk_create(lim_dws)
        for author in map(lambda u: u.author, self.authors):
            author.update_stats()
            author.save()

        if limerick.revision_set.count() > 0:
            limerick.revision_set.filter(
                version__lt=limerick.revision_set.aggregate(Max("version"))[
                    "version__max"
                ]
            ).update(is_latest_revision=False)

    def save(self, *args, **kwargs):
        if (
            self.limerick
            and self.version
            == self.limerick.revision_set.aggregate(Max("version"))["version__max"]
        ):
            self.is_latest_revision = True
        result = super(Revision, self).save(*args, **kwargs)
        self.denormalise_to_limerick()
        return result


class RFA(Model):
    limerick = ForeignKey(Limerick, on_delete=CASCADE)
    author = ForeignKey(User, on_delete=CASCADE, null=False)
    new = BooleanField(default=True)

    def __str__(self):
        return f"{self.pk}: RFA on {self.limerick.pk} by {self.author}"


class Tag(Model):
    id = HashidAutoField(primary_key=True)
    author = ForeignKey(User, on_delete=CASCADE)
    limericks = ManyToManyField(Limerick, related_name="tags", blank=True)
    text = CharField(max_length=65)

    def __str__(self):
        return f"{self.pk}: \"{self.text}\" by {self.author.username}"


class CommentActions(IntEnum):
    NONE = 0
    RFA_ADD = 1
    RFA_REMOVE = 2
    ADD_CURTAIN = 3
    REMOVE_CURTAIN = 4
    HOLD = 5
    UNHOLD = 6
    CLEAR_RFAS = 7
    STC = 8
    COUNTDOWN_STOP = 9
    ADD_REVISION = 10
    CFA = 11
    CFA_END = 12

    class Labels:
        NONE = "Just comment"
        RFA_ADD = "Add RFA"
        RFA_REMOVE = "Remove RFA"
        ADD_CURTAIN = "Add curtain"
        REMOVE_CURTAIN = "Remove curtain"
        HOLD = "Hold"
        UNHOLD = "Unhold"
        CLEAR_RFAS = "Clear ALL RFAs"
        STC = "Set to confirming"
        COUNTDOWN_STOP = "Countdown stopped"
        ADD_REVISION = "Add revision"
        CFA = "Call For Attention"
        CFA_END = "CFA ended"

    @classmethod
    def by_label(cls, label):
        for action in cls.__members__.values():
            if action.label == label:
                return action

        else:
            raise KeyError(label)


class Comment(OrderedModel):
    COMMENT_LINK_RE = re.compile(r"#(\w+)")

    id = HashidAutoField(primary_key=True, allow_int_lookup=True)
    revision = ForeignKey(Revision, on_delete=CASCADE)
    author = ForeignKey(User, on_delete=CASCADE, null=True)
    date_time = DateTimeField(null=False, db_index=True)
    message = TextField()
    poster_ip = CharField(max_length=20)
    edit_date_time = DateTimeField(null=True)
    order_with_respect_to = "pk"
    action = EnumIntegerField(CommentActions, null=True)

    def __str__(self):
        return f"{self.pk}: {self.author}: {self.message[:30]}..."

    def _modify_link(self, comment_pk):
        comments = self.revision.limerick.revision_set.filter(comment__pk=comment_pk)
        if comments.exists():
            comment = Comment.objects.get(pk=comment_pk)
            return f"<a href='#{comment.id}'>#{comment_pk}</a>"

        else:
            return f"#{comment_pk}"

    @property
    def message_with_comment_links(self):
        return self.COMMENT_LINK_RE.sub(
            lambda x: self._modify_link(x.group(1)), self.message
        )

    class Meta:
        ordering = ("-date_time",)


class ActivityMessageType(IntEnum):
    TEXT = 0
    NEW_LIMERICK = 1
    REVISION = 2
    ATTENTION = 3  # used when someone clicks No on final approval
    COMMENT = 4
    WS_COMMENT = 4  # alias
    STATE_CHANGE = 5
    APPROVAL = 6
    PROMOTION = 7
    CFA = 8
    USER_MESSAGE = 9


class ActivityMessage(Model):
    id = HashidAutoField(primary_key=True)
    source = ForeignKey(
        User, on_delete=CASCADE, related_name="message_source", null=True
    )
    dest = ForeignKey(User, on_delete=CASCADE, related_name="message_dest")
    revision = ForeignKey(Revision, on_delete=CASCADE, null=True)
    author = ForeignKey(User, on_delete=CASCADE, related_name="message_author")
    message_type = EnumIntegerField(ActivityMessageType)
    legacy_message_id = PositiveIntegerField(default=0, db_index=True)
    date_time = DateTimeField()
    data = TextField()
    seen = BooleanField(default=True)

    def __str__(self):
        return f"{self.pk}: {self.data[:30]}..."


class SiteSettings(SingletonModel):
    alphabet_end = CharField(max_length=10, default="zzzzzzzzzz")
    admin_email = CharField(max_length=80, null=True)
    eic_email = CharField(max_length=80, null=True, verbose_name="EiC email")
    shutdown = TextField(null=True)
    copyright = TextField(default="")
    last_lace_check = DateTimeField(null=True, verbose_name="Last LACE check")
    laces_per_check = IntegerField(default=0, verbose_name="LACEs per check")
    limerick_number = IntegerField(default=0)
    wpsi_submission = BooleanField(default=False, verbose_name="WPSI Submission")
    inactive_days = PositiveSmallIntegerField(
        default=30,
        help_text="Number of days without logging in, "
        "after which user is considered inactive",
    )
    countdown_days = IntegerField(
        default=7, help_text="Number of days that an approval countdown runs for"
    )
    lace_resend_days = IntegerField(
        default=14, help_text="Number of days after which a second LACE will be sent"
    )
    cfa_contributors = IntegerField(
        default=7,
        verbose_name="CFA contributors",
        help_text="Number of WEs who must contribute to a CFA in order to close it",
    )
    cfa_dismissal_timeout = IntegerField(
        default=86400,
        verbose_name="CFA dismissal timeout (seconds)",
        help_text="Number of seconds before an active CFA is displayed again",
    )

    def __unicode__(self):
        return "Site Settings"

    class Meta:
        verbose_name = "Site Settings"


class SiteStatistics(SingletonModel):
    total = IntegerField(default=0)
    approved = IntegerField(default=0)
    approved_showcase = IntegerField(default=0)
    approved_general = IntegerField(default=0)
    approved_general_showcase = IntegerField(default=0)

    def __unicode__(self):
        return "Statistics"

    class Meta:
        verbose_name = "Statistics"


class SiteDocument(Model):
    title = CharField(max_length=80)
    text = TextField(null=True)


class Showcase(OrderedModel):
    user = ForeignKey(User, on_delete=CASCADE)
    limerick = ForeignKey(Limerick, on_delete=CASCADE)
    order_with_respect_to = "user"

    def __unicode__(self):
        return f"{self.id} (limerick {self.limerick.id})"

    __str__ = __unicode__


class TextMacro(Model):
    id = HashidAutoField(primary_key=True)
    title = CharField(max_length=40)
    body = TextField()
    author_type = EnumIntegerField(AuthorTypes)


class Topic(MP_Node):
    id = HashidAutoField(primary_key=True, allow_int_lookup=True)
    name = TextField()
    description = TextField(blank=True)

    node_order_by = ['name']

    def __unicode__(self):
        return f"{self.name} [{self.id}]"

    __str__ = __unicode__


class LimerickWatcher(Model):
    """
    When an update is made to a limerick, every author listed in this table
    (except for the person making the update)
    gets an ActivityMessage.
    """

    author = ForeignKey(User, on_delete=CASCADE)
    limerick = ForeignKey(Limerick, on_delete=CASCADE)

    def __unicode__(self):
        return f"LimerickWatcher: user {self.author.pk} watching limerick {self.limerick.pk}"

    __str__ = __unicode__


class LimerickDefinedWordsThroughModel(OrderedModel):
    limerick = ForeignKey(Limerick, on_delete=CASCADE)
    defined_word = ForeignKey(DefinedWord, on_delete=CASCADE)
    order_with_respect_to = 'limerick'

    class Meta:
        ordering = ('limerick', 'order')


class RevisionDefinedWordsThroughModel(OrderedModel):
    revision = ForeignKey(Revision, on_delete=CASCADE)
    defined_word = ForeignKey(DefinedWord, on_delete=CASCADE)
    order_with_respect_to = 'revision'

    class Meta:
        ordering = ('revision', 'order')


class Project(Model):

    """
    A Project is used to identify a set of limericks
    which we would like WEs to focus their workshopping attention on
    """

    abbreviation = CharField(max_length=10)
    short_description = CharField(max_length=80)
    description = TextField(null=True, blank=True)
    active = BooleanField()
    limericks = ManyToManyField(Limerick, blank=True)

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        self.abbreviation = self.abbreviation.upper()
        super(Project, self).save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return f"({self.pk}) {self.abbreviation}"


class GrabType(IntEnum):

    NONE = 0
    RANDOM = 1
    RFA0 = 2
    RFA1 = 3
    RFA2 = 4
    RFA3 = 5
    RFA4 = 6
    PROJECT = 7
    STC = 8

    class Labels:
        NONE = "This should never be used!"
        RANDOM = "Random"
        RFA0 = "0 RFAs"
        RFA1 = "1 RFA"
        RFA2 = "2 RFAs"
        RFA3 = "3 RFAs"
        RFA4 = "4+ RFAs"
        PROJECT = "Project"
        STC = "Eligible for STC"


class Grabbed(Model):

    """
    A limerick can be Grabbed for a workshopping purpose.  But it cannot be Grabbed more than once
    in a week.
    """

    limerick = ForeignKey(Limerick, on_delete=CASCADE)
    author = ForeignKey(User, on_delete=CASCADE)
    type = EnumIntegerField(GrabType, default=GrabType.NONE)
    project = ForeignKey(Project, on_delete=CASCADE, blank=True, null=True)
    date = DateTimeField(default=now)

    def __str__(self):
        return f"({self.pk}): user {self.author_id} grabbed {self.limerick_id}"


# A Call For Attention can be raised on a limerick
# This is presented prominently to all WEs
# until a sufficient number of WEs have commented.


class CFA(Model):

    active = BooleanField(default=True)
    creator = ForeignKey(User, on_delete=CASCADE)
    limerick = ForeignKey(Limerick, on_delete=CASCADE)


class CFAResponse(Model):

    cfa = ForeignKey(CFA, on_delete=CASCADE)
    contributed = BooleanField(default=False)
    dismissed = BooleanField(default=False)
    date_time = DateTimeField(auto_now_add=True)
    author = ForeignKey(User, on_delete=CASCADE)
