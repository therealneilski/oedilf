import absoluteuri
from django.db.models.signals import post_save, pre_save, post_delete
from django.dispatch import receiver
from django_slack import slack_message
from fieldsignals import pre_save_changed, post_save_changed
from lim.models import *
from pinax.announcements.models import Announcement
from tellme.models import Feedback


# noinspection PyUnusedLocal


@receiver(post_save, sender=User)
def add_author_to_user(sender, instance, created, **kwargs):
    if (
        created
        and not kwargs.get("raw", False)
        and not Author.objects.filter(user=instance).exists()
    ):
        Author.objects.create(user=instance)


@receiver(pre_save, sender=Comment)
def denormalise_comment(**kwargs):
    comment = kwargs["instance"]
    update_fields = kwargs["update_fields"]
    if update_fields is None:
        return

    limerick = comment.revision.limerick
    if "date_time" in update_fields:
        limerick.last_workshop_date = comment.date_time
    if "edit_date_time" in update_fields:
        limerick.last_workshop_date = comment.edit_date_time
    limerick.save()


# noinspection PyUnusedLocal


def update_limerick_save_state_date(sender, instance, **kwargs):
    instance.state_date = datetime.datetime.now()


pre_save_changed.connect(
    update_limerick_save_state_date, sender=Limerick, fields=["state"]
)


@receiver([post_save, post_delete], sender=RFA)
def update_rfa_count(sender, instance, **kwargs):
    if kwargs.get("raw", False):
        return False

    limerick = instance.limerick
    revision = limerick.latest_revision
    if revision is None:
        return False

    revision.rfa_count = limerick.rfa_set.count()
    revision.save()


##############################
# Activity messages
##############################

# Comments
# When non-RFA comment created by me:
# - add me to watchers if I am not already there
# - notify all watchers except me

# noinspection PyUnusedLocal
@receiver(pre_save, sender=Comment)
def add_to_limerick_watchers_when_commenting(sender, instance, raw, **kwargs):
    if (
        not raw
        and instance.action != CommentActions.RFA_ADD
        and instance.author is not None
    ):
        LimerickWatcher.objects.get_or_create(
            author=instance.author, limerick=instance.revision.limerick
        )


@receiver(post_save, sender=Comment)
def add_comment_activity_message(sender, instance, **kwargs):
    if kwargs.get("raw", False):
        return False
    if not kwargs['created']:
        return
    if instance.action != CommentActions.NONE:
        return

    message_type = ActivityMessageType.COMMENT
    revision = instance.revision
    authors_to_notify = map(
        lambda x: x[0],
        revision.limerick.limerickwatcher_set.exclude(
            author=instance.author
        ).values_list('author'),
    )
    if instance.message == "":
        message = ""
    elif len(instance.message) > 120:
        message = instance.message[:120] + "..."
    else:
        message = instance.message
    ActivityMessage.objects.bulk_create(
        [
            ActivityMessage(
                revision=revision,
                message_type=message_type,
                data=message,
                date_time=instance.date_time,
                source=instance.author,
                dest=User.objects.get(pk=author_id),
                author=revision.primary_author,
                seen=False,
            )
            for author_id in authors_to_notify
        ]
    )


# Revisions
# When revision created by me:
# - add all authors as watchers unless already there
# - notify all watchers except me


@receiver(pre_save, sender=Revision)
def add_to_limerick_watchers_when_adding_revision(sender, instance, raw, **kwargs):
    if not raw:
        LimerickWatcher.objects.get_or_create(
            author=instance.primary_author, limerick=instance.limerick
        )
        if instance.secondary_author is not None:
            LimerickWatcher.objects.get_or_create(
                author=instance.secondary_author, limerick=instance.limerick
            )


@receiver(post_save, sender=Revision)
def add_revision_activity_message(sender, instance, raw, created, **kwargs):
    if raw or not created:
        return

    if instance.limerick.revision_set.count() == 1:
        message_type = ActivityMessageType.NEW_LIMERICK
    else:
        message_type = ActivityMessageType.REVISION
    authors_to_notify = map(
        lambda x: x[0],
        instance.limerick.limerickwatcher_set.exclude(
            author=instance.primary_author
        ).values_list('author'),
    )
    if instance.updated_at is None:
        instance.updated_at = datetime.datetime(1970, 1, 1)
    ActivityMessage.objects.bulk_create(
        [
            ActivityMessage(
                revision=instance,
                message_type=message_type,
                data=instance.verse[: instance.verse.find('\n')] + "...",
                date_time=instance.updated_at,
                source=instance.primary_author,
                dest=User.objects.get(pk=author_id),
                author=instance.primary_author,
                seen=False,
            )
            for author_id in authors_to_notify
        ]
    )


# Limericks
# When limerick created by me:
# - add me as watcher
# - add all authors who are watching for new limericks
# - notify watchers except me
#
# Notification is taken care of by the new revision


@receiver(post_save, sender=Limerick)
def notify_when_adding_or_changing_state_of_limerick(
    sender, instance, created, raw, using, update_fields, **kwargs
):
    if raw:
        return True

    # This needs to be in post_save because otherwise there's no limerick id
    if created or (update_fields is not None and 'state' in update_fields):
        authors_to_notify = User.objects.filter(author__monitor_new=MonitorNew.ALL)
        LimerickWatcher.objects.bulk_create(
            [
                LimerickWatcher(author=author, limerick=instance)
                for author in authors_to_notify
            ]
        )


# noinspection PyUnusedLocal
def notify_when_changing_state_of_limerick(sender, instance, **kwargs):
    authors_to_notify = map(
        lambda x: x[0],
        instance.limerickwatcher_set.exclude(
            author=instance.primary_author
        ).values_list('author'),
    )
    if instance.latest_revision is not None:
        ActivityMessage.objects.bulk_create(
            [
                ActivityMessage(
                    revision=instance.latest_revision,
                    message_type=ActivityMessageType.STATE_CHANGE,
                    data=instance.latest_revision.verse[
                        : instance.latest_revision.verse.find('\n')
                    ]
                    + "...",
                    date_time=instance.latest_revision.updated_at,
                    source=instance.primary_author,
                    dest=User.objects.get(pk=author_id),
                    author=instance.primary_author,
                    seen=False,
                )
                for author_id in authors_to_notify
            ]
        )


post_save_changed.connect(
    notify_when_changing_state_of_limerick, sender=Limerick, fields=["state"]
)


# RFAs
# When RFA created by me and I am *not* a watcher:
# - If MonitorRFAs == OFF, don't do anything
# - If MonitorRFAs == ALL, add me to watchers
# - If MonitorRFAs == REVISION, add me to watchers (TODO: enhance this)
# - If MonitorRFAs == STATE, add me to watchers (TODO: enhance this)


@receiver(pre_save, sender=RFA)
def add_to_limerick_watchers_when_adding_rfa(sender, instance, raw, **kwargs):
    if not raw and instance.author.author.monitor_rfas != MonitorRFAs.OFF:
        LimerickWatcher.objects.get_or_create(
            author=instance.author, limerick=instance.limerick
        )


# Feedback from django-tellme


@receiver(post_save, sender=Feedback)
def send_feedback(sender, instance: Feedback, **kwargs):
    screenshot_url = (
        instance.screenshot.url
        if "http" in instance.screenshot.url
        else absoluteuri.build_absolute_uri("/" + instance.screenshot.url)
    )
    attachments = [
        {
            "fields": [
                {"title": "User", "value": instance.user.username, "short": True},
                {"title": "Feedback ID", "value": instance.pk, "short": True},
                {"title": "Page", "value": instance.url, "short": False},
                {"title": "Comment", "value": instance.comment, "short": False},
            ],
            "pretext": "Feedback received!",
            "ts": instance.created.timestamp(),
            "image_url": screenshot_url,
        }
    ]
    slack_message("slack/tellme.slack", {"fb": instance}, attachments)


# When a new announcement is created (from pinax-announcements), post this to Slack


@receiver(post_save, sender=Announcement)
def announce_to_slack(sender, instance: Announcement, **kwargs):
    slack_message("slack/announcement.slack", {"announcement": instance})
