from django.contrib.auth.validators import ASCIIUsernameValidator
from django.utils.translation import gettext_lazy as _


class ASCIISpaceUsernameValidator(ASCIIUsernameValidator):
    regex = r'^[ \w.@+-]+$'
    message = _(
        'Enter a valid username. This value may contain only English letters, '
        'numbers, spaces, and @/./+/-/_ characters.'
    )


custom_username_validators = [ASCIISpaceUsernameValidator()]
