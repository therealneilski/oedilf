import re

import bleach
from bs4 import BeautifulSoup

NUMERIC = re.compile(r'^\d+$')


def is_numeric(s):
    return NUMERIC.match(s)


def clean_trumbowyg(s):
    if "<div" in s and not s.startswith("<div"):
        i = s.index("<div")
        s = "<div>{}</div>{}".format(s[:i], s[i:])
    s = bleach.clean(
        s,
        [
            "b",
            "i",
            "s",
            "strike",
            "sup",
            "sub",
            "big",
            "small",
            "tt",
            "u",
            "ul",
            "ol",
            "li",
            "h1",
            "h2",
            "h3",
            "h4",
            "div",
        ],
    )
    soup = BeautifulSoup(s, "html.parser")
    for div in soup.find_all("div"):
        div.append("\n")
        div.unwrap()
    return str(soup).strip()


def oxford_join(list_to_join, sep=", ", final_sep=", and ", strip_empty=False):
    if strip_empty:
        list_to_join = list(
            filter(lambda x: x is not None and str(x).strip() != "", list_to_join)
        )
    if len(list_to_join) == 0:
        return ""
    elif len(list_to_join) == 1:
        return str(list_to_join[0])
    else:
        return final_sep.join((sep.join(list_to_join[:-1]), list_to_join[-1]))
