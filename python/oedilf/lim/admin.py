import re

import impersonate.admin
import impersonate.models
import pinax.announcements.admin
import pinax.announcements.models
import tellme.admin
import tellme.models
from django.conf import settings
from django.contrib import admin
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.db.models import Q, Count
from django.forms import ModelForm
from django.template.response import TemplateResponse
from django.urls import path
from django.utils.decorators import method_decorator
from django.utils.functional import LazyObject
from django.utils.html import format_html
from fsm_admin.mixins import FSMTransitionMixin
from solo.admin import SingletonModelAdmin
from tinymce import AdminTinyMCE
from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory

from lim.models import (
    Author,
    DefinedWord,
    Limerick,
    Revision,
    Comment,
    Project,
    SiteSettings,
    SiteStatistics,
    SiteDocument,
    Topic,
)


class LimAdminSite(admin.AdminSite):
    def has_permission(self, request):
        return (request.user.is_active and not request.user.is_anonymous) and (
            request.user.is_superuser or request.user.has_perm("lim.can_view_admin")
        )


class LazyLimAdminSite(LazyObject):
    def _setup(self):
        self._wrapped = LimAdminSite("lim_admin")


lim_admin_site = LazyLimAdminSite()


class StaffRequiredAdminMixin(object):
    def check_perm(self, user_obj):
        if not user_obj.is_active or user_obj.is_anonymous:
            return False
        if user_obj.is_superuser or user_obj.has_perm("lim.can_view_admin"):
            return True
        return False

    def has_add_permission(self, request):
        return self.check_perm(request.user)

    def has_change_permission(self, request, obj=None):
        return self.check_perm(request.user)

    def has_delete_permission(self, request, obj=None):
        return self.check_perm(request.user)

    def has_view_permission(self, request, obj=None):
        user_obj = request.user
        if user_obj.is_anonymous or not user_obj.is_active:
            return False
        else:
            return self.check_perm(user_obj) or user_obj.has_perm("lim.can_view_admin")

    def has_module_permission(self, request):
        return self.has_view_permission(request)


class AuthorAdmin(StaffRequiredAdminMixin, admin.ModelAdmin):
    fieldsets = [
        ("Profile", {"fields": ["user", "author_type", "author_rank"]}),
        (
            "Notifications",
            {
                "fields": [
                    "notify_changes",
                    "monitor_new",
                    "monitor_comments",
                    "monitor_attention",
                    "use_email",
                    "approval_emails",
                    "milestone_emails",
                    "news_emails",
                ]
            },
        ),
        (
            "Personal",
            {
                "fields": [
                    "real_name",
                    "biography",
                    "feedback_style",
                    "time_zone",
                    "lat",
                    "lng",
                    "born_in",
                    "living_in",
                    "twitter_handle",
                ]
            },
        ),
        ("Sensitivity", {"fields": ["curtain_filtering", "junior_mode"]}),
    ]
    list_display = (
        "user",
        "author_type",
        "author_rank",
        "real_name",
        "biography",
        "notify_changes",
        "monitor_new",
        "monitor_comments",
        "monitor_attention",
        "use_email",
        "feedback_style",
        "time_zone",
        "monitor_rfas",
        "junior_mode",
        "curtain_filtering",
        "text_macro_buttons",
        "ws_comment_display_limit",
        "lat",
        "lng",
        "born_in",
        "living_in",
        "approval_emails",
        "milestone_emails",
        "news_emails",
        "twitter_handle",
    )
    search_fields = ("user__username",)


lim_admin_site.register(Author, AuthorAdmin)


class CommentInline(admin.StackedInline):
    model = Comment
    extra = 0
    readonly_fields = ("author",)


class RevisionInline(admin.StackedInline):
    model = Revision
    extra = 0
    readonly_fields = (
        "version",
        "legacy_verse_id",
        "primary_author",
        "secondary_author",
    )
    fields = (
        "verse",
        "version",
        "primary_author",
        "secondary_author",
        "legacy_verse_id",
        "author_notes",
        "editor_notes",
        "title",
        "updated_at",
        "workshop_state",
    )
    show_change_link = True


class RevisionAdmin(StaffRequiredAdminMixin, admin.ModelAdmin):
    model = Revision
    fields = RevisionInline.fields
    list_display = RevisionInline.fields
    inlines = [CommentInline]
    show_change_link = True


class LimerickAdmin(StaffRequiredAdminMixin, FSMTransitionMixin, admin.ModelAdmin):
    list_display = (
        "id",
        "formatted_defined_words",
        "original_id",
        "limerick_number",
        "state",
        "primary_author",
        "secondary_author",
    )
    fields = list_display
    inlines = [RevisionInline]
    list_filter = ("state",)
    readonly_fields = (
        "id",
        "formatted_defined_words",
        "original_id",
        "limerick_number",
        "primary_author",
        "secondary_author",
    )
    list_select_related = True


lim_admin_site.register(Limerick, LimerickAdmin)
lim_admin_site.register(Revision, RevisionAdmin)

lim_admin_site.register(SiteSettings, SingletonModelAdmin)
lim_admin_site.register(SiteStatistics, SingletonModelAdmin)


class SiteDocumentForm(ModelForm):
    class Meta:
        model = SiteDocument
        widgets = {"text": AdminTinyMCE()}
        fields = "__all__"


class SiteDocumentAdmin(StaffRequiredAdminMixin, admin.ModelAdmin):
    list_display = (
        "title",
        "text",
        # 'html_view'
    )
    form = SiteDocumentForm

    def html_view(self, obj):  # pragma: no cover
        return format_html(obj.text)

    html_view.empty_value_display = "None"


lim_admin_site.register(SiteDocument, SiteDocumentAdmin)


class TopicAdmin(StaffRequiredAdminMixin, TreeAdmin):
    form = movenodeform_factory(Topic)
    list_per_page = 2000


lim_admin_site.register(Topic, TopicAdmin)


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        widgets = {"text": AdminTinyMCE()}
        fields = ("abbreviation", "short_description", "description", "active")


class ProjectAdmin(StaffRequiredAdminMixin, admin.ModelAdmin):
    list_display = ("abbreviation", "short_description", "active")
    fields = ("abbreviation", "short_description", "description", "active", "limericks")
    raw_id_fields = ("limericks",)
    form = ProjectForm
    related_lookup_fields = {'m2m': ["limericks"]}

    def limerick_count(self, instance):
        return instance.limericks.count()

    limerick_count.short_description = "Limericks in project"


lim_admin_site.register(Project, ProjectAdmin)


class DefinedWordAdmin(StaffRequiredAdminMixin, admin.ModelAdmin):
    def get_urls(self):
        urls = super(DefinedWordAdmin, self).get_urls()
        my_urls = [path(r'bulk_change/', self.my_view, name="defined-word-bulk-change")]
        return my_urls + urls

    @method_decorator(staff_member_required)
    def my_view(self, request):
        # ...
        context = dict(
            self.admin_site.each_context(request), title="Bulk change required words"
        )
        if request.method == 'POST':
            words = re.split(
                r'\s*[\n;][\d\s]*', "\n" + request.POST['bulk-change-paste']
            )
            words = set([word for word in words if word])  # exclude ''
            if request.POST['action'] == "Add words":
                self.add_words(request, words)
            elif request.POST['action'] == "Remove words":
                self.remove_words(request, words)
        return TemplateResponse(request, "admin/required-word.html", context)

    @staticmethod
    def add_words(request, words):
        words_created = {False: set(), True: set()}
        for word in words:
            dw, created = DefinedWord.objects.get_or_create(word_text=word)
            words_created[created].add(dw)
        if words_created[True]:
            messages.info(
                request,
                "Created RequiredWords: "
                + ", ".join([word.word_text for word in words_created[True]]),
            )
        if words_created[False]:
            messages.warning(
                request,
                "Did not create RequiredWords: "
                + ", ".join([word.word_text for word in words_created[False]]),
            )

    @staticmethod
    def remove_words(request, words):
        defined_words_to_delete = (
            DefinedWord.objects.filter(Q(word_text__in=words))
            .annotate(Count("limerick_set"), Count("revision_set"))
            .filter(Q(limerick_set__count=0) & Q(revision_set__count=0))
        )
        words_to_delete = set(
            map(lambda x: x[0], defined_words_to_delete.values_list('word_text'))
        )
        defined_words_to_delete.delete()
        if words_to_delete:
            messages.info(request, "Deleted words: " + ", ".join(words_to_delete))
        words_requested = set(words)
        if words_requested != words_to_delete:
            messages.warning(
                request,
                "Did not delete words: "
                + ", ".join(words_requested.difference(words_to_delete)),
            )


lim_admin_site.register(DefinedWord, DefinedWordAdmin)


if "axes" in settings.INSTALLED_APPS:
    import axes.admin
    import axes.models

    class AccessLogAdmin(StaffRequiredAdminMixin, axes.admin.AccessLogAdmin):

        pass

    class AccessAttemptAdmin(StaffRequiredAdminMixin, axes.admin.AccessAttemptAdmin):

        pass

    lim_admin_site.register(axes.models.AccessLog, AccessLogAdmin)
    lim_admin_site.register(axes.models.AccessAttempt, AccessAttemptAdmin)


# pinax-notifications


class AnnouncementForm(ModelForm):
    class Meta:
        model = pinax.announcements.models.Announcement
        widgets = {"content": AdminTinyMCE()}
        fields = "__all__"


class AnnouncementAdmin(
    StaffRequiredAdminMixin, pinax.announcements.admin.AnnouncementAdmin
):
    form = AnnouncementForm

    def get_changeform_initial_data(self, request):
        return {
            "site_wide": True,
            "dismissal_type": pinax.announcements.models.Announcement.DISMISSAL_PERMANENT,
        }


class DismissalAdmin(StaffRequiredAdminMixin, pinax.announcements.admin.DismissalAdmin):
    pass


lim_admin_site.register(pinax.announcements.models.Announcement, AnnouncementAdmin)
lim_admin_site.register(pinax.announcements.models.Dismissal, DismissalAdmin)


# django-tellme


class FeedbackAdmin(StaffRequiredAdminMixin, tellme.admin.FeedbackAdmin):
    pass


lim_admin_site.register(tellme.models.Feedback, FeedbackAdmin)


# django-impersonate


class ImpersonationLogAdmin(
    StaffRequiredAdminMixin, impersonate.admin.ImpersonationLogAdmin
):
    pass


lim_admin_site.register(impersonate.models.ImpersonationLog, ImpersonationLogAdmin)


# finishing touches

admin.site.site_title = "OEDILF System Administration"
admin.site.site_header = "OEDILF System Administration"

lim_admin_site.site_title = "OEDILF Administration"
lim_admin_site.site_header = "OEDILF Administration"
