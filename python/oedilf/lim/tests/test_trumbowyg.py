from django.test import TestCase
from lim.utils import clean_trumbowyg


class TrumbowygTester(TestCase):
    def test_divs_all_round(self):
        sample = "<div>There was an old man of <b>Peru</b></div><div>Whose limericks stopped at line two.</div>"
        self.assertEqual(
            clean_trumbowyg(sample),
            "There was an old man of <b>Peru</b>\nWhose limericks stopped at line two.",
        )

    def test_first_div_missing(self):
        sample = "<div>There was an old man of <b>Peru</b></div><div>Whose limericks stopped at line two.</div>"
        self.assertEqual(
            clean_trumbowyg(sample),
            "There was an old man of <b>Peru</b>\nWhose limericks stopped at line two.",
        )

    def test_no_divs(self):
        sample = "RFA"
        self.assertEqual(clean_trumbowyg(sample), "RFA")
