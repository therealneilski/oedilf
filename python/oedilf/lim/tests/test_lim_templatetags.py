from django.test import TestCase

from lim.models import LimStates
from lim.templatetags import lim as lim_templatetags
from lim.tests import factories


class OedilfLinkExpansionTester(TestCase):
    def setUp(self):
        self.user = factories.UserFactory()
        self.author = self.user.author
        self.rev = factories.RevisionWithDefinedWordFactory(
            limerick__state=LimStates.TENTATIVE,
            limerick__original_id=1,
            limerick__limerick_number=0,
        )

    def test_not_a_link(self):
        # format has to be [[#nnn(:xxxxx)?]] - don't forget the #
        self.assertEquals(lim_templatetags.lim_links("[[T1]]"), "[[T1]]")
        self.assertEquals(lim_templatetags.lim_links("[[1]]"), "[[1]]")
        self.assertEquals(lim_templatetags.lim_links("[[T1:test]]"), "[[T1:test]]")
        self.assertEquals(lim_templatetags.lim_links("[[1:test]]"), "[[1:test]]")

    def test_tentative_no_text(self):
        self.assertEquals(
            '<a href="/limericks/{}/">#T1</a>'.format(self.rev.limerick.pk),
            lim_templatetags.lim_links("[[#T1]]"),
        )

    def test_approved_no_text(self):
        self.assertEquals(
            '<a href="/limericks/{}/">#0</a>'.format(self.rev.limerick.pk),
            lim_templatetags.lim_links("[[#0]]"),
        )
        self.assertEquals(
            f'<a href="/limericks/{self.rev.limerick.pk}/">#{self.rev.limerick.pk}</a>',
            lim_templatetags.lim_links(f"[[#{self.rev.limerick.pk}]]"),
        )

    def test_tentative_text(self):
        self.assertEquals(
            '<a href="/limericks/{}/">Hello</a>'.format(self.rev.limerick.pk),
            lim_templatetags.lim_links("[[#T1:Hello]]"),
        )

    def test_approved_text(self):
        self.assertEquals(
            '<a href="/limericks/{}/">Banana</a>'.format(self.rev.limerick.pk),
            lim_templatetags.lim_links("[[#0:Banana]]"),
        )
        self.assertEquals(
            f'<a href="/limericks/{self.rev.limerick.pk}/">Hello</a>',
            lim_templatetags.lim_links(f"[[#{self.rev.limerick.pk}:Hello]]"),
        )

    def test_destination_does_not_exist(self):
        self.assertEquals("[[#10]]", lim_templatetags.lim_links("[[#10]]"))
        self.assertEquals("[[#T10]]", lim_templatetags.lim_links("[[#T10]]"))
        self.assertEquals("[[#10:Hello]]", lim_templatetags.lim_links("[[#10:Hello]]"))
        self.assertEquals(
            "[[#T10:Hello]]", lim_templatetags.lim_links("[[#T10:Hello]]")
        )
