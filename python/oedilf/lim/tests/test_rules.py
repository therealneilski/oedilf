from django.test import TestCase

from lim.models import AuthorRanks, LimStates
from lim.tests import factories


class RulesTester(TestCase):
    def setUp(self):
        self.revision = factories.RevisionWithDefinedWordFactory()
        self.author = self.revision.primary_author
        self.we = factories.UserFactory(author__author_rank=AuthorRanks.WE)
        self.rookie = factories.UserFactory(author__author_rank=AuthorRanks.C)

    def tearDown(self):
        self.rookie.delete()
        self.revision.delete()
        self.author.delete()

    def test_can_add_curtain(self):

        self.assertIsNotNone(self.revision.pk)
        self.assertIsNotNone(self.revision.limerick.pk)

        self.assertTrue(self.author.has_perm("lim.add_curtain", self.revision.limerick))
        self.assertTrue(self.we.has_perm("lim.add_curtain", self.revision.limerick))
        self.assertFalse(
            self.rookie.has_perm("lim.add_curtain", self.revision.limerick)
        )

    def test_can_remove_curtain(self):

        self.revision.limerick.curtained = True
        self.revision.limerick.save()

        self.assertTrue(
            self.author.has_perm("lim.remove_curtain", self.revision.limerick)
        )
        self.assertTrue(self.we.has_perm("lim.remove_curtain", self.revision.limerick))
        self.assertFalse(
            self.rookie.has_perm("lim.remove_curtain", self.revision.limerick)
        )

    def test_can_hold(self):

        self.assertIsNotNone(self.revision.pk)
        self.assertIsNotNone(self.revision.limerick.pk)

        self.assertTrue(self.author.has_perm("lim.hold", self.revision.limerick))
        self.assertTrue(self.we.has_perm("lim.hold", self.revision.limerick))
        self.assertFalse(self.rookie.has_perm("lim.hold", self.revision.limerick))

    def test_can_unhold_held_by_author(self):

        self.revision.limerick.state = LimStates.HELD
        self.revision.limerick.held_by = self.author.author
        self.revision.limerick.save()

        self.assertTrue(self.author.has_perm("lim.unhold", self.revision.limerick))
        self.assertFalse(self.we.has_perm("lim.unhold", self.revision.limerick))
        self.assertFalse(self.rookie.has_perm("lim.unhold", self.revision.limerick))

    def test_can_unhold_held_by_we(self):

        self.revision.limerick.state = LimStates.HELD
        self.revision.limerick.held_by = self.we.author
        self.revision.limerick.save()

        self.assertFalse(self.author.has_perm("lim.unhold", self.revision.limerick))
        self.assertTrue(self.we.has_perm("lim.unhold", self.revision.limerick))
        self.assertFalse(self.rookie.has_perm("lim.unhold", self.revision.limerick))

    def test_hold_states(self):
        can_hold_states = (
            LimStates.TENTATIVE,
            LimStates.REVISED,
            LimStates.NEW,
            LimStates.UNTENDED,
        )
        # noinspection PyTypeChecker
        for state in LimStates:
            self.revision.limerick.state = state
            self.revision.limerick.save()
            if state in can_hold_states:
                self.assertTrue(
                    self.author.has_perm("lim.hold", self.revision.limerick),
                    "Should be able to hold in state {}".format(state.name),
                )
            else:
                self.assertFalse(
                    self.author.has_perm("lim.hold", self.revision.limerick),
                    "Should not be able to hold in state {}".format(state.name),
                )

    def test_add_comment(self):

        self.assertTrue(self.author.has_perm("lim.add_comment", self.revision.limerick))
        self.assertFalse(
            self.rookie.has_perm("lim.add_comment", self.revision.limerick)
        )
        self.assertTrue(self.we.has_perm("lim.add_comment", self.revision.limerick))

    def test_edit_delete_comment(self):

        comment = factories.CommentFactory(revision=self.revision, author=self.we)
        self.assertFalse(self.author.has_perm("lim.change_comment", comment))
        self.assertFalse(self.rookie.has_perm("lim.change_comment", comment))
        self.assertTrue(self.we.has_perm("lim.change_comment", comment))
        self.assertFalse(self.author.has_perm("lim.delete_comment", comment))
        self.assertFalse(self.rookie.has_perm("lim.delete_comment", comment))
        self.assertTrue(self.we.has_perm("lim.delete_comment", comment))

    def test_add_rfa(self):

        self.assertFalse(self.author.has_perm("lim.add_rfa", self.revision.limerick))
        self.assertFalse(self.rookie.has_perm("lim.add_rfa", self.revision.limerick))
        self.assertTrue(self.we.has_perm("lim.add_rfa", self.revision.limerick))
        factories.RFAFactory(limerick=self.revision.limerick, author=self.we)
        self.assertFalse(self.we.has_perm("lim.add_rfa", self.revision.limerick))

    def test_add_self_rfa(self):

        for _ in range(4):
            factories.RFAFactory(limerick=self.revision.limerick)
        self.assertTrue(self.author.has_perm("lim.add_rfa", self.revision.limerick))

    def test_remove_rfa(self):

        self.assertFalse(self.we.has_perm("lim.remove_rfa", self.revision.limerick))
        factories.RFAFactory(limerick=self.revision.limerick, author=self.we)
        self.assertFalse(self.author.has_perm("lim.remove_rfa", self.revision.limerick))
        self.assertFalse(self.rookie.has_perm("lim.remove_rfa", self.revision.limerick))
        self.assertTrue(self.we.has_perm("lim.remove_rfa", self.revision.limerick))

        for state in (LimStates.CONFIRMING, LimStates.APPROVED):
            self.revision.limerick.state = state
            self.revision.limerick.save()
            self.assertFalse(self.we.has_perm("lim.remove_rfa", self.revision.limerick))

    def test_clear_rfas(self):

        self.assertFalse(self.author.has_perm("lim.clear_rfas", self.revision.limerick))
        factories.RFAFactory(limerick=self.revision.limerick, author=self.we)
        self.assertFalse(self.we.has_perm("lim.clear_rfas", self.revision.limerick))
        self.assertFalse(self.rookie.has_perm("lim.clear_rfas", self.revision.limerick))
        self.assertTrue(self.author.has_perm("lim.clear_rfas", self.revision.limerick))

        for state in (LimStates.CONFIRMING, LimStates.APPROVED):
            self.revision.limerick.state = state
            self.revision.limerick.save()
            self.assertFalse(
                self.author.has_perm("lim.clear_rfas", self.revision.limerick)
            )
