import factory
from django.conf import settings
from django.db.models.signals import post_save
from django.utils import timezone

from lim.models import *

TEST_VERSE = """
This limerick's only a <b>test</b>.
It's not really one of my best.
I'm sorry it's boring.
It's used for exploring
This site -- as you've probably guessed."""

WORKSHOP_VERSE = """
To <b>workshop</b>, first you must see
A limerick something like me.
We will all rearrange it,
Convert it and change it.
Approval is when it's set free!
"""


@factory.django.mute_signals(post_save)
class AuthorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Author

    # We pass in profile=None to prevent UserFactory from creating another profile
    # (this disables the RelatedFactory)
    user = factory.SubFactory('lim.tests.mixins.UserFactory', profile=None)


@factory.django.mute_signals(post_save)
class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = settings.AUTH_USER_MODEL

    # We pass in 'user' to link the generated Profile to our just-generated User
    # This will call ProfileFactory(user=our_new_user), thus skipping the SubFactory.
    author = factory.RelatedFactory(AuthorFactory, 'user')

    username = factory.LazyAttribute(lambda obj: obj.first_name + '.' + obj.last_name)
    password = "password"
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.LazyAttribute(
        lambda a: '{0}.{1}@example.com'.format(a.first_name, a.last_name).lower()
    )


@factory.django.mute_signals(post_save)
class LimerickFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Limerick

    limerick_number = factory.Sequence(lambda n: n)
    original_id = factory.Sequence(lambda n: n)
    curtained = False
    state = LimStates.NEW


@factory.django.mute_signals(post_save)
class DefinedWordFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DefinedWord

    word_text = factory.Faker('word')


@factory.django.mute_signals(post_save)
class RevisionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Revision

    limerick = factory.SubFactory(LimerickFactory)
    primary_author = factory.SubFactory(UserFactory)
    secondary_author = None
    workshop_state = WorkshopStates.NONE
    order = 0
    updated_at = timezone.now()
    verse = TEST_VERSE

    legacy_verse_id = factory.Sequence(lambda n: n)


@factory.django.mute_signals(post_save)
class RevisionDefinedWordsThroughModelFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = RevisionDefinedWordsThroughModel

    revision = factory.SubFactory(RevisionFactory)
    defined_word = factory.SubFactory(DefinedWordFactory)
    order = factory.Sequence(lambda n: n)


class RevisionWithDefinedWordFactory(RevisionFactory):
    dw = factory.RelatedFactory(RevisionDefinedWordsThroughModelFactory, 'revision')


@factory.django.mute_signals(post_save)
class CommentFactory(factory.DjangoModelFactory):
    class Meta:
        model = Comment

    revision = factory.SubFactory(RevisionFactory)
    author = factory.SubFactory(UserFactory)
    date_time = datetime.datetime.now()


@factory.django.mute_signals(post_save)
class RFAFactory(factory.DjangoModelFactory):
    class Meta:
        model = RFA

    limerick = factory.SubFactory(LimerickFactory)
    author = factory.SubFactory(UserFactory)
