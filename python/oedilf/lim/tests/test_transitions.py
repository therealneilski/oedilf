import datetime

from django.test import TestCase
from django_fsm import TransitionNotAllowed

from lim.models import AuthorRanks, LimStates, RFA
from . import factories


class LimerickTransitionsTestCase(TestCase):
    def setUp(self):
        self.author = factories.UserFactory(author__author_rank=AuthorRanks.WE)
        self.wes = [
            factories.UserFactory(author__author_rank=AuthorRanks.WE) for _ in range(4)
        ]
        self.ae = factories.UserFactory(author__author_rank=AuthorRanks.AE)

    def test_new(self):
        lim = factories.LimerickFactory(primary_author=self.author)
        self.assertEqual(lim.state, LimStates.NEW)

        self.assertRaises(TransitionNotAllowed, lim.off_hold, self.author)
        self.assertRaises(TransitionNotAllowed, lim.set_to_approved, self.author)
        self.assertRaises(TransitionNotAllowed, lim.unapprove, self.author)
        self.assertRaises(TransitionNotAllowed, lim.set_to_confirming, self.ae)
        self.assertRaises(TransitionNotAllowed, lim.store, self.ae)

        for trans in lim.on_hold, lim.set_to_tentative, lim.set_to_revised, lim.exclude:
            self.assertTrue(trans, self.author)
            lim.state = LimStates.NEW

    def test_revised(self):
        lim = factories.LimerickFactory(
            primary_author=self.author, state=LimStates.REVISED
        )
        self.assertEqual(lim.state, LimStates.REVISED)

        self.assertRaises(TransitionNotAllowed, lim.off_hold, self.author)
        self.assertRaises(TransitionNotAllowed, lim.set_to_approved, self.author)
        self.assertRaises(TransitionNotAllowed, lim.unapprove, self.author)
        self.assertRaises(TransitionNotAllowed, lim.set_to_confirming, self.ae)
        self.assertRaises(TransitionNotAllowed, lim.store, self.ae)

        for trans in lim.on_hold, lim.set_to_tentative, lim.exclude:
            self.assertTrue(trans, self.author)
            lim.state = LimStates.REVISED

    def test_tentative(self):
        lim = factories.LimerickFactory(
            primary_author=self.author, state=LimStates.TENTATIVE
        )
        self.assertEqual(lim.state, LimStates.TENTATIVE)

        self.assertRaises(TransitionNotAllowed, lim.off_hold, self.author)
        self.assertRaises(TransitionNotAllowed, lim.set_to_approved, self.author)
        self.assertRaises(TransitionNotAllowed, lim.unapprove, self.author)
        self.assertRaises(TransitionNotAllowed, lim.set_to_confirming, self.ae)
        self.assertRaises(TransitionNotAllowed, lim.store, self.ae)

        for trans in lim.on_hold, lim.set_to_revised, lim.exclude:
            self.assertTrue(trans, self.author)
            lim.state = LimStates.TENTATIVE

    def test_confirming(self):
        lim = factories.LimerickFactory(
            primary_author=self.author, state=LimStates.TENTATIVE
        )
        for we in self.wes:
            RFA(limerick=lim, author=we)

        self.assertRaises(TransitionNotAllowed, lim.set_to_confirming, self.author)
        self.assertRaises(TransitionNotAllowed, lim.set_to_confirming, self.ae)

        RFA(limerick=lim, author=self.author)

        self.assertRaises(TransitionNotAllowed, lim.set_to_confirming, self.author)
        self.assertTrue(lim.set_to_confirming, self.ae)

    def test_approved(self):
        lim = factories.LimerickFactory(primary_author=self.author)
        for we in self.wes + [self.author]:
            RFA(limerick=lim, author=we)
        lim.state = LimStates.CONFIRMING
        lim.state_date = datetime.datetime.now() - datetime.timedelta(days=8)
        lim.add_author_approval(self.author)
        self.assertTrue(lim.set_to_approved(self.author))
        lim.save()

        for trans in (
            lim.on_hold,
            lim.off_hold,
            lim.set_to_tentative,
            lim.set_to_approved,
            lim.set_to_confirming,
        ):
            self.assertRaises(TransitionNotAllowed, trans, self.ae)
        self.assertRaises(TransitionNotAllowed, lim.set_to_revised, self.author)

        for trans in (lim.store, lim.exclude):
            self.assertTrue(trans, self.ae)
            lim.state = LimStates.APPROVED

    def test_force_approved(self):
        lim = factories.LimerickFactory(primary_author=self.author)
        for we in self.wes + [self.author]:
            RFA(limerick=lim, author=we)
        lim.state = LimStates.CONFIRMING
        lim.state_date = datetime.datetime.now() - datetime.timedelta(days=8)
        self.assertTrue(lim.force_final_approval(self.ae))
        lim.save()

        for trans in (
            lim.on_hold,
            lim.off_hold,
            lim.set_to_tentative,
            lim.set_to_approved,
            lim.set_to_confirming,
        ):
            self.assertRaises(TransitionNotAllowed, trans, self.ae)
        self.assertRaises(TransitionNotAllowed, lim.set_to_revised, self.author)

        for trans in (lim.store, lim.exclude):
            self.assertTrue(trans, self.ae)
            lim.state = LimStates.APPROVED

    def test_stored(self):
        lim = factories.LimerickFactory(
            primary_author=self.author, state=LimStates.STORED
        )

        for trans in (
            lim.on_hold,
            lim.off_hold,
            lim.set_to_revised,
            lim.set_to_approved,
            lim.set_to_confirming,
            lim.store,
        ):
            self.assertRaises(TransitionNotAllowed, trans, self.ae)
        for trans in (lim.set_to_tentative, lim.exclude, lim.unapprove):
            self.assertTrue(trans, self.ae)
            lim.state = LimStates.STORED

    def test_exclude(self):
        lim = factories.LimerickFactory(primary_author=self.author)
        # noinspection PyTypeChecker
        for state in LimStates:
            lim.state = state
            self.assertTrue(lim.exclude, self.ae)

        lim.state = LimStates.EXCLUDED
        for trans in (
            lim.on_hold,
            lim.off_hold,
            lim.set_to_revised,
            lim.set_to_approved,
            lim.set_to_confirming,
            lim.store,
        ):
            self.assertRaises(TransitionNotAllowed, trans, self.ae)
        for trans in (lim.set_to_tentative, lim.unapprove):
            self.assertTrue(trans, self.ae)
