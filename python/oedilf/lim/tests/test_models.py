from django.conf import settings
from django.test import TestCase

from lim.models import Revision
from lim.tests import factories


class LimerickTester(TestCase):
    def test_environment(self):
        import os

        self.assertIsNotNone(os.getenv("TESTING", None))
        try:
            settings.__getattr__("HAYSTACK_SIGNAL_PROCESSOR")
        except AttributeError:
            pass
        else:
            self.fail("Did not raise AttributeError")

    def test_user(self):
        author = factories.UserFactory().author
        self.assertIsNotNone(author)
        self.assertIsNotNone(author.user)
        self.assertNotEqual(author.user.username, "")

    def test_defined_words(self):
        revision: Revision = factories.RevisionWithDefinedWordFactory()
        self.assertNotEqual(revision.formatted_defined_words, "")

    def test_limerick(self):
        revision: Revision = factories.RevisionWithDefinedWordFactory()
