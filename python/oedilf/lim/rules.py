import rules

from lim.models import *


@rules.predicate
def is_limerick_author(user, limerick_or_revision):
    if isinstance(limerick_or_revision, Limerick):
        return is_limerick_author(user, limerick_or_revision.latest_revision)

    if limerick_or_revision is None:
        return False

    return limerick_or_revision.primary_author == user or (
        limerick_or_revision.secondary_author is not None
        and limerick_or_revision.secondary_author == user
    )


@rules.predicate
def is_site_editor(user):
    return user.is_authenticated and (
        user.is_superuser or user.author.author_rank == AuthorRanks.EIC
    )


@rules.predicate
def is_approving_editor(user):
    return user.is_authenticated and (
        user.is_superuser
        or user.author.author_rank in (AuthorRanks.AE, AuthorRanks.EIC)
    )


@rules.predicate
def is_workshopping_editor(user):
    return user.is_authenticated and user.author.author_rank not in (
        AuthorRanks.R,
        AuthorRanks.C,
        AuthorRanks.CE,
    )


@rules.predicate
def can_view_limerick(user, limerick):
    if limerick is None:
        return False
    if not user.is_authenticated:
        if limerick.curtained or limerick.state != LimStates.APPROVED:
            return False

    else:
        if is_limerick_author(user, limerick):
            return True

        if limerick.curtained and (
            user.author.curtain_filtering or user.author.junior_mode
        ):
            return False

        if (
            limerick.state != LimStates.APPROVED
            and user.author.author_rank == AuthorRanks.R
        ):
            return False

    return True


@rules.predicate
def can_add_limerick(user):
    return user.is_authenticated and user.author.author_type not in (
        AuthorTypes.BANNED,
    )


@rules.predicate
def can_view_revision(user, revision):
    if revision is None:
        return False
    if not user.is_authenticated:
        if revision.limerick.curtained or not revision.is_latest_revision:
            return False

    else:
        return can_view_limerick(revision.limerick)


@rules.predicate
def can_add_revision(user, limerick):
    if not user.is_authenticated:
        return False

    if is_approving_editor(user):
        return True

    if not is_limerick_author(user, limerick):
        return False

    if limerick.state not in (LimStates.APPROVED, LimStates.STORED, LimStates.EXCLUDED):
        return True

    return False


@rules.predicate
def can_comment(user, limerick):
    if is_limerick_author(user, limerick):
        return True

    if (
        user.is_authenticated
        and user.author.author_rank != AuthorRanks.R
        and user.author.author_rank != AuthorRanks.C
    ):
        return True

    else:
        return False


@rules.predicate
def can_edit_delete_comment(user, comment):
    return comment.author == user and comment.revision.is_latest_revision


@rules.predicate
def can_add_rfa(user, limerick):
    if limerick.rfa_set.filter(author=user).exists():
        return False

    if limerick.state in (
        LimStates.APPROVED,
        LimStates.HELD,
        LimStates.EXCLUDED,
        LimStates.STORED,
    ):
        return False

    if is_limerick_author(user, limerick):
        return limerick.rfa_set.count() >= 4

    if not can_comment(user, limerick):
        return False

    return True


@rules.predicate
def can_remove_rfa(user, limerick):
    if (
        not can_comment(user, limerick)
        or not limerick.rfa_set.filter(author=user).exists()
        or limerick.state in (LimStates.APPROVED, LimStates.CONFIRMING)
    ):
        return False

    else:
        return True


@rules.predicate
def can_clear_rfas(user, limerick):
    return (
        limerick.rfa_set.count() > 0
        and limerick.state not in (LimStates.APPROVED, LimStates.CONFIRMING)
        and (is_limerick_author(user, limerick) or is_approving_editor(user))
    )


@rules.predicate
def can_manage_profile(user, target_user):
    return user.is_authenticated and target_user == user


@rules.predicate
def can_hold(user, limerick):
    if (
        is_workshopping_editor(user) or is_limerick_author(user, limerick)
    ) and limerick.state not in (
        LimStates.APPROVED,
        LimStates.STORED,
        LimStates.EXCLUDED,
        LimStates.HELD,
        LimStates.CONFIRMING,
        LimStates.OBSOLETE,
    ):
        return True

    else:
        return False


@rules.predicate
def can_unhold(user, limerick):
    """
    You can unhold a limerick if:
    - you are an author of the limerick, and one of the authors put it on hold; or
    - you are not an author of the limerick, but you were the one who put it on hold.
    :param user: 
    :param limerick: 
    :return: 
    """
    if limerick.state == LimStates.HELD and (
        (
            is_limerick_author(user, limerick)
            and is_limerick_author(limerick.held_by.user, limerick)
        )
        or (is_workshopping_editor(user) and user == limerick.held_by.user)
    ):
        return True

    else:
        return False


@rules.predicate
def can_add_curtain(user, limerick):
    if not limerick.curtained and (
        is_limerick_author(user, limerick) or is_workshopping_editor(user)
    ):
        return True

    else:
        return False


@rules.predicate
def can_remove_curtain(user, limerick):
    if limerick.curtained and (
        is_limerick_author(user, limerick) or is_workshopping_editor(user)
    ):
        return True

    else:
        return False


@rules.predicate
def can_set_to_confirming(user, limerick):
    return (
        user.is_authenticated
        and is_approving_editor(user)
        and can_add_rfa(user, limerick)
        and limerick.is_eligible_for_stc
    )


@rules.predicate
def can_stop_countdown(user, limerick):
    return (
        user.is_authenticated
        and limerick.is_confirming
        and not limerick.confirmation_countdown_ended
        and is_workshopping_editor(user)
    )


@rules.predicate
def can_send_lace(user, limerick):
    return (
        user.is_authenticated
        and limerick.is_confirming
        and limerick.confirmation_countdown_ended
        and user not in limerick.authors
        and is_site_editor(user)
    )


@rules.predicate
def can_add_final_approval(user, limerick):
    return (
        user.is_authenticated
        and limerick.is_confirming
        and limerick.confirmation_countdown_ended
        and user in limerick.authors
        and not approval_given(user, limerick)
    )


def approval_given(user, limerick):
    if limerick.primary_author == user:
        return limerick.primary_author_approved

    elif limerick.secondary_author == user:
        return limerick.secondary_author_approved

    else:
        return False


def awaiting_coauthor_approval(user, limerick):
    if (
        limerick.secondary_author is None
        or limerick.secondary_author.username == "Workshop"
        or user not in limerick.authors
    ):
        return False

    else:
        if limerick.primary_author == user:
            coauthor_approval_given = limerick.secondary_author_approved
        else:
            coauthor_approval_given = limerick.primary_author_approved
        return (
            user.is_authenticated
            and limerick.is_confirming
            and limerick.confirmation_countdown_ended
            and not coauthor_approval_given
        )


@rules.predicate
def can_force_final_approval(user, limerick):
    return (
        user.is_authenticated
        and limerick.is_confirming
        and limerick.confirmation_countdown_ended
        and is_site_editor(user)
    )


@rules.predicate
def can_manage_topics(user, limerick):
    return user.is_authenticated and (
        user in limerick.authors or is_approving_editor(user)
    )


@rules.predicate
def can_manage_projects(user):
    return is_approving_editor(user)


@rules.predicate
def is_authenticated(user):
    return user.is_authenticated


@rules.predicate
def can_call_for_attention(user, limerick):
    return is_workshopping_editor(user) or is_limerick_author(user, limerick)


@rules.predicate
def can_manage_pinax_announcements(user):
    return is_approving_editor(user)


@rules.predicate
def can_impersonate(user):
    return is_site_editor(user)


def request_can_impersonate(request):
    return request.user.has_perm("lim.can_impersonate")


rules.add_perm("lim.is_author", is_limerick_author)
rules.add_perm("lim.is_site_editor", is_site_editor)
rules.add_perm("lim.is_approving_editor", is_approving_editor)
rules.add_perm("lim.is_workshopping_editor", is_workshopping_editor)
rules.add_perm("lim.add_limerick", can_add_limerick)
rules.add_perm("lim.view_limerick", can_view_limerick)
rules.add_perm("lim.view_revision", can_view_revision)
rules.add_perm("lim.add_revision", can_add_revision)
rules.add_perm("lim.add_comment", can_comment)
rules.add_perm("lim.change_comment", can_edit_delete_comment)
rules.add_perm("lim.delete_comment", can_edit_delete_comment)
rules.add_perm("lim.manage_profile", can_manage_profile)
rules.add_perm("lim.add_rfa", can_add_rfa)
rules.add_perm("lim.clear_rfas", can_clear_rfas)
rules.add_perm("lim.remove_rfa", can_remove_rfa)
rules.add_perm("lim.set_to_confirming", can_set_to_confirming)
rules.add_perm("lim.send_lace", can_send_lace)
rules.add_perm("lim.stop_countdown", can_stop_countdown)
rules.add_perm("lim.set_to_approved", can_add_final_approval)
rules.add_perm("lim.awaiting_coauthor_approval", awaiting_coauthor_approval)
rules.add_perm("lim.force_final_approval", can_force_final_approval)
rules.add_perm("lim.hold", can_hold)
rules.add_perm("lim.unhold", can_unhold)
rules.add_perm("lim.add_curtain", can_add_curtain)
rules.add_perm("lim.remove_curtain", can_remove_curtain)
rules.add_perm("lim.manage_topics", can_manage_topics)
rules.add_perm("lim.manage_projects", can_manage_projects)
rules.add_perm("lim.manage_tags", is_workshopping_editor)
rules.add_perm("lim.is_authenticated", is_authenticated)
rules.add_perm("lim.can_view_admin", is_site_editor)
rules.add_perm("lim.call_for_attention", can_call_for_attention)
rules.add_perm("announcements.can_manage", can_manage_pinax_announcements)
rules.add_perm("lim.can_impersonate", can_impersonate)
