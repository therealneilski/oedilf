from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.views.generic.base import RedirectView

import lim.views.email_view
import lim.views.limerick_view
from lim.feeds import feeds
from . import views, api

# Serializers define the API representation.


# ViewSets define the view behavior.


urlpatterns = [
    path("", views.HomeOrActivityView.as_view(), name="site-root"),
    path("home", views.HomeView.as_view(), name="home"),
    path("limericks/<limerick_id>/", views.LimerickView.as_view(), name="limerick"),
    path(
        "ajax/limericks/<limerick_id>/",
        views.AjaxLimerickView.as_view(),
        name="ajax-limerick",
    ),
    path(
        "limericks/<limerick_id>/workshop/",
        views.WorkshopView.as_view(),
        name="workshop",
    ),
    path(
        "ajax/revisions/<revision_id>/workshop/",
        views.AjaxRevisionView.as_view(),
        name="revision-workshop",
    ),
    path(
        "ajax/revisions/<revision_id>/approvers/",
        views.RevisionApproversView.as_view(),
        name="revision-approvers",
    ),
    path(
        "limericks/<limerick_id>/add-revision/",
        views.RevisionView.as_view(),
        name="add-revision",
    ),
    path("add-limerick/", views.LimerickAddView.as_view(), name="add-limerick"),
    path(
        "limericks/<limerick_id>/set-to-confirming/",
        views.LimerickSTCView.as_view(),
        name="set-to-confirming",
    ),
    path(
        "limericks/<limerick_id>/stop-countdown/",
        views.LimerickStopCountdownView.as_view(),
        name="limerick-stop-countdown",
    ),
    path(
        "limericks/<limerick_id>/author-approval/",
        views.LimerickAuthorApprovalView.as_view(),
        name="limerick-author-approval",
    ),
    path(
        "limericks/<limerick_id>/author-rejection/",
        views.LimerickAuthorRejectionView.as_view(),
        name="limerick-author-rejection",
    ),
    path(
        "limericks/<limerick_id>/force-final-approval/",
        views.LimerickForceFinalApprovalView.as_view(),
        name="limerick-force-final-approval",
    ),
    path(
        "limericks/<limerick_id>/resend-lace/",
        lim.views.email_view.LimerickResendLACEView.as_view(),
        name="limerick-resend-lace",
    ),
    path(
        "ajax/comments/",
        views.CommentCreateAjaxView.as_view(),
        name="comment-create-ajax",
    ),
    path("ajax/comments/<pk>/", views.CommentAjaxView.as_view(), name="comment-ajax"),
    path(
        "ajax/comment-form/<limerick_id>/",
        views.CommentAddFormView.as_view(),
        name="comment-create-form",
    ),
    path(
        "ajax/limericks/<pk>/topic-badges/",
        lim.views.limerick_view.TopicBadgeAjaxView.as_view(),
        name="ajax-limerick-topic-badges",
    ),
    path(
        "comments/<pk>/update/",
        views.CommentUpdateView.as_view(),
        name="comment-update",
    ),
    path(
        "comments/<pk>/delete/",
        views.CommentDeleteView.as_view(),
        name="comment-delete",
    ),
    path("comments/<pk>/", views.CommentView.as_view(), name="comment-div"),
    path(
        "ajax/limericks/<limerick_id>/rfas/",
        views.ClearRFAsView.as_view(),
        name="clear-rfas",
    ),
    path("authors/", views.AuthorListDatatableView.as_view(), name="authors"),
    path(
        "authors/datatable/view/",
        views.AuthorListDatatableDataView.as_view(),
        name="authors-list-json",
    ),
    path(
        "authors/<author>/limericks/",
        views.AuthorListView.as_view(),
        name="limericks-by-author",
    ),
    path(
        "authors/<author>/limericks-with-toc/",
        views.AuthorListWithTOCJsonView.as_view(),
        name="limericks-by-author-with-toc",
    ),
    path("limericks/", views.LimerickListView.as_view(), name="limericks-list"),
    path(
        "limericks-with-toc/",
        views.LimerickListWithTOCJsonView.as_view(),
        name="limericks-list-with-toc",
    ),
    path(
        "topics/<topic_pk>/limericks/",
        views.TopicListView.as_view(),
        name="limericks-by-topic",
    ),
    path(
        "ajax/topics/<topic_pk>/limericks",
        views.TopicListWithTOCJsonView.as_view(),
        name="ajax-limerick-by-topic-with-toc",
    ),
    path("authors/<author>/", views.AuthorProfileView.as_view(), name="author-profile"),
    path(
        "authors/<author>/manage-profile/",
        views.AuthorProfileManageUpdate.as_view(),
        name="manage-author-profile",
    ),
    path(
        "authors/<author>/showcase/",
        views.AuthorShowcaseView.as_view(),
        name="author-showcase",
    ),
    path(
        "authors/<author>/showcase-toc/",
        views.AuthorShowcaseTOCView.as_view(),
        name="author-showcase-toc",
    ),
    path(
        "authors/<author>/showcase/manage/",
        views.ManageShowcaseView.as_view(),
        name="manage-showcase",
    ),
    path("showcases/", views.AuthorShowcasesDatatableView.as_view(), name="showcases"),
    path(
        "showcases/datatable/view/",
        views.AuthorShowcasesDatatableDataView.as_view(),
        name="showcases-json",
    ),
    path("search/", views.LimerickSearchView.as_view(), name="search"),
    path(
        "ajax/search-results",
        views.LimerickSearchResultsViewAjax.as_view(),
        name="limericks-search-results-ajax",
    ),
    path(
        "definitions/<str:word>/",
        views.DefinitionListView.as_view(),
        name="limericks-by-definition",
    ),
    path(
        "definitions/<str:word>/toc/",
        views.DefinitionListWithTOCJsonView.as_view(),
        name="limericks-by-definition-with-toc",
    ),
    path(
        "ajax/activity/",
        views.NotificationsCardView.as_view(),
        name="notifications-card",
    ),
    path(
        "stc/list-lace-eligible/",
        views.ListLACEView.as_view(),
        name="stc-list-lace-eligible",
    ),
    path(
        "stc/send-first-lace/",
        views.SendFirstLACEView.as_view(),
        name="stc-send-first-lace",
    ),
    path(
        "stc/send-followup-lace/",
        views.SendFollowupLACEView.as_view(),
        name="stc-send-followup-lace",
    ),
    path(
        "activity/<pk>/remove/",
        views.RemoveActivityWorkshopView.as_view(),
        name="remove-activity",
    ),
    path("grab/<str:grab_type>/", views.GrabView.as_view(), name="grab"),
    path("grab/", views.GrabNothingView.as_view(), name="grab-nothing"),
    path(
        "grab/<str:grab_type>/exhausted/",
        views.GrabNothingView.as_view(),
        name="grab-exhausted",
    ),
    path(
        "project/<str:project_abbrev>/author/<author>/add",
        views.AddAuthorToProjectView.as_view(),
        name="add-author-to-project",
    ),
    path(
        "ajax/project/<str:project_abbrev>/search/add",
        views.AddSearchToProjectAjaxView.as_view(),
        name="add-search-to-project",
    ),
    path(
        "ajax/limerick/<pk>/toggle-watching",
        views.ToggleWatchLimerickAjaxView.as_view(),
        name="toggle-watching",
    ),
    path(
        "stc/list-stc-eligible/", views.STCListView.as_view(), name="list-stc-eligible"
    ),
    path(
        "words/not-done/", views.RequiredWordListView.as_view(), name="words-not-done"
    ),
    path(
        "words/random/",
        views.RandomRequiredWordView.as_view(),
        name="random-words-not-done",
    ),
    path(
        "words/summary/",
        views.DefinedWordPrefixSummaryView.as_view(),
        name="words-summary-by-prefix",
    ),
    path("tags/", views.TagListView.as_view(), name="personal-tags"),
    path(
        "tags/<tag_id>/", views.TagLimerickListView.as_view(), name="limericks-by-tag"
    ),
    path(
        "tags/<tag_id>/limericks-with-toc",
        views.TagListWithTOCJsonView.as_view(),
        name="limericks-by-tag-with-toc",
    ),
    path(
        "ajax/limericks/<pk>/tag-badges/",
        lim.views.limerick_view.TagBadgeAjaxView.as_view(),
        name="ajax-limerick-tag-badges",
    ),
    path("rfa-table/", views.RfaTableView.as_view(), name="rfa-table"),
    path(
        "rfa-table/datatable/view",
        views.RfaTableDataView.as_view(),
        name="rfa-table-json",
    ),
    path(
        "confirming-table/",
        views.ConfirmingTableView.as_view(),
        name="confirming-table",
    ),
    path(
        "confirming-table/datatable/view",
        views.ConfirmingTableDataView.as_view(),
        name="confirming-table-json",
    ),
    path("activity/", views.ActivityTableView.as_view(), name="activity"),
    path(
        "activity/datatable/view/",
        views.ActivityTableDataView.as_view(),
        name="activity-table-json",
    ),
    path(
        "rss/filtered.xml", feeds.FilteredLimericksRSSFeed(), name="feed-filtered-rss"
    ),
    path(
        "rss/unfiltered.xml",
        feeds.UnfilteredLimericksRSSFeed(),
        name="feed-unfiltered-rss",
    ),
    path(
        "atom/filtered.xml",
        feeds.FilteredLimericksAtomFeed(),
        name="feed-filtered-atom",
    ),
    path(
        "atom/unfiltered.xml",
        feeds.UnfilteredLimericksAtomFeed(),
        name="feed-unfiltered-atom",
    ),
    path("rss/authors/<author_pk>.xml", feeds.AuthorRSSFeed(), name="feed-author-rss"),
    path(
        "atom/authors/<author_pk>.xml", feeds.AuthorAtomFeed(), name="feed-author-atom"
    ),
    path(
        "db/feeds/rssfiltered.xml",
        RedirectView.as_view(url="/rss/filtered.xml", permanent=True),
    ),
    path(
        "db/feeds/rss.xml",
        RedirectView.as_view(url="/rss/unfiltered.xml", permanent=True),
    ),
    path(
        "db/feeds/Author<user_pk>rss.xml", feeds.LegacyAuthorRSSRedirectView.as_view()
    ),
    path(
        "rss/announcements.xml", feeds.AnnouncementsRSSFeed(), name="feed-announcements"
    ),
    # user-to-user messages
    path(
        "user-message/to/<author_pk>",
        views.NewUserMessageView.as_view(),
        name="user-message-new",
    ),
    path(
        "user-message/<message_pk>",
        views.ReplyUserMessageView.as_view(),
        name="user-message-reply",
    ),
    # furniture
    path("api/", include(api.router.urls)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
