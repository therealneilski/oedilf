from datetime import datetime

from django.contrib import messages
from django.core.mail import get_connection, send_mail
from django.db.models import Count, Q, Max
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.timezone import now, timedelta
from django.views.generic import TemplateView, RedirectView
from django.views.generic.detail import SingleObjectMixin
from rules.contrib.views import PermissionRequiredMixin

from lim.models import Limerick, Comment, LimStates, SiteSettings


class LimerickResendLACEView(PermissionRequiredMixin, SingleObjectMixin, TemplateView):

    template_name = "lim/workshop.html"
    permission_required = "lim.send_lace"
    model = Limerick
    pk_url_kwarg = "limerick_id"

    def get_permission_object(self):
        return self.get_object()

    def handle_no_permission(self):
        messages.error(self.request, "You may not send LACEs")
        return super().handle_no_permission()

    def get(self, request, *args, **kwargs):
        limerick = self.get_object()
        lace_messages = limerick.get_lace_mails(request)
        for m in lace_messages:
            send_mail(m)
        comment = Comment()
        comment.author = self.request.user
        comment.message = "{} Limerick Approval Confirmation Email to {}".format(
            "Sent" if limerick.lace_set.count() == 1 else "Resent",
            limerick.latest_revision.author_names,
        )
        comment.revision = limerick.latest_revision
        comment.date_time = datetime.now()
        comment.save()
        messages.success(self.request, comment.message)
        return HttpResponseRedirect(
            reverse("workshop", kwargs={"limerick_id": limerick.id})
        )

    @property
    def object(self):
        return self.get_object()


class ListLACEView(PermissionRequiredMixin, TemplateView):
    """
    List all the limericks which are eligible to have a LACE sent.
    """

    template_name = "lim/stc/list-lace-eligible.html"
    permission_required = "lim.is_approving_editor"

    def handle_no_permission(self):
        messages.error(self.request, "You may not send LACEs")
        return super().handle_no_permission()

    def get_context_data(self, **kwargs):
        context = super(ListLACEView, self).get_context_data(**kwargs)
        last_state_date = now() - timedelta(days=SiteSettings.get_solo().countdown_days)
        confirming_limericks = (
            Limerick.objects.filter(
                Q(state=LimStates.CONFIRMING) & Q(state_date__lte=last_state_date)
            )
            .annotate(lace_count=Count("lace"))
            .annotate(latest_lace_date=Max("lace__date"))
            .order_by("lace_count", "pk")
        )

        context["limericks"] = confirming_limericks
        context["first_lace"] = confirming_limericks.filter(lace_count=0).order_by(
            "state_date"
        )
        lace_resend_days = now() - timedelta(SiteSettings.get_solo().lace_resend_days)
        context["followup_lace"] = confirming_limericks.filter(
            lace_count__gte=1, latest_lace_date__lte=lace_resend_days
        ).order_by("latest_lace_date")
        context["lace_sent"] = confirming_limericks.filter(
            lace_count__gte=1, latest_lace_date__gt=lace_resend_days
        ).order_by("latest_lace_date")
        return context


class SendFirstLACEView(PermissionRequiredMixin, RedirectView):

    permission_required = "lim.is_approving_editor"
    pattern_name = "stc-list-lace-eligible"

    def get_permission_object(self):
        return self.request.user

    def handle_no_permission(self):
        messages.error(self.request, "You may not send LACEs")
        return super().handle_no_permission()

    def can_send_lace(self, limerick):
        return limerick.is_eligible_for_first_lace

    def get(self, request, *args, **kwargs):
        lace_mails = []
        for limerick in Limerick.objects.filter(state=LimStates.CONFIRMING):
            if self.can_send_lace(limerick):
                lace_mails.extend(limerick.get_lace_mails(self.request))
        if len(lace_mails) == 0:
            messages.warning(
                self.request, "No suitable limericks found to send LACEs to"
            )
        else:
            mailer = get_connection()
            mailer.send_messages(lace_mails)
            messages.success(
                self.request,
                "Sent {} Limerick Approval Confirmation Email{}".format(
                    len(lace_mails), "" if len(lace_mails) == 1 else "s"
                ),
            )
        return super(SendFirstLACEView, self).get(request, *args, **kwargs)


class SendFollowupLACEView(SendFirstLACEView):
    def can_send_lace(self, limerick):
        return limerick.is_eligible_for_followup_lace
