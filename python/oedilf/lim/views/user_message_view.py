from django.views.generic.edit import CreateView
from django.shortcuts import get_object_or_404, redirect, reverse
from django.contrib import messages

from lim.forms import NewUserMessageForm, ReplyUserMessageForm
from lim.models import ActivityMessage, Author


class UserMessageMixin:

    template_name = "lim/user-message.html"

    model = ActivityMessage

    def form_valid(self, form):
        super(UserMessageMixin, self).form_valid(form)
        messages.info(
            self.request,
            "Sent message to {}".format(form.cleaned_data["dest"].username),
        )
        return redirect(reverse("activity"))

    def form_invalid(self, form):
        pass

    def get_success_url(self):
        return reverse("activity")


class NewUserMessageView(UserMessageMixin, CreateView):

    form_class = NewUserMessageForm

    def get_form_kwargs(self):
        kwargs = super(NewUserMessageView, self).get_form_kwargs()
        kwargs["request"] = self.request
        kwargs["destination_author"] = get_object_or_404(
            Author, pk=self.kwargs["author_pk"]
        )
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(NewUserMessageView, self).get_context_data(**kwargs)
        context["destination_author"] = get_object_or_404(
            Author, pk=self.kwargs["author_pk"]
        )
        return context


class ReplyUserMessageView(UserMessageMixin, CreateView):

    form_class = ReplyUserMessageForm

    def get_context_data(self, **kwargs):
        context = super(ReplyUserMessageView, self).get_context_data(**kwargs)
        context["message"] = get_object_or_404(
            ActivityMessage, pk=self.kwargs["message_pk"]
        )
        context["destination_author"] = context["message"].source
        return context

    def get_form_kwargs(self):
        kwargs = super(ReplyUserMessageView, self).get_form_kwargs()
        kwargs["request"] = self.request
        kwargs["destination_author"] = get_object_or_404(
            ActivityMessage, pk=self.kwargs["message_pk"]
        ).author
        kwargs["in_reply_to"] = self.kwargs["message_pk"]
        return kwargs
