from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView

from lim.models import LimStates, Author


class UserIsAuthorMixin(UserPassesTestMixin):
    # noinspection PyUnresolvedReferences
    @property
    def author_pk(self):
        if 'author' in self.kwargs:
            author_pk = self.kwargs['author']
        else:
            author_pk = self.request.GET.get('author_pk', None)
        return author_pk

    # noinspection PyUnresolvedReferences
    def test_func(self):
        return self.request.user.author.pk == self.author_pk


class ManageShowcaseView(UserIsAuthorMixin, TemplateView):
    template_name = 'lim/manage-showcase.html'
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(object_list=[])
        page = self.kwargs.get('page', self.request.GET.get('page', 1))
        author = get_object_or_404(Author, pk=self.author_pk)
        context['author_pk'] = self.author_pk
        context['author'] = author

        context['request'] = self.request
        context['title'] = 'Manage showcase'
        context['page'] = page

        showcase = author.user.showcase_set.filter(
            limerick__state=LimStates.APPROVED
        ).order_by('order')
        context['showcase'] = showcase

        return context
