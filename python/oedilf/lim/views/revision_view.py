from crispy_forms.utils import render_crispy_form
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.template.context_processors import csrf
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from rules.contrib.views import PermissionRequiredMixin

from lim.forms import RevisionForm
from lim.models import *
from lim.utils import clean_trumbowyg


class RevisionApproversView(TemplateView):
    template_name = "lim/inclusion/revision-approvers.html"

    def get_context_data(self, **kwargs):
        context = super(RevisionApproversView, self).get_context_data(**kwargs)
        context["revision"] = get_object_or_404(Revision, pk=kwargs.pop("revision_id"))
        return context


class RevisionView(PermissionRequiredMixin, FormView):
    permission_required = "lim.add_revision"
    template_name = "lim/add-revision.html"
    model = Revision
    form_class = RevisionForm

    fields = [
        "limerick_id",
        "verse",
        "author_notes",
        "editor_notes",
        "workshop_comment",
        "all_defined_words",
    ]

    def __init__(self):
        super(RevisionView, self).__init__()
        self.form = None
        self._limerick_id = None

    def get_permission_object(self):
        return get_object_or_404(Limerick, pk=self.limerick_id)

    @property
    def limerick_id(self):
        if self._limerick_id is None:
            self._limerick_id = self.kwargs.pop("limerick_id")
        return self._limerick_id

    def get_form_kwargs(self):
        kwargs = super(RevisionView, self).get_form_kwargs()
        kwargs["limerick_id"] = self.limerick_id
        kwargs["request"] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        limerick_id = self.limerick_id
        limerick = get_object_or_404(Limerick, pk=limerick_id)
        form = RevisionForm(
            instance=limerick.latest_revision,
            request=self.request,
            initial={
                "all_defined_words": limerick.all_defined_words,
                "primary_author": limerick.primary_author,
                "secondary_author": limerick.secondary_author,
            },
        )
        context = super(RevisionView, self).get_context_data(**kwargs)
        context["limerick"] = limerick
        context["revision_form"] = form
        return context

    def form_valid(self, form):
        limerick = get_object_or_404(Limerick, pk=form.cleaned_data["limerick_id"])
        if not self.request.user.has_perm("lim.add_revision", limerick):
            raise PermissionError(
                '"{}" does not have permission to add a revision to this limerick'.format(
                    self.request.user.username
                )
            )

        revision = self.create_revision(form, limerick)
        if limerick.state in (LimStates.NEW, LimStates.REVISED, LimStates.TENTATIVE):
            limerick.state = LimStates.REVISED
            limerick.save()
        Comment.objects.create(
            author=self.request.user,
            message=form.cleaned_data["workshop_comment"],
            revision=revision,
            action=CommentActions.ADD_REVISION,
            date_time=datetime.datetime.now(),
        )
        self.success_url = reverse("workshop", kwargs={"limerick_id": limerick.pk})
        if self.request.is_ajax():
            response = {"success": True, "redirect_url": self.success_url}
            return JsonResponse(response)

        else:
            return super(RevisionView, self).form_valid(form)

    def create_revision(self, form, limerick):
        if limerick.revision_set:
            old_latest_revision = limerick.latest_revision
        else:
            old_latest_revision = None

        revision = Revision(limerick=limerick, workshop_state=WorkshopStates.NONE)
        revision.version = limerick.revision_set.count()

        revision.primary_author = form.cleaned_data["primary_author"]
        revision.secondary_author = form.cleaned_data["secondary_author"]
        revision.verse = clean_trumbowyg(form.cleaned_data["verse"])
        if form.cleaned_data["author_notes"]:
            revision.author_notes = clean_trumbowyg(form.cleaned_data["author_notes"])
        if form.cleaned_data["editor_notes"]:
            revision.editor_notes = clean_trumbowyg(form.cleaned_data["editor_notes"])
        revision.updated_at = now()
        revision.is_latest_revision = True
        revision.rfa_count = limerick.rfa_set.count()
        revision.save()  # this gives revision a pk for the dw's to hook on to
        dws = set()
        throughs = []
        for seq, word in enumerate(
            form.cleaned_data["all_defined_words"].split("\r\n")
        ):
            dw, created = DefinedWord.objects.get_or_create(word_text=word)
            dws.add(dw)
            throughs.append(
                RevisionDefinedWordsThroughModel(
                    revision=revision, defined_word=dw, order=len(throughs)
                )
            )
        RevisionDefinedWordsThroughModel.objects.bulk_create(throughs)

        if old_latest_revision is not None:
            old_latest_revision.is_latest_revision = False
            old_latest_revision.save()
        return revision

    def form_invalid(self, form):
        limerick = get_object_or_404(Limerick, pk=form.data["limerick_id"])
        self.success_url = reverse("workshop", kwargs={"limerick_id": limerick.pk})
        self.extra_context = {
            "limerick": limerick,
            "add_form": form,
            "request": self.request,
            "status": "failure",
        }
        if self.request.is_ajax():
            ctx = self.get_context_data()
            ctx.update(self.extra_context)
            ctx.update(csrf(self.request))
            ctx["request"] = self.request
            form.request = self.request
            # ensure trumbowyg doesn't double-div every line
            form.data = form.cleaned_data
            form.setup_helper()
            crispy_form = render_crispy_form(form, context=ctx)
            response = {
                "success": False,
                "status": 400,
                "form_html": crispy_form,
                "primary_author": {
                    "id": form.data["primary_author"].pk,
                    "username": form.data["primary_author"].username,
                },
            }

            if form.data["secondary_author"] is None:
                response["secondary_author"] = None
            else:

                response["secondary_author"] = {
                    "id": form.data["secondary_author"].pk,
                    "username": form.data["secondary_author"].username,
                }
            return JsonResponse(response)

        else:
            return super(RevisionView, self).form_invalid(form)
