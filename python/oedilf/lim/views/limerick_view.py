from django.http import JsonResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404, reverse, redirect
from django.template.context_processors import csrf
from django.template.loader import render_to_string
from django.utils.timezone import now
from django.views.generic.base import TemplateView, RedirectView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import FormView
from rules.contrib.views import PermissionRequiredMixin

from lim.forms import LimerickForm
from lim.models import Limerick, Revision, LimStates, Comment, CommentActions
from lim.utils import is_numeric
from .revision_view import RevisionView, render_crispy_form


class LimerickView(PermissionRequiredMixin, SingleObjectMixin, TemplateView):
    template_name = "lim/limerick-list.html"
    permission_required = "lim.view_limerick"
    model = Limerick
    pk_url_kwarg = "limerick_id"

    def get_context_data(self, **kwargs):
        limerick = get_object_or_404(Limerick, pk=kwargs["limerick_id"])
        # noinspection PyAttributeOutsideInit
        self.object = limerick
        context = super(LimerickView, self).get_context_data(**kwargs)
        context["limericks"] = [limerick]
        context["page"] = 1
        context["title"] = "<b>{}</b> by {}".format(
            limerick.formatted_defined_words, limerick.latest_revision.author_names
        )
        if self.request.user.is_authenticated:
            context["tags"] = limerick.tags.filter(author=self.request.user)
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if is_numeric(kwargs["limerick_id"]):
            return redirect(
                reverse("limerick", kwargs=dict(limerick_id=self.object.id.hashid)),
                request,
            )
        else:
            return self.render_to_response(context)


class LimerickAddView(RevisionView):
    permission_required = "lim.add_limerick"
    template_name = "lim/add-limerick.html"
    model = Limerick
    form_class = LimerickForm

    fields = ["verse", "author_notes", "editor_notes", "all_defined_words"]

    def __init__(self):
        super(LimerickAddView, self).__init__()
        self.form = None
        self._limerick_id = None

    @property
    def limerick_id(self):
        return None

    def get_permission_object(self):
        return self.request.user

    def get_form_kwargs(self):
        kwargs = super(LimerickAddView, self).get_form_kwargs()
        kwargs.pop("limerick_id", None)
        kwargs["request"] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        limerick = Limerick()
        form = LimerickForm(
            instance=Revision(),
            request=self.request,
            initial={
                "all_defined_words": "",
                "primary_author": self.request.user,
                "secondary_author": None,
            },
        )
        context = super(FormView, self).get_context_data(**kwargs)
        context["limerick"] = limerick
        context["revision_form"] = form
        return context

    def form_valid(self, form):
        if not self.request.user.has_perm("lim.add_limerick"):
            raise PermissionError(
                '"{}" does not have permission to add a limerick'.format(
                    self.request.user.username
                )
            )

        limerick = Limerick.objects.create(
            state=LimStates.NEW, last_workshop_date=now()
        )
        limerick.primary_author = self.request.user
        form.cleaned_data["primary_author"] = self.request.user
        self.create_revision(form, limerick)
        self.success_url = reverse("workshop", kwargs={"limerick_id": limerick.pk})
        if self.request.is_ajax():
            response = {"success": True, "redirect_url": self.success_url}
            return JsonResponse(response)

        else:
            return super(LimerickAddView, self).form_valid(form)

    def form_invalid(self, form):
        self.success_url = reverse("add-limerick")
        self.extra_context = {
            "add_form": form,
            "request": self.request,
            "status": "failure",
        }
        if self.request.is_ajax():
            ctx = self.get_context_data()
            ctx.update(self.extra_context)
            ctx.update(csrf(self.request))
            ctx["request"] = self.request
            form.request = self.request
            # ensure trumbowyg doesn't double-div every line
            form.data = form.cleaned_data
            form.setup_helper()
            crispy_form = render_crispy_form(form, context=ctx)
            response = {"success": False, "status": 400, "form_html": crispy_form}
            if form.data["primary_author"] is None:
                form.data["primary_author"] = self.request.user
            response["primary_author"] = {
                "id": form.data["primary_author"].pk,
                "username": form.data["primary_author"].username,
            }

            if form.data["secondary_author"] is None:
                response["secondary_author"] = None
            else:
                response["secondary_author"] = {
                    "id": form.data["secondary_author"].pk,
                    "username": form.data["secondary_author"].username,
                }
            return JsonResponse(response)

        else:
            return super(LimerickAddView, self).form_invalid(form)


class LimerickSTCView(PermissionRequiredMixin, RedirectView):
    permission_required = "lim.set_to_confirming"
    pattern_name = "workshop"

    def __init__(self):
        super(LimerickSTCView, self).__init__()
        self.form = None
        self._limerick_id = None
        self._limerick = None

    @property
    def limerick_id(self):
        if self._limerick_id is None:
            self._limerick_id = self.kwargs.pop("limerick_id")
        return self._limerick_id

    def get_permission_object(self):
        return self.limerick

    @property
    def limerick(self):
        if self._limerick is None:
            self._limerick = get_object_or_404(Limerick, pk=self.limerick_id)
        return self._limerick

    def get(self, request, *args, **kwargs):
        revision = self.limerick.latest_revision
        comment = Comment.objects.create(
            author=self.request.user,
            message="Set limerick to Confirming state. "
            "Barring any issues, this limerick will be approved in a week. "
            "Authors may still make alterations to their writing by stopping the countdown "
            "to return the limerick to Tentative.",
            # TODO put this message into SiteSettings or similar
            date_time=now(),
            action=CommentActions.STC,
            revision=revision,
        )
        comment.save()

        self.limerick.approving_editor = self.request.user
        self.limerick.state = LimStates.CONFIRMING
        self.limerick.save()

        return super(LimerickSTCView, self).get(request, *args, **kwargs)


class LimerickStopCountdownView(PermissionRequiredMixin, RedirectView):
    permission_required = "lim.stop_countdown"
    pattern_name = "workshop"

    def __init__(self):
        super(LimerickStopCountdownView, self).__init__()
        self.form = None
        self._limerick_id = None
        self._limerick = None

    @property
    def limerick_id(self):
        if self._limerick_id is None:
            self._limerick_id = self.kwargs.pop("limerick_id")
        return self._limerick_id

    def get_permission_object(self):
        return self.limerick

    @property
    def limerick(self):
        if self._limerick is None:
            self._limerick = get_object_or_404(Limerick, pk=self.limerick_id)
        return self._limerick

    def get(self, request, *args, **kwargs):
        revision = self.limerick.latest_revision
        comment = Comment.objects.create(
            author=self.request.user,
            message="Countdown stopped\n",
            # TODO put this message into SiteSettings or similar
            date_time=now(),
            action=CommentActions.COUNTDOWN_STOP,
            revision=revision,
        )
        comment.save()

        self.limerick.approving_editor = self.request.user
        self.limerick.state = LimStates.TENTATIVE
        self.limerick.save()

        return super(LimerickStopCountdownView, self).get(request, *args, **kwargs)


class JSONResponseMixin:
    """
    A mixin that can be used to render a JSON response.
    """

    def render_to_json_response(self, context, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        return JsonResponse(self.get_data(context), **response_kwargs)

    def get_data(self, context):
        """
        Returns an object that will be serialized as JSON by json.dumps().
        """
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        return context


class TopicBadgeAjaxView(JSONResponseMixin, SingleObjectMixin, TemplateView):

    template_name = "lim/inclusion/topics-list.html"
    model = Limerick

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        return super(TopicBadgeAjaxView, self).get_context_data(**kwargs)

    def render_to_response(self, context, **response_kwargs):
        if self.request.is_ajax():
            return JsonResponse(
                dict(
                    pk=self.object.pk.hashid,
                    html=render_to_string(self.template_name, context, self.request),
                )
            )
        else:
            return super(TopicBadgeAjaxView, self).render_to_response(
                context, **response_kwargs
            )


class TagBadgeAjaxView(JSONResponseMixin, SingleObjectMixin, TemplateView):

    template_name = "lim/inclusion/tags-list.html"
    model = Limerick

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(TagBadgeAjaxView, self).get_context_data(**kwargs)
        context["tags"] = self.object.tags
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.request.is_ajax():
            return JsonResponse(
                dict(
                    pk=self.object.pk.hashid,
                    html=render_to_string(self.template_name, context, self.request),
                )
            )
        else:
            return super(TagBadgeAjaxView, self).render_to_response(
                context, **response_kwargs
            )


class AjaxLimerickView(TemplateView):
    model = Limerick
    template_name = "lim/cards/limerick-card.html"

    def get_queryset(self):
        return (
            Limerick.objects.select_related("primary_author", "secondary_author")
            .prefetch_related("definedword_set", "showcase_set")
            .filter(pk=self.kwargs["limerick_id"])
        )

    def get_context_data(self, object_list=None, **kwargs):
        context = super(AjaxLimerickView, self).get_context_data(
            object_list=self.get_queryset(), **kwargs
        )
        limerick = self.get_queryset().first()
        context["limerick"] = limerick
        if self.request.user.is_authenticated:
            context["tags"] = limerick.tags.filter(author=self.request.user)
        return context

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            html = super(AjaxLimerickView, self).get(request, *args, **kwargs)
            return JsonResponse({"success": True, "html": html.rendered_content})

        else:
            return HttpResponseForbidden("<h1>403 Forbidden</h1>")
