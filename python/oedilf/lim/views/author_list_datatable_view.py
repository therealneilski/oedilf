from django.db.models import Count
from django.shortcuts import reverse
from django.utils.html import format_html
from django.views.generic import TemplateView
from django_datatables_view.base_datatable_view import BaseDatatableView

from lim.models import Author, LimStates


class AuthorListDatatableDataView(BaseDatatableView):

    model = Author

    columns = [
        "user__username",
        "approved_count",
        "submitted_count",
        "backlog",
        "confirming_count",
        "tentative_count",
        "revised_count",
        "held_count",
        "new_count",
        "stored_count",
        "excluded_count",
        "untended_count",
    ]

    order_columns = columns

    max_display_length = 100

    @staticmethod
    def quantize(row, column):
        value = getattr(row, column)
        value = int(value) if int(value) == value else value
        return value

    def render_column(self, row, column):
        if column == "user__username":
            return format_html(
                '<a href="{}">{}</a>',
                reverse("author-profile", kwargs={"author": row.pk}),
                row.user.username,
            )

        elif column == "backlog":
            return "{0:.0f}%".format(row.backlog * 100)

        elif column == "submitted_count":
            return format_html(
                '<a href="{}">{}</a>',
                reverse("limericks-by-author", kwargs={"author": row.pk}),
                "{}".format(self.quantize(row, column)),
            )

        else:
            state = LimStates[column.replace("_count", "").upper()]
            return format_html(
                '<a href="{}?states={}">{}</a>',
                reverse("limericks-by-author", kwargs={"author": row.pk}),
                state.name.title(),
                "{}".format(self.quantize(row, column)),
            )


class AuthorListDatatableView(TemplateView):

    template_name = "lim/datatables/author-list-datatable.html"


class AuthorShowcasesDatatableDataView(BaseDatatableView):

    model = Author

    columns = ["user__username", "showcase_size"]

    order_columns = columns
    max_display_length = 100

    @staticmethod
    def quantize(row, column):
        value = getattr(row, column)
        value = int(value) if int(value) == value else value
        return value

    def render_column(self, row, column):
        if column == "user__username":
            return format_html(
                '<a href="{}">{}</a>',
                reverse("author-showcase", kwargs={"author": row.pk}),
                row.user.username,
            )

        elif column == "showcase_size":
            showcase_size = row.user.showcase_set.count()
            showcase_quantity = row.showcase_quantity()
            if showcase_quantity < 1 and showcase_size > 0:
                showcase_quantity = 1
            return format_html(
                '<a href="{}">{}</a>',
                reverse("author-showcase", kwargs={"author": row.pk}),
                showcase_size
                if showcase_size <= showcase_quantity
                else f"{showcase_size} ({showcase_quantity} shown)",
            )

    def get_initial_queryset(self):
        return self.model.objects.annotate(
            showcase_size=Count("user__showcase")
        ).filter(showcase_size__gt=0)


class AuthorShowcasesDatatableView(TemplateView):

    template_name = "lim/author-showcases-datatable.html"
