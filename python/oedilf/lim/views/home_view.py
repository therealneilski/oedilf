from django.db.models import Count
from django.views.generic.base import TemplateView, RedirectView

from lim.models import Limerick, DefinedWord, Author, LimStates, SiteSettings


class HomeView(TemplateView):
    template_name = "lim/home.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        stats = {
            "total_limericks": Limerick.objects.all().count(),
            "total_approved": Limerick.objects.filter(state=LimStates.APPROVED).count(),
            "total_unique_words": DefinedWord.objects.annotate(
                defined=Count("revision_set")
            )
            .filter(defined__gt=0)
            .count(),
            "total_authors": Author.objects.count(),
        }
        latest_approved_limericks = Limerick.objects.filter(
            state=LimStates.APPROVED, curtained=False
        ).order_by("-last_workshop_date")[:4]
        random_limericks = Limerick.objects.filter(
            state=LimStates.APPROVED, curtained=False
        ).order_by("?")[:4]
        context["stats"] = stats
        context["limericks"] = latest_approved_limericks
        context["random_limericks"] = random_limericks
        context["alphabet_end"] = SiteSettings.get_solo().alphabet_end
        return context


class HomeOrActivityView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            self.url = "activity"
        else:
            self.url = "home"
        return super(HomeOrActivityView, self).get_redirect_url(*args, **kwargs)
