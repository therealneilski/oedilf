from django.contrib.auth.models import User
from django.db.models import Q, QuerySet, F
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.views.generic.base import TemplateView
from lim.models import Limerick, LimStates, Revision
from django.contrib.postgres.search import *


class LimerickListViewAjax(TemplateView):
    template_name = "lim/limerick-list-ajax.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # check that the requested author exists
        author_pk = self.request.GET.get("author_pk", None)
        get_object_or_404(User, pk=author_pk)
        state_names = self.request.GET.get("states")
        if state_names == "":
            limericks = QuerySet(model=Limerick).exclude(pk__gt=0)
        else:
            states = [LimStates[state.upper()] for state in state_names.split(",")]

            limericks = Limerick.objects.filter(
                (Q(primary_author__pk=author_pk) | Q(secondary_author__pk=author_pk))
                & Q(state__in=states)
            )

        order_by = self.request.GET.get("order_by", "last_revision_date")
        if order_by in ("last_revision_date", "last_workshop_date", "created_date"):
            limericks = limericks.order_by("-" + order_by)
        elif order_by == "state":
            limericks = limericks.order_by("state", "-last_revision_date")
        else:
            limericks = limericks.order_by("-last_revision_date")

        context["limericks"] = limericks
        context["request"] = self.request
        return context

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            html = render_to_string(self.template_name, self.get_context_data())
            return HttpResponse(html)

        else:
            return HttpResponseForbidden("<h1>403 Forbidden</h1>")
