from lim.models import Limerick, DefinedWord
from lim.views.limerick_list_view import LimerickListView, LimerickListWithTOCJsonMixin


class DefinitionListView(LimerickListView):
    template_name = "lim/limerick-list-definition.html"

    def __init__(self):
        super(DefinitionListView, self).__init__()
        self._defined_word = None
        self._dw_qs = None
        self._word = None

    @property
    def defined_word(self):
        if self._dw_qs is None:
            if "word" in self.kwargs:
                word = self.kwargs["word"]
            else:
                word = self.request.GET.get("word", None)
            self._word = word
            self._dw_qs = DefinedWord.objects.filter(word_text=word)
            if self._dw_qs.exists():
                self._defined_word = self._dw_qs.first()
            else:
                self._defined_word = None
        return self._defined_word

    def get_title(self, context):
        return f"Limericks on <b>{self._word}</b>"

    def get_queryset(self):
        if self.defined_word is None:
            return Limerick.objects.none()
        else:
            return self.defined_word.limerick_set

    def get_context_data(self, **kwargs):
        context = super().get_context_data(word=self._word)
        return context


class DefinitionListWithTOCJsonView(LimerickListWithTOCJsonMixin, DefinitionListView):
    pass
