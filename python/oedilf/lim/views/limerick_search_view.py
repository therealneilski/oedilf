from django.contrib.postgres.search import *
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormMixin, ProcessFormView
from el_pagination.views import AjaxListView

from lim.forms import LimerickSearchForm
from lim.models import *
from lim.models import Limerick, LimStates
from lim.utils import oxford_join


class LimerickSearchMixin:
    @staticmethod
    def get_title(context):
        return f"Search results for <b>{context['q']}</b>"

    def get_limericks(self, **kwargs):
        if kwargs["q"] == "" and kwargs["a"] != "":
            qs = (
                Revision.objects.annotate(rank=Value(1.0, output_field=FloatField()))
                .filter(is_latest_revision=1)
                .select_related("limerick")
            )
        else:
            search_query = SearchQuery(kwargs["q"])
            # self.ensure_booleans_are_boolean(kwargs)
            rank = SearchRank(
                F('search_vector'),
                search_query,
                weights=self.get_search_rank_weights(kwargs),
            )
            qs = (
                Revision.objects.annotate(rank=rank)
                .filter(search_vector=kwargs["q"], is_latest_revision=1, rank__gt=0)
                .order_by("-rank")
                .select_related("limerick")
            )
        if not self.request.user.is_authenticated:
            qs = qs.filter(
                limerick__state=LimStates.APPROVED, limerick__curtained=False
            )
        if kwargs["r_l"] >= 0:
            qs = qs.filter(rfa_count__gte=kwargs["r_l"])
        if kwargs["r_m"] >= 0:
            qs = qs.filter(rfa_count__lte=kwargs["r_m"])
        if kwargs["y"] >= 0:
            qs = qs.filter(limerick__created_date__year=kwargs["y"])
        if kwargs["a"] is not None:
            qs = qs.filter(
                Q(limerick__primary_author__pk=kwargs["a"])
                | Q(limerick__secondary_author__pk=kwargs["a"])
            )
        if kwargs["co_a"] == "n":
            qs = qs.filter(limerick__secondary_author__isnull=True)
        elif kwargs["co_a"] == "y":
            qs = qs.exclude(limerick__secondary_author__isnull=True)
        if kwargs["c_by"] is not None:
            qs = qs.filter(
                limerick__revision__comment__author__pk=kwargs["c_by"]
            ).distinct()
        if kwargs["s"]:
            qs = qs.filter(limerick__state__in=kwargs["s"])

        # The queryset generated here doesn't allow the count() method:
        # it complains about an unhashable type.
        # We have to include 'rank' somewhere in the values to allow the count to happen.
        # So we override the count() method.
        qs.count = lambda: qs.values_list('rank').count()
        return qs

    def get_search_rank_weights(self, kwargs):
        return [
            0.1 if kwargs["s_c"] else 0,
            1.0 if kwargs["s_dw"] else 0,
            0.2 if kwargs["s_an"] else 0,
            0.4 if kwargs["s_l"] else 0,
        ]

    def get_states_context(self, request):
        context = {}

        state_names = request.GET.get("states")
        if state_names is None:
            states = LimStates.__members__.values()
        elif state_names == "":
            states = []
        else:
            states = [LimStates[state.upper()] for state in state_names.split(",")]

        context["state_buttons"] = [
            LimStates.NEW,
            LimStates.TENTATIVE,
            LimStates.REVISED,
            LimStates.CONFIRMING,
            LimStates.APPROVED,
            LimStates.HELD,
            LimStates.UNTENDED,
            LimStates.STORED,
            LimStates.EXCLUDED,
        ]
        context["states"] = states

        return context


class LimerickSearchView(LimerickSearchMixin, FormMixin, ProcessFormView, TemplateView):

    template_name = 'lim/search.html'
    form_class = LimerickSearchForm

    def form_valid(self, form):
        self.template_name = "lim/limerick-list-search.html"
        self.success_url = reverse('search')
        context = self.get_context_data()
        context.update(
            {
                "ajax_path": "lim/limerick-list-ajax.html",
                'q': form.cleaned_data["q"],
                "s_c": form.cleaned_data["s_c"],
                "s_dw": form.cleaned_data["s_dw"],
                "s_an": form.cleaned_data["s_an"],
                "s_l": form.cleaned_data["s_l"],
                "project": form.cleaned_data["project"],
                "r_l": form.cleaned_data["r_l"],
                "r_m": form.cleaned_data["r_m"],
                "y": form.cleaned_data["y"],
                "co_a": form.cleaned_data["co_a"],
                "a": form.cleaned_data["a"],
                "c_by": form.cleaned_data["c_by"],
                "states": list(LimStates.__members__),
                "page": form.cleaned_data["page"] if "page" in form.cleaned_data else 1,
                "limericks": self.get_limericks(**form.cleaned_data),
                "form": form,
                "search_in_fields": [
                    "defined words" if form.cleaned_data["s_dw"] else "",
                    "limericks" if form.cleaned_data["s_l"] else "",
                    "author notes" if form.cleaned_data["s_an"] else "",
                    "comments" if form.cleaned_data["s_c"] else "",
                ],
            }
        )
        context['search_summary'] = self.search_summary(form)
        if form.cleaned_data["a"]:
            context["a__username"] = User.objects.get(
                pk=form.cleaned_data["a"]
            ).username
        if form.cleaned_data["c_by"]:
            context["c_by__username"] = User.objects.get(
                pk=form.cleaned_data["c_by"]
            ).username
        return self.render_to_response(context=context)

    def search_summary(self, form):
        summary = []
        data = form.cleaned_data
        if len(data["s"]) != len(list(LimStates.choices())):
            summary.append(
                oxford_join(list(LimStates(i).name.lower() for i in data["s"]))
                + " limericks"
            )
        if data["a"]:
            author = User.objects.get(pk=data["a"]).username
            if data['co_a'] == 'b':
                summary.append(f"written by {author}")
            elif data['co_a'] == 'n':
                summary.append(f"written solely by {author}")
            else:
                summary.append(f"co-written by {author}")
        if data["r_l"] > 0 and data["r_m"] > 0:
            summary.append(f"with {data['r_l']} to {data['r_m']} RFAs")
        elif data['r_l'] > 0:
            summary.append(f"with at least {data['r_l']} RFAs")
        elif data['r_m'] > 0:
            summary.append(f"with at most {data['r_m']} RFAs")
        if data["c_by"]:
            summary.append(
                "with a comment from " + User.objects.get(pk=data["c_by"]).username
            )
        if data["y"] >= 0:
            summary.append(f"originally submitted in {data['y']}")
        return summary

    def form_invalid(self, form):
        return super(LimerickSearchView, self).form_invalid(form)

    def get(self, request, *args, **kwargs):
        if "q" in request.GET:
            form = self.form_class(request.GET)
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)
        else:
            return super(LimerickSearchView, self).get(request, *args, **kwargs)


class LimerickSearchResultsViewAjax(LimerickSearchMixin, AjaxListView):
    context_object_name = "limericks"
    template_name = "lim/limerick-list-search.html"
    page_template = "lim/limerick-list-ajax.html"
    model = Limerick

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["limericks"] = self.get_limericks(**self.request.GET)
        context["request"] = self.request
        context["page"] = self.kwargs.get("page", self.request.GET.get("page", 1))

        context["order_by"] = "-rank"

        return context
