import re

from django.db.models import Count
from django.db.models.functions import Left, Lower
from django.views.generic import ListView

from lim.models import DefinedWord, LimStates


class DefinedWordPrefixSummaryView(ListView):

    model = DefinedWord
    template_name = "lim/word-summary.html"

    def get_queryset(self):
        return (
            super(DefinedWordPrefixSummaryView, self)
            .get_queryset()
            .annotate(prefix=Left(Lower("word_text"), 2))
            .values("prefix")
            .order_by("prefix", "limerick_set__state")
            .annotate(Count("limerick_set", distinct=True))
            .values("prefix", "limerick_set__count", "limerick_set__state")
        )

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(DefinedWordPrefixSummaryView, self).get_context_data(
            object_list=object_list, **kwargs
        )
        letters_re = re.compile(u'^[^\W\d_]{2}$', re.U)
        word_data = {}
        for row in self.get_queryset():
            if letters_re.findall(row["prefix"]):
                prefix = row["prefix"]
            else:
                prefix = " (other)"
            word_data.setdefault(
                prefix, dict(approved=0, in_progress=0, untended=0, excluded=0, total=0)
            )
            data = word_data[prefix]
            if row["limerick_set__state"] is not None:
                state = LimStates(row["limerick_set__state"])
                if state in (LimStates.APPROVED, state == LimStates.CONFIRMING):
                    data["approved"] += row["limerick_set__count"]
                elif state in (LimStates.NEW, LimStates.REVISED, LimStates.TENTATIVE):
                    data["in_progress"] += row["limerick_set__count"]
                elif state == LimStates.UNTENDED:
                    data["untended"] += row["limerick_set__count"]
                elif state in (LimStates.HELD, LimStates.STORED, LimStates.EXCLUDED):
                    data["excluded"] += row["limerick_set__count"]
                data["total"] += row["limerick_set__count"]

        context["word_data"] = word_data
        context["title"] = "Summary of Words by Prefix"
        context["word_column_name"] = "Prefix"
        return context
