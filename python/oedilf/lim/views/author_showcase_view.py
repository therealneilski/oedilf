from django.db.models import Q
from django.shortcuts import get_object_or_404
from el_pagination.views import AjaxListView

from lim.models import LimStates, Showcase, Author


class AuthorShowcaseView(AjaxListView):
    context_object_name = "showcase"
    template_name = "lim/limerick-list-showcase.html"
    page_template = "lim/limerick-list-showcase-ajax.html"
    model = Showcase

    @staticmethod
    def get_title(context):
        return f"Showcase of limericks by <b>{context['author'].user.username}</b>"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            object_list=[], page_template=self.page_template
        )
        if "author" in self.kwargs:
            author_pk = self.kwargs["author"]
        else:
            author_pk = self.request.GET.get("author_pk", None)
        page = self.kwargs.get("page", self.request.GET.get("page", 1))
        author = get_object_or_404(Author, pk=author_pk)
        context["author_pk"] = author.pk.hashid
        context["author"] = author

        context["request"] = self.request
        context["title"] = self.get_title(context)
        context["page"] = page

        showcase_filter = Q(user__author=author) & Q(limerick__state=LimStates.APPROVED)
        if (
            self.request.user.is_anonymous
            or self.request.user.author.curtain_filtering
            or self.request.user.author.junior_mode
        ):
            showcase_filter &= Q(limerick__curtained=False)
        showcase = (
            Showcase.objects.filter(showcase_filter)
            .select_related("limerick")
            .prefetch_related("limerick__definedword_set")
        )[: author.showcase_quantity()]
        context["showcase"] = showcase
        context["showcase_len"] = min(author.showcase_quantity(), showcase.count())

        context["ajax_path"] = self.page_template

        return context


class AuthorShowcaseTOCView(AuthorShowcaseView):
    page_template = "lim/limerick-list-showcase-toc-ajax.html"
