import string

from django.db.models import Count, Q
from django.db.models.functions import Lower, Left
from django.views.generic.edit import FormMixin
from el_pagination.views import AjaxListView, MultipleObjectMixin

from lim.forms import RequiredWordForm
from lim.models import DefinedWord, SiteSettings


class RequiredWordMixin(MultipleObjectMixin):
    @staticmethod
    def get_letter_pairs():
        alphabet_end = SiteSettings.get_solo().alphabet_end.lower()
        for i in string.ascii_lowercase:
            for j in string.ascii_lowercase:
                pair = i + j
                yield pair
                if pair == alphabet_end:
                    return

    def get_words_lacking_limericks(self):
        """
        Return all DefinedWords which either have no Limericks,
        or for which all the Limericks are untended.

        In theory, the untended limericks could be found with
        DefinedWord.objects.exclude(
            limerick_set__state__in=(
                [state for state in LimStates if state != LimStates.UNTENDED]
            )
        )
        but this is incredibly slow (over 20 minutes on my laptop with 108,000 Limericks
        and 119,000 DefinedWords).
        A slightly quicker method is outlined in populate_untended_definedwords() deep in the
        migrations, but this is still way too slow for practical use.
        So we resort to the `untended` flag on DefinedWord.
        :return:
        """
        queryset = (
            super(RequiredWordMixin, self)
            .get_queryset()
            .annotate(
                lower_word=Lower("word_text"), limerick_set_count=Count("limerick_set")
            )
            .filter(Q(limerick_set_count=0) | Q(untended=True))
        )
        return queryset


class RequiredWordListView(RequiredWordMixin, FormMixin, AjaxListView):

    form_class = RequiredWordForm
    model = DefinedWord
    template_name = "lim/word-list.html"
    page_template = "lim/word-list-ajax.html"

    def get_queryset(self):
        form = self.get_form()
        if form.is_valid():
            prefix = form.cleaned_data["prefix"]
        else:
            prefix = None
        queryset = super(RequiredWordListView, self).get_words_lacking_limericks()
        if prefix == 'non-letter':
            queryset = queryset.annotate(prefix=Left('word_text', 1)).exclude(
                Q(prefix__in=[x for x in string.ascii_letters])
            )
        elif prefix:
            queryset = queryset.annotate(prefix=Lower(Left('word_text', 2))).filter(
                prefix=prefix
            )
        else:
            queryset = queryset.filter(
                lower_word__lt=SiteSettings.get_solo().alphabet_end
            )
        return queryset.order_by('lower_word')

    def get_form_kwargs(self):
        return {'data': self.request.GET}

    def get_context_data(self, **kwargs):
        context = super(RequiredWordListView, self).get_context_data(**kwargs)
        context['requiredword_list'] = context['object_list']
        if "page" not in context:
            context["page"] = 1
        form = self.get_form()
        if (
            form.is_valid()
            and "prefix" in form.cleaned_data
            and form.cleaned_data["prefix"]
        ):
            context[
                "title"
            ] = f"Words Lacking Limericks ({form.cleaned_data['prefix']})"
        else:
            context["title"] = "Words Lacking Limericks"
        context["letter_pairs"] = self.get_letter_pairs()
        return context


class RandomRequiredWordView(RequiredWordMixin, AjaxListView):

    model = DefinedWord
    template_name = "lim/word-list.html"
    page_template = "lim/word-list-ajax.html"

    def get_queryset(self):
        queryset = (
            super(RandomRequiredWordView, self)
            .get_words_lacking_limericks()
            .annotate(prefix=Lower(Left('word_text', 2)))
            .filter(
                Q(prefix__gt='aa') & Q(prefix__lte=SiteSettings.get_solo().alphabet_end)
            )
        )
        return queryset.order_by('?')[:20]

    def get_context_data(self, **kwargs):
        context = super(RandomRequiredWordView, self).get_context_data(**kwargs)
        context['requiredword_list'] = context['object_list']
        context["title"] = "Random Words Needing Limericks"
        context["page"] = 1
        context["letter_pairs"] = self.get_letter_pairs()
        return context
