from django.shortcuts import get_object_or_404

from lim.models import Topic
from .limerick_list_view import LimerickListView, LimerickListWithTOCJsonMixin


class TopicListView(LimerickListView):
    template_name = "lim/limerick-list-topic.html"

    def __init__(self):
        super(TopicListView, self).__init__()
        self._topic = None

    @property
    def topic(self):
        if self._topic is None:
            if "topic_pk" in self.kwargs:
                topic_pk = self.kwargs["topic_pk"]
            else:
                topic_pk = self.request.GET.get("topic_pk", None)
            self._topic = get_object_or_404(Topic, pk=topic_pk)
        return self._topic

    def get_queryset(self):
        return self.topic.limerick_set

    def get_title(self, context):
        return f"Limericks on <b>{self.topic.name}</b>"

    def get_context_data(self, **kwargs):

        context = super().get_context_data(topic=self.topic, topic_pk=self.topic.pk)

        return context


class TopicListWithTOCJsonView(LimerickListWithTOCJsonMixin, TopicListView):
    pass
