import random
from datetime import datetime, timedelta

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Q, Count, F
from django.http import JsonResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.utils.html import format_html
from django.views.generic import RedirectView, TemplateView
from django.views.generic.edit import FormMixin, ProcessFormView

from lim.forms import LimerickSearchForm
from lim.models import (
    Limerick,
    LimStates,
    GrabType,
    Grabbed,
    Project,
    LimerickWatcher,
    Author,
)
from lim.views.limerick_search_view import LimerickSearchMixin


class GrabProjectMixin:
    @staticmethod
    def get_project_and_type(grab_type_s):
        """
        Given a string, find out what grab type (and project, if appropriate) matches.
        :param grab_type_s:
        :return: GrabType, Project
        """
        project = Project.objects.filter(abbreviation=grab_type_s.upper())
        if project.count() > 0:
            return GrabType.PROJECT, project.first()
        else:
            try:
                return GrabType[grab_type_s.upper()], None
            except KeyError:
                return GrabType.RANDOM, None


class GrabView(GrabProjectMixin, PermissionRequiredMixin, RedirectView):

    permanent = False
    pattern_name = 'workshop'
    permission_required = "lim.is_workshopping_editor"

    def get_redirect_url(self, *args, **kwargs):
        grab_type, grab_project = self.get_project_and_type(
            kwargs.get("grab_type", None)
        )

        Grabbed.objects.filter(
            author=self.request.user, date__lt=datetime.now() - timedelta(days=7)
        ).delete()
        grabbed_limerick_ids = map(
            lambda x: x[0],
            Grabbed.objects.filter(
                author=self.request.user, type=grab_type
            ).values_list('limerick_id'),
        )

        watched_limerick_ids = map(
            lambda x: x[0],
            LimerickWatcher.objects.filter(author=self.request.user).values_list(
                'limerick_id'
            ),
        )

        if grab_type == GrabType.PROJECT:
            candidates = grab_project.limericks.all()
        else:
            candidates = Limerick.objects.all()
        candidates = (
            candidates.annotate(rfa_count=Count('rfa'))
            .filter(
                Q(state__in=(LimStates.NEW, LimStates.REVISED, LimStates.TENTATIVE))
            )
            .exclude(
                Q(pk__in=grabbed_limerick_ids)
                | Q(pk__in=watched_limerick_ids)
                | Q(primary_author=self.request.user)
                | Q(secondary_author=self.request.user)
            )
        )
        if grab_type == GrabType.RFA0:
            candidates = candidates.filter(Q(rfa_count=0))
        elif grab_type == GrabType.RFA1:
            candidates = candidates.filter(Q(rfa_count=1))
        elif grab_type == GrabType.RFA2:
            candidates = candidates.filter(Q(rfa_count=2))
        elif grab_type == GrabType.RFA3:
            candidates = candidates.filter(Q(rfa_count=3))
        elif grab_type == GrabType.RFA4:
            candidates = candidates.filter(Q(rfa_count__gte=4)).exclude(
                Q(rfa__author=F("primary_author"))
                | Q(rfa__author=F("secondary_author"))
            )
        elif grab_type == GrabType.STC:
            candidates = candidates.filter(Q(eligible_for_stc=True))
        if candidates.count() == 0:
            self.pattern_name = "grab-exhausted"
            if grab_type == GrabType.PROJECT:
                return super(GrabView, self).get_redirect_url(
                    grab_type=grab_project.abbreviation.lower()
                )
            else:
                return super(GrabView, self).get_redirect_url(grab_type=grab_type.name)
        random_idx = random.randint(0, candidates.count() - 1)
        lim = candidates[random_idx]
        Grabbed.objects.create(limerick=lim, author=self.request.user, type=grab_type)
        kwargs['limerick_id'] = lim.pk
        del kwargs['grab_type']
        return super(GrabView, self).get_redirect_url(*args, **kwargs)


class GrabNothingView(GrabProjectMixin, PermissionRequiredMixin, TemplateView):

    permission_required = "lim.is_workshopping_editor"
    template_name = "lim/grab-nothing.html"

    def get_context_data(self, **kwargs):
        context = super(GrabNothingView, self).get_context_data(**kwargs)
        for container in self.request.GET, kwargs:
            if "grab_type" in container:
                context["grab_type"], context[
                    "grab_project"
                ] = self.get_project_and_type(container["grab_type"])
                break
        excluded_grab_types = [GrabType.NONE, GrabType.PROJECT]
        if not self.request.user.has_perm("lim.is_approving_editor"):
            excluded_grab_types.append(GrabType.STC)
        context["grab_types"] = [
            t for t in GrabType.__members__.values() if t not in excluded_grab_types
        ]
        context["grab_projects"] = Project.objects.annotate(
            limerick_count=Count('limericks')
        ).filter(active=True, limerick_count__gt=0)
        return context


class AddAuthorToProjectView(PermissionRequiredMixin, RedirectView):

    permission_required = "lim.manage_projects"
    permanent = False
    pattern_name = "author-profile"

    def get_redirect_url(self, *args, **kwargs):
        author = get_object_or_404(Author, pk=kwargs['author'])
        project = get_object_or_404(
            Project, abbreviation=kwargs['project_abbrev'].upper()
        )

        author_limericks = Limerick.objects.filter(
            (Q(primary_author=author.user) | Q(secondary_author=author.user))
            & Q(state__in=(LimStates.NEW, LimStates.TENTATIVE, LimStates.REVISED))
        )
        project.limericks.add(*list(author_limericks.all()))
        messages.info(
            self.request,
            f"{author_limericks.count()} limericks added to {project.abbreviation}",
        )

        del kwargs['project_abbrev']
        return super(AddAuthorToProjectView, self).get_redirect_url(*args, **kwargs)


class AddSearchToProjectAjaxView(
    PermissionRequiredMixin, LimerickSearchMixin, FormMixin, ProcessFormView
):

    permission_required = "lim.manage_projects"
    form_class = LimerickSearchForm

    http_method_names = ['post']

    def form_valid(self, form):
        if not self.request.is_ajax():
            return HttpResponseForbidden("<h1>403 Forbidden</h1>")

        project = get_object_or_404(
            Project, abbreviation=form.cleaned_data["project"].upper()
        )
        old_count = project.limericks.count()
        limericks = self.get_limericks(**form.cleaned_data)
        project.limericks.add(
            *list(
                l.limerick
                for l in limericks.filter(
                    limerick__state__in=(
                        LimStates.NEW,
                        LimStates.TENTATIVE,
                        LimStates.REVISED,
                    )
                )
            )
        )
        new_count = project.limericks.count()
        added = new_count - old_count

        message = format_html(
            "{} of {} limerick{} added to {} project {}",
            added,
            limericks.count(),
            "" if limericks.count() == 1 else "s",
            project.abbreviation,
            "(the others were either already in the project or were not in a workshoppable state)"
            if added < limericks.count()
            else "",
        )

        return JsonResponse(
            {
                "success": True,
                "message": message,
                "title": "Limericks added" if added > 0 else "No limericks added",
                "class": "alert-success" if added > 0 else "alert-warning",
            }
        )

    def form_invalid(self, form):
        return HttpResponseForbidden("Forbidden")
