from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q, ExpressionWrapper, BooleanField
from django.http import HttpResponseRedirect
from django.shortcuts import reverse
from django.template.loader import render_to_string
from django.utils.html import format_html
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from django_datatables_view.base_datatable_view import BaseDatatableView
from django_tables2 import RequestConfig

from lim.forms import ActivityUpdateForm
from lim.models import ActivityMessage
from lim.tables import ActivityTable


class ActivityView(LoginRequiredMixin, TemplateView):

    template_name = "lim/activity.html"
    table_class = ActivityTable
    model = ActivityMessage

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        mine = int(self.request.GET.get("mine", 1)) != 0
        context["mine"] = mine
        activity = (
            ActivityMessage.objects.filter(dest=self.request.user)
            .annotate(
                mine=ExpressionWrapper(Q(author=self.request.user), BooleanField())
            )
            .order_by('-mine', 'revision__limerick_id', '-date_time')
            .distinct("mine", "revision__limerick_id")
        )

        table = ActivityTable(activity)
        RequestConfig(self.request, paginate={"per_page": 20}).configure(table)
        context["table"] = table
        context["title"] = f"Activity for {self.request.user.username}"
        context["activity"] = activity
        return context


class ActivityUpdateView(LoginRequiredMixin, FormView):

    form_class = ActivityUpdateForm

    def get_form_kwargs(self):
        kwargs = super(ActivityUpdateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def render_to_response(self, context, **response_kwargs):
        return HttpResponseRedirect(reverse('activity'))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form, **kwargs)
        else:
            return self.form_invalid(form, **kwargs)

    def form_invalid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        context['form'] = form
        # here you can add things like:
        return self.render_to_response(context)

    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        context['form'] = form
        # map from {'checkbox-Xnq9Fb': True, 'checkbox-Zq5oAl': False}
        # to ["Xnq9Fb"]
        message_pks = list(
            map(
                lambda cb_name: cb_name[9:],
                filter(
                    lambda x: x[:9] == 'checkbox-' and form.cleaned_data[x] is True,
                    form.cleaned_data.keys(),
                ),
            )
        )
        messages = ActivityMessage.objects.filter(
            dest=self.request.user, pk__in=message_pks
        )
        if form.cleaned_data['action'] == 'seen':
            messages.update(seen=True)
        elif form.cleaned_data['action'] == 'unseen':
            messages.update(seen=False)
        elif form.cleaned_data['action'] == 'delete':
            limerick_ids = list(
                map(lambda x: x[0], messages.values_list('revision__limerick_id'))
            )
            ActivityMessage.objects.filter(
                dest=self.request.user, revision__limerick_id__in=limerick_ids
            ).delete()
        return self.render_to_response(context)


class ActivityTableDataView(LoginRequiredMixin, BaseDatatableView):

    model = ActivityMessage

    columns = [
        'seen',
        'source',
        'author',
        "defines",
        "message",
        # 'data',
        'date_time',
        'pk',
    ]

    order_columns = columns
    max_display_length = 25

    def render_column(self, row, column):
        if column in ("source", "author"):
            return self.render_user_column(row, column)
        elif column == "seen":
            return self.render_seen(row)
        elif column == "message":
            return self.render_data(row)
        elif column == "date_time":
            return self.render_contents(row, row.date_time.strftime("%c"))
        elif column == "defines":
            return self.render_defines(row)
        elif column == "pk":
            return row.pk.hashid
        else:
            return super(ActivityTableDataView, self).render_column(row, column)

    def render_seen(self, row):
        if not row.seen:
            return format_html(
                '<i class="fa fa-envelope"></i> <small>#{}</small>', row.pk.id
            )
        else:
            return format_html('<small>#{}</small>', row.pk.id)

    def render_user_column(self, row, column):
        return render_to_string(
            "lim/inclusion/activity-user.html",
            context={"user": getattr(row, column), "column": column},
        )

    def render_contents(self, row, contents):
        return format_html(
            '<a href="{}">{}</a>',
            "#"
            if row.revision is None
            else reverse("workshop", args=[row.revision.limerick.pk]),
            contents,
        )

    def render_defines(self, row):
        return render_to_string(
            "lim/inclusion/activity-message-limerick.html", context={"row": row}
        )

    def render_data(self, row):
        return render_to_string(
            "lim/inclusion/activity-message-data.html", context={"row": row}
        )

    def get_initial_queryset(self):
        return (
            ActivityMessage.objects.filter(dest=self.request.user)
            .annotate(
                mine=ExpressionWrapper(Q(author=self.request.user), BooleanField())
            )
            .order_by('-mine', 'revision__limerick_id', '-date_time')
            .distinct("mine", "revision__limerick_id")
        )

    def ordering(self, qs):
        return qs

    def filter_queryset(self, qs):
        # use parameters passed in GET request to filter queryset

        search = self.request.GET.get('search[value]', None)
        if search:
            qs = qs.filter(
                Q(source__username__icontains=search)
                | Q(author__username__icontains=search)
                | Q(data__icontains=search)
            )

        return qs


class ActivityTableView(LoginRequiredMixin, TemplateView):

    template_name = "lim/datatables/activity-datatable.html"
