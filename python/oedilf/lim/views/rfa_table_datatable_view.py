from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.postgres.aggregates import StringAgg
from django.db.models import (
    OuterRef,
    Count,
    Q,
    IntegerField,
    F,
    Case,
    When,
    Value,
    CharField,
    Subquery,
)
from django.db.models.functions import Cast, Concat
from django.shortcuts import reverse
from django.utils.html import format_html, mark_safe
from django.views.generic import TemplateView
from django_datatables_view.base_datatable_view import BaseDatatableView

from lim.models import Limerick, LimStates, Revision
from lim.templatetags.lim import state_badge


class LimerickTableDataView(LoginRequiredMixin, BaseDatatableView):

    model = Limerick
    max_display_length = 100

    @staticmethod
    def quantize(row, column):
        value = getattr(row, column)
        value = int(value) if int(value) == value else value
        return value

    @staticmethod
    def render_as_link(row, link_text):
        return format_html(
            '<a href="{}">{}</a>',
            reverse('limerick', kwargs={"limerick_id": row.pk}),
            mark_safe(link_text),
        )

    def get_initial_queryset(self):
        latest_revisions = Revision.objects.filter(
            is_latest_revision=True, limerick=OuterRef('pk')
        )
        return self.model.objects.annotate(
            rfa_count=Count('rfa', distinct=True),
            my_rfa_count=Count(
                'rfa', filter=Q(rfa__author=self.request.user), distinct=True
            ),
            words=StringAgg("definedword_set__word_text", ", ", distinct=True),
            self_rfa_count=(
                Cast('primary_author_approved', IntegerField())
                + Cast('secondary_author_approved', IntegerField())
            ),
            author_names=(
                Concat(
                    F("primary_author__username"),
                    Case(
                        When(
                            secondary_author__isnull=False,
                            then=Concat(Value(", "), F("secondary_author__username")),
                        ),
                        default=Value(""),
                        output_field=CharField(),
                    ),
                )
            ),
            verse=Subquery(latest_revisions.values('verse')[:1]),
        )

    def render_column(self, row, column):
        if column == "verse":
            # just show the first line
            return self.render_as_link(row, row.verse[: row.verse.find('\n')])
        elif column == "words":
            return self.render_as_link(row, row.formatted_defined_words)
        elif column == "pk":
            return self.render_as_link(row, f"#{row.pk.id}")
        else:
            return super(LimerickTableDataView, self).render_column(row, column)


class RfaTableDataView(LimerickTableDataView):

    columns = [
        "words",
        "author_names",
        "last_revision_date",
        "rfa_count",
        "self_rfa_count",
        "my_rfa_count",
        "state",
        "verse",
        "last_workshop_date",
        "pk",
    ]
    order_columns = columns

    def render_column(self, row, column):
        if column == "last_revision_date":
            return self.render_last_revision_date(row)
        elif column == "last_workshop_date":
            return self.render_last_workshop_date(row)
        elif column == "rfa_count":
            return row.rfa_count
        elif column == "self_rfa_count":
            return row.self_rfa_count
        elif column == "my_rfa_count":
            if row.my_rfa_count:
                return format_html('<i class="fa fa-check"></i>')
            else:
                return format_html('<i class="fa fa-times"></i>')
        elif column == "state":
            return state_badge(row.state)
        else:
            return super(RfaTableDataView, self).render_column(row, column)

    @staticmethod
    def render_last_workshop_date(row):
        if row.last_workshop_date is not None:
            return row.last_workshop_date.strftime("%d %b %Y %H:%M:%S")
        else:
            return None

    @staticmethod
    def render_last_revision_date(row):
        if row.last_revision_date is not None:
            return row.last_revision_date.strftime("%d %b %Y %H:%M:%S")
        else:
            return None

    def filter_queryset(self, qs):
        # use parameters passed in GET request to filter queryset

        search = self.request.GET.get('search[value]', None)
        if search:
            qs = qs.filter(
                Q(author_names__icontains=search)
                | Q(words__icontains=search)
                | Q(verse__icontains=search)
            )

        return qs

    def ordering(self, qs):
        qs = super(RfaTableDataView, self).ordering(qs)
        if not qs.ordered:
            qs = qs.order_by("-rfa_count")

        return qs

    def get_initial_queryset(self):
        return (
            super(RfaTableDataView, self)
            .get_initial_queryset()
            .filter(
                rfa_count__gt=3,
                state__in=(LimStates.NEW, LimStates.TENTATIVE, LimStates.REVISED),
            )
            .distinct()
        )


class RfaTableView(TemplateView):

    template_name = "lim/datatables/rfa-datatable.html"
