from django.http import HttpResponse
from django.template.loader import render_to_string
from django.core.exceptions import PermissionDenied

from . import ActivityView


class NotificationsCardView(ActivityView):
    template_name = "lim/notifications-card.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["message_count"] = context["activity"].count()
        context["new_message_count"] = context["activity"].exclude(seen=True).count()
        context["activity"] = context["activity"][:5]

        return context

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            html = render_to_string(self.template_name, self.get_context_data())
            return HttpResponse(html)
        else:
            raise PermissionDenied
