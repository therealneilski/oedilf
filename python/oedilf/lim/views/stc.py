from el_pagination.views import AjaxListView

from lim.models import Limerick


class STCListView(AjaxListView):
    # Display all limericks by the specified author
    context_object_name = "limericks"
    template_name = "lim/limerick-list-no-state-selection.html"
    page_template = "lim/limerick-list-ajax.html"
    model = Limerick

    @staticmethod
    def get_title(context):
        return f"Limericks eligible for STC"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            object_list=[], page_template=self.page_template
        )
        page = self.kwargs.get("page", self.request.GET.get("page", 1))
        order_by = self.request.GET.get("order_by", "-last_revision_date")

        context["order_by"] = order_by
        context["request"] = self.request
        context["title"] = self.get_title(context)
        context["page"] = page

        limericks = Limerick.objects.filter(eligible_for_stc=True).order_by(order_by)
        context["limericks"] = limericks

        return context
