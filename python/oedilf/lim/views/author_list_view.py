from django.db.models import Q
from django.shortcuts import get_object_or_404

from lim.models import Author
from .limerick_list_view import LimerickListView, LimerickListWithTOCJsonMixin


class AuthorListView(LimerickListView):
    # Display all limericks by the specified author
    template_name = "lim/limerick-list-author.html"

    def get_title(self, context):
        return f"Limericks by <b>{context['author'].user.username}</b>"

    def get_filter(self, context):
        return Q(primary_author__author__pk=context["author_pk"]) | Q(
            secondary_author__author__pk=context["author_pk"]
        )

    def get_context_data(self, **kwargs):
        if "author" in self.kwargs:
            author_pk = self.kwargs["author"]
        else:
            author_pk = self.request.GET.get("author_pk", None)
        author = get_object_or_404(Author, pk=author_pk)

        context = super().get_context_data(author_pk=author_pk, author=author)

        return context


class AuthorListWithTOCJsonView(LimerickListWithTOCJsonMixin, AuthorListView):
    pass
