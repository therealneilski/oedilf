from datetime import datetime

from crispy_forms.utils import render_crispy_form
from django.contrib import messages
from django.db.models import Q, ExpressionWrapper, BooleanField
from django.http import (
    HttpResponse,
    HttpResponseForbidden,
    JsonResponse,
    HttpResponseRedirect,
)
from django.shortcuts import get_object_or_404, redirect, reverse
from django.template.context_processors import csrf
from django.template.loader import render_to_string
from django.utils.html import format_html
from django.views.generic import TemplateView, RedirectView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import UpdateView, DeleteView, FormView
from lim.forms import (
    CommentForm,
    CommentUpdateForm,
    LimerickAuthorApprovalForm,
    LimerickAuthorRejectionForm,
)
from lim.models import (
    ActivityMessage,
    Limerick,
    Comment,
    Revision,
    CommentActions,
    RFA,
    LimStates,
    LimerickWatcher,
    CFA,
    CFAResponse,
    SiteSettings,
)
from lim.utils import clean_trumbowyg, is_numeric
from rules.contrib.views import PermissionRequiredMixin

from . import LimerickView


class ActivityMessageMixin:
    @staticmethod
    def get_activity_messages(request):
        if request.user.is_authenticated:
            return (
                ActivityMessage.objects.filter(dest=request.user)
                .annotate(
                    mine=ExpressionWrapper(Q(author=request.user), BooleanField())
                )
                .order_by('-mine', 'revision__limerick_id', '-date_time')
                .distinct("mine", "revision__limerick_id")
            )
        else:
            return ActivityMessage.objects.none()

    @staticmethod
    def get_position_in_activity_messages(limerick, activity_messages):
        for i, m in enumerate(activity_messages):
            if m.revision.limerick.pk == limerick.pk:
                return i

    def get_activity_for_context(self, limerick, request):
        activity_messages = self.get_activity_messages(request)
        if activity_messages.exists():
            pos = self.get_position_in_activity_messages(limerick, activity_messages)
            if pos is not None:
                activity = {
                    "self": activity_messages[pos],
                    "position": pos + 1,
                    "total": len(activity_messages),
                    "this": activity_messages[pos],
                }
                if pos > 0:
                    activity["first"] = activity_messages.first()
                    activity["prev"] = activity_messages[pos - 1]
                if pos < len(activity_messages) - 1:
                    activity["last"] = activity_messages.last()
                    activity["next"] = activity_messages[pos + 1]
                return activity


class AjaxRevisionView(ActivityMessageMixin, TemplateView):
    model = Revision
    template_name = "lim/workshop-revision-ajax.html"

    def get_queryset(self):
        return Revision.objects.prefetch_related(
            "primary_author",
            "secondary_author",
            "comment_set",
            "comment_set__author",
            "definedword_set",
        ).filter(pk=self.kwargs["revision_id"])

    def get_context_data(self, object_list=None, **kwargs):
        context = super(AjaxRevisionView, self).get_context_data(
            object_list=self.get_queryset(), **kwargs
        )
        context["revision"] = self.get_queryset().first()
        limerick = context["revision"].limerick
        context["limerick"] = limerick
        if context["revision"].is_latest_revision:
            context["add_form"] = CommentForm(
                instance=context["revision"], request=self.request
            )
        else:
            context["add_form"] = None
        if self.request.user.is_authenticated:
            context["watching"] = LimerickWatcher.objects.filter(
                limerick=limerick, author=self.request.user
            ).exists()
            context["activity"] = self.get_activity_for_context(limerick, self.request)
        return context

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            html = super(AjaxRevisionView, self).get(request, *args, **kwargs)
            return JsonResponse({"success": True, "html": html.rendered_content})

        else:
            return HttpResponseForbidden("<h1>403 Forbidden</h1>")


class WorkshopView(LimerickView):
    def __init__(self):
        super(WorkshopView, self).__init__()
        self._object = None

    permission_required = "lim.is_authenticated"
    template_name = "lim/workshop.html"

    def get_object(self, queryset=None):
        if self._object is None:
            self._object = get_object_or_404(
                Limerick.objects.prefetch_related(
                    "revision_set",
                    "revision_set__primary_author",
                    "revision_set__secondary_author",
                    "revision_set__comment_set",
                    "revision_set__comment_set__author",
                    "revision_set__definedword_set",
                ),
                pk=self.kwargs["limerick_id"],
            )
        return self._object

    def render_to_response(self, context, **response_kwargs):
        if self.request.user.is_authenticated:
            ActivityMessage.objects.filter(
                Q(dest=self.request.user)
                & Q(revision__limerick=self.get_object())
                & Q(seen=False)
            ).update(seen=True)
        return super(WorkshopView, self).render_to_response(context, **response_kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        limerick = self.get_object()
        context["limerick"] = limerick
        context["add_form"] = CommentForm(
            instance=limerick.latest_revision, request=self.request
        )
        if limerick.confirmation_countdown_ended:
            context["approval_form"] = LimerickAuthorApprovalForm(
                limerick_id=limerick.pk
            )
            context["rejection_form"] = LimerickAuthorRejectionForm(
                limerick_id=limerick.pk
            )
        if self.request.user.is_authenticated:
            context["watching"] = LimerickWatcher.objects.filter(
                limerick=limerick, author=self.request.user
            ).exists()
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if is_numeric(kwargs["limerick_id"]):
            return redirect(
                reverse("workshop", kwargs=dict(limerick_id=self.object.id.hashid)),
                request,
            )
        else:
            return self.render_to_response(context)


class RemoveActivityWorkshopView(RedirectView):

    permanent = False
    query_string = True
    pattern_name = 'workshop'

    def get_redirect_url(self, *args, **kwargs):
        activity = get_object_or_404(ActivityMessage, pk=kwargs['pk'])
        if activity.dest != self.request.user:
            raise PermissionError("That is not your activity")
        ActivityMessage.objects.filter(
            dest=self.request.user, revision=activity.revision
        ).delete()
        if 'next' in self.request.GET:
            next_activity = get_object_or_404(
                ActivityMessage, pk=self.request.GET['next']
            )
            return reverse(
                'workshop', kwargs={'limerick_id': next_activity.revision.limerick_id}
            )
        else:
            return reverse('activity')


class CommentCreateAjaxView(FormView):
    template_name = "lim/inclusion/workshop-comment.html"

    model = Comment
    form_class = CommentForm

    def get_form_kwargs(self):
        kwargs = super(CommentCreateAjaxView, self).get_form_kwargs()
        kwargs.update(
            {
                "request": self.request,
                "instance": get_object_or_404(
                    Revision, pk=self.request.POST["revision_id"]
                ),
            }
        )
        return kwargs

    def form_valid(self, form):
        revision = get_object_or_404(Revision, pk=form.cleaned_data["revision_id"])
        if not self.request.user.has_perm("lim.add_comment", revision):
            raise PermissionError("You do not have permission to add a comment here")

        message = clean_trumbowyg(form.cleaned_data["message"])
        action = CommentActions[form.cleaned_data["action"]]
        comment = Comment.objects.create(
            revision=revision,
            author=self.request.user,
            message=message,
            date_time=datetime.now(),
            poster_ip=self.request.META["REMOTE_ADDR"],
            action=action,
        )
        added_comments = [comment]

        if action == CommentActions.CFA:
            self._create_cfa(comment)
        else:
            self._update_existing_cfa(added_comments, comment)

        if action == CommentActions.RFA_ADD:
            self._add_rfa(added_comments, comment)
        elif action == CommentActions.RFA_REMOVE:
            self._remove_rfa(comment)
        elif action == CommentActions.ADD_CURTAIN:
            self._add_curtain(comment)
        elif action == CommentActions.REMOVE_CURTAIN:
            self._remove_curtain(comment)
        elif action == CommentActions.HOLD:
            self._on_hold(comment)
        elif action == CommentActions.UNHOLD:
            self._off_hold(comment)
        elif action == CommentActions.CLEAR_RFAS:
            pass  # NOSONAR
        else:
            # noinspection PyUnusedLocal
            action = CommentActions.NONE

        # noinspection PyAttributeOutsideInit
        self.object = comment
        ctx = self.get_context_data()
        ctx.update(
            {
                "request": self.request,
                "revision": comment.revision,
                "limerick": comment.revision.limerick,
            }
        )
        ctx.update(csrf(self.request))
        self.success_url = reverse("comment-div", kwargs={"pk": comment.pk})
        added_comments_html = []
        for c in added_comments:
            c.save()
            ctx["comment"] = c
            added_comments_html.append(
                render_to_string(CommentView.template_name, context=ctx)
            )

        # RFA count may have been updated
        comment.revision.refresh_from_db(fields=["rfa_count"])

        return JsonResponse(
            {
                "success": True,
                "status": 200,
                "html": added_comments_html,
                "form_html": render_crispy_form(
                    CommentForm(request=self.request, instance=comment.revision),
                    context=ctx,
                ),
                "revision_version": comment.revision.version,
                "approvers_html": render_to_string(
                    "lim/inclusion/revision-approvers.html", context=ctx
                ),
                "metadata_html": render_to_string(
                    "lim/inclusion/revision-metadata.html", context=ctx
                ),
            }
        )

    def _off_hold(self, comment):
        limerick = comment.revision.limerick
        if not comment.author.has_perm("lim.unhold", limerick):
            raise PermissionError(
                "You do not have permission to take this limerick off hold"
            )

        limerick.off_hold(comment.author)
        limerick.save()

    def _on_hold(self, comment):
        limerick = comment.revision.limerick
        if not comment.author.has_perm("lim.hold", limerick):
            raise PermissionError(
                "You do not have permission to place this limerick on hold"
            )

        limerick.on_hold(comment.author)
        limerick.save()

    def _remove_curtain(self, comment):
        limerick = comment.revision.limerick
        if not comment.author.has_perm("lim.remove_curtain", limerick):
            raise PermissionError("You do not have permission to remove the curtain")

        limerick.curtained = False
        limerick.save()

    def _add_curtain(self, comment):
        limerick = comment.revision.limerick
        if not comment.author.has_perm("lim.add_curtain", limerick):
            raise PermissionError("You do not have permission to add the curtain")

        limerick.curtained = True
        limerick.save()

    def _remove_rfa(self, comment):
        if not comment.author.has_perm("lim.remove_rfa", comment.revision.limerick):
            raise PermissionError("You do not have permission to remove an RFA")

        comment.revision.limerick.rfa_set.filter(author=comment.author).delete()

    def _add_rfa(self, added_comments, comment):
        if not comment.author.has_perm("lim.add_rfa", comment.revision.limerick):
            raise PermissionError("You do not have permission to add an RFA")

        rfa = RFA(author=comment.author, limerick=comment.revision.limerick)
        rfa.save()
        if comment.author.has_perm("lim.is_author", comment.revision.limerick):
            info = Comment()
            info.message = (
                f"Based on the workshopping so far, {comment.author.username} "
                f"considers this limerick Ready for Final Approval."
            )
            info.author = None
            info.revision = comment.revision
            info.date_time = datetime.now()
            added_comments.append(info)

    def _create_cfa(self, comment):
        if not comment.author.has_perm(
            "lim.call_for_attention", comment.revision.limerick
        ):
            raise PermissionError("You do not have permission to Call For Attention")

        CFA.objects.create(limerick=comment.revision.limerick, creator=comment.author)
        comment.message = (
            "<h3>##### Call For Attention #####</h3><strong>"
            + comment.message
            + "</strong><br><br>"
            "NB Once the CFA has been completed, deleting this comment will <b>not</b> retract the messages"
            " sent to all the other workshoppers. "
            "The attention messages will be removed when "
            + str(SiteSettings.get_solo().cfa_contributors)
            + "different authors have contributed to the discussion."
        )
        comment.save()

    def _update_existing_cfa(self, added_comments, comment):
        # There may be a CFA on this limerick
        cfa_qs = CFA.objects.filter(limerick=comment.revision.limerick, active=True)
        if cfa_qs.exists():
            cfa = cfa_qs.first()
            CFAResponse.objects.get_or_create(
                cfa=cfa, author=self.request.user, contributed=True
            )
            if cfa.cfaresponse_set.count() >= SiteSettings.get_solo().cfa_contributors:
                cfa.active = False
                cfa.save()
                info = Comment(
                    author=None,
                    message=(
                        f"The following {cfa.cfaresponse_set.count()} editors have now commented on this limerick, "
                        f"and therefore the CFA is complete:\r"
                        + ", ".join(
                            [
                                cfaresponse.author.username
                                for cfaresponse in cfa.cfaresponse_set.all()
                            ]
                        )
                    ),
                    revision=comment.revision,
                    date_time=datetime.now(),
                    action=CommentActions.CFA_END,
                )
                added_comments.append(info)

    def form_invalid(self, form):
        revision = get_object_or_404(Revision, pk=form.initial["id"])
        self.success_url = reverse(
            "workshop", kwargs={"limerick_id": revision.limerick.pk}
        )
        self.extra_context = {
            "limerick": revision.limerick,
            "add_form": form,
            "request": self.request,
            "status": "failure",
        }
        if self.request.is_ajax():
            ctx = self.get_context_data()
            ctx.update(self.extra_context)
            ctx.update(csrf(self.request))
            ctx["request"] = self.request
            form.request = self.request
            form.setup_helper()
            return JsonResponse(
                {
                    "success": False,
                    "status": 400,
                    "form_html": render_crispy_form(form, context=ctx),
                }
            )

        else:
            return super(CommentCreateAjaxView, self).form_invalid(form)


class CommentView(TemplateView):
    template_name = "lim/inclusion/workshop-comment-list-group.html"

    def get_context_data(self, **kwargs):
        context = super(CommentView, self).get_context_data()
        pk = kwargs.pop("pk", self.kwargs.pop("pk"))
        comment = get_object_or_404(Comment, pk=pk)
        context["comment"] = comment
        context["request"] = self.request
        return context

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            html = render_to_string(self.template_name, self.get_context_data())
            return HttpResponse(html)

        else:
            return HttpResponseForbidden("<h1>403 Forbidden</h1>")


class CommentAjaxView(CommentView):
    template_name = "lim/inclusion/workshop-comment.html"

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            html = render_to_string(self.template_name, self.get_context_data())
            return JsonResponse({"success": True, "html": html})

        else:
            return HttpResponseForbidden("<h1>403 Forbidden</h1>")


class CommentAddFormView(TemplateView):
    template_name = "lim/inclusion/crispy-form.html"

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            context = self.get_context_data(**kwargs)
            context.update(csrf(self.request))
            pk = kwargs.pop("limerick", self.kwargs.pop("limerick_id"))
            lim = get_object_or_404(Limerick, pk=pk)
            form = CommentForm(request=self.request, instance=lim.latest_revision)
            return JsonResponse(
                {"html": render_crispy_form(form, context=context), "status": 200}
            )

        else:
            return HttpResponseForbidden("<h1>403 Forbidden</h1>")


class CommentUpdateView(UpdateView):
    template_name = "lim/inclusion/crispy-form.html"
    model = Comment
    form_class = CommentUpdateForm

    def form_valid(self, form):
        comment = get_object_or_404(Comment, pk=form.cleaned_data["comment_id"])
        if not self.request.user.has_perm("lim.change_comment", self.object):
            raise PermissionError("You do not have permission to change this comment")

        comment.message = clean_trumbowyg(form.cleaned_data["message"])
        comment.edit_date_time = datetime.now()
        comment.poster_ip = self.request.META["REMOTE_ADDR"]
        form.instance = comment
        form.save()
        self.success_url = reverse(
            "workshop", kwargs={"limerick_id": comment.revision.limerick.pk}
        )
        ctx = self.get_context_data()
        ctx.update({"request": self.request, "comment": comment})
        return JsonResponse(
            {
                "success": True,
                "status": 200,
                "html": render_to_string(
                    "lim/inclusion/workshop-comment.html", context=ctx
                ),
            }
        )

    def form_invalid(self, form):
        comment = get_object_or_404(Comment, pk=form.cleaned_data["comment_id"])
        revision = comment.revision
        self.extra_context = {
            "limerick": revision.limerick,
            "add_form": form,
            "request": self.request,
            "status": "failure",
        }
        if self.request.is_ajax():
            ctx = self.get_context_data()
            ctx.update(self.extra_context)
            ctx.update(csrf(self.request))
            ctx["request"] = self.request
            form.request = self.request
            form.setup_helper()
            return JsonResponse(
                {
                    "success": False,
                    "status": 400,
                    "form_html": render_crispy_form(form, context=ctx),
                }
            )

        else:
            raise Exception(
                form.is_valid(), repr(form.errors), repr(form.non_field_errors)
            )


class CommentDeleteView(DeleteView):
    template_name = "lim/workshop.html"
    model = Comment

    def delete(self, request, *args, **kwargs):
        if self.request.user.has_perm("lim.delete_comment", self.get_object()):
            return super(CommentDeleteView, self).delete(request, *args, **kwargs)

        else:
            raise PermissionError("You do not have permission to delete comments")

    def get_success_url(self):
        success_url = reverse(
            "workshop", kwargs={"limerick_id": self.object.revision.limerick.pk}
        )
        return success_url

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class ClearRFAsView(DeleteView):
    model = Limerick
    pk_url_kwarg = "limerick_id"

    def delete(self, request, *args, **kwargs):
        if not self.request.is_ajax():
            return HttpResponseForbidden("<h1>403 Forbidden</h1>")

        limerick = self.get_object()
        if self.request.user.has_perm("lim.clear_rfas", limerick):
            info = Comment()
            info.message = "{} cleared all RFAs ({})".format(
                self.request.user.username,
                ", ".join(rfa.author.username for rfa in limerick.rfa_set.all()),
            )
            info.author = None
            info.revision = limerick.latest_revision
            info.date_time = datetime.now()
            info.save()
            limerick.rfa_set.all().delete()
            return JsonResponse(
                {"status": 200, "success": True, "href": self.get_success_url()}
            )

        else:
            return JsonResponse(
                {"status": 403, "success": False, "message": "Forbidden"}
            )

    def get_success_url(self):
        success_url = reverse("workshop", kwargs={"limerick_id": self.get_object().pk})
        return success_url


class LimerickAuthorApprovalView(FormView):

    template_name = "lim/limerick-list.html"
    form_class = LimerickAuthorApprovalForm

    def form_valid(self, form):
        limerick = get_object_or_404(Limerick, pk=form.cleaned_data["limerick_id"])
        if not self.request.user.has_perm("lim.set_to_approved", limerick):
            raise PermissionError("You do not have permission to approve this limerick")

        comment = Comment()
        comment.author = None
        comment.message = "Approval message:\n{} (author {}) said Yes.\nI'm happy to approve this limerick.".format(
            self.request.user.username, self.request.user.id
        )
        comment.revision = limerick.latest_revision
        comment.date_time = datetime.now()
        comment.save()
        limerick.add_author_approval(self.request.user)
        if limerick.all_authors_have_approved:
            limerick.set_to_approved(self.request.user)
            limerick.save()
            self.success_url = reverse("limerick", kwargs={"limerick_id": limerick.id})
            messages.success(
                self.request,
                format_html(
                    "Your limerick #{} has been approved.  It can be found at "
                    "<a href='{}'>{}</a>",
                    limerick.id,
                    self.success_url,
                    self.request.build_absolute_uri(self.success_url),
                ),
            )
        else:
            limerick.save()
            self.success_url = reverse("workshop", kwargs={"limerick_id": limerick.id})
            messages.success(
                self.request,
                "Your approval has been noted.  The limerick will be approved "
                "once your co-author has also given their approval.",
            )
        return super().form_valid(form)


class LimerickAuthorRejectionView(FormView):

    template_name = "lim/workshop.html"
    form_class = LimerickAuthorRejectionForm

    def form_valid(self, form):
        limerick = get_object_or_404(Limerick, pk=form.cleaned_data["limerick_id"])
        if not self.request.user.has_perm("lim.set_to_approved", limerick):
            raise PermissionError("You do not have permission to reject this limerick")

        comment = Comment()
        comment.author = self.request.user
        comment.message = form.cleaned_data["rejection_message"]
        comment.revision = limerick.latest_revision
        comment.date_time = datetime.now()
        comment.action = CommentActions.RFA_REMOVE
        comment.save()
        limerick.rfa_set.filter(author=self.request.user).all().delete()
        limerick.state = LimStates.TENTATIVE
        limerick.save()
        self.success_url = reverse("workshop", kwargs={"limerick_id": limerick.id})
        messages.info(
            self.request,
            "Your comments have been noted, your RFA removed, and the limerick returned to Tentative.",
        )
        return super(LimerickAuthorRejectionView, self).form_valid(form)

    def form_invalid(self, form):
        limerick = get_object_or_404(Limerick, pk=form.cleaned_data["limerick_id"])
        for error in form.errors:
            messages.error(self.request, "{}: {}".format(error, form.errors[error][0]))
        self.success_url = reverse("workshop", kwargs={"limerick_id": limerick.id})
        return HttpResponseRedirect(self.success_url)


class LimerickForceFinalApprovalView(
    PermissionRequiredMixin, SingleObjectMixin, TemplateView
):

    template_name = "lim/workshop.html"
    permission_required = "lim.force_final_approval"
    model = Limerick
    pk_url_kwarg = "limerick_id"

    def get_permission_object(self):
        return self.get_object()

    def handle_no_permission(self):
        messages.error(self.request, "You cannot force this limerick to be Approved")
        return super().handle_no_permission()

    def get(self, request, *args, **kwargs):
        limerick = self.get_object()
        limerick.force_final_approval(self.request.user)
        limerick.save()
        comment = Comment()
        comment.author = None
        comment.message = "Approval message:\n{} (author {}) directly approved this limerick.".format(
            self.request.user.username, self.request.user.id
        )
        comment.revision = limerick.latest_revision
        comment.date_time = datetime.now()
        comment.save()
        messages.success(self.request, "You have set this limerick to be Approved.")
        return HttpResponseRedirect(
            reverse("limerick", kwargs={"limerick_id": limerick.id})
        )

    @property
    def object(self):
        return self.get_object()


class ToggleWatchLimerickAjaxView(
    SingleObjectMixin, PermissionRequiredMixin, TemplateView
):

    model = Limerick
    permission_required = 'lim.is_workshopping_editor'
    template_name = "lim/inclusion/watch-button.html"

    def get(self, request, *args, **kwargs):
        # noinspection PyAttributeOutsideInit
        self.object = self.get_object()
        if not self.request.is_ajax():
            return HttpResponseForbidden()
        context = self.get_context_data(**kwargs)
        watchers = LimerickWatcher.objects.filter(
            limerick=self.get_object(), author=self.request.user
        )
        if watchers.exists():
            watchers.delete()
            context["watching"] = False
        else:
            LimerickWatcher.objects.get_or_create(
                limerick=self.get_object(), author=self.request.user
            )
            context["watching"] = True

        return JsonResponse(
            {
                "success": True,
                "status": 200,
                "html": render_to_string(self.template_name, context),
            }
        )
