from django.db.models import Q
from django.http import JsonResponse, HttpResponseForbidden
from django.template.loader import render_to_string
from el_pagination.views import AjaxListView

from lim.models import Limerick, LimStates


class LimerickListView(AjaxListView):
    # Display all limericks
    context_object_name = "limericks"
    template_name = "lim/limerick-list-list.html"
    page_template = "lim/limerick-list-ajax.html"
    model = Limerick

    @staticmethod
    def get_title(context):
        return f"Limericks</b>"

    def get_filter(self, context):
        return Q()

    def get_context_data(self, **kwargs):
        if not hasattr(self, "object_list"):
            self.object_list = []
        if "object_list" not in kwargs:
            kwargs["object_list"] = self.object_list
        if "page_template" not in kwargs:
            kwargs["page_template"] = self.page_template
        context = super().get_context_data(**kwargs)
        page = self.kwargs.get("page", self.request.GET.get("page", 1))

        state_names = self.request.GET.get("states")
        if state_names is None:
            states = LimStates.__members__.values()
        elif state_names == "":
            states = []
        else:
            states = [LimStates[state.upper()] for state in state_names.split(",")]

        order_by = self.request.GET.get("order_by", "-last_revision_date")

        context["state_buttons"] = [
            LimStates.NEW,
            LimStates.TENTATIVE,
            LimStates.REVISED,
            LimStates.CONFIRMING,
            LimStates.APPROVED,
            LimStates.HELD,
            LimStates.UNTENDED,
            LimStates.STORED,
            LimStates.EXCLUDED,
        ]
        context["states"] = list(
            states
        )  # https://github.com/carltongibson/django-filter/issues/560
        context["order_by"] = order_by
        context["request"] = self.request
        context["title"] = self.get_title(context)
        context["page"] = page

        limericks = (
            self.get_queryset()
            .filter(Q(state__in=context["states"]) & self.get_filter(context))
            .order_by(context["order_by"])
            .prefetch_related("revision_set", "topics", "tags")
            .select_related("primary_author", "secondary_author")
        )
        context["limericks"] = limericks

        return context


class LimerickListWithTOCJsonMixin:
    def get(self, request, *args, **kwargs):
        if not self.request.is_ajax():
            return HttpResponseForbidden("403 Forbidden")

        context = self.get_context_data(**kwargs)
        toc = render_to_string("lim/limerick-list-toc-ajax.html", context)
        html = render_to_string("lim/limerick-list-ajax.html", context)
        response = {"success": True, "toc": toc, "html": html}
        return JsonResponse(response)


class LimerickListWithTOCJsonView(LimerickListWithTOCJsonMixin, LimerickListView):
    pass
