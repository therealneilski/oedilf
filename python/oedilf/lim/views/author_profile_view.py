from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView

from lim.forms import AuthorProfileManageForm
from lim.models import Author, Project


class AuthorProfileView(TemplateView):
    template_name = "lim/author-profile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["author"] = get_object_or_404(Author, pk=kwargs["author"])
        context["active_projects"] = Project.objects.filter(active=True)
        return context


class AuthorProfileManageView(LoginRequiredMixin, TemplateView):
    template_name = "lim/author-form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        author = get_object_or_404(Author, pk=kwargs["author"])
        if (
            kwargs["author"] != self.request.user.pk
            and not self.request.user.is_superuser
        ):
            raise PermissionError("You cannot edit the profile of someone else")

        context["author"] = author
        context["form"] = AuthorProfileManageForm(instance=author)
        return context


class AuthorProfileManageUpdate(UpdateView):
    model = Author
    form_class = AuthorProfileManageForm
    pk_url_kwarg = "author"
    template_name = "lim/author-form.html"

    def get_object(self, queryset=None):
        pk = self.kwargs.get(self.pk_url_kwarg)
        return Author.objects.get(pk=pk)
