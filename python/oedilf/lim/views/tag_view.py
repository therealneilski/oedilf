from django.shortcuts import get_object_or_404
from django.views.generic import ListView

from lim.models import Tag
from lim.views.limerick_list_view import LimerickListView, LimerickListWithTOCJsonMixin


class TagListView(ListView):

    model = Tag
    template_name = "lim/tag-list.html"

    def get_context_data(self, **kwargs):
        context = super(TagListView, self).get_context_data(**kwargs)
        if self.request.user.is_superuser:
            context["tags"] = Tag.objects.all().order_by("author", "text")
            context["title"] = "All personal tags"
        else:
            context["tags"] = Tag.objects.filter(author=self.request.user).order_by(
                "text"
            )
            context["title"] = "My personal tags"
        return context


class TagLimerickListView(LimerickListView):
    # Display all limericks in the specified tag

    template_name = "lim/limerick-list-tag.html"

    def __init__(self):
        super(TagLimerickListView, self).__init__()
        self._tag = None

    @property
    def tag(self):
        if self._tag is None:
            if "tag_id" in self.kwargs:
                tag_pk = self.kwargs["tag_id"]
            else:
                tag_pk = self.request.GET.get("tag_id", None)
            self._tag = get_object_or_404(Tag, pk=tag_pk)
        return self._tag

    def get_queryset(self):
        return self.tag.limericks

    def get_title(self, context):
        return f"Limericks tagged <b>{context['tag'].text}</b>"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(tag=self.tag, tag_pk=self.tag.pk)
        return context


class TagListWithTOCJsonView(LimerickListWithTOCJsonMixin, TagLimerickListView):
    pass
