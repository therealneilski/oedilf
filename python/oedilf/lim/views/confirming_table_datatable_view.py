from django.db.models import *
from django.views.generic import TemplateView

from lim.models import LimStates
from lim.views.rfa_table_datatable_view import LimerickTableDataView


class ConfirmingTableDataView(LimerickTableDataView):

    columns = [
        "confirmation_countdown_end_date",
        "words",
        "verse",
        "author_names",
        "approving_editor",
        "pk",
    ]
    order_columns = columns

    def render_column(self, row, column):
        if column == "confirmation_countdown_end_date":
            return row.confirmation_countdown_end_date.strftime("%d %b %Y %H:%M:%S")
        elif column == "approving_editor":
            return row.approving_editor.username
        else:
            return super(ConfirmingTableDataView, self).render_column(row, column)

    def filter_queryset(self, qs):
        # use parameters passed in GET request to filter queryset

        search = self.request.GET.get('search[value]', None)
        if search:
            qs = qs.filter(
                Q(author_names__icontains=search)
                | Q(approving_editor__username__icontains=search)
                | Q(words__icontains=search)
                | Q(verse__icontains=search)
            )

        return qs

    def ordering(self, qs):
        qs = super(ConfirmingTableDataView, self).ordering(qs)
        if not qs.ordered:
            qs = qs.order_by("confirmation_countdown_end_date")

        return qs

    def get_initial_queryset(self):
        return (
            super(ConfirmingTableDataView, self)
            .get_initial_queryset()
            .filter(state=LimStates.CONFIRMING)
            .distinct()
        )


class ConfirmingTableView(TemplateView):

    template_name = "lim/datatables/confirming-datatable.html"
