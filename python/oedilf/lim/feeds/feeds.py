from django.contrib.auth.models import User
from django.contrib.syndication.views import Feed
from django.db.models import Q
from django.shortcuts import get_object_or_404, reverse
from django.utils import timezone
from django.utils.feedgenerator import Atom1Feed
from django.views.generic.base import RedirectView
from pinax.announcements.models import Announcement

from lim.models import Limerick, LimStates, Author


class OedilfFeed(Feed):
    # Using a template here doesn't work properly -
    # using the linebreaksbr filter causes the HTML tags to be double-escaped
    # (eg &amp;lt;br&amp;gt;)
    def item_description(self, item):
        result = (
            f"\"{item.formatted_defined_words}\" by {item.latest_revision.author_names}<br><br>"
            + item.latest_revision.verse.replace("\r\n", "<br/>\r\n")
        )
        if item.latest_revision.author_notes:
            result += "<br><br>" + item.latest_revision.author_notes.replace(
                "\r\n", "<br/>\r\n"
            )
        if item.latest_revision.editor_notes:
            result += (
                "<br><br>EDITOR NOTES:<br><br>"
                + item.latest_revision.editor_notes.replace("\r\n", "<br/>\r\n")
            )
        return result

    def items(self, _):
        return self.get_queryset()[:10]

    def item_title(self, item):
        return (
            f"\"{item.formatted_defined_words}\" by {item.latest_revision.author_names}"
        )

    def get_queryset(self, obj=None):
        return Limerick.objects.filter(state=LimStates.APPROVED).order_by('-state_date')


class UnfilteredLimericksRSSFeed(OedilfFeed):
    title = "OEDILF: New limericks (may contain adult content)"
    link = "/"
    description = "Newly approved limericks from The OEDILF (may contain adult content)"


class FilteredLimericksRSSFeed(UnfilteredLimericksRSSFeed):
    title = "OEDILF: New limericks"
    description = "Newly approved limericks from The OEDILF"

    def get_queryset(self, obj=None):
        return (
            super(FilteredLimericksRSSFeed, self).get_queryset().filter(curtained=False)
        )


class UnfilteredLimericksAtomFeed(UnfilteredLimericksRSSFeed):
    feed_type = Atom1Feed
    subtitle = UnfilteredLimericksRSSFeed.description


class FilteredLimericksAtomFeed(FilteredLimericksRSSFeed):
    feed_type = Atom1Feed
    subtitle = FilteredLimericksRSSFeed.description


# noinspection PyMethodMayBeStatic
class AuthorRSSFeed(OedilfFeed):
    def items(self, author):
        return self.get_queryset(author)[:10]

    def get_queryset(self, obj=None):
        return (
            super(AuthorRSSFeed, self)
            .get_queryset()
            .filter(Q(primary_author=obj.user) | Q(secondary_author=obj.user))
        )

    def get_object(self, request, *args, **kwargs):
        return get_object_or_404(Author, pk=kwargs["author_pk"])

    def title(self, author):
        return "OEDILF: New limericks by " + author.user.username

    def link(self, author):
        return reverse("limericks-by-author", kwargs=dict(author=author.pk))

    def description(self, author):
        return "Newly approved limericks from The OEDILF by " + author.user.username


class AuthorAtomFeed(AuthorRSSFeed):
    feed_type = Atom1Feed
    subtitle = AuthorRSSFeed.description


class LegacyAuthorRSSRedirectView(RedirectView):

    permanent = True
    pattern_name = "feed-author-rss"

    def get_redirect_url(self, *args, **kwargs):
        user = get_object_or_404(User, pk=kwargs["user_pk"])
        kwargs = {"author_pk": user.author.pk.hashid}
        return super(LegacyAuthorRSSRedirectView, self).get_redirect_url(
            *args, **kwargs
        )


class AnnouncementsRSSFeed(Feed):
    title = "OEDILF Announcements"
    link = "/"
    description = "Announcements from The OEDILF"

    def get_queryset(self, obj=None):
        return Announcement.objects.filter(
            Q(site_wide=True)
            & Q(members_only=False)
            & Q(publish_start__lte=timezone.now())
            & (Q(publish_end__isnull=True) | Q(publish_end__gte=timezone.now()))
        ).order_by("-publish_start")

    def items(self, _):
        return self.get_queryset()[:10]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.content

    def item_link(self, item):
        return item.get_absolute_url()

    def item_pubdate(self, item):
        return item.publish_start
