import random

from django.conf import settings
from django.db.models import Q

from .models import ActivityMessage, Limerick, LimStates, Project


def unseen_message_count(request):
    """
    Adds a count of unseen activity messages to every page context.
    :param request: HttpRequest
    :return: dictionary
    """
    if request.user.is_authenticated:
        new_message_count = ActivityMessage.objects.filter(
            Q(dest=request.user) & Q(seen=False)
        ).count()
    else:
        new_message_count = 0
    return {"new_message_count": new_message_count}


def get_random_approved_limerick(request):
    if hasattr(settings, "OEDILF_RANDOM_LIMERICK"):
        return {
            "random_limerick": Limerick.objects.get(pk=settings.OEDILF_RANDOM_LIMERICK)
        }

    include_curtained = (
        request.user.is_authenticated and request.user.author.curtain_filtering is False
    )
    if include_curtained:
        limericks = Limerick.objects.filter(state=LimStates.APPROVED)
    else:
        limericks = Limerick.objects.filter(curtained=False, state=LimStates.APPROVED)
    if limericks.exists():
        r = random.randint(0, limericks.count() - 1)
        pk = limericks.values_list("pk", flat=True)[r]
        return {"random_limerick": limericks.get(pk=pk)}
    else:
        return {"random_limerick": Limerick.objects.none()}


def active_projects(request):
    if request.user.is_authenticated:
        return {"active_projects": Project.objects.filter(active=True)}
    else:
        return {}
