from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.db.models import Q

from lim.models import Limerick, LimStates


class Command(BaseCommand):
    help = (
        "Produces a text dump of all limericks by the given author,"
        "optionally in the given state"
    )

    def add_arguments(self, parser):
        parser.add_argument("author", type=str, help="The author name to dump")
        parser.add_argument(
            "-s", "--state", type=str, help="Dump limericks in this state"
        )

    def handle(self, *args, **kwargs):
        author = User.objects.get(username=kwargs["author"])

        qs = Limerick.objects.filter(
            Q(primary_author=author) | Q(secondary_author=author)
        )

        if kwargs["state"] is None:
            state = None
        else:
            state = LimStates[kwargs["state"].upper()]
            qs = qs.filter(state=state)

        qs = qs.order_by("state_date")

        for l in qs:
            print(
                f"Limerick #{l.pk.id} on {l.formatted_defined_words} by {l.latest_revision.author_names}\n"
            )
            print(f"{l.latest_revision.verse}\n")
            if l.latest_revision.author_notes.strip() != "":
                print(f"{l.latest_revision.author_notes}\n")
            if l.latest_revision.editor_notes.strip() != "":
                print(f"\nEDITOR NOTES:\n\n{l.latest_revision.editor_notes}\n")
            print(f"Revised: {l.latest_revision.updated_at.strftime('%d %B %Y')}")
            if state is None:
                print("State: {!s}\n".format(LimStates(l.state)))
            print("\n-----\n")
