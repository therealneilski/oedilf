import django_tables2 as tables
from django.db.models import F
from django.urls import reverse
from django.utils.html import format_html

from lim.models import Author


class AuthorTable(tables.Table):
    class Meta:
        model = Author
        template = "django_tables2/bootstrap.html"
        fields = (
            "user",
            "approved_count",
            "submitted_count",
            "backlog",
            "confirming_count",
            "tentative_count",
            "revised_count",
            "held_count",
            "new_count",
            "stored_count",
            "excluded_count",
            "untended_count",
        )
        sequence = (
            "user",
            "approved_count",
            "submitted_count",
            "backlog",
            "confirming_count",
            "tentative_count",
            "revised_count",
            "held_count",
            "new_count",
            "stored_count",
            "excluded_count",
            "untended_count",
        )
        order_by = "-approved_count"
        attrs = {
            "data-cascade": "true",
            "data-expand-all": "true",
            # 'data-sorting': 'true',
            "class": "table table-striped",
            "id": "author-list",
        }

    user = tables.Column(
        attrs={"td": {"data-title": "Author"}},
        verbose_name="Author",
        order_by="user__username",
    )
    approved_count = tables.Column(attrs={"td": {"data-title": "Approved"}})
    confirming_count = tables.Column(attrs={"td": {"data-title": "Confirming"}})
    tentative_count = tables.Column(attrs={"td": {"data-title": "Tentative"}})
    revised_count = tables.Column(attrs={"td": {"data-title": "Revised"}})
    held_count = tables.Column(attrs={"td": {"data-title": "Held"}})
    new_count = tables.Column(attrs={"td": {"data-title": "New"}})
    untended_count = tables.Column(attrs={"td": {"data-title": "Untended"}})
    submitted_count = tables.Column(attrs={"td": {"data-title": "Submitted"}})
    backlog = tables.Column(attrs={"td": {"data-title": "Backlog"}})
    stored_count = tables.Column(attrs={"td": {"data-title": "Stored"}})
    excluded_count = tables.Column(attrs={"td": {"data-title": "Excluded"}})

    # noinspection PyPep8

    def before_render(self, request):
        self.user = request.user

    def render_state_count(self, record, value, state=None):
        value = value if int(value) != value else int(value)
        if self.user.is_authenticated:
            if state is None:
                states_query = ""
            else:
                states_query = f"?states={state}"
            return format_html(
                '<a href="{}{}" class="text-dark">{}</a>',
                reverse("limericks-by-author", kwargs={"author": record.user.pk}),
                states_query,
                value,
            )

        else:
            return value

    def render_approved_count(self, record, value):
        return self.render_state_count(record, value, "Approved")

    def render_submitted(self, record, value):
        return self.render_state_count(record, value)

    def render_confirming_count(self, record, value):
        return self.render_state_count(record, value, "Confirming")

    def render_tentative_count(self, record, value):
        return self.render_state_count(record, value, "Tentative")

    def render_revised_count(self, record, value):
        return self.render_state_count(record, value, "Revised")

    def render_held_count(self, record, value):
        return self.render_state_count(record, value, "Held")

    def render_new_count(self, record, value):
        return self.render_state_count(record, value, "New")

    def render_untended_count(self, record, value):
        return self.render_state_count(record, value, "Untended")

    def render_stored_count(self, record, value):
        return self.render_state_count(record, value, "Stored")

    def render_excluded_count(self, record, value):
        return self.render_state_count(record, value, "Excluded")

    @staticmethod
    def render_backlog(value):
        return "{0:.0f}%".format(100 * value)

    @staticmethod
    def render_user(record):
        result = format_html(
            """
                <div class="d-md-none text-center">
                    <a href="{}">
                        <h3>{}</h3>
                        <span class="text-muted">{}</span>
                    </a>
                </div>
                <div class="d-none d-md-block">
                    <a href="{}">
                        <p>{}<br/><span class="small text-muted">{}</span></p>
                    </a>
                </div>
            """,
            reverse("author-profile", kwargs={"author": record.user.pk}),
            record.user.username,
            record.author_rank.verbose_name(),
            reverse("author-profile", kwargs={"author": record.user.pk}),
            record.user.username,
            record.author_rank.verbose_name(),
        )
        if not record.is_active:
            result = format_html("<i>{}</i>", result)
        return result
