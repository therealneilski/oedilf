import django_tables2 as tables
from django.urls import reverse
from django.utils.html import format_html

from lim.models import ActivityMessage
from lim.templatetags import lim as lim_templates


def get_bootstrap_class(record):
    if record.mine:
        if not record.seen:
            return "font-weight-bold table-primary"
        else:
            return "table-info"
    elif not record.seen:
        return "font-weight-bold"
    else:
        return


class ActivityTable(tables.Table):
    class Meta:
        model = ActivityMessage
        template = "django_tables2/bootstrap.html"
        fields = ("source", "data", "author", "date_time", "mine")
        sequence = ("select", "seen", "source", "data", "author", "date_time")
        attrs = {"class": "table table-hover", "id": "activity-list"}
        order_by = ["-mine", "-revision__limerick_id"]
        row_attrs = {"class": lambda record: get_bootstrap_class(record)}

    source = tables.Column(
        attrs={"td": {"data-title": "Source"}}, verbose_name="From", orderable=False
    )
    data = tables.Column(
        attrs={"td": {"data-title": "Message"}}, verbose_name="Message", orderable=False
    )
    author = tables.Column(
        attrs={"td": {"data-title": "Author"}},
        verbose_name="Limerick Author",
        orderable=False,
    )
    date_time = tables.DateTimeColumn(
        attrs={"td": {"data-title": "Date"}}, verbose_name="Date", orderable=False
    )

    select = tables.TemplateColumn(
        """
        <div class="form-check">
            <input type="checkbox" class="form-check-input" name="checkbox-{{ record.pk }}" />
        </div>
        """,
        attrs={"td": {"align": "center"}},
        orderable=False,
    )

    seen = tables.Column(
        attrs={"td": {"data-title": "New", "align": "center"}},
        verbose_name="New",
        orderable=False,
    )

    mine = tables.Column(visible=False)

    @staticmethod
    def render_data(record):
        return format_html(
            """
                <div class="col-12">
                    <a href="{}">
                        <span class="badge badge-info">{}</span>
                        <span class="h6">
                            {}
                        </span>
                        <span class="text-muted small"> #{}</span>
                        <br/>
                        <span>{}</span>
                        <span class="sr-only">(unseen)</span>
                    </a>
                </div>
            """,
            reverse("workshop", args=[record.revision.limerick.pk]),
            record.message_type,
            record.revision.limerick.formatted_defined_words,
            record.revision.limerick.pk,
            format_html(record.data),
        )

    @staticmethod
    def render_user(user):
        return format_html(
            """
                <div class="col-12">
                    <a href="{}">
                        <div class="row">
                            <div class="avatar-list-img40x40 col-3" style="min-width: 45px">
                                {}
                            </div>
                            <div class="avatar-detail col-9">
                                <span class="h6" data-toggle="tooltip" title="{}">{}</span><br/>
                            </div>
                        </div>
                    </a>
                </div>
            """,
            reverse("author-profile", args=[user.pk]),
            format_html(lim_templates.lim_gravatar_image(user, 40)),
            user.author.author_rank.verbose_name(),
            user.username,
        )

    def render_source(self, record):
        return self.render_user(record.source)

    def render_author(self, record):
        return self.render_user(record.author)

    def render_seen(self, record):
        if record.seen:
            return format_html('')
        else:
            return format_html('<i class="fa fa-envelope"></i>')

    @staticmethod
    def render_date_time(value):
        return value.ctime()
