import bleach
from bs4 import BeautifulSoup
from crispy_forms import bootstrap
from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.shortcuts import reverse, get_object_or_404
from django.template.loader import render_to_string
from django.utils import timezone
from str2bool import str2bool

from lim.models import (
    Author,
    PredefinedFeedbackStyles,
    Comment,
    Revision,
    CommentActions,
    Limerick,
    LimStates,
    ActivityMessage,
    ActivityMessageType,
    SiteSettings,
)
from lim.templatetags.lim import lim_gravatar_image


class OedilfTextarea(forms.widgets.Textarea):

    template_name = "lim/forms/widgets/textarea.html"


TIMEZONES = list(
    (t, "GMT{}{}".format("" if t < 0 else "+", t))
    for t in range(-14, 14)
    # Note: the following should produce a dropdown with all timezones,
    # but is unbelievably slow.
    #
    # (pytz.timezone(tz).utcoffset(datetime.now()).seconds / 3600, tz)
    # for tz in pytz.common_timezones
)


class CheckboxField(Field):
    def __init__(self, *args, **kwargs):
        if "css_class" not in kwargs:
            kwargs["css_class"] = "custom-control-input"
        if "wrapper_class" not in kwargs:
            kwargs["wrapper_class"] = "custom-control custom-checkbox"
        super(CheckboxField, self).__init__(*args, **kwargs)


class CustomCheckbox(Field):
    template = "lim/forms/widgets/custom_checkbox.html"


class BlockRadios(Field):
    """
    Layout object for rendering radiobuttons as a block::

        Radios('field_name')
    """

    template = "%s/layout/radioselect_inline.html"

    def __init__(self, *args, **kwargs):
        if "css_class" not in kwargs:
            kwargs["css_class"] = "custom-control-input"
        if "wrapper_class" not in kwargs:
            kwargs["wrapper_class"] = "custom-control custom-checkbox"
        super(BlockRadios, self).__init__(*args, **kwargs)

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK, **kwargs):
        return super(BlockRadios, self).render(
            form, form_style, context, template_pack=template_pack, extra_context={}
        )


class TrumbowygCleanerMixin:
    @staticmethod
    def clean_trumbowyg_input(s):
        s = s.strip().replace("\r", "")
        if "<div" in s and not s.startswith("<div"):
            i = s.index("<div")
            s = "<div>{}</div>{}".format(s[:i], s[i:])
        import re

        s = re.sub(r"<div><br/?>\s*</div>", "<div><br></div>", s)
        s = re.sub(r"</div>[\r\n]+<div>", "</div><div>", s)
        s = bleach.clean(
            s,
            [
                "b",
                "i",
                "s",
                "strike",
                "sup",
                "sub",
                "big",
                "small",
                "tt",
                "u",
                "ul",
                "ol",
                "li",
                "h1",
                "h2",
                "h3",
                "h4",
                "div",
                "br",
            ],
        )
        soup = BeautifulSoup(s, "html.parser")
        for br in soup.find_all("br"):
            br.decompose()
        for div in soup.find_all("div"):
            div.append("\n")
            div.unwrap()
        message = str(soup).strip()
        message = re.sub(r"^\s*\n", "\n", message, re.MULTILINE)
        return message


class TrumbowygCommentMixin(TrumbowygCleanerMixin):
    def clean(self):
        cleaned_data = super(TrumbowygCommentMixin, self).clean()
        message = self.clean_trumbowyg_input(cleaned_data.get("message", ""))
        if message == "" and cleaned_data.get("action") in (None, "NONE"):
            self.add_error(
                "__all__", "Can't add empty comments (unless you are changing your RFA)"
            )
        cleaned_data["message"] = message
        return cleaned_data


class CommentForm(TrumbowygCommentMixin, forms.ModelForm):

    revision_id = forms.CharField()
    message = forms.CharField(required=False, widget=OedilfTextarea)
    save_add_rfa = forms.BooleanField(required=False)
    save_remove_rfa = forms.BooleanField(required=False)
    rfa_type = forms.CharField(required=False)
    curtain = forms.CharField(required=False)
    action = forms.CharField(required=False)

    class Meta:
        model = Comment
        fields = ["message", "revision_id", "id", "action"]

    def __init__(self, *args, **kwargs):
        self.revision = None
        if "instance" in kwargs:
            self.revision = kwargs["instance"]
        elif "data" in kwargs:
            revision_id = kwargs["data"]["revision_id"]
            self.revision = get_object_or_404(Revision, pk=revision_id)
        self.request = kwargs.pop("request", None)
        super(CommentForm, self).__init__(*args, **kwargs)
        self.setup_helper()

    @staticmethod
    def submit_button(action, colour, text):
        return bootstrap.StrictButton(
            text,
            css_class="w-100 btn btn-{}{}".format(
                "" if colour == "primary" else "outline-", colour
            ),
            type="button",
            name=f"action_{action.name.lower()}",
            id=f"action-{action.name.lower()}",
            value=action.value,
            onclick=f"addComment($('#add-comment-form form'), '{action.name}')",
        )

    def setup_helper(self):
        # noinspection PyAttributeOutsideInit
        self.helper = FormHelper()
        if self.request is not None and not self.request.user.is_authenticated:
            self.helper.layout = Layout()
            return

        self.helper.form_action = reverse("comment-create-ajax")
        self.helper.template_pack = "bootstrap4"
        self.helper.form_class = "form-horizontal needsvalidation"
        self.helper.form_show_labels = False
        self.helper.error_text_inline = False
        self.helper.field_class = "col-md-12"
        self.helper.form_group_wrapper_class = "form-group row"
        self.helper.attrs = {"novalidate": True}
        layout = Layout(
            Hidden("revision_id", self.revision.pk),
            HTML("<h5>Add a comment</h5>"),
            Div(
                Div(
                    bootstrap.StrictButton(
                        "Quote Limerick",
                        css_class="btn btn-outline-secondary btn-sm mp1",
                        onclick="quoteLimerick('#id_message')",
                    ),
                    bootstrap.StrictButton(
                        "Quote Author's Note",
                        css_class="btn btn-outline-secondary btn-sm m-1",
                        onclick="quoteAN('#id_message')",
                    ),
                ),
                HTML(
                    """
                    <label for="id_text-macros" class="d-none">Text macros</label>
                    <select class="ml-auto col-12 col-md-4 col-xl-6" id="id_text-macros" 
                            onchange="includeMacro('#id_text-macros', '#id_message');"></select>
                """
                ),
                css_class="d-flex col-12 justify-content-between",
            ),
            Field("message", css_class="trumbowyg", required=False),
            Hidden("rfa_type", "none"),
            Hidden("curtain", "none"),
            Hidden("action", ""),
        )

        form_action_buttons = [
            self.submit_button(
                CommentActions.NONE, "primary", '<i class="fa fa-comment"></i> Comment'
            )
        ]
        if self.request.user.has_perm("lim.add_rfa", self.revision.limerick):
            form_action_buttons.append(
                self.submit_button(
                    CommentActions.RFA_ADD,
                    "secondary",
                    '<i class="fa fa-check-circle"></i> Add RFA',
                )
            )
        if self.request.user.has_perm("lim.remove_rfa", self.revision.limerick):
            form_action_buttons.append(
                self.submit_button(
                    CommentActions.RFA_REMOVE,
                    "secondary",
                    '<i class="fa fa-minus-circle"></i> Remove RFA',
                )
            )
        if self.request.user.has_perm("lim.hold", self.revision.limerick):
            form_action_buttons.append(
                self.submit_button(
                    CommentActions.HOLD,
                    "warning",
                    '<i class="fa fa-pause-circle"></i> Hold',
                )
            )
        if self.request.user.has_perm("lim.unhold", self.revision.limerick):
            form_action_buttons.append(
                self.submit_button(
                    CommentActions.UNHOLD,
                    "warning",
                    '<i class="fa fa-pause-circle"></i> Unhold',
                )
            )
        if self.request.user.has_perm("lim.add_curtain", self.revision.limerick):
            form_action_buttons.append(
                self.submit_button(
                    CommentActions.ADD_CURTAIN,
                    "warning",
                    '<i class="fa fa-exclamation-triangle"></i> Add curtain',
                )
            )
        if self.request.user.has_perm("lim.remove_curtain", self.revision.limerick):
            form_action_buttons.append(
                self.submit_button(
                    CommentActions.REMOVE_CURTAIN,
                    "warning",
                    '<i class="fa fa-exclamation-triangle"></i> Remove curtain',
                )
            )
        if self.request.user.has_perm("lim.clear_rfas", self.revision.limerick):
            form_action_buttons.append(
                self.submit_button(
                    CommentActions.CLEAR_RFAS,
                    "danger",
                    '<i class="fa fa-times-circle"></i> Clear ALL RFAs',
                )
            )
        if self.request.user.has_perm("lim.call_for_attention", self.revision.limerick):
            form_action_buttons.append(
                self.submit_button(
                    CommentActions.CFA,
                    "danger",
                    '<i class="fa fa-exclamation-circle"></i> Call For Attention',
                )
            )
        form_action_buttons.append(
            Reset(
                "cancel",
                "Cancel",
                css_class="btn btn-flat w-100",
                onclick="$('#id_message').trumbowyg('html', '')",
            )
        )

        self.helper.layout = Layout(
            layout,
            Layout(
                Div(
                    bootstrap.FormActions(
                        *form_action_buttons,
                        css_class="mt-2 col-12 col-lg-4 btn-group-vertical",
                        role="group",
                    ),
                    HTML(
                        render_to_string(
                            "lim/inclusion/author-preferences.html",
                            {"author": self.revision.primary_author.author},
                        )
                    ),
                    css_class="row",
                ),
                HTML("""<a id="comment-{}"></a>""".format(self.revision.version)),
            ),
        )


class CommentUpdateForm(TrumbowygCommentMixin, forms.ModelForm):

    comment_id = forms.CharField()
    message = forms.CharField(required=False, widget=OedilfTextarea)

    class Meta:
        model = Comment
        fields = ["message", "id"]

    def __init__(self, *args, **kwargs):
        if "instance" in kwargs:
            self.revision = kwargs["instance"].revision
            self.comment = kwargs["instance"]
        super(CommentUpdateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.setup_helper()

    def setup_helper(self):
        self.helper.form_action = reverse(
            "comment-update", kwargs={"pk": self.comment.pk}
        )
        self.helper.template_pack = "bootstrap4"
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "d-none"
        self.helper.field_class = "col-md-12"
        self.helper.form_group_wrapper_class = "form-group row"
        self.helper.layout = Layout(
            Hidden("comment_id", self.comment.pk if self.comment else 0),
            HTML(
                lim_gravatar_image(self.comment.author, 20)
                + ' <span class="h5 mb-1 mx-1" id="comment_author_'
                + str(self.comment.pk)
                + '">'
                + (
                    "Info"
                    if self.comment.author is None
                    else self.comment.author.username
                )
                + "</span>"
            ),
            Div(
                Div(
                    bootstrap.StrictButton(
                        "Quote Limerick",
                        css_class="btn btn-outline-secondary btn-sm",
                        onclick=f"quoteLimerick('#id_message-{self.comment.pk}')",
                    ),
                    bootstrap.StrictButton(
                        "Quote Author's Note",
                        css_class="btn btn-outline-secondary btn-sm",
                        onclick=f"quoteAN('#id_message-{self.comment.pk}')",
                    ),
                ),
                HTML(
                    """
                    <label for="id_text-macros" class="d-none">Text macros</label>
                    <select class="ml-auto col-12 col-md-4" id="id_text-macros-{}"
                            onchange="includeMacro('#id_text-macros-{}', '#id_message-{}');">
                    </select>
                """.format(
                        self.comment.pk, self.comment.pk, self.comment.pk
                    )
                ),
                css_class="d-flex col-12 justify-content-between",
            ),
            Field(
                "message",
                css_class="trumbowyg col-12",
                id=f"id_message-{self.comment.pk}",
                template="bootstrap4/layout/textarea.html",
            ),
            bootstrap.FormActions(
                HTML(
                    f"""
                    <a role="button" class="btn btn-primary mb-2" href="javascript:updateComment('{self.comment.pk}')">
                        <i class="fa fa-comment"></i> Update comment
                    </a>
                """
                ),
                HTML(
                    '<a role="button" class="btn btn-secondary mb-2" '
                    'href="javascript:cancelUpdateComment({})">Cancel</a>'.format(
                        self.comment.pk
                    )
                ),
            ),
        )


class AuthorProfileManageForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = [
            "real_name",
            "biography",
            "notify_changes",
            "monitor_new",
            "monitor_comments",
            "monitor_attention",
            "use_email",
            "feedback_style_standard",
            "feedback_style",
            "time_zone",
            "monitor_rfas",
            "curtain_filtering",
            "text_macro_buttons",
            "ws_comment_display_limit",
            "lat",
            "lng",
            "born_in",
            "living_in",
            "approval_emails",
            "milestone_emails",
            "news_emails",
            "twitter_handle",
        ]
        widgets = {
            "time_zone": forms.Select(
                choices=TIMEZONES,
                attrs={
                    "data-style": "btn btn-primary mt-5",
                    "data-live-search": "true",
                    "style": "min-width: 10rem",
                },
            ),
            "ws_comment_display_limit": forms.Select(
                choices=((25, 25), (50, 50), (0, "unlimited")),
                attrs={
                    "data-style": "btn btn-primary mt-5",
                    "style": "min-width: 10rem",
                },
            ),
        }

    def __init__(self, *args, **kwargs):
        super(AuthorProfileManageForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.template_pack = "bootstrap4"
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-md-2"
        self.helper.field_class = "col-md-8"
        self.helper.form_group_wrapper_class = "form-group row"
        notifications_layout = Div(
            Div(
                HTML('<h5 class="card-title">Notifications</h5>'),
                CustomCheckbox("notify_changes"),
                BlockRadios("monitor_new", wrapper_class="form-row"),
                BlockRadios("monitor_comments", wrapper_class="form-row"),
                CustomCheckbox("monitor_attention"),
                CustomCheckbox("approval_emails"),
                CustomCheckbox("milestone_emails"),
                CustomCheckbox("news_emails"),
                css_class="card-body",
            ),
            css_class="card w-100 my-2",
        )
        writing_layout = Div(
            Div(
                HTML('<h5 class="card-title">Writing preferences</h5>'),
                BlockRadios("feedback_style_standard", wrapper_class="form-row"),
                Field(
                    "feedback_style",
                    wrapper_class="row"
                    if self.instance.feedback_style_standard
                    == PredefinedFeedbackStyles.CUSTOM
                    else "collapse",
                ),
                css_class="card-body",
            ),
            css_class="card w-100 my-2",
        )
        workshopping_layout = Div(
            Div(
                HTML('<h5 class="card-title">In workshops</h5>'),
                CustomCheckbox("curtain_filtering"),
                BlockRadios("monitor_rfas", wrapper_class="row"),
                CustomCheckbox("text_macro_buttons"),
                Field("ws_comment_display_limit", css_class="select2"),
                css_class="card-body",
            ),
            css_class="card w-100 my-2",
        )
        personal_layout = Div(
            Div(
                HTML('<h5 class="card-title">Personal information</h5>'),
                Field("biography", css_class="textarea"),
                Field("time_zone", css_class="select2"),
                MultiField(
                    "Where are you?",
                    Field(
                        "lat",
                        input_type="hidden",
                        css_class="gllpLatitude",
                        wrapper_class="d-none",
                        value=self.instance.lat,
                    ),
                    Field(
                        "lng",
                        input_type="hidden",
                        css_class="gllpLongitude",
                        wrapper_class="d-none",
                        value=self.instance.lng,
                    ),
                    template="bootstrap4/layout/googlemaps-latlong-picker.html",
                ),
                Field("born_in", blank=False),
                Field("living_in", blank=False),
                bootstrap.PrependedText("twitter_handle", "@", blank=False),
                css_class="card-body",
            ),
            css_class="card w-100 my-2",
        )
        self.helper.layout = Layout(
            Row(
                Field("real_name", wrapper_class="pt-2 mt-2 col-12"),
                css_class="form-row w-100 col-12",
            ),
            notifications_layout,
            writing_layout,
            workshopping_layout,
            personal_layout,
            bootstrap.FormActions(
                Submit("save", "Save changes"),
                HTML(
                    '<a href="{}" class="btn btn-secondary" role="button">Cancel</a>'.format(
                        self.instance.get_absolute_url()
                    )
                ),
            ),
        )
        self.helper.form_method = "post"
        self.helper.form_action = reverse(
            "manage-author-profile", kwargs={"author": self.instance.pk}
        )


class RevisionForm(TrumbowygCleanerMixin, forms.ModelForm):

    verse = forms.CharField(required=True, widget=OedilfTextarea)
    all_defined_words = forms.CharField(
        required=True,
        widget=forms.Textarea(attrs={"rows": 3, "cols": 20}),
        help_text="One word per line. All lowercase except for proper nouns or acronyms. "
        "Just add the word, not the definition.",
    )
    author_notes = forms.CharField(required=False, widget=OedilfTextarea)
    editor_notes = forms.CharField(required=False, widget=OedilfTextarea)
    workshop_comment = forms.CharField(
        required=True, widget=OedilfTextarea(attrs={"rows": 5})
    )
    limerick_id = forms.CharField(required=True)
    confirm_short_verse = forms.BooleanField(
        required=False,
        label="Confirm short verse",
        help_text="The verse you have supplied has fewer than five lines, which is the usual minimum "
        "for a limerick.  Your limerick might be rejected. Please confirm your understanding of this.",
    )
    confirm_dw_out_of_range = forms.BooleanField(
        required=False,
        label="Confirm defined word out of range",
        help_text="The first word you have listed above is outside our current range"
        # " (Aa-{})"
        ". Your limerick might be rejected.  Please confirm your understanding of this.".format(
            "",
            # site_settings.alphabet_end.title()
        ),
    )
    confirm_dw_absent = forms.BooleanField(
        required=False,
        label="Confirm defined word not in verse or notes",
        help_text="Your first defined word does not appear boldfaced in the verse or author's note. "
        "Please confirm that this is intentional.",
    )
    primary_author = forms.ChoiceField(required=True)
    secondary_author = forms.ChoiceField(required=False)

    class Meta:
        model = Revision
        fields = [
            "verse",
            "all_defined_words",
            "author_notes",
            "editor_notes",
            "workshop_comment",
            "limerick_id",
            "confirm_short_verse",
            "confirm_dw_out_of_range",
            "confirm_dw_absent",
            "primary_author",
            "secondary_author",
        ]

    def __init__(self, *args, **kwargs):
        self.limerick = self._get_limerick(kwargs)
        self.request = kwargs.pop("request", None)
        super(RevisionForm, self).__init__(*args, **kwargs)
        self.setup_helper()

    @staticmethod
    def _get_limerick(kwargs):
        limerick_id = None
        if "instance" in kwargs:
            limerick_id = kwargs["instance"].limerick.pk
        elif "limerick_id" in kwargs:
            limerick_id = kwargs.pop("limerick_id")
        elif "data" in kwargs:
            limerick_id = kwargs["data"]["limerick_id"]
        return get_object_or_404(Limerick, pk=limerick_id)

    def _get_limerick_pk(self):
        return self.limerick.pk

    def setup_helper(self):
        # noinspection PyAttributeOutsideInit
        self.helper = FormHelper()
        self.helper.form_id = "form-add-revision"
        self.helper.form_action = self._get_form_action()
        self.helper.template_pack = "bootstrap4"
        self.helper.form_class = "form-horizontal needsvalidation"
        self.helper.label_class = "col-md-2"
        self.helper.field_class = "col-md-10"
        self.helper.form_show_labels = True
        self.helper.help_text_inline = False
        self.helper.error_text_inline = True
        self.helper.error_text_in_label = True
        self.helper.form_group_wrapper_class = "form-group row"
        self.helper.attrs = {"novalidate": True}
        checkbox_cdoor = None
        checkbox_short = None
        checkbox_dw_absent = None
        if hasattr(self, "cleaned_data"):
            if not self.first_dw_in_range():
                checkbox_cdoor = CheckboxField(
                    "confirm_dw_out_of_range",
                    required=True,
                    id="checkbox-confirm_dw_out_of_range",
                    css_class="checkbox checkbox-input",
                )
            if len(self.cleaned_data.get("verse", "").split("\n")) < 5:
                checkbox_short = CheckboxField(
                    "confirm_short_verse",
                    required=True,
                    id="checkbox-confirm_short_verse",
                )
            if not self.first_dw_in_verse_or_an():
                checkbox_dw_absent = CheckboxField(
                    "confirm_dw_absent",
                    required=True,
                    id="checkbox-dw-absent",
                    css_class="checkbox checkbox-input",
                )
        layout = Layout(
            Hidden("limerick_id", self._get_limerick_pk()),
            Field("all_defined_words", required=True),
            checkbox_cdoor,
            checkbox_dw_absent,
            *self._get_author_buttons(),
            Field("verse", css_class="trumbowyg", required=True),
            checkbox_short,
            Field("author_notes", css_class="trumbowyg", required=False),
            Field(
                "workshop_comment",
                css_class="trumbowyg",
                placeholder="(enter a description of your changes)",
                required=False,
            )
            if "workshop_comment" in self._meta.fields
            else HTML(""),
        )
        if self.request.user.is_authenticated and self.request.user.has_perm(
            "lim.is_approving_editor"
        ):
            layout.append(Field("editor_notes", css_class="trumbowyg", required=False))
        self.helper.layout = Layout(
            layout,
            Layout(
                bootstrap.FormActions(
                    *self._get_form_action_buttons(), css_class="mt-2"
                )
            ),
        )

    def _get_form_action_buttons(self):
        return [
            HTML(
                """
                <a role="button" class="btn btn-primary mb-2" id="revision-submit"
                   onclick="addRevision('form-add-revision')" href="#">
                    <i class="fa fa-edit"></i> Add revision
                </a>
            """
            ),
            Reset(
                "cancel",
                "Cancel",
                css_class="btn btn-flat mb-2",
                onclick="window.location.href='"
                + reverse("workshop", kwargs={"limerick_id": self.limerick.pk})
                + "'",
            ),
        ]

    def _get_author_buttons(self):
        return (
            Field("primary_author", css_class="author-select"),
            Field("secondary_author", css_class="author-select"),
        )

    def _get_form_action(self):
        return reverse("add-revision", kwargs={"limerick_id": self.limerick.pk})

    def clean(self):
        cleaned_data = super(RevisionForm, self).clean()
        cleaned_data["verse"] = self.clean_trumbowyg_input(
            cleaned_data.get("verse", "")
        )
        cleaned_data["author_notes"] = self.clean_trumbowyg_input(
            cleaned_data.get("author_notes", "")
        )
        cleaned_data["editor_notes"] = self.clean_trumbowyg_input(
            cleaned_data.get("editor_notes", "")
        )
        cleaned_data["workshop_comment"] = self.clean_trumbowyg_input(
            cleaned_data.get("workshop_comment", "")
        )

        if len(cleaned_data["verse"].split("\n")) < 5 and not self.cleaned_data.get(
            "confirm_short_verse", False
        ):
            self.add_error(
                "confirm_short_verse", forms.ValidationError("Confirmation needed")
            )

        if not self.first_dw_in_range() and not self.cleaned_data.get(
            "confirm_dw_out_of_range", False
        ):
            self.add_error(
                "confirm_dw_out_of_range", forms.ValidationError("Confirmation needed")
            )

        if not self.first_dw_in_verse_or_an() and not self.cleaned_data.get(
            "confirm_dw_absent", False
        ):
            self.add_error(
                "confirm_dw_absent", forms.ValidationError("Confirmation needed")
            )

        for key in "primary_author", "secondary_author":
            author_id = self.data.get(key, None)
            if author_id is not None and author_id != "":
                user = User.objects.filter(pk=int(author_id))
                if user.exists():
                    cleaned_data[key] = user.first()
                    del self._errors[key]
                else:
                    self.add_error(
                        key, forms.ValidationError("No such user: {}".format(author_id))
                    )
            else:
                cleaned_data[key] = None

        return cleaned_data

    def first_dw_in_range(self):
        return (
            self.cleaned_data.get("all_defined_words", "zz").split("\n")[0][:2].lower()
            <= SiteSettings.get_solo().alphabet_end
        )

    def first_dw_in_verse_or_an(self):
        for key in "verse", "author_notes":
            first_dw = (
                self.cleaned_data.get("all_defined_words", "zz")
                .split("\r\n")[0]
                .lower()
            )
            soup = BeautifulSoup(self.cleaned_data.get(key, ""), "html.parser")
            for candidate in soup.find_all("b"):
                if candidate.text.strip().lower() == first_dw.lower():
                    return True

        return False


class LimerickForm(RevisionForm):

    verse = forms.CharField(required=True, widget=OedilfTextarea)
    all_defined_words = forms.CharField(
        required=True,
        widget=forms.Textarea(attrs={"rows": 3, "cols": 20}),
        help_text="One word per line. All lowercase except for proper nouns or acronyms. "
        "Just add the word, not the definition.",
    )
    author_notes = forms.CharField(required=False, widget=OedilfTextarea)
    editor_notes = forms.CharField(required=False, widget=OedilfTextarea)
    workshop_comment = forms.CharField(
        required=False, widget=OedilfTextarea(attrs={"rows": 5})
    )
    limerick_id = forms.IntegerField(required=False, initial=-1)
    confirm_short_verse = forms.BooleanField(
        required=False,
        label="Confirm short verse",
        help_text="The verse you have supplied has fewer than five lines, which is the usual minimum "
        "for a limerick.  Your limerick might be rejected. Please confirm your understanding of this.",
    )
    confirm_dw_out_of_range = forms.BooleanField(
        required=False,
        label="Confirm defined word out of range",
        help_text="The first word you have listed above is outside our current range"
        # " (Aa-{})"
        ". Your limerick might be rejected.  Please confirm your understanding of this.".format(
            "",
            # site_settings.alphabet_end.title()
        ),
    )
    confirm_dw_absent = forms.BooleanField(
        required=False,
        label="Confirm defined word not in verse or notes",
        help_text="Your first defined word does not appear in the verse or author's note. "
        "Please confirm that this is intentional.",
    )
    primary_author = forms.ChoiceField(required=False, initial="")
    secondary_author = forms.ChoiceField(required=False)

    class Meta:
        model = Revision
        fields = [
            "verse",
            "all_defined_words",
            "author_notes",
            "editor_notes",
            "confirm_short_verse",
            "confirm_dw_out_of_range",
            "confirm_dw_absent",
            "primary_author",
        ]

    @staticmethod
    def _get_limerick(kwargs):
        return Limerick()

    def _get_form_action(self):
        return reverse("add-limerick")

    def _get_author_buttons(self):
        return (
            Field("primary_author", css_class="author-select", disabled="disabled"),
        )

    def _get_form_action_buttons(self):
        return [
            HTML(
                """
                <a role="button" class="btn btn-primary mb-2" id="limerick-submit"
                   onclick="addLimerick('form-add-revision')" href="#">
                    <i class="fa fa-edit"></i> Add limerick
                </a>
            """
            ),
            Reset(
                "cancel",
                "Cancel",
                css_class="btn btn-flat mb-2",
                onclick="window.location.href='" + reverse("add-limerick") + "'",
            ),
        ]

    def _get_limerick_pk(self):
        return -1


class LimerickAuthorApprovalForm(forms.ModelForm):

    limerick_id = forms.IntegerField(widget=forms.HiddenInput())

    class Meta:
        model = Limerick
        fields = ["limerick_id"]

    def __init__(self, *args, **kwargs):
        self.limerick = self._get_limerick(kwargs)
        self.request = kwargs.pop("request", None)
        super(LimerickAuthorApprovalForm, self).__init__(*args, **kwargs)

    @staticmethod
    def _get_limerick(kwargs):
        limerick_id = None
        if "instance" in kwargs:
            limerick_id = int(kwargs["instance"].limerick.pk)
        elif "limerick_id" in kwargs:
            limerick_id = int(kwargs.pop("limerick_id"))
        elif "data" in kwargs:
            limerick_id = int(kwargs["data"]["limerick_id"])
        return get_object_or_404(Limerick, pk=limerick_id)

    def _get_limerick_pk(self):
        return self.limerick.pk


class LimerickAuthorRejectionForm(TrumbowygCleanerMixin, forms.ModelForm):

    limerick_id = forms.IntegerField()
    rejection_message = forms.CharField(required=True, widget=OedilfTextarea)

    class Meta:
        model = Limerick
        fields = ["limerick_id", "rejection_message"]

    def __init__(self, *args, **kwargs):
        self.limerick = self._get_limerick(kwargs)
        self.request = kwargs.pop("request", None)
        super(LimerickAuthorRejectionForm, self).__init__(*args, **kwargs)

    @staticmethod
    def _get_limerick(kwargs):
        limerick_id = None
        if "instance" in kwargs:
            limerick_id = int(kwargs["instance"].limerick.pk)
        elif "limerick_id" in kwargs:
            limerick_id = int(kwargs.pop("limerick_id"))
        elif "data" in kwargs:
            limerick_id = int(kwargs["data"]["limerick_id"])
        return get_object_or_404(Limerick, pk=limerick_id)

    def _get_limerick_pk(self):
        return self.limerick.pk


class ActivityUpdateForm(forms.Form):
    """
    The Activity list is a dynamic form - we don't know ahead of time
    what the names ofr the checkboxes will be.
    So we need to fudge the form to think it has all the right values.
    see https://jacobian.org/2010/feb/28/dynamic-form-generation/
    """

    action = forms.CharField()

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(ActivityUpdateForm, self).__init__(*args, **kwargs)

        for am in ActivityMessage.objects.filter(dest=user):
            self.fields[f"checkbox-{am.pk}"] = forms.BooleanField(required=False)


class LimerickSearchForm(forms.Form):

    # We deliberately use short variable names here
    # because we want to be able to GET searches rather than POST
    # and it prevents the URL from becoming excessively long.
    # With apologies to future me.

    q = forms.CharField(label="Search", required=False)
    s_dw = forms.BooleanField(
        label="Search defined words", required=False, initial=True
    )
    s_l = forms.BooleanField(label="Search in limerick", required=False, initial=True)
    s_an = forms.BooleanField(
        label="Search in author notes", required=False, initial=True
    )
    s_c = forms.BooleanField(label="Search in comments", required=False, initial=False)
    project = forms.CharField(required=False, widget=forms.HiddenInput)
    a = forms.CharField(
        label="Author",
        required=False,
        widget=forms.Select(attrs={"class": "author-select"}),
    )
    r_l = forms.ChoiceField(
        required=False,
        label="RFAs (at least)",
        initial=0,
        choices=zip(range(6), range(6)),
    )
    r_m = forms.ChoiceField(
        required=False,
        label="RFAs (at most)",
        widget=forms.RadioSelect,
        choices=(
            (0, "0"),
            (1, "1"),
            (2, "2"),
            (3, "3"),
            (4, "4"),
            (5, "5"),
            ("", "(Any)"),
        ),
    )
    co_a = forms.CharField(
        required=False,
        label="Include co-authored limericks?",
        widget=forms.Select(
            choices=(
                ("b", "Both sole and co-authored limericks"),
                ("n", "Sole author only"),
                ("y", "With co-author only"),
            ),
            attrs={"data-style": "btn btn-primary mt-5", "style": "min-width: 10rem"},
        ),
    )
    c_by = forms.CharField(
        label="With comment by",
        required=False,
        widget=forms.Select(attrs={"class": "author-select"}),
    )
    y = forms.IntegerField(required=False, label="Submission year", initial="")
    adv = forms.BooleanField(required=False, initial=True, widget=forms.HiddenInput)
    s = forms.MultipleChoiceField(
        required=False,
        label="State",
        widget=forms.CheckboxSelectMultiple,
        choices=LimStates.choices(),
    )

    def clean_integer_field(self, field_name):
        val = self.cleaned_data[field_name]
        if val is None or val == "":
            return -1
        else:
            return int(val)

    def clean(self):
        cleaned_data = super(LimerickSearchForm, self).clean()
        if cleaned_data["q"] == "" and (
            cleaned_data["a"] is None
            or cleaned_data["a"] == 0
            or cleaned_data["a"] == ""
        ):
            raise ValidationError(
                "You cannot leave both the search field and the author blank",
                code="no_excessive_search",
            )
        if "adv" not in self.data:
            user_q = User.objects.filter(username=cleaned_data["q"])
            if user_q.exists():
                user = user_q.first()
                cleaned_data["a"] = user.pk
                cleaned_data["q"] = ""
        for key in cleaned_data:
            if isinstance(cleaned_data[key], str):
                if cleaned_data[key] == "on":
                    return True
                else:
                    return str2bool(cleaned_data[key])
            elif cleaned_data[key] is None:
                return False
            else:
                return cleaned_data[key]

    def clean_r_l(self):
        return self.clean_integer_field("r_l")

    def clean_r_m(self):
        return self.clean_integer_field("r_m")

    def clean_y(self):
        return self.clean_integer_field("y")

    def clean_adv(self):
        return "adv" in self.data

    def clean_user_field(self, field_name):
        if self.cleaned_data[field_name] == "":
            return None
        else:
            try:
                if User.objects.filter(pk=int(self.cleaned_data[field_name])).exists():
                    return int(self.cleaned_data[field_name])
                else:
                    return None
            except ValueError:
                return None

    def clean_a(self):
        return self.clean_user_field("a")

    def clean_c_by(self):
        return self.clean_user_field("c_by")

    def clean_co_a(self):
        if self.cleaned_data["co_a"] in ("b", "n", "y"):
            return self.cleaned_data["co_a"]
        else:
            return "b"

    def clean_s(self):
        if len(self.cleaned_data["s"]) == 0:
            return [state.value for state in LimStates if state != LimStates.OBSOLETE]
        else:
            return list(map(int, self.cleaned_data["s"]))

    def __init__(self, *args, **kwargs):
        super(LimerickSearchForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Div(
            Div(
                Div(
                    HTML(
                        "<h5>{}</h5>".format(
                            "Refine search" if self.is_bound else "Search"
                        )
                    ),
                    css_class="card-header",
                ),
                Div(self.helper.build_default_layout(self), css_class="card-body"),
                Div(Submit("submit", "Search"), css_class="card-footer"),
                css_class="card",
            ),
            css_class="col-12",
        )
        self.helper.form_id = "search_form"
        self.helper.template_pack = "bootstrap4"
        self.helper.form_class = "form-horizontal needsvalidation"
        self.helper.form_method = "get"
        self.helper.form_action = reverse('search')

        self.helper['r_l'].wrap(bootstrap.InlineRadios)
        self.helper['r_m'].wrap(bootstrap.InlineRadios)
        self.helper['s'].wrap(bootstrap.InlineCheckboxes)

        # self.helper.add_input(Submit('submit', 'Search'))


class RequiredWordForm(forms.Form):

    prefix = forms.CharField(label="prefix", required=False)
    page = forms.IntegerField(initial=1, required=False)

    def clean_prefix(self):
        prefix = self.cleaned_data["prefix"].lower()
        if prefix == "non-letter" or (
            prefix is not None
            and len(prefix) >= 2
            and "a" <= prefix[0] <= "z"
            and "a" <= prefix[1] <= "z"
        ):
            return prefix
        else:
            return ""


class UserMessageForm(TrumbowygCleanerMixin, forms.ModelForm):

    data = forms.CharField(required=True, widget=OedilfTextarea(attrs={"rows": 5}))
    dest = forms.ChoiceField(required=True)
    source = forms.ChoiceField(required=True)
    author = forms.ChoiceField(required=True)
    in_reply_to = forms.CharField(required=False)

    class Meta:
        model = ActivityMessage
        fields = ["source", "author", "dest", "data", "in_reply_to"]

    def setup_helper(self):
        # noinspection PyAttributeOutsideInit
        self.helper = FormHelper()
        self.helper.form_id = "form-add-comment"
        self.helper.form_action = reverse(
            "user-message-new", kwargs={"author_pk": self.destination_author.pk}
        )
        self.helper.template_pack = "bootstrap4"
        self.helper.form_class = "form-horizontal needsvalidation"
        self.helper.layout = Layout(
            Hidden("source", self.request.user.author.pk),
            Hidden("author", self.request.user.author.pk),
            Hidden("dest", self.destination_author.pk),
            Hidden("in_reply_to", self.in_reply_to),
            Field("primary_author", css_class="author-select"),
            Field(
                "data",
                css_class="trumbowyg",
                placeholder="(Use these transient messages for gaining attention,"
                "not for passing messages of lasting value.)",
                required=True,
            ),
            bootstrap.FormActions(
                Submit("submit", "Send message"),
                Reset("reset", "Reset"),
                css_class="mt-2",
            ),
        )
        self.fields["data"].label = "Message"

    def clean(self):
        cleaned_data = super(UserMessageForm, self).clean()
        cleaned_data["data"] = self.clean_trumbowyg_input(cleaned_data.get("data", ""))
        for key in "source", "dest", "author":
            author_id = self.data.get(key, None)
            if author_id is not None and author_id != "":
                author = get_object_or_404(Author, pk=author_id)
                cleaned_data[key] = author.user
                del self._errors[key]
            else:
                self.add_error(
                    key, forms.ValidationError("No such user: {}".format(author_id))
                )
        if "in_reply_to" in cleaned_data:
            cleaned_data["in_reply_to"] = ActivityMessage.objects.filter(
                pk=cleaned_data["in_reply_to"]
            )
        else:
            cleaned_data["in_reply_to"] = ActivityMessage.objects.none()
        return cleaned_data

    def save(self, commit=True):
        self.cleaned_data["in_reply_to"].delete()
        self.instance.message_type = ActivityMessageType.USER_MESSAGE
        self.instance.date_time = timezone.now()
        return super(UserMessageForm, self).save(commit=commit)


class NewUserMessageForm(UserMessageForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        self.destination_author = kwargs.pop("destination_author", None)
        self.in_reply_to = None
        super(NewUserMessageForm, self).__init__(*args, **kwargs)
        self.setup_helper()


class ReplyUserMessageForm(UserMessageForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        self.destination_author = kwargs.pop("destination_author", None).author
        self.in_reply_to = kwargs.pop("in_reply_to", None)
        super(ReplyUserMessageForm, self).__init__(*args, **kwargs)
        self.setup_helper()
