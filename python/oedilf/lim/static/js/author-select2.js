function formatAuthor(user) {
    return user.username;
}

function formatAuthorSelection(user) {
    if (user.id === '') {
        return "Pick an author";
    }
    return user.text || user.username;
}

function startAuthorSelect(formatAuthorFunction, formatAuthorSelectionFunction) {

    <!-- Selectbox with search -->
    $(".author-select").select2({
        <!--theme: "bootstrap4",-->
        placeholder: "Pick an author",
        allowClear: true,
        width: "100%",
        ajax: {
            url: "/api/users/",
            dataType: 'json',
            data: function (params) {
              return {
                search: params.term,
                page: params.page || 1,
                ordering: "username"
              };
              // Query parameters will be ?search=[term]&page=[page]
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.results,
                    pagination: {
                        more: (params.page * 100) < data.count
                    }
                };
            }
        },
        //minimumInputLength: 1,
        templateResult: formatAuthorFunction,
        templateSelection: formatAuthorSelectionFunction,
        escapeMarkup: function (markup) { return markup; },
        debug: true,
    });
}


