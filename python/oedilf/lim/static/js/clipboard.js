$(document).ready(function () {
    var clipboard = new ClipboardJS('.clipboard-js');
    clipboard.on("success", function (e) {
        var btn = $(e.trigger);
        btn.attr("data-original-title", "Copied!").tooltip("show");
    });
    $(".clipboard-js").on("hidden.bs.tooltip", function () {
            $(".clipboard-js").attr("data-original-title", "Copy reference to clipboard");
        });
});
