var trumbowyg_options = {
        semantic: false,
        btns: [
            ['undo', 'redo'],
            ['bold', 'italic', 'underline', 'strike'],
            ['superscript', 'subscript'],
            ['removeformat'],
            ['viewHTML'],
            ['fullscreen'],
        ],
        removeformatPasted: true,
    };

