import copy

from .settings import *

INSTALLED_APPS.append("django_behave")
TEST_RUNNER = "django_behave.runner.DjangoBehaveTestSuiteRunner"

DATABASES["default"]["TEST"] = copy.copy(DATABASES["default"])
DATABASES["default"]["TEST"]["NAME"] = "test_oedilf"

TESTING = 1
os.environ["TESTING"] = "1"
logging.getLogger("factory").setLevel(logging.WARN)
logging.getLogger("faker").setLevel(logging.WARN)
