"""oedilf URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.conf import settings
from django.contrib import admin
from django.urls import path, include

from lim.admin import lim_admin_site

urlpatterns = [
    path("admin/", admin.site.urls),
    path("lim_admin/", lim_admin_site.urls),
    path("tinymce/", include("tinymce.urls")),
    path("", include("lim.urls")),
    path("api-auth/", include("rest_framework.urls")),
    path("accounts/", include("allauth.urls")),
    path("__debug__/", include(debug_toolbar.urls)),
    path("announcements/", include("pinax.announcements.urls", namespace="pinax_announcements")),
    path('tellme/', include("tellme.urls")),  # NOSONAR
    path('impersonate', include('impersonate.urls')),  # NOSONAR
]

handler404 = 'perfect404.views.page_not_found'

if settings.DEBUG:
    urlpatterns = [] + urlpatterns
