[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=therealneilski_oedilf&metric=alert_status)](https://sonarcloud.io/dashboard?id=therealneilski_oedilf)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)


# README #

### What is this repository for? ###

* This is a private repo to hold the code for The OEDILF.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Who do I talk to? ###

* Neil Padgen <neil.padgen@gmail.com>
