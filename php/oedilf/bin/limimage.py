#!/usr/local/bin/python
# -*- coding: utf-8 -*-

#from tweetlim import make_image
from PIL import Image, ImageFont, ImageDraw, ImageColor, ImageChops
import os
import re
import time
import sys
import tempfile
import HTMLParser

def trim(im):
    # Calculate the true bounding box, and trim the image, leaving a bit of a border.
    bg = Image.new(im.mode, im.size, im.getpixel((0, 0)))
    diff = ImageChops.difference(im, bg)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    if bbox:
        return im.crop((0, 0, bbox[2]+10, bbox[3]+10))

def make_image(author, lim_id, defined_words, lim):
    filename = "/usr/local/share/oedilf/cached_images/Lim{0}.png".format(lim_id)
    if os.path.exists(filename):
        return filename

    # Creates an image containing the limerick
    if os.path.exists('/var/www/db'):
        sspr = ImageFont.truetype('/var/www/db/fonts/SourceSansPro-Regular.otf', size=24)
        ssprb = ImageFont.truetype('/var/www/db/fonts/SourceSansPro-Bold.otf', size=24)
        sspri = ImageFont.truetype('/var/www/db/fonts/SourceSansPro-It.otf', size=10)
        oedilfi = Image.open("/var/www/db/oedilfi.jpg")
    else:
        sspr = ImageFont.truetype('/Users/neil/Library/Fonts/SourceSansPro-Regular.otf', size=24)
        ssprb = ImageFont.truetype('/Users/neil/Library/Fonts/SourceSansPro-Bold.otf', size=24)
        sspri = ImageFont.truetype('/Users/neil/Library/Fonts/SourceSansPro-It.otf', size=10)
        oedilfi = Image.open("/Users/neil/src/oedilf/oedilfi.jpg")

    black = ImageColor.getrgb('black')


    image = Image.new("RGB", (2000, 4000), ImageColor.getrgb('white'))

    image.paste(oedilfi, (10, 10))

    draw = ImageDraw.Draw(image)

    draw.text((160, 25), "Limerick #{0} on {1} by {2}".format(lim_id, defined_words, author), font=ssprb, fill=black)
    y = 60
    parser = HTMLParser.HTMLParser()
    # strip out OEDILF links like [[#T12345:this]]
    links_re = re.compile(r'\[\[[#TA0-9]*?:(.*?)\]\]')
    for line in lim.split('\n'):
        line = line.strip()
        line = re.sub(r'<.*?>', '', line)
        line = line.decode("utf-8")
        line = parser.unescape(line)
        line = links_re.sub('\\1', line)
        draw.text((160, y), line, font=sspr, fill=black)
        y += 30

    draw.text((10, y+10), u"© Copyright 2004-{0} OEDILF. The information in this image may not be reproduced in any form without written permission by the OEDILF Editor-in-Chief.".format(time.gmtime().tm_year), font=sspri, fill=black)
    draw.text((10, y+22), u"All limericks remain the property of the authors who contributed them and are reproduced here by permission.", font=sspri, fill=black)
    draw.text((10, y+34), u"If you reproduce an individual limerick under the doctrine of fair use or fair dealing, please credit the limerick's author.", font=sspri, fill=black)
    draw.text((10, y+50), "Generated: {0}".format(time.ctime(time.time())), font=sspri, fill=black)
    
    image = trim(image)
    
    image.save(filename, "PNG")
    
    return filename
    

if __name__ == '__main__':
    author = sys.argv[1]
    lim_id = sys.argv[2]
    verse_id = sys.argv[3]
    defined_words = sys.argv[4]
    lim = sys.argv[5]
    print make_image(
        author,
        lim_id,
        defined_words,
        lim
        )
