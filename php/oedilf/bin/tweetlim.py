#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import tweepy
import sys
import requests
import tempfile
import os
import re
import time
from PIL import Image, ImageFont, ImageDraw, ImageColor, ImageChops
from limimage import make_image
import HTMLParser

def tweet():
    
    author = sys.argv[1]
    lim_id = sys.argv[2]
    verse_id = sys.argv[3]
    defined_words = sys.argv[4]
    lim = sys.argv[5]
    if len(sys.argv) > 6:
        twitter_author = sys.argv[6]
    else:
        twitter_author = author

    
    auth = tweepy.OAuthHandler(
        'o8ntBWyM38OSHKPkcfrCS2iEF',
        'f4Xkfx6x43w5RVMUjPbZf4bbexEHVz9OKfZ68txsS9ZbZct1TR'
    )
    auth.set_access_token(
        '331021600-3m6Be9ga0CYY6FdpJl1AElnCv8yKqBdEougrinPT',
        '90Luxh3nNcj35C0lM3uv9BsAjGmxtaVMT2NosrCSK9EkA'
    )

    api = tweepy.API(auth)

    # strip html formatting
    lim = re.sub(r'<.*?>', '', lim)

    parser = HTMLParser.HTMLParser()
    tweet = u'Limerick #{0} on {1} by {2}:\nhttps://oedilf.com/db/Lim.php?VerseId=A{0}\n{3}'.format(
        lim_id,
        defined_words,
        twitter_author,
        lim[:lim.index('\n')]
    )
    # max tweet length is 292 because Twitter will shorten it to a t.co link
    lim_lines = lim.split('\n')
    index = 1
    while len(tweet) < 293 and index < len(lim_lines):
        if len(tweet + lim_lines[index]) > 293:
            tweet += ' ...'
            break
        else:
            tweet += '\n' + lim_lines[index]
            index += 1

    if os.path.exists('/var/www/db'):
        log = open("/var/www/db/logs/tweetlim.log", "a")
    else:
        log = sys.stdout
    log.write("{0}: Tweeting {1}\n".format(time.ctime(), lim_id))
    log.write(tweet)
    log.write("\n")
    try:
        filename = make_image(author, lim_id, defined_words, lim)
        retval = api.update_with_media(filename, tweet)
        log.write("Success!\n")
    except Exception, e:
        log.write("Failed:\n")
        log.write("{0}\n".format(sys.exc_info()[0]))
        log.write("{0}\n".format(sys.exc_info()[1]))
        log.write("{0}\n".format(sys.exc_info()[2]))
    log.write("\n")
    log.close()
    return retval
    

if __name__ == '__main__':
    print tweet()

