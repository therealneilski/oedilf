<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimFormBuilder
{
	protected $formName;
	protected $hasCancel;
    protected $updateButtonText = "Update";
	protected $fields = array();
	protected $hiddens = array();
  
	public function __construct($formName, $hasCancel) 
	{
		$this->formName = $formName;
		$this->hasCancel = $hasCancel;
	}
	
	public function AddField($field)
	{
		$this->fields[] = $field;
        return $field;
	}
	
	public function AddHidden($name, $value)
	{
		$this->hiddens[$name] = $value;
	}

    public function SetUpdateButtonText($buttonText)
    {
        $this->updateButtonText = $buttonText;
    }

	public function FormatHeader($html, $formParams="")
	{
		if (!$formParams) $formParams = "Update=".$this->formName;
		$html->BeginForm($formParams, $this->formName);
		$html->BeginTable($this->formName."Fields", "widetable");
	}

    public function FormatFullWidthHeading($html, $text)
    {
		$html->BeginTableRow();
        $html->BeginTableHeadingCell(2);
        $html->Text($text);
        $html->EndTableHeadingCell();
		$html->EndTableRow();
    }
	
	public function FormatBody($html, $record)
	{
		foreach ($this->fields as $field)
		{
			$html->BeginTableRow();

			$html->BeginTableCell('', 'darkpanel');
			$html->Text(htmlspecialchars($field->GetScreenLabel(), ENT_QUOTES));
            if ($field->GetHint())
            {
                $html->LineBreak();
                $html->Text($field->GetHint(), '', 'hinttext');
            }
			$html->EndTableCell();
			
			$html->BeginTableCell('', 'lightpanel');
			$html->Text($field->GetFormHtml($record[$field->GetDbFieldName()]));
			$html->EndTableCell();
			
			$html->EndTableRow();
		}	
	}

	public function FormatFooter($html)
	{
		$html->BeginTableRow();
		$html->BeginTableCell('', 'darkpanel', 2);
		foreach ($this->hiddens as $name=>$value)
			$html->Text("<input type='hidden' name='$name' value='$value'>");
		$html->SubmitButton($this->formName."UpdateButton", $this->updateButtonText);
		if ($this->hasCancel)
		{
			$html->Text(' ');
			$html->SubmitButton($this->formName."CancelButton", "Cancel");
		}
		$html->EndTableCell();
		$html->EndTableRow();	
		$html->EndTable();
		$html->EndForm();
	}
	
	public function InvalidEntryMessages($postValues)
	{
		$messages = array();
		foreach ($this->fields as $field) 
		{
			if ($field->HasValue($postValues))
			{
				if ($message = $field->InvalidEntryMessage($postValues))
				{
					$messages[] = $field->GetScreenLabel().": ".$field->InvalidEntryMessage($postValues);
				}
			}
		}
		return $messages;
	}
	
	public function GetUpdateRecord($postValues)
	{
		$record = array();
		foreach ($this->fields as $field) 
		{
			if ($field->HasValue($postValues))
				$record[$field->GetDbFieldName()] = $field->GetSqlUpdateValue($postValues);
		}
		return $record;
	}
}
?>