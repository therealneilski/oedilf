<?php
// interface for delayed actions to be triggered by an email link token.
interface ITokenHandler
{
    public function execute($token);
}

?>