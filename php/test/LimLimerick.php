<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimLimerick
{
	public $OriginalId = 0;
	public $VerseId = 0;
	public $Record = array();
	
	protected $memberId = 0;
	protected $wordList = false;
	protected $topicList = false;
	protected $RFAList = false;
	protected $authorRFAs = array();
	protected $nonAuthorRFAs = array();
	protected $tagList = array();
	protected $oldVersions = false;
	protected $history = array();
	protected $authorRecord = false;
	protected $hasMonitor = '?';
	protected $hasConfirmingMessage = '?';
    protected $lastVisit;
	
	public function __construct($anyVerseId, $memberId) 
	{
		$this->memberId = $memberId;
		if ($anyVerseId<>0)
		{
			$result = DbQuery("SELECT * FROM DILF_Limericks 
				WHERE State<>'obsolete' AND 
				OriginalId=(SELECT OriginalId FROM DILF_Limericks WHERE VerseId='$anyVerseId')
				ORDER BY VerseId DESC LIMIT 1");
			if (DbQueryRows($result)<>1) die(sprintf('Limerick #%d not found<br>', $anyVerseId));
			$this->Record = DbFetchArray($result);
			DbEndQuery($result);
			$this->OriginalId = $this->Record['OriginalId'];
			$this->VerseId = $this->Record['VerseId'];
            $this->lastVisit = new LimLastVisit($this->OriginalId, $this->memberId);
		}
	}
	
	public function IsMine() {
		return (($this->memberId == $this->Record['PrimaryAuthorId']) or 
			($this->memberId == $this->Record['SecondaryAuthorId']));
	}
	
	public function IsPrimarilyMine() {
		return ($this->memberId == $this->Record['PrimaryAuthorId']);
	}
	
	public function HasMyMonitor()
	{
		if ($this->hasMonitor = '?')
		{
			$result = DbQuery("SELECT * FROM DILF_Monitors 
				WHERE AuthorId=$this->memberId AND OriginalId=$this->OriginalId");
			$this->hasMonitor = (DbQueryRows($result) > 0);
			DbEndQuery($result);
		}
		return $this->hasMonitor;
	}
	
	public function GetWordList()
	{
		if ($this->wordList===false)
			$this->wordList = new LimWordList($this->VerseId);
		return $this->wordList;
	}

	protected static $defaultDate = '0000-00-00 00:00:00';
	 
	public function GetAuthorRecord()
	{
		if ($this->authorRecord===false)
		{
			$authorId = $this->Record['PrimaryAuthorId'];
			$result = DbQuery("SELECT * FROM DILF_Authors 
				WHERE AuthorId=$authorId LIMIT 1");
			if ($line = DbFetchArray($result))
			{
				$this->authorRecord = $line;
				if ($line['FirstLimDate']==self::$defaultDate)
					$this->authorRecord['FirstLimDate'] = $this->FixFirstLimDate($authorId);
			}
			DbEndQuery($result);
		}
		return $this->authorRecord;
	}
	
	protected function FixFirstLimDate($authorId)
	{
		$result = DbQuery(sprintf("SELECT MIN(DateTime) FirstLim
			FROM DILF_Limericks WHERE PrimaryAuthorId=%d OR SecondaryAuthorId=%d", $authorId, $authorId));
		if ($line = DbFetchArray($result)) 
		{
			$date = $line['FirstLim'];
			DbQuery("UPDATE DILF_Authors SET FirstLimDate='$date' WHERE AuthorId=$authorId LIMIT 1"); 
		}
		else $date = self::$defaultDate; 
		DbEndQuery($result);	
		return $date;
	}
	
	public function GetTopicList() 
	{
		if ($this->topicList===false)
		{
			$this->topicList = array();
			$result = DbQuery("SELECT T.TopicId, T.Name FROM DILF_TopicLims L, DILF_Topics T 
				WHERE L.TopicId=T.TopicId AND L.OriginalId=$this->OriginalId ORDER BY T.Name");
			while ($line=DbFetchArray($result))
				$this->topicList[$line['TopicId']] = $line['Name'];
			DbEndQuery($result);
		}
		return $this->topicList;
	}
	
	protected function loadRFAList()
	{
		if ($this->RFAList===false)
		{
			$this->authorRFAs = array();
			$this->nonAuthorRFAs = array();
			$this->RFAList = array();
			$result = DbQuery("SELECT A.Name, R.AuthorId, R.New FROM DILF_RFAs R, DILF_Authors A 
				WHERE R.OriginalId=$this->OriginalId AND R.AuthorId=A.AuthorId ORDER BY A.Name");
			while ($line=DbFetchArray($result))
			{
				$this->RFAList[$line['AuthorId']] = $line['Name'];
				if (($line['AuthorId']==$this->Record['PrimaryAuthorId']) or
					($line['AuthorId']==$this->Record['SecondaryAuthorId']))
					$this->authorRFAs[$line['AuthorId']] = $line['Name'];
				else
					$this->nonAuthorRFAs[$line['AuthorId']] = $line['Name'];
			}
			DbEndQuery($result);
		}
	}
	
	public function ReloadRFAInfo()
	{
		$this->RFAList = false;
	}
	
	public function GetRFAList() 
	{
		$this->loadRFAList();
		return $this->RFAList;
	}
	
	public function GetAuthorRFAList() 
	{
		$this->loadRFAList();
		return $this->authorRFAs;
	}
	
	public function GetNonAuthorRFAList() 
	{
		$this->loadRFAList();
		return $this->nonAuthorRFAs;
	}
	
	public function RFACount()
	{
		$this->loadRFAList();
		return count($this->RFAList);
	}
	
	public function NonAuthorRFACount()
	{
		$this->loadRFAList();
		return count($this->nonAuthorRFAs);
	}
	
	public function HasMyRFA()
	{
		$this->loadRFAList();
		return isset($this->RFAList[$this->memberId]);
	}
	
	public function HasPrimaryRFA()
	{
		$this->loadRFAList();
		return isset($this->authorRFAs[$this->Record['PrimaryAuthorId']]);
	}
	
	public function HasSecondaryRFA()
	{
		$this->loadRFAList();
		return ($this->Record['SecondaryAuthorId']==0) or
			isset($this->authorRFAs[$this->Record['SecondaryAuthorId']]);
	}
	
	public function GetTagList($authorId)
	{
		if (!isset($this->tagList[$authorId]))
		{
			$this->tagList[$authorId] = array();
			$result = DbQuery("SELECT TagText FROM DILF_Tags T 
				WHERE AuthorId=1 AND OriginalId=$this->OriginalId GROUP BY TagText ORDER BY TagText");
			while ($line=DbFetchArray($result))
				$this->tagList[$authorId][] = $line['TagText'];
			DbEndQuery($result);
		}
		return $this->tagList[$authorId];
	}
	
	public function GetObsoleteRevisions()
	{
		if ($this->oldVersions===false)
		{
			$this->oldVersions = array();
			$result = DbQuery("SELECT * FROM DILF_Limericks 
				WHERE OriginalId=$this->OriginalId AND State='obsolete' ORDER BY Version DESC");
			while ($line=DbFetchArray($result))
				$this->oldVersions[$line['Version']] = $line;
			DbEndQuery($result);
		}
		return $this->oldVersions;
	}
	
	public function GetComments($VerseId)
	{
		if (!isset($this->history[$VerseId]))
		{
			$this->history[$VerseId] = array();
			$result = DbQuery("SELECT * FROM DILF_Workshop 
				WHERE VerseId=$VerseId ORDER BY DateTime DESC");
			while ($line=DbFetchArray($result))
            {
				$this->history[$VerseId][] = $line;
                if ($line['AuthorId']==$this->memberId)
                    $this->lastVisit->SetMinCommentIndex($line['WorkshopId']);
            }
			DbEndQuery($result);
            if ($this->VerseId==$VerseId)
                $this->lastVisit->SaveLastViewed($this->history[$VerseId][0]['WorkshopId']);
		}
		return $this->history[$VerseId];
	}

    public function GetLastOldCommentId()
    {
        return $this->lastVisit->GetLastVisitCommentId();
    }

	public function HasConfirmingMessage()
	{
		if ($this->hasConfirmingMessage=='?')
		{
			$result = DbQuery(sprintf("SELECT VerseId FROM DILF_Workshop 
				WHERE AuthorId=0 AND VerseId=%d AND Message LIKE '%%(author%d) said Yes.%%'",
				$this->VerseId, $this->memberId));
			$this->hasConfirmingMessage = (DbQueryRows($result) > 0);
			DbEndQuery($result);
		}	
		return $this->hasConfirmingMessage;
	}
	
	public function IsApproved() { return $this->Record['State'] == 'approved'; }
	public function IsConfirming() { return $this->Record['State'] == 'confirming'; }
	public function IsHeld() { return $this->Record['State'] == 'held'; }
	public function IsNew() { return $this->Record['State'] == 'new'; }
	public function IsTentative() { return $this->Record['State'] == 'tentative'; }
	public function IsRevised() { return $this->Record['State'] == 'revised'; }
	public function IsStored() { return $this->Record['State'] == 'stored'; }
	public function IsExcluded() { return $this->Record['State'] == 'excluded'; }
}
?>