<?php
class LimCellFormatterDelTag extends LimCellFormatterText
{
	public function FormatCell($cellValue)
	{
		return RedXLink("DelTagAll=".rawurlencode($cellValue), '',
			"Remove this tag from all limericks");
	}
}
?>