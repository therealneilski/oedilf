<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimField
{
	protected $dbFieldName;
	protected $screenLabel;
    protected $hint;
  
	public function __construct($fieldName, $screenLabel) 
	{
		$this->dbFieldName = $fieldName;
		$this->screenLabel = $screenLabel;
	}
	
	public function GetDbFieldName()
	{
		return $this->dbFieldName;
	}
	
	public function GetScreenLabel()
	{
		return $this->screenLabel;
	}
	
	public function GetFormHtml($defaultValue)
	{
		return "<p>Uninitialized Edit Field</p>";
	} 
	
	public function InvalidEntryMessage($postValues)
	{
		return "";
	}
	
	public function HasValue($postValues)
	{
		return isset($postValues[$this->dbFieldName]);
	}
	
	public function GetSqlUpdateValue($postValues)
	{
		if (isset($postValues[$this->dbFieldName]))
			return $postValues[$this->dbFieldName];
		else return "";
	}

    public function SetHint($text)
    {
        $this->hint = $text;
        return $this;
    }

    public function GetHint()
    {
        return $this->hint;
    }
}
?>