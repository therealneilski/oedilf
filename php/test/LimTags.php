<?php
// Adding personal tags to limericks.

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function FormatAddRemoveTag($OriginalId) {
  global $member;
  $memberid = $member->GetMemberId();
  $WordsDisplay = '';
  
  if ($member->CanWorkshop()) {
    $WordsDisplay .= '<table class="menutable">';
    $WordsDisplay .= '<tr><td class="menus">';
    $CurrentTags = array();
    $result = DbQuery("SELECT TagText FROM DILF_Tags T WHERE AuthorId=$memberid AND OriginalId=$OriginalId GROUP BY TagText ORDER BY TagText");
    if (DbQueryRows($result)==0)
      $WordsDisplay .= "You have no personal tags on this limerick. You can define tags or keywords to categorize limericks.";
    else {
        $WordsDisplay .= "Your personal tags on this limerick are: ";
        while ($line=DbFetchArray($result)) {
          $WordsDisplay .= " ".$line['TagText'].RedXLink(array("TagLimerick=$OriginalId", "DelTag=".rawurlencode($line['TagText'])), "", "Remove ".$line['TagText']." tag from this limerick")."; ";
          $CurrentTags[$line['TagText']] = 1;
        }
        if (DbQueryRows($result)>1)
          $WordsDisplay .= DarkButton(array("TagLimerick=$OriginalId", "DelAllTags=1"), "Remove All", "Remove all tags from this limerick");
    }
    DbEndQuery($result);
    $WordsDisplay .= '<br >';

    $WordsDisplay .= sprintf('<form action="%s" method="get" name="TagAddForm" style="display:inline;">', PHPSELF);
    $WordsDisplay .= sprintf('<input type=hidden name="TagLimerick" value="%s">', $OriginalId);
    $WordsDisplay .= '<input name="NewTags" value="" type="text"> ';
    $WordsDisplay .= SubmitButton("LimerickTagButton", "Add Tag");
    $WordsDisplay .= ' <span class="hinttext">Separate multiple tags using semicolons.';

    // look for other tags
    $result = DbQuery("SELECT TagText FROM DILF_Tags T WHERE AuthorId=$memberid AND OriginalId<>$OriginalId GROUP BY TagText ORDER BY TagText");
    if (DbQueryRows($result)!=0) {
      $WordsDisplay .= " Other tags you've defined: ";
      while ($line=DbFetchArray($result)) {
        if (!$CurrentTags[$line['TagText']])
          $WordsDisplay .= $line['TagText']."; ";
      }
    }
    $WordsDisplay .= '</span>';
    DbEndQuery($result);
    $WordsDisplay .= '</form>';

    $WordsDisplay .= '</td></tr>';
    $WordsDisplay .= '</table>';
  }

  return $WordsDisplay;
}

function FormatTagsPage() {
	global $member;
	$html = new LimHtml();
	if ($member->CanWorkshop())
	{
		$start = $_GET['Start']+0;
		$sqlTable = new LimSqlTableFormatter(
			"SELECT TagText 'Tag Name', COUNT(*) 'Count', TagText 'Delete Tag' FROM DILF_Tags T 
			WHERE AuthorId=".$member->GetMemberId()." GROUP BY TagText ORDER BY TagText",
			array('Tag Name'=>new LimCellFormatterTagLink(),
				'Count'=>new LimCellFormatterText(),
				'Delete Tag'=>new LimCellFormatterDelTag()),
			$start, 100, 'Action=ShowTags');
		$sqlTable->HeadingText = 'My Tags';
		$sqlTable->FormatTable($html);
		if ($sqlTable->NumMatches==0)
			$html->Paragraph("You can add tags to limericks in the limerick workshops.
				Think of them as keywords. You can add as many different tags as you want to as many
				limericks as you want. Once you've added a tag or two, come back here to see a summary
				and to search for the limericks you've tagged.");
	}
	return $html->FormattedHtml();
}

function FormatTagsSearchPage($Tag) {
  global $member;
  $WordsDisplay = '';

  if ($member->CanWorkshop()) {
    $result = DbQuery("SELECT L.OriginalId FROM DILF_Limericks L, DILF_Tags T WHERE L.OriginalId=T.OriginalId AND L.State<>'obsolete' AND TagText LIKE '$Tag' AND T.AuthorId=".$member->GetMemberId());
    $SearchMatches = array();
    while ($line = DbFetchArray($result)) {
      $SearchMatches[] = $line['OriginalId'] ;
    }
    DbEndQuery($result);

    $_SESSION['SearchList'] = implode(",", $SearchMatches);
    $_SESSION['SearchTitle'] = "Limericks tagged with ".stripslashes($Tag);
    $WordsDisplay .= FormatSearchResults();
  }
  return $WordsDisplay;
}

function ProcessAddTags($OriginalId, $Tags) {
  global $member;
  $memberid = $member->GetMemberId();
  $WordsDisplay = '';

  $NewTags = explode(";", $Tags);
  foreach ($NewTags as $Tag) {
    if (trim($Tag)<>'') {
      $CleanTag = DeTagWord(trim($Tag));
      $CleanTag = str_replace('"', '', $CleanTag); //double quotes cause problems
      
      $result = DbQuery("DELETE FROM DILF_Tags WHERE AuthorId=$memberid AND OriginalId=$OriginalId AND TagText LIKE '$CleanTag'");
      DbInsert("DILF_Tags", array("AuthorId"=>$memberid, "OriginalId"=>$OriginalId, "TagText"=>$CleanTag));
    }
  }
  return $WordsDisplay;
}

function ProcessRemoveTag($OriginalId, $OldTag) {
  global $member;
  $memberid = $member->GetMemberId();
  $WordsDisplay = '';
  
  $result = DbQuery("DELETE FROM DILF_Tags WHERE AuthorId=$memberid AND OriginalId=$OriginalId AND TagText LIKE '$OldTag'");
  return $WordsDisplay;
}

function ProcessRemoveAllTags($OriginalId) {
  global $member;
  $memberid = $member->GetMemberId();
  $WordsDisplay = '';

  $result = DbQuery("DELETE FROM DILF_Tags WHERE AuthorId=$memberid AND OriginalId=$OriginalId");
  return $WordsDisplay;
}

function ProcessRemoveTagFromAll($OldTag) {
  global $member;
  $memberid = $member->GetMemberId();
  $WordsDisplay = '';

  $result = DbQuery("DELETE FROM DILF_Tags WHERE AuthorId=$memberid AND TagText LIKE '$OldTag'");
  return $WordsDisplay;
}

function FormatAddActivityTag() {
  global $member;
  $memberid = $member->GetMemberId();
  $WordsDisplay = '';
  $SelectList = '';
  if ($member->CanWorkshop()) {
    $result = DbQuery("SELECT TagText FROM DILF_Tags T WHERE AuthorId=$memberid GROUP BY TagText ORDER BY TagText");
    if (DbQueryRows($result)!=0) {
      while ($line=DbFetchArray($result)) {
          $SelectList .= '<option value="'.$line['TagText'].'">'.$line['TagText'].'</option>';
      }
    }
    
    if ($SelectList) {
      $WordsDisplay .= '<br ><br ><select name="ActivitiesTag"> <option value="" selected="selected">(select a tag)</option>'.$SelectList.'</select>';
      $WordsDisplay .= ' '.SubmitButton("ActivityTagButton", "Add tag to selected limericks");
    }
    DbEndQuery($result);
  }

  return $WordsDisplay;
}

function ProcessAddActivityTag() {
  global $member;
  $WordsDisplay = '';

  $MsgIds = array();
  if ($member->CanContribute()) {
    if ($_POST['ActivitiesTag']) {

      foreach ($_POST as $cbox => $value) {
        if ((substr($cbox, 0,6)=='DelMsg') and ($value=='on')) {
          $MsgNum = substr($cbox, 6);
          $MsgIds[] = $MsgNum;
        }
      }
      $TagCount = 0;
      if (count($MsgIds)) {
        $MsgList = implode(",", $MsgIds);
        $result = DbQuery("SELECT L.OriginalId FROM DILF_Messages M, DILF_Limericks L WHERE MsgId IN ($MsgList) AND M.VerseId=L.OriginalId AND L.State<>'obsolete' GROUP BY L.OriginalId");
        while ($line = DbFetchArray($result)) {
          //$WordsDisplay .= $line['OriginalId']." " ;
          if ($line['OriginalId']) {
            ProcessAddTags($line['OriginalId'], $_POST['ActivitiesTag']);
            $TagCount++;
          }
        }
        DbEndQuery($result);

        //$result = DbQuery(sprintf("DELETE FROM DILF_Messages WHERE MsgId=%d", $MsgNum));
      }
      $WordsDisplay .= sprintf('<p>%d limericks tagged.</p>', $TagCount);
      SetReload('Action=Activity');
    } else {
      $WordsDisplay .= "<p>No tag selected.</p>";
    }
  }

  $WordsDisplay .= '<p>'.LinkSelf("Action=Activity", "Activity").'</p>';

  return $WordsDisplay;
}

?>
