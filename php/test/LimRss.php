<?php
//
// Routines for notifications and messages
//
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }
define('RSS_FEED_PATH', 'feeds/');

function RSSChannelNewLims($Author='') {
  global $Configuration;

  $Feed = sprintf('<?xml version="1.0" encoding="iso-8859-1" ?>
<rss version="0.92">
<channel>
 <title>OEDILF: New Limericks%s</title>
 <link>http://www.oedilf.com/</link>
 <description>New limericks from the Omnificent English Dictionary In Limerick Form</description>
 <lastBuildDate>%s</lastBuildDate>
 <docs>http://backend.userland.com/rss092</docs>
 <managingEditor>%s</managingEditor>
 <webMaster>%s</webMaster>',
    ($Author=='') ? '' : ' by '.$Author,
    gmdate("r"),
    $Configuration['EiCEmail'],
    $Configuration['AdminEmail']);

  return $Feed;
}

function RSSChannelWorkshop($OriginalId) {
  global $Configuration;

  $Feed = sprintf('<?xml version="1.0" encoding="iso-8859-1" ?>
<rss version="0.92">
<channel>
 <title>OEDILF: Limerick #T%s</title>
 <link>http://www.oedilf.com/</link>
 <description>Workshop activity summary</description>
 <lastBuildDate>%s</lastBuildDate>
 <docs>http://backend.userland.com/rss092</docs>
 <managingEditor>%s</managingEditor>
 <webMaster>%s</webMaster>',
    $OriginalId,
    gmdate("r"),
    $Configuration['EiCEmail'],
    $Configuration['AdminEmail']);

  return $Feed;
}

function RSSItemNewLim($line) {
  $words = QueryWordList($line['VerseId']);
  $wl = $words[0];
  if (count($words)>1) $wl .= ', '.$words[1];
  if (count($words)>2) $wl .= '...';
  $AN = SplitAN($line['AuthorNotes']);

  $Feed = sprintf(' <item>
  <title>Limerick #%s on %s by %s</title>
  <link>%s?VerseId=%d</link>
  <description>%s</description>
 </item>',
    ($line['State']=='approved') ? $line['LimerickNumber'] : 'T'.$line['OriginalId'],
    $wl,
    GetAuthorHtml($line['PrimaryAuthorId']).
      (($line['SecondaryAuthorId']<>0) ? sprintf(' and %s', GetAuthorHtml($line['SecondaryAuthorId'])) : ''),
    'http://'.THIS_APP,
    $line['OriginalId'],
    htmlspecialchars(
      (($AN[0]<>'') ? AddBreaks(FormatQuickDocLinks($AN[0])).'<br ><br >' : '').
      AddBreaks(FormatQuickDocLinks($line['Verse'])).
      (($AN[1]<>'') ? '<br ><br >'.AddBreaks(FormatQuickDocLinks($AN[1])) : '')
    ));

  return $Feed;
}

function RSSItemWorkshopComment($comment) {
  $Feed = sprintf(' <item>
  <title>Comment on #T%s by %s</title>
  <link>%s?Workshop=%d</link>
  <description>Comment added %s GMT %s</description>
 </item>',
    $comment['OriginalId'], GetAuthorHtml($comment['AuthorId']),
    'http://'.THIS_APP, $comment['OriginalId'],
    date("d M Y H:i:s ", DecodeTime($comment['DateTime'])),
    $comment['EditDateTime'] ?
      " and edited ".date("d M Y H:i:s", DecodeTime($comment['EditDateTime'])) :
      ""
  );

  return $Feed;
}

function RSSItemWorkshopRevision($line) {
  $Feed = sprintf(' <item>
  <title>Revision of #T%s</title>
  <link>%s?Workshop=%d</link>
  <description>Limerick revised %s GMT</description>
 </item>',
    $line['OriginalId'],
    'http://'.THIS_APP, $line['OriginalId'],
    date("d M Y H:i:s ", DecodeTime($line['DateTime']))
  );

  return $Feed;
}

function RSSEndChannel() {
  $Feed = sprintf('</channel>
</rss>');

  return $Feed;
}

function RSSFeed() {
  RSSFeedFile('rssfiltered.xml', 'AND Category="normal"'); // filtered
  RSSFeedFile('rss.xml', ''); // includes the curtained room
}

function RSSFeedFile($filename, $filter) {
  $Feed = RSSChannelNewLims();

  $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE State='approved' AND UpdateDateTime>'%s' $filter ORDER BY DateTime DESC",
    LimTimeConverter::FormatGMDateFromNow(-24*60*60))); // anything in the last day
  while ($line = DbFetchArray($result)) {
    $Feed .= RSSItemNewLim($line);
  }
  DbEndQuery($result);

  $Feed .= RSSEndChannel();

  if (!$fh = fopen( RSS_FEED_PATH.$filename, 'w' )) die("Can't open rss file: $filename.<br >\n");
  if (!fwrite($fh, $Feed)) die("Can't write to rss file: $filename.<br >\n");
  fclose( $fh );

}

function RSSAuthorFeed($AuthorId, $Days=7) {
  $Feed = RSSChannelNewLims(GetAuthorHtml($AuthorId));
  $minItems = 25;
  $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE State='approved' AND UpdateDateTime>'%s' AND (PrimaryAuthorId=%d OR SecondaryAuthorId=%d)ORDER BY DateTime DESC",
    LimTimeConverter::FormatGMDateFromNow(-$Days*24*60*60), $AuthorId, $AuthorId)); // anything in the last n days
  if (DbQueryRows($result)<$minItems) {
    DbEndQuery($result);
    $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks 
		WHERE State='approved' AND (PrimaryAuthorId=%d OR SecondaryAuthorId=%d) 
		ORDER BY DateTime DESC LIMIT %d",
		$AuthorId, $AuthorId, $minItems));
  }  
  while ($line = DbFetchArray($result)) {
    $Feed .= RSSItemNewLim($line);
  }
  DbEndQuery($result);

  $Feed .= RSSEndChannel();

  $filename = 'Author'.$AuthorId.'rss.xml';
  if (!$fh = fopen( RSS_FEED_PATH.$filename, 'w' )) die("Can't open $filename.<br >\n");
  if (!fwrite($fh, $Feed)) die("Can't write to $filename.<br >\n");
  fclose( $fh );

}

function RSSInitializeWorkshopFeedLink($OriginalId) {
  global $WorkshopFeed;
  $filename = RSS_FEED_PATH.'Workshop'.$OriginalId.'rss.xml';
  if (!file_exists($filename)) {
    RSSWorkshopFeed($OriginalId);
  }

  $WorkshopFeed = '<link rel="alternate" type="application/rss+xml" title="Workshop RSS" href="'.$filename.'" >';
}

function RSSAuthorFeedFile($AuthorId) {
	return RSS_FEED_PATH.'Author'.$AuthorId.'rss.xml';
}

function RSSInitializeAuthorFeedLink($AuthorId) {
  global $AuthorFeed;
  $filename = RSSAuthorFeedFile($AuthorId);
  if (!file_exists($filename)) {
    RSSAuthorFeed($AuthorId);
  }
  $authorName = GetAuthorHtml($AuthorId);
  $AuthorFeed = '<link rel="alternate" type="application/rss+xml" title="Limerick RSS for '.$authorName.'" href="'.$filename.'" >';
}

function RSSLink($filename) {
	return "<a href='$filename' rel='alternate' type='application/rss+xml' title='RSS feed'><img src='feed-icon-12x12.png' ></a>";	
}

function RSSWorkshopFeed($OriginalId) {
  $Feed = RSSChannelWorkshop($OriginalId);

  $OrderedFeed = array();
  $TimeLimit = LimTimeConverter::FormatGMDateFromNow(-24*60*60); // anything in the last day

  $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE OriginalId=%d AND DateTime>'%s' ORDER BY DateTime DESC",
    $OriginalId, $TimeLimit));
  while ($line = DbFetchArray($result)) {
    $OrderedFeed[] = $line['DateTime'].RSSItemWorkshopRevision($line);
  }
  DbEndQuery($result);

  $result = DbQuery(sprintf("SELECT * FROM DILF_Workshop WHERE OriginalId=%d AND (DateTime>'%s' OR EditDateTime>'%s') ORDER BY DateTime DESC",
    $OriginalId, $TimeLimit, $TimeLimit));
  while ($comment = DbFetchArray($result)) {
    if ($comment['EditDateTime'])
      $OrderedFeed[] = $comment['EditDateTime'].RSSItemWorkshopComment($comment);
    else
      $OrderedFeed[] = $comment['DateTime'].RSSItemWorkshopComment($comment);
  }
  DbEndQuery($result);

  rsort($OrderedFeed);
  foreach($OrderedFeed as $item) {
    $Feed .= substr($item, 19);
  }
  $Feed .= RSSEndChannel();

  $filename = 'Workshop'.$OriginalId.'rss.xml';
  if (!$fh = fopen( RSS_FEED_PATH.$filename, 'w' )) die("Can't open $filename.<br >\n");
  if (!fwrite($fh, $Feed)) die("Can't write to $filename.<br >\n");
  fclose( $fh );
}

function XMLDictionary($limit) {
  $WordsDisplay = '';

  $filename = 'OEDILF.xml';
  if (!$fh = fopen( RSS_FEED_PATH.$filename, 'w' )) die("Can't open $filename.<br >\n");
  
  fwrite($fh, '<?xml version="1.0" encoding="iso-8859-1" ?>' . "\n<OEDILF>\n"); // root node
  $entries = 0;
  $result = DbQuery("SELECT * FROM DILF_Limericks WHERE State='approved' LIMIT $limit");
  while ($line = DbFetchArray($result)) {
    $AN = SplitAN($line['AuthorNotes']);
    $content = htmlspecialchars(
      (($AN[0]<>'') ? AddBreaks(FormatQuickDocLinks($AN[0])).'<br ><br >' : '').
      AddBreaks(FormatQuickDocLinks($line['Verse'])).
      (($AN[1]<>'') ? '<br ><br >'.AddBreaks(FormatQuickDocLinks($AN[1])) : '')
    );
    $author = GetAuthor($line['PrimaryAuthorId']);
    if ($line['SecondaryAuthorId'])
      $author .= " and ".GetAuthor($line['SecondaryAuthorId']);
    $author = htmlspecialchars($author);
    
    $filter = (($line['Category'] != "normal") ? 1 : 0);
    
    $words = QueryWordList($line['VerseId']);
    foreach ($words as $word) {
      $cleanword = htmlspecialchars($word);
      $node = "<Definition>\n".
        "<Word>$cleanword</Word>\n".
        "<Limerick>$content</Limerick>\n".
        "<Author>$author</Author>\n".
        "<Filter>$filter</Filter>\n".
        "</Definition>\n";
      if (!fwrite($fh, $node)) die("Can't write to $filename.<br >\n");
      $entries++;
    }
  }
  DbEndQuery($result);
  $WordsDisplay .= "<p>Exported $entries entries to $filename.</p>";

  fwrite($fh, "</OEDILF>\n");
  fclose( $fh );
  return $WordsDisplay;
}


?>
