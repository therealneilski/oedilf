<?php
// pagination of long lists

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimPaginator {
    protected $Link, $TotalCount, $ItemsPerPage, $CurrentItem;
	protected $CurrRange;
	protected $PrevParam, $PrevRange;
	protected $NextParam, $NextRange;
 
	public function LimPaginator($linkParams, $totalcount, $itemsperpage, $current) {
	  $this->Link = PHPSELF."?".DelimitParamList($linkParams)."&amp;";
	  $this->TotalCount = $totalcount;
	  $this->ItemsPerPage = $itemsperpage;
	  $this->CurrentItem = $current;
	  
	  $this->FormatPaginationLinks();
	} 
 
	protected function FormatMultiplePageLinks() {
	  $WordsDisplay = '';
	
	  $start=0;
	  $sep='';
	  while ($start < $this->TotalCount) {
	    $end = $start+$this->ItemsPerPage;
	    if ($end>$this->TotalCount) $end = $this->TotalCount;
	    $WordsDisplay .= $sep.sprintf('<a href="%sStart=%s">%d-%d</a>', 
		  $this->Link, $start, $start+1, $end);
	    $sep=' ';
	    $start += $this->ItemsPerPage;
	  }
	  return $WordsDisplay;
	}
	
	public function FormatNeatMultiplePageLinks() {
	  if ($this->TotalCount <= $this->ItemsPerPage)
	    return "";
	 
	  if ($this->TotalCount <= $this->ItemsPerPage*10)
	    return $this->FormatMultiplePageLinks();
	  
	  $WordsDisplay = '';
	  
	  $start=0;
	  $sep='';
	  $surroundpages = 3;
	  $surroundstart = $this->CurrentItem - $surroundpages*$this->ItemsPerPage;
	  $surroundend = $this->CurrentItem+$surroundpages*$this->ItemsPerPage;
	  
	  while ($start < $surroundstart) {
		$WordsDisplay .= $sep.sprintf('<a href="%sStart=%s">%d</a> ...', $this->Link, $start, $start+1);
	    $sep=' ';
	    $start += $this->ItemsPerPage*10;
	  } 
	
	  if ($start > $surroundstart) $start = $surroundstart;
	  if ($start < 0) $start = 0;
	  
	  while (($start <= $surroundend) and ($start<$this->TotalCount)) {
	    $end = $start+$this->ItemsPerPage;
	    if ($end>$this->TotalCount) $end = $this->TotalCount;
		$WordsDisplay .= $sep.sprintf('<a href="%sStart=%s">%d-%d</a>', $this->Link, $start, $start+1, $end);
	    $sep=' ';
	    $start += $this->ItemsPerPage;
	  }
	
	  $lastpagestart = $this->TotalCount - (($this->TotalCount-1) % $this->ItemsPerPage) - 1;
	  if ($lastpagestart>$start) {
	    $bump = ($lastpagestart - $start) % ($this->ItemsPerPage*10);
	    $start += $bump;
	  }
	  while ($start<$this->TotalCount) {
		$WordsDisplay .= $sep.sprintf('... <a href="%sStart=%s">%d</a>', $this->Link, $start, $start+1);
	    $sep=' ';
	    $start += $this->ItemsPerPage*10;
	  }
	  
	  return $WordsDisplay;
	}

    public function CurrRange() { return $this->CurrRange; }
    public function PrevParam() { return $this->PrevParam; }
    public function PrevRange() { return $this->PrevRange; }
    public function NextParam() { return $this->NextParam; }
    public function NextRange() { return $this->NextRange; }
	
	public function FormatPaginationMenu() {
	  $WordsDisplay = '';
	
	  if (($this->PrevRange<>'') or ($this->NextRange<>'')) {
	    $WordsDisplay .= '<table class="menutable"><tr>';
	    $WordsDisplay .= sprintf('<td class="menus">');
	    if ($this->PrevRange<>'') 
			$WordsDisplay .= sprintf('<a class="nav" href="%s%s">Previous Page %s</a>', 
				$this->Link, $this->PrevParam, $this->PrevRange);
	    $WordsDisplay .= '&nbsp;</td>';
	    $WordsDisplay .= sprintf('<td class="menusright">&nbsp;');
	    if ($this->NextRange<>'') 
			$WordsDisplay .= sprintf('<a class="nav" href="%s%s">Next Page %s</a>', 
				$this->Link, $this->NextParam, $this->NextRange);
	    $WordsDisplay .= '</td>';
	    $WordsDisplay .= '</tr></table>';
	  }
	  return $WordsDisplay;
	}
	
	protected function FormatPaginationLinks() {
	  $this->PrevParam = '';
	  $this->PrevRange = '';
	  $this->NextParam = '';
	  $this->NextRange = '';
	  $this->CurrRange = '';
	
	  if ($this->CurrentItem<0) $this->CurrentItem = 0;
	
	  if ($this->CurrentItem>0) {
	    $prev = $this->CurrentItem - $this->ItemsPerPage;
	    if ($prev<0) $prev=0;
	    $this->PrevParam = 'Start='.$prev;
	
	    $prevstop = $prev+$this->ItemsPerPage;
	    if ($prevstop>$this->TotalCount) $prevstop = $this->TotalCount;
	    $this->PrevRange = sprintf('(%d - %d)', $prev+1, $prevstop);
	  }
	
	  $next = $this->CurrentItem + $this->ItemsPerPage;
	  if ($this->TotalCount>$next) {
	    $this->NextParam = 'Start='.$next;
	
	    $nextstop = $next+$this->ItemsPerPage;
	    if ($nextstop>$this->TotalCount) $nextstop=$this->TotalCount;
	    $this->NextRange = sprintf('(%d - %d)', $next+1, $nextstop);
	  }
	
	  if (($this->PrevRange<>'') or ($this->NextRange<>'')) {
	    $stop = $this->CurrentItem + $this->ItemsPerPage;
	    if ($stop>$this->TotalCount) $stop = $this->TotalCount;
	    $this->CurrRange = sprintf('(%d - %d of %d)', $this->CurrentItem+1, $stop, $this->TotalCount);
	  }
	}
}


?>
