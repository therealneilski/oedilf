<?php
// HTML constructor

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimHtml {
    protected $htmlString = ''; 
	
	protected function DelimitParamList($params) {
		if (is_array($params))
			return implode("&amp;", $params);
		else 
			return $params;
	}
	
	public function FormattedHtml() {
		return $this->htmlString;
	}  
	
	public function Text($textString,  $style='', $class='') {
		$add = '';
        if ($style) $add .= " style='$style'";
		if ($class) $add .= " class='$class'";
        if ($add)
            $this->htmlString .= "<span$add>$textString</span>";
        else
            $this->htmlString .= $textString;
	}
	
	public function TextLine($textString) {
		$this->htmlString .= $textString;
		$this->LineBreak();
	}

    public function Anchor($id, $text="") {
        $this->htmlString .= "<a id=\"$id\">$text</a>";
    }

	public function Link($href, $text, $title='', $newBrowser=false, $class='') {
		$add = '';
		if ($class) $add .= ' class="'.$class.'"'; 
		if ($newBrowser) $add .= " target='_blank' rel='nofollow'";
		$this->htmlString .= sprintf('<a title="%s" href="%s"%s>%s</a>', 
			$title, $href, $add, str_replace(" ","&nbsp;",$text));
	}
	
	public function LinkSelf($params, $text, $title='', $class='') {
		$this->Link(sprintf('%s?%s', PHPSELF, DelimitParamList($params)),
			$text, $title, false, $class);
	}
	
	public function NavLinkSelf($params, $text, $title='') {
		$this->LinkSelf($params, $text, $title, "nav");
	}
	
	public function LightLinkSelf($params, $text, $title='') {
		$this->LinkSelf($params, $text, $title, "macro");
	}
		
	public function Heading($text, $level, $class='') {
    	$attribs = '';
		if ($class) $attribs .= " class='$class'";		
		$this->htmlString .= "<h$level$attribs>$text</h$level>";
	}

	public function Paragraph($text) {
		$this->BeginParagraph();
		$this->Text($text);
		$this->EndParagraph();
	}
	
	public function BeginParagraph() {
		$this->htmlString .= "<p>";
	}
	
	public function EndParagraph() {
		$this->htmlString .= "</p>";
	}

	public function LineBreak() {
		$this->htmlString .= "<br >";
	}

	public function WordBreak() {
		$this->htmlString .= "<wbr >";
	}
	
	public function BeginDiv($class='', $style='', $script='') {
		$attribs="";
		if ($class) $attribs .= " class='$class'";
		if ($style) $attribs .= " style='$style'";
		if ($script) $attribs .= " $script";
		$this->htmlString .= "<div$attribs>";
	}
	
	public function EndDiv() {
		$this->htmlString .= "</div>";
	}

	public function BeginForm($params, $name){
		$this->htmlString .= sprintf('<form action="%s?%s" method="post" name="%s" id="%s">',
			PHPSELF, DelimitParamList($params), $name, $name);
	}

	public function EndForm(){
	 	$this->htmlString .= "</form>";
	}
	
	public function Checkbox($name, $checked=false, $id=0, $label="", $title="") {
		$attribs="";
		if ($checked) $attribs .= " checked";
		if ($id) $attribs .= " id='$id'";
		if ($title) $attribs .= " title='$title'";
		$this->htmlString .= "<input type=checkbox name='$name'$attribs>";
		if ($label)
			$this->htmlString .= "<label for='$id'>$label</label>";
	}
	
	public function EntryField($name, $value="", $id=0, $size=0) {
		$attribs="";
		if ($id) $attribs .= " id='$id'";
		if ($size) $attribs .= " size='$size'";
		$this->htmlString .= "<input type='text' name='$name' value='$value'$attribs>";
	}
	
	public function SubmitButton($name, $text, $title="") {
		$this->htmlString .= SubmitButton($name, $text, $title);
	}

    public function Selection($name, $list) {
		$this->htmlString .= "<select name='$name'>$list</select>";
	}

    public function BeginSelection($name) {
		$this->htmlString .= "<select name='$name'>";
	}

    public function SelectionOption($value, $text, $isSelected=false) {
		if ($isSelected) $selected = " selected";
		else $selected = ""; 
		$this->htmlString .= '<option value="'.$value.'"'.$selected.'>'.$text.'</option>';
	}
	
    public function EndSelection() {
		$this->htmlString .= "</select>";
	}

    public function BeginTable($name=null, $class=null) {
    	if ($name) $nameText = " name='$name'";
    	else $nameText = "";
		if ($class) $classText = " class='$class'";
		else $classText = "";
		$this->htmlString .= "<table$nameText$classText><tbody>";
	}

    public function EndTable() {
		$this->htmlString .= "</tbody></table>";
	}
	
    public function BeginTableRow() {
		$this->htmlString .= "<tr>";
	}
	
    public function EndTableRow() {
		$this->htmlString .= "</tr>\n";
	}
	
    public function BeginTableHeadingCell($colSpan=1) {
		if ($colSpan>1) $colSpanText = " colspan=$colSpan";
		else $colSpanText = "";
		$this->htmlString .= "<th$colSpanText>";
	}
	
    public function EndTableHeadingCell() {
		$this->htmlString .= "</th>";
	}
	
	public function TableHeadingCell($text, $colSpan=1) {
    	$this->BeginTableHeadingCell($colSpan);
		$this->Text($text);
		$this->EndTableHeadingCell();
	}
	
    public function BeginTableCell($style='', $class='', $colSpan=1, $rowSpan=1) {
    	$cellAttribs = '';
        if ($style) $cellAttribs .= " style='$style'";
		if ($class) $cellAttribs .= " class='$class'";
		if ($colSpan>1) $cellAttribs .= " colspan='$colSpan'";
		if ($rowSpan>1) $cellAttribs .= " rowspan='$rowSpan'";
		$this->htmlString .= "<td$cellAttribs>";
	}
	
    public function EndTableCell() {
		$this->htmlString .= "</td>";
	}
	
	public function TableCell($text, $style='', $class='', $colSpan=1, $rowSpan=1) {
    	$this->BeginTableCell($style, $class, $colSpan, $rowSpan);
		$this->Text($text);
		$this->EndTableCell();
	}
	
	public function Image($src, $text, $height=0, $width=0, $class="") {
		$attributes = "";
		if ($text) $attributes .= " alt='$text' title='$text'";
		if ($height) $attributes .= " height='$height'";
		if ($width) $attributes .= " width='$width'";
		if ($class) $attributes .= " class='$class'";
		$this->htmlString .= "<img src='$src'$attributes >";
	}
}


?>
