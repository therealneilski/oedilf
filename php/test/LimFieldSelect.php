<?php
class LimFieldSelect extends LimField
{
	protected $options;
	
	public function __construct($fieldName, $screenLabel, $options) 
	{
		parent::__construct($fieldName, $screenLabel);
		$this->options = $options;
	}

	public function GetFormHtml($defaultValue)
	{
		$optionString = '';
		foreach ($this->options as $value => $string)
		{
			$optionString .= sprintf("<option value='%s'%s>%s", 
				$value, ($defaultValue==$value) ? ' selected':'', 
				htmlspecialchars($string, ENT_QUOTES));
		}
		return "<select name='$this->dbFieldName'>$optionString</select>";
	}
	
	public function InvalidEntryMessage($postValues)
	{
		if (isset($postValues[$this->dbFieldName]) and
			isset($this->options[$postValues[$this->dbFieldName]]))
			return "";
		else return "Invalid selection for ".$this->GetScreenLabel();
	}
}
?>