<?php
// general two column formatter

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimDualColFormatter extends LimFormatter 
{
	protected $limNum = 0;
	protected $tableClassStyle = '';
	
	public function __construct($loggedin, $tableClassStyle)
	{
		parent::__construct($loggedin);
		$this->tableClassStyle = $tableClassStyle;
	}
	
	public function FormatPageTop()
	{
		$this->limNum = 0;
		return "<table ".$this->tableClassStyle.">";
	}

	public function FormatPageEnd()
	{
		$WordsDisplay = '';
	    if (($this->limNum %2)==1) {
			$WordsDisplay .= $this->FormatLimerickPrelude()."&nbsp;".$this->FormatLimerickPostlude();
		}
		$WordsDisplay .= "</table>";
		return $WordsDisplay;
	}

	public function FormatLimerickPrelude() {
		$WordsDisplay = '';
		if (($this->limNum % 2)==0) {		
			$WordsDisplay .= "<tr>";
		}
		$WordsDisplay .= "<td class='lightpanel' width=50%>";
		return $WordsDisplay;
	}

	public function FormatLimerickPostlude() {
		$WordsDisplay = '';
		$WordsDisplay .= "</td>";
		if (($this->limNum % 2)==1) {		
			$WordsDisplay .= "</tr>";
		}
		$this->limNum++;
		return $WordsDisplay;
	}
}
?>