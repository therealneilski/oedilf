<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function CreatePrevNextWordButton($Word) {
  $WordsDisplay = '';

  $result = DbQuery(sprintf("SELECT DISTINCT WordText FROM DILF_Words W 
	WHERE IsPhraseWord=0 AND WordText<'%s' 
	ORDER BY WordText DESC LIMIT 1", DbEscapeString($Word)));
  if ($line = DbFetchArray($result))
    $WordsDisplay .= LightLinkSelf("Word=".rawurlencode($line['WordText']), 'Previous Word: '.$line['WordText']);
  DbEndQuery($result);

  $result = DbQuery(sprintf("SELECT DISTINCT WordText FROM DILF_Words W 
	WHERE IsPhraseWord=0 AND WordText>'%s' 
	ORDER BY WordText LIMIT 1", DbEscapeString($Word)));
  if ($line = DbFetchArray($result))
    $WordsDisplay .= LightLinkSelf("Word=".rawurlencode($line['WordText']), 'Next Word: '.$line['WordText']);
  DbEndQuery($result);

  return $WordsDisplay;
}

function GetPrevNextSearchIds($OriginalId) {
  $LimNums = explode(",",$_SESSION['SearchList']);
  $total = count($LimNums);
  if (!$LimNums[0]) $total = 0;
  $firstId = 0;
  $prevId = 0;
  $nextId = 0;
  $lastId = 0;
  $newindex = 0;
  $index = $_SESSION['SearchIndex'] ? $_SESSION['SearchIndex']-1 : 0;
  if ($index>($total-1)) $index = 0;
  if ($total) {
    $firstId = $LimNums[0];
    $lastId = $LimNums[$total-1];
    $key = array_search($OriginalId, $LimNums);
    if ($key!==FALSE)
      $index = $key;
    if ($index>0) $prevId=$LimNums[$index-1];
    if ($index<($total-1)) $nextId=$LimNums[$index+1];
  }
  $newindex = $index+1 ;
  $_SESSION['SearchIndex'] = $newindex;
  return array($firstId, $prevId, $nextId, $lastId, $newindex, $total);
}

function CreatePrevNextSearchButtons($firstId, $prevId, $nextId, $lastId, $thisIndex, $total) {
  // search buttons
  $WordsDisplay = 'Search ('.$thisIndex.'/'.$total.'): ';

 if ($firstId > 0)
   $WordsDisplay .= LightLinkSelf("Workshop=".$firstId, "&lt;&lt;");

 if ($prevId > 0)
   $WordsDisplay .= LightLinkSelf("Workshop=".$prevId, "&lt;");

 if ($nextId > 0)
   $WordsDisplay .= LightLinkSelf("Workshop=".$nextId, "&gt;");

 if ($lastId > 0)
    $WordsDisplay .= LightLinkSelf("Workshop=".$lastId, "&gt;&gt;");

 $WordsDisplay .= '<br>';

  return $WordsDisplay;
}

function FormatWorkshopSearch() {
  global $member;
  $WordsDisplay = '';

  $WordsDisplay .= '<h3>Search Limerick Database</h3>';
  $WordsDisplay .= sprintf('<form action="%s" method="get">', PHPSELF);
  $WordsDisplay .= '<table class="widetable">';

  $AuthorList = AuthorSelectionList(0, FALSE, TRUE);

  $WordsDisplay .= '<tr><td class="darkpanel">Author</td><td class="lightpanel">';
  $WordsDisplay .= '<select name="searchauthor" id="searchauthor"><option value="0" selected>(any author)<option value="active">(active author)' . $AuthorList .'</select></td></tr>';

  $WordsDisplay .= '<tr><td class="darkpanel">State</td><td class="lightpanel">';
  $WordsDisplay .= '<select name="searchstate" id="searchstate"><option value="0" selected>(any)<option value="new">new<option value="tentative">tentative<option value="confirming">confirming<option value="approved">approved<option value="revised">revised<option value="excluded">bone yard<option value="stored">bottom drawer<option value="held">held<option value="untended">untended</select></td></tr>';

  $WordsDisplay .= '<tr><td class="darkpanel">Workshop State</td><td class="lightpanel">';
  $WordsDisplay .= '<select name="searchwsstate" id="searchwsstate"><option value="0" selected>(any)<option value="none">unworkshopped<option value="started">started</select></td></tr>';

  $WordsDisplay .= '<tr><td class="darkpanel">RFAs (at least)</td><td class="lightpanel">';
  $WordsDisplay .= '<select name="searchrfamin" id="searchrfamin"><option value="0" selected>(any)
	<option value="1">1<option value="2">2<option value="3">3<option value="4">4<option value="5">5</select></td></tr>';

  $WordsDisplay .= '<tr><td class="darkpanel">RFAs (at most)</td><td class="lightpanel">';
  $WordsDisplay .= '<select name="searchrfamax" id="searchrfamax"><option value="-1" selected>(any)
	<option value="0">0<option value="1">1<option value="2">2<option value="3">3<option value="4">4<option value="5">5</td></tr>';

  $WordsDisplay .= '<tr><td class="darkpanel">My RFA</td><td class="lightpanel">';
  $WordsDisplay .= '<select name="searchmyrfa" id="searchmyrfa"><option value="-1" selected>(don\'t care)<option value="0">No<option value="1">Yes</td></tr>';

//  $WordsDisplay .= '<tr><td class="darkpanel">In Showcase</td><td class="lightpanel">';
//  $WordsDisplay .= '<select name="searchshowcase" id="searchshowcase"><option value="" selected>(don\'t care)<option value="0">No<option value="1">Yes</td></tr>';

//  searching by specific topic is also enabled, from the topic pages
  $WordsDisplay .= '<tr><td class="darkpanel">In Topic Browser</td><td class="lightpanel">';
  $WordsDisplay .= '<select name="searchtopic" id="searchtopic"><option value="" selected>(don\'t care)<option value="No">No<option value="Yes">Yes</td></tr>';
  $WordsDisplay .= '<tr><td class="darkpanel">In Curtained Room</td><td class="lightpanel">';
  $WordsDisplay .= '<select name="searchcurtain" id="searchcurtain"><option value="0" selected>(any)<option value="normal">uncurtained<option value="curtained room">curtained</select></td></tr>';

  $WordsDisplay .= '<tr><td class="darkpanel">Find in limerick</td><td class="lightpanel">';
  $WordsDisplay .= '<input type=text name="searchtext" id="searchtext"></td></tr>';

  $WordsDisplay .= '<tr><td class="darkpanel">Find in notes</td><td class="lightpanel">';
  $WordsDisplay .= '<input type=text name="searchnotes" id="searchnotes"></td></tr>';

  $WordsDisplay .= '<tr><td class="darkpanel">Find defined word</td><td class="lightpanel">';
  $WordsDisplay .= '<input type=text name="searchword" id="searchword"></td></tr>';

  $WordsDisplay .= '<tr><td class="darkpanel">With comment by</td><td class="lightpanel">';
  $WordsDisplay .= '<select name="searchcommentby" id="searchcommentby"><option value="0">(any author)<option value="-1">(anyone but me)' . $AuthorList .'</select></td></tr>';

  $WordsDisplay .= '<tr><td class="darkpanel">Find in comments</td><td class="lightpanel">';
  $WordsDisplay .= '<input type=text name="searchcomments" id="searchcomments"></td></tr>';

  $WordsDisplay .= '<tr><td class="darkpanel">Submission year</td><td class="lightpanel">';
  $WordsDisplay .= '<input type=text name="searchyear" id="searchyear"></td></tr>';
  
  $WordsDisplay .= '<tr><td class="darkpanel">Sort by</td><td class="lightpanel">';
  $WordsDisplay .= '<select name="searchorder" id="searchorder">
    <option value="Submission" selected>Submission Date</option>
    <option value="Word">Defined Word</option>
	<option value="Revision">Last Revision Date</option>
    <option value="Comment">Last Comment Date</option>
    <option value="RFAs">RFA count</option>
    <option value="State">Last State Change</option>
    <option value="Random">Random</option></select></td></tr>';

  $WordsDisplay .= '<tr><td class="darkpanel" colspan=2>'.
		SubmitButton("searchstart", "Search");
  if ($member->CanAdministrate())
    $WordsDisplay .= ' <input type=checkbox name="ShowHistory">Show Histories';
  $WordsDisplay .= '</td></tr>';

  $WordsDisplay .= '</table></form>';

  $WordsDisplay .= '<script>
        $(document).ready(function() {
		$("#searchauthor").select2({width: "250px"});
		$("#searchstate").select2({width: "250px"});
		$("#searchwsstate").select2({width: "250px"});
		$("#searchrfamin").select2({width: "250px"});
		$("#searchrfamax").select2({width: "250px"});
		$("#searchmyrfa").select2({width: "250px"});
		$("#searchtopic").select2({width: "250px"});
		$("#searchcurtain").select2({width: "250px"});
		$("#searchorder").select2({width: "250px"});
		$("#searchcommentby").select2({width: "250px"});
	});
    </script>
';

  return $WordsDisplay;
}

function ProcessTopicSearch() {
	global $member, $CurtainFilter;
	$WordsDisplay = '';
	$workshopSearch = new LimWorkshopSearch();
	$workshopSearch->AddTitle('Search Results: Topic Search');
	$workshopSearch->AddTable("DILF_TopicLims TL");
	$workshopSearch->AddTable("DILF_Topics T");
	$workshopSearch->AddFilter("AND L.OriginalId = TL.OriginalId");
    $workshopSearch->AddFilter("AND TL.TopicId=T.TopicId");
    $workshopSearch->AddFilter("AND T.Name LIKE '%".$_GET['searchtopic']."%'");
    $workshopSearch->AddFilter("AND (L.Verse LIKE '%".$_GET['searchtext']."%' OR L.AuthorNotes LIKE '%".$_GET['searchtext']."%')");
	$workshopSearch->AddOrder("L.OriginalId");
	// now do the search and build a list of matches
	$workshopSearch->Search($CurtainFilter);
	$_SESSION['SearchList'] = $workshopSearch->GetOriginalIdList();
	$_SESSION['SearchTitle'] = trim($workshopSearch->GetTitle(), "; ");
	$WordsDisplay .= FormatSearchResults();
	return $WordsDisplay;
}

function ProcessLACEListSearch() {
	global $member, $CurtainFilter;
    $memberid = $member->GetMemberId();
	$WordsDisplay = '';
	$workshopSearch = new LimWorkshopSearch();
	$workshopSearch->AddTitle('Search Results: STCable List');
	$workshopSearch->AddSelect(", L.SecondaryAuthorId,
		count(R.OriginalID) as rfa_count,
		sum(R.AuthorID<>$memberid) as rfa_order");
	$workshopSearch->AddTable("DILF_RFAs R");
	$workshopSearch->AddFilter("AND (L.State='tentative')");
    $workshopSearch->AddFilter("AND (R.OriginalId=L.OriginalId)");
    $workshopSearch->AddFilter("AND (L.PrimaryAuthorId<>$memberid)");
    $workshopSearch->AddFilter("AND (L.SecondaryAuthorId<>$memberid)");
    $workshopSearch->AddFilter("AND ((L.RFAs-L.PrimaryRFA-L.SecondaryRFA)>=".RFAS_NEEDED.")");
    $workshopSearch->AddFilter("AND (PrimaryRFA>0)");
    $workshopSearch->AddFilter("AND (SecondaryRFA>0 OR SecondaryAuthorId=0) GROUP BY OriginalId");
	$workshopSearch->AddOrder("rfa_order DESC");
	// now do the search and build a list of matches
	$workshopSearch->Search($CurtainFilter);
	$_SESSION['SearchList'] = $workshopSearch->GetOriginalIdList();
	$_SESSION['SearchTitle'] = trim($workshopSearch->GetTitle(), "; ");
	$WordsDisplay .= FormatSearchResults();
	return $WordsDisplay;
}

function ProcessWorkshopSearch() {
	global $member, $CurtainFilter;
	$WordsDisplay = '';

	if ($member->CanWorkshop()) 
	{
        StripMagicQuotes();
		$workshopSearch = new LimWorkshopSearch();
		
		if ($_GET['Search']=='LACEList') 
			$WordsDisplay .= ProcessLACEListSearch();
	    else if ($_GET['Search']=='TopicSearch') 
			$WordsDisplay .= ProcessTopicSearch();
		else 
		{
			$workshopSearch->AddTitle('Search Results:');

			$GroupByOriginalId = FALSE;
	        if ($_GET['searchauthor']=='active') 
			{
				// special marker for active authors only
                $workshopSearch->AddTable('DILF_Authors A');
				$workshopSearch->AddFilter(sprintf('AND (A.AuthorId=L.PrimaryAuthorId OR
					A.AuthorId=L.SecondaryAuthorId) AND A.AccessTime > %d', 
					time()-INACTIVE_TIME));
				$workshopSearch->AddTitle('Active Authors;');
	        }
			else if (($_GET['searchauthor']<>'0') and ($_GET['searchauthor']<>'')) 
			{
                $workshopSearch->AddFilter(sprintf('AND (L.PrimaryAuthorId=%d OR
                    L.SecondaryAuthorId=%d)',
                    intval($_GET['searchauthor']), intval($_GET['searchauthor'])));
                $workshopSearch->AddTitle('Author='.GetAuthorHtml($_GET['searchauthor']).';');
			}

			if (($_GET['searchstate']<>'0') and ($_GET['searchstate']<>'')) 
			{
				$workshopSearch->AddFilter(sprintf("AND (L.State='%s')", DbEscapeString($_GET['searchstate'])));
				$workshopSearch->AddTitle('State='.htmlspecialchars($_GET['searchstate'], ENT_QUOTES).';');
			}

			if (($_GET['searchwsstate']<>'0') and ($_GET['searchwsstate']<>'')) 
			{
				$workshopSearch->AddFilter(sprintf("AND (L.WorkshopState='%s')", DbEscapeString($_GET['searchwsstate'])));
				$workshopSearch->AddTitle('Workshop State='.htmlspecialchars($_GET['searchwsstate'], ENT_QUOTES).';');
			}

			if (($_GET['searchcurtain']<>'0') and ($_GET['searchcurtain']<>''))
			{
				$workshopSearch->AddFilter(sprintf("AND (L.Category='%s')", DbEscapeString($_GET['searchcurtain'])));
				$workshopSearch->AddTitle('Category='.htmlspecialchars($_GET['searchcurtain'], ENT_QUOTES).';');
			}

			if ($_GET['searchtext']<>'') 
			{
				$workshopSearch->AddFilter(sprintf("AND (L.Verse LIKE '%%%s%%')", DbEscapeString($_GET['searchtext'])));
				$workshopSearch->AddTitle('Limerick contains='.htmlspecialchars($_GET['searchtext'], ENT_QUOTES).';');
			}

			if ($_GET['searchnotes']<>'') 
			{
				$workshopSearch->AddFilter(sprintf("AND ((L.AuthorNotes LIKE '%%%s%%') OR
					(L.EditorNotes LIKE '%%%s%%'))",
					DbEscapeString($_GET['searchnotes']), DbEscapeString($_GET['searchnotes'])));
				$workshopSearch->AddTitle('Notes contain='.htmlspecialchars($_GET['searchnotes'], ENT_QUOTES).';');
			}

			if ($_GET['searchword']<>'') 
			{
				$workshopSearch->AddTable('DILF_Words WD');
				$workshopSearch->AddFilter(sprintf("AND WD.VerseId=L.VerseId AND (WD.WordText LIKE '%s')",
					DbEscapeString($_GET['searchword'])));
				$workshopSearch->AddTitle('Defined Word='.htmlspecialchars($_GET['searchword'], ENT_QUOTES).';');
			}

			$topicSearch = null;
			if (($_GET['searchtopic']=='Yes') || ($_GET['searchtopic']=='No')) 
			{
				$topicSearch = new LimWorkshopSearch("DILF_TopicLims L");
				$topicSearch->Search("");
			    $topicSearch->AddTitle($_GET['searchtopic']=='0' ? 
					'No Topics;' : 'With Topic(s);');
			}
			else if ($_GET['searchtopic']<>'') 
			{
				// specific topic number: find its name
				$result = DbQuery(sprintf('SELECT Name from DILF_Topics 
					WHERE TopicId=%d LIMIT 1', intval($_GET['searchtopic'])));
				if ($line=DbFetchArray($result))
					$workshopSearch->AddTitle('Topic='.htmlspecialchars($line['Name'], ENT_QUOTES).';');
				DbEndQuery($result);

				$workshopSearch->AddTable('DILF_TopicLims T');
				$workshopSearch->AddFilter(sprintf('AND T.OriginalId=L.OriginalId AND T.TopicId=%d',
					intval($_GET['searchtopic'])));
			}

			$notMeCommentSearch = null;
			$commentSearch = null;
			if ($_GET['searchcommentby']=='-1') 
			{
				$notMeCommentSearch = new LimWorkshopSearch("DILF_Workshop L");
				$notMeCommentSearch->AddFilter("AND L.AuthorId=".$member->GetMemberId());
				$notMeCommentSearch->Search("");
				$notMeCommentSearch->AddTitle('No comments by me;');
		    }
			else if (($_GET['searchcommentby']<>'0') and ($_GET['searchcommentby']<>'')) 
			{
				$commentSearch = new LimWorkshopSearch("DILF_Workshop L");
				$commentSearch->AddFilter(sprintf("AND L.AuthorId=%d",
					intval($_GET['searchcommentby'])));
				$commentSearch->AddTitle('Comment By='.GetAuthorHtml($_GET['searchcommentby']).';');
			}
			
			if ($_GET['searchcomments']<>'') 
			{
				if (!$commentSearch)
					$commentSearch = new LimWorkshopSearch("DILF_Workshop L");
				$commentSearch->AddFilter(sprintf('AND (L.Message LIKE \'%%%s%%\')',
					DbEscapeString($_GET['searchcomments'])));
				$commentSearch->AddTitle('Comment contains='.htmlspecialchars($_GET['searchcomments'], ENT_QUOTES).';');
			}
			if ($commentSearch)
				$commentSearch->Search("");

			if ($_GET['searchyear']<>'')
			{
				$year = 0+$_GET['searchyear'];
				$result = DbQuery("SELECT MAX(L.OriginalId) AS 'Max', MIN(L.OriginalId) AS 'Min' FROM DILF_Limericks L WHERE L.Version=0 AND LEFT(L.DateTime, 4)=$year");
				if (($line = DbFetchArray($result)) and ($line['Max']))
				{
					$workshopSearch->AddFilter(sprintf("AND (L.OriginalId>='%s') AND (L.OriginalId<='%s')", $line['Min'], $line['Max']));
					$workshopSearch->AddTitle('Submission Year='.$year.';');
				}
				DbEndQuery($result);
			}
				
			$HavingRFA='';
			$SelectRFA='';
			if (($_GET['searchrfamin']<>'0') and ($_GET['searchrfamin']<>'')) 
			{
				$workshopSearch->AddFilter(sprintf('AND (L.RFAs>=%d)', intval($_GET['searchrfamin'])));
				$SelectRFA = ', L.RFAs as rfa_count';
				$GroupByOriginalId = TRUE;
				$workshopSearch->AddTitle('RFA count &gt;='.$_GET['searchrfamin'].';');
			}
			if (($_GET['searchrfamax']<>'-1') and ($_GET['searchrfamax']<>'')) 
			{
				$workshopSearch->AddFilter(sprintf('AND (L.RFAs<=%d)', intval($_GET['searchrfamax'])));
				$SelectRFA = ', L.RFAs as rfa_count';
				$GroupByOriginalId = TRUE;
				$workshopSearch->AddTitle('RFA count &lt;='.$_GET['searchrfamax'].';');
			}

			if (($_GET['searchmyrfa']<>'-1') and ($_GET['searchmyrfa']<>'')) 
			{
				$workshopSearch->AddJoin('LEFT JOIN DILF_RFAs R ON R.OriginalId=L.OriginalId');
				if ($_GET['searchmyrfa']==0)
				{
					$HavingRFA .= ' AND ((my_rfa=0) OR ISNULL(my_rfa))';
					$workshopSearch->AddTitle('Without my RFA;');
				}
				else
				{
					$HavingRFA .= ' AND (my_rfa=1)';
					$workshopSearch->AddTitle('With my RFA;');
				}
				$SelectRFA .= sprintf(', MAX(R.AuthorID=%d) AS my_rfa', $member->GetMemberId());
				$GroupByOriginalId = TRUE;
			}

			if ($_GET['searchshowcase']<>'') {
			    $workshopSearch->AddFilter(sprintf('AND L.ShowcasePriority=%d', intval($_GET['searchshowcase'])));
			    $workshopSearch->AddTitle('Showcase='.intval($_GET['searchshowcase']).';');
			}

			$SearchGroupBy = '';
			if ($GroupByOriginalId) $SearchGroupBy = 'L.OriginalId';

			
			if ($SearchGroupBy<>'')
				$workshopSearch->AddFilter("GROUP BY ".$SearchGroupBy);
			$workshopSearch->AddFilter("HAVING 1 ".$HavingRFA);
			
			$workshopSearch->AddSelect($SelectRFA);

			// now do the search and build a list of matches
			$workshopSearch->Search($CurtainFilter);
			if ($topicSearch) 
				FilterByTopicYesNo($workshopSearch, $topicSearch, ($_GET['searchtopic']=='Yes'));
			if ($notMeCommentSearch)
			{
				$workshopSearch->AddTitle($notMeCommentSearch->GetTitle());
				$workshopSearch->AndNotResults($notMeCommentSearch->GetOriginalIdArray());
			}
			if ($commentSearch)
			{
				$workshopSearch->AddTitle($commentSearch->GetTitle());
				$workshopSearch->AndResults($commentSearch->GetOriginalIdArray());
			}
			$_SESSION['SearchList'] = $workshopSearch->GetOriginalIdList();
			$_SESSION['SearchTitle'] = trim($workshopSearch->GetTitle(), "; ");

            if ($_SESSION['SearchList'])
            {
                unset($workshopSearch);
                OrderSearchResults($_GET['searchorder']);
            }

			$WordsDisplay .= FormatSearchResults();
		}
	}
	else $WordsDisplay .= '<p>You must be logged in as a workshopper to view this page.</p>';

	return $WordsDisplay;
}

function OrderSearchResults($searchOrder) // change $_SESSION['SearchList'] to match the order requested
{
    $orderClauses = array("Submission"=>"L.OriginalId",
        "Word"=>"WD.SortOrder",
        "Revision"=>"L.DateTime",
        "Comment"=>"L.UpdateDateTime",
        "RFAs"=>"rfa_count DESC",
        "State"=>"L.StateDateTime",
        "Random"=>"Rand");
    $orderClause = $orderClauses[$searchOrder];

    if (!$orderClause) $orderClause = "L.OriginalId"; // default for missing or invalid ordering

    $workshopSearch = new LimWorkshopSearch();

    $workshopSearch->AddFilter("AND L.OriginalId IN (".$_SESSION['SearchList'].")");
    $workshopSearch->AddOrder($orderClause);

	if ($searchOrder=="RFAs")
        $workshopSearch->AddSelect(", L.RFAs as rfa_count");
    else if ($searchOrder=='Random')
        $workshopSearch->AddSelect(", RAND() Rand");
    else if ($searchOrder=='Word')
    {
        $workshopSearch->AddTable('DILF_Words WD');
        $workshopSearch->AddFilter("AND WD.VerseId=L.VerseId");
        $workshopSearch->AddFilter("AND WD.Sequence=0 AND WD.IsPhraseWord=0");
    }

    $workshopSearch->Search("");
    $_SESSION['SearchList'] = $workshopSearch->GetOriginalIdList();
}

function FilterByTopicYesNo($workshopSearch, $topicSearch, $present)
{
	$workshopSearch->AddTitle($topicSearch->GetTitle());
	if ($present)
		$workshopSearch->AndResults($topicSearch->GetOriginalIdArray());
	else
		$workshopSearch->AndNotResults($topicSearch->GetOriginalIdArray());
}

function FormatSearchResults() {
  $WordsDisplay = '';
  $pageParams = array("Action=SearchResults");
  
  $start = intval($_GET['Start']);
  if ($_GET['Pagelen']) {
    $pagelen = intval($_GET['Pagelen']);
	$pageParams[] = "Pagelen=".$pagelen;
  }
  else {
    $pagelen = 20;
  }

  $WordsDisplay .= FormatLimericksFromList($_SESSION['SearchTitle'], $_SESSION['SearchList'],
    $start, $pagelen, $pageParams, $NumLimericks, $_GET['ShowHistory']);

  return $WordsDisplay;
}

function FormatSTCableLim() {
  global $member;
  $memberid = $member->GetMemberId();
  $WordsDisplay = '';

  if ($member->CanEdit()) {

    // get exclusion list of all limericks grabbed in the last day, and all the limericks you have grabbed in the last six weeks
    $result = DbQuery(sprintf("SELECT VerseId FROM DILF_Log 
        WHERE (DateTime>'%s' OR (DateTime>'%s' AND AuthorId=%d)) AND Action='Limerick Grabbed For STC'",
	    LimTimeConverter::FormatGMDateFromNow(-24*3600), LimTimeConverter::FormatGMDateFromNow(-42*24*3600), $member->GetMemberId()));
    $sep='';
    $excludelist = '';
    while ($line = DbFetchArray($result)) {
      $excludelist .= $sep.$line['VerseId'];
      $sep = ',';
    }
    DbEndQuery($result);
    if ($excludelist)
      $excludelist = "AND NOT (L.OriginalId IN ($excludelist))";

    // make a list of inactive authors to avoid
    $InactiveTime = time() - INACTIVE_TIME;
    $result = DbQuery("SELECT AuthorId FROM DILF_Authors WHERE AccessTime<'$InactiveTime' AND UserType<>'applicant' AND Rank<>'R'");
    $sep='';
    $ExcludeAuthors = '';
    while ($line = DbFetchArray($result)) {
      $ExcludeAuthors .= $sep.$line['AuthorId'];
      $sep = ',';
    }
    DbEndQuery($result);

    if ($ExcludeAuthors)
      $excludelist .= " AND NOT (L.PrimaryAuthorId IN ($ExcludeAuthors)) AND NOT (L.SecondaryAuthorId IN ($ExcludeAuthors))";

    $NeedRFAs = RFAS_NEEDED+1;
    $NeedRFAsInc = RFAS_NEEDED+2;

    // find the list of eligible limericks that have an RFA by the current member
    $result = DbQuery("SELECT L.OriginalId
FROM DILF_RFAs R, DILF_Limericks L
WHERE
 R.AuthorId=$memberid AND
 L.OriginalId=R.OriginalId AND
 L.State = 'tentative' AND
 L.PrimaryRFA>0 AND
 (L.SecondaryRFA>0 OR L.SecondaryAuthorId=0)AND
 (L.PrimaryAuthorId<>$memberid) AND
 (L.SecondaryAuthorId<>$memberid) AND
 ((L.SecondaryAuthorId=0 AND L.RFAs>=$NeedRFAs) OR
 (L.SecondaryAuthorId!=0 AND L.RFAs>=$NeedRFAsInc))");
    $sep='';
    $OwnRFAList = '';
    while ($line = DbFetchArray($result)) {
      $OwnRFAList .= $sep.$line['OriginalId'];
      $sep = ',';
    }
    DbEndQuery($result);
    // create a way to subtract 1 if the current member is in the RFA list.
    if ($OwnRFAList)
      $OwnRFAList = "-(L.OriginalId IN ($OwnRFAList))";

    $result = DbQuery("SELECT L.OriginalId, (L.RFAs $OwnRFAList) AS RFACount
FROM DILF_Limericks L
WHERE
 L.State='tentative' AND
 L.PrimaryRFA>0 AND
 (L.SecondaryRFA>0 OR L.SecondaryAuthorId=0)AND
 (L.PrimaryAuthorId<>$memberid) AND
 (L.SecondaryAuthorId<>$memberid) AND
 ((L.SecondaryAuthorId=0 AND L.RFAs>=$NeedRFAs) OR
 (L.SecondaryAuthorId!=0 AND L.RFAs>=$NeedRFAsInc))
 $excludelist
GROUP BY OriginalId
ORDER BY RFACount DESC, UpdateDateTime ASC
LIMIT 1");

    if ($line = DbFetchArray($result)) {
      AuditLog($line['OriginalId'], 'Limerick Grabbed For STC');
      SetRedirect("Workshop=".$line['OriginalId']);
    }
    else
      $WordsDisplay .= '<p>No STCable limericks available at the moment.</p>';
    DbEndQuery($result);
  }
  else $WordsDisplay .= '<p>You must be logged in as an Associate Editor to view this page.</p>';

  return $WordsDisplay;
}

function htmlspecialchars_nodouble($text, $quote_style, $charset)
{
    $text = htmlspecialchars($text, $quote_style, $charset);
    return preg_replace('~&amp;(#[0-9]+;)~', '&$1', $text);
}

//----------------------------------------------------------------
function FormatWordLookup() {
  global $CurtainFilter, $member;
  $WordsDisplay = '';
  StripMagicQuotes();

  $_GET['Word'] = substr($_GET['Word'], 0, 100);

  $ret = htmlspecialchars_nodouble($_GET['Word'], ENT_QUOTES, "UTF-8");
  Dump($ret);
  
  $cleanedword = trim(htmlspecialchars_nodouble($_GET['Word'], ENT_QUOTES, "UTF-8"), " \n\t");
  $match = trim(str_replace(array('%', '_'), array('\%', '\_'), $_GET['Word']), " \n\t");
  $start = $_GET['Start']+0;

  if ($match=='%') {
    return "<p>Your search string matches too many words. Please be more specific.</p>";
    //---------------------------------------------------Early return
  }
  
  if (defined('CACHE_LOOKUPS') and !LimSession::LoggedIn()) {
    $CacheFile = $CurtainFilter.'_Word_'.$_GET['Word']."_".$start;
    $CacheFile = "cache/".rawurlencode(utf8_encode($CacheFile)).".htm";
    if (substr(PHPSELF, -3, 3)!="php")
      $CacheFile .= rawurlencode(utf8_encode(PHPSELF));
    //echo $CacheFile."<br >";
    if (file_exists($CacheFile)) {
      $CacheAge = time() - filectime($CacheFile);
      if ($CacheAge < 7*24*3600) {
        if ($f = @fopen($CacheFile, "r")) {
          $WordsDisplay .= fread($f, filesize($CacheFile));
          fclose($f);
          //echo "Read the file<br >".file_exists($CacheFile)."<br >";
          $WordsDisplay .= sprintf("<br ><small>This search was cached %.1f hours ago.</small>", $CacheAge/3600);
          return($WordsDisplay);
        }
      }
    }
    //else echo "No Cache<br >";
  }

  $WordsDisplay .= CreatePrevNextWordButton($_GET['Word']);

  $searchCondition = CreateSearchCondition("SearchText", $match, ToSearchText);

  $WordsDisplay .= FormatLimerickList("Limericks on <i>$cleanedword</i>",
    "SELECT DISTINCT L.* FROM DILF_Limericks L, DILF_Words W
    WHERE $searchCondition AND L.VerseId=W.VerseId  AND (State='tentative' OR State='confirming' OR State='approved') $CurtainFilter ORDER BY W.IsPhraseWord ASC, L.State DESC, W.SortOrder, L.OriginalId",
    $start, 10, 'Word='.rawurlencode($_GET['Word']), $NumLimericks);

  if ($NumLimericks==0) {
    // First see if we have a limerick but have it hidden.
    $result = DbQuery(sprintf("SELECT DISTINCT L.* FROM DILF_Limericks L, DILF_Words W WHERE WordText LIKE '%s' AND L.VerseId=W.VerseId", DbEscapeString($match)));
    $HiddenMatches = DbQueryRows($result);
    DbEndQuery($result);

    if ($HiddenMatches)
      $WordsDisplay .= "<p>The OEDILF contains at least one limerick matching your search but no matches can be displayed at the moment. Either the matching limericks are not yet ready for public display or they are hidden due to content filtering.</p>";

    // work out similar words
	$searchCondition = CreateSearchCondition("Sound", $match, ToSoundText);
	LimGeneral::VarDump($searchCondition);
    
	$result = DbQuery(sprintf("SELECT DISTINCT WordText FROM DILF_Words W, DILF_Limericks L
		WHERE $searchCondition AND W.Sound<>'' AND W.VerseId=L.VerseId AND 
		(L.State='tentative' OR L.State='confirming' OR L.State='approved')
		$CurtainFilter 
		ORDER BY W.SortOrder"));
	$NumWords = DbQueryRows($result);
	if ($NumWords==0) $WordsDisplay .= "<p>No similar sounding words found.</p>";
	else 
	{
		$WordsDisplay .= sprintf('<table class="widetable">
            <tr><th>No matching limericks found for %s. Would you like to try:</th></tr>
            <tr><td class="lightpanel">',
			$cleanedword);
		while ($line = DbFetchArray($result))
		{
			$MatchWord = $line['WordText'];
			$WordsDisplay .= sprintf('<a href="%s?Word=%s">%s</a> ', 
				PHPSELF, rawurlencode($MatchWord), $MatchWord);
		}
		$WordsDisplay .= '</td></tr></table>';
	}	
	DbEndQuery($result);
    
	// nearby words
	$limWord = new LimWord($match);
	$sortWord = DbEscapeString($limWord->SortOrder());
	$startLetter = DbEscapeString(substr($limWord->SortOrder(), 0, 1));
	$nearList = array();
	
	$result = DbQuery("SELECT DISTINCT WordText FROM DILF_Words W, DILF_Limericks L
		WHERE LEFT(W.SortOrder,1)='$startLetter' AND W.SortOrder<='$sortWord' AND
		W.VerseId=L.VerseId AND 
		(L.State='tentative' OR L.State='confirming' OR L.State='approved')
		$CurtainFilter 
		ORDER BY W.SortOrder DESC LIMIT 10");
	while ($line = DbFetchArray($result)) $nearList[] = $line['WordText'];
	DbEndQuery($result);
	$nearList = array_reverse($nearList);
	
	$result = DbQuery("SELECT DISTINCT WordText FROM DILF_Words W, DILF_Limericks L
		WHERE LEFT(W.SortOrder,1)='$startLetter' AND W.SortOrder>'$sortWord' AND
		W.VerseId=L.VerseId AND 
		(L.State='tentative' OR L.State='confirming' OR L.State='approved')
		$CurtainFilter 
		ORDER BY W.SortOrder LIMIT 10");
	while ($line = DbFetchArray($result)) $nearList[] = $line['WordText'];
	DbEndQuery($result);
	
	if (count($nearList)>0) 
	{
		$WordsDisplay .= '<table class="widetable"><tr><th>Nearby words:</th></tr><tr><td class="lightpanel">';
		foreach ($nearList as $MatchWord)
			$WordsDisplay .= sprintf('<a href="%s?Word=%s">%s</a> ', 
				PHPSELF, rawurlencode($MatchWord), $MatchWord);
		$WordsDisplay .= '</td></tr></table>';
	}
  }
  
  if (!LimSession::LoggedIn()) {
    //echo "Writing to $CacheFile <br >";
    if ($f = @fopen($CacheFile, "w")) {
      fwrite($f, $WordsDisplay);
      fclose($f);
    }
    //if ($f===FALSE) echo "Problem opening file<br >";
  }
  return $WordsDisplay;
}

function ToSearchText($word)
{
	$limWord = new LimWord($word);
	$match = $limWord->SearchText();
	return DbEscapeString($match);
}

function ToSoundText($word)
{
	$limWord = new LimWord($word);
	$match = $limWord->Sound();
	return DbEscapeString($match);
}

function ToSearchAnywhere($word)
{
	return DbEscapeString("%".$word."%");
}

function CreateSearchCondition($fieldName, $match, $ToText) 
{	
	$lowerMatch = strtolowerascii($match);
	$condition = "(";
	$condition .= $fieldName." LIKE '".$ToText($match)."'";
	$word = new LimWord($match);
	$otherMatches = $word->CreateWordPermutations();
	foreach ($otherMatches as $altMatch)
		$condition .= " OR ".$fieldName." LIKE '".$ToText($altMatch)."'";
	$condition .= ")";
	// var_dump($condition);
	return $condition;
}

//----------------------------------------------------------------
function FormatWordSearch() {
    global $CurtainFilter;
    $WordsDisplay = '';
    StripMagicQuotes();
    $_GET['Word'] = substr($_GET['Word'], 0, 100);

    $cleanedword = trim(htmlspecialchars_nodouble($_GET['Word'], ENT_QUOTES, "UTF-8"), " \n\t");
    $match = trim(str_replace(array('%', '_'), array('\%', '\_'), $_GET['Word']), " \n\t");
    $start = $_GET['Start']+0;
  
    $condition = "(".CreateSearchCondition("Verse", $match, ToSearchAnywhere).
        " OR ".CreateSearchCondition("AuthorNotes", $match, ToSearchAnywhere).")";

    $contentQuery = "SELECT * FROM DILF_Limericks
        WHERE (State='tentative' OR State='confirming' OR State='approved')
        AND $condition $CurtainFilter ORDER BY State DESC, OriginalId";

    $WordsDisplay .= FormatLimerickList("Limericks containing <b>$cleanedword</b>",
        $contentQuery, $start, 10,
        array('button=Search', 'Word='.rawurlencode($_GET['Word'])), $NumLimericks);

    return $WordsDisplay;
}

?>
