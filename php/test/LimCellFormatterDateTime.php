<?php
class LimCellFormatterDateTime extends LimCellFormatterText
{
	protected $converter;
	
	public function __construct($timeOffsetHours)
	{
		$this->converter = new LimTimeConverter($timeOffsetHours);
	} 
	
	public function FormatCell($cellValue)
	{
		return $this->converter->LocalDateTime($cellValue, 0);
	}
}
?>