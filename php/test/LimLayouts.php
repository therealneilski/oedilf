<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function FormatBanner() {
  $WordsDisplay = '';
  $WordsDisplay .= '<table class="bannertable"><tr>';
  $WordsDisplay .= sprintf('<td class="banner" id="bannerlogocell">
  	<a href="%s"><img src="oedilfi.jpg" id="bannerlogoimg" alt="OEDILF" ></a></td>', PHPSELF);

  $WordsDisplay .= '<td class="banner">';
  $WordsDisplay .= FormatSocialButtons();
  $WordsDisplay .= '<h1>OEDILF</h1><div class="subheading">The Omnificent English Dictionary In Limerick Form</div>';
  if (LimSession::LoggedIn())
    $WordsDisplay .= '<div>'.FormatDocument('Member Announcement', FALSE, TRUE).'</div>';
  else
    $WordsDisplay .= '<div>'.FormatDocument('Announcement', FALSE, TRUE).'</div>';
    
  $WordsDisplay .= '</td>';
  $WordsDisplay .= '<td class="banner" id="bannerrldcell" rowspan="2">';
  $WordsDisplay .= FormatRandomLimerick();
  $WordsDisplay .= '</td></tr>';
  
  $WordsDisplay .= '<tr><td class="banner" id="bannerlookupcell" colspan=2>';
  $WordsDisplay .= '<div>'.FormatDictionaryLookup().'</div>';
  $WordsDisplay .= '</td></tr>';
  
  if (LimSession::LoggedIn())
    $WordsDisplay .= '<tr><td class="banner" id="banneronlinecell" colspan=3>'.FormatAuthorsOnline().'</td></tr>';
  $WordsDisplay .= '</table>';

  return $WordsDisplay;
}

function FormatDictionaryLookup() {
	$WordsDisplay = '';

	if (isset($_GET['Word']))  
	{
		$Word=$_GET['Word'];
        Dump($Word);
		if (get_magic_quotes_gpc()) 
			$Word = stripslashes($Word);
        Dump($Word);
		$Word = htmlspecialchars($Word, ENT_QUOTES);
        $Word = preg_replace('~&amp;(#[0-9]+;)~', '&$1', $Word);
        Dump($Word);
	}
	else $Word="";
	$WordsDisplay .= sprintf('<form action="%s" method="get">', PHPSELF);
	$WordsDisplay .= '<div>Find this word or phrase: <input type=text name="Word" value="'.$Word.'">';
	$WordsDisplay .= ' '.SubmitButton("WordLookupButton", "Defined", "Search for a definition of a specific word or phrase");
	$WordsDisplay .= ' '.SubmitButton("WordSearchButton", "Used", "Search for any mention of the word in limericks in which it may not specifically be defined");
	$WordsDisplay .= ' in our dictionary.</div>';
	$WordsDisplay .= '</form>';
	return $WordsDisplay;
}

function FormatSocialButtons() {
    return <<<ENDSOCIALBUTTONS
<div class="social">
<a href="https://twitter.com/OEDILF" class="twitter-follow-button" data-show-count="false">Follow @OEDILF</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<br/>
<div class="fb-like" data-href="https://facebook.com/oedilf" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
</div>
ENDSOCIALBUTTONS;

}



?>
