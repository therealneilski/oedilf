<?php
if (isset($_GET['AltDir'])) $dbDir = $_GET['AltDir'];
else $dbDir = "db";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>OEDILF - Map</title>
    <link href="../<?php echo $dbDir; ?>/LimIce15.css" rel="stylesheet" type="text/css" />
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAnV1w8W_RtmlWLiWjoJT1FhQzFqO5RLrcri-_nPK3VBI4SGZD4xTAaMV1cEmMqk8dUG9OTsTqXu6HSg"
      type="text/javascript"></script>
    <script type="text/javascript">

    //<![CDATA[
    var currentMarker;

    function load() {
      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        map.addControl(new GLargeMapControl());
        map.addControl(new GMapTypeControl());
<?php
  if ($_GET['AuthorId']) {
    if ($_GET['Lat']<99) {
      echo "map.setCenter(new GLatLng(".$_GET['Lat'].", ".$_GET['Lng']."), 2);\n";
    }
    else {
      echo "map.setCenter(new GLatLng(52.67402718287737, -8.565902709960938), 2);\n";
    }
  }
  else echo "map.setCenter(new GLatLng(19, -8.565902709960938), 2);\n";

?>
        // Creates a marker at the given point with the given number label
        function createMarker(point, label) {
          var marker = new GMarker(point, {title:label});
          /* GEvent.addListener(marker, "click", function() {
            marker.openInfoWindowHtml(label);
          }); */

          return marker;
        }
<?php
  if ($_GET['AuthorId']) {
    echo 'currentMarker = createMarker(new GLatLng('.$_GET['Lat'].', '.$_GET['Lng'].'), "Place this marker by clicking on the map.");';
?>
        map.addOverlay(currentMarker);
        GEvent.addListener(map, "click", function(marker, point) {
            if (!marker) {
              map.removeOverlay(currentMarker);
              currentMarker = createMarker(point, "Place this marker by clicking on the map.");
              map.addOverlay(currentMarker);
              document.getElementById("savelocation").href = "/<?php echo $dbDir; ?>/Lim.php?Lat="+point.lat()+"&Lng="+point.lng();
              document.getElementById("savelocation").style.display = "inline";
              document.getElementById("cancel").title = "Return to the OEDILF without changing your marker";
            }
        });
<?php
  }
  else {
    define('IN_LIM', true);
    require "../$dbDir/LimGeneral.php";
    require "../$dbDir/LimSession.php";
    require "../$dbDir/LimDb.php";
    require "../$dbDir/LimDbFunctions.php";
    LimDbConnect($dblink);

    echo "var allUsers = [";
    $result = DbQuery("SELECT A.Lat, A.Lng, A.Name, R.Title
        FROM DILF_Authors A, DILF_Ranks R
        WHERE A.Lat<99 AND A.Rank=R.Rank AND A.Rank<>'R'
            AND A.UserType<>'banned' AND A.UserType<>'applicant'
            AND A.AccessTime > ".(time() - 3 * 30 * 24 * 3600));
    $sep="";
    while ($line = DbFetchArray($result)) {

      printf("\n$sep [%s,%s, \"%s (%s)\"]", $line['Lat'], $line['Lng'], $line['Name'], $line['Title']);
      $sep=",";
    }
    DbEndQuery($result);
    echo "];\n";
    LimDbDisconnect($dblink);
?>
        for (var i = 0; i < allUsers.length; i++) {
          map.addOverlay(createMarker(new GLatLng(allUsers[i][0], allUsers[i][1]), allUsers[i][2]));
        }
<?php
  }
?>
      }
    }

    //]]>
    </script>
  </head>
  <body onload="load()" onunload="GUnload()">
   <table class="bannertable" style="width:100%;"><tr>
    <td class="banner"><h1>OEDILF</h1><div class="subheading">The Omnificent English Dictionary In Limerick Form</div>
   </td>
<?php
  if ($_GET['AuthorId']) {
?>
   <td><p>Select your map location by clicking on the map. For greater accuracy, use the controls on the left of the map to zoom in. If you don't want to show exactly where you live, place your marker on a park or library nearby.
(You can reposition the map by dragging your mouse while holding the left mouse button. Double-click your mouse to center the map on a particular location.)</p>
   </td>
<?php
  }
?>
   </tr></table>
   <table class="menutable"><tbody><tr><td class="menus">
      <a class="nav" id="savelocation" style="display:none;" title="Click this when you've placed your marker" href="/<?php echo $dbDir; ?>/Lim.php">Save My Location</a>
      <a class="nav" id="cancel" title="Return to the OEDILF" href="/<?php echo $dbDir; ?>/Lim.php">Cancel</a>
<?php
  if ($_GET['AuthorId']) {
?>
      <a class="nav" id="clearlocation" title="This will remove your location marker from the map" href="/<?php echo $dbDir; ?>/Lim.php?Lat=100&Lng=100">Remove My Location</a>
<?php
  }
?>
   <a class="nav" id="maphelp" href="http://local.google.com/support/bin/answer.py?answer=16532&topic=1499">Can't see a map</a></td></tr></tbody></table>
   <div id="map" style="width:100%; height:400px"></div>
  </body>
</html>
