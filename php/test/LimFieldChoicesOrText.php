<?php
class LimFieldChoicesOrText extends LimField
{
	protected $options;
	protected $size;
	
	public function __construct($fieldName, $screenLabel, $options, $size) 
	{
		parent::__construct($fieldName, $screenLabel);
		$this->options = $options;
		$this->size = $size;
	}

	public function GetFormHtml($defaultValue)
	{
		$optionString = '';
		$isChecked = false;
		foreach ($this->options as $value => $string)
		{
			$match = ($defaultValue.""==$value.""); // must be compared as strings!
			if ($match) $isChecked = true;
			$optionString .= sprintf("<input type='radio' name='$this->dbFieldName' value='%s'%s>%s<br>", 
				$value, ($match ? ' checked' : ''), 
				htmlspecialchars($string, ENT_QUOTES));
		}
		$userOptionId = $this->dbFieldName."UserId";
		$optionString .= sprintf("<input type='radio' name='$this->dbFieldName' id='$userOptionId' value='9999'%s>".
			"<input type='text' name='".$this->dbFieldName."UserText' value='%s' ".
			"size='$this->size' onfocus='document.getElementById(\"$userOptionId\").checked=true'>", 
			($isChecked ? '' : ' checked'), 
			($isChecked ? '' : htmlspecialchars($defaultValue, ENT_QUOTES)));
		
		return $optionString;
	}
	
	public function GetSqlUpdateValue($postValues)
	{
		$value = "";
		if (isset($postValues[$this->dbFieldName]))
		{
			$value = $postValues[$this->dbFieldName];
			if ($value==9999) 
				$value = $postValues[$this->dbFieldName."UserText"];
		}
		return $value;
	}

	public function InvalidEntryMessage($postValues)
	{
		return "";
	}
}
?>