<?php
class LimFieldConfirmedPassword extends LimField
{
	protected $retyped;
    protected $allowEmpty;
	
	public function __construct($fieldName, $screenLabel, $allowEmpty=true)
	{
		parent::__construct($fieldName, $screenLabel);
		$this->retyped = $this->dbFieldName."Retyped";
        $this->allowEmpty = $allowEmpty;
	}	
	
	public function GetFormHtml($defaultValue)
	{
		return "<input type='password' name='$this->dbFieldName' value=''><br>".
			"<input type='password' name='$this->retyped' value=''> (Retype)";
	} 	

	public function HasValue($postValues)
	{
		return (!$this->allowEmpty
            or (isset($postValues[$this->dbFieldName]) and (strlen($postValues[$this->dbFieldName])>0))
			or (isset($postValues[$this->retyped]) and (strlen($postValues[$this->retyped])>0)));
	}
	
	public function InvalidEntryMessage($postValues)
	{
		$p1 = isset($postValues[$this->dbFieldName]) ? $postValues[$this->dbFieldName] : "";
		$p2 = isset($postValues[$this->retyped]) ? $postValues[$this->retyped] : "";

        if ($p1!=$p2) return "Password and retyped password do not match.";
        else if (!$this->allowEmpty && $p1=="")
            return "This field must not be left empty.";
        else return "";
	}
	
	public function GetSqlUpdateValue($postValues)
	{
		if (isset($postValues[$this->dbFieldName]))
			return md5($postValues[$this->dbFieldName]);
		else return "";
	}
}
?>