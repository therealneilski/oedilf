<?php
// two column formatter for showcase

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimShowcaseFormatter extends LimDualColFormatter 
{
	public function FormatLimerickBlock()
	{
		global $member;
		
		$html = new LimHtml();
		$html->BeginDiv("showcasetitle");
		$Words = QueryWordList($this->LimRecord['VerseId']);
		$html->Text(implode('; ', $Words));
		$html->EndDiv();
		$html->Text(FormatStyledQuoteMeLine($this->LimRecord['OriginalId']));
		
		$AN = SplitAN($this->LimRecord['AuthorNotes']);
		if ($AN[0])
		{
			$html->BeginDiv("limerickplan");
			$html->Text(FormatQuickDocLinks(AddBreaks($AN[0])));
			$html->EndDiv();
		}
		
		$html->BeginDiv("showcaseverse");
		$html->Text(FormatQuickDocLinks(AddBreaks($this->LimRecord['Verse'])));
		$html->EndDiv();
		
		if ($AN[1])
		{
			$html->BeginDiv("limericknotes");
			$html->Text(FormatQuickDocLinks(AddBreaks($AN[1])));
			$html->EndDiv();
		}
		
		if ($this->LimRecord['EditorNotes']<>'')
		{
			$html->BeginDiv("limericknotes");
			$html->Text(FormatEditorNotes(FormatQuickDocLinks(AddBreaks($this->LimRecord['EditorNotes']))));
			$html->EndDiv();
		}
					
		$topics = LimerickTopicLinks($this->LimRecord['OriginalId']);
		if ($topics) 
		{
			$html->BeginDiv("limericknotes");
			$html->Text('Topics: '.$topics);
			$html->EndDiv();
		}

		if ($this->LimRecord['PrimaryAuthorId'] == $member->GetMemberId())
			$this->FormatPriorityControls($html);
		
		return $html->FormattedHtml();
	} 
	
	private function FormatPriorityControls($html)
	{
		$html->BeginDiv("limerickdetails");
		
		$html->Text('Importance ');
		if ($this->LimRecord['ShowcasePriority']>1)
			$html->LinkSelf(array("ShowcaseAction=Priority", 
					"ShowcasePri=".$this->LimRecord['VerseId'], 
					"NewPri=".($this->LimRecord['ShowcasePriority']-1)), 
				"[-]", 'Move limerick down');
		$html->Text(' '.$this->LimRecord['ShowcasePriority'].' ');
		$html->LinkSelf(array("ShowcaseAction=Priority", 
				"ShowcasePri=".$this->LimRecord['VerseId'], 
				"NewPri=".($this->LimRecord['ShowcasePriority']+1)), 
			"[+]", 'Move Limerick Up');
		$html->Text(' ');
		$html->LinkSelf(array("ShowcaseAction=Remove", 
				"ShowcaseRemove=".$this->LimRecord['VerseId']), 
			"Remove from Showcase");

		$html->EndDiv();
	}
}

?>