<?php
// Adding personal tags to limericks.

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function MenuScript() {
return '
<script type="text/javascript"><!--//--><![CDATA[//><!--

sfHover = function() {
	var sfEls = document.getElementById("mnav").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);

//--><!]]></script>
';
}

function MenuLinkSelf($params, $text, $title) {
  return sprintf('<a class="menu" title="%s" href="%s?%s">%s</a>', $title, PHPSELF, $params, str_replace(" ","&nbsp;",$text));
}

function FormatMenuItemTag($style) {
  if ($style) return "<li style='".$style."'>";
  else return '<li>';
}
function FormatMenuItemOpen($params, $text, $title, $style="") {
  return FormatMenuItemTag($style).MenuLinkSelf(DelimitParamList($params), $text, $title);
}
function FormatMenuItemClose() {
  return "</li>
";
}
function FormatMenuItem($params, $text, $title, $style="") {
  return FormatMenuItemOpen($params, $text, $title, $style).FormatMenuItemClose();
}

function FormatWikiLink() {
  return '<a class="nav" title="Visit the Wiki to seek answers to your questions" href="/wiki/">Wiki/Help</a>';
}

function FormatMainMenu() {
	// Remember: Any menu that must be accessible should be a top menu. When JavaScript is disabled, the submenus are unavailable
	global $member, $Configuration;
    $memberid = $member->GetMemberId();
	$WordsDisplay = '';
	$MenuSep = " ";
	
	$WordsDisplay .= '<table class="menutable"><tr><td class="menus">';
	$WordsDisplay .= "<ul id='mnav'>".
	FormatMenuItemOpen("", "Home", "Main Page", 'width:4em;').
	"<ul>".
	FormatMenuItem("View=About", "About", "What's it all about?").
	FormatMenuItem("Topic=1", "Limericks by topic", "Browse limericks by topic").
	FormatMenuItem("Show=Topics", "Limerick topic map", "Show all topics").
	FormatMenuItem("ShowcaseAction=Index", "Author Showcases", "View the limerick showcases").
	FormatMenuItem("Show=Authors", "Limericks by author", "Show a list of all limerick authors").
	"</ul>".FormatMenuItemClose();
    
	if (LimSession::LoggedIn()) {

		$WordsDisplay .= FormatMenuItem("Action=New", "Add a Limerick", 
			"I can write a limerick", "width:7em;");
		
		$WordsDisplay .= FormatMenuItemOpen("AuthorId=".$memberid, "My Limericks", 
			"List the limericks I've contributed", "width:7em;").
			"<ul>".
			FormatMenuItem(array("AuthorId=$memberid", "MyLimOrder=Revised"), "By revision date", 
				"Show my limericks sorted by last revision date").
			FormatMenuItem(array("AuthorId=$memberid", "MyLimOrder=Action"), "By workshopping date", 
				"Show my limericks sorted by last workshopping date").
			FormatMenuItem(array("AuthorId=$memberid", "MyLimOrder=TNum"), "By creation date", 
				"Show my limericks sorted by creation date").
			FormatMenuItem(array("AuthorId=$memberid", "MyLimOrder=DW"), "By defined word", 
				"Show my limericks sorted by defined word").
			FormatMenuItem(array("AuthorId=$memberid", "MyLimOrder=State"), "By state", 
				"Show my limericks sorted by limerick state").
			"</ul>".FormatMenuItemClose();
		
		$WordsDisplay .= FormatMenuItemOpen("Action=Activity", "Activity", 
			"A summary of recent comments", "width:5em;").
			"<ul>".
			FormatMenuItem("Action=RFATable", "RFA Table", "Show limericks that are ready for approval");

		if ($member->CanWorkshop()) {
			$WordsDisplay .= FormatMenuItem("Action=ShowTags", "Tags", "Show my list of limerick tags");
			$WordsDisplay .= FormatMenuItem("Action=ConfirmingTable", "Confirming Limericks", 
				"Show limericks scheduled for approval");
			$WordsDisplay .= FormatMenuItem(array("Action=GrabWnoaLim", "WnoaRFAs=-1"), 
				"Grab WNOA Limerick", 
				"Workshop a random Who Needs Our Attention limerick");
			$WordsDisplay .= FormatMenuItem(array("Action=GrabWeoaLim", "WeoaRFAs=-1"), 
				"Grab Random Limerick", 
				"Workshop a random limerick");
			$WordsDisplay .= FormatMenuItem(array("Action=GrabWeoaLim", "WeoaRFAs=0"), 
				"Random Limerick: 0 RFAs", 
				"Workshop a random limerick with no RFAs");
			$WordsDisplay .= FormatMenuItem(array("Action=GrabWeoaLim", "WeoaRFAs=1"), 
				"Random Limerick: 1 RFA", 
				"Workshop a random limerick with 1 RFA");
			$WordsDisplay .= FormatMenuItem(array("Action=GrabWeoaLim", "WeoaRFAs=2"), 
				"Random Limerick: 2 RFAs", 
				"Workshop a random limerick with 2 RFAs");
			$WordsDisplay .= FormatMenuItem(array("Action=GrabWeoaLim", "WeoaRFAs=3"), 
				"Random Limerick: 3 RFAs", 
				"Workshop a random limerick with 3 RFAs");
			$WordsDisplay .= FormatMenuItem(array("Action=GrabWeapLim", "WeapRFAs=-1"), 
				"Grab Project Limerick", 
				"Workshop a random current project limerick");
		}
		if ($member->CanChat()) {
			$WordsDisplay .= FormatMenuItemTag("").
				'<a class="menu" title="Go to the common chat room in a new browser window."
					href="../chat/OEDILFChat.php" target="_blank">Chat</a>'.
				FormatMenuItemClose();			
			$WordsDisplay .= FormatMenuItemTag("").
				'<a class="menu" title="Open the common chat room in split screen."
					href="LimFrameChat.php" target="_top">Split Screen Chat</a>'.
				FormatMenuItemClose();
			$WordsDisplay .= FormatMenuItemTag("").
				'<a class="menu" title="Close the split screen chat."
					href="Lim.php" target="_top">Close Split Screen</a>'.
				FormatMenuItemClose();
		}
		$WordsDisplay .= "</ul>".FormatMenuItemClose();

  
		$WordsDisplay .= FormatMenuItemOpen("Profile=Show", "Profile", 
			"My personal information", "width:5em;").
      		"<ul>".
			FormatMenuItem("Profile=Edit", "Edit Profile Settings", "").
			FormatMenuItem("LoginManager=ChangePassword", "Change Password", "").
			FormatMenuItem(array("ShowcaseAction=Author", "ShowcaseAuthor=".$memberid), "View Showcase/Bio", "");
		if ($member->CanSetLocation()) {
			$WordsDisplay .= FormatMenuItemTag("").
			    FormatMapLink("menu").
				FormatMenuItemClose();
		}
		$WordsDisplay .= "</ul>".FormatMenuItemClose();

    
		$WordsDisplay .= FormatMenuItemOpen(
			array(sprintf("Todo=[! <%s]", $Configuration['AlphabetEnd']), "Needed=1"),
			"Words", "Words that haven't been limericked", "width:5em;");
		$WordsDisplay .= "<ul>";
		$WordsDisplay .= FormatMenuItem("Action=RandomWords", "Random Words", 
			"Display 10 random words from the unlimericked words list");
		if ($member->CanWorkshopPlus()) {
			$WordsDisplay .= FormatMenuItem("Action=NewWords", "New Words", 
				"Adjust the unlimericked words list");
		} 
		$WordsDisplay .= "</ul>".FormatMenuItemClose();
		
		if ($member->CanEdit()) {
			$WordsDisplay .= FormatMenuItemOpen("Action=GrabSTCableLim", "Grab STC", 
				"Get the next limerick eligible for STC (Set To Confirming)", "width:5em;").
				"<ul>".
				FormatMenuItem(array("searchstart=Search", "Search=LACEList"), "STCables", 
					"Limericks eligible for STC (Set To Confirming)").
				"</ul>".FormatMenuItemClose();
		}
		
		if ($member->CanAdministrate()) {
			$WordsDisplay .= FormatMenuItemOpen("Admin=Settings", "Admin.", "Not for everybody...", "width:5em;").
				"<ul>".
				FormatMenuItem("Documents=Show", "Documents", "Edit the help documents").
				FormatMenuItem("Utility=WEAP", "Configure WEAP/Project", "Set the WEAP targets").
				FormatMenuItem("Utility=WNOA", "Configure WNOA", "Set the WNOA targets").
				FormatMenuItem("Utility=WEOA", "Configure Random Limericks", "Set the Random limerick target list").
				"</ul>".FormatMenuItemClose();
		}
		
		if ($member->CanWorkshop()) {
			$WordsDisplay .= FormatMenuItem("Action=WorkshopSearch", "Search", 
				"Advanced Search", "width:4em;");
		}

	} else {
		// logged out menus
		if ($_SESSION['OpenCurtainMode']=='On') $FilterText = "Content Filter (Filter is Off)";
		else $FilterText = "Content Filter (Filter is On)";
		$WordsDisplay .= FormatMenuItem("View=Filter", $FilterText, 
			"Choose whether to filter limericks by content", "width:12em;");
	}
  
	$WordsDisplay .= "</ul>";
	$WordsDisplay .= '</td><td class="menusright">';
	
	$WordsDisplay .= '<a class="nav" title="Go to the discussion forums" href="/forum/">Forum</a>';
	$WordsDisplay .= $MenuSep.FormatWikiLink();
  
	if (LimSession::LoggedIn()) {
		$WordsDisplay .= $MenuSep.LightLinkSelf("LoginManager=Logout", "Log out ".GetAuthorHtml($memberid));
	}
	else {
		$WordsDisplay .= $MenuSep.NavLinkSelf("LoginManager=Login", "Log In", 
			"Log in (for those who've already joined)");
		$WordsDisplay .= $MenuSep.NavLinkSelf("Action=Join", "Join the Project", "It's free!");
	}
	$WordsDisplay .= "</td></tr></table>";
	return $WordsDisplay;
}

function FormatMapLink($class) {
	global $member;
	return sprintf('<a class="'.$class.'" title="Set your world map location" 
		href="../map/LimWorldMap.php?AuthorId=%d&amp;Lat=%s&amp;Lng=%s">Map&nbsp;Location</a>',
		$member->GetMemberId(), $member->Record('Lat'), $member->Record('Lng'));
}

?>
