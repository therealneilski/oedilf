<?php
// basic limerick formatter

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimFormatter {
	protected $LimRecord;
	
	public $ShowTentativeWarnings;
	public $ShowLinks = TRUE;
	public $ShowWordDefinitionLinks = FALSE;
	
	public function __construct($loggedin) {
		$this->ShowTentativeWarnings = $loggedin;
	} 

    public function SetLimerick($limRecord) {
		$this->LimRecord = $limRecord;
	}

	public function FormatLimerickBlock() {
		$WordsDisplay = '';
		if (!$this->ShowTentativeWarnings and ($this->LimRecord['State']=='tentative'))
			$WordsDisplay .= '<p>'.TentativeWarning().'</p>';
		$WordsDisplay .= FormatLimBlock($this->LimRecord, $this->ShowLinks, 
			$this->ShowWordDefinitionLinks, QueryWordList($this->LimRecord['VerseId'])) ;
		return $WordsDisplay;
	} 
	
	public function FormatPageTop() {
		return "";
	}

	public function FormatPageEnd() {
		return "";
	}

	public function FormatLimerickPrelude() {
		return "";
	}

	public function FormatLimerickPostlude() {
		return "";
	}
}


?>
