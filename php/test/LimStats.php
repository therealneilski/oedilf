<?php
//
// Routines for displaying statistics & lists
//
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function FormatStatsDisplay() {
  global $Configuration;
  $WordsDisplay = '';

  $WordsDisplay .= "<h2>Statistics</h2>";
  $result = DbQuery(sprintf("SELECT * FROM DILF_Authors ORDER BY Name"));
  $NumAuthors = DbQueryRows($result);
  DbEndQuery($result);
  $result = DbQuery(sprintf("SELECT DISTINCT WordText FROM DILF_Words 
	WHERE WordText<'%s' AND IsPhraseWord=0 AND Type='Defined'", $Configuration['AlphabetEnd']));
  $NumWords = DbQueryRows($result);
  DbEndQuery($result);
  $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE (State='tentative' OR State='confirming' OR State='approved' OR State='revised')"));
  $NumLimericks = DbQueryRows($result);
  DbEndQuery($result);
  $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE State='approved'"));
  $NumApproved = DbQueryRows($result);
  DbEndQuery($result);
  $WordsDisplay .= "<table>";
  $WordsDisplay .= sprintf("<tr><td>Limericks (not counting revisions)</td><td>%d</td></tr>", $NumLimericks);
  $WordsDisplay .= sprintf("<tr><td>Approved Limericks</td><td>%d</td></tr>", $NumApproved);
  $WordsDisplay .= sprintf("<tr><td>Unique words</td><td>%d</td></tr>", $NumWords);
  $WordsDisplay .= sprintf("<tr><td>Members</td><td>%d</td></tr>", $NumAuthors);
  $WordsDisplay .= "</table>";

  $WordsDisplay .= FormatNewLimerickGraph();
  $WordsDisplay .= FormatLimerickStatesGraph();
  if ($_SESSION['loggedin'])
    $WordsDisplay .= FormatTentativesByRFAsGraph();

  $WordsDisplay .= '<iframe marginwidth="0"  marginheight="0" width="600" height="350"
src="https://www.embeddedanalytics.com/reports/displayreport?reportcode=mDtV2Q8jqP&chckcode=gahjS6ZuUQsG9iKuuSMAo8"
type="text/html" frameborder="0" scrolling="no" title="EmbeddedAnalytics - Extend the Power of Google Analytics"></iframe>';

  return $WordsDisplay;
}

function UpdateAuthorStats($AuthorId) {
  $WordsDisplay = '';
  
  $HeldCount = 0;
  $NewCount = 0;
  $ApprovedCount = 0;
  $ConfirmingCount = 0;
  $RevisedCount = 0;
  $TentativeCount = 0;
  $UntendedCount = 0;
  
  $result = DbQuery("SELECT PrimaryAuthorId, SecondaryAuthorId, State FROM DILF_Limericks WHERE (PrimaryAuthorId=$AuthorId OR SecondaryAuthorId=$AuthorId) AND (State='approved' OR State='confirming' OR State='tentative' OR State='held' OR State='new' OR State='revised' OR State='untended')");
  while ($line = DbFetchArray($result)) {
    $inc = 1;
    if (($line['SecondaryAuthorId']!=0) and ($line['SecondaryAuthorId']!=$line['PrimaryAuthorId'])) $inc = 0.5;

    if ($line['State']=='held') $HeldCount += $inc;
    else if ($line['State']=='new') $NewCount += $inc;
    else if ($line['State']=='approved') $ApprovedCount += $inc;
    else if ($line['State']=='confirming') $ConfirmingCount += $inc;
    else if ($line['State']=='revised') $RevisedCount += $inc;
    else if ($line['State']=='tentative') $TentativeCount += $inc;
    else if ($line['State']=='untended') $UntendedCount += $inc;
  }
  DbEndQuery($result);
  
  $result = DbQuery("UPDATE DILF_Authors SET
  HeldCount=$HeldCount,
  NewCount=$NewCount,
  ApprovedCount=$ApprovedCount,
  ConfirmingCount=$ConfirmingCount,
  RevisedCount=$RevisedCount,
  TentativeCount=$TentativeCount,
  UntendedCount=$UntendedCount
  WHERE AuthorId=$AuthorId LIMIT 1");
  
  return $WordsDisplay;
}

function FormatAuthorStatsList() {
  global $member;
  $WordsDisplay = '';

  $SortDir = ($_GET['SortDir'] == 'Up' ? 'Up' : 'Down');
  $SortOtherDir = ($SortDir == 'Down' ? 'Up' : 'Down');

  $SortText = ($SortDir=='Up' ? 'ASC' : 'DESC');
  $SortOtherText = ($SortOtherDir=='Up' ? 'ASC' : 'DESC');
  
  if (!$_GET['Sort']) $_GET['Sort'] = 'Approved';
  
  switch ($_GET['Sort']) {
    case 'Author': $SortOrder = "Author $SortText"; break;
    case 'Approved': $SortOrder = "Approved $SortText, Submitted $SortText, Author $SortOtherText"; break;
    case 'Limericks': $SortOrder = "Submitted $SortText, Author $SortOtherText"; break;
    case 'Backlog': $SortOrder = "Backlog $SortText, Submitted $SortText, Author $SortOtherText"; break;
    case 'Confirming': $SortOrder = "Confirming $SortText, Submitted $SortText, Author $SortOtherText"; break;
    case 'Tentative': $SortOrder = "Tentative $SortText, Submitted $SortText, Author $SortOtherText"; break;
    case 'Revised': $SortOrder = "Revised $SortText, Submitted $SortText, Author $SortOtherText"; break;
    case 'Held': $SortOrder = "Held $SortText, Submitted $SortText, Author $SortOtherText"; break;
    case 'New': $SortOrder = "New $SortText, Submitted $SortText, Author $SortOtherText"; break;
    case 'Untended': $SortOrder = "Untended $SortText, Submitted $SortText, Author $SortOtherText"; break;
    default: $SortOrder = "Approved $SortText, Submitted $SortText, Author $SortOtherText";
  }

  // Table header, with links for sorting by columns
  $WordsDisplay .= '<h3>Author Limerick Counts</h3>';
  $WordsDisplay .= '<p>Click any column heading to sort the table by that column.</p>';
  $WordsDisplay .= LinkSelf('Show=AuthorNames', 'View authors and titles<br>');
  $WordsDisplay .= '<table class="widetable" id="authortable">';
  
  $HeadingRow = '<tr><th>';
  $HeadingRow .= LinkSelf(array("Show=Authors", "Sort=Author", "SortDir=".($_GET['Sort'] == 'Author' ? $SortOtherDir : 'Up')), "Author") . '</th><th>';
  $HeadingRow .= LinkSelf(array("Show=Authors", "Sort=Approved", "SortDir=".($_GET['Sort'] == 'Approved' ? $SortOtherDir : $SortDir)), "Approved") . '</th><th>';
  $HeadingRow .= LinkSelf(array("Show=Authors", "Sort=Limericks", "SortDir=".($_GET['Sort'] == 'Limericks' ? $SortOtherDir : $SortDir)), "Submitted", "excludes held limericks") . '</th><th>';
  $HeadingRow .= LinkSelf(array("Show=Authors", "Sort=Backlog", "SortDir=".($_GET['Sort'] == 'Backlog' ? $SortOtherDir : $SortDir)), "Backlog") . '</th><th>';
  $HeadingRow .= LinkSelf(array("Show=Authors", "Sort=Confirming", "SortDir=".($_GET['Sort'] == 'Confirming' ? $SortOtherDir : $SortDir)), "Confirming") . '</th><th>';
  $HeadingRow .= LinkSelf(array("Show=Authors", "Sort=Tentative", "SortDir=".($_GET['Sort'] == 'Tentative' ? $SortOtherDir : $SortDir)), "Tentative") . '</th><th>';
  $HeadingRow .= LinkSelf(array("Show=Authors", "Sort=Revised", "SortDir=".($_GET['Sort'] == 'Revised' ? $SortOtherDir : $SortDir)), "Revised") . '</th><th>';
  $HeadingRow .= LinkSelf(array("Show=Authors", "Sort=Held", "SortDir=".($_GET['Sort'] == 'Held' ? $SortOtherDir : $SortDir)), "Held") . '</th><th>';
  $HeadingRow .= LinkSelf(array("Show=Authors", "Sort=New", "SortDir=".($_GET['Sort'] == 'New' ? $SortOtherDir : $SortDir)), "New") . '</th><th>';
  $HeadingRow .= LinkSelf(array("Show=Authors", "Sort=Untended", "SortDir=".($_GET['Sort'] == 'Untended' ? $SortOtherDir : $SortDir)), "Untended") . '</th></tr>';

  $InactiveTime = time() - INACTIVE_TIME;
  $TotApproved = 0;
  $TotConfirming = 0;
  $TotTentative = 0;
  $TotRevised = 0;
  $TotHeld = 0;
  $TotNew = 0;
  $TotUntended = 0;
  $TotalLimericks = 0;
  $TotAuthors = 0;
  
  $result = DbQuery("SELECT AuthorId, Name Author, AccessTime, ApprovedCount Approved,
    (ApprovedCount+ConfirmingCount+TentativeCount+RevisedCount+NewCount) Submitted,
    (100*(1-(ApprovedCount+ConfirmingCount)/(ApprovedCount+ConfirmingCount+TentativeCount+RevisedCount+NewCount))) Backlog,
    ConfirmingCount Confirming, TentativeCount Tentative,
    RevisedCount Revised, HeldCount Held, NewCount New, UntendedCount Untended
    FROM DILF_Authors
    WHERE ApprovedCount+ConfirmingCount+TentativeCount+RevisedCount+NewCount+UntendedCount > 0
    ORDER BY $SortOrder");
  $row = 0;	
  while ($line = DbFetchArray($result)) {
      if (($row % 30)==0) 
	    $WordsDisplay .= $HeadingRow;
	  $row++;
	  
      $id = $line['AuthorId'];
      $TotAuthors++;
      $TotalLimericks += $line['Submitted'];
      
	  if ($member->CanWorkshop())
        $link = LinkSelf(array("searchstart=Search", "searchauthor=$id"), $line['Author']);
      else
        $link = FormatAuthor($id, TRUE);
      if ($member->CanWorkshop() && $line['AccessTime']<$InactiveTime)
        $link = '<i>'.$link.'</i>';
      if ($line['Approved']>0)  
        $link = RSSLink(RSSAuthorFeedFile($id))." ".$link;
        
      if ($member->CanWorkshop() and ($line['Approved']>0))
        $ApprovedLink = LinkSelf(array("searchstart=Search", "searchauthor=$id", "searchstate=approved"), $line['Approved']);
      else if ($line['Approved']>0)
        $ApprovedLink = $line['Approved'];
      else $ApprovedLink = '&nbsp;';
      $TotApproved += $line['Approved'];

      if ($member->CanWorkshop() and ($line['Confirming']>0))
        $ConfirmingLink = LinkSelf(array("searchstart=Search", "searchauthor=$id", "searchstate=confirming"), $line['Confirming']);
      else if ($line['Confirming']>0)
        $ConfirmingLink = $line['Confirming'];
      else $ConfirmingLink = '&nbsp;';
      $TotConfirming += $line['Confirming'];

      if ($member->CanWorkshop() and ($line['Tentative']>0))
        $TentativeLink = LinkSelf(array("searchstart=Search", "searchauthor=$id", "searchstate=tentative"), $line['Tentative']);
      else if ($line['Tentative']>0)
        $TentativeLink = $line['Tentative'];
      else $TentativeLink = '&nbsp;';
      $TotTentative += $line['Tentative'];

      if ($member->CanWorkshop() and ($line['Revised']>0))
        $RevisedLink = LinkSelf(array("searchstart=Search", "searchauthor=$id", "searchstate=revised"), $line['Revised']);
      else if ($line['Revised']>0)
        $RevisedLink = $line['Revised'];
      else $RevisedLink = '&nbsp;';
      $TotRevised += $line['Revised'];

      if ($member->CanWorkshop() and ($line['Held']>0))
        $HeldLink = LinkSelf(array("searchstart=Search", "searchauthor=$id", "searchstate=held"), $line['Held']);
      else if ($line['Held']>0)
        $HeldLink = $line['Held'];
      else $HeldLink = '&nbsp;';
      $TotHeld += $line['Held'];

      if ($member->CanWorkshop() and ($line['New']>0))
        $NewLink = LinkSelf(array("searchstart=Search", "searchauthor=$id", "searchstate=new"), $line['New']);
      else if ($line['New']>0)
        $NewLink = $line['New'];
      else $NewLink = '&nbsp;';
      $TotNew += $line['New'];

      if ($member->CanWorkshop() and ($line['Untended']>0))
        $UntendedLink = LinkSelf(array("searchstart=Search", "searchauthor=$id", "searchstate=untended"), $line['Untended']);
      else if ($line['Untended']>0)
        $UntendedLink = $line['Untended'];
      else $UntendedLink = '&nbsp;';
      $TotUntended += $line['Untended'];
	  
	  // fill them in
      $WordsDisplay .= sprintf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%.0f%%</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
        $link, $ApprovedLink, $line['Submitted'], $line['Backlog'], $ConfirmingLink, $TentativeLink, $RevisedLink, $HeldLink, $NewLink, $UntendedLink);
  }
  DbEndQuery($result);
  
  $BacklogTotal = sprintf("%.0f%%", 100*(1-($TotApproved+$TotConfirming)/($TotalLimericks)));
  // build search links for bottom row (totals)
  if ($member->CanWorkshop()) {
      $ApprovedLink = LinkSelf(array("searchstart=Search", "searchstate=approved"), $TotApproved);
      $ConfirmingLink = LinkSelf(array("searchstart=Search", "searchstate=confirming"), $TotConfirming);
      $TentativeLink = LinkSelf(array("searchstart=Search", "searchstate=tentative"), $TotTentative);
      $RevisedLink = LinkSelf(array("searchstart=Search", "searchstate=revised"), $TotRevised);
      $HeldLink = LinkSelf(array("searchstart=Search", "searchstate=held"), $TotHeld);
      $NewLink = LinkSelf(array("searchstart=Search", "searchstate=new"), $TotNew);
      $UntendedLink = LinkSelf(array("searchstart=Search", "searchstate=untended"), $TotUntended);
  	  // fill them in
      $WordsDisplay .= "<tr><th>Total</th><th>$ApprovedLink</th><th>$TotalLimericks</th><th>$BacklogTotal</th><th>$ConfirmingLink</th><th>$TentativeLink</th><th>$RevisedLink</th><th>$HeldLink</th><th>$NewLink</th><th>$UntendedLink</th></tr>";
  }
  else
      $WordsDisplay .= "<tr><th>Total</th><th>$TotApproved</th><th>$TotalLimericks</th><th>$BacklogTotal</th><th>$TotConfirming</th><th>$TotTentative</th><th>$TotRevised</th><th>$TotHeld</th><th>$TotNew</th><th>$TotUntended</th></tr>";
  
  $WordsDisplay .= '</table>';
  $WordsDisplay .= "<p>$TotAuthors contributing authors found.";
  if ($member->CanWorkshop())
    $WordsDisplay .= '<br><i>Italics</i> mark authors who have not logged in for more than 30 days.';
  $WordsDisplay .= '</p>';

  return $WordsDisplay;
}

function FormatAuthorNamesList() {
  global $member;
  $WordsDisplay = '';
  $InactiveTime = time() - INACTIVE_TIME;
  $TotAuthors = 0;

  $WordsDisplay .= '<h3>Authors and Titles</h3>';
  $WordsDisplay .= LinkSelf('Show=Authors', 'View author limerick counts<br>');
  $WordsDisplay .= '<table class="widetable" id="authortable"><tr><th>Author</th><th>Title</th><th>Showcase</th></tr>';

  $result = DbQuery("SELECT A.AuthorId, A.Name, A.AccessTime, R.Title
    FROM DILF_Authors A, DILF_Ranks R
    WHERE A.Rank=R.Rank AND (A.ApprovedCount+A.ConfirmingCount+A.TentativeCount+A.RevisedCount+A.NewCount+A.UntendedCount > 0)
    ORDER BY A.Name");
  while ($line = DbFetchArray($result)) {
    $TotAuthors++;
    $id = $line['AuthorId'];
    $link = FormatAuthor($id, TRUE);
    if ($member->CanWorkshop() && $line['AccessTime']<$InactiveTime)
      $link = '<i>'.$link.'</i>';

    $WordsDisplay .= '<tr><td>'.$link.'</td><td>'.$line['Title'].'</td>';
    if (($title<>'Contributor') or LimSession::LoggedIn())
      $WordsDisplay .= '<td>'.LinkSelf(array("ShowcaseAction=Author", 'ShowcaseAuthor='.$id), 'View').'</td>';
    else
      $WordsDisplay .= '<td>&nbsp;</td>';
    $WordsDisplay .= '</tr>';
  }
  DbEndQuery($result);
  $WordsDisplay .= '</table>';
  $WordsDisplay .= "<p>$TotAuthors contributing authors found.";
  if ($member->CanWorkshop())
    $WordsDisplay .= '<br><i>Italics</i> mark authors who have not logged in for more than 30 days.';
  $WordsDisplay .= '</p>';

  return $WordsDisplay;
}

function FormatAdminGraph($GraphParam)
{
  $html = new LimHtml();
  
  if ($GraphParam==1) {
    $graph = new LimBarGraph("Pages");
    $graph->CollectData("SELECT ROUND( PageTime /600 ) Time, COUNT( * ) Count
FROM `DILF_Timing` WHERE 1
GROUP BY Time ORDER BY Time DESC");

    $graph->PlotBars("Count", 600, array('Time'=>GraphTime), $html);
  }
  else if ($GraphParam==2) {
  	if (isset($_GET['Days'])) $days = $_GET['Days']+0;
  	else $days = 1;
    $dayStart = time()-$days*24*3600;
  	$graph = new LimBarGraph("Average Page Timing");
    $graph->CollectData("SELECT ROUND( PageTime /600 ) Time, COUNT(*) Count,
    ROUND(AVG(Duration)) 'Average (msec)'
FROM `DILF_Timing` WHERE PageTime>$dayStart
GROUP BY Time ORDER BY Time DESC");

    $graph->PlotBars("Average (msec)", 500, array('Time'=>GraphTime), $html);
  }
  else if ($GraphParam==3) {
    $dayStart = time()-24*3600;
    $graph = new LimBarGraph("Last 24 hours");
    $graph->CollectData("SELECT LEFT(Parameters, 8) Params, SUM(Duration) TotalDuration, ROUND(AVG(Duration)) 'Avg (msec)', SUM(Duration * LoggedIn) MemberDuration, Count(*) Pages, SUM(LoggedIn) / COUNT(*) PercentLoggedIn
FROM DILF_Timing
WHERE PageTime>$dayStart
GROUP BY Params
ORDER BY TotalDuration DESC");

    $graph->PlotBars("TotalDuration", 300, array(), $html);
  }
  else if ($GraphParam==4) {
    $dayStart = time()-24*3600;
    $graph = new LimBarGraph("Last 24 hours");
    $graph->CollectData("SELECT LEFT(Parameters, 8) Params, SUM(Duration) TotalDuration, ROUND(AVG(Duration)) 'Avg (msec)', COUNT(*) Pages, LoggedIn
FROM DILF_Timing
WHERE PageTime>$dayStart
GROUP BY Params, LoggedIn
ORDER BY TotalDuration DESC");

    $graph->PlotBars("TotalDuration", 300, array(), $html);
  }
  else if ($GraphParam=='Searches') {
  	if (isset($_GET['Days'])) $days = $_GET['Days']+0;
  	else $days = 1;
    $dayStart = time()-$days*24*3600;
    $graph = new LimBarGraph("Word Searches, Last ".(24*$days)." hours");
    $graph->CollectData("SELECT SUBSTR(Parameters, 9, 20) Params, COUNT(*) Searches
FROM DILF_Timing
WHERE (PageTime>$dayStart) AND (SUBSTR(Parameters, 3, 20) LIKE 'Word%')
GROUP BY Params
ORDER BY Searches DESC");

    $graph->PlotBars("Searches", 500, array(), $html);
  }
  return $html->FormattedHtml();
}

function GraphTime($time) {
	global $member;
	$timeConv = new LimTimeConverter($member->Record('TimeZone'));
	return LinkSelf(array("Admin=Table", "Table=Timing", "TimeSlot=".($time*600)),
		$timeConv->LocalDateTimeFromStamp($time*600)); 
}

function FormatNewLimerickGraph()
{
  $html = new LimHtml();

  $graph = new LimBarGraph("New Limericks");
  $graph->CollectData("SELECT MID(DateTime,6,2) Month, LEFT(DateTime,4) Year, COUNT(DateTime) 'New Limericks' FROM DILF_Limericks WHERE Version=0 GROUP BY LEFT(DateTime, 7) ORDER BY DateTime");

  $graph->PlotBars("New Limericks", 400, array("Month"=>GraphMonth), $html);

  return $html->FormattedHtml();
}

function GraphMonth($month){
  return date("M", mktime(0, 0, 0, $month+0, 15, 1998));
}

function FormatLimerickStatesGraph()
{
  $html = new LimHtml();

  $graph = new LimBarGraph("Limerick States");
  $graph->CollectData("SELECT State, COUNT(*) 'Limerick Count' FROM `DILF_Limericks` WHERE State<>'obsolete' GROUP BY State");

  $graph->PlotBars("Limerick Count", 400, array(), $html);

  return $html->FormattedHtml();
}

function FormatTentativesByRFAsGraph()
{
  $html = new LimHtml();

  $graph = new LimBarGraph("Tentative Limericks By RFA Count");
  $graph->CollectData("SELECT RFAs, COUNT(*) 'Limerick Count' FROM `DILF_Limericks` WHERE State='tentative' GROUP BY RFAs ORDER BY RFAs");

  $graph->PlotBars("Limerick Count", 400, array(), $html);

  return $html->FormattedHtml();
}

?>
