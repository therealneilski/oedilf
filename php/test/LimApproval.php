<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function CreateLimerickEmail($line, $Words, $TemplateName, $TargetAuthorId, $CodedLink, &$Author, &$Email) {
  $PrimaryAuthorId = $line['PrimaryAuthorId'];
  $SecondaryAuthorId = $line['SecondaryAuthorId'];
  $OriginalId = $line['OriginalId'];
  $Limerick = trim($line['Verse'], "\n\r ");
  $Notes = trim($line['AuthorNotes'], "\n\r ");
  if (!$Notes) $Notes='none';
  $FirstLine = $LimLines[0];
  $Comment = 'none';

  // see if there's a WS comment to report
  // ignore any automatic LACE comments
  $result = DbQuery(sprintf("SELECT AuthorId, Message from DILF_Workshop WHERE VerseId='%d' AND Message NOT LIKE 'Sent Approval Confirmation%%' ORDER BY DateTime DESC LIMIT 1", $line['VerseId']));
  $line = DbFetchArray($result);
  if ($line['AuthorId'] != '')
	$Comment = sprintf("%s%s", ($line['AuthorId'] ? GetAuthor($line['AuthorId']).': ' : ''), $line['Message']);

  // get template email message
  $Message = GetDocument($TemplateName);

  $result = DbQuery(sprintf("SELECT * FROM DILF_Authors WHERE AuthorId=%d LIMIT 1", $TargetAuthorId));
  if ($line = DbFetchArray($result)) {
    $Author = $line['Name'];
    $Email = $line['Email'];
  }
  DbEndQuery($result);

  $keys = array("@@AUTHOR@@", "@@NUMBER@@", "@@WORD@@", "@@LIMERICK@@", "@@AN@@", "@@CODEDLINK@@", "@@WS@@");
  $strings = array($Author, $OriginalId, $Words, $Limerick, $Notes, $CodedLink, $Comment);

  return str_replace($keys, $strings, $Message);
}

function SendRejectionEmail() {
  global $member;
  $WordsDisplay = '';

  if ($member->CanWorkshopPlus()) {
    SendMailFromEiC($_POST['email'], "Limerick Rejection Notice", stripslashes($_POST['emailmessagetext']));
    $WordsDisplay .= sprintf('<p>Rejection Message Sent to %s</p>', $_POST['email']);
    LimGeneral::Log("Limerick Rejection to ".$_POST['email']);
    AuditLog($_POST['OriginalId'], 'Sent rejection to '.$_POST['email']);

    // Delete the limerick(s)
    $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE OriginalId=%d", $_POST['OriginalId']));
    if (DbQueryRows($result)>0) {
      while ($line = DbFetchArray($result)) {
        $WordsDisplay .= AnnihilateSingleLimerick($line['VerseId'], $_POST['OriginalId'], $line['Version']);
      }
      $WordsDisplay .= sprintf('<p>Limerick %d Deleted</p>', $_POST['OriginalId']);
    }
    else {
      $WordsDisplay .= '<p>Limerick not found.</p>';
    }
    DbEndQuery($result);

  }
  return $WordsDisplay;
}

function FormatRejectionEmail($VerseId) {
  global $member;
  $WordsDisplay = '';

  if ($member->CanWorkshopPlus()) {
    $line = GetVerse($VerseId);
    $PrimaryAuthorId = $line['PrimaryAuthorId'];
    $OriginalId = $line['OriginalId'];

    $Words = GetWordList($VerseId, FALSE);

    $Message = CreateLimerickEmail($line, $Words, 'Rejection Email', $PrimaryAuthorId, '', $Author, $Email);

    $WordsDisplay .= sprintf('<form action="%s?Workshop=%d" method="post" name="MsgTable">', PHPSELF, $VerseId);
    $WordsDisplay .= '<table class="widetable">';
    $WordsDisplay .= sprintf('<tr><th>Edit Rejection Message to be Sent to %s (%s)</th></tr>', htmlspecialchars($Author, ENT_QUOTES), $Email);
    $WordsDisplay .= '<tr>';
    $WordsDisplay .= FormatTableCell(sprintf('<TEXTAREA NAME="emailmessagetext" COLS=80 ROWS=9>%s</TEXTAREA>', $Message), 'darkpanel');
    $WordsDisplay .= '</tr>';
    $WordsDisplay .= '<tr>';
    $WordsDisplay .= FormatTableCell(SubmitButton("RejectButton", "Send Email and Delete Limerick").
		' (Warning: This will <b>delete all</b> revisions of the limerick)',
		'darkpanel');
    $WordsDisplay .= '</tr>';
    $WordsDisplay .= '</table>';
    $WordsDisplay .= sprintf('<input type=hidden name="email" value="%s">', $Email);
    $WordsDisplay .= sprintf('<input type=hidden name="OriginalId" value="%s">', $OriginalId);
    $WordsDisplay .= '</form>';

  }
  return $WordsDisplay;
}

define('TNUMRANDOFFS', 39746564); // just a random offset to discourage hacking - not a serious deterrent

function SendApprovalMail($TagNum, $AuthorId, $lim, $Words)
{
    $user = new LimMember($AuthorId);
    if ($user->Record('UseEmail')=='On' && $user->Record('ApprovalEmails')=='On')
    {
        $CodedLink = sprintf('http://%s?ApprovalCode=%s%d%s', THIS_APP, md5($TagNum+$AuthorId), TNUMRANDOFFS+$lim->VerseId, $AuthorId);
        $Message = CreateLimerickEmail($lim->Record, $Words, 'Coded Approval Email', $AuthorId, $CodedLink, $Author, $Email);
        SendMailFromEiC($Email, "Approve Limerick #T".$lim->OriginalId." on ".$Words, $Message);
        LimGeneral::Log("LACE to $Author $Email $CodedLink");
        AuditLog($lim->OriginalId, 'Sent LACE to '.$Email);
        $result = "Sent Approval Confirmation Email to ".htmlspecialchars($user->GetMemberName(), ENT_QUOTES);
    }
    else
    {
        LimGeneral::Log("LACE not sent to $Author $Email");
        AuditLog($lim->OriginalId, 'Not sent LACE to '.$user->Record('Email'));
        $result = "Can't send Approval Confirmation Email to ".htmlspecialchars($user->GetMemberName(), ENT_QUOTES)." due to email preferences or problems.";
    }
    return $result;
}

function SendApprovalRequest($VerseId) {
  global $member;
  $WordsDisplay = '';

  if ($member->CanEdit()) {
    $lim = new LimLimerick($VerseId, $member->GetMemberId());

    NotifyOfStateChanges($VerseId, 'Confirming');

    // Choose a random number for an access code
    $TagNum = time()+rand(100,10000);
    $result = DbQuery(sprintf("UPDATE DILF_Limericks SET LinkCode='%d', State='confirming'
                    WHERE OriginalId = '%d' AND State<>'obsolete' LIMIT 1",
        $TagNum, $lim->OriginalId));
    $Words = GetWordList($VerseId, FALSE);

    $wsMessage = SendApprovalMail($TagNum, $lim->Record['PrimaryAuthorId'], $lim, $Words);

    if ($lim->Record['SecondaryAuthorId']<>0) {
        $wsMessage .= " \n".SendApprovalMail($TagNum, $lim->Record['SecondaryAuthorId'], $lim, $Words);
    }
    $result = AddWorkshopComment($member->GetMemberId(), $VerseId, $wsMessage, $commentId);
  }

  return $WordsDisplay;
}

function YesResponse() {
  return "I'm happy with this final version of my writing. Put it into The OEDILF with my blessings and&mdash;Gee, Willikers!&mdash;it's certainly an honor to be a part of this grand undertaking!<br>";
}

function NoResponse() {
  return "Hold on there a minute! That piece was ready for Pulitzer Prize consideration before you screwed with it! My objections, major and/or minor, include the following:<br><br>".
	FormatWorkshopCommentBox('State reasons for your refusal here before clicking on the "No" button above.');
}

function FormatWorkshopApproval($VerseId) {
  $WordsDisplay = '';

  $WordsDisplay .= "<p>This is the most recent version of your limerick submitted for inclusion into The Omnificent English Dictionary In Limerick Form (The OEDILF). It has completed the workshopping process and is, with your approval, ready for permanent entry into The OEDILF. Do you approve of this piece in this form?</p>";
  $WordsDisplay .= sprintf('<form action="%s?WorkshopApproval=%d" method="post">', PHPSELF, $VerseId);

  $WordsDisplay .= '<table class="widetable"><tr><th>Your Decision:</th></tr><tr><td class="entryform">';
  $WordsDisplay .= SubmitButton("ApprovalButton", "Yes").'<br>';
  $WordsDisplay .= YesResponse();
  $WordsDisplay .= '</td></tr><tr><td class="entryform">';
  $WordsDisplay .= SubmitButton("ApprovalButton", "No").'<br>';
  $WordsDisplay .= NoResponse();
  $WordsDisplay .= sprintf('<input type=hidden name="VerseId" value="%s">', $VerseId);
  $WordsDisplay .= '</td></tr></table>';

  $WordsDisplay .= '</form>';
  return $WordsDisplay;
}

function FormatApprovalConfirmation($ApprovalCode) {
  global $CurtainFilter;
  $WordsDisplay = '';

  $Code = substr($ApprovalCode, 0, 32);
  $VerseId = substr($ApprovalCode, 32, 8)-TNUMRANDOFFS;
  $AuthorId = substr($ApprovalCode, 40)+0;

  $lim = new LimLimerick($VerseId, $AuthorId);

  $WordsDisplay .= '<p>Welcome, '.GetAuthorHtml($AuthorId).'</p>';
  if ((!$lim->IsTentative() && !$lim->IsConfirming()) || ($Code<>md5($lim->Record['LinkCode']+$AuthorId))) {
    $WordsDisplay .= "<p>The coded link you have used doesn't match a limerick that is ready for approval.
        This could be due to one of the following circumstances:
        <ul><li>The limerick may have been revised since the code was emailed.
        <li>The link may have been corrupted or incorrectly copied from your email program.
        <li>The link may have already been used to confirm or reject the limerick approval.</ul>
        Please contact an administrator for assistance.</p>";
  }
  else {
    if ($lim->Record['Category']<>'normal') {
      $CurtainFilter = ''; // stop the formatter from censoring
    }
    $WordsDisplay .= "<p>This is the most recent version of your limerick submitted for inclusion into The Omnificent English Dictionary In Limerick Form (The OEDILF). It has completed the workshopping process and is, with your approval, ready for permanent entry into The OEDILF. Do you approve of this piece in this form?</p>";
    $WordsDisplay .= FormatLimerickBlock($lim->Record, FALSE);

    $WordsDisplay .= sprintf('<form action="%s?ProcessApproval=%d" method="post">', PHPSELF, $VerseId);

    $WordsDisplay .= '<table class="widetable"><tr><th>Your Decision:</th></tr><tr><td class="entryform">';
    $WordsDisplay .= SubmitButton("ApprovalButton", "Yes").'<br>';
    $WordsDisplay .= YesResponse();
    $WordsDisplay .= '</td></tr><tr><td class="entryform">';
    $WordsDisplay .= SubmitButton("ApprovalButton", "No").'<br>';
    $WordsDisplay .= NoResponse();
    $WordsDisplay .= sprintf('<input type=hidden name="VerseId" value="%s">', $VerseId);
    $WordsDisplay .= sprintf('<input type=hidden name="AuthorId" value="%s">', $AuthorId);
    $WordsDisplay .= sprintf('<input type=hidden name="Code" value="%s">', $Code);
    $WordsDisplay .= '</td></tr></table>';

    $WordsDisplay .= '</form>';
  }
  LimGeneral::Log("Stored code=".$lim->Record['LinkCode']." Code=$Code VerseId=$VerseId AuthorId=$AuthorId");

  return $WordsDisplay;
}

function ProcessApproval($VerseId, $AuthorId) {
  $WordsDisplay = '';
  StripMagicQuotes();

  $line=GetVerse($VerseId);

  if ($_POST['ApprovalButton']=='Yes') {
    $Answer = "I'm happy to approve this limerick.";
    $WordsDisplay .= "<p>Your approval has been noted.</p>";
  }
  else if ($_POST['ApprovalButton']=='No') {
    $Answer = $_POST['workshoptext'];
    $WordsDisplay .= "<p>Your objection has been noted.</p>";
  }

  $result = AddWorkshopComment(0, $VerseId, sprintf("Confirmation Message:\n%s (author%d) said %s.\n", GetAuthorHtml($AuthorId), $AuthorId, $_POST['ApprovalButton']).$Answer, $commentId);
  AuditLog($VerseId, 'Confirm Approval: author'.$AuthorId.' '.$_POST['ApprovalButton']);

  if ($_POST['ApprovalButton']=='Yes') {
    if ($line['SecondaryAuthorId']==0) {
      $NewNum = ApproveLimerick($VerseId);
      $WordsDisplay .= sprintf('<p>Limerick T%d is now approved. Call it Limerick #%d</p>', $VerseId, $NewNum);
    }
    else {
      if ($line['PrimaryAuthorId']==$AuthorId)
        $OtherAuthor = $line['SecondaryAuthorId'];
      else $OtherAuthor = $line['PrimaryAuthorId'];

      // search for confirmation from other author
      $result2 = DbQuery(sprintf("SELECT * FROM DILF_Workshop WHERE AuthorId=0 AND VerseId=%d AND Message LIKE '%%(author%d) said Yes.%%'",
        $VerseId, $OtherAuthor));
      if (DbQueryRows($result2)<>0) {
        $NewNum = ApproveLimerick($VerseId);
        $WordsDisplay .= sprintf('<p>Limerick T%d is now approved. Call it Limerick #%d</p>', $VerseId, $NewNum);
      }
      else $WordsDisplay .= sprintf('<p>Limerick T%d awaits confirmation from %s.</p>', $VerseId, GetAuthorHtml($OtherAuthor));
      DbEndQuery($result2);
    }
  }
  else if ($_POST['ApprovalButton']=='No') {
    $result = DbQuery(sprintf("UPDATE DILF_Limericks SET LinkCode=0, State='tentative' WHERE VerseId = '%d' AND State<>'obsolete' LIMIT 1",
      $VerseId));
    // clear self RFAs
    $OriginalId = GetOriginalId($VerseId);
    DeleteRFA($OriginalId, $line['PrimaryAuthorId']);
    if ($line['SecondaryAuthorId']) DeleteRFA($OriginalId, $line['SecondaryAuthorId']);

    NotifyOfApprovalConfirmation($VerseId, $AuthorId);
  }

  SetReload('Workshop='.$VerseId);

  return $WordsDisplay;
}

function ProcessApprovalConfirmation() {
  $WordsDisplay = '';

  $Code = $_POST['Code'];
  $VerseId = $_POST['VerseId'];
  $AuthorId = $_POST['AuthorId'];

  $line=GetVerse($VerseId);

  if ((($line['State']!='tentative') and ($line['State']!='confirming')) or ($Code<>md5($line['LinkCode']+$AuthorId))) {
    $WordsDisplay .= "<p>The coded link you have used doesn't match a limerick that is ready for approval. Please review the workshop comments then contact an administrator should you require assistance.</p>";
  }
  else {
    $WordsDisplay .= ProcessApproval($VerseId, $AuthorId);
  }
  return $WordsDisplay;
}

?>