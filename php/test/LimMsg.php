<?php
//
// Routines for notifications and messages
//
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function FormatTableCell($text, $class, $width='') {
  if ($width) $wstring=' width='.$width;
  else $wstring='';
  return sprintf('<td class="%s"%s>%s</td>', $class, $wstring, $text);
}

function ProcessDeleteMessages() {
  global $member;
  $WordsDisplay = '';
  $DelCount = 0;

  if ($member->CanContribute()) {
    foreach ($_POST as $cbox => $value) {
      if ((substr($cbox, 0,6)=='DelMsg') and ($value=='on')) {
        $MsgNum = substr($cbox, 6);
        AuditLog(0, 'Deleted notification message: MessageId='.$MsgNum.' AuthorId='.$member->GetMemberId());
        $result = DbQuery(sprintf("DELETE FROM DILF_Messages WHERE MsgId=%d", $MsgNum));
        $DelCount++;
      }
    }
    $WordsDisplay .= sprintf('<p>%d messages deleted.</p>', $DelCount);
    SetReload('Action=Activity');
    $WordsDisplay .= '<p>'.LinkSelf("Action=Activity", "Activity").'</p>';

  }
  else
    $WordsDisplay .= sprintf('<p>You must be logged in before you can view messages.</p>');

  return $WordsDisplay;
}

function InitSortVars() {
  if (!$_SESSION['$SortOrder']) $_SESSION['$SortOrder'] = 'AuthorSort %s,DateTime %s';
  if (!$_SESSION['$SortAscDesc']) $_SESSION['$SortAscDesc'] = 'ASC';
}

function FormatMsgTable() {
  global $member;
  $WordsDisplay = '';

  if ($member->CanAdministrate()) {
    if (!$_SESSION['DoneCheckingExpiredSelfRFAs']) {
      CheckForExpiredSelfRFAs(FALSE); // don't show output
      $_SESSION['DoneCheckingExpiredSelfRFAs'] = TRUE; // only do it once per login
    }
  }

  if (0 and $member->CanContribute() and ($member->Record('ApprovedCount')>=1)) {
    // eligible to vote
    $result = DbQuery("SELECT * FROM banner_votes WHERE AuthorId=".$member->GetMemberId());
    $votes = DbQueryRows($result);
    DbEndQuery($result);

    $result = DbQuery("SELECT * FROM banner_sets");
    $entries = DbQueryRows($result);
    DbEndQuery($result);
    
    if ($votes>0) $action = "";
    else $action = "?Action=Start";
    
    if ($votes < $entries*($entries-1)) {
      $WordsDisplay .= '<p><a class="macro" style="background-color: #F88;" title="Have your say in choosing our banners" href="./BannerVoting.php'.$action.'">Vote for the best OEDILF banners</a></p>';
	} 
  }
   
  if ($member->CanContribute()) {
    $result = DbQuery("SELECT * FROM DILF_Authors WHERE MonitorNew='Held' AND AuthorId=".$member->GetMemberId());
    if ($line = DbFetchArray($result)) {
      // strip out notifications of new limericks that have already been released
      $result2 = DbQuery(sprintf("SELECT * FROM DILF_Messages M, DILF_Limericks L WHERE L.OriginalId=M.VerseId AND M.DstId=%d AND M.MsgType='New Limerick' AND L.State<>'new' AND L.State<>'obsolete'", $member->GetMemberId()));
      while ($line2 = DbFetchArray($result2)) {
        $result3 = DbQuery(sprintf("DELETE FROM DILF_Messages WHERE MsgId=%d", $line2['MsgId']));
        AuditLog($line2['VerseId'], 'AutoDeleted New Limerick notification: AuthorId='.$member->GetMemberId());
      }
      DbEndQuery($result2);
    }
    DbEndQuery($result);
	
    $WordsDisplay .= FormatJavaScriptSellAllFunction();
    if ($_GET['Sort']=='Time') {
      if ($_SESSION['$SortOrder'] == 'DateTime %s,DateTime %s') $_SESSION['$SortAscDesc'] = ($_SESSION['$SortAscDesc'] == 'ASC') ? 'DESC' : 'ASC';
      else {
      	$_SESSION['$SortOrder'] = 'DateTime %s,DateTime %s';
		$_SESSION['$SortAscDesc'] = 'ASC';
      }
    }
    else if ($_GET['Sort']=='TNum') {
      if ($_SESSION['$SortOrder'] == 'VerseId %s,DateTime %s') $_SESSION['$SortAscDesc'] = ($_SESSION['$SortAscDesc'] == 'ASC') ? 'DESC' : 'ASC';
      else {
      	$_SESSION['$SortOrder'] = 'VerseId %s,DateTime %s';
		$_SESSION['$SortAscDesc'] = 'ASC';
      }
    }
    else if ($_GET['Sort']=='From') {
      if ($_SESSION['$SortOrder'] == 'SrcId %s,DateTime %s') $_SESSION['$SortAscDesc'] = ($_SESSION['$SortAscDesc'] == 'ASC') ? 'DESC' : 'ASC';
      else {
      	$_SESSION['$SortOrder'] = 'SrcId %s,DateTime %s';
		$_SESSION['$SortAscDesc'] = 'ASC';
      }
    }
    else if ($_GET['Sort']=='Type') {
      if ($_SESSION['$SortOrder'] == 'MsgType %s,DateTime %s') $_SESSION['$SortAscDesc'] = ($_SESSION['$SortAscDesc'] == 'ASC') ? 'DESC' : 'ASC';
      else {
      	$_SESSION['$SortOrder'] = 'MsgType %s,DateTime %s';
		$_SESSION['$SortAscDesc'] = 'ASC';
      }
    }
    else if ($_GET['Sort']=='Author') {
      if ($_SESSION['$SortOrder'] == 'AuthorSort %s,DateTime %s') $_SESSION['$SortAscDesc'] = ($_SESSION['$SortAscDesc'] == 'ASC') ? 'DESC' : 'ASC';
      else {
      	$_SESSION['$SortOrder'] = 'AuthorSort %s,DateTime %s';
		$_SESSION['$SortAscDesc'] = 'ASC';
      }
    }

    $Sort = sprintf($_SESSION['$SortOrder'], $_SESSION['$SortAscDesc'], $_SESSION['$SortAscDesc']);

    $result = DbQuery(sprintf("SELECT *, (AuthorId<>%d)*AuthorId AuthorSort
        FROM DILF_Messages WHERE DstId=%d ORDER BY %s", $member->GetMemberId(), $member->GetMemberId(), $Sort));
    $NumMessages = DbQueryRows($result);
    $start = $_GET['Start']+0;
    if ($start<0) $start=0;
    $pagesize = 30;
    $pages = new LimPaginator('Action=Activity', $NumMessages, $pagesize, $start);

    $WordsDisplay .= "<h3>Activity ".$pages->CurrRange()."</h3>";
    $WordsDisplay .= '<p>'.LinkSelf('Action=RFATable', 'Show RFA Table') ;
    if ($member->CanWorkshop()) {
      $WordsDisplay .= ' '.LinkSelf('Action=ConfirmingTable', 'Show Confirming Limericks');
      $WordsDisplay .= ' '.LinkSelf('Action=ShowTags', 'My Tags');
    }
    $WordsDisplay .= '</p>';

    $WordsDisplay .= "This table shows a summary of recent activity. Only the first event of any type is shown for each affected limerick.<br>";
    $WordsDisplay .= "<b>Delete Your Messages:</b> After you've visited the workshop that a message refers to, you should delete the notification message so that you can see when a limerick receives more attention. You're only up to date if your Activity page is completely empty.<br>";
    if ($NumMessages>0) {
      DbQuerySeek($result, $start);
      if ($NumMessages>$pagesize) {
        $WordsDisplay .= $pages->FormatNeatMultiplePageLinks();
	  }
    }

    $WordsDisplay .= sprintf('<form action="%s?Action=ActivityMessages" method="post" name="MsgTable" id="MsgTable">', PHPSELF);
	if (DbQueryRows($result)>0)
		$WordsDisplay .= DeleteSelectDeselect();
	$WordsDisplay .= '<table class="widetable">';
    if (DbQueryRows($result)==0) $WordsDisplay .= '<tr><td>No Messages</td></tr>';
    else $WordsDisplay .= sprintf('<tr><th>Select</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>Subject</th></tr>',
      LinkSelf(array('Action=Activity', 'Sort=Time'), 'Time', 'Sort by time'),
      LinkSelf(array('Action=Activity', 'Sort=From'), 'From', 'Sort by source'),
      LinkSelf(array('Action=Activity', 'Sort=Type'), 'Type', 'Sort by notification type'),
      LinkSelf(array('Action=Activity', 'Sort=TNum'), 'Limerick', 'Sort by limerick'),
      LinkSelf(array('Action=Activity', 'Sort=Author'), 'Author', 'Sort by limerick author'));

    $rows = 0;
    while (($line = DbFetchArray($result)) AND ($rows<$pagesize)) {
      $WordsDisplay .= '<tr>';
      $WordsDisplay .= FormatTableCell(sprintf('<input type=checkbox name="DelMsg%s" >',$line['MsgId']), 'darkpanel');
      $WordsDisplay .= FormatTableCell(FormatDateTime($line['DateTime']), 'lightpanel');
      $WordsDisplay .= FormatTableCell(GetAuthorHtml($line['SrcId']), 'lightpanel');
      $WordsDisplay .= FormatTableCell($line['MsgType'], 'lightpanel');
      $WordsDisplay .= FormatTableCell($line['VerseId']<>0 ? ('#T'.$line['VerseId']) : '-', 'lightpanel');
      $WordsDisplay .= FormatTableCell($line['AuthorId']<>0 ? GetAuthorHtml($line['AuthorId']) : '-', 'lightpanel');
      if ($line['VerseId']<>0)
        $WordsDisplay .= FormatTableCell(LinkSelf('Workshop='.$line['VerseId'], '['.$line['MsgData'].']'), 'lightpanel');
      else
        $WordsDisplay .= FormatTableCell($line['MsgData'], 'lightpanel');
      $WordsDisplay .= '</tr>';
      $rows++;
    }
    $WordsDisplay .= '</table>';
    if (DbQueryRows($result)>0)
		$WordsDisplay .= DeleteSelectDeselect();
    $WordsDisplay .= FormatAddActivityTag();
    $WordsDisplay .= '</form>';
    DbEndQuery($result);

    $WordsDisplay .= $pages->FormatPaginationMenu();
  }
  else
    $WordsDisplay .= sprintf('<p>You must be logged in before you can view messages.</p>');
  return $WordsDisplay;
}

function DeleteSelectDeselect()
{
	return SubmitButton("MsgButton", "Delete Selected Messages").
      ' <a href="javascript:selall(\'MsgTable\',1);">Select All</a> <a href="javascript:selall(\'MsgTable\', 0);">Deselect All</a>';
}

function GetFirstWord($VerseId) {
  $result = DbQuery(sprintf("SELECT * FROM DILF_Words WHERE VerseId=%d AND IsPhraseWord=0 ORDER BY Sequence", $VerseId));
  if ($line = DbFetchArray($result)) {
    $FirstWord = $line['WordText'];
  }
  DbEndQuery($result);
  return addslashes($FirstWord);
}

function InsertNewMessage($SrcId, $DstId, $VerseId, $AuthorId, $DateTime, $MsgType, $MsgData) {
	DbInsert("DILF_Messages", array("SrcId"=>$SrcId, "DstId"=>$DstId, "VerseId"=>$VerseId, 
		"AuthorId"=>$AuthorId, "DateTime"=>$DateTime, "MsgType"=>$MsgType, "MsgData"=>DbEscapeString($MsgData)));
}

function NotifyOfNewLimerick($VerseId, $FirstLine) {
  global $member;
  $dt = LimTimeConverter::FormatGMDateFromNow();

  $line = GetVerse($VerseId);
  $PrimaryAuthorId = $line['PrimaryAuthorId'];

  $FirstWord = GetFirstWord($VerseId);

  AuditLog($VerseId, 'New Limerick notifications: get monitors.');
  $result = DbQuery("SELECT * FROM DILF_Authors WHERE MonitorNew<>'Off'");
  while ($line = DbFetchArray($result)) {
    if ($member->GetMemberId() <> $line['AuthorId']) {
      InsertNewMessage($member->GetMemberId(), $line['AuthorId'], $VerseId, $PrimaryAuthorId, $dt, 'New Limerick', $FirstWord.' '.$FirstLine);
      AuditLog($VerseId, 'New Limerick notification added for author '.$line['AuthorId']);
    }
  }
  DbEndQuery($result);
}

function DrawAttention($VerseId, $FirstLine) {
  global $member;
  $dt = LimTimeConverter::FormatGMDateFromNow();

  // read limerick to get originalid
  $line = GetVerse($VerseId);
  $PrimaryAuthorId = $line['PrimaryAuthorId'];
  $OriginalId = $line['OriginalId'];

  $FirstWord = GetFirstWord($VerseId);

  $result = DbQuery("SELECT * FROM DILF_Authors WHERE MonitorAttention = 'On'");
  while ($line = DbFetchArray($result)) {
    // check that the author hasn't already been notified
    $result2 = DbQuery(sprintf("SELECT * FROM DILF_Messages WHERE DstId=%d AND VerseId=%d AND MsgType='Attention'", $line['AuthorId'], $OriginalId));
    if (DbQueryRows($result2)==0)
      InsertNewMessage($member->GetMemberId(), $line['AuthorId'], $OriginalId, $PrimaryAuthorId, $dt, 'Attention', $FirstWord.' '.$FirstLine);
    DbEndQuery($result2);
  }
  DbEndQuery($result);
}

function NotifyOfWorkshopComment($VerseId, $FirstLine) {
  NotifyOfWorkshopUpdate($VerseId, $FirstLine, 'WS Comment');
}

function NotifyOfRevision($VerseId, $FirstLine) {
  NotifyOfWorkshopUpdate($VerseId, $FirstLine, 'Revision');
}

function NotifyOfRFAExpiry($VerseId, $OriginalId, $PrimaryAuthorId) {
  global $member;
  $dt = LimTimeConverter::FormatGMDateFromNow();
  InsertNewMessage($member->GetMemberId(), $PrimaryAuthorId, $OriginalId, $PrimaryAuthorId, $dt, 'State Change', 'Your self-RFA has expired. See workshop comments.');
}

// notify RFAers when RFAs are cleared or self-RFA given
function NotifyOfRFAChanges($VerseId, $Message) {
  global $member;
  $dt = LimTimeConverter::FormatGMDateFromNow();

  // read limerick to get info
  $line = GetVerse($VerseId);
  $PrimaryAuthorId = $line['PrimaryAuthorId'];
  $OriginalId = $line['OriginalId'];
  $State = $line['State'];

  $FirstWord = GetFirstWord($VerseId);
  $NotificationList = array();

  // collect the RFAers who haven't disabled this notification
  $result = DbQuery(sprintf("SELECT R.AuthorId from DILF_RFAs R, DILF_Authors A WHERE R.AuthorId=A.AuthorId AND R.OriginalId=%d AND A.MonitorRFAs<>'Off'",
    $OriginalId));    
  while ($line = DbFetchArray($result)) {
  $NotificationList[$line['AuthorId']]++;
  //echo 'Send to '.$line['AuthorId'].'<br>';
  }
  DbEndQuery($result);
  
   // now send notification to any on the list
  foreach ($NotificationList as $id=>$count) {
    if (($id<>$member->GetMemberId()) and ($State<>'stored')) {
      $result2 = DbQuery(sprintf("SELECT * FROM DILF_Messages WHERE DstId=%d AND VerseId=%d AND MsgType='%s'", $id, $OriginalId, $MsgType));
      if (DbQueryRows($result2)==0)
        InsertNewMessage($member->GetMemberId(), $id, $OriginalId, $PrimaryAuthorId, $dt, 'State Change', '('.$FirstWord.') '.$Message);
    }
  }
}

function NotifyOfWorkshopUpdate($VerseId, $FirstLine, $MsgType, $AuthorOnly = FALSE) {
  global $member;
  $dt = LimTimeConverter::FormatGMDateFromNow();

  // read limerick to get author(s)
  $line = GetVerse($VerseId);
  $PrimaryAuthorId = $line['PrimaryAuthorId'];
  $SecondaryAuthorId = $line['SecondaryAuthorId'];
  $OriginalId = $line['OriginalId'];
  $State = $line['State'];

  $FirstWord = GetFirstWord($VerseId);

  $NotificationList = array();
  // first get the author and optionally anyone who is monitoring all comments
  $result = DbQuery(sprintf("SELECT AuthorId FROM DILF_Authors WHERE %s ((MonitorComments='All' OR MonitorComments='Workshopped' OR NotifyChanges='On') AND (AuthorId=%d OR AuthorId=%d))",
    (! $AuthorOnly) ? "(MonitorComments='All') OR " : "",
    $PrimaryAuthorId, $SecondaryAuthorId));
  while ($line = DbFetchArray($result)) {
    $NotificationList[$line['AuthorId']]++;
    //echo 'Send to '.$line['AuthorId'].'<br>';
  }
  DbEndQuery($result);

  if (! $AuthorOnly) {
    // now find anyone who is monitoring the limerick and enabled monitoring
    $result = DbQuery(sprintf("SELECT M.AuthorId FROM DILF_Monitors M, DILF_Authors A WHERE M.AuthorId=A.AuthorId AND M.OriginalId=%d AND A.MonitorComments<>'Off'",
      $OriginalId));
    while ($line = DbFetchArray($result)) {
      $NotificationList[$line['AuthorId']]++;
      //echo 'Also to '.$line['AuthorId'].'<br>';
    }
    DbEndQuery($result);

    // also find anyone who has RFA'd the limerick and wants extra notifications
    if (($MsgType == 'Revision') or ($MsgType == 'State Change')) {
      $result = DbQuery(sprintf("SELECT R.AuthorId from DILF_RFAs R, DILF_Authors A WHERE R.AuthorId=A.AuthorId AND R.OriginalId=%d AND (A.MonitorRFAs='All' OR A.MonitorRFAs='Revision')",
        $OriginalId));
      while ($line = DbFetchArray($result)) {
        $NotificationList[$line['AuthorId']]++;
        //echo 'And to '.$line['AuthorId'].'<br>';
      }
      DbEndQuery($result);
    }
  }

  // now send notification to any on the list
  foreach ($NotificationList as $id=>$count) {
    if (($id<>$member->GetMemberId()) and ($State<>'stored')) {
      $result2 = DbQuery(sprintf("SELECT * FROM DILF_Messages WHERE DstId=%d AND VerseId=%d AND MsgType='%s'", $id, $OriginalId, $MsgType));
      if (DbQueryRows($result2)==0)
        InsertNewMessage($member->GetMemberId(), $id, $OriginalId, $PrimaryAuthorId, $dt, $MsgType, '('.$FirstWord.') '.$FirstLine);
    }
  }
}

function NotifyOfStateChanges($VerseId, $State) {
  global $member;
  $dt = LimTimeConverter::FormatGMDateFromNow();

  // read limerick to get author(s)
  $line = GetVerse($VerseId);
  $PrimaryAuthorId = $line['PrimaryAuthorId'];
  $SecondaryAuthorId = $line['SecondaryAuthorId'];
  $OriginalId = $line['OriginalId'];
  $LimLines = explode("\n", $line['Verse']);
  $FirstLine = $LimLines[0];

  $FirstWord = GetFirstWord($VerseId);

  // notify the author(s)
  $result = DbQuery(sprintf("SELECT AuthorId FROM DILF_Authors WHERE NotifyChanges='On' AND (AuthorId=%d OR AuthorId=%d)",
    $PrimaryAuthorId, $SecondaryAuthorId));
  while ($line = DbFetchArray($result)) {
    if ($member->GetMemberId() <> $line['AuthorId']) {
      $result2 = DbQuery(sprintf("SELECT * FROM DILF_Messages WHERE DstId=%d AND VerseId=%d AND MsgType='State Change'", $line['AuthorId'], $OriginalId));
      if (DbQueryRows($result2)==0)
        InsertNewMessage($member->GetMemberId(), $line['AuthorId'], $OriginalId, $PrimaryAuthorId, $dt, 'State Change', $State.': '.$FirstWord.' '.$FirstLine);
    }
  }
  DbEndQuery($result);

}

function NotifyOfApprovalConfirmation($VerseId, $AuthorId) {
  $dt = LimTimeConverter::FormatGMDateFromNow();

  $line = GetVerse($VerseId);
  $PrimaryAuthorId = $line['PrimaryAuthorId'];
  $SecondaryAuthorId = $line['SecondaryAuthorId'];
  $OriginalId = $line['OriginalId'];
  $LimLines = explode("\n", $line['Verse']);
  $FirstLine = $LimLines[0];

  $FirstWord = GetFirstWord($VerseId);

  $result = DbQuery("SELECT AuthorId FROM DILF_Authors WHERE UserType='administrator'");
  while ($line = DbFetchArray($result)) {
    $result2 = DbQuery(sprintf("SELECT * FROM DILF_Messages WHERE DstId=%d AND VerseId=%d AND MsgType='Approval'", $line['AuthorId'], $OriginalId));
    if (DbQueryRows($result2)==0)
      InsertNewMessage($AuthorId, $line['AuthorId'], $OriginalId, $PrimaryAuthorId, $dt, 'Approval', $FirstWord.' '.$FirstLine);
    DbEndQuery($result2);
  }
  DbEndQuery($result);
}

function NotifyOfPromotion($AuthorId, $Rank) {
  global $member;
  $dt = LimTimeConverter::FormatGMDateFromNow();

  $result = DbQuery("SELECT AuthorId FROM DILF_Authors WHERE UserType='administrator'");
  while ($line = DbFetchArray($result)) {
    InsertNewMessage($member->GetMemberId(), $line['AuthorId'], 0, $AuthorId, $dt, 'Promotion', 'Promoted to '.$Rank);
  }
  DbEndQuery($result);
}


function FormatRFATable() {
  global $member;
  $WordsDisplay = '';

  // Who=id, Who=name+with+spaces
  $WhoAuthorId = '';
  if (($_GET['Who']!='') and ($_GET['Who']!='0')) {
	if (0 < 0+$_GET['Who']) {
	    $WhoName = GetAuthor($_GET['Who']);
	    $WhoAuthorId = $_GET['Who'];
	} else {
	    $WhoAuthorId = GetAuthorId(addslashes($_GET['Who']));
	    $WhoName = $_GET['Who'];
	}
  }
  // no Who, or name not found
  if (! $WhoAuthorId) {
  	$WhoName = '';	
	$WhoAuthorId = 0;
  }

  if ($member->CanContribute()) {
     if (!$member->CanWorkshop())
	$ReduceList = 'AND (L.PrimaryAuthorId='.$member->GetMemberId().' OR L.SecondaryAuthorId='.$member->GetMemberId().')';
     else if ($WhoAuthorId)
	$ReduceList = 'AND (L.PrimaryAuthorId='.$WhoAuthorId.' OR L.SecondaryAuthorId='.$WhoAuthorId.')';
    else
	$ReduceList = '';
    $result = DbQuery("SELECT L.DateTime, L.UpdateDateTime, L.OriginalId, L.PrimaryAuthorId AuthorId, COUNT(R.OriginalId) Count, 
        L.Verse, W.WordText, MAX(R.AuthorId=L.PrimaryAuthorId) SelfRFA, MAX(R.AuthorId=".$member->GetMemberId().") MyRFA
	FROM DILF_Limericks L 
	LEFT JOIN DILF_RFAs R ON R.OriginalId=L.OriginalId, 
	DILF_Words W
	WHERE W.VerseId=L.VerseId AND W.Sequence=0 AND W.IsPhraseWord=0 AND 
		(L.State='tentative' OR L.State='revised') $ReduceList 
	GROUP BY OriginalId ORDER BY Count DESC, L.OriginalId");
    $NumMessages = DbQueryRows($result);
    $start = $_GET['Start']+0;
    if ($start<0) $start=0;
    $pagesize = 30;
	$pages = new LimPaginator(array('Action=RFATable', 'Who='.$WhoAuthorId), $NumMessages, $pagesize, $start);

    $WordsDisplay .= sprintf("<h3>RFAs%s%s ".$pages->CurrRange()."</h3>", ($WhoName ? " for " : ""), htmlspecialchars($WhoName, ENT_QUOTES));
//    $WordsDisplay .= '<p>'.LinkSelf('Action=Activity', 'Show Activity Table').'</p>';
    if ($member->CanAdministrate()) {
      if ($WhoAuthorId)
          $WordsDisplay .= '<p>'.LinkSelf('Action=RFATable', 'Show All Limericks').'</p>';

      $WordsDisplay .= sprintf('<p><form action="%s" method="get">', PHPSELF);
      $WordsDisplay .= '<p>Show RFAs for Author: <input type=text name="Who">';
      $WordsDisplay .= ' <INPUT TYPE=HIDDEN name="Action" value="RFATable">';
      $WordsDisplay .= ' '.SubmitButton("button", "Go").'</p>';
      $WordsDisplay .= '</form></p>';
      $WordsDisplay .= '<p>';
    }
    else if ($member->CanWorkshop()) {
      if ($WhoAuthorId)
        $WordsDisplay .= '<p>'.LinkSelf('Action=RFATable', 'Show All Limericks').'</p>';
      else
        $WordsDisplay .= '<p>'.LinkSelf(array('Action=RFATable', 'Who='.$member->GetMemberId()), 'Show My Limericks').'</p>';
    }
    if ($NumMessages>0) {
      DbQuerySeek($result, $start);
      if ($NumMessages>$pagesize) {
        $WordsDisplay .= $pages->FormatNeatMultiplePageLinks();
	  }
    }
    $WordsDisplay .= '<table class="widetable">';
    if ($member->CanWorkshop()) $SelfRFAHeading = '<th>Self RFA</th>';
    else $SelfRFAHeading = '';
    if (DbQueryRows($result)==0) $WordsDisplay .= '<tr><td>No RFAs</td></tr>';
    else $WordsDisplay .= '<tr><th>Time</th><th>Limerick</th><th>Author</th><th>#RFAs</th>'.$SelfRFAHeading.'<th>My RFA</th><th>Word</th><th>First Line</th><th>Last WS</th></tr>';
    $rows = 0;
    while (($line = DbFetchArray($result)) AND ($rows<$pagesize)) {
      $WordsDisplay .= '<tr>';
      $WordsDisplay .= FormatTableCell(FormatDateTime($line['DateTime']), 'lightpanel');
      $WordsDisplay .= FormatTableCell('#T'.$line['OriginalId'], 'lightpanel');
      $WordsDisplay .= FormatTableCell(GetAuthorHtml($line['AuthorId']), 'lightpanel');
      $WordsDisplay .= FormatTableCell($line['Count'], 'lightpanel');
      if ($member->CanWorkshop()) $WordsDisplay .= FormatTableCell($line['SelfRFA'], 'lightpanel');
      $WordsDisplay .= FormatTableCell($line['MyRFA'], 'lightpanel');
      $WordsDisplay .= FormatTableCell($line['WordText'], 'lightpanel');
      list($text) = explode("\n", $line['Verse']);
      $WordsDisplay .= FormatTableCell(LinkSelf('Workshop='.$line['OriginalId'], $text), 'lightpanel');

      // Show the date in bold if it has been more than 7 days since the last workshop activity 
      preg_match ("@([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})@", $line['UpdateDateTime'], $regs);
      $elapsed = time() - mktime($regs[4], $regs[5], $regs[6], $regs[2], $regs[3], $regs[1]);
      if ($elapsed>7*24*60*60)
        $WordsDisplay .= FormatTableCell('<b>'.FormatDateTime($line['UpdateDateTime']).'</b>', 'lightpanel');
      else
        $WordsDisplay .= FormatTableCell(FormatDateTime($line['UpdateDateTime']), 'lightpanel');
      $WordsDisplay .= '</tr>';
      $rows++;
    }
    $WordsDisplay .= '</table>';
    DbEndQuery($result);

    $WordsDisplay .= $pages->FormatPaginationMenu();
  }
  else
    $WordsDisplay .= sprintf('<p>You must be logged in before you can view RFAs.</p>');

  return $WordsDisplay;
}

function ProcessSendMessage() {
  global $member;
  $WordsDisplay = '';

  if (($member->CanContribute()) and ($member->Record('ApprovedCount')>=1)) {
    $dt = LimTimeConverter::FormatGMDateFromNow();
    $workshoptext = stripslashes(trim(CleanupHtml($_POST['sendmsgtext']), "\n\r "));
    InsertNewMessage($member->GetMemberId(), $_GET[ProcessSendMsg], 0, $line['PrimaryAuthorId'], $dt, 'Text', $workshoptext);

    $WordsDisplay .= '<p>Message sent to '.GetAuthorHtml($_GET[ProcessSendMsg]).'.</p>';
    AuditLog($VerseId, 'Sent message to '.GetAuthor($_GET[ProcessSendMsg]));

    $WordsDisplay .=LinkSelf('', 'Home');
  }
  else
    $WordsDisplay .= sprintf('<p>You do not have sufficient privilege to send messages.</p>');

  return $WordsDisplay;
}

function FormatSendMessage() {
  global $member;
  $WordsDisplay = '';

  if ($member->CanChat()) {
    $result = DbQuery(sprintf("SELECT ApprovedCount FROM DILF_Authors WHERE AuthorId=%d", $_GET[SendMsg]));
    if ($line = DbFetchArray($result))
      $NoReply = ($line['ApprovedCount']<1);
    else $NoReply = FALSE;
    DbEndQuery($result);

    $WordsDisplay .= sprintf('<form action="%s?ProcessSendMsg=%s&amp;Popup=1" method="post" name="SendMsg">', PHPSELF, $_GET[SendMsg]);
    $WordsDisplay .= '<table class="widetable">';
    $WordsDisplay .= '<tr><th>Message to</th><th>Text</th></tr>';
    $WordsDisplay .= '<tr>';
    $WordsDisplay .= FormatTableCell(GetAuthorHtml($_GET[SendMsg]), 'darkpanel');
    $WordsDisplay .= FormatTableCell('<textarea name="sendmsgtext" cols=60 rows=5></textarea>', 'lightpanel');
    $WordsDisplay .= '</tr>';
    $WordsDisplay .= '<tr><td colspan=2 class="lightpanel">';
    if ($NoReply) $WordsDisplay .= '<p><b>Warning: The recipient of this message does not yet have one approved limerick and will not be able to reply.</b></p>';
    $WordsDisplay .= SubmitButton("SendMsgButton", "Send Message");
    $WordsDisplay .= '<span class="hinttext"> (Use these transient messages for gaining attention, not for passing messages of lasting value.)</span>';
    $WordsDisplay .= '</td></tr>';
    $WordsDisplay .= '</table>';
    $WordsDisplay .= '</form>';
  }
  else
    $WordsDisplay .= sprintf('<p>You do not have sufficient privilege to send messages.</p>');

  return $WordsDisplay;
}

function GetSentMessages() {
  // check for any special messages and return them, otherwise return nothing
  global $member;
  $WordsDisplay = '';

  $result = DbQuery(sprintf("SELECT * FROM DILF_Messages WHERE DstId=%d AND MsgType='Text' ORDER BY DateTime", $member->GetMemberId()));
  if (DbQueryRows($result)>0) {
    $WordsDisplay .= '<table class="widetable">';
    $WordsDisplay .= '<tr><th colspan=4>Special Messages. Please read and delete.</th></tr>';
    $WordsDisplay .= '<tr><th>From</th><th>Time</th><th>Text</th><th>Delete</th></tr>';
    while ($line = DbFetchArray($result)) {
      $WordsDisplay .= '<tr>';
      $SenderName = GetAuthorHtml($line['SrcId']);
      if ($member->CanChat())
        $ReplyLink = " (".LinkPopup('SendMsg='.$line['SrcId'], reply, "Reply to ".$SenderName).")";
      else $ReplyLink = "";

      $WordsDisplay .= FormatTableCell($SenderName.$ReplyLink, 'lightpanel');
      $WordsDisplay .= FormatTableCell(FormatDateTime($line['DateTime']), 'lightpanel');
      $WordsDisplay .= '<td>'.AddBreaks($line['MsgData']).'</td>';
      $WordsDisplay .= FormatTableCell(LinkPopup('DelSendMsg='.$line['MsgId'], '', 'Delete this message', '<img src="RedX.gif">'), 'lightpanel') ;
      $WordsDisplay .= '</tr>';
    }
    $WordsDisplay .= '</table>';
  }
  DbEndQuery($result);

  return $WordsDisplay;
}

function DeleteSendMessage() {
  global $member;
  $WordsDisplay = '';
  if ($member->CanContribute()) {
    $MsgNum = $_GET['DelSendMsg'];
    AuditLog(0, 'Deleted text message: MessageId='.$MsgNum.' AuthorId='.$member->GetMemberId());
    $result = DbQuery(sprintf("DELETE FROM DILF_Messages WHERE MsgId=%d AND MsgType='Text' AND DstId=%d", $MsgNum, $member->GetMemberId()));
    if (DbAffectedRows()==0)
      $WordsDisplay .= '<p>Message not found. It could be that you had already deleted this message but it was still displayed since you hadn\'t changed pages.</p>';
    else $WordsDisplay .= '<p>Message deleted.</p><p>When you close this window, the message will still be displayed in your original browser window. Just ignore it. It will disappear the next time you change pages.</p>';
    //SetRedirect('');
  }
  else
    $WordsDisplay .= sprintf('<p>You must be logged in before you can delete messages.</p>');
  return $WordsDisplay;
}

function FormatConfirmingTable()
{
	global $member;
	$html = new LimHtml();
	$html->Paragraph("These limericks have been scheduled for approval. Unless the timeline is interrupted, 
		a LACE will be sent out to the author at the end of the indicated delay.
		If you feel a piece may not yet be ready for final approval, click on the limerick number link
		to enter the workshop to see if your concerns have already been addressed before doing anything else.
		If they haven't, hit the \"Stop Countdown\" button and express your concerns in a WSing comment.
		This will stop the clock on this particular limerick, returning it to Tentative and removing the
		author's self-RFA.");
	
	$start = $_GET['Start']+0;
	$textCellFormatter = new LimCellFormatterText();
	$sqlTable = new LimSqlTableFormatter(
		"SELECT L.StateDateTime 'End Delay', L.OriginalId 'Limerick', 
			W.WordText 'Word', A.Name 'Author', B.Name 'Approving Editor' 
		FROM DILF_Limericks L, DILF_Authors A, DILF_Authors B, DILF_Words W 
		WHERE L.State='confirming' AND L.PrimaryAuthorId=A.AuthorId AND L.ApprovingEditor=B.AuthorId AND 
			W.VerseId=L.VerseId AND W.Sequence=0 AND W.IsPhraseWord=0 AND LinkCode=0 
		ORDER BY L.StateDateTime",
		array('End Delay' => new LimCellFormatterTimeDelay($member->Record("TimeZone"), LACE_TIME),
			'Limerick' => new LimCellFormatterTLink(),
			'Word' => $textCellFormatter,
			'Author' => $textCellFormatter, 
			'Approving Editor' => $textCellFormatter),
		$start, 100, 'Action=ConfirmingTable');
	$sqlTable->HeadingText = 'Confirming Limericks';
	$sqlTable->FormatTable($html);
		
	return $html->FormattedHtml();
}
?>
