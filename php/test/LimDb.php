<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimDb extends mysqli
{
    protected static $lastConnection;

    public function __construct($server, $user, $password, $database)
    {
    	LimGeneral::Log("Try to connect to $database", LimGeneral::DEBUGLOG_DB);
        parent::__construct($server, $user, $password, $database);
        if (mysqli_connect_error())
        {
            LimGeneral::DieMessage("We could be having some server problems. Try again in 10 minutes.",
                "Connect Error (" . mysqli_connect_errno() . ") " . mysqli_connect_error());
        }
        else self::$lastConnection = $this;
    	LimGeneral::Log("Connected successfully", LimGeneral::DEBUGLOG_DB);
    }

    public static function GetDb()
    {
        return self::$lastConnection;
    }

    public function query($query)
    {
        LimGeneral::Log($query, LimGeneral::DEBUGLOG_DB);
        $started = LimGeneral::GetMicroTime();

        $result = parent::query($query);
        if ($result===false) LimGeneral::DieMessage("", "Query failed : " . $this->error);
        LimGeneral::Log(sprintf("Query took %.2fms", LimGeneral::MsElapsed($started)), LimGeneral::DEBUGLOG_DB);
        
        return $result;
    }

    public function ReadSingleRecord($selectQuery)
    {
        $value = false;
        $result = $this->query($selectQuery);
        if ($result)
        {
            if ($result->num_rows>0)
                $value = $result->fetch_assoc();
            $result->free();
        }
        return $value;
    }

    public function InsertRecord($tableName, $record)
    {
    	$fields = implode(",", array_keys($record));
        $values = implode("','", $record);
        return $this->query("INSERT INTO $tableName ($fields) VALUES ('$values')");
    }

    public function DeleteRecords($tableName, $condition, $count=0)
    {
        $limit = (($count>0) ? "LIMIT $count" : '');
        return $this->query("DELETE FROM $tableName WHERE ($condition) $limit");
    }

    public function QuoteString($text)
    {
        return "'".$this->real_escape_string($text)."'";
    }
}
?>
