<?php
LimGeneral::Initialize();  // sets up start time

// define an autoloader
$pathBits = explode('/', str_replace('\\', '/', dirname(__FILE__)));
$parentDir = $pathBits[count($pathBits)-1];
define('PHP_LIM_PATH', "../$parentDir");

function __autoload($class_name)
{
    require_once PHP_LIM_PATH.'/'.$class_name.'.php';
}

define('ONE_MINUTE', 60);
define('ONE_HOUR', 3600);
define('ONE_DAY', 24*ONE_HOUR);
define('ONE_WEEK', 7*ONE_DAY);
define('ONE_MONTH', 30*ONE_DAY);
define('ONE_YEAR', 365*ONE_DAY);


class LimGeneral
{
    protected static $startTime = false;

    // This Initialize method is run very early in the script.
    // Don't make too many assumptions about what is loaded - i.e. no debug
    public static function Initialize()
    {
        if (self::$startTime===false)
            self::$startTime = microtime(true);
    }

    public static function MsElapsed($since=0)
    {
    	if (!$since) $since = self::$startTime;
        return (microtime(true) - $since)*1000;
    }

    public static function GetMicroTime()
    {
        return microtime(true);
    }

    const DEBUGLOG_DB = 1;
    const DEBUGLOG_DUMPS = 2;
    const DEBUGLOG_COOKIES = 4;

    const DEBUGLOG_ALL = 65535;

    protected static $logOpened=false;
    protected static $debugMask=0;

    // compress any group of CR, LF, TAB to a single space
    public static function CompressTabsAndFeeds($text)
    {
        return preg_replace("/[\n\r\t]+/", ' ', $text);
    }

    public static function Log($message, $filter=self::DEBUGLOG_ALL)
    {
        if ((self::$debugMask & $filter)==0)
            return;

        if (!self::$logOpened)
        {
            openlog('Lim', 0, LOG_USER);
            self::$logOpened = true;
        }
        syslog(LOG_DEBUG, sprintf("%.2f %s", self::MsElapsed(), self::CompressTabsAndFeeds($message)));
    }

    public static function VarDump($var, $title='')
    {
        self::Log($title." ".var_export($var, true), LimGeneral::DEBUGLOG_DUMPS);
    }

    public static function SetDebugMask($maskValue)
    {
        self::$debugMask = $maskValue;
    }

    public static function GetDebugMask()
    {
        return self::$debugMask;
    }

    public static function DieMessage($message, $details)
    {
        echo "<html>";
        echo "<h3>Apologies</h3>";
        echo "<div style='font-weight: bold; color:#C44;'>";
        echo $message;
        echo "</div>";
        echo "<div style='color:#000;'>";
        echo $details;
        echo "</div>";
        echo "</html>";

        self::Log("Dying ".$message);
        self::Log("      ".$details);
        exit;
    }

}

?>
