<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function TweetLimerick($VerseId) {
  AuditLog($VerseId, 'Tweet Limerick');
  #fopen("/var/www/db/logs/tweetlim.log", "a").write("TweetLimerick: Attempting to tweet {$VerseId}\n");
  $NewVerseId = GetApprovedVerseId("A{$VerseId}");
  if ($NewVerseId == 0) {
    AuditLog($VerseId, "TweetLimerick: Could not get the approved number for {$VerseId}");
    #fopen("/var/www/db/logs/tweetlim.log", "a").write("TweetLimerick: Could not get the approved number for {$VerseId}\n");
    return;
  }
  $line = GetVerse($NewVerseId);
  $author = GetAuthor($line['PrimaryAuthorId']);
  $twitter_author = GetAuthor($line['PrimaryAuthorId'], TRUE);
  if ($line['SecondaryAuthorId']) {
    $author = $author . ' and ' . GetAuthor($line['SecondaryAuthorId']);
    $twitter_author = $twitter_author . ' and ' . GetAuthor($line['SecondaryAuthorId'], TRUE);
  }
  $limId = $line['LimerickNumber'];
  $verse = $line['Verse'];
  require_once('LimFormat.php');
  $wordlist = GetWordList($NewVerseId, false);

  AuditLog($VerseId, "TweetLimerick: About to call python");

  $return = exec('/usr/local/bin/python' . ' ' .
       '/usr/local/share/oedilf/bin/tweetlim.py' . ' ' .
       escapeshellarg($author) . ' ' .
       $limId . ' ' .
       $NewVerseId . ' ' .
       escapeshellarg($wordlist) . ' ' .
       escapeshellarg($verse) . ' ' .
       escapeshellarg($twitter_author) 
       #. ' 2>&1 >>/var/www/db/logs/tweetlim.log'
      );
  AuditLog($VerseId, "TweetLimerick: python returned " . print_r($return, True));
}

function GetApprovedVerseId($VerseId) {
  global $CurtainFilter;


  if ($VerseId{0}=='A') {
    if ($VerseId{1}=='A') {
      $VerseId = substr($VerseId, 1);
    }
    $result = DbQuery(sprintf('SELECT * FROM DILF_Limericks WHERE LimerickNumber=%d AND State="approved" '.$CurtainFilter, 0+substr($VerseId, 1)));
  } else {
    $result = DbQuery(sprintf('SELECT * FROM DILF_Limericks WHERE OriginalId=%d AND (State="tentative" OR State="confirming" OR State="approved") '.$CurtainFilter, $VerseId));
  }
  if ($line = DbFetchArray($result)) {
    $approvedVerseId = $line['VerseId'];
    DbEndQuery($result);
    return $approvedVerseId;
  } else {
    DbEndQuery($result);
    header('Location: /db/oedilfi.jpg');
    return 0;
  }

}

function GenerateLimerickImage($VerseId) {
  fopen("/var/www/db/logs/tweetlim.log", "a").write("GenerateLimerickImage: Attempting to generate {$VerseId}\n");
  $NewVerseId = GetApprovedVerseId($VerseId);
  if ($NewVerseId == 0) {
    fopen("/var/www/db/logs/tweetlim.log", "a").write("GenerateLimerickImage: Could not get the approved number for {$VerseId}\n");
    return;
  }
  $VerseId = $NewVerseId;
  $line = GetVerse($VerseId);
  $author = GetAuthor($line['PrimaryAuthorId']);
  if ($line['SecondaryAuthorId'])
    $author = $author . ' and ' . GetAuthor($line['SecondaryAuthorId']);
  $limId = $line['LimerickNumber'];
  $verse = $line['Verse'];
  require_once('LimFormat.php');
  $wordlist = GetWordList($VerseId, false);


  $imagefile = exec('/usr/local/bin/python' . ' ' .
       '/usr/local/share/oedilf/bin/limimage.py' . ' ' .
       escapeshellarg($author) . ' ' .
       escapeshellarg($limId) . ' ' .
       escapeshellarg($VerseId) . ' ' .
       escapeshellarg($wordlist) . ' ' .
       escapeshellarg($verse)
      );
  header("Content-Disposition: inline; filename=" . basename($imagefile));
  return $imagefile;
}

function FacebookLimerick($VerseId) {
  AuditLog($VerseId, 'FacebookLimerick');
  #fopen("/var/www/db/logs/tweetlim.log", "a").write("FacebookLimerick: Attempting to post {$VerseId}\n");
  #$NewVerseId = GetApprovedVerseId($VerseId);
  $NewVerseId = GetApprovedVerseId("A{$VerseId}");
  if ($NewVerseId == 0) {
    AuditLog($VerseId, "FacebookLimerick: Could not get the approved number for {$VerseId}");
    #fopen("/var/www/db/logs/tweetlim.log", "a").write("FacebookLimerick: Could not get the approved number for {$VerseId}\n");
    return;
  }
  $line = GetVerse($NewVerseId);
  $author = GetAuthor($line['PrimaryAuthorId']);
  if ($line['SecondaryAuthorId'])
    $author = $author . ' and ' . GetAuthor($line['SecondaryAuthorId']);
  $limId = $line['LimerickNumber'];
  $verse = strip_tags($line['Verse']);
  $verse = preg_replace('/\[\[[\#AT0-9]*?:(.*?)\]\]/', '$1', $verse);
  require_once('LimFormat.php');
  $wordlist = GetWordList($NewVerseId, false);

  #$page_access_token = "EAACEdEose0cBAPwTAyHGeZATZBSCD4u5GZCut2ZCGqZCGTBPmVZCg3UGviQrsq7FTdYMNGSHblZBc9yy8tEvzQmeH8EdLB13ExLbbqpWGhZCnYwwO33FvGb6puaaqCNmeluu6YcdnqByrNs8Y6UzljZACoVPcGr1F0pY5fDalms5WVQtF3qvXnFm1C1ZA0pSLdE4gKgiBVFzuJegZDZD";
  $page_access_token = "EAALgUZBZBJc2UBABrK7ZCTBshhStKjf8cyQ518ZBSZArrRX4of5FNPZBZCZC5FpkmZCeOc0l63kvTTAZCTVAXhZBKckwPskyVksVJ25tT749Dx4tzflgjMbZBN7LwO1d2F7X7BmLwG3lscwC1hlKkZChJ9TiPRmKdW7Y3IeEvoZAQWUYctp9nMS5jmOrfnuVJgTsXKLNELXNVYvkN1F0YuChKtIIZCZC";
  $page_id = "1463472420595964";

  # the lighthouse looks better on facebook
  #$data['picture'] = "https://www.oedilf.com/db/Lim.php?LimImage=A{$limId}";
  $data['picture'] = "https://www.oedilf.com/db/oedilfi.jpg";
  $data['link'] = "https://www.oedilf.com/db/Lim.php?VerseId=A{$limId}";
  $data['message'] = "Limerick #{$limId} on {$wordlist} by {$author}:\n${verse}";
  if ($line['AuthorNotes'])
    $data['message'] .= "\n\n" . strip_tags($line['AuthorNotes']);
  if ($line['EditorNotes'])
    $data['message'] .= "\n\nEditor's Note:\n\n" . strip_tags($line['EditorNotes']);
  #$data['caption'] = "Limerick #{$limId} on {$wordlist} by {$author}";
  $data['caption'] = "https://oedilf.com/db/Lim.php?VerseId=A${limId}";
  $data['description'] = "The Omnificent English Dictionary In Limerick Form\n\nLimerick #{$limId} on {$wordlist} by {$author}";
  $data['access_token'] = $page_access_token;
  $post_url = "https://graph.facebook.com/".$page_id."/feed";

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $post_url);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  AuditLog($VerseId, "FacebookLimerick: About to do curl_exec");
  $return = curl_exec($ch);
  curl_close($ch);
  AuditLog($VerseId, "FacebookLimerick: curl_exec returned " . print_r($return, True));
  return $return;
}


?>
