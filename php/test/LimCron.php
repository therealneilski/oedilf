<?php
// regular background tasks

function __autoload($class_name) {
    require_once $class_name . '.php';
}

define('IN_LIM', true);
define('PHPSELF', $_SERVER['PHP_SELF']);
define('THIS_APP', $_SERVER['HTTP_HOST'].PHPSELF);

var_dump($argv);

if (count($argv)<2)
  die("This script must be run from the command line.");

require "LimDbFunctions.php";
require "LimWorkshop.php";
require "LimFormat.php";
require "LimMember.php";
require "LimMsg.php";
require "LimStats.php";
require "LimSearch.php";
require "LimEdit.php";
require "LimUtilities.php";

LimDbConnect($dblink);
$_SESSION['memberid'] = 1; // pretend to be Virge.

if ($argv[1]=="CheckForLACEs") {
	echo "Doing LACE Check\n";
	
	
	echo LACE_TIME;
	echo "CheckForLACEs(LACE_TIME, TRUE, $Count);";
}

function UpdateWordEntry($id, $word)
{
	$limWord = new LimWord($word);
	
	$searchWord = DbEscapeString($limWord->SearchText());
	$soundWord = DbEscapeString($limWord->Sound());
	$sortOrder = DbEscapeString($limWord->SortOrder());
    $result = DbQuery("UPDATE DILF_Words SET
		SearchText='$searchWord', Sound='$soundWord', SortOrder='$sortOrder'
		WHERE WordId=$id LIMIT 1");
}

if ($argv[1]=="SetSearchTexts") {
    $i=1;
    $result = DbQuery("SELECT * FROM DILF_Words WHERE 1");
    while ($line = DbFetchArray($result)) 
	{
		$id = $line['WordId'];

		$word = $line['WordText'];
		UpdateWordEntry($id, $word);
        if (($i % 100)==0)
        {
            print( $i."\t".$line['WordText']."\n");
        }
        $i++;
			
		$doingPhraseWords = false;
		
		$subWords = explode(' ', $word);
		if ($doingPhraseWords and (count($subWords)>1))
		{
			foreach($subWords as $index=>$subWord)
			{
				$word = trim($subWord, ", \t\r\n");
				if ($word)
				{
					print("[$subWord] ");
				}
			}
    		echo "\n";
		}
    }
    DbEndQuery($result);
}

if ($argv[1]=="ResetLimerickWords")
{
    $result = DbQuery("SELECT * FROM DILF_Limericks WHERE State<>'obsolete'");
    while ($line = DbFetchArray($result)) 
	{
		$verseId = $line['VerseId'];
		$wordList = new LimWordList($verseId);
		
		$html = new LimHtml();
		$wordList->FormatWordList($html, "; ");
		print($verseId." ".$html->FormattedHtml()."\n");
		DbQuery("DELETE FROM DILF_Words WHERE VerseId=$verseId");
		AddLimerickWords($verseId, $wordList->WordList());
	}
}

if ($argv[1]=="Test") {

}

if ($argv[1]=="UpdateAuthorStats") {
	$start = 1;
	$stop = 1;
	$result = DbQuery("SELECT MAX(AuthorId) maxAuthor FROM DILF_Authors WHERE 1");
	if ($line = DbFetchArray($result))
		$stop = $line['maxAuthor']+5;
	DbEndQuery($result);	
	  
    for ($i=$start; $i<=$stop; $i++) {
      if ($i>0 && GetAuthor($i)) {
        UpdateAuthorStats($i);
        print("Updated Author $i\n");
      }
    }
}

LimDbDisconnect($dblink);

echo "\n";
?>