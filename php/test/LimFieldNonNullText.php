<?php
class LimFieldNonNullText extends LimFieldText
{
	protected $nullIsError;
	
	public function __construct($fieldName, $screenLabel, $nullIsError=false)
	{
		$this->nullIsError = $nullIsError;
		parent::__construct($fieldName, $screenLabel);
	} 

	public function InvalidEntryMessage($postValues)
	{
		if (($this->nullIsError) and
			(!isset($postValues[$this->dbFieldName]) or 
			(strlen($postValues[$this->dbFieldName])==0)))
			return "This field must not be left empty.";
		else return "";
	}
	
	public function HasValue($postValues)
	{
		if ($this->nullIsError) 
			return true;
		else
			return (isset($postValues[$this->dbFieldName]) and 
				(strlen($postValues[$this->dbFieldName])>0));
	}
}
?>