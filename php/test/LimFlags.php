<?php
class LimFlags
{
	protected $memberRecord;
    protected $countryNames = array();
    protected $countryFlags = array();

	public function __construct($memberRecord)
	{
		$this->memberRecord = $memberRecord;
        $list = array();
        if ($this->memberRecord['BornIn'])
            $list[] = "'".$this->memberRecord['BornIn']."'";
        if ($this->memberRecord['LivingIn'] and
            ($this->memberRecord['LivingIn']!=$this->memberRecord['BornIn']))
            $list[] = "'".$this->memberRecord['LivingIn']."'";
        if (COUNT($list)>0)
        {
            $result = DbQuery("SELECT * FROM DILF_Countries
                WHERE Code IN (".implode(",",$list).")");
            while ($info = DbFetchArray($result))
            {
                $this->countryNames[$info['Code']] = $info['Name'];
                $this->countryFlags[$info['Code']] = $info['Flag'];
            }
            DbEndQuery($result);
        }
	}

    protected function CountryNameFlag($html, $code)
    {
        if ($this->countryNames[$code])
            $html->Text($this->countryNames[$code]." ");
        if ($this->countryFlags[$code])
            $html->Image("flags/".$this->countryFlags[$code], $this->countryNames[$code]);
    }

    public function CountryDescription($html)
    {
        if ($this->memberRecord['BornIn'] and
            ($this->memberRecord['LivingIn']==$this->memberRecord['BornIn']))
        {
            $html->Text("Born and living in: ");
            $this->CountryNameFlag($html, $this->memberRecord['BornIn']);
        }
        else
        {
            if ($this->memberRecord['BornIn'])
            {
                $html->Text("Born in: ");
                $this->CountryNameFlag($html, $this->memberRecord['BornIn']);
            }
            if ($this->memberRecord['LivingIn'])
            {
                $html->Text(" Living in: ");
                $this->CountryNameFlag($html, $this->memberRecord['LivingIn']);
            }
        }
    }
}

?>
