<?php
// Assumes source will contain complete tags (of the specified types), but not necessarily paired tags.
// Fix closes any tags left open.
class LimTagMatcher
{
	protected $tags;
	protected $debug = false;
	
	function __construct($tagArray, $debugEnable=false)
	{
		$this->debug = $debugEnable;
		$tagSorter = array();
		foreach ($tagArray as $tag)
			$tagSorter[$tag] = strlen($tag);
		arsort($tagSorter);		
		$this->tags = array_keys($tagSorter);
		if ($this->debug) foreach ($this->tags as $tag) printf("Tag: %s<br>", htmlentities($tag));
	}
	
	function Debug($enable) 
	{
		$this->debug = $enable;
	}
	
	function Fix($string) 
	{
		if ($this->debug) printf("Input: %s<br>", htmlentities($string));
		$out = '';
		$stack = array();
		$loc = 0;
		$lower = strtolower($string);
		while (($startTag = strpos($string, "<", $loc))!==false)
		{
			$out .= substr($string, $loc, $startTag - $loc);
			if ($this->debug) printf("Outed: %s<br>", htmlentities($out));
			$loc = $startTag+1;
			
			$currentTag = '';
			$close = false;
			if ($string{$loc}=="/")
			{
				$loc++;
				$close = true;
			}
			foreach ($this->tags as $tag) 
			{
				if ($tag == substr($lower, $loc, strlen($tag)))
				{
					$currentTag = $tag;
					break;
				}
			}
			if (!$currentTag) // treat this as pure text
			{
				$out .= "<";
				if ($this->debug) printf("Outed: %s<br>", htmlentities($out));
			}
			else 
			{
				$endTag = strpos($string, ">", $loc);
				if ($endTag===false)
					$endTag = strlen($string);
				
				if ($close)
				{
					// search the stack for a matching tag. If we find one, we'll empty the stack to that point. If not, we'll just discard this close tag.
					for ($i=count($stack)-1; $i>=0; $i--)
					{
						if ($currentTag==$stack[$i]) break;
					}
					if ($i>=0) // close up unclosed tags
					{
						while ((($oldTag = array_pop($stack)) != $currentTag) and $oldTag!=null)
						{
							$out .= "</".$oldTag.">";
							if ($this->debug) printf("Added: %s<br>", htmlentities($out));
						}
						$out .= substr($string, $startTag, $endTag - $startTag).">";
						if ($this->debug) printf("Outed: %s<br>", htmlentities($out));
					}
					else if ($this->debug) 
						printf("Dropped: %s<br>", 
							htmlentities(substr($string, $startTag, $endTag - $startTag)));
				}
				else
				{
					//if (!in_array($currentTag, $stack))
					//{
						array_push($stack, $currentTag);
						$out .= substr($string, $startTag, $endTag - $startTag).">";
						if ($this->debug) printf("Outed: %s<br>", htmlentities($out));
					//}
					//else if ($this->debug)
					//	printf("Dropped: %s<br>", htmlentities(substr($string, $startTag, $endTag - $startTag)));
				}
				$loc =$endTag+1;
			}
		}
		if ($loc<strlen($string)) 
		{
			$out .= substr($string, $loc);
			if ($this->debug) printf("Outed: %s<br>", htmlentities($out));
		}
		while ($oldTag = array_pop($stack))
		{
			$out .= "</".$oldTag.">";
			if ($this->debug) printf("Added: %s<br>", htmlentities($out));
		}
		return $out;
	}	
}


?>