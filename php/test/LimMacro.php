<?php
 
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimMacro {
	protected $targetTextArea, $finds=array(), $replaces=array();
	protected $buttonBlobs = array();
	
	public function LimMacro($textArea) {
		$this->targetTextArea = $textArea;
	}
	
	protected function jsaddslashes($s)
	{
	 $o="";
	 $l=strlen($s);
	 for($i=0;$i<$l;$i++)
	 {
	  $c=$s[$i];
	  switch($c)
	  {
	   case '<': $o.='\\x3C'; break;
	   case '>': $o.='\\x3E'; break;
	   case '\'': $o.='\\\''; break;
	   case '\\': $o.='\\\\'; break;
	   case '"':  $o.='\\x22'; break;
	   case "\n": $o.='\\n'; break;
	   case "\r": $o.='\\r'; break;
	   default:
	   $o.=$c;
	  }
	 }
	 return $o;
	}
	
	protected function MakeParam($text)
	{
		return str_replace(array('&', '#', "'"), array('And','Hash',''), $text);
	}

	public function CreateCommentButton($text, $comment)
	{
		return sprintf('<a class="macro" href="javascript:document.getElementById(\'%s\').value += \'%s\';document.getElementById(\'%s\').focus();">%s</a>', 
			$this->targetTextArea, 
			$this->jsaddslashes($comment), 
			$this->targetTextArea, 
			str_replace(" ","&nbsp;",$text));
	}
	
	public function CreateQuoteLimerickButton($limerickText) {
		$limerickText = trim($limerickText, "\n\r ")."\n\r";  // remove multiple newlines from end
		return $this->CreateCommentButton("Quote Limerick in Comment", $limerickText);
	}

	public function CreateQuoteANButton($ANText) {
		$ANText = trim($ANText, "\n\r ")."\n\r";  // remove multiple newlines from end
		return $this->CreateCommentButton("Quote Author's Note in Comment", $ANText);
	}
	
	
	public function CreateMacroButton($buttonLabel, $authorId) {
		return sprintf('<a class="macro" href="javascript:macroButt(\'%s\',\'%s\',%s);">%s</a>', 
			$this->targetTextArea, 
			rawurlencode($this->MakeParam($buttonLabel)), $authorId,
			str_replace(" ", "&nbsp;", $buttonLabel));
	}
	
	public function SetSubstitutions($findTexts, $replaceTexts){
		$this->finds = $findTexts;
		$this->replaces = $replaceTexts;
	}

	public function ParseMacrosFrom($macroDocument){
		$this->buttonBlobs = array_merge($this->buttonBlobs, explode("@@Button=",	$macroDocument));
		sort($this->buttonBlobs);
	}

	public function FormatAjaxText($buttonName)
	{
		foreach($this->buttonBlobs as $ButtonDef) 
		{
			$Button = explode("@@", $ButtonDef, 2); // split the button name from the text
			if ($this->MakeParam($Button[0])==$buttonName)
				return trim($Button[1], "\n\r ");
		}
		return htmlspecialchars(" Macro button '$buttonName' not found. ");
	}
	
	public function TemplateSubstitute($buttonText)
	{
		return str_replace($this->finds, $this->replaces, $buttonText);
	} 
	
    public function FormatButtons($html) 
    {
		foreach($this->buttonBlobs as $ButtonDef) 
		{
			$Button = explode("@@", $ButtonDef, 2); // split the button name from the text
			$ButtonText = $this->TemplateSubstitute($Button[1]);
			if ($Button[1]) 
				$html->Text("\n ".
					$this->CreateCommentButton($Button[0], trim($ButtonText, "\n\r ")));
		}		
	}
	
	public function FormatAjaxButtons($html, $authorId) 
    {
		foreach($this->buttonBlobs as $ButtonDef) 
		{
			$Button = explode("@@", $ButtonDef, 2); // split the button name from the text
			if (isset($Button[1])) 
				$html->Text("\n ".$this->CreateMacroButton($Button[0], $authorId));
		}		
	}
	
}

?>
