<?php

class LimTokenTable
{
    const TokenTableName = 'DILF_Tokens';
    const TokenCol = 'token';
    const ExpiryCol = 'expires';
    const HandlerCol = 'handler';
    const DescriptionCol = 'description';

    protected $db;

    public function __construct($databaseHandle)
    {
        $this->db = $databaseHandle;
    }

    public function AddDelayedValidationToken($tokenHandler, $expiryTimeStamp, $description)
    {
        $blob = serialize($tokenHandler);
        $tryCount = 0;

        do
        {
            $token = substr(md5($blob.rand()), 0, 10);
            $query = LimQueryBuilder::Select()
                ->From(self::TokenTableName)
                ->Where(self::TokenCol."=".$this->db->QuoteString($token))
                ->Limit(1);
            $record = $this->db->ReadSingleRecord($query->ToSQL());
            if ($record)
                $didInsert = false;
            else
                $didInsert = $this->db->InsertRecord(self::TokenTableName, array(
                    self::TokenCol =>$token,
                    self::ExpiryCol =>$expiryTimeStamp,
                    self::DescriptionCol =>$this->db->real_escape_string($description),
                    self::HandlerCol =>$this->db->real_escape_string($blob)));
            $tryCount++;
        } while ((!$didInsert) && ($tryCount<5));
        if (!$didInsert) $token = false;
        return $token;
    }

    public function GetDelayedValidationHandler($token)
    {
        $query = LimQueryBuilder::Select()
            ->From(self::TokenTableName)
            ->Where(self::TokenCol."=".$this->db->QuoteString($token))
            ->Limit(1);
        $record = $this->db->ReadSingleRecord($query->ToSQL());
        if ($record)
            return unserialize($record['handler']);
        else return null;
    }

    public function RemoveToken($token)
    {
        $query = LimQueryBuilder::Delete()
            ->From(self::TokenTableName)
            ->Where(self::TokenCol."=".$this->db->QuoteString($token))
            ->Limit(1);
        return $this->db->query($query->ToSQL());
    }

    public function PurgeTable()
    {
        $this->db->DeleteRecords(self::TokenTableName, self::ExpiryCol." < ".time());
    }
}


?>
