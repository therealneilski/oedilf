<?php
class LimFieldCheckbox extends LimField
{
    protected $label = '';

	public function __construct($fieldName, $screenLabel, $label)
	{
		parent::__construct($fieldName, $screenLabel);
		$this->label = $label;
	}

	public function GetFormHtml($defaultValue)
	{
		$text = "<input type='checkbox' name='$this->dbFieldName' ".($defaultValue ? "checked='checked' " : "")."id='id_$this->dbFieldName' />";
        if ($this->label) $text .= "<label for='id_$this->dbFieldName'>".htmlspecialchars($this->label, ENT_QUOTES)."</label>";
        return $text;
	}

	public function GetSqlUpdateValue($postValues)
	{
        // no sql provided as yet until we have a standard storage for a checkbox
		return "";
	}

}
?>
