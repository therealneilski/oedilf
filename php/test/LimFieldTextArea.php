<?php
class LimFieldTextArea extends LimField
{
	protected $cols;
	protected $rows;
	
	public function __construct($fieldName, $screenLabel, $rows, $cols) 
	{
		parent::__construct($fieldName, $screenLabel);
		$this->rows = $rows;
		$this->cols = $cols;
	}

	public function GetFormHtml($defaultValue)
	{
		return "<textarea name='$this->dbFieldName' rows='$this->rows' cols='$this->cols'>".
			htmlspecialchars($defaultValue, ENT_QUOTES)."</textarea>";
	}
}
?>