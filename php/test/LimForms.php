<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function AnnihilateLimerickVersions($OriginalId) {
  $WordsDisplay = '';

  $affectedAuthors = new LimStatsUpdater();
  $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE OriginalId=%d ", $OriginalId));
  if (DbQueryRows($result)>0) {
    while ($line = DbFetchArray($result)) {
	  $affectedAuthors->Remember($line['PrimaryAuthorId'], $line['SecondayAuthorId']);
      $WordsDisplay .= AnnihilateSingleLimerick($line['VerseId'], $OriginalId, $line['Version'], false);
    }
  }
  else {
    $WordsDisplay .= '<p>Limerick not found.</p>';
  }
  DbEndQuery($result);
  
  $affectedAuthors->Update();
  return $WordsDisplay;
}

function AnnihilateSingleLimerick($VerseId, $OriginalId, $Revision, $UpdateStats=true) {
  $WordsDisplay = '';
  
  $line = GetVerse($VerseId);
  
  $result = DbQuery(sprintf("DELETE FROM DILF_Limericks WHERE VerseId=%d AND OriginalId=%d AND Version=%d LIMIT 1",
    $VerseId, $OriginalId, $Revision));

  $result = DbQuery(sprintf("DELETE FROM DILF_Workshop WHERE VerseId=%d", $VerseId));
  $wscomments = DbAffectedRows();

  // remove words that referenced it
  $result = DbQuery(sprintf("DELETE FROM DILF_Words WHERE VerseId=%d", $VerseId));
  $words = DbAffectedRows();

  // remove notification messages that referenced it
  $result = DbQuery(sprintf("DELETE FROM DILF_Messages WHERE VerseId=%d", $VerseId));
  $notifications = DbAffectedRows();

  // remove it from topics
  $result = DbQuery(sprintf("DELETE FROM DILF_TopicLims WHERE OriginalId=%d", $OriginalId));
  $topics = DbAffectedRows();


  $WordsDisplay .= sprintf('<p>Limerick #%d (revision %d) Annihilated<br>', $OriginalId, $Revision);
  $WordsDisplay .= sprintf('Workshop comments removed: %d<br>', $wscomments);
  $WordsDisplay .= sprintf('Linked words removed: %d<br>', $words);
//  $WordsDisplay .= sprintf('Gradings removed: %d<br>', $gradings);
  $WordsDisplay .= sprintf('Notifications removed: %d<br>', $notifications);
  $WordsDisplay .= sprintf('Topic links removed: %d</p>', $topics);
  if ($UpdateStats)
  {
	  UpdateStats();
	  if ($line['State']!='obsolete') {
	    UpdateAuthorStats($line['PrimaryAuthorId']);
	    if ($line['SecondaryAuthorId'])
	      UpdateAuthorStats($line['SecondaryAuthorId']);
	  }
  }
  AuditLog($VerseId, 'Annihilate Limerick');

  return $WordsDisplay;
}

function SendPromotionInstructions($user, $templateDoc, $title)
{
    global $member;
    $Message = str_replace("@@AUTHOR@@", $user->GetMemberName(), GetDocument($templateDoc));

    if ($user->Record('MilestoneEmails')=='On' && $user->Record('UseEmail')=='On')
    {
        SendMailFromEiC($user->Record('Email'), $title, $Message);
        LimGeneral::Log("Sent PromoInstructions to ".$user->Record('Email')." : $title");
    }
    else
        InsertNewMessage($member->GetMemberId(), $user->GetMemberId(), 0, 0, LimTimeConverter::FormatGMDateFromNow(),
            'Text', $title."\n\n".$Message);
}

function AutoTitleSet($AuthorId, $ApprovedCount, $PrimaryCount) {
  $user = new LimMember($AuthorId);

  if ($ApprovedCount>=1000) {
    if (($user->Record('Rank')=='W750') or ($user->Record('Rank')=='W500'))
      PromoteAuthor($AuthorId, 'W1000', 'workshopper', $user->Record('UserType')) ;
  }
  else if ($ApprovedCount>=500) {
    if (($user->Record('Rank')=='W400') or ($user->Record('Rank')=='W300'))
      PromoteAuthor($AuthorId, 'W500', 'workshopper', $user->Record('UserType')) ;
  }
  else if ($ApprovedCount>=300) {
    if ($user->Record('Rank')=='W200')
      PromoteAuthor($AuthorId, 'W300', 'workshopper', $user->Record('UserType')) ;
  }
  else if ($ApprovedCount>=200) {
    if ($user->Record('Rank')=='W100')
      PromoteAuthor($AuthorId, 'W200', 'workshopper', $user->Record('UserType')) ;
  }
  else if ($ApprovedCount>=100) {
    if ($user->Record('Rank')=='W50')
      PromoteAuthor($AuthorId, 'W100', 'workshopper', $user->Record('UserType')) ;
  }
  else if ($ApprovedCount>=50) {
    if ($user->Record('Rank')=='WE')
      PromoteAuthor($AuthorId, 'W50', 'workshopper', $user->Record('UserType')) ;
  }
  else if (($ApprovedCount>=20) or ($PrimaryCount>=20)) { // now based on primary count
    if (($user->Record('Rank')=='C') or ($user->Record('Rank')=='CE')) {
      PromoteAuthor($AuthorId, 'WE', 'workshopper', $user->Record('UserType')) ;
      SendPromotionInstructions($user, 'WE Welcome Email', "Welcome New Workshopping Editor");
    }
  }
  else if ($ApprovedCount>=10) {
    if ($user->Record('Rank')=='C') {
      PromoteAuthor($AuthorId, 'CE', 'member', $user->Record('UserType')) ;
      SendPromotionInstructions($user, 'CE Welcome Email', "Welcome New Contributing Editor");
    }
  }
}

function PromoteAuthor($AuthorId, $Rank, $Privilege, $CurrentPrivilege='banned') {
  $power = array('banned'=>0, 'applicant'=>10, 'member'=>20, 'workshopper'=>30, 'senior workshopper'=>40, 'editor'=>50, 'administrator'=>60);

  // avoid demoting anyone who already has elevated privileges
  if ($power[$Privilege]<$power[$CurrentPrivilege])
    $Privilege = $CurrentPrivilege;

  $result = DbQuery(sprintf("UPDATE DILF_Authors SET Rank='%s', UserType='%s' WHERE AuthorId=%d LIMIT 1",
     $Rank, $Privilege, $AuthorId));

  if (($Privilege=='workshopper') or ($Privilege=='senior workshopper') or ($Privilege=='editor') or ($Privilege=='administrator')) {
    $result = DbQuery(sprintf("UPDATE DILF_Authors SET MonitorComments='Workshopped', MonitorAttention='On' WHERE AuthorId=%d LIMIT 1",
      $AuthorId));
  }
  AuditLog(0, 'Changed title of '.GetAuthor($AuthorId).' to '.$Rank);
  NotifyOfPromotion($AuthorId, $Rank);
}

function FormatDocument($title, $intable, $quicklinks, $addbreaks=TRUE) {
  $DisplayText = '';

  $doc = GetDocument(DbEscapeString($title));
  if ($doc!==false) {
    if ($intable)
      $DisplayText .= '<table class="widetable"><tr><td class="darkpanel">';
    if ($addbreaks) $DisplayText .= AddBreaks($doc);
    else $DisplayText .= $doc;
    if ($intable)
      $DisplayText .= '</td></tr></table>';
  }
  else $DisplayText .= sprintf("<p>Couldn't find document \"%s\"</p>", $title);

  if ($quicklinks)
    return FormatQuickDocLinks($DisplayText);
  else return $DisplayText;
}

?>
