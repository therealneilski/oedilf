<?php
class LimFieldFeedbackStyle extends LimFieldChoicesOrText
{
	public static $Preferences = array(
"0"=>"No preference yet. Let's see how we go.",
"1"=>"Feel free to devote your WSing efforts elsewhere since I will be unable to respond to your comments in the immediate future.",
"2"=>"Just tell me what's wrong. I'll ask for help whenever I need it. *",
"3"=>"Tell me what's wrong. Explain how to fix it, but don't provide fixes. *",
"4"=>"Point out what needs to improve and offer hints -- a word here and there.",
"5"=>"Offer me criticisms, suggestions, or even partial rewrites.",
"6"=>"I'd really appreciate any guidance, explanations, edits, or rewrites you'd care to give.",
			); 
	
	public function __construct($fieldName, $screenLabel, $size)
	{
		parent::__construct($fieldName, $screenLabel, 
			self::$Preferences, $size);
	}
	
	public static function PreferenceIndex($storedStyle)
	{
		if ($storedStyle=='') 
			return 0;
		else if (isset(self::$Preferences[$storedStyle]))
			return $storedStyle;
		else
			return -1;
	}
	
	public static function ExpandToText($storedStyle)
	{
		if ($storedStyle=='') 
			return self::$Preferences[0];
		else if (isset(self::$Preferences[$storedStyle]))
			return self::$Preferences[$storedStyle];
		else
			return $storedStyle;
	}
	
}
?>