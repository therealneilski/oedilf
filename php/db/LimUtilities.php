<?php
// Utilities for maintaining the db.

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function DoUtilityAction() {
	global $Configuration, $member, $PageTitle;
	$WordsDisplay = '';
  
  if ($_GET['Utility']=='Ajax') {
  	AddToHeader("LimAjaxJs", '<script type="text/javascript" src="LimAjax.js"></script>');
  	$hintId = "'txtHint'";
	$WordsDisplay .= '<form>Name: <input type="text" id="txt1" onkeyup="showHint(this, '.
		$hintId.')" size="40" /><div class="hinttext" id='.$hintId.'> </div></form>';
  }

	if ($_GET['Utility']=='UpdateFrontPageLims') {
		require_once "LimCache.php";
		CacheNewApprovedLims();
		$WordsDisplay .= "<p>Updated front page limericks.</p>";
	}
  
    if ($_GET['Utility']=='UpdateCleanWordList') {
        $filename = "CleanWords.txt";
        if (!$fh = fopen( "feeds/".$filename, 'w' )) die("Can't open $filename.<br >\n");

        $result = DbQuery('SELECT L.OriginalId, W.WordText FROM DILF_Limericks L, DILF_Words W
            WHERE W.VerseId = L.VerseId AND L.State = "approved" AND W.IsPhraseWord =0 
            AND LEFT(W.SortOrder,1)<>"#" AND L.Category = "normal"
            ORDER BY W.SortOrder');
        while ($line = DbFetchArray($result)) {
            fwrite($fh, sprintf("%s\thttp://%s?VerseId=%s\n",
                $line['WordText'], THIS_APP, $line['OriginalId']));
        }
        DbEndQuery($result);
        fclose( $fh );
        $WordsDisplay .= "<p>Updated clean word list for eSpindle Learning.</p>";
    }

  if ($_GET['Utility']=='CheckForLACEs') {
    $WordsDisplay .= "<h3>Checking LACEs</h3>";
    $WordsDisplay .= sprintf("<p>%s Last check was %d seconds ago.</p>",
      FormatDateTime(gmdate("Y-m-d H:i:s")),
      time()-$Configuration['LastLACECheck']);
    if ($member->CanEdit()) {
      $WordsDisplay .= CheckForLACEs(LACE_TIME, TRUE, $Count);
      if ($Count==0) {
        $WordsDisplay .= "<p>This page will reload every 200 seconds.</p>";
        SetReload("Utility=CheckForLACEs", 200);
      }
      else {
        $WordsDisplay .= "<p>This page will reload every 10 seconds.</p>";
        SetReload("Utility=CheckForLACEs", 10);
      }
    }
    else {
      $WordsDisplay .= "<p>You need to be logged in with Editor privileges to perform this function.</p>";
    }
  }

  if ($_GET['Utility']=="SummarizeCharacters") 
	{
		$WordsDisplay .= "<p>";
		$letters = array();
		$wordLetters = array();
        $nonLatinLetters = array();
		$result = DbQuery("SELECT DISTINCT WordText FROM DILF_Words WHERE 1");
		while ($line = DbFetchArray($result)) {
			$word = $line['WordText'];
			$isWord = false;
			for ($i=0; $i<strlen($word); $i++)
			{
				$letters[$word[$i]] = 1;
				if (ctype_alpha($word[$i])) $isWord = true;
			}
			if ($isWord)
			for ($i=0; $i<strlen($word); $i++)
			{
				$wordLetters[$word[$i]] = 1;
			}
            preg_match_all('~&[\w#]+?;~i', $word, $matches, PREG_PATTERN_ORDER);
            if (isset($matches[0]))
            {
              foreach ($matches[0] as $match)
                $nonLatinLetters[$match] = 1;
            }
		}
		DbEndQuery($result);
        ksort($letters);
        ksort($wordLetters);
        ksort($nonLatinLetters);
		$WordsDisplay .= "All characters:<br>";
		foreach($letters as $indexChar=>$flag)
			$WordsDisplay .= "[ ".htmlspecialchars($indexChar, ENT_QUOTES)." ]<br>";
		$WordsDisplay .= "Characters that appear in words:<br>";
		foreach($wordLetters as $indexChar=>$flag)
			$WordsDisplay .= "[ ".htmlspecialchars($indexChar, ENT_QUOTES)." ]<br>";
		$WordsDisplay .= "Non-Western characters that appear in words:<br>";
		foreach($nonLatinLetters as $indexChar=>$flag)
			$WordsDisplay .= "[ ".$indexChar." ]".htmlspecialchars("&", ENT_QUOTES).substr($indexChar, 1)."<br>";
	
		$WordsDisplay .= "</p>";
	}
  
    if ($_GET['Utility']=="SummarizeUsernameCharacters")
	{
		$WordsDisplay .= "<p>";
		$letters = array();
        $nonLatinLetters = array();
		$result = DbQuery("SELECT Name FROM DILF_Authors WHERE 1");
		while ($line = DbFetchArray($result)) {
			$word = $line['Name'];
			for ($i=0; $i<strlen($word); $i++)
				$letters[$word[$i]] = 1;
            preg_match_all('~&[\w#]+?;~i', $word, $matches, PREG_PATTERN_ORDER);
            if (isset($matches[0]))
            {
              foreach ($matches[0] as $match)
                $nonLatinLetters[$match] = 1;
            }
		}
		DbEndQuery($result);
        ksort($letters);
        ksort($nonLatinLetters);
		$WordsDisplay .= "All characters:<br>";
		foreach($letters as $indexChar=>$flag)
			$WordsDisplay .= "[ ".htmlspecialchars($indexChar, ENT_QUOTES)." ]<br>";
		$WordsDisplay .= "Entity encoded characters that appear in names:<br>";
		foreach($nonLatinLetters as $indexChar=>$flag)
			$WordsDisplay .= "[ ".$indexChar." ]".htmlspecialchars("&", ENT_QUOTES).substr($indexChar, 1)."<br>";

		$WordsDisplay .= "</p>";
	}

  else if ($_GET['Utility']=='CheckForDuplicateNumbers') {
    if ($member->CanEdit()) {
	    $result = DbQuery("SELECT COUNT(*) LimCount, LimerickNumber 
			FROM `DILF_Limericks` 
			WHERE LimerickNumber<>0 AND State<>'obsolete' 
			GROUP BY LimerickNumber ORDER BY LimCount DESC, LimerickNumber ASC LIMIT 1");
	    if (($line = DbFetchArray($result)) and
			($line['LimCount']>1)) {
			$limNumber = $line['LimerickNumber'];
			$limCount = $line['LimCount'];
			$newNumber = FindNewNumberNear($limNumber);
			$WordsDisplay .= "<p>Limerick #$limNumber has $limCount. Changed to #$newNumber. </p>";
			DisambiguateLimerick($limNumber, $newNumber);
			
	        $WordsDisplay .= "<p>This page will reload every second.</p>";
	        SetReload("Utility=CheckForDuplicateNumbers", 1);		
	    }
	    else {
			$WordsDisplay .= "<p>No duplicates found</p>";
		}
	    DbEndQuery($result);
    }
  }
  
  else if ($_GET['Utility']=='UpdateAuthorStats') {
    $start = $_GET['Start']+0;
    if ($_GET['Stop']) $stop = $_GET['Stop']+0;
	else {
	  $result = DbQuery("SELECT MAX(AuthorId) maxAuthor FROM DILF_Authors WHERE 1");
	  if ($line = DbFetchArray($result))
	    $stop = $line['maxAuthor']+5;
	  DbEndQuery($result);	
	}
    if ($_GET['Count']) $count=$_GET['Count']+0;
    else $count=25;
	$WordsDisplay .= "<p>";
    for ($i=$start; $i<$start+$count; $i++) {
      if ($i>0 && GetAuthor($i)) {
        UpdateAuthorStats($i);
        $WordsDisplay .= "Updated Author $i<br>";
      }
    }
	$WordsDisplay .= "</p>";
    $start += $count;
    if ($start<$stop) {
	  $WordsDisplay .= "<p>There's more to do. This page will reload shortly. Please be patient.</p>";
      SetReload(array("Utility=UpdateAuthorStats", "Start=$start", "Count=$count", "Stop=$stop"), 1);
	}
	else $WordsDisplay .= "<p>Done updating author statistics.</p>";
  }

  else if ($member->CanAdministrate() and ($_GET['Utility']=='PurgeLogs')) {
      $seconds = 122*24*3600;
      $date = LimTimeConverter::FormatGMDateFromNow(-$seconds);
      DbQuery("DELETE FROM DILF_Log WHERE DateTime<'$date'");
      DbQuery("DELETE FROM DILF_Timing WHERE PageTime<".(time()-$seconds));
      $WordsDisplay .= "<p>Deleted log data prior to $date</p>";
  }

  else if ($member->CanAdministrate() and ($_GET['Utility']=='AnnihilateSoloLimericks')) {
    $AuthorId = $_GET['AuthorId']+0;
    if ($_GET['Count']) $count=$_GET['Count']+0;
    else $count=1;

	$didit = false;
    $result = DbQuery("SELECT * FROM DILF_Limericks 
		WHERE State='excluded' AND PrimaryAuthorId=$AuthorId AND SecondaryAuthorId=0 LIMIT $count");
    while ($line = DbFetchArray($result)) {
      $i = $line['OriginalId'];
	  $WordsDisplay .= "<br><hr><p>Annihilating #T$i: ".$line['Verse']."</p>";
      $WordsDisplay .= AnnihilateLimerickVersions($i);
	  $didit = true;
    }
    DbEndQuery($result);
	
	if ($didit and ($_GET['Auto']>0)) {
		SetReload(array("Utility=AnnihilateSoloLimericks", "AuthorId=$AuthorId", "Count=$count", "Auto=1"), 1);
	}
  }
  
  
  else if ($_GET['Utility']=='UpdateWorkshopFeeds') {
    $start = $_GET['Start']+0;
    $stop = $_GET['Stop']+0;
    if ($_GET['Count']) $count=$_GET['Count']+0;
    else $count=25;

    $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE OriginalId>=%d AND OriginalId<=%d AND State<>'obsolete' ORDER BY OriginalId",
        $start, $start+$count-1));
    while ($line = DbFetchArray($result)) {
      $i = $line['OriginalId'];
      RSSWorkshopFeed($i);
      $WordsDisplay .= "<p>Updated Feed for #T$i</p>";
    }
    DbEndQuery($result);

    $start += $count;
    if ($start<$stop)
      SetReload(array("Utility=UpdateWorkshopFeeds", "Start=$start", "Count=$count", "Stop=$stop"), 1);
  }

  else if ($_GET['Utility']=='Feed') {
    $PageTitle = 'RSS Feed';
    RSSFeed();
    RSSAuthorFeed($member->Record('AuthorId'));
  }
  
  else if ($_GET['Utility']=='AllFeeds') {
    $PageTitle = 'RSS Feeds';
    RSSFeed();
    $result = DbQuery('SELECT DISTINCT A.AuthorId FROM DILF_Authors A, DILF_Limericks L WHERE (L.PrimaryAuthorId=A.AuthorId OR L.SecondaryAuthorId=A.AuthorId) AND L.State="approved"');
    while ($line = DbFetchArray($result)) {
      $WordsDisplay .= $line['AuthorId']." ";
      RSSAuthorFeed($line['AuthorId'], 100);
    }
    DbEndQuery($result);
  }
  
  else if ($_GET['Utility']=='XMLDictionary') {
    $WordsDisplay .= XMLDictionary($_GET['Entries']);
  }
  
  else if ($_GET['Utility']=='ExpiredRFAs') {
    $WordsDisplay .= CheckForExpiredSelfRFAs($_GET['ListOnly']);
  }
  
  // transition to RFA count storage in limerick record
  else if ($_GET['Utility']=='UpdateRFAs') {
    $WordsDisplay .= UpdateLimerickRFAs($_GET['UpdateRFAs']);
  }
  else if ($_GET['Utility']=='UpdateAllRFAs') {
    $result = DbQuery("SELECT OriginalId FROM DILF_Limericks WHERE State<>'obsolete' AND RFAs=-1 LIMIT ".$_GET['UpdateAllRFAs']);
    $WordsDisplay .= "<p>".LinkSelf(array("Utility=UpdateAllRFAs", "UpdateAllRFAs=".$_GET['UpdateAllRFAs']), "Next ".$_GET['UpdateAllRFAs'])."</p>";
    while ($line = DbFetchArray($result)) {
      $WordsDisplay .= UpdateLimerickRFAs($line['OriginalId']);
    }
    DbEndQuery($result);
  }
  
  else if ($_GET['Utility']=='CheckWords') {
    $PageTitle = 'Checking Word List';
    $Start = $_GET['Start']+0;
    $Stop = $_GET['Stop']+0;
    if ($_GET['Initialize'])
      $_SESSION['ProcessedWords'] = array();
    $WordsDisplay .= CheckForUnlimerickedWords($Start, $Stop);
    if ($Start>0) {
      $Stop=$Start-1;
      $Start = $Start-1000;
      if ($Start<0) $Start=0;
      $WordsDisplay .= "<p>There's more work to be done. The next word group will start in 1 second.</p>";
      SetReload(array("Utility=CheckWords", "Start=$Start", "Stop=$Stop"), 1);
    }
    else
      $WordsDisplay .= FormatNewWordsForm();
  }
  
  else if ($_GET['Utility']=='UpdateLimerickedWords') {
    $PageTitle = 'Checking Word List';
    $WordsDisplay .= CheckForLimerickedWords();
  }
  
  else if ($_GET['Utility']=='CheckUntended') {
    $PageTitle = 'Checking for Untended Limericks';
	$count = 20;
	if ($_GET['Count']) $count = $_GET['Count']+0;
    $WordsDisplay .= CheckForUntendedLimericks($count);
  }
  
  else if ($_GET['Utility']=='WEAP') {
    if ($member->CanAdministrate()) {
	    $PageTitle = 'WEAP Configuration';
	    $weap = new LimWeap($_SESSION['SearchList']);
	    $html = new LimHtml();
	    $WordsDisplay .= $weap->FormatConfigurationPage($html);
	}
  }
  else if ($_GET['Utility']=='WEAPRemove') {
    if ($member->CanAdministrate()) {
	    $PageTitle = 'WEAP Configuration';
	    $weap = new LimWeap($_SESSION['SearchList']);
	    $WordsDisplay .= $weap->ProcessWeapRemove();
	}
  }
  else if ($_GET['Utility']=='WEAPAdd') {
    if ($member->CanAdministrate()) {
	    $PageTitle = 'WEAP Configuration';
	    $weap = new LimWeap($_SESSION['SearchList']);
	    $WordsDisplay .= $weap->ProcessWeapAdd();
	}
  }

  else if ($_GET['Utility']=='WNOA') {
    if ($member->CanAdministrate()) {
	    $PageTitle = 'WNOA Configuration';
	    $wnoa = new LimWeap($_SESSION['SearchList'], "WNOA");
	    $html = new LimHtml();
	    $WordsDisplay .= $wnoa->FormatConfigurationPage($html);
	}
  }
  else if ($_GET['Utility']=='WNOARemove') {
    if ($member->CanAdministrate()) {
	    $PageTitle = 'WNOA Configuration';
	    $wnoa = new LimWeap($_SESSION['SearchList'], "WNOA");
	    $WordsDisplay .= $wnoa->ProcessWeapRemove();
	}
  }
  else if ($_GET['Utility']=='WNOAAdd') {
    if ($member->CanAdministrate()) {
	    $PageTitle = 'WNOA Configuration';
	    $wnoa = new LimWeap($_SESSION['SearchList'], "WNOA");
	    $WordsDisplay .= $wnoa->ProcessWeapAdd();
	}
  }

  else if ($_GET['Utility']=='WEOA') {
    if ($member->CanAdministrate()) {
	    $PageTitle = 'WEOA Configuration';
	    $weoa = new LimWeap($_SESSION['SearchList'], "WEOA");
	    $html = new LimHtml();
	    $WordsDisplay .= $weoa->FormatConfigurationPage($html);
	}
  }
  else if ($_GET['Utility']=='WEOARemove') {
    if ($member->CanAdministrate()) {
	    $PageTitle = 'WEOA Configuration';
	    $weoa = new LimWeap($_SESSION['SearchList'], "WEOA");
	    $WordsDisplay .= $weoa->ProcessWeapRemove();
	}
  }
  else if ($_GET['Utility']=='WEOAAdd') {
    if ($member->CanAdministrate()) {
	    $PageTitle = 'WEOA Configuration';
	    $weoa = new LimWeap($_SESSION['SearchList'], "WEOA");
	    $WordsDisplay .= $weoa->ProcessWeapAdd();
	}
  }
  
  return $WordsDisplay;
}

function CheckForExpiredSelfRFAs($ListOnly) {
	global $member;
	$WordsDisplay = '';

	if ($member->CanEdit()) 
	{
	    $Days = 14;
	    $TimeLimit = LimTimeConverter::FormatGMDateFromNow(-$Days*24*3600);

	    $result = DbQuery("SELECT
			L.VerseId, L.OriginalId, L.PrimaryAuthorId,
			MAX(CONCAT(W.AuthorId IN (SELECT AuthorId FROM DILF_Authors 
				WHERE UserType='editor' OR UserType='administrator') AND
				W.AuthorId<>L.PrimaryAuthorId, W.DateTime)) LastAEComment,
			MAX(CONCAT(W.AuthorId=L.PrimaryAuthorId, W.DateTime)) LastAuthorComment,
			MAX(CONCAT(W.Message LIKE 'Based on the workshopping so far%', W.DateTime))
				LastSelfRFA
			FROM DILF_Limericks L, DILF_Workshop W
			WHERE L.State='tentative' AND W.VerseId=L.VerseId AND PrimaryRFA=1 AND 
				(W.Message NOT LIKE 'Automated search and replace%')
			GROUP BY L.VerseId
			HAVING
				LastAEComment>LastAuthorComment AND
				LastAEComment>LastSelfRFA AND
				LastAEComment<'1$TimeLimit' AND
			    LastAuthorComment<'1$TimeLimit' AND
			    LastSelfRFA<'1$TimeLimit'");

	    $SearchMatches = array();
	    while ($line = DbFetchArray($result)) 
		{
			$KillRFA = true;
			$WordsDisplay .= "LastAuthorComment=".$line['LastAuthorComment'].
				" LastSelfRFA=".$line['LastSelfRFA']." ".$line['LastAuthorComment']{0}.
				" ".$line['LastSelfRFA']{0}."<br>";

			if (($line['LastAuthorComment']{0}=='0') and ($line['LastSelfRFA']{0}=='0')) 
			{
		        $WordsDisplay .= "Zeroes.";
		        $KillRFA = false; // on second thoughts
		        // check further back in the comment history for author action
		        $OriginalId = $line['OriginalId'];
		        $AuthorId = $line['PrimaryAuthorId'];
		        $result2 = DbQuery("SELECT
					MAX(CONCAT(W.AuthorId=$AuthorId, W.DateTime)) LastAuthorComment,
					MAX(CONCAT(W.Message LIKE 'Based on the workshopping so far%', W.DateTime))
						LastSelfRFA
					FROM DILF_Workshop W
					WHERE W.OriginalId=$OriginalId
					GROUP BY W.OriginalId
					HAVING LastAuthorComment<'1$TimeLimit' AND LastSelfRFA<'1$TimeLimit'");
				if ($line2 = DbFetchArray($result2)) 
				{
					$KillRFA = true; // yep, it does deserve to die
					$WordsDisplay .= "Found further back.";
				}
				DbEndQuery($result2);
			}		
			
			if ($KillRFA and $ListOnly) $SearchMatches[] = $line['OriginalId'] ;

			if ($KillRFA and !$ListOnly) 
			{
		        DeleteRFA($line['OriginalId'], $line['PrimaryAuthorId']);
		        AddWorkshopComment(0, $line['VerseId'], 
					"Author-RFA was removed automatically after $Days days. <br > ".
					GetAuthorHtml($line['PrimaryAuthorId']).
					", please address the issues raised before adding your RFA.", $commentId);
		        NotifyOfRFAExpiry($line['VerseId'], $line['OriginalId'], 
					$line['PrimaryAuthorId']);
			}
	      
			if ($KillRFA) $WordsDisplay .= "Kill RFA on ".$line['OriginalId']."<br>";
	    }
	    DbEndQuery($result);

	    if ($ListOnly) 
		{
			$_SESSION['SearchList'] = implode(",", $SearchMatches);
			$_SESSION['SearchTitle'] = 'Expired Self-RFAs';
			$WordsDisplay .= "<br >".FormatSearchResults();
	    }
	}
	return $WordsDisplay;
}

function CheckForLACEs($LaceTime, $ShowThem, &$NumSent) {
  global $Configuration;
  $WordsDisplay = '';
  $NumSent = 0;
  
//  if ((time()-$Configuration['LastLACECheck'])>60*3) {// only once per 3 minute interval
    // remember time first
    DbQuery(sprintf("UPDATE DILF_Settings SET LastLACECheck=%d WHERE 1", time()));

    // find all the Confirming limericks that have done their time but have not yet been LACEd.
    $TimeLimit = LimTimeConverter::FormatGMDateFromNow(-$LaceTime);
    $result = DbQuery("SELECT * FROM DILF_Limericks WHERE State='confirming' AND LinkCode=0 AND StateDateTime<'$TimeLimit' AND StateDateTime>0");
    $limit = $Configuration['LACEsPerCheck'];
    $WordsDisplay .= sprintf("<p>%d LACEs waiting. Send limit %d per iteration.</p>",
      DbQueryRows($result), $limit);
    while (($line = DbFetchArray($result)) and ($limit>0)) {
      $WordsDisplay .= '<p>Sending LACEs<br>';
      $WordsDisplay .= SendApprovalRequest($line['VerseId']);
      $WordsDisplay .= '</p>';
      $NumSent++;
      if (!$ShowThem)
        $WordsDisplay = ''; // don't bother displaying
      $limit--;
    }
    DbEndQuery($result);
//  }
  return $WordsDisplay;
}

function CheckForUntendedLimericks($count) {
    global $Configuration;
    $WordsDisplay = 'Untended Limericks:<br>';

    $affectedAuthors = new LimStatsUpdater();
    // any tentative limerick whose primary author has not logged on in the last 30 days
    $InactiveTime = time() - INACTIVE_TIME;
    $result = DbQuery("SELECT * FROM DILF_Limericks
        WHERE State='tentative' AND PrimaryAuthorId IN (
            SELECT AuthorId FROM DILF_Authors WHERE AccessTime<$InactiveTime
        )");
    while (($line = DbFetchArray($result)) and ($count>0)) {
        // check for a primary author RFA
        $result2 = DbQuery("SELECT * FROM DILF_RFAs WHERE AuthorId=".$line['PrimaryAuthorId']." AND OriginalId=".$line['OriginalId']);
        if (DbQueryRows($result2)==0)
        {
            ChangeLimerickState($line['VerseId'], $line['OriginalId'], 'tentative', 'untended', false);
            DbQuery("UPDATE DILF_Words SET Type='Untended' WHERE Type='Defined' AND VerseId=".$line['VerseId']);
            $affectedAuthors->Remember($line['PrimaryAuthorId'], $line['SecondayAuthorId']);
            $WordsDisplay .= "#T".$line['OriginalId']." by ".GetAuthorHtml($line['PrimaryAuthorId'])."<br>";
            $count--;
        }
        DbEndQuery($result2);
    }
    DbEndQuery($result);

    $affectedAuthors->Update();
    return $WordsDisplay;
}

function TendMyUntendedLimericks() {
  global $Configuration, $member;
  
  $affectedAuthors = new LimStatsUpdater();
  $result = DbQuery("SELECT * FROM DILF_Limericks 
    WHERE State='untended' AND PrimaryAuthorId=".$member->GetMemberId());
  while ($line = DbFetchArray($result)) {
	ChangeLimerickState($line['VerseId'], $line['OriginalId'], 'untended', 'tentative', true);
	DbQuery("UPDATE DILF_Words SET Type='Defined' WHERE Type='Untended' AND VerseId=".$line['VerseId']);
	$affectedAuthors->Remember($line['PrimaryAuthorId'], $line['SecondayAuthorId']);
  }
  DbEndQuery($result); 
  $affectedAuthors->Update();
}


function FormatNewWordsForm() {
  global $member;
  $WordsDisplay = '';

  if ($member->CanWorkshopPlus()) {
    $WordsDisplay .= '<p>This is where we manipulate the list of words waiting to be limericked.</p>';
    $WordsDisplay .= sprintf('<form action="%s?Action=NewWords" method="post">', PHPSELF);
    $WordsDisplay .= '<table class="widetable">';
    $WordsDisplay .= '<tr><th>Enter a list of words delimited by semicolons or new lines (any lines that start with numbers will have numbers stripped)</th></tr>';
    $WordsDisplay .= sprintf('<tr><td class="darkpanel"><TEXTAREA NAME="newwordlist" COLS=80 ROWS=20></TEXTAREA></td></tr>');
    $WordsDisplay .= '<tr><td class="darkpanel">'.
    	SubmitButton("NewWords", "Add Words");
    $WordsDisplay .= ' '.SubmitButton("NewWords", "Remove Words");

    $WordsDisplay .= "<br ><br >".DarkButton("Utility=UpdateLimerickedWords", "Update Limericked Words")." (Make sure any word that is defined by a limerick is removed from the unlimericked word list.)";

    $result = DbQuery('SELECT COUNT(*) "DoneWords" FROM DILF_WordList WHERE Status<>"not done"');
    $DoneWords = 0;
    if ($line = DbFetchArray($result)) $DoneWords = $line['DoneWords'];

    if ($DoneWords>0) {
      $Start = floor(($DoneWords-1) / 1000)*1000;
      $Stop = $DoneWords-1;
      $WordsDisplay .= "<br >".DarkButton(array("Utility=CheckWords", "Start=$Start", "Stop=$Stop", "Initialize=1"), "Check All Limericked Words")." (Make sure any words that no longer have a limerick are returned to the unlimericked word list.)";
    }
    DbEndQuery($result);

    $WordsDisplay .= '</td></tr>';
    $WordsDisplay .= '</table></form>';
  }

  return $WordsDisplay;
}

// Add word to the word list
function AddToWordList($word) 
{
  // if the word exists in the list and is marked as 'not done', then change it to 'partial'
  $result = DbQuery(sprintf("SELECT * FROM DILF_WordList WHERE WordText='%s'", addslashes($word)));
  if ($line = DbFetchArray($result)) {
    if ($line['Status']=='not done') 
		DbQuery(sprintf("UPDATE DILF_WordList 
			SET Status='partial', Review='check me' WHERE WordText='%s'", addslashes($word)));
    else
		DbQuery(sprintf("UPDATE DILF_WordList 
			SET Review='check me' WHERE WordText='%s'", addslashes($word)));
  }
  DbEndQuery($result);
}

function ProcessNewWordsForm() {
  global $member;

  $WordsDisplay = '';
  if ($member->CanWorkshopPlus()) {
    $wordblob = str_replace("\n", ";", $_POST["newwordlist"]);
    $wordblobs = explode(";",$wordblob);
    foreach($wordblobs as $index=>$blob) {
      $blob = trim($blob, " \t\r\n");
      $blob = preg_replace('#^([0-9.]+) #', '', $blob);
      $wordblobs[$index]=$blob;
    }

    if ($_POST['NewWords']=="Add Words") {
      // Step 1: compile a list of existing words
      $Existing = array();
      $result = DbQuery("SELECT WordText FROM DILF_WordList");
      $NumWords = DbQueryRows($result);
      $WordsDisplay .= sprintf("<p>%d words already present</p>",$NumWords);
      for ($i=0; $i<$NumWords; $i++) {
        $line = DbFetchArray($result) ;
        $Existing[] = strtolowerascii($line['WordText']);
      }
      DbEndQuery($result);

      // Step 2: Add any new words that were not already there
      $wc = 0;
      foreach($wordblobs as $trimword) {
        if (($trimword<>'') and (!in_array(strtolowerascii(stripslashes($trimword)),$Existing))) {
          DbInsert("DILF_WordList", array("WordText"=>$trimword, "Status"=>'not done'));
          $Existing[] = strtolowerascii(stripslashes($trimword));
          $wc++;
        }
      }
      $WordsDisplay .= sprintf('<p>Added %d words.</p>',$wc);

    }
    else if ($_POST['NewWords']=="Remove Words") {
      $wc = 0;
      foreach($wordblobs as $trimword) {
        if ($trimword<>'') {
          //echo '['.$trimword.']';
          $result = DbQuery(sprintf("DELETE FROM DILF_WordList WHERE WordText='%s'", $trimword));
          $wc++;
        }
      }
      $WordsDisplay .= sprintf('<p>Removed %d words.</p>',$wc);
    }
    else if ($_POST['NewWords']=="Set Needed Words") {
      $wc = 0;
      foreach($wordblobs as $trimword) {
        if ($trimword<>'') {
          //echo '['.$trimword.']';
          $result = DbQuery(sprintf("UPDATE DILF_WordList SET WordType='needed' WHERE WordText='%s'", $trimword));
          $wc++;
        }
      }
      $WordsDisplay .= sprintf('<p>Set %d words as needed.</p>',$wc);
    }
    else if ($_POST['NewWords']=="Set Extra Words") {
      $wc = 0;
      foreach($wordblobs as $trimword) {
        if ($trimword<>'') {
          //echo '['.$trimword.']';
          $result = DbQuery(sprintf("UPDATE DILF_WordList SET WordType='extra' WHERE WordText='%s'", $trimword));
          $wc++;
        }
      }
      $WordsDisplay .= sprintf('<p>Set %d words as extras.</p>',$wc);
    }
  }
  $WordsDisplay .= FormatNewWordsForm();
  return $WordsDisplay;
}

function CheckForLimerickedWords() {
  global $member;

  $WordsDisplay = '';
  
  if ($member->CanWorkshopPlus()) {
      // Update the status of the words in the wordlist
      // for all the "not done" words in the word list, check if exists in Words.
      // - if so, set the word to "partial"

      $result = DbQuery('SELECT WL.WordText FROM DILF_WordList WL, DILF_Words W WHERE WL.WordText=W.WordText AND W.Type="Defined" AND W.IsPhraseWord=0 AND WL.Status="not done"');
      $WordList = array();
      while ($line = DbFetchArray($result)) {
        $WordList[] = $line['WordText'];
      }
      DbEndQuery($result);

      $result = DbQuery('UPDATE DILF_WordList WL, DILF_Words W SET WL.Status="partial" WHERE WL.WordText=W.WordText AND W.Type="Defined" AND W.IsPhraseWord=0 AND WL.Status="not done"');

      if (count($WordList)>0) {
        $WordsDisplay .= "<p>Words removed from unlimericked word list:<br >";
        foreach($WordList as $word)
          $WordsDisplay .= '&nbsp;&nbsp;'.$word."<br >";
        $WordsDisplay .= "</p>";
      }
      else {
        $WordsDisplay .= "<p>No words removed from unlimericked word list.</p>";
      }
  }

  $WordsDisplay .= FormatNewWordsForm();
  return $WordsDisplay;
}

// Yet to do:
// - define a word state for boneyarded words, or remove them from the limerick when it goes to the yard
// - when words go untended or back to defined, we need to update the unlimericked word list
// - 
function CheckForUnlimerickedWords($Start, $Stop) {
  global $member;

  $WordsDisplay = '';
  if ($member->CanWorkshopPlus()) {
      // for all the "partial" and "complete" words, check if exists in Words.
      // - if not, set the word to "not done"

      $WordsDisplay .= "Checking words $Start to $Stop.<br >";
      
      // first compile a list of existing defined words
      $Existing = array();
      $result = DbQuery("SELECT DISTINCT WordText FROM DILF_Words 
		WHERE Type='Defined' AND IsPhraseWord=0");
      $NumWords = DbQueryRows($result);
      for ($i=0; $i<$NumWords; $i++) {
        $line = DbFetchArray($result) ;
        $Existing[] = strtolowerascii($line['WordText']);
      }
      DbEndQuery($result);

      $NewRemovals = 0;
      $result = DbQuery(sprintf('SELECT * FROM DILF_WordList WHERE Status<>"not done" LIMIT %d,%d', $Start, $Stop-$Start+1));
      $NumWords = DbQueryRows($result);
      for ($i=0; $i<$NumWords; $i++) {
        $line = DbFetchArray($result) ;

        if (!in_array(strtolowerascii(stripslashes($line['WordText'])),$Existing)) {

          DbQuery(sprintf('UPDATE DILF_WordList SET Status="not done" WHERE WordText="%s"', $line['WordText']));
          $_SESSION['ProcessedWords'][] = $line['WordText'];
          $NewRemovals++;
        }
      }
      DbEndQuery($result);
      $WordsDisplay .= sprintf("From %d 'done' words, reset %d words to 'not done'.<br >", $NumWords, $NewRemovals);
      $WordsDisplay .= sprintf("Done. %.0f ms<br>", LimGeneral::MsElapsed());

      $Total = count($_SESSION['ProcessedWords']);
      if ($Total>0) {
        $WordsDisplay .= "<p>Total: $Total words reset to 'not done'<br >";
        foreach ($_SESSION['ProcessedWords'] as $word)
          $WordsDisplay .= '&nbsp;&nbsp;'.$word."<br >";
        $WordsDisplay .= "</p>";
      }

  }
  return $WordsDisplay;
}

function FindNewNumberNear($limNumber) {
    $start = $limNumber - 500;
    if ($start<1000) $start = 1000;
    
    $result = DbQuery("SELECT DISTINCT LimerickNumber 
		FROM `DILF_Limericks` 
		WHERE LimerickNumber>=$start AND State<>'obsolete' 
		ORDER BY LimerickNumber ASC");
	$found = false;	
    while (!$found and ($line = DbFetchArray($result))) {
		$limNumber = $line['LimerickNumber'];
		//printf("start=%s, limNumber=%s <br >", $start, $limNumber);
		if ($limNumber>$start) $found = true;
		else $start++;
    }
    DbEndQuery($result);
	
	return $start;
}

function DisambiguateLimerick($limNumber, $newNumber) {
	$result = DbQuery("SELECT VerseId FROM DILF_Limericks 
		WHERE LimerickNumber=$limNumber AND State<>'obsolete' LIMIT 1");
	$line = DbFetchArray($result);
	$VerseId = $line['VerseId'];
	DbEndQuery($result);
  
	$result2 = DbQuery("UPDATE DILF_Limericks SET LimerickNumber=$newNumber WHERE VerseId=$VerseId LIMIT 1");
	AuditLog($VerseId, "Renumbered:\t$limNumber\t$newNumber");
	AddWorkshopComment(0, $VerseId, "This limerick was renumbered from #$limNumber to #$newNumber because there was another approved limerick labelled as #$limNumber (due to a software bug). Sorry for the inconvenience.", $commentId);	
	NotifyOfWorkshopComment($VerseId, "BugFix: Renumbering.");
}

?>