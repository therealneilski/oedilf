<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function ParseShowcaseInput() {
	global $PageTitle, $member;
	$WordsDisplay = '';
	
	if ($_GET['ShowcaseAction']=='Index') {
		$PageTitle = 'Showcase List';
		$WordsDisplay .= FormatShowcaseIndex();		
	}
	else if ($_GET['ShowcaseAuthor']) {
		$PageTitle = 'Author Showcase';
		$WordsDisplay .= FormatAuthorShowcase($_GET['ShowcaseAuthor']+0);
	}
	else if ($member->CanContribute())
	{
		if ($_GET['ShowcaseAdd']) {
			$PageTitle = 'Add to Showcase';
			$WordsDisplay .= ShowcaseLimerick(1, $_GET['ShowcaseAdd']+0) ;
		}	
		else if ($_GET['ShowcasePri']) {
			$PageTitle = 'Adjust Showcase Priority';
			$WordsDisplay .= ShowcaseLimerick($_GET['NewPri']+0, $_GET['ShowcasePri']+0) ;
		}
		else if ($_GET['ShowcaseRemove']) {
			$PageTitle = 'Remove from Showcase';
			$WordsDisplay .= ShowcaseLimerick(0, $_GET['ShowcaseRemove']+0) ;
		}
		else if ($_GET['ShowcaseAction']=='Fodder') {
			$PageTitle = 'Eligible for Showcase';
			$start = $_GET['Start']+0;
			$WordsDisplay .= ShowcaseEligibles($start);
		}
		else if (($_GET['ShowcaseAction']=='UpdatePriorities') and
			isset($_POST['UpdateShowcasePriorities'])) {
			$PageTitle = 'Updating Showcase Priorities';
			$WordsDisplay .= UpdateShowcasePriorities();
		}
		else BugOrHack();	
	}
	else $WordsDisplay .= '<p>You must be logged in before you can edit your showcase.</p>';
	
	return $WordsDisplay;
}

function FormatShowcaseIndex() {
	$WordsDisplay = '';

	$ShowCount = array();
	$result = DbQuery("SELECT L.PrimaryAuthorId, COUNT(L.PrimaryAuthorId) TotApproved, A.ApprovedCOunt
		FROM DILF_Limericks L, DILF_Authors A 
		WHERE L.PrimaryAuthorId=A.AuthorId AND A.Rank<>'C' AND L.State='approved' AND L.ShowcasePriority>0 
		GROUP BY L.PrimaryAuthorId 
		ORDER BY TotApproved DESC");
	while ($line = DbFetchArray($result)) {
		$Desired = $line['TotApproved'] ;
		$Allowed = LimMember::ShowcaseLimit($line['ApprovedCOunt']); 
		$Min = min($Desired, $Allowed);
		if ($Min>0)
			$ShowCount[$line['PrimaryAuthorId']] = $Min;
	}
	DbEndQuery($result);

	arsort($ShowCount, SORT_NUMERIC);
	reset($ShowCount);

	$WordsDisplay .= '<h3>Author Showcases</h3>';
	$WordsDisplay .= '<table class="widetable"><tr><th>Author</th><th>Showcase Size</th></tr>';
	foreach ($ShowCount as $id => $count) {
		$WordsDisplay .= '<tr><td class="darkpanel">';
		$WordsDisplay .= LinkSelf(array("ShowcaseAction=Author", 'ShowcaseAuthor='.$id), GetAuthorHtml($id));
		$WordsDisplay .= '</td><td class="darkpanel">'.$count.'</tr>';
		$WordsDisplay .= '</td></tr>';
	}
	$WordsDisplay .= '</table>';

	return $WordsDisplay;
}

function FormatAuthorShowcase($AuthorId) {
	global $member, $CurtainFilter;
	$WordsDisplay = '';

	RSSInitializeAuthorFeedLink($AuthorId);

	$result = DbQuery("SELECT A.*, R.*, C1.Name Born, C1.Flag BornFlag, 
			C2.Name Living, C2.Flag LivingFlag
		FROM DILF_Authors A, DILF_Ranks R, DILF_Countries C1, DILF_Countries C2 
		WHERE A.Rank=R.Rank AND A.AuthorId=$AuthorId AND C1.Code=A.BornIn AND C2.Code=A.LivingIn");
	$AuthorInfo = DbFetchArray($result);
	DbEndQuery($result);
	
	$ApprovedCount = $AuthorInfo['ApprovedCount'];
	$limit = LimMember::ShowcaseLimit($ApprovedCount);

	$FlagDisplay = '';
	if ($AuthorInfo[BornFlag] and ($AuthorInfo[LivingFlag]==$AuthorInfo[BornFlag])) 
	{
		$FlagDisplay .= sprintf(' Born and living in %s <img alt="%s" src="flags/%s" >',
		$AuthorInfo['Born'], $AuthorInfo['Born'], $AuthorInfo[BornFlag]);
	}
	else
	{
		if ($AuthorInfo[BornFlag])
			$FlagDisplay .= sprintf(' Born in %s <img alt="%s" src="flags/%s" >',
				$AuthorInfo['Born'], $AuthorInfo['Born'], $AuthorInfo[BornFlag]);
		if ($AuthorInfo[LivingFlag])
			$FlagDisplay .= sprintf(' Living in %s <img alt="%s" src="flags/%s" >',
				$AuthorInfo['Living'], $AuthorInfo['Living'], $AuthorInfo[LivingFlag]);
	}

	$WordsDisplay .= sprintf('<div class="showcasetitle">Author Showcase: <span style="font-size:larger;">%s</span></div>', GetAuthorHtml($AuthorId));
	
	if ($AuthorId == $member->GetMemberId())
		$WordsDisplay .= LinkSelf('ShowcaseAction=Fodder', 'List eligible limericks').'<br>';

	$result = DbQuery("SELECT VerseId FROM DILF_Limericks 
		WHERE PrimaryAuthorId=$AuthorId AND ShowcasePriority>0 AND State='approved'");
	$NumShowcaseLimericks = DbQueryRows($result);
	DbEndQuery($result);
	$result = DbQuery("SELECT * FROM DILF_Limericks 
		WHERE PrimaryAuthorId=$AuthorId AND ShowcasePriority>0 AND State='approved' $CurtainFilter");
	if ($NumShowcaseLimericks > DbQueryRows($result))
		$WordsDisplay .= "<p>Some limericks are listed as unsuitable for a general audience and are omitted from this display.</p>";
	DbEndQuery($result);
	
	$start = $_GET['Start']+0;
	if ($start<0) $start=0;
	$pagesize = 10;
  	
	$formatter = new LimShowcaseFormatter(LimSession::LoggedIn(), "class='widetable'");
	$WordsDisplay .= FormatLimerickList('Selected Limericks',
		"SELECT * FROM DILF_Limericks 
    	WHERE PrimaryAuthorId=$AuthorId AND ShowcasePriority>0 AND State='approved' $CurtainFilter 
    	ORDER BY ShowcasePriority DESC, LimerickNumber DESC LIMIT $limit", 
		$start, $pagesize, array("ShowcaseAction=Author", 'ShowcaseAuthor='.$_GET['ShowcaseAuthor']), 
		$NumLimericks, $formatter); 

	$WordsDisplay .= sprintf('<table class="widetable"><tr><th>About the Author</th></tr><tr><td class="darkpanel">');
	$WordsDisplay .= '<h3>'.htmlspecialchars($AuthorInfo['Name'], ENT_QUOTES).' (OEDILF '.$AuthorInfo['Title'].')</h3>';
	if ($AuthorInfo['RealName'])
		$WordsDisplay .= '<p>Real Name: '.htmlspecialchars($AuthorInfo['RealName'], ENT_QUOTES).'</p>';
	if ($FlagDisplay) 
		$WordsDisplay .= "<p>$FlagDisplay</p>";
	$WordsDisplay .= FormatQuickDocLinks(AddBreaks($AuthorInfo['Biography']));
	$WordsDisplay .= '</td></tr></table>';

	return $WordsDisplay;
}

function ShowcaseLimerick($Priority, $VerseId) {
  global $member;
  $WordsDisplay = '';

  if ($member->CanContribute()) {
    AuditLog($VerseId, sprintf('ShowcaseLimerick: Priority=%s',$Priority));
    $result = DbQuery(sprintf("UPDATE DILF_Limericks SET ShowcasePriority=%d WHERE VerseId='%d' AND PrimaryAuthorId=%d LIMIT 1",
      $Priority, $VerseId, $member->GetMemberId()));
      
    if ($Priority>1) SetRedirect(array("ShowcaseAction=Author", 'ShowcaseAuthor='.$member->GetMemberId()));
    else {
      if ($Priority>0) $WordsDisplay .= "<p>Added limerick to showcase.</p>";
      else $WordsDisplay .= "<p>Removed limerick from showcase.</p>";
      $WordsDisplay .= '<p>'.LinkSelf(array("ShowcaseAction=Author", 'ShowcaseAuthor='.$member->GetMemberId()), 'Showcase') .
        ' '.LinkSelf('AuthorId='.$member->GetMemberId(), 'My Limericks').'</p>';
      SetReload(array("ShowcaseAction=Author", 'ShowcaseAuthor='.$member->GetMemberId()));
    }
    UpdateStats();
  }
  return $WordsDisplay;
}

function ShowcaseEligibles($start)
{
	global $member;
	
	$pagesize = 100;
	$linkparams = array("ShowcaseAction=Fodder");
	
	$html = new LimHtml();
	$result = DbQuery("SELECT ShowcasePriority, LimerickNumber, Verse, VerseId
		FROM DILF_Limericks WHERE PrimaryAuthorId=".$member->GetMemberId()." AND State='approved'
		ORDER BY ShowcasePriority DESC, LimerickNumber DESC");
	$NumMatches = DbQueryRows($result);

	if ($start<0) $start=0;
	$pages = new LimPaginator($linkparams, $NumMatches, $pagesize, $start);

	$html->Heading("Eligible Showcase Limericks ".$pages->CurrRange(), 3);

	if ($NumMatches>0)
	{
		if ($NumMatches>$pagesize)
			$html->Text($pages->FormatNeatMultiplePageLinks());
		$html->BeginForm(array("ShowcaseAction=UpdatePriorities","Start=$start"), "ShowcaseEligiblesForm");
		$html->Paragraph("Higher priority limericks will appear closer to the start of your showcase. Priority 0 limericks won't be showcased. 
			Change as many of the numbers as you want and then click Update Priorities to save your changes.");
		$html->SubmitButton("UpdateShowcasePriorities", "Update Priorities");
		$html->BeginTable("ShowcaseSelectionTable", "widetable");

		DbQuerySeek($result, $start);

		for ($i=$start; ($i<$NumMatches) and ($i<$start+$pagesize); $i++)
		{
			$line = DbFetchArray($result) ;
			if ($i==$start)
			{
				$html->BeginTableRow();
				$html->TableHeadingCell("Priority");
				$html->TableHeadingCell("Limerick");
				$html->EndTableRow();
			}
			$html->BeginTableRow();
			$html->BeginTableCell();
			$html->EntryField("TNum".$line['VerseId'], $line['ShowcasePriority'], 0, 3);
			$html->EndTableCell();
			$html->TableCell("#".$line['LimerickNumber']." ".
				$line['Verse']);
			$html->EndTableRow();
		}
		$html->EndTable();
		$html->SubmitButton("UpdateShowcasePriorities", "Update Priorities");
		$html->EndForm();
	}
	else 
	{
		$html->Paragraph('No approved limericks found.');
	}
	DbEndQuery($result);

	$html->Text($pages->FormatPaginationMenu());

	return $html->FormattedHtml();
}

function UpdateShowcasePriorities()
{
	$WordsDisplay = "";
	$start = $_GET['Start']+0;
	
	$count = 0;
	foreach ($_POST as $field => $value)
	{
		if ((substr($field, 0,4)=='TNum') and is_numeric($value))
		{
			$verseId = substr($field, 4);
			$result = DbQuery(sprintf("UPDATE DILF_Limericks SET ShowcasePriority=%d WHERE VerseId=%d", $value, $verseId));
			$count++;
		}
	}
	$WordsDisplay .= sprintf('<p>%d showcase limericks updated.</p>', $count);
	SetReload(array("ShowcaseAction=Fodder", "Start=$start"));
	$WordsDisplay .= '<p>'.LinkSelf(array("ShowcaseAction=Fodder", "Start=$start"), "Back to eligible limericks.").'</p>';
	return $WordsDisplay;
}
?>