<?php

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimStatsUpdater {
	protected $affectedAuthors = array();
	
	public function __construct() {
	} 
	
	public function Remember($primary, $secondary=0)
	{
		if ($primary)
			$this->affectedAuthors[$primary] = 1;
		if ($secondary)
			$this->affectedAuthors[$secondary] = 1;
	}
	
	public function Update()
	{
		if (count($this->affectedAuthors)>0) {  
			foreach ($this->affectedAuthors as $author => $flag) {
				UpdateAuthorStats($author);
			}
		}
		UpdateStats();		
	}
}
?>