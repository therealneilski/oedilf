<?php
class LimTimeConverter
{
	protected $timeOffsetSeconds;
	protected static $dbDateTimeFormat = "Y-m-d H:i:s";
	protected static $dateTimeFormat = "d M Y H:i";
	protected static $dateFormat = "d M Y";
	
	public function __construct($timeOffsetHours)
	{
		$this->timeOffsetSeconds = $timeOffsetHours * 3600;
	}
	
	public function LocalDateTimeFromStamp($stamp)
	{
		return gmdate(self::$dateTimeFormat, $stamp + $this->timeOffsetSeconds);
	}
	
	public function LocalDateTime($gmDateTime, $additionalOffsetSeconds=0)
	{
		return gmdate(self::$dateTimeFormat, $this->localTimestampFrom($gmDateTime)+$additionalOffsetSeconds);
	}
	
	public function LocalDate($gmDateTime, $additionalOffsetSeconds=0) {
		return gmdate(self::$dateFormat, $this->localTimestampFrom($gmDateTime)+$additionalOffsetSeconds);
	}
	
	public static function FormatGMDateFromNow($offsetSeconds=0)
	{
		return gmdate(self::$dbDateTimeFormat, time()+$offsetSeconds);
	}

	protected function timestampFrom($gmDateTime)
	{
		preg_match("@([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})@",
			$gmDateTime, $regs);
		return gmmktime($regs[4], $regs[5], $regs[6], $regs[2], $regs[3], $regs[1]);
	}
	
	protected function localTimestampFrom($gmDateTime)
	{
		return $this->timestampFrom($gmDateTime) + $this->timeOffsetSeconds;
	}	
}
?>