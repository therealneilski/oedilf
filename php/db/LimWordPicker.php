<?php

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimWordPicker {

	protected $advisoryText = "<b>ADVISORY: Not all the words listed here will be acceptable as WDs (Words Defined) in The OEDILF.  This list is made up from several sources but none of them are specifically tailored to our unusual needs and while we do try to weed out the unusable, unacceptable WDs will occasionally slip through.  <i>Before you begin writing</i>, it is important that you confirm the validity of your WD.  OneLook.com is our primary source for this but any reputable dictionary will do as well.</b>";
	
	public function __construct() {
	} 
	
    public function FormatRandomWords($html) {
		global $Configuration;

		$html->Heading("Words you might like to define", 3);
		$html->BeginParagraph();
		$result = DbQuery("SELECT WordText 
			FROM DILF_WordList 
			WHERE Status='not done' AND
			WordText<'".$Configuration['AlphabetEnd']."' AND
			WordType='needed'
			ORDER BY RAND() LIMIT 10");
		$rows = 0;
		while ($word = DbFetchArray($result)) {
			$html->Text(OneLookLink($word['WordText'], true, true));
			$html->LineBreak();
			$rows++;
		}
		if ($rows==0)
			$html->Text("There are no words available in the unlimericked word list at the moment.");
		
		DbEndQuery($result);
		$html->EndParagraph();
		
		if ($rows) {
			$html->BeginParagraph();
			$html->Text($this->advisoryText);
			$html->EndParagraph();
		}
				
		return $html->FormattedHtml();
	}
	
	public function FormatTodoList($html) {
		global $Configuration;
		
		$randletter = 'A'.chr(rand(ord('a'),ord($Configuration['AlphabetEnd']{1})-1));
		$Letters = LastValidLetters($Configuration['AlphabetEnd']);

		$html->Text(str_replace("@@LETTERS@@", $Letters, FormatDocument('Words Page', FALSE, TRUE)));

		$html->LinkSelf(array("Todo=[! <".$Configuration['AlphabetEnd']."]", 
				"Needed=1", "Start=0"), 
			'All words in the current range.');
		$html->LineBreak();
		$html->Text('Words and word variants beginning with ');

		$html->Text(FormatLetterList($Configuration['AlphabetEnd'], "Todo", 
			array("Needed=1", "Start=0")));
  
		$html->LineBreak();
		$html->Text('Check for words that limerick writers have abandoned in ');
		$html->LinkSelf("Action=UntendedWordSummary", "Summary of untended limericked words");
		$html->Text('.');
  		$html->LineBreak();
		$html->Text('Check ');
		$html->LinkSelf("Action=WordSummary", "The Summary of limericked words");
		$html->Text(' to see whether any are not yet adequately covered.');
		$html->LineBreak();
		$html->EndParagraph();

		$html->BeginParagraph();
		$html->Text("More obscure words are likely to be omitted from any abridged publication of The OEDILF.");
		$html->EndParagraph();
  
		$html->BeginParagraph();
		$html->Text($this->advisoryText);
		$html->EndParagraph();

		$match = $this->CreateWordSearchFilter($_GET['Todo'], 'WordText');

		if ($match=='WordText LIKE "%"') $limit = "AND WordText<'".$Configuration['AlphabetEnd']."'";
		else $limit='';

		if ($_GET['Needed']==1)
			$result = DbQuery(sprintf("SELECT WordText, WordType 
				FROM DILF_WordList WHERE (Status='not done') AND %s AND WordType='needed' %s 
				ORDER BY WordText", $match, $limit));
		else
			$result = DbQuery(sprintf("SELECT WordText, WordType 
				FROM DILF_WordList WHERE (Status='not done') AND %s AND WordType<>'needed' %s 
				ORDER BY WordText", $match, $limit));

		$NumWords = DbQueryRows($result);

		$start = $_GET['Start']+0;
		if (($start<0) or ($start>=$NumWords)) $start=0;
		$pagesize = 100;
		$cols=4;
		$rows=25;
		$wordsperpage=$cols*$rows;
		$colwidth = floor(100 / $cols);

		$pages = new LimPaginator(array('Todo='.$_GET['Todo'], 'Needed='.$_GET['Needed']), 
			$NumWords, $wordsperpage, $start);
		$html->Text($pages->FormatNeatMultiplePageLinks());

		// discard the words we don't need
		if ($NumWords>0)
			DbQuerySeek($result, $start);
		$NumWords -= $start;

		$html->BeginTable(null, "widetable");
		$html->BeginTableRow();
		$html->BeginTableHeadingCell($cols);
		$html->Text(sprintf('Words lacking limericks: %s %s', 
			htmlspecialchars($_GET['Todo'], ENT_QUOTES), $pages->CurrRange()));
		$html->EndTableHeadingCell();
		$html->EndTableRow();
		
		$html->BeginTableRow();
		$i=0;
		for ($col=0 ; $col<$cols; $col++) {
			$icol = $i;
			if ($icol<$NumWords) 
				$html->BeginTableCell("width:$colwidth%;", "lightpanel");
			for ($row=0 ; $row<$rows; $row++) {
				if ($i<$NumWords) {
					$line = DbFetchArray($result) ;
					$html->Text(OneLookLink($line['WordText'], true, true));
					$html->LineBreak();
				}
				$i++;
			}
			if ($icol<$NumWords) 
				$html->EndTableCell();
		}
		$html->EndTableRow();
		$html->EndTable();
  
		DbEndQuery($result);

		$html->Text($pages->FormatPaginationMenu());
	
		return $html->FormattedHtml();
	}

	public function CreateWordSearchFilter($letterRange, $field)
	{
		if (substr($letterRange,0,1)=='[') {
			$match = 'LEFT('.$field.',2)>="'.htmlspecialchars(substr($letterRange,1,2)).'"';
			if (substr($letterRange,3,1)=='<')
				$match .= 'AND LEFT('.$field.',2)<"'.htmlspecialchars(substr($letterRange,4,2)).'"';
			else
				$match .= 'AND LEFT('.$field.',2)<="'.htmlspecialchars(substr($letterRange,4,2)).'"';
		}
		else if (substr($letterRange,0,1)=='<')
			$match = $field.'<"'.htmlspecialchars(substr($letterRange,1)).'"';
		else
			$match = $field.' LIKE "'.htmlspecialchars(str_replace('*', '%', $letterRange)).'"';
		return $match;
	}
	
	public function FormatWordBrowser($html) 
	{
		$match = $this->CreateWordSearchFilter($_GET['Browse'], 'SortOrder');
			
		$start = $_GET['Start']+0;
		if ($start<0) $start=0;
		$cols=4;
		$rows=25;
		$wordsperpage=$cols*$rows;
		$colwidth = floor(100 / $cols);

		$result = DbQuery(sprintf('SELECT DISTINCT WordText, SortOrder FROM DILF_Words W, DILF_Limericks L WHERE W.VerseId=L.VerseId AND W.IsPhraseWord=0 AND %s AND (State="tentative" OR State="confirming" OR State="approved") '.$CurtainFilter.' ORDER BY SortOrder', $match));
		$NumWords = DbQueryRows($result);

		$pages = new LimPaginator('Browse='.$_GET['Browse'], $NumWords, $wordsperpage, $start);
		$html->Text($pages->FormatNeatMultiplePageLinks());

		// discard the words we don't need
		if ($NumWords>0)
		DbQuerySeek($result, $start);
		$NumWords -= $start;

		$html->BeginTable(null, "widetable");
		$html->BeginTableRow();
		$html->BeginTableHeadingCell($cols);
		$html->Text(sprintf('Words: %s %s', 
			htmlspecialchars($_GET['Browse'], ENT_QUOTES), $pages->CurrRange()));
		$html->EndTableHeadingCell();
		$html->EndTableRow();
		
		$html->BeginTableRow();

		$i=0;
		for ($col=0 ; $col<$cols; $col++) {
			$icol = $i;
			if ($icol<$NumWords) 
				$html->BeginTableCell("width:$colwidth%;", "lightpanel");
			for ($row=0 ; $row<$rows; $row++) 
			{
				if ($i<$NumWords) 
				{
					$line = DbFetchArray($result) ;
					
					$html->LinkSelf("Word=".rawurlencode($line['WordText']), $line['WordText']);
					if ($_SESSION['ShowSearchText']) 
						$html->Text(" <small><b>".$line['SortOrder']."</b></small>");
					$html->LineBreak();
				}
				$i++;
			}
			if ($icol<$NumWords) 
				$html->EndTableCell();
		}
		$html->EndTableRow();
		$html->EndTable();
		DbEndQuery($result);
		
		$html->Text($pages->FormatPaginationMenu());
		return $html->FormattedHtml();		
	}
}
?>
