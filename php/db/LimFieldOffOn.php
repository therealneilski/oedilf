<?php
class LimFieldOffOn extends LimFieldSelect
{
	public function __construct($fieldName, $screenLabel) 
	{
		parent::__construct($fieldName, $screenLabel, array('Off'=>'Off', 'On'=>'On'));
	}
}

?>