<?php
class LimFieldText extends LimField
{
	public function GetFormHtml($defaultValue)
	{
		return "<input type='text' name='$this->dbFieldName' value='".
			htmlspecialchars($defaultValue, ENT_QUOTES)."'>";
	} 	
}
?>