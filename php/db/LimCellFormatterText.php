<?php
class LimCellFormatterText
{
	public function FormatHeaderCell($column)
	{
		return $column;
	}
	
	public function FormatCell($cellValue)
	{
		return htmlspecialchars($cellValue, ENT_QUOTES);
	}
}
?>