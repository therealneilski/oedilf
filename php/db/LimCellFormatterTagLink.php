<?php
class LimCellFormatterTagLink extends LimCellFormatterText
{
	public function FormatCell($cellValue)
	{
		return LinkSelf("SearchTag=".rawurlencode($cellValue), $cellValue);
	}
}
?>