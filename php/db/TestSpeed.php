<?php
$startTime = microtime(true);
require_once "LimGeneral.php";

define('IN_LIM', true);

LimSession::Setup();

require_once "LimDb.php";
require_once "LimDbFunctions.php";

LimDbConnect($dblink);

$result = DbQuery("SELECT MAX(WorkshopId) AS MostRecentId FROM DILF_Workshop");
if ($line = DbFetchArray($result))
{
    DebugMessage("Id=".$line['MostRecentId']);
}
DbEndQuery($result);

LimDbDisconnect($dblink);

LimGeneral::Log("Done");
?>
