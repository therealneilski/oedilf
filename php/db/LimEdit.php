<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }


function LimQueueMessage() {
  return '<p>Limerick Added to Queue.</p><p>It may take some hours before your limerick appears in the public lists depending on editor availability.</p>';
}

function FormatLimNotesTextBox($preset, $name) {
  return sprintf('<TEXTAREA NAME="%s" COLS=80 ROWS=6>%s</TEXTAREA>', $name, htmlentities($preset, ENT_QUOTES));
}

function FormatLimerickTextBox($preset) {
  return FormatLimNotesTextBox($preset, "limericktext");
}
function FormatLimerickNotesBox($preset) {
  return FormatLimNotesTextBox($preset, "limericknotes");
}
function FormatLimerickEdNotesBox($preset) {
  return FormatLimNotesTextBox($preset, "limerickednotes");
}
function CleanUpPostedText($text) {
    return stripslashes(trim(CleanupHtml($text), "\n\r "));
}

function DeTagWord($DefinedWord) {
  // if there are any complete tag-pairs, then strip them.
  if ((strpos($DefinedWord, '<')!==FALSE) and (strpos($DefinedWord, '>')!==FALSE))
    return strip_tags($DefinedWord) ;
  else
    return $DefinedWord ;
}

function AuthorSelectionList($preselectid, $IncludeNone=TRUE, $ContributorsOnly=FALSE) {
  $WordsDisplay = '';
  if ($ContributorsOnly)
    $result = DbQuery("SELECT * FROM DILF_Authors WHERE Rank<>'R' AND UserType<>'applicant' ORDER BY Name");
  else
    $result = DbQuery("SELECT * FROM DILF_Authors WHERE 1 ORDER BY Name");
  $NumAuthors = DbQueryRows($result);
  if ($IncludeNone) {
      if ($preselectid==0)
	$WordsDisplay .= '<option value="0" selected>(none)</option>';
      else
	$WordsDisplay .= '<option value="0">(none)</option>';
  }
  for ($i=0; $i<$NumAuthors; $i++) {
    $line = DbFetchArray($result) ;
    if ($line['AuthorId']==$preselectid) $sel = 'selected';
    else $sel = '';
    $WordsDisplay .= sprintf('<option value="%s" %s>%s</option>', $line['AuthorId'], $sel, htmlspecialchars($line['Name'], ENT_QUOTES));
  }
  DbEndQuery($result);
  return $WordsDisplay;
}

function FormatLimerickEntryForm($Preview) {
  global $LimerickEditHelpMsg, $WordHintMsg, $NewLimHelpMsg, $member, $Configuration;
  $WordsDisplay = '';

  if ($member->CanContribute()) {
    if ($Preview) {
      $WordsDisplay .= "<h3>This is a preview of your limerick. It hasn't been saved.</h3><p>It won't be saved until you click the 'Add Limerick' button.</p>";
      $WordsDisplay .= FormatPreview('', $ErrText);
    }
    $WordsDisplay .= sprintf('<form action="%s?Action=WelcomePage" method="post">', PHPSELF);
    if ($Preview and !$ErrText)
      $WordsDisplay .= SubmitButton("LimerickButton", "Add Limerick").'<br><br>';
    $WordsDisplay .= '<table class="widetable"><tr><th>New Limerick '.$NewLimHelpMsg.'</th></tr>';
    if ($member->CanEdit()) {
      $WordsDisplay .= '<tr><td colspan=2 class="entryform">';
      if ($_POST['primaryauthor']) $Primary = $_POST['primaryauthor'];
      else $Primary = $member->GetMemberId();
      if ($_POST['secondaryauthor']) $Secondary = $_POST['secondaryauthor'];
      else $Secondary = 0;
      if ($_POST['limerickdate']) $date = $_POST['limerickdate'];
      else $date = LimTimeConverter::FormatGMDateFromNow();
      
      if ($_SESSION['EditAuthors']) {
        $WordsDisplay .= 'Author <select name="primaryauthor">' . AuthorSelectionList($Primary) .'</select> ' ;
        $WordsDisplay .= 'Coauthor <select name="secondaryauthor">' . AuthorSelectionList($Secondary) .'</select> ' ;
      }
      $WordsDisplay .=  sprintf('Date <input type=text name="limerickdate" value="%s">', $date);
      $WordsDisplay .= '</td><tr>';
    }
    $WordsDisplay .= '<tr><td colspan=2 class="entryform">';
    $WordsDisplay .= 'Limerick Text '.$LimerickEditHelpMsg.'<br>';
    $WordsDisplay .= FormatLimerickTextBox(CleanUpPostedText($_POST['limericktext']));
    $WordsDisplay .= '</td><tr>';
    $WordsDisplay .= '<tr><td colspan=2 class="entryform">';
    $WordsDefined = DeTagWord(stripslashes($_POST["wordsdefined"]));
    $WordsDisplay .= sprintf('Word(s) Defined %s<br><TEXTAREA NAME="wordsdefined" COLS=30 ROWS=3>%s</TEXTAREA>', $WordHintMsg, $WordsDefined);
    $WordsDisplay .= "Make sure your main word starts within the OEDILF's current alphabetical range (Aa-".LastValidLetters($Configuration['AlphabetEnd']).").";
    $WordsDisplay .= '<tr><td colspan=2 class="entryform">';
    $WordsDisplay .= sprintf('<input type=hidden name="PosterId" value="%s">', $member->GetMemberId());
    $WordsDisplay .= '<br>';
    $WordsDisplay .= SubmitButton("LimerickButton", "Preview Limerick");
    if ($member->CanEdit() and !$_SESSION['EditAuthors']) {
      $WordsDisplay .= ' '.SubmitButton("LimerickButton", "Edit Authors");
    }
    if ($Preview)
      $WordsDisplay .= ' '.SubmitButton("LimerickButton", "Add Limerick");
    $WordsDisplay .= ' '.SubmitButton("LimerickButton", "Cancel");
    $WordsDisplay .= '</td></tr>';

    $WordsDisplay .= '<tr><th>Optional Extra Information:</th></tr>';
    $WordsDisplay .= '<tr><td colspan=2 class="entryform">';
    if ($Configuration['WPSISubmission']=='On')
        $WordsDisplay .= "Instructions: Leave the [[WPSI]] tag in your Author's note if your limerick was submitted to the Washington Post Style Invitational, otherwise delete it.<br>";
    if (!$Preview and !($_POST['limericknotes'])) {
      $WordsDisplay .= 'Author\'s Note '.$LimerickEditHelpMsg.'<br>'.
        FormatLimerickNotesBox(($Configuration['WPSISubmission']=='On') ? "[[WPSI]]" : "");
    }
    else
      $WordsDisplay .= 'Author\'s Note '.$LimerickEditHelpMsg.'<br>'.
        FormatLimerickNotesBox(CleanUpPostedText($_POST['limericknotes']));
    $WordsDisplay .= '</td><tr>';
    $WordsDisplay .= '</table>';
    $WordsDisplay .= '</form>';
  }
  else $WordsDisplay .= '<p>You must be logged in to submit a limerick.</p>';

  return $WordsDisplay;
}

function CheckLimerick($limericktext, $dwords) { //-------- Check for common faults.
  global $Configuration;
  $WordsDisplay = '';
  $AnyWarnings = FALSE;
  $NumWords = 0;

  // This should return an empty string if there were no warnings.
  $AnyWithin = FALSE;
  $WordsWithout = '';
  $FormattingInWD = FALSE;
  $sep = '';
  foreach($dwords as $word) {
    if (trim($word)<>'') {
      if (trim(strtolowerascii($word))<$Configuration['AlphabetEnd']) $AnyWithin = TRUE;
      else {
        $WordsWithout .= $sep.trim($word);
        $sep = '; ';
      }
      $NumWords++;
    }
  }
  if ($NumWords==0) {
    $WordsDisplay .= sprintf('<p>Warning: No dictionary words were specified in the Defined Words list.</p>');
    $AnyWarnings = TRUE;
  }
  else if (!$AnyWithin) {
    $WordsDisplay .= sprintf('<p>Warning: You are defining a word that is beyond the range of words being worked on. (<b>%s</b>)</p>',
      stripslashes($WordsWithout));
    $AnyWarnings = TRUE;
  }

  $LimLines = explode("\n", $limericktext);
  if (count($LimLines)<>5) {
    $WordsDisplay .= sprintf('<p>Warning: Your limerick contains %d lines. It should have 5.</p>', count($LimLines));
    $AnyWarnings = TRUE;
  }
  $NeedCaps = FALSE;
  $AnyBold = FALSE;
  $DefinedWordInLim = FALSE;
  foreach ($LimLines as $LimLine) {
    $line = trim($LimLine, " \t\n\r(\"'-");
    if (preg_match("#^[a-z]#", $line))
      $NeedCaps = TRUE;
    if (preg_match("#<[ ]*b[ ]*>#i", $line) or (preg_match("#<[ ]*strong[ ]*>#i", $line)))
      $AnyBold = TRUE;
    foreach($dwords as $word) {
      if ((trim($word)<>'') and (stristr($line, trim($word)))) $DefinedWordInLim = TRUE;
    }
  }
  if ($NeedCaps) {
    $WordsDisplay .= '<p>Warning: Please use capital letters for the start of each line (unless you\'re splitting a long word between two lines, in which case don\'t capitalize the second half of the word).</p>';
    $AnyWarnings = TRUE;
  }
  if ($DefinedWordInLim and !$AnyBold) {
    $WordsDisplay .= '<p>Warning: Please highlight the defined words within your limerick using formatting tags ( &lt;b&gt; and &lt;/b&gt; ).</p>';
    $AnyWarnings = TRUE;
  }

  if ($AnyWarnings)
    $WordsDisplay = FormatWarning($WordsDisplay).
      '<p>To avoid delays in releasing your limerick, please consider revising it to address the warnings.</p>';

  return $WordsDisplay;
}

function FormatPreview($OrigId, &$ErrText) {
  $WordsDisplay = '';

  if ($_POST['primaryauthor']) {
    $primary = $_POST['primaryauthor'];
    $secondary = $_POST['secondaryauthor'];
    $date = $_POST['limerickdate'];
  }
  else {
    $primary = $_POST['PosterId'];
    $secondary = 0;
    $date = LimTimeConverter::FormatGMDateFromNow(); // default = now
  }
  $limericktext = CleanUpPostedText($_POST['limericktext']);
  $line['Verse'] = stripslashes($limericktext);
  $line['AuthorNotes'] = CleanUpPostedText($_POST['limericknotes']);
  $line['EditorNotes'] = CleanUpPostedText($_POST['limerickednotes']);
  $line['State'] = 'preview';
  if ($OrigId) $line['OriginalId'] = $OrigId;
  else $line['OriginalId'] = '?';
  $line['PrimaryAuthorId'] = $primary;
  $line['SecondaryAuthorId'] = $secondary;
  $line['DateTime'] = $date;
  $line['Version'] = '?';
  $line['Category'] = 'normal';
  $words = array();
  $WordsDefined = DeTagWord(stripslashes($_POST["wordsdefined"]));
  $dwords = explode("\n", $WordsDefined."\n");
  foreach($dwords as $word) {
    if (trim($word)<>'') $words[] = trim($word);
  }
  $WordsDisplay .= FormatLimBlock($line, FALSE, FALSE, $words) ;

  // return ErrText separately so that the caller knows if there was an error
  $ErrText = CheckLimerick($limericktext, $dwords);
  $WordsDisplay .= $ErrText;
  return $WordsDisplay;
}

function ProcessNewLimerick() {
  global $member;
  $WordsDisplay = '';
  //echo print_r($_POST).'<br>';

  if ($member->CanContribute()) {

    if ($_POST['primaryauthor']) {
      $primary = $_POST['primaryauthor'];
      $secondary = $_POST['secondaryauthor'];
      $date = $_POST['limerickdate'];
    }
    else {
      $primary = $_POST['PosterId'];
      $secondary = 0;
      $date = LimTimeConverter::FormatGMDateFromNow(); // default = now
    }
    $PosterId = $_POST['PosterId'];

    $limericktext = CleanUpPostedText($_POST['limericktext']);

    $result = DbQuery(sprintf("SELECT Verse FROM DILF_Limericks WHERE Verse='%s'", DbEscapeString($limericktext)));
    if (DbQueryRows($result)>0) $Duplicate = TRUE;
    else $Duplicate = FALSE;
    DbEndQuery($result);

    if ($Duplicate) $WordsDisplay .= '<p>This limerick has already been submitted.</p>';
    else if ($primary==0) $WordsDisplay .= '<p>You must specify a primary author.</p>';
    else if (trim($_POST['limericktext'])=='') $WordsDisplay .= '<p>Empty Limerick not stored.</p>';
    else {
      $limericknotes = CleanUpPostedText($_POST['limericknotes']);
      $WordsDefined = DeTagWord(stripslashes($_POST["wordsdefined"]));
      $dwords = explode("\n", $WordsDefined."\n");

      AuditLog(0, DbEscapeString('New Limerick: '.substr($limericktext, 0, 20)));
      
      DbInsert("DILF_Limericks", array("Verse"=>DbEscapeString($limericktext), "DateTime"=>$date,
      	"PrimaryAuthorId"=>$primary, "SecondaryAuthorId"=>$secondary, 
      	"AuthorNotes"=>DbEscapeString($limericknotes), "OriginalId"=>0, "Version"=>0,
      	"UpdateDateTime"=>$date,"State"=>'new'));
      $VerseId = DbLastInsert();

      // New limerick: update the OriginalId -- must set the field to default to the VerseId
      $result = DbQuery(sprintf("UPDATE DILF_Limericks SET OriginalId = '%d' WHERE VerseId='%d' LIMIT 1",
        $VerseId, $VerseId));

      // Add Keywords with links to the limerick
	  AddLimerickWords($VerseId, $dwords);

      $LimLines = explode("\n", $limericktext);
      NotifyOfNewLimerick($VerseId, $LimLines[0]);

      $WordsDisplay .= LimQueueMessage();

      if ($WarningMsg = CheckLimerick($limericktext, $dwords))
        $WordsDisplay .= $WarningMsg;
      else
        SetReload('Workshop='.$VerseId);
        
      UpdateLimerickRFAs($VerseId);
      $result = DbQuery("UPDATE DILF_Authors SET Rank='C', FirstLimDate='$date' 
		WHERE AuthorId IN ($primary, $secondary) AND Rank='R'"); // update rank to contributor if member was merely registered
      
      UpdateAuthorStats($primary);
      if ($secondary) UpdateAuthorStats($secondary);

      UpdateStats();
      AuditLog($VerseId, 'New Limerick Complete: '.substr($limericktext, 0, 20));
    }
    $WordsDisplay .= sprintf('<p><a href="%s?Workshop=%d">Workshopping</a>.</p>', PHPSELF, $VerseId);

  }
  else $WordsDisplay .= '<p>You must be logged in to submit a limerick.</p>';

  return $WordsDisplay;
}

function InsertNewWord($verseId, $limWord, $seqNum, $phraseWord)
{
	DbInsert("DILF_Words", array('WordText'=>addslashes($limWord->WordText()), 'VerseId'=>$verseId, 
		'Type'=>'Defined', 'Sequence'=>$seqNum, 'SearchText'=>addslashes($limWord->SearchText()),
		'Sound'=>$limWord->Sound(), 'SortOrder'=>addslashes($limWord->SortOrder()), 'IsPhraseWord'=>$phraseWord));
}

function AddLimerickWords($verseId, $dwords)
{
    $seqNum = 0;
	$doneWords = array();
    foreach($dwords as $word) 
	{
        if ((trim($word)<>'') and (!in_array(trim($word), $doneWords)))
		{
			$limWord = new LimWord($word);
			InsertNewWord($verseId, $limWord, $seqNum, 0);
			$doneWords[] = $limWord->WordText();
			$seqNum++;
			AddToWordList($limWord->WordText());
		}
	}
	foreach($dwords as $word)
	{
		if (trim($word)<>'')
		{
			$limWord = new LimWord($word);
			$subWords = $limWord->SubWords();
			if (count($subWords)>1)
			{
				foreach($subWords as $subWord)
				{
					if (!in_array($subWord, $doneWords))
					{
						$phraseWord = new LimWord($subWord);
						InsertNewWord($verseId, $phraseWord, $seqNum, 1);
						$doneWords[] = $phraseWord->WordText();
						$seqNum++;
					}
				}				
			}
        }
    }
}

function FormatLimerickRevisionForm($VerseId, $Preview) {
  global $LimerickEditHelpMsg, $WordHintMsg, $member, $WSRevisionPromptMsg;
  $WordsDisplay = '';
  
  $editConflict = false;
  $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE VerseId='%d'", $VerseId));
  $NumMessages = DbQueryRows($result);
  if ($NumMessages>0) {
    $line = DbFetchArray($result) ;
    $mine = IsThisMine($line);

	if ($line['State']=='obsolete') {
      $WordsDisplay .= '<p class="warninghighlight">It appears that someone else has just finished editing this limerick since the time you last viewed the workshop. Please return to the workshop and review the changes they\'ve made before revising.</p><p>(If you\'ve made lots of changes, you might want to copy your changes or jot them down before you go back to the workshop and lose them.)</p><p>'.WorkshopLink($VerseId).'</p>';	  
	  $editConflict = true;
	}
    if ($Preview or !$editConflict) {
     if ($member->CanEdit() or $mine) {

      if ($Preview) {
        $WordsDisplay .= "<h3>This is a preview of your limerick. It hasn't been saved.</h3>";
		if (!$editConflict) $WordsDisplay .= "<p>It won't be saved until you click the 'Save Revised Limerick' button.</p>";
        $WordsDisplay .= FormatPreview($line['OriginalId'], $ErrText);
        $Wordsbox = DeTagWord(stripslashes($_POST["wordsdefined"]));
        $Primary = $_POST["primaryauthor"];
        $Secondary = $_POST["secondaryauthor"];
        $Versebox =  CleanUpPostedText($_POST['limericktext']);
        $Authornotesbox = CleanUpPostedText($_POST['limericknotes']);
        $Editornotesbox = CleanUpPostedText($_POST['limerickednotes']);
        $Workshopcommentbox = CleanUpPostedText($_POST['workshoptext']);
      }
      else {
        $ErrText = '';
        $words = QueryWordList($line['VerseId']);
        $Wordsbox = implode("\n", $words);
        $Primary = $line['PrimaryAuthorId'];
        $Secondary = $line['SecondaryAuthorId'];
        $Versebox =  $line['Verse'];
        $Authornotesbox = $line['AuthorNotes'];
        $Editornotesbox = $line['EditorNotes'];
        $Workshopcommentbox = $WSRevisionPromptMsg;
      }
      AuditLog($VerseId, 'FormatLimerickRevisionForm');
      $WordsDisplay .= sprintf('<form action="%s?Workshop=%d" method="post">', PHPSELF, $VerseId);
      if ($Preview and !$ErrText and !$editConflict)
        $WordsDisplay .= SubmitButton("LimerickButton", "Save Revised Limerick").'<br><br>';
      $WordsDisplay .= '<table class="widetable"><tr><th>Revise Limerick:</th></tr>';
      $WordsDisplay .= '<tr><td colspan=2 class="entryform">';
      if ($_SESSION['EditAuthors']) {
        $WordsDisplay .= 'Author <select name="primaryauthor">' . AuthorSelectionList($Primary) .'</select> ' ;
        $WordsDisplay .= 'Coauthor <select name="secondaryauthor">' . AuthorSelectionList($Secondary) .'</select> ' ;

      }
      else {
        $AuthorName = "<b>".GetAuthorHtml($Primary)."</b>";
        if ($Secondary)
          $CoauthorName = " and <b>".GetAuthorHtml($Secondary)."</b>;";
        else $CoauthorName = ";";
        
        $WordsDisplay .= "By $AuthorName$CoauthorName ";
        $WordsDisplay .= sprintf('<input type=hidden name="primaryauthor" value="%s"> ', $Primary);
        $WordsDisplay .= sprintf('<input type=hidden name="secondaryauthor" value="%s"> ', $Secondary);
      }
      if ($member->CanEdit()) {
        $WordsDisplay .=  sprintf('Date <input type=text name="limerickdate" value="%s">', LimTimeConverter::FormatGMDateFromNow());
      }
      $WordsDisplay .= '</td><tr>';
      $WordsDisplay .= '<tr><td colspan=2 class="entryform">';
      $WordsDisplay .= 'Limerick Text '.$LimerickEditHelpMsg.'<br>'.FormatLimerickTextBox($Versebox);
      $WordsDisplay .= '<br>Workshop Comment ';
	if (!$mine)
	{
		$html = new LimHtml();
		$html->BeginDiv("", "margin-left: 30px; display:inline;", HidePromptScript($WSRevisionPromptMsg, "wstextid"));
		$lim = new LimLimerick($VerseId, $member->GetMemberId());
		FormatTextMacroButtons($html, $lim, $member);
		$html->EndDiv();
		$WordsDisplay .= $html->FormattedHtml();
	} 
      $WordsDisplay .= '<br>'.FormatWorkshopCommentBox($Workshopcommentbox, 4, $WSRevisionPromptMsg);
      $WordsDisplay .= '</td><tr>';
      $WordsDisplay .= '<tr><td colspan=2 class="entryform">';
      $WordsDisplay .= sprintf('Word(s) Defined %s<br><TEXTAREA NAME="wordsdefined" COLS=30 ROWS=3>%s</TEXTAREA>', $WordHintMsg, $Wordsbox);
      $WordsDisplay .= '</td><tr>';
      $WordsDisplay .= '<tr><td colspan=2 class="entryform">';
      $WordsDisplay .= sprintf('<input type=hidden name="PosterId" value="%s">', $member->GetMemberId());
      $WordsDisplay .= sprintf('<input type=hidden name="VerseId" value="%s">', $VerseId);
      $WordsDisplay .= '<br>';
	  if (!$editConflict) {
        $WordsDisplay .= SubmitButton("LimerickButton", "Preview Revised Limerick");
        if (!$_SESSION['EditAuthors']) {
          $WordsDisplay .= ' '.SubmitButton("LimerickButton", "Revise Authors");
        }
        if ($Preview)
          $WordsDisplay .= ' '.SubmitButton("LimerickButton", "Save Revised Limerick");
	  }
      $WordsDisplay .= ' '.SubmitButton("LimerickButton", "Cancel");
      $WordsDisplay .= '</td></tr>';
      $WordsDisplay .= '<tr><th>Optional Extra Information:</th></tr>';

      $WordsDisplay .= '<tr><td colspan=2 class="entryform">';
      $WordsDisplay .= 'Author\'s Note '.$LimerickEditHelpMsg.'<br>'.FormatLimerickNotesBox($Authornotesbox);
      $WordsDisplay .= '</td><tr>';

      if ($member->CanAdministrate()) {
        $WordsDisplay .= '<tr><td colspan=2 class="entryform">';
        $WordsDisplay .= 'Editor\'s Note<br>'.FormatLimerickEdNotesBox($Editornotesbox);
        $WordsDisplay .= '</td><tr>';
      }
      else {
        $WordsDisplay .= sprintf('<input type=hidden name="useoldednotes" value="1">');
      }

      $WordsDisplay .= '</table>';
      $WordsDisplay .= '</form>';
      
		$html = new LimHtml();
		$html->Heading("Current workshop state:", 3);
    	$lim = new LimLimerick($line['OriginalId'], $member->GetMemberId());
		FormatWorkshopHistory($html, $lim, $member);
		$WordsDisplay .= $html->FormattedHtml();
    }
    else {
      $WordsDisplay .= ShowNoAuthority();
      AuditLog($VerseId, sprintf('Revise memberid=[%s], PrimaryAuthorId=[%s], SecondaryAuthorId=[%s], editprivilege=[%s], mine=[%s]',
        $member->GetMemberId(), $line['PrimaryAuthorId'], $line['SecondaryAuthorId'], $member->CanEdit(), $mine));
    }
   }
  }

  return $WordsDisplay;
}

function IsThisMine($lim) {
	global $member;
	return ($member->GetMemberId() and
			(($member->GetMemberId()==$lim['PrimaryAuthorId']) or 
			($member->GetMemberId()==$lim['SecondaryAuthorId'])));
}

function ProcessRevisedLimerick() {
  global $member, $WSRevisionPromptMsg;
  $WordsDisplay = '';
  //echo print_r($_POST).'<br>';
  
  $VerseId = $_POST['VerseId'];
  $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE VerseId='%d'", $VerseId));
  $NumMessages = DbQueryRows($result);
  if ($NumMessages>0) {
    $line = DbFetchArray($result) ;
    DbEndQuery($result);
    $OriginalId = $line['OriginalId'];
    $mine = IsThisMine($line);
    $WSState = $line['WorkshopState'];
    if ($member->CanEdit() or $mine) {

      if ($_POST['limerickdate']) {
        $date = $_POST['limerickdate'];
      }
      else {
        $date = LimTimeConverter::FormatGMDateFromNow(); // default = now
      }
      $primary = $_POST['primaryauthor'];
      $secondary = $_POST['secondaryauthor'];
      $PosterId = $_POST['PosterId'];

      if ($primary==0) $WordsDisplay .= '<p>You must specify a primary author.</p>';
      else if (trim($_POST['limericktext'])=='') $WordsDisplay .= '<p>Empty Limerick not stored.</p>';
      else {
        AuditLog($VerseId, sprintf('ProcessRevisedLimerick: OId=%d Vers=%d State=%s', $OriginalId, $line['Version'], $line['State']));

        $limericktext = CleanUpPostedText($_POST['limericktext']);
        $limericknotes = CleanUpPostedText($_POST['limericknotes']);
        $limerickednotes = CleanUpPostedText($_POST['limerickednotes']);

        // find the previous version before we add another
        $result = DbQuery(sprintf('SELECT * FROM DILF_Limericks WHERE OriginalId="%d" AND State<>"obsolete"', $OriginalId));
        if (DbQueryRows($result)<>1) die(sprintf("There are %d current definitions for limerick %d. Please note the limerick you were working on and contact an OEDILF Administrator.", DbQueryRows($result), $OriginalId));
        $previousline = DbFetchArray($result) ;
        $HeldById = $previousline['HeldById'];
        $PenultimateId = $previousline['VerseId'];
        $PenultimateVers = $previousline['Version'];
        $NewState = $previousline['State'];
        // non-WEs must go through the 'revised' state after any limerick edit. WEs don't have to.
        if  (!$member->CanWorkshop() and ($NewState=='tentative')) $NewState ='revised';
        if  (($NewState=='confirming') and !$member->CanEdit()) {
          // drop back to tentative/revised state
          if (!$member->CanWorkshop()) $NewState ='revised';
          else $NewState ='tentative';
          // clear self RFAs
          DeleteRFA($OriginalId, $primary);
          if ($secondary) DeleteRFA($OriginalId, $secondary);
        }
        $LimNumber = $previousline['LimerickNumber'];
        $Category = $previousline['Category'];

        DbEndQuery($result);
        AuditLog($VerseId, sprintf('ProcessRevisedLimerick+: PrevId=%s PrevVers=%s', $PenultimateId, $PenultimateVers));

        if ($_POST['useoldednotes']) $limerickednotes = $previousline['EditorNotes']; // needed a special kludge, since erased notes would look like no notes

        // insert the new revision
		DbInsert("DILF_Limericks", array("Verse"=>DbEscapeString($limericktext), "DateTime"=>$date,
			"PrimaryAuthorId"=>$primary, "SecondaryAuthorId"=>$secondary, "AuthorNotes"=>DbEscapeString($limericknotes),
			"EditorNotes"=>DbEscapeString($limerickednotes), "OriginalId"=>$OriginalId,
			"Version"=>$PenultimateVers+1, "UpdateDateTime"=>$date, 
			"State"=>$NewState, "WorkshopState"=>$WSState, "LimerickNumber"=>$LimNumber, 
			"Category"=>$Category, "HeldById"=>$HeldById, "StateDateTime"=>$date));
        $VerseId = DbLastInsert();

        AuditLog($VerseId, 'ProcessRevisedLimerick+: Added');

        // Add Keywords with links to the limerick
        $WordsDefined = DeTagWord(stripslashes($_POST["wordsdefined"]));
        $dwords = explode("\n", $WordsDefined."\n");
		AddLimerickWords($VerseId, $dwords);
		
        // Add the workshop comment
        $workshoptext = CleanUpPostedText(str_replace($WSRevisionPromptMsg, '', $_POST['workshoptext']));
        if ($workshoptext)
          $result = AddWorkshopComment($member->GetMemberId(), $VerseId, $workshoptext, $commentId);

        if ($WarningMsg = CheckLimerick($limericktext, $dwords))
          $WordsDisplay .= $WarningMsg;
        else
          SetReload('Workshop='.$VerseId);
          
        UpdateLimerickRFAs($OriginalId);

        AuditLog($VerseId, 'ProcessRevisedLimerick+: Added Words');
        $LimLines = explode("\n", $limericktext);
        NotifyOfRevision($VerseId, $LimLines[0]);

        // remove the DILF_Words links to the previous version
        $result = DbQuery(sprintf('DELETE FROM DILF_Words WHERE VerseId="%d"', $PenultimateId));

        // set the previous version to obsolete
        $result =DbQuery(sprintf('UPDATE DILF_Limericks SET State = "obsolete" WHERE VerseId=%d', $PenultimateId));

        UpdateAuthorStats($primary);
        if ($secondary) UpdateAuthorStats($secondary);
        UpdateStats();
        RSSWorkshopFeed($OriginalId);
      
        AuditLog($VerseId, 'ProcessRevisedLimerick+: Removed old word links and obsoleted old limerick.');

        $WordsDisplay .= LimQueueMessage();

      }
    }
    else {
      $WordsDisplay .= ShowNoAuthority();
      AuditLog($VerseId, sprintf('Revise memberid=[%s], PrimaryAuthorId=[%s], SecondaryAuthorId=[%s], editprivilege=[%s], mine=[%s]',
        $member->GetMemberId(), $line['PrimaryAuthorId'], $line['SecondaryAuthorId'], $member->CanEdit(), $mine));
    }

    $WordsDisplay .= WorkshopLink($VerseId);

  }
  else {
    DbEndQuery($result);
    $ErrStr = sprintf("<p>Could not find Limerick Record [%s]<br> This error has been observed to occur when 'Save Revised Limerick' is pressed before the preview page has fully loaded.</p>", $_POST['VerseId']);
    $WordsDisplay .= $ErrStr;
    AuditLog($VerseId, 'ProcessRevisedLimerick '.$ErrStr);
  }

  return $WordsDisplay;
}

function ShowNoAuthority() {
	global $member;
	$WordsDisplay = '<p>';
	if (!$member->GetMemberId()) $WordsDisplay .= 'You are not logged in.<br>';
	$WordsDisplay .= 'You do not have the authority to revise this limerick.</p>';
	return $WordsDisplay;
}

// update the LastUpdate field of the limerick to note that the limerick has changed or has had a workshop comment added
function TouchLimerick($VerseId) {
  $result = DbQuery(sprintf("UPDATE DILF_Limericks SET UpdateDateTime = '%s' WHERE VerseId='%d' LIMIT 1",
        LimTimeConverter::FormatGMDateFromNow(), $VerseId));
}

function SetWorkshopState($VerseId, $State) {
  // states are 'none' - initial, 'started' - once the first non-author comment is added, 'attention' - needing attention, 'rfa' - the approval email has been sent
  $result = DbQuery(sprintf("UPDATE DILF_Limericks SET WorkshopState = '%s' WHERE VerseId='%d' LIMIT 1",
        $State, $VerseId));
}

// Set the time we last viewed workshop/revision info
function MarkAllRead($AuthorId, $timestamp) {
  $result = DbQuery(sprintf("UPDATE DILF_Authors SET WorkshopTime = '%s' WHERE AuthorId='%d' LIMIT 1",
        $timestamp, $AuthorId));
}

?>
