<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function FormatNewLimerickPage() {
	global $member;
    $memberid = $member->GetMemberId();
	$WordsDisplay = '';
	$_SESSION['EditAuthors'] = 0;

	$Approved = 0;
	$Total = 0;
	$result = DbQuery("SELECT SUM(State='approved') Approved, 
		SUM(State='confirming') Confirming, COUNT(*) Total 
		FROM DILF_Limericks 
		WHERE (PrimaryAuthorId=$memberid AND SecondaryAuthorId=0) AND 
			(State='approved' OR State='confirming' OR State='tentative' OR 
			State='revised' OR State='new')");
	if (($line = DbFetchArray($result)) AND ($line['Total']>0)) {
		$Approved += ($line['Approved']+$line['Confirming']);
		$Total += $line['Total'];
	}
	DbEndQuery($result);

	$result = DbQuery("SELECT SUM(State='approved') Approved, 
		SUM(State='confirming') Confirming, COUNT(*) Total 
		FROM DILF_Limericks 
		WHERE ((PrimaryAuthorId=$memberid AND SecondaryAuthorId!=0) OR (SecondaryAuthorId=$memberid)) AND
			(State='approved' OR State='confirming' OR State='tentative' OR 
			State='revised' OR State='new')");
	if (($line = DbFetchArray($result)) AND ($line['Total']>0)) {
		$Approved += ($line['Approved']+$line['Confirming'])/2;
		$Total += $line['Total']/2;
	}
	DbEndQuery($result);

	$PrimaryHeld = 0;
	$PrimaryTotal = 0;
	$PrimaryApproved = 0;
	$result = DbQuery("SELECT SUM(State='approved') Approved, 
		SUM(State='confirming') Confirming, SUM(State='held') Held, COUNT(*) Total 
		FROM DILF_Limericks 
		WHERE (PrimaryAuthorId=$memberid) AND (State<>'obsolete')");
	if (($line = DbFetchArray($result)) AND ($line['Total']>0)) {
		$PrimaryHeld += $line['Held'];
		$PrimaryTotal += $line['Total'];
		$PrimaryApproved += ($line['Approved']+$line['Confirming']);
	}
	DbEndQuery($result);

	$Limit=0.75;
	if ($Total>0) $Backlog = ($Total-$Approved)/$Total;
	else $Backlog=0;

	$ZeroRFATentatives = 0;
	$TotalTentatives = 0;
	$ZeroRFALimit = 1.0;
	$ZeroRFAPercentage = 0;
	$result = DbQuery("SELECT SUM(RFAs=0) ZeroRFACount, COUNT(*) Total 
		FROM DILF_Limericks 
		WHERE (PrimaryAuthorId=$memberid) AND (State='tentative' OR State='revised')");
	if (($line = DbFetchArray($result)) AND ($line['Total']>0)) {
		$ZeroRFATentatives = $line['ZeroRFACount'];
		$TotalTentatives = $line['Total'];
		$ZeroRFAPercentage = $ZeroRFATentatives / ($TotalTentatives + $Approved) ;
	}
	DbEndQuery($result);  

	if ($member->HasLimit('NoNewLims'))
		$WordsDisplay .= "<p><br><br>Sorry. You are not allowed to submit any new limericks at the moment.<br><br></p>";
	else if (($Backlog>=$Limit) and ($Total>=50)) {
		$WordsDisplay .= str_replace(array('@@BACKLOG@@', '@@BACKLOGLIMIT@@'),
		array(sprintf("%.1f%%", $Backlog*100),($Limit*100)."%"),
		FormatDocument('New Limerick Limit', TRUE, TRUE));
	}
	else if (($ZeroRFAPercentage>=$ZeroRFALimit) and ($Total>=50)) {
		$WordsDisplay .= str_replace(array('@@ZERORFAPERCENTAGE@@', '@@ZERORFALIMIT@@'),
		array(sprintf("%.1f%%", $ZeroRFAPercentage*100), sprintf("%.1f%%", $ZeroRFALimit*100)),
		FormatDocument('New Limerick Limit 2', TRUE, TRUE));	
	}
	else {
		if (($PrimaryTotal>2*$PrimaryHeld) or ($PrimaryTotal<5) or ($PrimaryApproved>0))
			$WordsDisplay .= FormatLimerickEntryForm(FALSE);
		else {
			$WordsDisplay .= str_replace(array('@@HELDCOUNT@@', '@@LIMERICKCOUNT@@'),
			array($PrimaryHeld, $PrimaryTotal),
			FormatDocument('Held Limerick Limit', TRUE, TRUE));
		}
	}
	return $WordsDisplay;
}

?>