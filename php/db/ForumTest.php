<?php
define('IN_PHPBB', true);
$phpbb_root_path = '../forum/';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.'.$phpEx);

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);
//
// End session management
//
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Example sessions page</title>
<body>
<?php
if($userdata['session_logged_in'])
{
echo('Hi '.$userdata['username'].'! <a href="' . append_sid($phpbb_root_path . 'login.php?logout=true&redirect=..%2F') . '">Logout</a>');
}
else
{
?>

Hi Guest!<br />
<form action="<?php echo($phpbb_root_path); ?>login.php" method="post" enctype="multipart/form-data">
Username: <input type="text" name="username"><br />
Password: <input type="password" name="password"><br />
<input type="hidden" name="redirect" value="../test/ForumTest.php">
<input type="submit" value="login" name="login">
</form>
<?php
}

?>
</body>
</html>
