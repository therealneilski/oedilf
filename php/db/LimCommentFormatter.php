<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimCommentFormatter
{
	protected $workshopper;
	protected $isAdmin;
	protected $timeConverter;
	protected $lastViewedId;

	public function __construct($workshopper, $isAdmin, $timeConverter) 
	{
		$this->workshopper = $workshopper;
		$this->isAdmin = $isAdmin;
		$this->timeConverter = $timeConverter;
	}

    public function SetLastViewedComment($commentId)
    {
        $this->lastViewedId = $commentId;
    }

	public function BeginCommentFrame($html)
	{
		$html->BeginTable("", "CommentTable");
	}
	
	public function EndCommentFrame($html)
	{
		$html->EndTable();
	}
	
	public function FormatComment($html, $comment, $allowEdit)
	{
		$html->BeginTableRow();
		
		$html->BeginTableCell("", "CommentDetails");
		
		$html->BeginDiv("CommentAuthor");
        $html->Anchor("Comment".$comment['WorkshopId']);
		if ($comment['AuthorId']!=0)
			$html->Text(GetAuthorHtml($comment['AuthorId']));
		else $html->Text("Workshop Message");
		$html->EndDiv();

        $html->BeginDiv("smalltext");
        if (isset($this->lastViewedId) and
            ($comment['WorkshopId']>$this->lastViewedId))
        {
    		$html->Image("New.gif", "New comment");
            $html->Text(" ");
        }
		$html->Text("Posted: ".$this->timeConverter->LocalDateTime($comment['DateTime']));
		$html->EndDiv();
		
		if ($comment['EditDateTime'] <> NULL ) 
		{
			$html->BeginDiv("smalltext");
			$html->Text("[Last edit ".$this->timeConverter->LocalDateTime($comment['EditDateTime'])."]");
			$html->EndDiv();
		}
		$html->BeginDiv("smalltext");
		$html->Text("Comment #".$comment['WorkshopId']);
		$html->EndDiv();
		
		$html->EndTableCell();
		
		if ($comment['AuthorId']!=0)
			$html->BeginTableCell("", "CommentContent");
		else $html->BeginTableCell("", "WorkshopMessageContent");
		
		if ($allowEdit and ($this->isAdmin or ($comment['AuthorId']==$this->workshopper)))
		{
			$html->BeginDiv("CommentButtons");
			$html->Text(PurplePenLink("EditWorkshop=".$comment['WorkshopId'], "Edit", "Edit this comment"));
			$html->Text(RedXLink("DeleteWorkshop=".$comment['WorkshopId'], "Delete", "Delete this comment"));
			$html->EndDiv();
		}
		
		$html->Text(FormatQuickDocLinks(addbreaks($comment['Message'])));
		
		$html->EndTableCell();
		
		$html->EndTableRow();
	}
}
?>