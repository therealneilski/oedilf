<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }
define("RFAS_NEEDED", 4);		// RFAs needed before self-RFA allowed
define("LACE_TIME", 7*24*3600);		// 1-week waiting period
define("CFA_CONTRIBUTORS", 7);  // number of different opinions required to clear a CFA

function FormatWorkshopCommentBox($preset, $rows=10, $prompt="")
{
	if ($prompt)
		$script = "onfocus=\"if (this.value=='$prompt') this.value='';\"
			onblur=\"if (this.value=='') this.value='$prompt';\"";
	else $script = "";
	
	return sprintf('<textarea id="wstextid" name="workshoptext" cols="65" rows="%d" %s>%s</textarea>',
		$rows, $script, htmlentities($preset, ENT_QUOTES));
}

function HidePromptScript($prompt, $entryFieldId)
{
	$script = "onmouseover=\"var entryField = document.getElementById('$entryFieldId'); if (entryField.value=='$prompt') entryField.value='';\"";
	$script .= " onmouseout=\"var entryField = document.getElementById('$entryFieldId'); if (entryField.value=='') entryField.value='$prompt';\"";
	return $script;
}

function FormatTruncatedComment($comment)
{
    $truncated = substr(trim(strip_tags($comment), "\n\r "), 0, 40);
    $lastWord = strrchr($truncated, " ");
    if (strlen($lastWord)<=10)
        $truncated = substr($truncated, 0, -strlen($lastWord));
    return $truncated;
}

function CreatePrevNextActivityButtons($OriginalId) {
  global $member;
  $memberid = $member->GetMemberId();
  $WordsDisplay = "";
  
  // activity buttons
  $Sort = sprintf($_SESSION['$SortOrder'], $_SESSION['$SortAscDesc'], $_SESSION['$SortAscDesc']);

  // find prev, next VerseIds from result set
  $firstId = 0;
  $prevId = 0;
  $curId = 0;
  $nextId = 0;
  $lastId = 0;
  $thisCount = 0;
  $result = DbQuery("SELECT VerseId, MIN(DateTime) DateTime, (AuthorId<>$memberid)*AuthorId AuthorSort FROM DILF_Messages WHERE DstId=$memberid GROUP BY VerseId ORDER BY $Sort");

  while ($line = DbFetchArray($result)) {
	if ($firstId == 0)
		$firstId = $line['VerseId'];
	if ($curId == 0) {
		$thisCount++;
		if ($line['VerseId'] == $OriginalId)
			$curId = $OriginalId;
		else
			$prevId = $line['VerseId'];
	} else if ($nextId == 0) {
		$nextId = $line['VerseId'];
	}
	$lastId = $line['VerseId'];
  }
  $total = DbQueryRows($result);
  DbEndQuery($result);

  if ($curId > 0)
      $Activity = 'Activity ('.$thisCount.'/'.$total.'): ';
  else
      $Activity = 'Activity ('.$total.'): ';

  if ($firstId > 0) {
    $WordsDisplay .= $Activity.LightLinkSelf("Workshop=".$firstId, "&lt;&lt;", "Display the first workshop on my activity list");
    $Activity = '';
  }

  if ($curId > 0 and $prevId > 0) {
    $WordsDisplay .= $Activity.LightLinkSelf("Workshop=".$prevId, "&lt;", "Display the previous workshop on my activity list");
    $Activity = '';
  }

  if ($curId > 0) {
      if ($nextId > 0) {
        $WordsDisplay .= $Activity.LightLinkSelf(array("Workshop=".$nextId, "ClearActivity=".$OriginalId), "Del.+&gt;", "Delete activity messages on this limerick and then display the next on my list");
        $WordsDisplay .= LightLinkSelf("Workshop=".$nextId, "&gt;", "Display the next workshop on my activity list");
        $Activity = '';
      } else {
  	    $WordsDisplay .= $Activity.LightLinkSelf(array("Workshop=-1", "ClearActivity=".$OriginalId), "Del.", "Delete activity messages on this limerick and then display my activity page");
        $Activity='';
      }
  }

  if ($lastId > 0) {
    $WordsDisplay .= $Activity.LightLinkSelf("Workshop=".$lastId, "&gt;&gt;", "Jump to the last workshop on my activity list");
    $Activity = '';
  }

  return $WordsDisplay;
}

function FormatQuoteMacroButtons($html, $lim)
{
	if ($_SESSION['JavascriptWorks'])
	{
		$macros = new LimMacro('wstextid');
	
		$html->Text($macros->CreateQuoteLimerickButton($lim->Record['Verse']));
		if (trim($lim->Record['AuthorNotes'], "\n\r "))
			$html->Text(" ".$macros->CreateQuoteANButton($lim->Record['AuthorNotes']));  
	}
}

function FormatTextMacroButtons($html, $lim, $member)
{
	global $Configuration;
	if ($_SESSION['JavascriptWorks'])
	{
		$macros = new LimMacro('wstextid');
	
		if ($member->CanWorkshop() and ($member->Record('TextMacroButtons')=='On')) 
		{
			$html->Text(" ");
			$html->BeginDiv('trigger', 'display:inline;');
			$html->Text("Text Macros");
			$html->EndDiv();

			$html->BeginDiv();
			$html->Text("Add comment about:");
			
			$Letters = LastValidLetters($Configuration['AlphabetEnd']);    
			$macros->SetSubstitutions(array("@@AUTHOR@@", "@@LETTERS@@"), 
				array(GetAuthorHtml($lim->Record['PrimaryAuthorId']), $Letters));
			
			$macros->ParseMacrosFrom(FormatDocument("Buttons for WEs", FALSE, FALSE, FALSE));
			if ($member->CanAdministrate()) {
				$macros->ParseMacrosFrom(FormatDocument("Buttons for EiC", FALSE, FALSE, FALSE));
			}
			AddToHeader("LimAjaxJs", '<script type="text/javascript" src="LimAjax.js"></script>');
			$macros->FormatAjaxButtons($html, $lim->Record['PrimaryAuthorId']);
			$html->EndDiv();
		}
	}
}

function FormatWorkshopEntryForm($lim, $ShowSetToConfCheckbox, $AllowSelfRFA) {
	global $EditHelpMsg, $member;
	$WordsDisplay = '';
	$WordsDisplay .= '<table class="widetable"><tr><th>Workshop Comments:</th></tr><tr><td class="entryform">';

	if ($_POST['workshoptext']) {
		$workshoptext = stripslashes(CleanupHtml($_POST['workshoptext']));
		if ($member->CanWorkshop() and $_POST['CFACheckBox']) 
			$messageline['Message'] = FormatCFAComment($workshoptext);
		else
			$messageline['Message'] = $workshoptext;		
		$messageline['AuthorId'] = $member->GetMemberId();
		$messageline['DateTime'] = LimTimeConverter::FormatGMDateFromNow();
		$messageline['WorkshopId'] = "Preview";
		$WordsDisplay .= "<h3>This is a preview of your comment. It hasn't been saved.</h3>
		<p>It won't be saved until you click the 'Add Comment' button.</p>";
		$WordsDisplay .= "<table class='widetable'>".FormatSingleComment($messageline)."</table><br>";
	}
	else $workshoptext = '';
	
	$WordsDisplay .= sprintf('<form action="%s?Workshop=%d" name="wsc" method="post">', PHPSELF, $lim->VerseId);

	$html = new LimHtml();
	FormatQuoteMacroButtons($html, $lim);
	FormatTextMacroButtons($html, $lim, $member);
	$WordsDisplay .= $html->FormattedHtml(); 
	
	$WordsDisplay .= "<table class='StructureTable'>";
	$WordsDisplay .= "<tr><td>";
	
	if (!($member->CanWorkshop()))
		$WordsDisplay .= 'ENTER YOUR RESPONSE TO WORKSHOP COMMENTS HERE<br>';
	$WordsDisplay .= FormatWorkshopCommentBox($workshoptext);
	
	$WordsDisplay .= "</td><td style='padding-left:10px;'>";
	
	$html = new LimHtml();
	FormatAboutTheAuthor($html, $lim, $member);
	$WordsDisplay .= $html->FormattedHtml();
	
	$WordsDisplay .= "</td></tr>";
	$WordsDisplay .= "</table>";
	
	$WordsDisplay .= sprintf('<input type=hidden name="VerseId" value="%s">', $lim->VerseId);
	$WordsDisplay .= sprintf('<input type=hidden name="PosterId" value="%s">', $member->GetMemberId());

	$useChunk = false;
	$htmlChunk = new LimHtml();
	$htmlChunk->Text("When you add a comment, set: ");
	if ($AllowSelfRFA) {
		$htmlChunk->Checkbox("RFACheckBox", $_POST['RFACheckBox'], "RFACbx", "Ready for Final Approval");
		$htmlChunk->Text('<input type=hidden name="SelfRFA" value="1">');
		$htmlChunk->Text(" ");
		$useChunk = true;
	}
  
	if ($member->CanWorkshop()) {
		if (!$lim->IsMine()) {
			if (($lim->IsNew() or $lim->IsRevised() or
				$lim->IsTentative()) and !$lim->HasMyRFA()) 
			{
				$htmlChunk->Checkbox("RFACheckBox", $_POST['RFACheckBox'], "RFACbx", "Ready for Final Approval");
				$htmlChunk->Text(" ");
				$useChunk = true;
			}
			if ($lim->IsNew() or $lim->IsRevised()) 
			{
				$htmlChunk->Checkbox("TentCheckBox", $_POST['TentCheckBox'] or !$_POST['workshoptext'], "TentCbx", "Tentative");
				$htmlChunk->Text(" ");
				$useChunk = true;
			}
			if (!$lim->IsStored()) 
			{
				$htmlChunk->Checkbox("CFACheckBox", $_POST['CFACheckBox'], "CFACbx", "Call For Attention");
				$htmlChunk->Text(" ");
				$useChunk = true;
			}
			if ($ShowSetToConfCheckbox) 
			{
				$htmlChunk->Checkbox("ConfirmingCheckBox", $_POST['ConfirmingCheckBox'], "ConfCbx", "To Confirming");
				$htmlChunk->Text(" ");
				$useChunk = true;
			}
		}
	}
	$htmlChunk->LineBreak();
	if ($useChunk)
		$WordsDisplay .= $htmlChunk->FormattedHtml();
	$WordsDisplay .= SubmitButton("WorkshopButton", "Preview Comment");
	$WordsDisplay .= ' '.SubmitButton("WorkshopButton", "Add Comment");
	if ($_POST['workshoptext']) 
		$WordsDisplay .= ' '.SubmitButton("WorkshopButton", "Cancel");
	$WordsDisplay .= ' '.$EditHelpMsg;
	

	$WordsDisplay .= '</form>';
	$WordsDisplay .= '</td></tr></table>';

	return $WordsDisplay;
}

function FormatAboutTheAuthor($html, $lim, $member)
{
	$info = $lim->GetAuthorRecord();
	$authorId = $info['AuthorId'];
	
	if (($info['FeedbackStyle']!="0") and trim($info['FeedbackStyle']))
	{
		$html->BeginDiv("smalltext");
		$html->Text("Author's Preference:");
		$html->EndDiv();
		$html->Text(FormatFeedbackStyle($info['FeedbackStyle']));
	}
	else {
		$html->BeginDiv("smalltext");
		$html->Text("About the author:");
		$html->EndDiv();
	}
	$html->BeginDiv("smalltext");
	$timeConv = new LimTimeConverter($member->Record('TimeZone'));
	$html->Text("Last login: ".$timeConv->LocalDateTimeFromStamp($info['AccessTime']));
	$html->LineBreak();
	$html->Text("First submission: ".$timeConv->LocalDateTime($info['FirstLimDate']));
	$html->LineBreak();
	$html->Text("Limericks submitted: ".($info['ApprovedCount']+$info['HeldCount']+$info['NewCount']+
		$info['ConfirmingCount']+$info['RevisedCount']+$info['TentativeCount']+$info['UntendedCount']));
	$html->LineBreak();
	$html->Text("Limericks approved: ".$info['ApprovedCount']);
    if ($info['BornIn'] or $info['LivingIn'])
    {
    	$html->LineBreak();
        $flags = new LimFlags($info);
        $flags->CountryDescription($html);
    }
	$html->EndDiv();
}

function FormatSingleComment($comment)
{
	global $member;
	$html = new LimHtml();
  	$timeFormatter = new LimTimeConverter($member->Record('TimeZone'));
	$commentFormatter = new LimCommentFormatter($member->GetMemberId(), 
		$member->CanAdministrate(), $timeFormatter);
	$commentFormatter->BeginCommentFrame($html);
	$commentFormatter->FormatComment($html, $comment, false);
	$commentFormatter->EndCommentFrame($html);
		
	return $html->FormattedHtml();
}

function FormatWorkshopDeleteForm($WorkshopId) {
	global $member;
	$WordsDisplay = '';

	$result = DbQuery(sprintf("SELECT * FROM DILF_Workshop WHERE WorkshopId='%d'", $WorkshopId));
	$NumMessages = DbQueryRows($result);

	if ($NumMessages>0)
	{
		$messageline = DbFetchArray($result) ;		
		$WordsDisplay .= FormatSingleComment($messageline);
	}
	DbEndQuery($result);
	
	$WordsDisplay .= sprintf('<form action="%s?Workshop=%d" method="post">', PHPSELF, $messageline['VerseId']);
	$WordsDisplay .= sprintf('<input type=hidden name="VerseId" value="%s">', $messageline['VerseId']);
	$WordsDisplay .= sprintf('<input type=hidden name="WorkshopId" value="%s">', $WorkshopId);
	$WordsDisplay .= sprintf('<input type=hidden name="PosterId" value="%s">', $member->GetMemberId());
	$WordsDisplay .= '<br>'.SubmitButton("WorkshopButton", "Delete Comment");
	$WordsDisplay .= ' '.SubmitButton("WorkshopButton", "Cancel");
	$WordsDisplay .= '</form>';

	return $WordsDisplay;
}

function FormatWorkshopEditForm($lim, $WorkshopId) {
  global $EditHelpMsg, $member;
  $WordsDisplay = '';

  $result = DbQuery(sprintf("SELECT * FROM DILF_Workshop WHERE WorkshopId='%d'", $WorkshopId));
  $NumMessages = DbQueryRows($result);

  if ($NumMessages>0) {
    $messageline = DbFetchArray($result) ;
    $WordsDisplay .= '<table class="widetable"><tr><th>Editing Workshop Comment #'.$WorkshopId;
    if ($messageline['AuthorId']!=$member->GetMemberId())
    	$WordsDisplay .= ' by '.GetAuthorHtml($messageline['AuthorId']);
    $WordsDisplay .= ':</th></tr><tr><td class="entryform">';

    $WordsDisplay .= sprintf('<form action="%s?Workshop=%d" NAME="wse" method="post">', PHPSELF, $lim->VerseId);

	$html = new LimHtml();
	FormatQuoteMacroButtons($html, $lim);
	FormatTextMacroButtons($html, $lim, $member);
	$WordsDisplay .= $html->FormattedHtml();
	 
	$WordsDisplay .= "<table class='StructureTable'>";
	$WordsDisplay .= "<tr><td>";
	
    $WordsDisplay .= FormatWorkshopCommentBox($messageline['Message']);
    $WordsDisplay .= sprintf('<input type=hidden name="VerseId" value="%s">', $lim->VerseId);
    $WordsDisplay .= sprintf('<input type=hidden name="WorkshopId" value="%s">', $WorkshopId);
    $WordsDisplay .= sprintf('<input type=hidden name="PosterId" value="%s">', $member->GetMemberId());
    $WordsDisplay .= '<br>'.SubmitButton("WorkshopButton", "Edit Comment");
    $WordsDisplay .= ' '.SubmitButton("WorkshopButton", "Cancel");
    $WordsDisplay .= ' '.$EditHelpMsg;
    
	$WordsDisplay .= "</td><td style='padding-left:10px;'>";
	
	$html = new LimHtml();
	FormatAboutTheAuthor($html, $lim, $member);
	$WordsDisplay .= $html->FormattedHtml();
	
	$WordsDisplay .= "</td></tr>";
	$WordsDisplay .= "</table>";    
    $WordsDisplay .= '</form>';
    $WordsDisplay .= '</td></tr></table>';
  }
  else $WordsDisplay .= '<p>Comment not found</p>';
  DbEndQuery($result);

  $html = new LimHtml();
  FormatWorkshopHistory($html, $lim, $member);
  $WordsDisplay .= $html->FormattedHtml();
  
  return $WordsDisplay;
}

function FormatTruncatedWorkshopOptions($OriginalId) {
  $WordsDisplay = '';
  $WordsDisplay .= '<p><span class="warninghighlight">Long Workshop Truncated</span><br >';
  $WordsDisplay .= LinkSelf(array("Workshop=$OriginalId", "WholeWorkshop=$OriginalId"), "Show me all of this workshop")."<br >";
  $WordsDisplay .= LinkSelf("Profile=Edit", "Let me change the WS Comment Display Limit in my profile");
  $WordsDisplay .= '</p>';
  return $WordsDisplay;
}

function ApproveLimerick($VerseId) {
  // make sure the limerick number is up to date (probably only needed the very first time this runs)
  $result = DbQuery("UPDATE DILF_Settings 
  	SET LimerickNumber = (SELECT MAX(LimerickNumber) FROM DILF_Limericks)");
  $result = DbQuery("UPDATE DILF_Settings S, DILF_Limericks L 
	SET L.LimerickNumber= S.LimerickNumber,
	L.State='approved',
	S.LimerickNumber= S.LimerickNumber+1
	WHERE L.VerseId=$VerseId");

  // now get the number that has been assigned
  $result = DbQuery("SELECT LimerickNumber FROM DILF_Limericks WHERE VerseId=$VerseId");
  if ($line = DbFetchArray($result)) {
    $NewNum = $line['LimerickNumber'];
  }
  else $NewNum = 0;
  DbEndQuery($result);

  TouchLimerick($VerseId);
  NotifyOfStateChanges($VerseId, 'Approved');
  AuditLog($VerseId, 'Approve Limerick');

  RSSFeed();
  $line=GetVerse($VerseId);
  RSSAuthorFeed($line['PrimaryAuthorId']);
  if ($line['SecondaryAuthorId'])
    RSSAuthorFeed($line['SecondaryAuthorId']);
  $Category = $line['Category'];

  $Author1 = $line['PrimaryAuthorId'];
  $Author2 = $line['SecondaryAuthorId'];

  $ApprovedCount = 0;
  $PrimaryCount = 0;
  $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE State='approved' AND PrimaryAuthorId='%s'", $Author1));
  while ($line = DbFetchArray($result)) {
    $PrimaryCount++;
    if ($line['SecondaryAuthorId']==0) $ApprovedCount++;
    else $ApprovedCount +=0.5;
  }
  DbEndQuery($result);
  $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE State='approved' AND SecondaryAuthorId='%s'", $Author1));
  while ($line = DbFetchArray($result)) {
    $ApprovedCount +=0.5;
  }
  DbEndQuery($result);

  AuditLog($VerseId, 'Approve Limerick - AutoTitleSet');
  AutoTitleSet($Author1, $ApprovedCount, $PrimaryCount);
  UpdateStats();
  UpdateAuthorStats($Author1);
  if ($Author2) UpdateAuthorStats($Author2);

  AuditLog($VerseId, 'Approve Limerick - DbUpdate(Type=>Defined)');
  DbUpdate("DILF_Words", array("Type"=>"Defined"), 'VerseId='.$VerseId, 1000);

  AuditLog($VerseId, 'Approve Limerick - CacheNewApprovedLims');
  require_once "LimCache.php";
  CacheNewApprovedLims();

  if ($Category == "normal") {
    AuditLog($VerseId, "Approve Limerick - Category = {$Category}");
    require_once "LimTweet.php";
    AuditLog($VerseId, 'Approve Limerick - TweetLimerick');
    $result = TweetLimerick('A' + $NewNum);
    AuditLog($VerseId, 'Approve Limerick - FacebookLimerick');
    $result = FacebookLimerick('A' + $NewNum);
  } else {
    AuditLog($VerseId, 'Not sharing to Twitter/Facebook because Status == "' . $line['Category'] . '"');
  }
  
  return $NewNum;
}

function AddWorkshopComment($PosterId, $VerseId, $workshoptext, &$commentId) {
	$OriginalId = GetOriginalId($VerseId);
	DbInsert("DILF_Workshop", array("VerseId"=>$VerseId, "AuthorId"=>$PosterId, 
		"DateTime"=>LimTimeConverter::FormatGMDateFromNow(), 
        "Message"=>DbEscapeString($workshoptext),
		"PosterIP"=>getenv("REMOTE_ADDR"), "OriginalId"=>$OriginalId));
	$commentId = DbLastInsert();
	TouchLimerick($VerseId);
	RSSWorkshopFeed($OriginalId);
	return CheckForExpiredCFA($VerseId, $OriginalId);
}

function DeleteWorkshopComment($commentId) {
	global $member, $Configuration;
	$WordsDisplay = '';
	
	$result = DbQuery("SELECT * FROM DILF_Workshop WHERE WorkshopId=$commentId");
	if (DbQueryRows($result)<1) 
		die(sprintf('Workshop comment #%s not found<br>', $commentId));
	$messageline = DbFetchArray($result) ;
	DbEndQuery($result);

	if ($member->CanEdit() or ($messageline['AuthorId']==$member->GetMemberId()))
	{
		$result = DbQuery("DELETE FROM DILF_Workshop WHERE WorkshopId=$commentId");
		AuditLog($messageline['VerseId'], 'Delete Comment: '.$messageline['AuthorId'].' '.
            FormatTruncatedComment($messageline['Message']));
		SetReload('Workshop='.$messageline['VerseId']);
		$WordsDisplay .= '<p>Comment deleted.</p>';
	}
	else
		$WordsDisplay .= '<p>You do not have the authority to delete this comment</p>';

	$WordsDisplay .= '<p>' . WorkshopLink($VerseId) . '</p>';
	return $WordsDisplay;
}

function ProcessWorkshop(&$WordsDisplay, &$WSPageTitle) {
  global $member, $Configuration;
  $processed = TRUE;
  $WSPageTitle = '';

  if (($_POST['WorkshopButton']=='Cancel') or ($_POST['LimerickButton']=='Cancel')) {
	$_POST['workshoptext'] = ''; // stop further comment preview processing
  }
  
  if (isset($_GET['ApprovalEmail'])) {
    $WSPageTitle = 'Approval Sent';
    $WordsDisplay .= SendApprovalRequest($_GET['ApprovalEmail']);
    $WordsDisplay .= WorkshopLink($_GET['ApprovalEmail']);
  }
  else if (isset($_GET['Confirming'])) {
    SetToConfirming($_GET['Workshop'], $_GET['Confirming']);
    SetRedirect('Workshop='.$_GET['Workshop']);
  }
  else if (isset($_GET['RejectionEmail'])) {
    $WSPageTitle = 'Rejection Sent';
    $WordsDisplay .= FormatRejectionEmail($_GET['RejectionEmail']);
  }
  else if (isset($_POST['WorkshopButton']) and ($_POST['WorkshopButton']=='Delete Comment')) {  
  	// ---- confirmed delete a workshop comment
    $WSPageTitle = 'Comment Deleted';
	$WordsDisplay .= DeleteWorkshopComment($_POST['WorkshopId']+0);
  }
  else if (isset($_GET['DeleteCFAComment'])) {
    $WSPageTitle = 'CFA Comment Deleted';
	$WordsDisplay .= DeleteWorkshopComment($_GET['DeleteCFAComment']+0);  
  }
  else if (isset($_GET['DeleteWorkshop'])) { // ---- delete a workshop comment
    $WSPageTitle = 'Delete Comment';
    $WorkshopId = stripslashes($_GET['DeleteWorkshop']);
    $WordsDisplay .= FormatWorkshopDeleteForm($WorkshopId);
  }
  else if (isset($_POST['WorkshopButton']) and ($_POST['WorkshopButton']=='Edit Comment')) {
  	// ---- confirmed edit a workshop comment
    $WSPageTitle = 'Comment Edited';
    $VerseId = $_POST['VerseId'];
    $WorkshopId = $_POST['WorkshopId'];
    $PosterId = $_POST['PosterId'];
    $workshoptext = stripslashes(CleanupHtml($_POST['workshoptext']));

    $result = DbQuery(sprintf("SELECT * FROM DILF_Workshop WHERE WorkshopId=%d", $WorkshopId));
    if (DbQueryRows($result)<1) die(sprintf('Workshop comment #%s not found<br>', $WorkshopId));
    $messageline = DbFetchArray($result) ;
    DbEndQuery($result);

    if ($member->CanEdit() or ($messageline['AuthorId']==$member->GetMemberId())) {
	  $result = DbQuery(sprintf("UPDATE DILF_Workshop SET Message = '%s', PosterIP='%s', EditDateTime = '%s' WHERE WorkshopId=%d",
	     DbEscapeString($workshoptext), getenv("REMOTE_ADDR"), LimTimeConverter::FormatGMDateFromNow(), $WorkshopId));
	  
      TouchLimerick($VerseId);
      RSSWorkshopFeed(GetOriginalId($VerseId));
      AuditLog($VerseId, 'Edit Comment: '.$messageline['AuthorId'].' '.FormatTruncatedComment($workshoptext));
      SetReload('Workshop='.$VerseId);

      NotifyOfWorkshopComment($VerseId, 'Edit: '.FormatTruncatedComment($workshoptext));
    }
    else
      $WordsDisplay .= '<p>You do not have the authority to edit this comment</p>';

    $WordsDisplay .= '<p>Comment edited.</p>';
    $WordsDisplay .= '<p>' . WorkshopLink($VerseId) . '</p>';
  }
  else if (isset($_GET['EditWorkshop'])) { // ---- edit a workshop comment
    $WSPageTitle = 'Edit Comment';
    $WorkshopId = $_GET['EditWorkshop']+0;
    $result = DbQuery(sprintf('SELECT * FROM DILF_Workshop WHERE WorkshopId="%d"', $WorkshopId));
    if (DbQueryRows($result)<1) die(sprintf('Workshop comment #%s not found<br>', $WorkshopId));
    $messageline = DbFetchArray($result) ;
    DbEndQuery($result);

	$lim = new LimLimerick($messageline['VerseId'], $member->GetMemberId());
	$WordsDisplay .= FormatWorkshopEditForm($lim, $WorkshopId);
  }
  else if (isset($_POST['WorkshopButton']) and ($_POST['WorkshopButton']=='Add Comment')) {
  	// ---- add a workshop comment
    $WSPageTitle = 'Comment Added';
    $VerseId = $_POST['VerseId'];
    $OriginalId = GetOriginalId($VerseId);
    $PosterId = $_POST['PosterId'];

    $workshoptext = stripslashes(CleanupHtml($_POST['workshoptext']));

    $result = DbQuery(sprintf('SELECT * FROM DILF_Limericks WHERE VerseId="%d"', $VerseId));
    $line = DbFetchArray($result) ;
    DbEndQuery($result);
    $mine = (($member->GetMemberId()==$line['PrimaryAuthorId']) or ($member->GetMemberId()==$line['SecondaryAuthorId']));

    if ($workshoptext=='') {
       $WordsDisplay .= '<p>Before pressing the Add Comment button, you need to have entered some text in the comment box above it.</p>';
    }
    else if ($member->CanWorkshop() or $mine) {
	  $commentId = 0;
      if ($member->CanWorkshop() and $_POST['CFACheckBox']) {
        $WordsDisplay .= AddWorkshopComment($PosterId, $VerseId, FormatCFAComment($workshoptext), $commentId);
      }
      else
        $WordsDisplay .= AddWorkshopComment($PosterId, $VerseId, $workshoptext, $commentId);
      if ($_POST['RFACheckBox'])
        AddRFA($line['OriginalId'], $VerseId, $_POST['SelfRFA'], $line);
      if ((!$mine) and ($_POST['TentCheckBox'] or $_POST['RFACheckBox'])) {
        ChangeLimerickState($VerseId, $line['OriginalId'], $line['State'], 'tentative', $mine);
        UpdateAuthorStats($line['PrimaryAuthorId']);
        if ($line['SecondaryAuthorId']) UpdateAuthorStats($line['SecondaryAuthorId']);
        UpdateStats();
      }
      
      if ((!$mine) and ($line['WorkshopState']=='none')) SetWorkshopState($VerseId, 'started');
      if (!$mine) AddWorkshopMonitor($OriginalId);

      AuditLog($VerseId, 'Add Comment: '.FormatTruncatedComment($workshoptext));

      if ($member->CanWorkshop() and $_POST['CFACheckBox']) {
        $WordsDisplay .= "Your <b>Call For Attention</b> comment has been posted but it's not too late to cancel and delete it. Proceeding will send notifications to a lot of workshoppers. Are you sure you want to Call For Attention? ";
        $WordsDisplay .= "<br>".DarkButton("CallForAttention=".$VerseId, "Call For Attention", "Cry 'Havoc,' and let slip the dogs of war");
        $WordsDisplay .= " ".DarkButton("DeleteCFAComment=".$commentId, "Cancel", "On second thoughts, scratch that CFA.");
      }
      else
        SetRedirect('Workshop='.$VerseId);
        
      NotifyOfWorkshopComment($VerseId, FormatTruncatedComment($workshoptext));

      $WordsDisplay .= '<p>Comment Added.</p>';
      
      if ($_POST['ConfirmingCheckBox']) {
        SetToConfirming($OriginalId, $VerseId);
      }
    }
    else
      $WordsDisplay .= '<p>You do not have the authority to add a comment</p>';
    $WordsDisplay .= '<p>' . WorkshopLink($VerseId) . '</p>';

  }
  else if ($member->CanAdministrate() and isset($_GET['Kill'])) {
    $WordsDisplay .= AnnihilateLimerickVersions($_GET['Kill']);
    $WordsDisplay .= LinkSelf(array("Annihilate=1", "Workshop=".$_GET['NextWorkshop']), "Next");
  }
  else if (isset($_GET['Workshop'])) { // ---------------------------------------------- workshop a limerick
    if (isset($_GET['ClearActivity'])) { // delete specified messages
      $DeleteId = stripslashes($_GET['ClearActivity']);
      $result = DbQuery(sprintf("DELETE FROM DILF_Messages WHERE DstId=%d AND VerseId=%d", $member->GetMemberId(), $DeleteId));
      AuditLog($DeleteId, 'Deleted '.DbAffectedRows().' notification message(s): AuthorId='.$member->GetMemberId());
    }

    $VerseId = stripslashes($_GET['Workshop'])+0;

    if ($VerseId > 0)
      $WordsDisplay .= ProcessWorkshopPage($VerseId, $WSPageTitle);
    else
      SetReload("Action=Activity");
  }
  else $processed = FALSE;

  return $processed;
}

function FormatCFAComment($workshoptext) {
	return '<h3>##### Call For Attention #####</h3><strong>'.$workshoptext.
        '</strong><br><br>NB Once the CFA has been completed, deleting this comment will <b>not</b> retract the messages sent to all the other workshoppers. The attention messages will be removed when '.CFA_CONTRIBUTORS.' different authors have contributed to the discussion.';
}

function ProcessedWorkshopUpdateAndReload($lim, $member)
{
	// Monitoring and RFAs - there can be multiple parameter matches
	$reloadNeeded = false;
	
	if ($member->CanContribute() and isset($_GET['Monitor']))
	{
  		AddRemoveMonitor($lim->OriginalId);
  		$reloadNeeded = true;
	}
	if (($lim->IsMine() or $member->CanWorkshop()) and isset($_GET['RFA']))
	{
		AddRemoveRFA($lim);
  		$reloadNeeded = true;
	}
	if ($member->CanWorkshop() and isset($_GET['Curtain']))
	{
		AddRemoveCurtain($lim->VerseId);
  		$reloadNeeded = true;
	}
	if (isset($_GET['WholeWorkshop'])) {
		$_SESSION['WholeWorkshop'] = $lim->OriginalId;
		SetRedirect("Workshop=".$lim->OriginalId);
  		$reloadNeeded = true;
	}
	if (isset($_GET['LimState']))
	{
		if ($lim->IsConfirming() and ($_GET['LimState']=='tentative'))
			AddWorkshopComment(0, $lim->VerseId, htmlspecialchars($member->GetMemberName(), ENT_QUOTES).
				' stopped the countdown and returned the limerick to tentative.', $commentId);
		ChangeLimerickState($lim->VerseId, $lim->OriginalId, $lim->Record['State'], $_GET['LimState'], 
			$lim->IsMine(), $lim->Record['HeldById']);
		UpdateAuthorStats($lim->Record['PrimaryAuthorId']);
		if ($lim->Record['SecondaryAuthorId'])
			UpdateAuthorStats($lim->Record['SecondaryAuthorId']);
		UpdateStats();
		
		if (isset($_GET['NextWorkshop']))
		{
			SetRedirect("Workshop=".$_GET['NextWorkshop']);
			$reloadNeeded = true;
		}
		else if (! isset($_GET['ClearRFAs'])) // don't do the redirect if we're going to need a confirmation step displayed
		{
			SetRedirect("Workshop=".$lim->OriginalId);
			$reloadNeeded = true;
		}
	}
	if (($member->CanWorkshop() or $lim->IsMine()) and isset($_GET['ClearRFAs']) and isset($_GET['ClearConfirmed']))
	{
		// notify RFAers
		NotifyOfRFAChanges($lim->VerseId, 'Your RFA has been cleared.');
		$AuthorList = htmlspecialchars(implode("; ", $lim->GetRFAList()), ENT_QUOTES);
		$result = DbQuery(sprintf("DELETE FROM DILF_RFAs WHERE OriginalId = '%d'", $lim->OriginalId));
		UpdateLimerickRFAs($lim->OriginalId);
		AuditLog($_GET['ClearRFAs'], 'Cleared RFAs');
		AddWorkshopComment(0, $_GET['ClearRFAs'], 
			htmlspecialchars($member->GetMemberName(), ENT_QUOTES)." cleared all RFAs ($AuthorList)", $commentId);
		SetRedirect("Workshop=".$lim->OriginalId);
		$reloadNeeded = true;
	}	
	return $reloadNeeded;
}

function FormatLimStateDetails($html, $lim, $member)
{
	$nameList = array();
	foreach ($lim->GetAuthorRFAList() as $name)
		$nameList[] = "<b>".htmlspecialchars($name, ENT_QUOTES)."</b>";
	foreach ($lim->GetNonAuthorRFAList() as $name)
		$nameList[] = htmlspecialchars($name, ENT_QUOTES);
	
	$html->BeginDiv("smalltext");
	if (count($nameList))
	{
		if (!$lim->IsApproved())
			$html->Text("Considered Ready for ");
		$html->Text("Final Approval by (".count($nameList)."):");
		foreach ($nameList as $name)
		{
			$html->Text(" ".$name);
			$html->Image("GreenTick.gif", "", 12, 12, "Tick");
		}
		$html->LineBreak();
	}
	if ($lim->IsApproved() and ($lim->Record['ApprovingEditor']))
	{
		$html->Text('Approved by '.GetAuthorHtml($lim->Record['ApprovingEditor']).'.');
		$html->LineBreak();
	}
	if ($lim->IsConfirming())
	{
		if ($lim->Record['ApprovingEditor'])
			$html->Text('Approval started by '.GetAuthorHtml($lim->Record['ApprovingEditor']).'. ');
		if ($lim->Record['StateDateTime']>'0000-00-00 00:00:00')
		{
			$timeConv = new LimTimeConverter($member->Record('TimeZone'));
			$html->Text('Confirmation delay ends '.
				$timeConv->LocalDateTime($lim->Record['StateDateTime'], LACE_TIME));
		}
		if ($lim->Record['LinkCode'])
			$html->Text(' <b>LACE sent.</b>');
		$html->LineBreak();
	}	
	if ($lim->IsHeld() and ($lim->Record['HeldById']<>0))
	{
		$html->Text('Held by '.GetAuthorHtml($lim->Record['HeldById']));
		$html->LineBreak();
	}
	$html->EndDiv();
}

function FormatStateChangeControls($html, $lim, $member, $nextSearchId)
{
	$isEmpty = true;
	$htmlChunk = new LimHtml();

	$htmlChunk->BeginDiv();
	$htmlChunk->Text('Set state to: ');
	if (AllowedStateChange($lim->Record['State'], 'held', $lim->IsMine(), $lim->Record['HeldById']))
	{
		$htmlChunk->LightLinkSelf(array("Workshop=".$lim->VerseId, "LimState=held"), 
			"Held", "Move this limerick out of the public eye while working on it.");
		$htmlChunk->Text(" ");
		$isEmpty = false;
	}
	
	if (($lim->IsHeld() or $member->CanWorkshop()) and 
		AllowedStateChange($lim->Record['State'], 'tentative', $lim->IsMine(), $lim->Record['HeldById']))
	{
		if (($lim->RFACount()>0) and $lim->IsRevised()) 
		{
			$htmlChunk->LightLinkSelf(array("Workshop=".$lim->VerseId, "LimState=tentative"), 
				"Tentative (edits were minor)", "Make this limerick publicly viewable (without clearing RFAs)");
			$htmlChunk->Text(" ");
			$htmlChunk->LightLinkSelf(array("Workshop=".$lim->VerseId, "LimState=tentative", "ClearRFAs=".$lim->VerseId),
				"Tentative (clear RFAs)", "Make this limerick publicly viewable (clearing RFAs in the process)");
			$htmlChunk->Text(" ");
		}
		else
		{
			if ($lim->IsConfirming())
				$htmlChunk->LightLinkSelf(array("Workshop=".$lim->VerseId, "LimState=tentative"), 
					"Stop Countdown", "Back to Tentative for the moment");
			else
				$htmlChunk->LightLinkSelf(array("Workshop=".$lim->VerseId, "LimState=tentative"),
					"Tentative", "Make this limerick publicly viewable");
			$htmlChunk->Text(" ");
			if ($lim->IsNew() and $nextSearchId > 0)
			{
				$htmlChunk->LightLinkSelf(array("Workshop=".$lim->VerseId, 
					"LimState=tentative", "NextWorkshop=".$nextSearchId), "Tent.+>",
					"Set this limerick to tentative and then display the next workshop.");
				$htmlChunk->Text(" ");
			}
		}
		$isEmpty = false;
	}
	
	if (AllowedStateChange($lim->Record['State'], 'excluded', $lim->IsMine()))
	{
		$htmlChunk->LightLinkSelf(array("Workshop=".$lim->VerseId, "LimState=excluded"), "Bone Yard");
		$htmlChunk->Text(" ");
		$isEmpty = false;
	}
	
	if (AllowedStateChange($lim->Record['State'], 'untended', $lim->IsMine()))
	{
		$htmlChunk->LightLinkSelf(array("Workshop=".$lim->VerseId, "LimState=untended"), "Untended");
		$htmlChunk->Text(" ");
		$isEmpty = false;
	}	  
	
	if (AllowedStateChange($lim->Record['State'], 'stored', $lim->IsMine()))
	{
		$htmlChunk->LightLinkSelf(array("Workshop=".$lim->VerseId, "LimState=stored"), "Bottom Drawer");
		$htmlChunk->Text(" ");
		$isEmpty = false;
	}
	      
	$htmlChunk->EndDiv();
	if (!$isEmpty)
		$html->Text($htmlChunk->FormattedHtml());	
}

function FormatMemberControls($html, $lim, $member)
{
	if ($member->CanEdit() or $lim->IsMine()) 
	{
		if ($member->CanEdit() or (!$lim->IsApproved() and !$lim->IsConfirming()))
		{
			$html->LightLinkSelf("Revise=".$lim->VerseId, "Revise Limerick");
			$html->Text(" ");
		}
	}

	if (!$lim->IsApproved() and !$lim->IsStored() and 
		!$lim->IsExcluded() and (!$lim->HasMyRFA()) and 
		(($lim->NonAuthorRFACount()>=RFAS_NEEDED) or !$lim->IsMine()))
	{
		$rfaParams = array("Workshop=".$lim->OriginalId, "RFA=On", "RFAVerseId=$lim->VerseId");
		if ($lim->IsMine()) $rfaParams[] = 'SelfRFA=1';
		if (($lim->IsNew()) or ($lim->IsRevised()))
			$rfaParams[] = "LimState=tentative";
		$html->LightLinkSelf($rfaParams, "Ready for Final Approval",
			"Add my RFA to this limerick");
		$html->Text(" ");
	}
	
	if ($lim->HasMyRFA() and !$lim->IsApproved())
	{
		$html->LightLinkSelf(array("Workshop=".$lim->OriginalId, "RFA=Off"), "DeRFA", 
			"Remove my RFA from this limerick");
		$html->Text(" ");
	}

	if (($member->CanWorkshop() or $lim->IsMine()) and ($lim->RFACount()>0) and !$lim->IsApproved())
	{
		$html->LightLinkSelf(array("Workshop=".$lim->VerseId, "ClearRFAs=".$lim->VerseId),
			"Clear RFAs", "This limerick has been significantly changed since the RFAs were added. Clear them all.");
		$html->Text(" ");
	}
		
	if ($member->CanWorkshop())
	{
		if ($lim->Record['Category']=='normal')
			$html->LightLinkSelf(array("Workshop=".$lim->VerseId, "Curtain=On"), 
				"Curtain", "Hide this limerick from prudish public");
		else
			$html->LightLinkSelf(array("Workshop=".$lim->VerseId, "Curtain=Off"), 
				"un-Curtain", "Make this limerick viewable by any site visitors");
		$html->Text(" ");
	}

	if (!$lim->IsMine())
	{
		if ($lim->HasMyMonitor())
			$html->LightLinkSelf(array("Workshop=$lim->OriginalId", "Monitor=Off"), 
				"Stop notifying me of workshop activity");
		else
			$html->LightLinkSelf(array("Workshop=$lim->OriginalId", "Monitor=On"),
				"Notify me of workshop activity");
		$html->Text(" ");
	}

	if ($lim->IsPrimarilyMine() and $lim->IsApproved())
	{
		if ($lim->Record['ShowcasePriority']>0)
			$html->LightLinkSelf(array("ShowcaseAction=Remove", "ShowcaseRemove=".$lim->VerseId),
				"Remove from Showcase");
		else if ($member->CanShowcase())
			$html->LightLinkSelf(array("ShowcaseAction=Add", "ShowcaseAdd=".$lim->VerseId),
				"Add to Showcase");
	}
}

function FormatWSAdminControls($html, $lim, $member, $nextSearchId)
{
	$html->BeginDiv();
	if ($member->CanAdministrate())
	{
		$html->Text('Admin: ');
		$html->LightLinkSelf("RejectionEmail=".$lim->VerseId, "Compose Rejection Email");
		$html->Text(" ");
		
		if ($_GET['Annihilate'])
		{
			if ($nextSearchId > 0)
				$html->LightLinkSelf(array("Kill=$lim->OriginalId", "NextWorkshop=$nextSearchId"),
					"Annihilate This Limerick +>");
			else $html->LightLinkSelf(array("Kill=$lim->OriginalId", "NextWorkshop=0"), 
					"Annihilate This Limerick");
		}
		else
			$html->LightLinkSelf(array("Workshop=$lim->OriginalId", "Annihilate=1"),
				"Kill Mode", "This limerick deserves to die. Enable the Annihilate button.");
		$html->Text(" ");
	}
	if (AllowedStateChange($lim->Record['State'], 'approved', $lim->IsMine()))
	{
		$html->LightLinkSelf(array("ApprovalEmail=".$lim->VerseId), "Send Approval Request Email");
		$html->Text(" ");
		$html->LightLinkSelf(array("Workshop=".$lim->VerseId, "LimState=approved"), "Approved");
	}
			
	$html->EndDiv();
}

//----------------------------------------------------------- Here's the guts of workshopping
function ProcessWorkshopPage($VerseId, &$WSPageTitle) {
	global $member;
	$WordsDisplay = '';

	$lim = new LimLimerick($VerseId, $member->GetMemberId());

	// In here we process some of the additional parameters that can be passed with the workshop id.
	// These actions will do their adjustments and then cause a reload with the extra parameters excluded
	if (ProcessedWorkshopUpdateAndReload($lim, $member))
		return $WordsDisplay;
		
	RSSInitializeWorkshopFeedLink($lim->OriginalId);

	$words = $lim->GetWordList();
	$WSPageTitle = implode('; ', $words->WordList());
	if (strlen($WSPageTitle)>70) $WSPageTitle = substr($WSPageTitle, 0, 70)."...";
   
	if ($lim->IsApproved()) $WSPageTitle .= ' #'.$lim->Record['LimerickNumber'];
	else $WSPageTitle .= ' #T'.$lim->Record['OriginalId'];
	$WSPageTitle = 'Workshop - '.htmlspecialchars($WSPageTitle, ENT_QUOTES);

	if (($member->CanWorkshop() or $lim->IsMine()) and 
		isset($_GET['ClearRFAs']) and !isset($_GET['ClearConfirmed']))
	{
		$WordsDisplay .= "<p><br><br>Are you sure you want to clear all the RFAs from this limerick?</p>";
		$WordsDisplay .= "<p>".LightLinkSelf(array('Workshop='.$lim->OriginalId, 'ClearRFAs='.$_GET['ClearRFAs'],
			'ClearConfirmed=1'), "Yes, clear them.");
		$WordsDisplay .= ' '.LightLinkSelf('Workshop='.$lim->OriginalId, "No.");
		$WordsDisplay .= "<br><br></p>";
	}
	
	if ($member->CanContribute() or $lim->IsMine())
	{
		$WordsDisplay .= FormatLimerickBlock($lim->Record, FALSE, TRUE);
 
		$htmlChunk = new LimHtml();
		FormatLimStateDetails($htmlChunk, $lim, $member);
		
		$WordsDisplay .= $htmlChunk->FormattedHtml();
		// 
		
		// Allow confirmation from within workshop
		$TimeLimit = LimTimeConverter::FormatGMDateFromNow(-LACE_TIME);
		
		if ($lim->IsMine() and $lim->IsConfirming() and ($lim->Record['StateDateTime']<$TimeLimit))
		{
			if ($lim->HasConfirmingMessage())
				$WordsDisplay .= sprintf('<p>You have confirmed that this is the final version of your limerick.</p>');
			else $WordsDisplay .= FormatWorkshopApproval($lim->VerseId);
		}

		if (($member->CanWorkshop() or $lim->IsMine()) and !$_POST['workshoptext'])
		{
			if ($member->CanWorkshop() or AllowedStateChange($lim->Record['State'], 'tentative', $lim->IsMine()))
			{
				// collect IDs of first, prev, next, last limericks in search
				// so they can be used for Tent+> button as well as search < > buttons
				list ($firstSearchId, $prevSearchId, $nextSearchId, 
					$lastSearchId, $thisSearchCount, $searchTotal) = 
					GetPrevNextSearchIds($lim->OriginalId);
			}
			
			$html = new LimHtml();
			$html->BeginDiv("WorkshopControlTable");			
			
			$html->BeginDiv("WorkshopNextButtons");
			if ($member->CanWorkshop())
				$html->Text(CreatePrevNextSearchButtons($firstSearchId, $prevSearchId, $nextSearchId, 
					$lastSearchId, $thisSearchCount, $searchTotal));
			$html->Text(CreatePrevNextActivityButtons($lim->OriginalId));
			$html->EndDiv();
			
			FormatMemberControls($html, $lim, $member);
			
			FormatStateChangeControls($html, $lim, $member, $nextSearchId);
			
			FormatWSAdminControls($html, $lim, $member, $nextSearchId);
			
			$html->BeginDiv("", "clear: both;");			
			$html->EndDiv();
			
			$html->EndDiv();
			
			$WordsDisplay .= $html->FormattedHtml();
		}
	
		if ($member->CanEdit() and !$lim->IsMine() and $lim->IsTentative() and
			($lim->NonAuthorRFACount()>=RFAS_NEEDED) and $lim->HasPrimaryRFA() and $lim->HasSecondaryRFA())
			$ShowSetToConfButton = TRUE;
		else
			$ShowSetToConfButton = FALSE;

		if ($lim->IsMine() and
			(!$lim->IsApproved()) and (!$lim->IsStored()) and 
			(!$lim->IsExcluded()) and (!$lim->IsHeld()) and 
			(!$lim->HasMyRFA()) and ($lim->NonAuthorRFACount()>=RFAS_NEEDED))
			$AllowSelfRFA = TRUE;
		else
			$AllowSelfRFA = FALSE;

		if ($ShowSetToConfButton)
		{
			$WordsDisplay .= str_replace('@@LIMERICKTEXT@@', $lim->Record['Verse'], 
				FormatDocument('Limerick Check List', TRUE, TRUE));
			$WordsDisplay .= "<p>".DarkButton(array("Workshop=".$lim->OriginalId, "Confirming=$lim->VerseId"), 
				"Set to Confirming")."</p>";
		}

		if (($member->CanWorkshop() or $lim->IsMine()) and !$_POST['workshoptext'])
			$WordsDisplay .= FormatTagsTopics($lim->OriginalId, $lim->Record['State'], $lim->IsMine());

		if ($member->CanWorkshop() or $lim->IsMine())
		{
			if ($lim->IsStored())
				$WordsDisplay .= '<h3>This limerick is in the bottom drawer until we get further through the alphabet.</h3>';
			$WordsDisplay .= FormatWorkshopEntryForm($lim, $ShowSetToConfButton, $AllowSelfRFA);
		}
    
		$commentLimit = 1000000;
		
		$html = new LimHtml();
		
		$html->BeginDiv("hinttext", "text-align: right;");
		global $HelpSymbol;
		$html->Text(LinkPopup('View=Abbreviations', ' Help: Abbreviations and Acronyms', '', $HelpSymbol));
		$html->EndDiv();

		FormatComments($html, $lim, $member, $lim->VerseId, true, $commentLimit, $commentsFound);
		
		if ($_SESSION['WholeWorkshop']!=$lim->OriginalId)
			$_SESSION['WholeWorkshop'] = 0;
      
		if (($_SESSION['WholeWorkshop']!=$lim->OriginalId) and 
			($member->Record('WSCommentDisplayLimit')!='Unlimited'))
			$commentLimit = $member->Record('WSCommentDisplayLimit') - $commentsFound;

		FormatWorkshopHistory($html, $lim, $member, true, $commentLimit);
		$WordsDisplay .= $html->FormattedHtml();
	}
	else $WordsDisplay .= '<p>You must be logged in to view the workshop discussions.</p>';

	return $WordsDisplay;
}

function FormatComments($html, $lim, $member, $verseId, $allowEditing, $commentLimit, &$commentsFound)
{
	$commentsFound = 0;
	$timeFormatter = new LimTimeConverter($member->Record('TimeZone'));
	$comments = $lim->GetComments($verseId);
	if (count($comments)>0)
	{
		$commentFormatter = new LimCommentFormatter($member->GetMemberId(), $member->CanAdministrate(), $timeFormatter);
        $commentFormatter->SetLastViewedComment($lim->GetLastOldCommentId());
		$commentFormatter->BeginCommentFrame($html);
		foreach ($comments as $comment)
		{
			if ($commentsFound < $commentLimit)
				$commentFormatter->FormatComment($html, $comment, $allowEditing);
			$commentsFound++; 
		}
		$commentFormatter->EndCommentFrame($html);
	}
	else
		$html->Paragraph("No workshop comments found for this revision.");
}

function FormatWorkshopHistory($html, $lim, $member, $obsoleteOnly=false, $commentLimit=1000000)
{
	$revisions = $lim->GetObsoleteRevisions();	
	if (!$obsoleteOnly)
		array_unshift($revisions, $lim->Record);

	if ((count($revisions)>0) and $obsoleteOnly)
		$html->Heading('History', 2);
	
	foreach ($revisions as $oldLim)
	{
		if ($commentLimit>0)
		{
			if ($oldLim['Version']==0) $html->Heading("Original Submission", 3, "expanded");
			else $html->Heading("Revision ".$oldLim['Version'], 3, "expanded");
			$html->BeginDiv();
			$html->Text(FormatLimerickBlock($oldLim, FALSE));
			FormatComments($html, $lim, $member, $oldLim['VerseId'], false, $commentLimit, $commentsFound);
			$commentLimit -= $commentsFound;
			$html->EndDiv(); 
		}
        else
            $commentLimit = -1 ; // force the truncation message because we've truncated a whole revision
	}
	if ($commentLimit<0)
		$html->Text(FormatTruncatedWorkshopOptions($lim->OriginalId));
}


function FormatTagsTopics($OriginalId, $State, $mine) {
  global $member;
  $WordsDisplay = '';
  $Label = '';
  $Sep = '';
  if ($member->CanWorkshop()) {
    $Label .= $Sep.'Tags';
    $Sep = ' and ';
  }
  
  $CanTopic = (($member->CanWorkshop() or $mine) and
     (($member->HasPrivilege('TopicEditor') or $mine) and
        (($State=='approved') or ($State=='tentative') or ($State=='revised') or 
			($State=='confirming') or ($State=='new'))));
  if ($CanTopic) {
    $Label .= $Sep.'Topics';
    $Sep = ' and ';
  }
  
  if ($member->CanWorkshop() or $CanTopic) {
    if ($_SESSION['$ShowTopicEdit']=='On')
      $WordsDisplay .= "<div class='expanded'>$Label<br ></div><div>";
    else
      $WordsDisplay .= "<div class='trigger'>$Label<br ></div><div>";
    if ($member->CanWorkshop())
      $WordsDisplay .= FormatAddRemoveTag($OriginalId);
    if ($CanTopic)
      $WordsDisplay .= FormatAddRemoveTopicLim($OriginalId);
    
    $WordsDisplay .= "</div>";
  }
  return $WordsDisplay;
}

function WorkshopLink($VerseId) {
  return LinkSelf('Workshop='.$VerseId, 'Back to the Workshop');
}

function AddRemoveCurtain($VerseId)
{
  $result = DbQuery(sprintf("UPDATE DILF_Limericks SET Category = '%s' WHERE VerseId='%d' LIMIT 1",
    ($_GET['Curtain']=='On') ? 'curtained room': 'normal', $VerseId));
  SetRedirect("Workshop=".$VerseId);
  UpdateStats();
}

function AddRemoveMonitor($OriginalId) {
  if ($_GET['Monitor']=='On') AddWorkshopMonitor($OriginalId);
  if ($_GET['Monitor']=='Off') DeleteWorkshopMonitor($OriginalId);
  SetRedirect("Workshop=".$OriginalId);
}

function AddWorkshopMonitor($OriginalId) {
  global $member;
  $result = DbQuery("SELECT * FROM DILF_Monitors WHERE AuthorId=".$member->GetMemberId()." AND OriginalId=$OriginalId");
  if (DbQueryRows($result)==0)
    DbInsert("DILF_Monitors", array("AuthorId"=>$member->GetMemberId(), "OriginalId"=>$OriginalId));
  DbEndQuery($result);
}

function DeleteWorkshopMonitor($OriginalId) {
  global $member;
  $result = DbQuery("DELETE FROM DILF_Monitors WHERE AuthorId=".$member->GetMemberId()." AND OriginalId=$OriginalId");
}

function AddRemoveRFA($lim)
{
	global $member;
	if ($_GET['RFA']=='On') 
		AddRFA($lim->OriginalId, $_GET['RFAVerseId'], $_GET['SelfRFA'], $lim->Record);
	if ($_GET['RFA']=='Off') 
	{
		DeleteRFA($lim->OriginalId, $member->GetMemberId());
		$lim->ReloadRFAInfo();
		if ($lim->IsConfirming())
		{
			if (($lim->NonAuthorRFACount()<RFAS_NEEDED) or !$lim->HasPrimaryRFA() or !$lim->HasSecondaryRFA())
			{
				ChangeLimerickState($lim->VerseId, $lim->OriginalId, 'confirming', 'tentative', $lim->IsMine());
				UpdateAuthorStats($lim->Record['PrimaryAuthorId']);
				if ($lim->Record['SecondaryAuthorId']) UpdateAuthorStats($lim->Record['SecondaryAuthorId']);
				UpdateStats();
			}
			else // reset the entry time
				SetLimerickStateDateTime($lim->OriginalId);
		}
	}
	SetRedirect("Workshop=".$lim->OriginalId);
}

function AddRFA($OriginalId, $VerseId, $SelfRFA, $line) {
  global $member;

  // add monitor for RFAers if requested
  if (!$SelfRFA) {
	  $result = DbQuery(sprintf("SELECT * FROM DILF_Authors WHERE AuthorId=%d", $member->GetMemberId()));
	  if ($AuthorLine = DbFetchArray($result))
	  	$pref = $AuthorLine['MonitorRFAs'];
	  else
	  	$pref = 'All';

	  if ($pref=='All')
	  	AddWorkshopMonitor($OriginalId);
	  DbEndQuery($result);
  }

  $result = DbQuery(sprintf("SELECT * FROM DILF_RFAs WHERE OriginalId=%d AND AuthorId=%d", $OriginalId, $member->GetMemberId()));
  if (DbQueryRows($result)==0) {
    DbInsert("DILF_RFAs", array("OriginalId"=>$OriginalId, "AuthorId"=>$member->GetMemberId()));
    AuditLog($OriginalId, 'Added RFA');

    if ($SelfRFA) {
      AddWorkshopComment(0, $VerseId, 'Based on the workshopping so far, '.GetAuthorHtml($member->GetMemberId()).' considers this limerick Ready for Final Approval.', $commentId);

      // notify RFAers
      NotifyOfRFAChanges($VerseId, 'The author has RFAed this limerick.');
    }
    else {
      // check for this being the correct number of RFAs to allow self-RFA
      // count RFAs on this piece
      $result3 = DbQuery(sprintf("SELECT * from DILF_RFAs where OriginalId=%d AND AuthorId <> %d AND AuthorId <> %d",
				$OriginalId, $line['PrimaryAuthorId'], $line['SecondaryAuthorId']));
      $NonAuthorRFAs = DbQueryRows($result3);
      DbEndQuery($result3);

      // if either author has self-RFA'd, then we'll assume that it was done after the requisite number of non-author RFAs, and so we'll assume the self-RFA available notification was sent at that stage.
      $result3 = DbQuery(sprintf("SELECT * from DILF_RFAs where OriginalId=%d AND (AuthorId=%d OR AuthorId=%d)",
				$OriginalId, $line['PrimaryAuthorId'], $line['SecondaryAuthorId']));
      $HasSelfRFA = DbQueryRows($result3);

      if ($NonAuthorRFAs == RFAS_NEEDED and $HasSelfRFA <= 0) NotifyOfWorkshopUpdate($VerseId, 'Self-RFA is now available for this limerick.', 'State Change', TRUE);
      DbEndQuery($result3);

      // a limerick with an RFA counts as workshopped
      if ($line['WorkshopState']=='none') SetWorkshopState($VerseId, 'started');
    }
  }
  DbEndQuery($result);
  UpdateLimerickRFAs($OriginalId);
}

function DeleteRFA($OriginalId, $AuthorId) {
  $result = DbQuery(sprintf("DELETE FROM DILF_RFAs WHERE OriginalId=%d AND AuthorId=%d", $OriginalId, $AuthorId));
  if (DbAffectedRows()>0) {
    UpdateLimerickRFAs($OriginalId);
    AuditLog($OriginalId, 'Removed RFA');
  }
}

function UpdateLimerickRFAs($OriginalId) {
  $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE OriginalId=%d AND State<>'obsolete'", $OriginalId));
  $LimLine = DbFetchArray($result);
  DbEndQuery($result);

  $result = DbQuery(sprintf("SELECT COUNT(*) RFAs, SUM(AuthorId=%d) PriRFA, SUM(AuthorId=%d) SecRFA 
  	FROM DILF_RFAs WHERE OriginalId=%d", $LimLine['PrimaryAuthorId'], $LimLine['SecondaryAuthorId'],$OriginalId));
  if ($line = DbFetchArray($result)) {
    DbQuery(sprintf("UPDATE DILF_Limericks SET RFAs=%d, PrimaryRFA=%d, SecondaryRFA=%d 
    	WHERE OriginalId=%d AND State<>'obsolete'", $line['RFAs'], $line['PriRFA'], $line['SecRFA'], $OriginalId));
  }
  DbEndQuery($result);
  
  return sprintf("T#%d %d RFAs, Pri=%d, Sec=%d<br>", $OriginalId, $line['RFAs'], $line['PriRFA'], $line['SecRFA']);
}

function SetToConfirming($OriginalId, $VerseId) {
  global $member;
  $line = GetVerse($VerseId);
  if ($member->CanEdit() and ($line['State']!='confirming')) {
    ChangeLimerickState($VerseId, $OriginalId, 'tentative', 'confirming', FALSE);
    NotifyOfWorkshopUpdate($VerseId, 'Set to Confirming.', 'State Change', FALSE);
    AddWorkshopComment($member->GetMemberId(), $VerseId, "Set limerick to Confirming state. Barring any issues, this limerick will be approved in a week. Authors may still make alterations to their writing by stopping the countdown to return the limerick to Tentative.", $commentId);
    AddWorkshopMonitor($OriginalId);
    // store the id of the editor.
    $result = DbQuery(sprintf("UPDATE DILF_Limericks SET ApprovingEditor=%d, LinkCode=0 WHERE OriginalId = '%d' AND State<>'obsolete' LIMIT 1",
      $member->GetMemberId(), $OriginalId));

    $line = GetVerse($VerseId);
    UpdateAuthorStats($line['PrimaryAuthorId']);
    if ($line['SecondaryAuthorId']) UpdateAuthorStats($line['SecondaryAuthorId']);
    UpdateStats();
  }
}

function AllowedStateChange($OldState, $NewState, $mine, $holder=0) {
  global $member;
  $Allowed = FALSE;

  if ($OldState<>$NewState) {
    switch ($NewState) {
    case 'held':
      $Allowed = ($member->CanEdit() or
        ($member->CanContribute() and ($OldState=='tentative')) or
        ($member->CanWorkshop() and ($OldState=='new') and !$mine) );
      break;
    case 'tentative':
      $Allowed = ($member->CanEdit() or
        ($mine and ($OldState=='untended')) or
        (($OldState!='approved') and
         ($member->CanWorkshopPlus() or
          (($mine or $member->CanWorkshop()) and ($OldState=='confirming')) or
          (($member->GetMemberId()==$holder) and ($OldState=='held')) or
          ($member->CanWorkshop() and ($OldState=='revised')) or
          ($member->CanWorkshop() and (!$mine) and (($OldState=='new') or ($OldState=='held')))
        )));
      break;
    case 'excluded':
    case 'stored':
      $Allowed = ($member->CanEdit());
      break;
    case 'confirming':
      $Allowed = ($member->CanEdit());
      break;
    case 'approved':
      $Allowed = ($member->CanAdministrate() and (($OldState=='tentative') or ($OldState=='confirming')));
      break;
    case 'untended':
      $Allowed = ($member->CanAdministrate() and ($OldState=='tentative'));
      break;
    }
  }
  return $Allowed;
}

function ChangeLimerickState($VerseId, $OriginalId, $OldState, $NewState, $mine, $holder=0) {
  global $member;

  if (AllowedStateChange($OldState, $NewState, $mine, $holder)) {
    if ($NewState=='approved') {  //---------------------Approve a limerick and give it a number
      $NewNum = ApproveLimerick($VerseId);
    }
    else {
      if ($NewState=='held') $ExtraSet = ', HeldById='.$member->GetMemberId();
      else $ExtraSet='';
      $result = DbQuery(sprintf("UPDATE DILF_Limericks SET State='%s', StateDateTime='%s' $ExtraSet WHERE OriginalId = '%d' AND State<>'obsolete' LIMIT 1",
        $NewState, LimTimeConverter::FormatGMDateFromNow(), $OriginalId));
      if ((($NewState=='tentative') and ($OldState<>'revised') and ($OldState<>'untended') and ($OldState<>'stored'))
        or ($NewState=='revised')) {
        // remove author RFAs
        $line = GetVerse($VerseId);
        DeleteRFA($OriginalId, $line['PrimaryAuthorId']);
        if ($line['SecondaryAuthorId']) DeleteRFA($OriginalId, $line['SecondaryAuthorId']);
      }
      NotifyOfStateChanges($VerseId, $NewState);
      AuditLog($VerseId, $NewState);
    }
  }
  else AuditLog($VerseId, 'Attempted to set to '.$NewState);
}

function SetLimerickStateDateTime($OriginalId) {

  $result = DbQuery(sprintf("UPDATE DILF_Limericks SET StateDateTime='%s' WHERE OriginalId = '%d' AND State<>'obsolete' LIMIT 1",
    LimTimeConverter::FormatGMDateFromNow(), $OriginalId));
  AuditLog($VerseId, "Reset StateDateTime");
}

function CallForAttention($VerseId) {
  global $member;
  $WordsDisplay = '';
  
  if ($member->CanWorkshop()) {
    $line = GetVerse($VerseId);
    $LimLines = explode("\n", $line['Verse']);
    DrawAttention($VerseId, $LimLines[0]) ;
    AuditLog($VerseId, 'Called for Workshopper Attention');
    $WordsDisplay .= 'Called for Workshopper Attention';
  }
  return $WordsDisplay;
}

function CheckForExpiredCFA($VerseId, $OriginalId) {
  $WordsDisplay = '';
  // check for the last CFA message
  $result = DbQuery(sprintf("SELECT COUNT(*) Count, MAX(DateTime) LastMsg FROM DILF_Messages WHERE VerseId=%d AND MsgType='Attention'", $OriginalId));
  if (($line = DbFetchArray($result)) and ($line['Count']>0)) {
    // count the number of different authored WS comments since the CFA was sent
    
    $result2 = DbQuery(sprintf("SELECT VerseId FROM DILF_Workshop WHERE VerseId=%d AND DateTime>'%s' GROUP BY AuthorId", $VerseId, $line['LastMsg']));
    $count = DbQueryRows($result2);
    DbEndQuery($result2);
    $WordsDisplay .= "<p>$count authors have contributed to the CFA discussion.</p>";
    //echo $WordsDisplay;
    if ($count>=CFA_CONTRIBUTORS) { // N different authors is enough attention. Time to delete the call.
      $result3 = DbQuery(sprintf("DELETE FROM DILF_Messages WHERE VerseId=%d AND MsgType='Attention'", $OriginalId));
    }
  }
  DbEndQuery($result);
  
  return $WordsDisplay;
}

?>
