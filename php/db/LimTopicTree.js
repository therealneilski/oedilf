var imgTreePlus = new Image();
var imgTreeMinus = new Image();
var imgOpenFolder = new Image();
var imgClosedFolder = new Image();
var imgLeafFolder = new Image();
var imgSelOpenFolder = new Image();
var imgSelClosedFolder = new Image();
var imgSelLeafFolder = new Image();
imgTreePlus.src = "TreePlus.gif";
imgTreeMinus.src = "TreeMinus.gif";
imgOpenFolder.src = "OpenFolder.gif";
imgClosedFolder.src = "ClosedFolder.gif";
imgLeafFolder.src = "LeafFolder.gif";
imgSelOpenFolder.src = "SelOpen.gif";
imgSelClosedFolder.src = "SelClosed.gif";
imgSelLeafFolder.src = "SelLeaf.gif";

var SelectedTopics = new Array();
var LeafTopic = new Array();
var strTopicTemplate = "javascript:ClickTopic(%s)";
//var strTopicTemplate = "?Topic=%s";

var OriginalId = "";
var TopicsChanged = false;

function ShowTopicTree() {
  document.getElementById("idTopicSelectionTree").style.display="block";
}

function ShowTopic(id, selected) {
  document.getElementById("d"+id).style.fontWeight = (selected ? "bold" : "normal");
  if (LeafTopic[id])
    document.images["dimg"+id].src = (selected ? imgSelLeafFolder.src : imgLeafFolder.src);
  else {
    if (document.getElementById("dbranch"+id).style.display=="block")
      document.images["dimg"+id].src = (selected ? imgSelOpenFolder.src : imgOpenFolder.src);
    else
      document.images["dimg"+id].src = (selected ? imgSelClosedFolder.src : imgClosedFolder.src);
  }
}

function ShowSelectedTopicList() {
  var strSep = "";
  var strTopics = "";
  var strTopicNums = ";";
  for (idTopic in SelectedTopics) {
    if (SelectedTopics[idTopic]) { // strip out the tagged stuff and extract the text.
      var strText = (document.getElementById("dtext"+idTopic).innerHTML).replace(new RegExp("<(.|\n)+?>", "g"),"");
      strTopics += strSep + "<a title=\"Find this topic in the tree\" href=\"javascript:RevealTopic(" + idTopic + ")\">" + strText + "</a>";
      strSep = "; ";
      strTopicNums += idTopic + ";";
    }
  }
  if (strTopics=="") strTopics = "(None. <a href=\"javascript:ShowTopicTree();\">Click subjects in the topic tree to select them</a>.)";
  if (TopicsChanged)
    strTopics += " <a class=\"macro\" href=\"?AddTopicLimerick=" + OriginalId + "&amp;LTopics=" + strTopicNums + "\">Save&nbsp;Selections</a>";
  document.getElementById("idTopicList").innerHTML = strTopics;
}

function ClickTopic(id) {
  TopicsChanged = true;
  SelectedTopics[id] = (SelectedTopics[id] ? false : true); // toggle
  ShowTopic(id, SelectedTopics[id]);
  ShowSelectedTopicList();
}


function DisplayTopicBranch(id, expanded) {
  document.getElementById("dbranch"+id).style.display = (expanded ? "block" : "none");
  document.images["branch" + id].src = (expanded ? imgTreeMinus.src : imgTreePlus.src);
  ShowTopic(id, SelectedTopics[id]);
}

function ExpandContract(id) {
  var idTree = "dbranch"+id;
  DisplayTopicBranch(id, document.getElementById(idTree).style.display != "block");
}

function ContractAll(divParent) {
  for (var i=0; i < divParent.childNodes.length; i++) {

    if (divParent.childNodes[i].id &&
      divParent.childNodes[i].id.substring(0,7)=="dbranch") {
      DisplayTopicBranch(divParent.childNodes[i].id.substring(7,1000), false);
      ContractAll(divParent.childNodes[i]);
    }
  }
}

function ExpandAll(divParent) {
  for (var i=0; i < divParent.childNodes.length; i++) {

    if (divParent.childNodes[i].id &&
      divParent.childNodes[i].id.substring(0,7)=="dbranch") {
      DisplayTopicBranch(divParent.childNodes[i].id.substring(7,1000), true);
      ExpandAll(divParent.childNodes[i]);
    }
  }
}

function RevealTopic(id) {
  ShowTopicTree();
  var divTree = document.getElementById("idTree");
  ContractAll(divTree);

  var divParent = document.getElementById("d"+id).parentNode;
  while (divParent && (divParent.id.substring(0,7)=="dbranch")) {
    idParent = divParent.id.substring(7,1000);
    DisplayTopicBranch(idParent, true);
    divParent = document.getElementById("d"+idParent).parentNode;
  }
  document.getElementById("dtext"+id).focus();  
}

function AddBranchNode(id, text) {
  document.write("<div id=\"d", id, "\"><a href=\"javascript:ExpandContract(", id, ");\"><img src=\"",imgTreePlus.src,"\" id=\"branch", id, "\"></a><a class=\"branchtopic\" href=\"", strTopicTemplate.replace("%s", id), "\" id=\"dtext", id, "\"><img id=\"dimg", id, "\"src=\"",imgClosedFolder.src,"\">", text, "</a></div>", "<div id=\"dbranch", id, "\">");
  LeafTopic[id] = false;
}
function FinishBranchNode() {
  document.write("</div>");
}
function AddLeafNode(id, text) {
  document.write("<div id=\"d", id, "\"><a class=\"leaftopic\" href=\"javascript:ClickTopic(", id,")\" id=\"dtext", id, "\"><img id=\"dimg", id, "\" src=\"",imgLeafFolder.src,"\">", text, "</a></div>");
  LeafTopic[id] = true;
}

function AddBranchNodeJSON(id, text) {
  if (id != 1) {
    container += '/' + text;
  }
  LeafTopic[id] = false;
  return '{"id":"' + id + '", "text":"' + text + '", "container":"' + container + '"}, ';
}
function FinishBranchNodeJSON() {
  container = container.substring(0, container.lastIndexOf('/'));
  return '';
}
function AddLeafNodeJSON(id, text) {
  LeafTopic[id] = true;
  return '{"id":"' + id + '", "text":"' + text + '", "container":"' + container + '"}, ';
}

function doLoad() {
  var divTree = document.getElementById("idTree");

  ContractAll(divTree);
  //ExpandContract(1);

  // Set up pre-selected topics
  for (idTopic in SelectedTopics) {
    if (SelectedTopics[idTopic])
      ShowTopic(idTopic, true);
  }
  ShowSelectedTopicList();

}
