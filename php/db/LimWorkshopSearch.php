<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimWorkshopSearch 
{
	protected $title = '';
	protected $searchSelect = '';
	protected $searchJoin = '';
	protected $searchFilter = '';
	protected $searchOrder = '';
	protected $table = 'DILF_Limericks L';
    protected $addedTables = '';
	protected $where = "L.State<>'obsolete'";
	protected $idList = array();
	
	public function __construct($table="")
	{
        if ($table) $this->SetTable($table);
	}

	public function AddTitle($newText)
	{
		if ($this->title!='') $this->title .= ' ';
		$this->title .= $newText;
	}
	
	public function AddSelect($newSelect)
	{
		$this->searchSelect .= ' '.$newSelect;
	}
	
	public function AddJoin($newJoin)
	{
		$this->searchJoin .= ' '.$newJoin;
	}
	
	public function AddFilter($newFilter)
	{
		$this->searchFilter .= ' '.$newFilter;
	}	
	
	public function AddOrder($newOrder)
	{
		if ($this->searchOrder=='')
			$this->searchOrder = " ORDER BY ".$newOrder;
		else
			$this->searchOrder .= ', '.$newOrder;
	}	
	
	public function SetTable($newTable)
	{
		$this->table = $newTable;
		$this->where = '1';
	}

    public function AddTable($addedTable)
    {
        $this->addedTables .= ', '.$addedTable;
    }
	
	public function Search($CurtainFilter)
	{
		$this->idList = array();
	    $result = DbQuery("SELECT DISTINCT L.OriginalId ".$this->searchSelect.
			" FROM ".$this->table.$this->searchJoin.$this->addedTables.
			" WHERE ".$this->where." $CurtainFilter".$this->searchFilter.
			$this->searchOrder);
	    while ($line = DbFetchArray($result)) 
		{
			$this->idList[] = $line['OriginalId'] ;
	    }
	    DbEndQuery($result);
		return count($this->idList);
	}
	
	public function GetOriginalIdArray()
	{   
		return $this->idList;		
	}
	
	public function GetOriginalIdList()
	{   
		return implode(",", $this->idList);		
	}
	
	public function GetTitle()
	{
		return $this->title;
	}
	
	public function AndResults($idArray)
	{
        $before = LimGeneral::GetMicroTime();
        LimGeneral::Log(sprintf("arraylengths %d, %d", count($this->idList), count($idArray)));
		$this->idList = array_intersect($this->idList, $idArray);
		LimGeneral::Log(sprintf("AndResults took %.2f ms", LimGeneral::MsElapsed($before)));
	}
	
	public function AndNotResults($idArray)
	{
        $before = LimGeneral::GetMicroTime();
		$this->idList = array_diff($this->idList, $idArray);
		LimGeneral::Log(sprintf("AndNotResults took %.2f ms", LimGeneral::MsElapsed($before)));
	}
	
}

?>