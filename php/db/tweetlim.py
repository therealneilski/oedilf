#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import tweepy
import sys
import requests
import tempfile
import os
import re
import time
from PIL import Image, ImageFont, ImageDraw, ImageColor, ImageChops

def trim(im):
    # Calculate the true bounding box, and trim the image, leaving a bit of a border.
    bg = Image.new(im.mode, im.size, im.getpixel((0, 0)))
    diff = ImageChops.difference(im, bg)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    if bbox:
        return im.crop((0, 0, bbox[2]+10, bbox[3]+10))

def make_image(author, lim_id, defined_words, lim):
    # Creates an image containing the limerick
    if os.path.exists('/var/www/db'):
        sspr = ImageFont.truetype('/var/www/db/fonts/SourceSansPro-Regular.otf', size=24)
        ssprb = ImageFont.truetype('/var/www/db/fonts/SourceSansPro-Bold.otf', size=24)
        sspri = ImageFont.truetype('/var/www/db/fonts/SourceSansPro-It.otf', size=10)
        oedilfi = Image.open("/var/www/db/oedilfi.jpg")
    else:
        sspr = ImageFont.truetype('/Users/neil/Library/Fonts/SourceSansPro-Regular.otf', size=24)
        ssprb = ImageFont.truetype('/Users/neil/Library/Fonts/SourceSansPro-Bold.otf', size=24)
        sspri = ImageFont.truetype('/Users/neil/Library/Fonts/SourceSansPro-It.otf', size=10)
        oedilfi = Image.open("/Users/neil/src/oedilf/oedilfi.jpg")

    black = ImageColor.getrgb('black')


    image = Image.new("RGB", (2000, 400), ImageColor.getrgb('white'))

    image.paste(oedilfi, (10, 10))

    draw = ImageDraw.Draw(image)

    draw.text((160, 25), "Limerick #{0} on {1} by {2}".format(lim_id, defined_words, author), font=ssprb, fill=black)
    y = 60
    for line in lim.split('\n'):
        line = line.strip()
        line = re.sub(r'<.*?>', '', line)
        draw.text((160, y), line.decode("utf-8"), font=sspr, fill=black)
        y += 30

    draw.text((10, y+10), u"© Copyright 2004-{0} OEDILF. The information in this image may not be reproduced in any form without written permission by the OEDILF Editor-in-Chief.".format(time.gmtime().tm_year), font=sspri, fill=black)
    draw.text((10, y+22), u"All limericks remain the property of the authors who contributed them and are reproduced here by permission.", font=sspri, fill=black)
    draw.text((10, y+34), u"If you reproduce an individual limerick under the doctrine of fair use or fair dealing, please credit the limerick's author.", font=sspri, fill=black)
    
    image = trim(image)
    
    _, filename = tempfile.mkstemp(suffix='.png', prefix='lim')
    
    image.save(filename, "PNG")
    
    return filename
    
def tweet():
    
    author = sys.argv[1]
    lim_id = sys.argv[2]
    verse_id = sys.argv[3]
    defined_words = sys.argv[4]
    lim = sys.argv[5]

    
    auth = tweepy.OAuthHandler(
        'o8ntBWyM38OSHKPkcfrCS2iEF',
        'f4Xkfx6x43w5RVMUjPbZf4bbexEHVz9OKfZ68txsS9ZbZct1TR'
    )
    auth.set_access_token(
        '331021600-3m6Be9ga0CYY6FdpJl1AElnCv8yKqBdEougrinPT',
        '90Luxh3nNcj35C0lM3uv9BsAjGmxtaVMT2NosrCSK9EkA'
    )

    api = tweepy.API(auth)

    # png_source = \
    #     'http://chart.apis.google.com/chart?chst=d_text_outline&chld=000000|24|h|FFFFFF|_|{0}'.format(
    #         lim.replace('\n', '|')
    #     )
    # png = requests.get(
    #     png_source
    # )

    # strip html formatting
    lim = re.sub(r'<.*?>', '', lim)

    tweet = u'Limerick #{0} on {1} by {2}:\nhttps://oedilf.com/db/Lim.php?VerseId={3}\n{4} ...'.format(
        lim_id,
        defined_words,
        author,
        verse_id,
        lim[:lim.index('\n')]
    )
    # max tweet length is 157 because Twitter will shorten it to a t.co link
    if len(tweet) >= 156:
        tweet = tweet[:155]

    if os.path.exists('/var/www/db'):
        log = open("/var/www/db/logs/tweetlim.log", "a")
    else:
        log = sys.stdout
    log.write("{0}: Tweeting {1}\n".format(time.ctime(), lim_id))
    log.write(tweet)
    log.write("\n")
    try:
        # if png.status_code == 200:
        #     _, filename = tempfile.mkstemp(suffix='.png', prefix='lim')
        #     f = open(filename, 'w')
        #     f.write(png.content)
        #     f.close()
        #     api.update_with_media(filename, tweet)
        #     os.remove(filename)
        # else:
        #     api.update_status(tweet)
        filename = make_image(author, lim_id, defined_words, lim)
        # print filename
        api.update_with_media(filename, tweet)
        os.remove(filename)
        log.write("Success!\n")
    except Exception, e:
        log.write("Failed:\n")
        log.write("{0}\n".format(sys.exc_info()[0]))
        log.write("{0}\n".format(sys.exc_info()[1]))
        log.write("{0}\n".format(sys.exc_info()[2]))
    log.write("\n")
    log.close()
    

if __name__ == '__main__':
    tweet()

