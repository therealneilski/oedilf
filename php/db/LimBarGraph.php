<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimBarGraph {
  protected $title, $data;

  public function LimBarGraph($Title) {
    $this->title = $Title;
    $this->data = array();
  }

  public function CollectData($query) {
    $result = DbQuery($query);
    while ($this->data[] = DbFetchArray($result)) {
    }
    DbEndQuery($result);
  }

  public function PlotBars($datacolname, $maxwidth, $FormatFunction, $html) {

    $html->Heading($this->title, 3);
    $MaxDataVal = 0;
    foreach ($this->data as $line) {
      $value = $line[$datacolname];
      if ($value>$MaxDataVal) {
        $MaxDataVal = $value;
      }
    }
    $scale = $MaxDataVal / $maxwidth;

	$html->BeginTable();
	$html->BeginTableRow();
    foreach ($this->data[0] as $heading => $value) {
    	$html->TableHeadingCell($heading);
    }
	$html->EndTableRow();

    foreach ($this->data as $line) {
		$html->BeginTableRow();
		if ($line[$datacolname]) {
			foreach ($line as $heading => $value) {
				$html->BeginTableCell('padding-top:0;padding-bottom:0;');
				if ($FormatFunction[$heading]) 
					$html->Text($FormatFunction[$heading]($value));
				else
					$html->Text($value);
				$html->EndTableCell();
			}
			$point = $line[$datacolname] / $scale;
			
			$html->BeginTableCell('padding-top:0;padding-bottom:0;');
			$html->Image("dot.gif", $line[$datacolname], 12, $point, "GraphBar");
			$html->EndTableCell();
		}
		$html->EndTableRow();
    }
	$html->EndTable();
    return $html->FormattedHtml();
  }
}

?>