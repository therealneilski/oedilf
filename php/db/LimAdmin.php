<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function ParseAdminInput() {
	global $PageTitle;
	$WordsDisplay = '';
	
	if (($_GET['Admin']=='Settings')  or
		isset($_POST['MemberPrivilegesCancelButton']) or
		isset($_POST['MemberProfileCancelButton'])) {
		$PageTitle = 'Update Settings';
		$WordsDisplay .= FormatGeneralAdminForm();		
	}
	else if ($_GET['Admin']=='Table') {
		$PageTitle = 'Admin Table';
		$WordsDisplay .= FormatAdminTable();
	} 
	else if (($_GET['Admin']=='AdminSettings') and
		isset($_POST['AdminSettingsUpdateButton'])) {
		$PageTitle = 'Settings Updated';
		$WordsDisplay .= ProcessGeneralAdminForm();
	}
	else if (($_GET['Admin']=='Shutdown') and
		(($_POST['AdminShutdownButton']=='Notify Shutdown') or 
		($_POST['AdminShutdownButton']=='Cancel Shutdown'))) {
		$PageTitle = 'Shutdown';
		$WordsDisplay .= ProcessShutdown() ;
	}
	else if ($_POST['AdminShareButton']=='Share') {
		$PageItile = 'Share Limerick';
		$WordsDisplay .= ProcessShare();
	}
	else if ($_POST['AdminCommentButton']=='Add Comment') {
		$PageTitle = 'Comment Added';
		$WordsDisplay .= ProcessAddAdminComment();
	}
	else if ($_POST['AdminHoldAndCommentButton']=='Hold and Comment') {
		$PageTitle = 'Comment Added';
		$WordsDisplay .= ProcessHoldAndComment();
	}
	else if ($_POST['AdminMergeButton']=='Merge') {
		$PageTitle = 'Merging Memberships';
		$WordsDisplay .= ProcessMemberMerge();
	}
	else if ($_POST['AdminGoodbyeButton']=='Goodbye Limerick') {
		$PageTitle = 'Limerick Deleted';
		$WordsDisplay .= ProcessAnnihilation();
	}
	else if ($_POST['AdminTextReplaceButton']=='Replace Text') {
		$PageTitle = 'Global Search and Replace';
		$WordsDisplay .= ProcessSearchReplace();
	}
	else if ($_GET['Admin']=='MailList') {
		$PageTitle = 'Generate Mailing List';
		$WordsDisplay .= FormatMailingList();
	}
	else if (($_GET['Admin']=='MailMessage') and 
		($_POST['AdminGenerateButton']=='Generate List')) {
		$PageTitle = 'Bulk Mail Message';
		$WordsDisplay .= FormatBulkMail();
	}
	else if (($_GET['Admin']=='SendBulk') and 
		($_POST['AdminSendBulkButton']=='Send Bulk Email')) {
		$PageTitle = 'Send Bulk Email';
		$WordsDisplay .= ProcessBulkMail();
	}
	else if ($_GET['AdminShowMessageButton']) {
		$PageTitle = 'Message Activity';
		if ($_GET['ProfileId']+0)
			$WordsDisplay .= FormatMessageActivityTable($_GET['ProfileId']+0);
		else $WordsDisplay .= UnpecifiedAuthor();
	}
	else if ($_GET['AdminShowLogButton']) {
		$PageTitle = 'Log Activity';
		if ($_GET['ProfileId']+0)
			$WordsDisplay .= FormatLogActivityTable($_GET['ProfileId']+0);
		else $WordsDisplay .= UnpecifiedAuthor();
	}
	else if ($_GET['AdminTitlePrivilegesButton']) {
		$PageTitle = 'Change Title and Privileges';
		$WordsDisplay .= FormatTitlePrivileges($_GET['ProfileId']+0);
	}
	else if (($_GET['Admin']=='MemberPrivileges') and
		isset($_POST['MemberPrivilegesUpdateButton']))
	{
		$PageTitle = 'Author Title and Privilege Change';
		$WordsDisplay .= ProcessTitlePrivilegeChange();
	}
	else if ($_GET['AdminEditProfileButton']) {
		$PageTitle = 'Edit Member Profile';
		$WordsDisplay .= FormatMemberProfile($_GET['ProfileId']+0);
	}
	else if (($_GET['Admin']=='MemberProfileTest') and
		(isset($_POST['MemberProfileUpdateButton']))) {
		$PageTitle = 'Member Profile Updated';
		$WordsDisplay .= ProcessMemberProfile();
	}
	else if ($_GET['Admin']=='SvnUpdate')
	{
		$WordsDisplay = "<pre>".htmlspecialchars(shell_exec('svn update'), ENT_QUOTES)."</pre>";
	} 
	else BugOrHack();	
	
	return $WordsDisplay;
}

function BuildAdminForm()
{
	$form = new LimFormBuilder('AdminSettings', false);
	$form->AddField(new LimFieldText('AlphabetEnd', 'Alphabet End'));
	$form->AddField(new LimFieldText('AdminEmail', 'Admin Email'));
	$form->AddField(new LimFieldText('EiCEmail', 'EiC Email'));
    $form->AddField(new LimFieldSelect('WPSISubmission', 'Add default WPSI comment for new submissions',
			array('Off'=>'Disabled', 'On'=>"Enabled")));
	return $form;
}

function FormatGeneralAdminForm() {
	global $Configuration;
	$WordsDisplay = '';

	$WordsDisplay .= FormatTableMenu();
	
	$html = new LimHtml();
	$form = BuildAdminForm();
	$form->FormatHeader($html, "Admin=AdminSettings");
	$form->FormatBody($html, $Configuration);
	$form->FormatFooter($html);
	$WordsDisplay .= $html->FormattedHtml();
		
	$AuthorList = AuthorSelectionList(0);
	$ShortAuthorList = AuthorSelectionList(0, TRUE, TRUE);
	
	$WordsDisplay .= sprintf('<form action="%s" method="get">', PHPSELF);
	$WordsDisplay .= '<table class="widetable">';
	$WordsDisplay .= sprintf('<tr><td class="darkpanel">');
	$WordsDisplay .= '<p>Edit Author Profile for <select name="ProfileId">'.$AuthorList.'</select><br>'.
		' '.SubmitButton("AdminTitlePrivilegesButton", "Change title and privileges"). 
		' '.SubmitButton("AdminEditProfileButton", "Show/Edit Profile"). 
		' '.SubmitButton("AdminShowMessageButton", "Show private message activity").
		SubmitButton("AdminShowLogButton", "Show log activity").
		'<input type=hidden name="Admin" value="MemberManagement"></p>';
	$WordsDisplay .= '</td></tr>';
	$WordsDisplay .= '</table>';
	$WordsDisplay .= '</form>';
	
	$WordsDisplay .= sprintf('<form action="%s?Admin=Merge" method="post">', PHPSELF);
	$WordsDisplay .= '<table class="widetable">';
	$WordsDisplay .= sprintf('<tr><td class="darkpanel">');
	$WordsDisplay .= '<p>Merge Author <select name="mergeauthor">'.$ShortAuthorList.'</select>
		Into Author <select name="intoauthor">'.$ShortAuthorList.'</select> '. 
		SubmitButton("AdminMergeButton", "Merge").'</p>';
	$WordsDisplay .= '</td></tr>';
	$WordsDisplay .= '</table>';
	$WordsDisplay .= '</form>';
	
	$WordsDisplay .= FormatUpdateStatsForm();
		
	$WordsDisplay .= sprintf('<form action="%s?Admin=TextReplace" method="post">', PHPSELF);
	$WordsDisplay .= '<table class="widetable">';
	$WordsDisplay .= sprintf('<tr><td class="darkpanel">');
	$LimNums = explode(",",$_SESSION['SearchList']);
	$WordsDisplay .= '<p>Within limericks in the current search list ('.count($LimNums).' selected),<br >
		replace <input type=text name="replaceoldtext" value=""> 
		with <input type=text name="replacenewtext" value=""> <br > 
		Scope: <input type=checkbox name="inlimerickbody">Limerick text 
		<input type=checkbox name="inauthornotes">Author notes 
		<input type=checkbox name="ineditornotes">Editor notes '. 
		SubmitButton("AdminTextReplaceButton", "Replace Text").'</p>';
	$WordsDisplay .= '</td></tr>';
	$WordsDisplay .= '</table>';
	$WordsDisplay .= '</form>';

	$WordsDisplay .= sprintf('<form action="%s?Admin=HoldAndComment" method="post">', PHPSELF);
	$WordsDisplay .= '<table class="widetable">';
	$WordsDisplay .= sprintf('<tr><td class="darkpanel">');
	$LimNums = explode(",",$_SESSION['SearchList']);
	$WordsDisplay .= '<p>Hold all limericks in the current search list ('.count($LimNums).' selected)
        and add the following workshop comment:<br >
        <textarea name="adminmsg" COLS=80 ROWS=7></textarea></p>
		<p>(<input type=checkbox name="sure">Yes, I\'m sure I want to do this irreversible action.)  '.
		SubmitButton("AdminHoldAndCommentButton", "Hold and Comment").'</p>';
	$WordsDisplay .= '</td></tr>';
	$WordsDisplay .= '</table>';
	$WordsDisplay .= '</form>';

	$WordsDisplay .= sprintf('<form action="%s?Admin=GoodbyeLim" method="post">', PHPSELF);
	$WordsDisplay .= '<table class="widetable">';
	$WordsDisplay .= sprintf('<tr><td class="darkpanel">');
	$WordsDisplay .= '<p>This function should be avoided. Try using the Kill Mode button in the workshop of a limerick you want to remove.</p><p>Annihilate limerick #T<input type=text name="annihilate"> revision # <input type=text name="revision" value="*"></p>' ;
	$WordsDisplay .= '<p>(<input type=checkbox name="sure">Yes, I\'m sure I want to do this irreversible action.) ';
	$WordsDisplay .= SubmitButton("AdminGoodbyeButton", "Goodbye Limerick").'</p>';
	$WordsDisplay .= '</td></tr>';
	$WordsDisplay .= '</table>';
	$WordsDisplay .= '</form>';
	
	$WordsDisplay .= FormatMultiLimCommentForm($AuthorList);

	$WordsDisplay .= FormatShareForm();

	$WordsDisplay .= FormatShutdownForm();
	
	return $WordsDisplay;
}

function ProcessGeneralAdminForm() {
	global $Configuration;
	$WordsDisplay = '';

	StripMagicQuotes();
	
	$form = BuildAdminForm();
	$updates = $form->GetUpdateRecord($_POST);
	$updates = array_map(DbEscapeString, $updates);
	DbUpdate("DILF_Settings", $updates, "1", 1);
	
	// re-read the configuration record
	$result = DbQuery(sprintf("SELECT * FROM DILF_Settings WHERE 1"));
	$Configuration = DbFetchArray($result) ;
	DbEndQuery($result);
	$WordsDisplay .= '<p>Administration settings updated.</p>';
	$WordsDisplay .= LinkSelf('Admin=Settings', 'Back to Administration');
	AuditLog(0, 'Updated Admin Settings');
	SetReload('Admin=Settings');

	return $WordsDisplay;
}

function FormatMessageActivityTable($AuthorId) {
	$WordsDisplay = '';
	$start = $_GET['Start']+0;

    $Name = GetAuthor($AuthorId);
    $WordsDisplay .= FormatSQLTable('Private Messages', 
    	"SELECT L.DateTime 'DateTime (GMT)', A.Name, L.Action FROM DILF_Log L, DILF_Authors A 
    		WHERE L.AuthorId=A.AuthorId AND (L.Action LIKE 'Sent message to $Name' OR 
    		(L.AuthorId=$AuthorId AND L.Action LIKE 'Sent message to %')) ORDER BY L.DateTime DESC",
		array(), $start, 100, 
		array('Admin=MemberManagement', 'AdminShowMessageButton=1', 'ProfileId='.$AuthorId), $NumMatches);
	return $WordsDisplay;
}

function FormatLogActivityTable($AuthorId) {
	$WordsDisplay = '';
	$start = $_GET['Start']+0;

	$WordsDisplay .= FormatSQLTable('Recent Activity', 
		"SELECT L.DateTime 'DateTime (GMT)', L.VerseId, L.Action, L.UserIP
		FROM DILF_Log L, DILF_Authors A WHERE L.AuthorId=A.AuthorId AND L.AuthorId=$AuthorId
		ORDER BY L.DateTime DESC LIMIT 2000",
		array(), $start, 100, array('Admin=MemberManagement', 'AdminShowLogButton=1', 'ProfileId='.$AuthorId), 
		$NumMatches);
	return $WordsDisplay;
}

function TitleOptionsList()
{
	$options = array();
	$result = DbQuery(sprintf("SELECT DISTINCT * FROM DILF_Ranks WHERE 1 ORDER BY Title"));
	while ($line=DbFetchArray($result))
		$options[$line['Rank']] = $line['Title'];
	DbEndQuery($result);
	return $options;
}

function UserTypeOptionsList()
{
	$types = array('applicant', 'member', 'workshopper', 'senior workshopper', 'editor', 'administrator', 'banned');
	$options = array('0'=>'(default)');
	foreach($types as $type)
		$options[$type] = $type;
	return $options;
}

function BuildPrivilegeForm()
{
	$form = new LimFormBuilder('MemberPrivileges', true);
	$form->AddField(new LimFieldSelect('Rank', 'Title', TitleOptionsList()));
	$form->AddField(new LimFieldSelect('UserType', 'Privileges', UserTypeOptionsList()));
	$form->AddField(new LimFieldMultiSelect('AddPrivileges', 'Additional privileges and restrictions',
		array("TopicEditor"=>"TopicEditor", "NoNewLims"=>"NoNewLims")));
	return $form;
}

function UnpecifiedAuthor()
{
	return '<p>You must select an author.</p>'.
		'<p>'.LinkSelf("Admin=Settings", "Back to Admin").'</p>';
}

function FormatTitlePrivileges($AuthorId) 
{
	if (!$AuthorId)
		return UnpecifiedAuthor();
		
	$html = new LimHtml();
	
	$html->Heading('Edit Title and Privileges for '.GetAuthorHtml($AuthorId), 3);
	$result = DbQuery("SELECT * FROM DILF_Authors WHERE AuthorId=$AuthorId");
	if ($line=DbFetchArray($result)) 
	{
		$form = BuildPrivilegeForm();
		$form->AddHidden("AuthorId", $AuthorId);
		$form->FormatHeader($html, "Admin=MemberPrivileges");
		$line['UserType']='0'; //default
		$form->FormatBody($html, $line);
		$form->FormatFooter($html);
	}
	DbEndQuery($result);
	return $html->FormattedHtml();
}

function ProcessTitlePrivilegeChange()
{
	$WordsDisplay = '';
	
	$form = BuildPrivilegeForm();
	$updates = $form->GetUpdateRecord($_POST);
	$AuthorId = $_POST['AuthorId']+0;
	
	$result = DbQuery(sprintf("SELECT * FROM DILF_Ranks WHERE Rank='%s' 
		ORDER BY Title", $updates['Rank']));
	$line = DbFetchArray($result) ;
	DbEndQuery($result);
	
	if ($updates['UserType']=='0') $priv = $line['Privilege'];
	else $priv = $updates['UserType'];
	
	PromoteAuthor($AuthorId, $line['Rank'], $priv);
	DbUpdate("DILF_Authors", array('AddPrivileges'=>$updates['AddPrivileges']), 
		"AuthorId=$AuthorId", 1);
	
	$WordsDisplay .= sprintf('<p>%s is now %s and has %s privileges.</p>',
		GetAuthorHtml($AuthorId), $line['Title'], $priv);
	
	AuditLog(0, 'Updated Title/Privilege Settings for '.$AuthorId);
	SetReload('Admin=Settings');
	$WordsDisplay .= LinkSelf("Admin=Settings", "Back to Admin");

	return $WordsDisplay;
}

function FormatMemberProfile($AuthorId) {
	if (!$AuthorId)
		return UnpecifiedAuthor();
		
	$WordsDisplay = '';

	$result = DbQuery("SELECT * FROM DILF_Authors WHERE AuthorId=$AuthorId");
	if ($line=DbFetchArray($result)) {
		
		$html = new LimHtml();
		$html->Heading("Edit Member Profile", 3);
		$html->BeginParagraph();
		$html->Text("Remember, if you change a person's username or password, you need to email them to let them know what you've changed before they can log in.");
		$html->EndParagraph();
		$form = LimProfile::BuildProfileForm();
		$form->AddHidden("AuthorId", $AuthorId);
		$form->FormatHeader($html, "Admin=MemberProfileTest");
		$form->FormatBody($html, $line);
		$form->FormatFooter($html);
		$WordsDisplay .= $html->FormattedHtml();
	}
	DbEndQuery($result);

	return $WordsDisplay;
}

function ProcessMemberProfile() {
	$WordsDisplay = '';
	$form = LimProfile::BuildProfileForm();
	$authorId = $_POST['AuthorId']+0;
	
	StripMagicQuotes();
	$messages = $form->InvalidEntryMessages($_POST);

	if (($_POST['Email']!='') and !ValidEmail($_POST['Email']))
		$messages[] = htmlspecialchars("Email address is badly formed: ".$_POST['Email'], ENT_QUOTES);

	if (!empty($messages))
	{
		$WordsDisplay .= "<p>".FormatWarning(implode("<br>", $messages))."</p>";
		$WordsDisplay .= "<p>The profile was not updated.</p>";
		$WordsDisplay .= "<p>".LinkSelf(array("ProfileId=".$authorId, 
				"AdminEditProfileButton=Show%2FEdit+Profile",
				"Admin=MemberManagement"), 
			"Back to profile editing")."</p>";
	}
	else
	{
    	$_POST['Biography'] = CleanupHtml($_POST['Biography']);
    	$updates = $form->GetUpdateRecord($_POST);
		$updates = array_map(DbEscapeString, $updates);
		DbUpdate("DILF_Authors", $updates, "AuthorId=".$authorId, 1);
        
		$WordsDisplay .= '<p>Profile information updated.</p>';
		AuditLog(0, 'Updated Profile Settings for '.$authorId);
		SetReload('Admin=Settings');
	}
	$WordsDisplay .= "<p>".LinkSelf("Admin=Settings", "Back to Admin")."</p>";
	return 	$WordsDisplay;
}

function ProcessAnnihilation() {
	$WordsDisplay = '';

    if ($_POST['annihilate']+0=='0')
      $WordsDisplay .= '<p>You must select a limerick number and a revision number.</p>';
    else if (!$_POST['sure'])
      $WordsDisplay .= '<p>You must check the box to confirm your intentions to annihilate a limerick completely.</p>';
    else {
      if ($_POST['revision']=='*') $VersionMatch='';
      else $VersionMatch = 'AND Version='.$_POST['revision'];

      $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE OriginalId=%d ".$VersionMatch,
        $_POST['annihilate']));
      if (DbQueryRows($result)>0) {
        while ($line = DbFetchArray($result)) {
          $WordsDisplay .= AnnihilateSingleLimerick($line['VerseId'], $_POST['annihilate'], $line['Version']);
        }
        $WordsDisplay .= LinkSelf('Admin=Settings', 'Back to Administration');
      }
      else {
        $WordsDisplay .= '<p>Limerick not found.</p>';
      }
      DbEndQuery($result);
    }

	return $WordsDisplay;
}

function ProcessHoldAndComment() {
	global $member;
	$WordsDisplay = '';
    StripMagicQuotes();

    if ($_POST['adminmsg']=='')
      $WordsDisplay .= '<p>You must enter a comment before it can be added.</p>';
    else if (!$_POST['sure'])
      $WordsDisplay .= '<p>You must check the box to confirm your intention to add lots of WS comments.</p>';
    else {
      $Comment = $_POST['adminmsg'];
      $LimNums = explode(",",$_SESSION['SearchList']);
      $AffectedLims = new LimStatsUpdater();

      foreach ($LimNums as $LimNum) {
        $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE OriginalId='%d' AND State<>'obsolete'", $LimNum));
        if ($line = DbFetchArray($result)) {
            ChangeLimerickState($line['VerseId'], $line['OriginalId'], $line['State'], 'held', false, $member->GetMemberId());
            AddWorkshopComment($member->GetMemberId(), $line['VerseId'], $Comment, $commentId);
            $AffectedLims->Remember($line['PrimaryAuthorId'], $line['SecondaryAuthorId']);
        }
        DbEndQuery($result);
      }

      $WordsDisplay .= sprintf('<p>Held and added comment to %d limericks.</p>',
        count($LimNums));
      $AffectedLims->Update();
    }
	$WordsDisplay .= LinkSelf('Admin=Settings', 'Back to Administration');

	return $WordsDisplay;
}

function ProcessSearchReplace() {
	global $member;
	$WordsDisplay = '';

    $LimNums = explode(",",$_SESSION['SearchList']);

    $OldText = stripslashes($_POST['replaceoldtext']);
    $NewText = stripslashes($_POST['replacenewtext']);
    foreach ($LimNums as $LimNum) {
      $Updates = "";

      $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE OriginalId='%d' AND State<>'obsolete'", $LimNum));
      if ($line = DbFetchArray($result)) {
        if ($_POST['inlimerickbody']) {
          $NewVerse = str_replace($OldText, $NewText, $line['Verse']) ;
          if ($NewVerse<>$line['Verse'])
            $Updates .= sprintf("Verse='%s',", AddSlashes($NewVerse));
        }
        if ($_POST['inauthornotes']) {
          $NewAN = str_replace($OldText, $NewText, $line['AuthorNotes']) ;
          if ($NewAN<>$line['AuthorNotes'])
            $Updates .= sprintf("AuthorNotes='%s',", AddSlashes($NewAN));
        }
        if ($_POST['ineditornotes']) {
          $NewEN = str_replace($OldText, $NewText, $line['EditorNotes']) ;
          if ($NewEN<>$line['EditorNotes'])
            $Updates .= sprintf("EditorNotes='%s',", AddSlashes($NewEN));
        }
        $Updates = trim($Updates, ",");
      }
      DbEndQuery($result);

      if ($Updates) {
        $result = DbQuery(sprintf("UPDATE DILF_Limericks SET %s WHERE OriginalId='%d' AND State<>'obsolete'", $Updates, $LimNum));
        $WordsDisplay .= sprintf('<p>Updated #T%d</p>', $LimNum);
        AddWorkshopComment($member->GetMemberId(), $line['VerseId'], "Automated search and replace: [".$_POST['replaceoldtext']."] to [".$_POST['replacenewtext']."]", $commentId);
      }
      else
        $WordsDisplay .= sprintf('<p>No substitutions done for #T%d</p>', $LimNum);

    }
	return $WordsDisplay;
}

function FormatUpdateStatsForm() {
	$WordsDisplay = '';
	$WordsDisplay .= '<table class="widetable">';
	$WordsDisplay .= sprintf('<tr><td class="darkpanel">');
	$WordsDisplay .= sprintf('<form action="%s" method="get">', PHPSELF);
	$WordsDisplay .= '<p><input type="hidden" name="Utility" value="UpdateAuthorStats"> '.
		SubmitButton("UpdateAuthorStatistics", "Update Author Statistics"). 
		' This is a slow function that goes through all the authors and counts up their limericks 
		to make sure the database shows the right values on the author page. 
		Use it only if the statistics seem to be out of whack.</p>';
	$WordsDisplay .= '</form>';
	$WordsDisplay .= sprintf('<form action="%s" method="get">', PHPSELF);
	$WordsDisplay .= '<p><input type="hidden" name="Utility" value="PurgeLogs"> '.
		SubmitButton("PurgeLogs", "Purge Very Old Log Data").
		' This function deletes old (more than 4 months) audit trail and page timing data. It makes backups run faster.</p>';
	$WordsDisplay .= '</form>';
	$WordsDisplay .= '</td></tr>';
	$WordsDisplay .= '</table>';
	
	return 	$WordsDisplay;
}

function FormatMultiLimCommentForm($AuthorList) {
	$WordsDisplay = '';
	$WordsDisplay .= sprintf('<form action="%s?Admin=Comment" method="post">', PHPSELF);
	$WordsDisplay .= '<table class="widetable">';
	$WordsDisplay .= sprintf('<tr><td class="darkpanel">');
	$WordsDisplay .= '<textarea name="adminmsg" COLS=80 ROWS=7></textarea>';
	$WordsDisplay .= '<p>Add <b>automatically boldfaced</b> comment to <select name="limstate"><option value="tentative" selected>all tentative<option value="all">all</select> limericks by 
		<select name="author">'.$AuthorList.'</select> (primary or secondary author).</p>';
	$WordsDisplay .= '<p>(<input type=checkbox name="sure">Yes, I\'m sure I want to do this irreversible action.) ';
	$WordsDisplay .= SubmitButton("AdminCommentButton", "Add Comment").'</p>';
	$WordsDisplay .= '</td></tr>';
	$WordsDisplay .= '</table>';
	$WordsDisplay .= '</form>';
	
	return 	$WordsDisplay;
}

function ProcessAddAdminComment() {
	global $member;
	$WordsDisplay = '';

    if (($_POST['author']=='0') or ($_POST['adminmsg']==''))
      $WordsDisplay .= '<p>You must select an author and enter a comment before it can be added.</p>';
    else if (!$_POST['sure'])
      $WordsDisplay .= '<p>You must check the box to confirm your intention to add possibly hundreds of WS comments.</p>';
    else {
      $Comment = '<b>'.$_POST['adminmsg'].'</b>';
    
      // find all applicable limericks
	  $filter = "State='tentative'";
	  if ($_POST['limstate']=='all') $filter = "State<>'Obsolete'";
      $result = DbQuery(sprintf("SELECT DISTINCT VerseId FROM DILF_Limericks WHERE %s AND (PrimaryAuthorId=%d OR SecondaryAuthorID=%d)",
      	$filter, $_POST['author'],$_POST['author']));
      $NumComments = DbQueryRows($result);
      while ($line = DbFetchArray($result)) {
      	AddWorkshopComment($member->GetMemberId(), $line['VerseId'], $Comment, $commentId);
      }

      $WordsDisplay .= sprintf('<p>Added comment to %d limericks by %s.</p>',
        $NumComments, GetAuthorHtml($_POST['author']));
    }
	$WordsDisplay .= LinkSelf('Admin=Settings', 'Back to Administration');
  
	return $WordsDisplay;
}

function ProcessMemberMerge() {
  $WordsDisplay = '';

  $mergeauthor = $_POST['mergeauthor'];
  $intoauthor = $_POST['intoauthor'];
  if (($mergeauthor==0) or ($intoauthor==0))
    $WordsDisplay .= '<p>You must specify both a merge author and the author to merge with.</p>';
  else {
    $WordsDisplay .= sprintf('<p>Merging author #%d [%s] into author #%d [%s]</p>',
      $mergeauthor, GetAuthorHtml($mergeauthor), $intoauthor, GetAuthorHtml($intoauthor));
    AuditLog(0, sprintf('Merging author %d [%s] into %d [%s]',
      $mergeauthor, GetAuthorHtml($mergeauthor), $intoauthor, GetAuthorHtml($intoauthor)));

    $result = DbQuery(sprintf("Update DILF_Limericks SET PrimaryAuthorId='%d' WHERE PrimaryAuthorId='%d'", $intoauthor, $mergeauthor));
    $lims = DbAffectedRows();
    $WordsDisplay .= sprintf('<p>Primary Author: changed %d limericks</p>', $lims);

    $result = DbQuery(sprintf("Update DILF_Limericks SET SecondaryAuthorId='%d' WHERE SecondaryAuthorId='%d'", $intoauthor, $mergeauthor));
    $lims = DbAffectedRows();
    $WordsDisplay .= sprintf('<p>Secondary Author: changed %d limericks</p>', $lims);

    $result = DbQuery(sprintf("Update DILF_Messages SET SrcId='%d' WHERE SrcId='%d'", $intoauthor, $mergeauthor));
    $msgs = DbAffectedRows();
    $WordsDisplay .= sprintf('<p>Notifications sent: changed %d messages</p>', $msgs);

    $result = DbQuery(sprintf("Update DILF_Messages SET DstId='%d' WHERE DstId='%d'", $intoauthor, $mergeauthor));
    $msgs = DbAffectedRows();
    $WordsDisplay .= sprintf('<p>Notifications received: changed %d messages</p>', $msgs);

    $result = DbQuery(sprintf("Update DILF_Messages SET AuthorId='%d' WHERE AuthorId='%d'", $intoauthor, $mergeauthor));
    $msgs = DbAffectedRows();
    $WordsDisplay .= sprintf('<p>Notifications on author\'s limericks: changed %d messages</p>', $msgs);

    $result = DbQuery(sprintf("Update DILF_Monitors SET AuthorId='%d' WHERE AuthorId='%d'", $intoauthor, $mergeauthor));
    $msgs = DbAffectedRows();
    $WordsDisplay .= sprintf('<p>Workshop monitors: changed %d monitors</p>', $msgs);

    $result = DbQuery(sprintf("Update DILF_RFAs SET AuthorId='%d' WHERE AuthorId='%d'", $intoauthor, $mergeauthor));
    $msgs = DbAffectedRows();
    $WordsDisplay .= sprintf('<p>RFAs: changed %d RFAs</p>', $msgs);

    $result = DbQuery(sprintf("Update DILF_Workshop SET AuthorId='%d' WHERE AuthorId='%d'", $intoauthor, $mergeauthor));
    $msgs = DbAffectedRows();
    $WordsDisplay .= sprintf('<p>Workshop: changed %d comments</p>', $msgs);

    $result = DbQuery(sprintf("Update DILF_Authors SET UserType='banned' WHERE AuthorId='%d'", $mergeauthor));
    $msgs = DbAffectedRows();
    $WordsDisplay .= sprintf('<p>Registration: changed %d merged author to banned.</p>', $msgs);
    
    UpdateAuthorStats($mergeauthor);
    UpdateAuthorStats($intoauthor);
  }

  return $WordsDisplay;
}

function FormatShutdownForm() {
	$WordsDisplay = '';
	$WordsDisplay .= sprintf('<form action="%s?Admin=Shutdown" method="post">', PHPSELF);
	$WordsDisplay .= '<table class="widetable">';
	$WordsDisplay .= '<tr><td class="darkpanel">';
	
	$WordsDisplay .= 'Schedule database shutdown in <input type=text size=4 name="shutdowntime" value="30"> minutes' ;
	$WordsDisplay .= ' '.SubmitButton("AdminShutdownButton", "Notify Shutdown");
	$WordsDisplay .= ' '.SubmitButton("AdminShutdownButton", "Cancel Shutdown");
	$WordsDisplay .= '</td></tr>';
	$WordsDisplay .= '</table>';
	$WordsDisplay .= '</form>';
	
	return 	$WordsDisplay;
}

function ProcessShutdown() {
	$WordsDisplay = '';

    $Shutdown = time()+$_POST['shutdowntime']*60;
    if ($_POST['AdminShutdownButton']=='Cancel Shutdown') $Shutdown='';
    
    $result = DbQuery(sprintf("UPDATE DILF_Settings SET Shutdown='%s' WHERE 1 LIMIT 1", $Shutdown));
    // re-read the configuration record
    $result = DbQuery(sprintf("SELECT * FROM DILF_Settings WHERE 1"));
    $Configuration = DbFetchArray($result) ;
    DbEndQuery($result);

    SetReload('Admin=Settings');
    if ($Shutdown=='')
      $WordsDisplay .= '<p>Shutdown Cancelled.</p>';
    else {
      $WordsDisplay .= '<p>Shutdown Scheduled.</p>';
      $_SESSION['MaintenanceMode'] = 'On';
    }
	return $WordsDisplay;
}


function FormatShareForm() {
	$WordsDisplay = '';
	$WordsDisplay .= sprintf('<form action="%s?Admin=Share" method="post">', PHPSELF);
	$WordsDisplay .= '<table class="widetable">';
	$WordsDisplay .= '<tr><td class="darkpanel">';
	
	$WordsDisplay .= 'Share approved limerick A<input type=text size=6 name="sharelimericknumber"> on Twitter and Facebook' ;
	$WordsDisplay .= ' '.SubmitButton("AdminShareButton", "Share");
	$WordsDisplay .= '</td></tr>';
	$WordsDisplay .= '</table>';
	$WordsDisplay .= '</form>';
	
	return 	$WordsDisplay;
}

function ProcessShare() {
	$log = fopen("/var/www/db/logs/tweetlim.log", "a");
	fwrite($log, "ProcessShare: posting ".$_POST['sharelimericknumber'] . "\n");
	$WordsDisplay = '';
	$VerseId = $_POST['sharelimericknumber'];
	$result = DbQuery(sprintf("SELECT LimerickNumber FROM DILF_Limericks WHERE LimerickNumber = %d AND State = 'approved'", $VerseId));
	$line = DbFetchArray($result);
	DbEndQuery($result);
	if ($line) {
		require_once("LimTweet.php");
		$WordsDisplay .= "Output from tweet:<br><pre>\n";
		fwrite($log, "ProcessShare: posting tweet\n");
		$WordsDisplay .= print_r(TweetLimerick("A".$VerseId), true);
		$WordsDisplay .= "</pre><br>Output from facebook:<br><pre>\n";
		fwrite($log, "ProcessShare: posting to facebook\n");
		$WordsDisplay .= print_r(FacebookLimerick("A".$VerseId), true);
		$WordsDisplay .= "</pre>\n";
	}
	fwrite($log, "ProcessShare: done\n");
	fclose($log);
	return $WordsDisplay;
}

function FormatTableMenu()
{
  $WordsDisplay = '';
  $MenuSep = " ";
  $WordsDisplay .= '<table class="menutable"><tr><td class="menus">';
  $WordsDisplay .= NavLinkSelf(array("Admin=Table", "Table=RawWorkshopping"), "WS Effort", "Raw WS Contribution Stats");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=WorkshoppingReceived"), "WS Received", "Raw WS Received Stats");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=WorkshoppingByMonth"), "WS/Month", "Total WS Stats");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=RFAsPosted"), "RFAs Posted", "Raw RFA Posting Stats");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=Letter"), "Words Limericked", "Stats by starting letters");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=Needed"), "Unlimericked (needed)", "Bold words by starting letters");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=Extra"), "Unlimericked (extra)", "Unbold words by starting letters");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=Log"), "Recent Audit Log", "Detailed log for last 3000 actions");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=Authors", "AuthorSort=Name"), "Authors", "Authors and e-mails");
  //$WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=Emails"), "Emails", "Authors sorted by e-mails");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=LACEs"), "LACEs", "The last 1000 LACEs sent");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=CFA"), "CFA", "Calls for Attention");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=Attention"), "Nearly WEs", "Who needs our attention?");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=MultiId"), "Multi-Id", "Members who may have multiple identities");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=Unlinked"), "Unlinked", "Find limericks without any defined words");
  $WordsDisplay .= $MenuSep.NavLinkSelf("Admin=MailList", "Send Bulk Mail", "Select a mailing list and compose a message");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=FirstLims"), "First Limericks", "Tabulate the first limerick of each author");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=ANLengths"), "AN Lengths", "Show average AN lengths by author");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=Curtains"), "Curtains", "Summary of curtained room limericks");
  $WordsDisplay .= $MenuSep.NavLinkSelf(array("Admin=Table", "Table=Unlimericked"), "Unlimericked", "List of unlimericked words");
  $WordsDisplay .= '</td></tr></table>';

  return $WordsDisplay;
}

function FormatAdminTable() {
  $WordsDisplay = '';
  $start = $_GET['Start']+0;
  $WordsDisplay .= FormatTableMenu();

  if ($_GET['Table']=='Log')
    $WordsDisplay .= FormatSQLTable('Audit Log', 
    	"SELECT L.DateTime 'Time (GMT)', A.Name, L.VerseId, L.Action 
    	FROM DILF_Log L, DILF_Authors A WHERE L.AuthorId=A.AuthorId ORDER BY L.DateTime DESC LIMIT 3000",
      array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);
      
  else if ($_GET['Table']=='RawWorkshopping') {
    $WordsDisplay .= '<p>';
    $WordsDisplay .= LinkSelf(array('Admin=Table', 'Table='.$_GET['Table'], 'WSSince=0000-00-00 00:00:00'), 'Show Total');
    $WordsDisplay .= ' '.LinkSelf(array('Admin=Table', 'Table='.$_GET['Table'], 
    	'WSSince='.LimTimeConverter::FormatGMDateFromNow(-7*24*60*60)), 'Last Week');
    $WordsDisplay .= ' '.LinkSelf(array('Admin=Table', 'Table='.$_GET['Table'],
    	'WSSince='.LimTimeConverter::FormatGMDateFromNow(-30*24*60*60)), 'Last Month');
    $WordsDisplay .= ' '.LinkSelf(array('Admin=Table', 'Table='.$_GET['Table'],
    	'WSSince='.LimTimeConverter::FormatGMDateFromNow(-61*24*60*60)), 'Last Two Months');
    $WordsDisplay .= '<p>';
    if ($_GET['WSSince']) {
    	$Limit = "AND W.DateTime>'".$_GET['WSSince']."'";
    	$TitleSince = " since ".FormatDate($_GET['WSSince']);
	    $WordsDisplay .= FormatSQLTable("Workshopping on Others' Limericks".$TitleSince,
	      "SELECT A.Name Workshopper, COUNT(W.Message) '#Comments', SUM(LENGTH(W.Message)) Length 
	      FROM DILF_Workshop W, DILF_Limericks L, DILF_Authors A 
	      WHERE L.VerseId=W.VerseId AND W.AuthorId=A.AuthorId AND 
	      	W.AuthorId<>L.PrimaryAuthorId AND W.AuthorId<>L.SecondaryAuthorId $Limit 
	      GROUP BY W.AuthorId ORDER BY Length DESC",
	      array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table'], 'WSSince='.$_GET['WSSince']), 
	      $NumMatches);
    }
    else {
    	$WordsDisplay .= '<p>Select one of the above time ranges. Be warned: the Show Total option is 
    		heavy on the server and may time out.</p>';
    }
  }
  else if ($_GET['Table']=='WorkshoppingReceived')

	$WordsDisplay .= FormatSQLTable("WS Comments Received", 
		"SELECT A.Name 'Primary Author', COUNT(*) '#Comments' 
		FROM DILF_Workshop W, DILF_Limericks L, DILF_Authors A 
		WHERE W.VerseId=L.VerseId AND L.PrimaryAuthorId=A.AuthorId AND W.AuthorId<>A.AuthorId 
		GROUP BY A.AuthorId ORDER BY `#Comments` DESC",
      array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);
  else if ($_GET['Table']=='WorkshoppingByMonth')

    $WordsDisplay .= FormatSQLTable("WS Comments by Month", 
    	"SELECT LEFT(DateTime,7) Month, COUNT(*) '#Comments', SUM(LENGTH(Message)) Length 
    	FROM `DILF_Workshop` WHERE AuthorId<>0 GROUP BY Month ORDER BY Month",
      array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);
  else if ($_GET['Table']=='RFAsPosted')

    $WordsDisplay .= FormatSQLTable("RFAs Posted", 
    	"SELECT A.Name, COUNT(*) '#RFAs' FROM DILF_RFAs R, DILF_Authors A 
    	WHERE R.AuthorId=A.AuthorId GROUP BY R.AuthorId ORDER BY `#RFAs` DESC",
      array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);
  else if ($_GET['Table']=='Letter') {

    $result = DbQuery("SELECT COUNT(*) Total FROM DILF_Words WHERE IsPhraseWord=0");
    if ($line = DbFetchArray($result))
        $WordsDisplay .= sprintf("<h4>Total: %d</h4>", $line['Total']);
    DbEndQuery($result);

//  Excluded, since number of approved limericks seemed out of place on a page dedicated to defined word totals
//    $result = DbQuery("SELECT COUNT(*) Approved FROM DILF_Limericks WHERE State='approved'");
//    if ($line = DbFetchArray($result))
//        $WordsDisplay .= sprintf("Approved: %d</h4>", $line['Approved']);
//    DbEndQuery($result);

    $WordsDisplay .= FormatSQLTable('Limericked Words', 
    	"SELECT LEFT(WordText,2) StartLetters, COUNT(WordText) Count FROM DILF_Words 
    	WHERE IsPhraseWord=0 GROUP BY StartLetters ORDER BY StartLetters",
    	array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);
  }
  
  else if ($_GET['Table']=='Needed')
    $WordsDisplay .= FormatSQLTable('Needed Words', 
    	"SELECT LEFT(WordText,2) 'Start Letters', COUNT(*) Count FROM `DILF_WordList` 
    	WHERE Status='not done' AND WordType='needed' GROUP BY `Start Letters`  ORDER BY `Start Letters`",
    	array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);
      
  else if ($_GET['Table']=='Extra')
    $WordsDisplay .= FormatSQLTable('Extra Words', 
    	"SELECT LEFT(WordText,2) 'Start Letters', COUNT(*) Count FROM `DILF_WordList` 
    	WHERE Status='not done' AND WordType='extra' GROUP BY `Start Letters`  ORDER BY `Start Letters`",
    	array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);

  else if ($_GET['Table']=='Authors') {
    $WordsDisplay .= '<p>Sort by:';
    $order = 'Name';
    if ($_GET['AuthorSort']=='Name') $order = "Name";
    else $WordsDisplay .= ' '.LinkSelf(array('Admin=Table', 'Table=Authors', 'AuthorSort=Name'), 'Name');
    if ($_GET['AuthorSort']=='Email') $order = "Email, Name";
    else $WordsDisplay .= ' '.LinkSelf(array('Admin=Table', 'Table=Authors', 'AuthorSort=Email'), 'Email');
    if ($_GET['AuthorSort']=='Rank') $order = "Rank, Name";
    else $WordsDisplay .= ' '.LinkSelf(array('Admin=Table', 'Table=Authors', 'AuthorSort=Rank'), 'Rank');
    if ($_GET['AuthorSort']=='Privilege') $order = "`Privilege Level`, Name";
    else $WordsDisplay .= ' '.LinkSelf(array('Admin=Table', 'Table=Authors', 'AuthorSort=Privilege'), 'Privilege');
    if ($_GET['AuthorSort']=='Access') $order = "`Last Access`, Name";
    else $WordsDisplay .= ' '.LinkSelf(array('Admin=Table', 'Table=Authors', 'AuthorSort=Access'), 'Last Access');
    $WordsDisplay .= '</p>';

    $WordsDisplay .= FormatSQLTable('Authors', 
    	"SELECT Name, Rank, CONCAT(UserType,AddPrivileges) 'Privilege Level', Email, AccessTime 'Last Access' 
    	FROM DILF_Authors WHERE 1 ORDER BY $order",
    	array('Last Access'=>'DecodeTime'), $start, 100, 
    	array('Admin=Table', 'Table='.$_GET['Table'], 'AuthorSort='.$_GET['AuthorSort']), $NumMatches);
  }
  else if ($_GET['Table']=='Emails')
    $WordsDisplay .= FormatSQLTable('Emails', 
    	"SELECT Email, Name, Rank, CONCAT(UserType,AddPrivileges) 'Privilege Level', AccessTime 'Last Access' 
    	FROM DILF_Authors WHERE 1 ORDER BY Email",
    	array('Last Access'=>'DecodeTime'), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);

  else if ($_GET['Table']=='LACEs')

    $WordsDisplay .= FormatSQLTable('LACEs Sent', 
    	"SELECT L.DateTime 'Time (GMT)', A.Name, L.VerseId, L.Action FROM DILF_Log L, DILF_Authors A 
    	WHERE L.AuthorId=A.AuthorId AND L.Action LIKE 'Sent LACE to%' ORDER BY L.DateTime DESC LIMIT 1000",
    	array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);
      
  else if ($_GET['Table']=='Attention')

    $WordsDisplay .= FormatSQLTable('Who needs our attention?', "SELECT A.Name, SUM(L.State='approved') 'Raw Count',
		SUM(L.State='approved' AND A.AuthorId=L.PrimaryAuthorId AND L.SecondaryAuthorId<>0)*0.5+
			SUM(L.State='approved' AND A.AuthorId=L.PrimaryAuthorId AND L.SecondaryAuthorId=0)+ 
			SUM(L.State='approved' AND A.AuthorId=L.SecondaryAuthorId)*0.5 'Fractional Count',
		SUM(L.State='confirming' AND A.AuthorId=L.PrimaryAuthorId) Confirming,
		SUM(L.State='tentative' AND A.AuthorId=L.PrimaryAuthorId) Tentative,
		SUM(L.State='revised' AND A.AuthorId=L.PrimaryAuthorId) Revised
		FROM DILF_Limericks L, DILF_Authors A
		WHERE (A.Rank='CE' OR A.Rank='C') AND (A.AuthorId=L.PrimaryAuthorId OR A.AuthorId=L.SecondaryAuthorId) AND 
			(L.State='approved' OR L.State='confirming' OR L.State='tentative' OR L.State='revised')
		GROUP BY A.AuthorId ORDER BY `Tentative` DESC",
		array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);

  else if ($_GET['Table']=='MultiId') {
    $WordsDisplay .= FormatSQLTable('Possible multi-id members', 
    	"SELECT DateTime, UserIp, Action 'Suspects' FROM DILF_Log 
    	WHERE VerseId=0 AND Action LIKE 'Multi? %%' ORDER BY DateTime DESC",
    	array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);
  }
  else if ($_GET['Table']=='CFA') {

    $result = DbQuery("SELECT MIN(DateTime) 'Since' FROM DILF_Log");

    if ($line = DbFetchArray($result))
        $WordsDisplay .= sprintf("<p>Using audit log data since: %s (Prior information has been purged.)</p>", 
        	FormatDate($line['Since']));
    DbEndQuery($result);
    $WordsDisplay .= FormatSQLTable('Self Calls for Attention', 
    	"SELECT A.Name, COUNT(*) Count FROM DILF_Log L, DILF_Authors A, DILF_Limericks V 
    	WHERE L.AuthorId=A.AuthorId AND Action LIKE '%Workshopper attention%' AND L.VerseId=V.VerseId AND 
    		(L.AuthorId=V.PrimaryAuthorId OR L.AuthorId=V.SecondaryAuthorId) 
    	GROUP BY L.AuthorId ORDER BY Count DESC",
    	array(), 0, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);
    $WordsDisplay .= FormatSQLTable('All Calls for Attention', 
    	"SELECT A.Name, COUNT(*) Count FROM DILF_Log L, DILF_Authors A 
    	WHERE L.AuthorId=A.AuthorId AND Action LIKE '%Workshopper attention%' 
    	GROUP BY L.AuthorId ORDER BY Count DESC",
    	array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumMatches);
  }
  else if ($_GET['Table']=='Unlinked') {
    $WordsDisplay .= FormatLimerickList("Limericks with no defined words",
    	"SELECT DISTINCT L.*, COUNT(W.VerseId) as w_count 
    		FROM DILF_Limericks L LEFT JOIN DILF_Words W ON W.VerseId=L.VerseId 
    		WHERE L.State<>'obsolete' GROUP BY L.VerseId HAVING (w_count<=0) ORDER BY L.VerseId",
    	$start, 20, array('Admin=Table', 'Table='.$_GET['Table']), $NumLimericks);
  }
  else if ($_GET['Table']=='FirstLims') {
    $WordsDisplay .= FormatSQLTable('First Limericks', 
    	"SELECT CONCAT('<a href=\"?&amp;searchstart=Search&amp;searchauthor=',A.AuthorId,'\">',A.Name,'</a>') Name, 
    		MIN(L.DateTime) 'First Limerick' FROM DILF_Authors A, DILF_Limericks L 
    		WHERE L.Version =0 AND L.PrimaryAuthorId=A.AuthorId 
    		GROUP BY A.AuthorId ORDER BY `First Limerick` ASC",
    	array('Name'=>'Raw', 'First Limerick'=>'DateTime'), $start, 100,
    	array('Admin=Table', 'Table='.$_GET['Table']), $NumLimericks);
  }
  else if ($_GET['Table']=='Timing') {
    $StartTime = $_GET['TimeSlot']-300;
    $StopTime = $_GET['TimeSlot']+300;
    $WordsDisplay .= FormatSQLTable('Timing', 
    	"SELECT * FROM DILF_Timing WHERE PageTime>=$StartTime AND PageTime<$StopTime ORDER BY PageTime DESC",
    	array('PageTime'=>'DecodeTime'), $start, 100,
    	array('Admin=Table', 'Table='.$_GET['Table'], 'TimeSlot='.$_GET['TimeSlot']), $NumLimericks);
  }
  else if ($_GET['Table']=='ANLengths') {
    $WordsDisplay .= "<p>All non-obsolete limericks are counted. AN Lengths are character counts. 
    	A Short AN is 20 characters or fewer. Authors with fewer than 20 limericks in the database are not included.</p>";
    $WordsDisplay .= FormatSQLTable('AN Lengths', "SELECT A.Name, COUNT(*) AS 'Limerick Count',
		ROUND(AVG(LENGTH(L.AuthorNotes)),0) AS 'Average AN Length',
		ROUND(100*SUM(LENGTH(L.AuthorNotes)<=20)/COUNT(*),0) AS 'Percentage having Short ANs' 
		FROM DILF_Limericks L, DILF_Authors A WHERE L.State<>'obsolete' AND
			L.PrimaryAuthorId=A.AuthorId 
		GROUP BY L.PrimaryAuthorId 
		HAVING COUNT(*)>=20
		ORDER BY `Average AN Length` DESC",
      array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumLimericks);
  }
  else if ($_GET['Table']=='Curtains') {  
    $WordsDisplay .= FormatSQLTable('Curtained Room Limericks Summary', 
		"SELECT State, COUNT(*) AS 'Curtained Limericks' FROM DILF_Limericks
		WHERE State<>'obsolete' AND Category='Curtained Room'
		GROUP BY State ORDER BY State",
      array(), $noStart, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumStates);
  
    $WordsDisplay .= FormatSQLTable('Curtained Room Limericks By Author', 
		"SELECT A.Name, COUNT(*) Total, SUM(L.Category='curtained room') Curtained,
		ROUND(100*SUM(L.Category='curtained room')/COUNT(*),1) '%Curtained'
		FROM DILF_Limericks L, DILF_Authors A
		WHERE State<>'obsolete' AND L.PrimaryAuthorId=A.AuthorId
		GROUP BY L.PrimaryAuthorId
		HAVING SUM(L.Category='curtained room')>0
		ORDER BY Curtained DESC, Total DESC, A.Name",
      array(), $start, 100, array('Admin=Table', 'Table='.$_GET['Table']), $NumLimericks);
  }
  else if ($_GET['Table']=='Unlimericked') {
    $result = DbQuery("SELECT WordText FROM DILF_WordList WHERE Status='not done' ORDER BY WordText");
    while ($line = DbFetchArray($result)) {
        $WordsDisplay .= htmlspecialchars($line['WordText'], ENT_QUOTES)."<br>\n";
    }
    DbEndQuery($result);
  }
  
  return $WordsDisplay;
}

function FormatMailingList() {
	$WordsDisplay = '';
  
    $WordsDisplay .= "<h3>Generate Mailing List</h3>";
    $WordsDisplay .= sprintf('<form action="%s?Admin=MailMessage" method="post" name="GenerateListForm">', PHPSELF);
    $WordsDisplay .= '<table class="widetable">';
    $WordsDisplay .= '<tr>';
    $WordsDisplay .= FormatTableCell('Membership type', 'darkpanel');
    $WordsDisplay .= FormatTableCell('Multiple allowed. Hold the ctrl key down while you click on member types 
in the list if you want to select multiple types.<br >
<select name="membertype[]" multiple size=7><option value="Appl">Applicant (not fully signed up)
<option value="Regi" selected>Applicant (signed up)
<option value="Cont" selected>Contributor
<option value="CEd" selected>Contributing Editor
<option value="Work" selected>Workshop Editor
<option value="Asso" selected>Associate Editor
<option value="Bann" >Banned Member
<option value="EiC" >Editor-in-Chief</select>', 'lightpanel');
    $WordsDisplay .= '</tr><tr>';
    $WordsDisplay .= FormatTableCell('Last login', 'darkpanel');
    $WordsDisplay .= FormatTableCell('<select name="lastlogin"><option value="Any" selected>Ignore last login time
<option value="Within">Within the last
<option value="NotWithin">Not within the last
</select> <input type=text name="daylimit" value="30"> days.', 'lightpanel');
    $WordsDisplay .= '</tr><tr>';
    $WordsDisplay .= FormatTableCell('Member Opt-out', 'darkpanel');
    $WordsDisplay .= FormatTableCell('<select name="optout"><option value="Respect" selected>Respect member opt-out of email
<option value="Override">This is a legal matter that takes precedence over any user preference
</select>', 'lightpanel');
    $WordsDisplay .= '</tr><tr>';
    $WordsDisplay .= '<td class="darkpanel" colspan=2>'.
		SubmitButton("AdminGenerateButton", "Generate List").'</td>';
    $WordsDisplay .= '</tr>';
    $WordsDisplay .= '</table>';
    $WordsDisplay .= '</form>';
	return $WordsDisplay;
}

function FormatBulkMail() {
	$WordsDisplay = '';

    $WordsDisplay .= "<h3>Bulk Mail Message</h3>";

    $WhereClause = "0";
    if ($_POST['membertype'])
      foreach ($_POST['membertype'] as $selectopt) {
        if ($selectopt=="Appl") $WhereClause .= " OR (Rank='R' AND UserType='applicant')";
        else if ($selectopt=="Regi") $WhereClause .= " OR (Rank='R' AND UserType='member')";
        else if ($selectopt=="Cont") $WhereClause .= " OR (Rank='C' AND UserType='member')";
        else if ($selectopt=="CEd") $WhereClause .= " OR (Rank='CE' AND UserType='member')";
        else if ($selectopt=="Work") $WhereClause .= " OR (UserType='workshopper' OR UserType='senior workshopper')";
        else if ($selectopt=="Asso") $WhereClause .= " OR (Rank='AE' OR UserType='editor')";
        else if ($selectopt=="Bann") $WhereClause .= " OR (UserType='banned')";
        else if ($selectopt=="EiC") $WhereClause .= " OR (Rank='EiC')";
      }
    //$WordsDisplay .= $WhereClause ."<br>";
    //$WordsDisplay .= "Days ".$_POST['daylimit']."<br>";
    if ($_POST['lastlogin']=='Within') $TimeClause = "AND AccessTime>=".(time()-$_POST['daylimit']*24*3600);
    else if ($_POST['lastlogin']=='NotWithin') $TimeClause = "AND AccessTime<".(time()-$_POST['daylimit']*24*3600);
    else $TimeClause = "";

    if ($_POST['optout']=='Respect') $OptClause = "AND NewsEmails='On'";
    else $OptClause = "";


    $WordsDisplay .= sprintf('<form action="%s?Admin=SendBulk" method="post" name="SendBulkForm">', PHPSELF, $TopicId);
    $WordsDisplay .= '<table class="widetable">';
    $WordsDisplay .= '<tr>';
    $WordsDisplay .= FormatTableCell('Addresses', 'darkpanel');
    $WordsDisplay .= '<td class="lightpanel"><textarea name="emaillist" cols=60 rows=10>';
    $result = DbQuery("SELECT * FROM DILF_Authors WHERE Email<>'' AND UseEmail='On' AND ($WhereClause) $TimeClause $OptClause ORDER BY Name");
    $NumAddresses = DbQueryRows($result);
    while ($line=DbFetchArray($result)) {
      $WordsDisplay .= $line['Email'].' "'.$line['Name']."\"\n" ;
    }
    DbEndQuery($result);
    $WordsDisplay .= "</textarea><br >$NumAddresses addresses found.</td>";

    $WordsDisplay .= '</tr><tr>';
    $WordsDisplay .= FormatTableCell('Subject', 'darkpanel');
    $WordsDisplay .= FormatTableCell('<input type=text name="subjectline" size=70 value="A message from The OEDILF">', 'lightpanel');
    $WordsDisplay .= '</tr><tr>';
    $WordsDisplay .= FormatTableCell('Message', 'darkpanel');
    $WordsDisplay .= FormatTableCell('<textarea name="emailmsg" cols=60 rows=15>Dear @@AUTHOR@@,

(replace this with your message text)

Sincerely,

Chris J. Strolin
Editor-in-Chief, The OEDILF

If you feel this email has been delivered to you in error, please forward it to an OEDILF administrator for investigation.
mailto:@@ADMINEMAIL@@
</textarea>', 'lightpanel');
    $WordsDisplay .= '</tr><tr>';
    $WordsDisplay .= '<td class="darkpanel" colspan=2>'.
		SubmitButton("AdminSendBulkButton", "Send Bulk Email").'</td>';
    $WordsDisplay .= '</tr>';
    $WordsDisplay .= '</table>';
    $WordsDisplay .= '</form>';
	return $WordsDisplay;
}

function ProcessBulkMail() {
	global $Configuration;
	$WordsDisplay = '';

    $WordsDisplay .= "<h3>Send Bulk Mail</h3>";
    $Message = stripslashes($_POST['emailmsg']);
    $MailAddresses = explode("\n", stripslashes($_POST['emaillist']));
    foreach ($MailAddresses as $Email) {
      if (trim($Email)) {
        $EmailParts = explode('"', $Email);
        $MailMessage = str_replace(array("\r\n", '@@AUTHOR@@', '@@ADMINEMAIL@@'),
          array("\n", trim($EmailParts[1]), $Configuration['AdminEmail']), $Message);

        SendMailFromEiC(trim($EmailParts[0]), stripslashes($_POST['subjectline']), $MailMessage);
        AuditLog(0, "Sent mail to ".$EmailParts[0]);
        $WordsDisplay .= "Sent mail to ".$EmailParts[0]."<br >";
      }
    }
	return $WordsDisplay;
}
?>
