<?php
require_once "LimGeneral.php";

define('IN_LIM', true);
define('PHPSELF', $_SERVER['PHP_SELF']);
define('THIS_APP', $_SERVER['HTTP_HOST'].PHPSELF);

header("Content-type: text/html; charset=ISO-8859-1");
LimSession::Setup();

require "LimDb.php";
require "LimDbFunctions.php";
require "LimFormat.php";
require "LimMember.php";
require "LimMenu.php";
require "LimLogin.php";
require "LimUtilities.php";

$PageTitle = 'OEDILF Banners';
$WordsDisplay = '';
$Banners = array();

LimDbConnect($dblink);

$WordsDisplay .= LimMember::CheckLogin();

if (LimGeneral::GetDebugMask()) $WordsDisplay .= " In Debug Mode ";

// read the singleton configuration record
$result = DbQuery(sprintf("SELECT * FROM DILF_Settings WHERE 1"));
$Configuration = DbFetchArray($result) ;
DbEndQuery($result);

//-----------------------------------------------
function FormatBannerMenus() {
  global $member;
  $WordsDisplay = '<hr ><small>';
  $WordsDisplay .= '<a href="./Lim.php?Action=Activity" title="Return to the limerick dictionary">Exit</a>';

  if ($_SESSION['VoteState'] == "Voting") {
    if ($_GET['Action']!='ShowTallies') {
	    $WordsDisplay .= " :: Background:";
	    $WordsDisplay .= " ".LinkSelf("bgcolor=FFF", "White", "View the images on a white background");
	    $WordsDisplay .= " ".LinkSelf("bgcolor=000", "Black", "View the images on a black background");
	    $WordsDisplay .= " ".LinkSelf("bgcolor=CCC", "Gray", "View the images on a gray background");
        
        $WordsDisplay .= " :: ".LinkSelf("Action=ShowTallies", "Show My Tallies", "Display all the banner sets and my voting tallies");
    }
    else {
	    $WordsDisplay .= " :: ".LinkSelf("", "Return to Voting");		
	}
  }	
  if ($member->CanAdministrate() and 
	  ($_GET['Action']!='ShowAllTallies') and 
	  ($_SESSION['VoteState'] == "Voted")) {
      $WordsDisplay .= " :: ".LinkSelf("Action=ShowAllTallies", "Show All Tallies", "Display all the banner sets and all voting tallies");
  }  
  $WordsDisplay .= '</small>';
  return $WordsDisplay;
}
  
function ReadBannerTable() {
  $banners = array();
	
  $result = DbQuery("SELECT * FROM banner_sets WHERE 1");
  while ($bannerEntry = DbFetchArray($result)) {
    $banners[$bannerEntry['SetId']] = $bannerEntry['FileName'];	
  }
  DbEndQuery($result);
  return $banners;	
}  

function HiddenName($bannerNumber, $resolution) {
  global $Banners;
  
  $bannerName = $Banners[$bannerNumber]; 
  list($baseName,$extension) = explode("*", $bannerName);
  //$hiddenName = "Banner.php?I=".str_rot13(strrev($baseName));
  
  return "../bannerimages/".$bannerNumber."_".$resolution.".".$extension;	
}
  
function FormatBannerSet($bannerNumber) {
  $WordsDisplay = '';
  
  $WordsDisplay .= "<table>
  <tr>
    <td colspan=3><img src='".HiddenName($bannerNumber, "468x60")."' ></td>
  </tr>
  <tr>
    <td colspan=2><img src='".HiddenName($bannerNumber, "234x60")."' ></td>
	<td rowspan=2><img src='".HiddenName($bannerNumber, "120x240")."' ></td>
  </tr>
  <tr>
    <td><img src='".HiddenName($bannerNumber, "125x125")."' ></td>
    <td><img src='".HiddenName($bannerNumber, "88x31")."' ><br ><br >
        <img src='".HiddenName($bannerNumber, "80x15")."' ></td>
  </tr>
  </table>";
  return $WordsDisplay;	
}

function CompareBannerSets($Base1, $Base2, $BackColor) {
  $WordsDisplay = '';
  
  $WordsDisplay .= "<h3>Banner Set Voting</h3>";
  $WordsDisplay .= "<p>Scroll down to peruse these two banner sets and then cast a vote for the one you prefer.</p>";
  $WordsDisplay .= "<div style='background-color: #$BackColor;'>";
  $WordsDisplay .= "<table>
    <tr><td><p style='background-color: #CCC;'>Set A</p>".FormatBannerSet($Base1)."</td></tr>
	<tr><td><p style='background-color: #CCC;'>Set B</p>".FormatBannerSet($Base2)."</td></tr>
  </table>";
  $WordsDisplay .= "</div>";
  
  $WordsDisplay .= sprintf("<p><form action='%s' method='post'>", PHPSELF);
  $WordsDisplay .= "Which banner set do you prefer: <br ><br > 
    <img src='".HiddenName($Base1, "88x31")."' ><input style='background-color:#FFF;' name='bannerset' value='A' type='radio'>Set A<br ><br >
	<img src='".HiddenName($Base2, "88x31")."' ><input style='background-color:#FFF;' name='bannerset' value='B' type='radio'>Set B<br ><br >".
	SubmitButton("bannervote", "Cast a vote")."<br >";
	
  $WordsDisplay .= "</form></p><br >";
  
  
  return $WordsDisplay;	
}

function SelectPairToCompare($memberId) {
  $WordsDisplay = '';
  $setA = 0;
  $setB = 0;
  
  //$WordsDisplay .= "SelectPairToCompare ";	
  $result = DbQuery("SELECT S.SetId, COUNT(V.Set1) numTests 
    FROM banner_sets S LEFT JOIN banner_votes V 
     ON V.Set1=S.SetId AND V.AuthorId=$memberId
	WHERE 1 
	GROUP BY S.SetId 
	ORDER BY numTests ASC, RAND() 
	LIMIT 1");	
  if ($bannerVote = DbFetchArray($result)) {
    $setA = $bannerVote['SetId'];
    //$WordsDisplay .= "A=$setA ";	
  }
  DbEndQuery($result);
  
  if ($setA>0) {
    // we wish to exclude any banners that have already been tested against setA
    $result = DbQuery("SELECT S.SetId, COUNT(V.Set1) numTests 
      FROM banner_sets S LEFT JOIN banner_votes V 
        ON V.Set1=S.SetId AND V.AuthorId=$memberId 
	  WHERE S.SetId<>$setA AND S.SetId NOT IN (SELECT Set1 FROM banner_votes WHERE AuthorId=$memberId AND Set2=$setA)
      GROUP BY S.SetId 
	  ORDER BY numTests ASC, RAND() 
	  LIMIT 1");
  
    if ($bannerVote = DbFetchArray($result)) {
      $setB = $bannerVote['SetId'];
      //$WordsDisplay .= "B=$setB ";	
    }
    DbEndQuery($result);
  }

  if ($setB>0) {
    $_SESSION['Banner1'] = $setA;
    $_SESSION['Banner2'] = $setB;
  }
  else {
    $_SESSION['Banner1'] = 0;
    $_SESSION['Banner2'] = 0;	
  }
  
  return $WordsDisplay;	
}

function CastVote($winner, $loser, $memberId) {
  DbInsert("banner_votes", array("AuthorId"=>$memberId, "Set1"=>$winner, "Set2"=>$loser, "Vote"=>1));
  DbInsert("banner_votes", array("AuthorId"=>$memberId, "Set1"=>$loser, "Set2"=>$winner, "Vote"=>0));
}

function ShowTallies($filter) {
  $WordsDisplay = '';
  
  $result = DbQuery("SELECT S.SetId, AVG(V.Vote) Approval, COUNT(V.Vote) Count
    FROM banner_sets S LEFT JOIN banner_votes V 
     ON V.Set1=S.SetId AND $filter
	WHERE 1 
	GROUP BY S.SetId 
	ORDER BY Approval DESC, Count DESC");	
  while ($bannerPreference = DbFetchArray($result)) {
    $WordsDisplay .= "<div style='background-color: #CCC;'>";
    $WordsDisplay .= FormatBannerSet($bannerPreference['SetId']);
    if ($bannerPreference['Count']>0)
	  $WordsDisplay .= "<p>Preference ".(100*$bannerPreference['Approval'])."%</p>";
	else  
	  $WordsDisplay .= "<p>Not yet rated.</p>";
    $WordsDisplay .= "</div>";
  }
  DbEndQuery($result);

  return $WordsDisplay;	
}

function ShowMyTallies($memberId) {
  $WordsDisplay = '';

  $WordsDisplay .= "<h3>My Voting Tallies</h3>";
  $WordsDisplay .= ShowTallies("V.AuthorId=$memberId");
  return $WordsDisplay;	
}

function ShowAllTallies() {
  $WordsDisplay = '';

  $WordsDisplay .= "<h3>Voting Tallies</h3>";
  $WordsDisplay .= ShowTallies("1");
  return $WordsDisplay;	
}

function FormatVotingCompletePage($memberId)
{
  $WordsDisplay = '';	
	
  $WordsDisplay .= "<p><b>Thank you. You've completed the banner set voting process.</b></p>";
  $WordsDisplay .= ShowMyTallies($memberId);  
  $WordsDisplay .= FormatBannerMenus(); 	
	
  return $WordsDisplay;	
}

  //-----------------------------------------------
  // parse the input
  $Banners = ReadBannerTable();
  
  if ($_GET['Action']=='Start') {
    $_SESSION['VoteState'] = '';
  }
  
  if ($_GET['Banner1']) {
    $_SESSION['Banner1'] = $_GET['Banner1']+0;
  }
  if ($_GET['Banner2']) {
    $_SESSION['Banner2'] = $_GET['Banner2']+0;
  }
  
  if ($_GET['bgcolor']) {
    $_SESSION['bgcolor'] = $_GET['bgcolor'];
  }
  else if (!$_SESSION['bgcolor']) {
    $_SESSION['bgcolor'] = 'CCC';	
  }

  if ($_POST['bannerset']=='A') {
    //$WordsDisplay .= " Voted for A<br >";	
    CastVote($_SESSION['Banner1'], $_SESSION['Banner2'], $member->GetMemberId());
    $WordsDisplay .= SelectPairToCompare($member->GetMemberId());
    if (!$_SESSION['Banner2']) $_SESSION['VoteState'] = "Voted";
    SetRedirect("");
  }
  else if ($_POST['bannerset']=='B') {
    //$WordsDisplay .= " Voted for B<br >";	
    CastVote($_SESSION['Banner2'], $_SESSION['Banner1'], $member->GetMemberId());
    $WordsDisplay .= SelectPairToCompare($member->GetMemberId());
    if (!$_SESSION['Banner2']) $_SESSION['VoteState'] = "Voted";
    SetRedirect("");
  }

  //-----------------------------------------------

  $WordsDisplay .= FormatBannerMenus();  
  if ($member->CanContribute() and ($member->Record('ApprovedCount')>=1)) {
     
    if ($member->CanAdministrate() and ($_GET['Action']=='ShowAllTallies')) {
	  $WordsDisplay .= ShowAllTallies($member->GetMemberId());
	}
    else if ($_GET['Action']=='ShowTallies') {
      $WordsDisplay .= ShowMyTallies($member->GetMemberId());
    }
    else if (!$_SESSION['VoteState']) { // first entry
      $WordsDisplay .= SelectPairToCompare($member->GetMemberId());
      if ($_SESSION['Banner2']) {
		$WordsDisplay .= "<h3>How does banner voting work?</h3><p>Rather than present all the banner sets at once and ask you to rank them, the software will present randomly selected pairs of banner sets and ask you to decide which of each pair you prefer. You can change the background color (white, black, gray) to help you visualize how the banners will appear on different websites. Each time you select a preferred banner set and press the button, the software will choose another pair for you to compare.</p>
		<p>The software will stop presenting new pairs when all possible comparisons have been tested, but you can stop at any time; all the decisions you've made will contribute to the voting tallies.</p>";
		$WordsDisplay .= "<p>".LinkSelf("", "I'm ready to start voting")."</p>";
	    $_SESSION['VoteState'] = "Voting";	
      } 
	  else {
	    $_SESSION['VoteState'] = "Voted";
		$WordsDisplay .= FormatVotingCompletePage($member->GetMemberId());
      }     
	}
    else if ($_SESSION['VoteState'] == "Voting") {
      $WordsDisplay .= CompareBannerSets($_SESSION['Banner1'], $_SESSION['Banner2'], $_SESSION['bgcolor']);		
	}	
    else  if ($_SESSION['VoteState'] == "Voted") {
		$WordsDisplay .= FormatVotingCompletePage($member->GetMemberId());
	}    
  }
  else 
    $WordsDisplay .= "<p>You must be logged on and have at least one approved limerick before you can vote.</p>";

  if ($RedirectLine) {
    header("Location: ".$RedirectLine );
  }    
  echo FormatOEDILFDocHeader();

  echo '<body ';
  if ($OnLoadLines) echo $OnLoadLines;
  echo '>';

  echo '<h1>OEDILF</h1><div class="subheading">The Omnificent English Dictionary In Limerick Form</div>';

  echo "<div id='content'>";
  echo $WordsDisplay;
  printf('<div class="copyright">'.AddBreaks($Configuration['Copyright']).'</div>');
  echo "</div>";

  LimDbDisconnect($dblink);

  echo "</body></html>";
?>