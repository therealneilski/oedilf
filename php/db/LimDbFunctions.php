<?php //limerick database access and formatting functions
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

require_once "LimGeneral.php";
require_once "LimConnect.php";

function DebugMessage($text) {
    LimGeneral::Log($text);
}

function Dump($var) {
    LimGeneral::VarDump($var);
}

function MsElapsed($since=0) {
    return LimGeneral::MsElapsed($since);
}

function LimDbConnect(&$dblink) {
	global $dbd, $dbu, $dbp, $dbs;
	$dblink = new LimDb($dbs, $dbu, $dbp, $dbd);
}

function LimDbDisconnect(&$dblink) {
	$dblink->close();
}

function LimGetHandle() {
	global $dblink;
    return $dblink;
}

function DbEscapeString($text) {
	global $dblink;
	return $dblink->real_escape_string($text);
}

function DbQuery($query) {
	global $dblink;
	return $dblink->query($query);
}

function DbEndQuery($queryResult) {
    $queryResult->close();
}

function DbQueryRows($queryResult) {
	return $queryResult->num_rows;
}

function DbQuerySeek($queryResult, $row) {
	return $queryResult->data_seek($row);
}

function DbFetchArray($queryResult) {
	return $queryResult->fetch_assoc();
}

function DbInsert($tableName, $record) {
	$fields = implode(",", array_keys($record));
	$values = implode("','", $record);
	return DbQuery("INSERT INTO $tableName ($fields) VALUES ('$values')"); 
}

function DbUpdate($tableName, $record, $where, $limit) {
	$setList = array();
	foreach ($record as $name=>$value)
		$setList[] = $name."='$value'";
	$sets = implode(",", $setList);
	return DbQuery("UPDATE $tableName SET $sets WHERE $where LIMIT $limit");
}

function DbLastInsert() {
	global $dblink;
	return $dblink->insert_id;
}

function DbAffectedRows() {
	global $dblink;
	return $dblink->affected_rows;
}

function DebugSelection($Heading, $Query) {
  global $dblink;

  $WordsDisplay = '';
  $result = DbQuery($Query);
  $WordsDisplay .= "<p>$Heading<br >$Query<br >";

  $doneHeading = false;
  $WordsDisplay .= "<table>";
  while ($line = DbFetchArray($result)) {
    $stuff = $line;
    if (!$doneHeading) {
      $WordsDisplay .= "<tr>";
      foreach($stuff as $field => $value) {
        $WordsDisplay .= "<th>".$field."</th>";
      }
      $WordsDisplay .= "\n";
      $doneHeading = true;
      $WordsDisplay .= "</tr>";
    }
    $WordsDisplay .= "<tr>";
    foreach($line as $field => $value) {
      $WordsDisplay .= "<td>".$value."</td>";
    }
    $WordsDisplay .= "</tr>";
    $WordsDisplay .= "\n";
  }
  $WordsDisplay .= "</table><br >";

  DbEndQuery($result);
  return $WordsDisplay;
}

function TabulateAuthors(){
  global $dblink, $AuthorNameTable, $AuthorLCNameTable;

  $AuthorNameTable = array();
  $AuthorNameTable[0] = '(undefined)';
  $result = DbQuery("SELECT AuthorId, Name FROM DILF_Authors WHERE 1");
  while ($line = DbFetchArray($result)) {
    $AuthorNameTable[$line['AuthorId']] = $line['Name'];
    $AuthorLCNameTable[$line['AuthorId']] = strtolower($line['Name']); // keep a lower case version for case insensitive searches
  }
  DbEndQuery($result);
}

function GetAuthor($AuthorId, $IncludeTwitter = FALSE) {
  global $AuthorNameTable;
  if (!isset($AuthorNameTable)) TabulateAuthors();
  if (isset($AuthorNameTable[$AuthorId])) $return = $AuthorNameTable[$AuthorId];
  else return "(undefined)";
  if ($IncludeTwitter) {
    $twitter = GetAuthorTwitterHandle($AuthorId);
    if ($twitter) {
      $return .= " (@{$twitter})";
    }
  }
  return $return;
}

function GetAuthorHtml($AuthorId, $IncludeTwitter = FALSE) {
    return htmlspecialchars(GetAuthor($AuthorId, $IncludeTwitter), ENT_QUOTES);
}

function GetAuthorId($Author) {
  global $AuthorLCNameTable;
  if (!isset($AuthorLCNameTable)) TabulateAuthors();
  return array_search(strtolower($Author), $AuthorLCNameTable);
}

function GetAuthorTwitterHandle($AuthorId) {
  $result = DbQuery(sprintf("SELECT TwitterHandle FROM DILF_Authors WHERE AuthorId = %d", $AuthorId));
  $line = DbFetchArray($result);
  DbEndQuery($result);
  if ($line)
    return $line['TwitterHandle'];
  else
    return '';
}

function GetVerse($VerseId) {
	global $dblink;
  $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE VerseId=%d LIMIT 1", $VerseId));
  $line = DbFetchArray($result);
  DbEndQuery($result);
  return $line;
}

function GetOriginalId($VerseId) {
  $line = GetVerse($VerseId);
  return $line['OriginalId'];
}

function GetDocument($docName) {
	$docText = FALSE;
	$result = DbQuery("SELECT Text FROM DILF_Documents WHERE Title='$docName' LIMIT 1");
	if ($line = DbFetchArray($result)) {
		$docText = $line['Text'];
	}
	DbEndQuery($result);
	return $docText;
}

?>
