<?php
class LimSqlTableFormatter
{
	protected $query;
	protected $columns;
	protected $start;
	protected $pagesize;
	protected $linkparams;
	public $HeadingText = "";
	public $NumMatches = 0;
	
	public function __construct($query, $columns, $start, $pagesize, $linkparams)
	{
		$this->query = $query;
		$this->columns = $columns;
		$this->start = max($start, 0);
		$this->pagesize = $pagesize;
		$this->linkparams = $linkparams;
	}
	
	public function FormatTable($html)
	{
		$result = DbQuery($this->query);
		$this->NumMatches = DbQueryRows($result);
		
		$pages = new LimPaginator($this->linkparams, $this->NumMatches, $this->pagesize, $this->start);
		
		if ($this->HeadingText)
			$html->Heading($this->HeadingText.' '.$pages->CurrRange(), 3);
		
		if ($this->NumMatches>0)
		{
			if ($this->NumMatches>$this->pagesize)
				$html->Text($pages->FormatNeatMultiplePageLinks());
			$html->BeginTable("", "widetable");
			
			DbQuerySeek($result, $this->start);
			
			for ($i=$this->start; ($i<$this->NumMatches) and ($i<$this->start+$this->pagesize); $i++)
			{
				$line = DbFetchArray($result) ;
				if ($i==$this->start) 
				{
					$html->BeginTableRow();
					foreach ($this->columns as $colHeading => $cellFormatter)
						$html->TableHeadingCell($cellFormatter->FormatHeaderCell($colHeading));
					$html->EndTableRow();
				}
			
				$html->BeginTableRow();
				foreach ($this->columns as $colHeading => $cellFormatter)
					$html->TableCell($cellFormatter->FormatCell($line[$colHeading]), "", "randompanel");
				$html->EndTableRow();
			}
			$html->EndTable();
		}
		else
		{
			$html->BeginTable("", "widetable");
			$html->BeginTableRow();
			$html->TableCell("None found.");
			$html->EndTableRow();
			$html->EndTable();
		}
		DbEndQuery($result);
		
		$html->Text($pages->FormatPaginationMenu());
	}
}
?>