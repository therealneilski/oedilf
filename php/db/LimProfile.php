<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimProfile
{
	protected $member;
	
	public function __construct($member)
	{
		$this->member = $member;	
	}

	public function ParseInput($html)
	{
		global $PageTitle;
		
		StripMagicQuotes();
		if (($_GET['Profile']=='Show') or
			(($_GET['Profile']=='EditForm') and (isset($_POST['EditProfileCancelButton']))))
		{
			$PageTitle = "Profile";
			$this->FormatProfile($html);
		}
		else if ($_GET['Profile']=='Edit')
		{
			$PageTitle = "Edit Profile";
			$this->FormatEditProfile($html);
		}
		else if (($_GET['Profile']=='EditForm') and (isset($_POST['EditProfileUpdateButton'])))
		{
			$PageTitle = "Edit Profile";
			$this->ProcessEditProfile($html);
		}
		else if ($_GET['Profile']=='Location')
		{
			$PageTitle = 'Map Location Update';
			$this->ProcessMapLocation($html, $_GET['Lat']+0, $_GET['Lng']+0);
		}
		else BugOrHack();
	}
	
	public function FormatProfile($html)
	{
		if ($this->member->CanContribute()) 
		{
			$this->FormatProfileMenu($html);
			$html->Heading("Profile Information for ".htmlspecialchars($this->member->GetMemberName(), ENT_QUOTES), 3);
			$this->FormatPersonalStatsDisplay($html);
			$this->FormatProfileMenu($html);
		}
		else
			$html->Paragraph('You must have a confirmed membership and be logged in before you can view this page.');
	}
	
	public function ProcessMapLocation($html, $Lat, $Lng) 
	{
		if ($this->member->CanContribute()) {
			$this->member->SetLocation($Lat, $Lng);
			$html->Paragraph("Updated your world map location.");
			$this->LinkBackToProfile($html);
			SetReload('Profile=Show');
		}
		else
			$html->Paragraph('You must have a confirmed membership and be logged in before you can update your location.');
	}
	
	public function FormatEditProfile($html)
	{
		if ($this->member->CanContribute()) 
		{
			$html->Heading("Edit Profile", 3);
            $html->BeginDiv("hinttext");
            $html->Text("Except where otherwise noted, information entered here may be visible to the public on various pages of this website.");
            $html->EndDiv();
			if ($this->member->CanWorkshop())
				$form = self::BuildWSProfileForm();
			else
				$form = self::BuildContributorProfileForm();
			$form->FormatHeader($html, "Profile=EditForm");
			$form->FormatBody($html, $this->member->GetRecord());
			$form->FormatFooter($html);
		}
		else
			$html->Paragraph('You must be logged in to edit your profile information.');
	}

	public function ProcessEditProfile($html)
	{
		if ($this->member->CanContribute()) 
		{
			if ($this->member->CanWorkshop())
				$form = self::BuildWSProfileForm();
			else
				$form = self::BuildContributorProfileForm();
				
			$messages = $form->InvalidEntryMessages($_POST);
			if (($_POST['Email']!='') and !ValidEmail($_POST['Email']))
				$messages[] = htmlspecialchars("Email address is badly formed: ".$_POST['Email'], ENT_QUOTES);
		
			if (!empty($messages))
			{
				$html->Paragraph(FormatWarning(implode("<br>", $messages)));
				$html->Paragraph("Your profile was not updated.");
				$html->BeginParagraph();
				$html->LinkSelf("Profile=Edit", "Back to profile editing");
				$html->EndParagraph();
			}
			else
			{
		    	$_POST['Biography'] = CleanupHtml($_POST['Biography']);
			while ($_POST['TwitterHandle']{0} == '@')
				$_POST['TwitterHandle'] = substr($_POST['TwitterHandle'], 1);
		    	$updates = $form->GetUpdateRecord($_POST);
				$updates = array_map(DbEscapeString, $updates);
				DbUpdate("DILF_Authors", $updates, "AuthorId=".$this->member->GetMemberId(), 1);
		        
				$html->Paragraph('Profile information updated.');
				AuditLog(0, 'Updated Profile Settings');
				SetReload('Profile=Show');
			}			
		}
		else
			$html->Paragraph('You must be logged in to edit your profile information.');
			
		$this->LinkBackToProfile($html);
	}
	
	protected function LinkBackToProfile($html)
	{
		$html->BeginParagraph();
		$html->LinkSelf("Profile=Show", "Back to profile");
		$html->EndParagraph();
	}
	
	protected static function BuildContributorProfileForm()
	{
		$form = new LimFormBuilder('EditProfile', true);
		self::AddEmailNotifyFields($form);
		self::AddWorkshopFields($form, false);			
		self::AddPersonalFields($form);
		return $form;
	}
	
	protected static function BuildWSProfileForm()
	{
		$form = new LimFormBuilder('EditProfile', true);
		self::AddEmailNotifyFields($form);
		self::AddMonitorFields($form);
		self::AddWorkshopFields($form, true);
		self::AddPersonalFields($form);
		return $form;
	}
	
	public static function BuildProfileForm()
	{
		$form = new LimFormBuilder('MemberProfile', true);
		$form->AddField(new LimFieldNonNullText('Name', 'Name', true));
		$form->AddField(new LimFieldConfirmedPassword('Password', 'Password (private)'));
		
		self::AddEmailNotifyFields($form);
		self::AddMonitorFields($form);
		self::AddWorkshopFields($form, true);
		
		$form->AddField(new LimFieldOffOn('JuniorMode', 'Junior Mode (locked curtain)'));
		self::AddPersonalFields($form);
		$form->AddField(new LimFieldText('Lat', 'Lattitude'));
		$form->AddField(new LimFieldText('Lng', 'Longitude'));
		return $form;
	}

	protected static function AddWorkshopFields($form, $allowMacros)
	{
		if ($allowMacros)
			$form->AddField(new LimFieldOffOn('TextMacroButtons', 'Text Macro Buttons'));
		$form->AddField(new LimFieldSelect('WSCommentDisplayLimit', 'WS Comment Display Limit', 
			array('25'=>'25', '50'=>'50', 'Unlimited'=>'Unlimited')));
		$feedbackField = new LimFieldFeedbackStyle('FeedbackStyle', 'Feedback Style', 60);
		$feedbackField->SetHint(
"* We recommend against these two options.<br>
<br>
Prohibiting workshopping editors from offering you<br>
suggestions severely limits their ability to help<br>
you improve your writing and, as a result, it may<br>
take significantly longer for your submissions to<br>
attain final approval.<br>
<br>
Note that you are never obligated to adopt a workshopping suggestion.");
		$form->AddField($feedbackField);	
	}
	
	protected static function AddMonitorFields($form)
	{
		$form->AddField(new LimFieldSelect('MonitorComments', 'Monitor Comments', 
			array('Off'=>'Only on my limericks', 'Workshopped'=>"On ones I've workshopped", 'All'=>'All')));
		$form->AddField(new LimFieldSelect('MonitorNew', 'Monitor New Limericks', 
			array('Off'=>'Off', 'Held'=>'Held', 'All'=>'All')));
		$form->AddField(new LimFieldOffOn('MonitorAttention', 'Monitor Calls For Attention'));
		$form->AddField(new LimFieldSelect('MonitorRFAs', 'Monitor RFAs',
			array('All'=>'Notify me on any activity',
				'Revision'=>'Notify me when the limerick is revised',
				'State'=>'Notify me when my RFA is cleared or the author self-RFAs',
				'Off'=>'No special post-RFA notifications')));
	}
	
	protected static function AddPersonalFields($form)
	{
		$form->AddField(new LimFieldOffOn('CurtainFiltering', 'Curtain Filtering'));
		$form->AddField(new LimFieldTimeZone('TimeZone', 'Time Zone'));
		$form->AddField(new LimFieldText('RealName', 'Real Name'));
		$twitterField = new LimFieldText('TwitterHandle', 'Twitter Handle');
		$twitterField->SetHint("Omit the leading @ - e.g. enter OEDILF, not @OEDILF");
		$form->AddField($twitterField);
		$form->AddField(new LimFieldTextArea('Biography', 'Biography', 25, 60));
		$form->AddField(new LimFieldSelect('BornIn', 'Born In', self::GetCountryCodes()));
		$form->AddField(new LimFieldSelect('LivingIn', 'Living In', self::GetCountryCodes()));
	}
	
	protected static function AddEmailNotifyFields($form)
	{
		$form->AddField(new LimFieldText('Email', 'Email (private)'));
        $form->AddField(new LimFieldOffOn('ApprovalEmails', 'Email when a limerick needs my approval'));
        $form->AddField(new LimFieldOffOn('MilestoneEmails', 'Email when I gain access to new site features'));
        $form->AddField(new LimFieldOffOn('NewsEmails', 'Email occasional newsletters and programs'));
		$form->AddField(new LimFieldOffOn('NotifyChanges', 'Monitor changes and comments on my limericks'));
	}
	
	protected static $countryCodes = array();
	protected static function GetCountryCodes()
	{
		if (count(self::$countryCodes)==0)
		{
			$result = DbQuery("SELECT * FROM DILF_Countries WHERE 1 ORDER BY Name");
			while ($line = DbFetchArray($result)) {
				self::$countryCodes[$line['Code']] = $line['Name'];
			}
			DbEndQuery($result);
		}
		return self::$countryCodes;
	}

	protected static function WSCommentDisplayLimitChoices()
	{
		return array('25'=>'25', '50'=>'50', 'Unlimited'=>'Unlimited');
	}
	
	protected function FormatProfileMenu($html)
	{
		$html->BeginTable("ProfileMenu", "menutable");
		$html->BeginTableRow();
		$html->BeginTableCell("", "menus");
		$html->NavLinkSelf("Profile=Edit", "Edit Profile Settings");
		$html->NavLinkSelf("LoginManager=ChangePassword", "Change Password");
		$html->NavLinkSelf(array("ShowcaseAction=Author", 
				"ShowcaseAuthor=".$this->member->GetMemberId()), 
			"View Showcase/Bio");
		if ($this->member->CanSetLocation())
			$html->Text(FormatMapLink("nav"));
		$html->EndTableCell();
		$html->EndTableRow();
		$html->EndTable();
	}

	protected function FormatPersonalStatsDisplay($html) 
	{
		$html->BeginTable("PersonalStats", "widetable");
		$result = DbQuery(sprintf("SELECT *, C1.Name Born, C1.Flag BornFlag, C2.Name Living, C2.Flag LivingFlag 
			FROM DILF_Authors A, DILF_Ranks R, DILF_Countries C1, DILF_Countries C2 
			WHERE A.Rank=R.Rank AND A.AuthorId=%d AND C1.Code=A.BornIn AND C2.Code=A.LivingIn",
			$this->member->GetMemberId()));
		if ($ALine=DbFetchArray($result)) {
			$html->BeginTableRow();
			$html->TableHeadingCell("Membership", 2);
			$html->EndTableRow();
			self::TableRowTwoCols($html, "Your Title:", htmlspecialchars($ALine['Title'], ENT_QUOTES));
			self::TableRowTwoCols($html, "You have", 
				htmlspecialchars($ALine['UserType'].$ALine['AddPrivileges']." privileges", ENT_QUOTES));
			$ShowcaseLimit = $ALine['ShowcaseLimit'];
		}
		DbEndQuery($result);
	
		$result = DbQuery(sprintf("SELECT State, COUNT(State) RawCount 
			FROM DILF_Limericks WHERE PrimaryAuthorId=%d OR SecondaryAuthorId=%d 
			GROUP BY State ORDER BY State",
			$this->member->GetMemberId(), $this->member->GetMemberId()));
		if (DbQueryRows($result)>0) {
			$html->BeginTableRow();
			$html->TableHeadingCell("Limerick Count");
			$html->TableHeadingCell("Total (including co-authored limericks)");
			$html->EndTableRow();
			while ($line=DbFetchArray($result)) 
			{
				if ($line['State']<>'obsolete') 
					self::TableRowTwoCols($html, $line['State'], $line['RawCount']);
			}
		}
		DbEndQuery($result);
	
		$html->BeginTableRow();
		$html->TableHeadingCell("Showcase", 2);
		$html->EndTableRow();
		
		$approved = $this->member->WeightedCount('approved');
		$solo = $this->member->SoleAuthoredCount('approved');
		$duo = $this->member->CoauthoredCount('approved');
		self::TableRowTwoCols($html, "Weighted approved count:", 
			htmlspecialchars($approved." ($solo as sole author + 0.5 x $duo shared with a coauthor)", ENT_QUOTES));
		
		
		$limit = LimMember::ShowcaseLimit($approved);
		self::TableRowTwoCols($html, "Showcase Allowance:", 
			htmlspecialchars(LimMember::ShowcaseLimitPercent($approved).
				"% of your approved limericks i.e. ".$limit, ENT_QUOTES));
		
		$showcased = $this->member->ShowcasedLimericks();
		$totalShowcased = $showcased['Unfiltered'] + $showcased['Filtered'];
		
		$message = $totalShowcased;
		if ($limit>0) 
			$message .= " ".LinkSelf("ShowcaseAction=Fodder", "List eligible limericks");
			
		self::TableRowTwoCols($html, "Limericks selected for showcase:", $message);
		if ($totalShowcased>$limit)
			self::TableRowTwoCols($html, "Displayed in unfiltered showcase:", $limit);
		if (($showcased['Filtered']>0) and ($showcased['Unfiltered']<$limit))
			self::TableRowTwoCols($html, "Displayed in curtain filtered showcase:", $showcased['Unfiltered']);
			
		$html->BeginTableRow();
		$html->TableHeadingCell("Email Settings", 2);
		$html->EndTableRow();
		self::TableRowTwoCols($html, "Email Address:", htmlspecialchars($ALine['Email'], ENT_QUOTES));
		self::TableRowTwoCols($html, "Limerick Approval Emails:", htmlspecialchars($ALine['ApprovalEmails'], ENT_QUOTES));
		self::TableRowTwoCols($html, "New Privilege Emails:", htmlspecialchars($ALine['MilestoneEmails'], ENT_QUOTES));
		self::TableRowTwoCols($html, "Newsletter and Program Emails:", htmlspecialchars($ALine['NewsEmails'], ENT_QUOTES));

		$html->BeginTableRow();
		$html->TableHeadingCell("Activity List Settings", 2);
		$html->EndTableRow();
		self::TableRowTwoCols($html, "Monitor Changes and Comments on My Limericks:", htmlspecialchars($ALine['NotifyChanges'], ENT_QUOTES));
		if ($this->member->CanWorkshop()) {
			self::TableRowTwoCols($html, "Monitor Comments:", htmlspecialchars($ALine['MonitorComments'], ENT_QUOTES));
			self::TableRowTwoCols($html, "Monitor New Limericks:", htmlspecialchars($ALine['MonitorNew'], ENT_QUOTES));
			self::TableRowTwoCols($html, "Monitor Attention Calls:", htmlspecialchars($ALine['MonitorAttention'], ENT_QUOTES));
			self::TableRowTwoCols($html, "Monitor RFAs:", htmlspecialchars($ALine['MonitorRFAs'], ENT_QUOTES));
        }

		$html->BeginTableRow();
		$html->TableHeadingCell("Miscellaneous Settings", 2);
		$html->EndTableRow();
		if ($this->member->CanWorkshop()) {
			self::TableRowTwoCols($html, "Text Macro Buttons:", htmlspecialchars($ALine['TextMacroButtons'], ENT_QUOTES));
		}
		self::TableRowTwoCols($html, "WS Comment Display Limit:", htmlspecialchars($ALine['WSCommentDisplayLimit'], ENT_QUOTES));
		self::TableRowTwoCols($html, "Feedback Style:", 
			htmlspecialchars(LimFieldFeedbackStyle::ExpandToText($ALine['FeedbackStyle']), ENT_QUOTES));
			
		self::TableRowTwoCols($html, "Curtain Filtering:", htmlspecialchars($ALine['CurtainFiltering'], ENT_QUOTES));
		self::TableRowTwoCols($html, "Time Zone:", htmlspecialchars("GMT".(($ALine['TimeZone']>=0)? "+" : "").$ALine['TimeZone'], ENT_QUOTES));

		$html->BeginTableRow();
		$html->TableHeadingCell("Showcase Information", 2);
		$html->EndTableRow();
		self::TableRowTwoCols($html, "Real Name:", htmlspecialchars($ALine['RealName'], ENT_QUOTES));
		self::TableRowTwoCols($html, "Biography:", FormatQuickDocLinks(AddBreaks($ALine['Biography'])));
		if ($ALine['TwitterHandle'])
			self::TableRowTwoCols($html, "Twitter:", '<a href="https://twitter.com/' . htmlspecialchars($ALine['TwitterHandle']) . '">@' . htmlspecialchars($ALine['TwitterHandle']) . "</a>"); 
		else
			self::TableRowTwoCols($html, "Twitter:", "");
		self::TableRowTwoCols($html, "Born In:", 
			$ALine['Born']." ".($ALine[BornFlag]? '<img src="flags/'.$ALine[BornFlag].'" >':''));
		self::TableRowTwoCols($html, "Living In:", 
			$ALine['Living']." ".($ALine[LivingFlag]? '<img src="flags/'.$ALine[LivingFlag].'" >':''));
	
		$html->EndTable();
	}
	
	protected static function TableRowTwoCols($html, $cell1, $cell2)
	{
		$html->BeginTableRow();
		$html->TableCell($cell1, "", "darkpanel");
		$html->TableCell($cell2, "", "lightpanel");
		$html->EndTableRow();	
	}
}
?>
