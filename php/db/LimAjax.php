<?php
define('IN_LIM', true);
define('PHPSELF', $_SERVER['PHP_SELF']);
require_once "LimGeneral.php";
require_once "LimDb.php";
require_once "LimDbFunctions.php";

LimDbConnect($dblink);

if (get_magic_quotes_gpc())
{
	function stripslashes_deep(&$value)
	{
		$value = is_array($value) ?
			array_map('stripslashes_deep', $value) :
			stripslashes($value);

		return $value;
	}
	stripslashes_deep($_GET);  
}

if (isset($_GET['AuthorName']))
{
	$id = "'".$_GET['Target']."'";
	$match = DbEscapeString($_GET['AuthorName']);
	$result = DbQuery("SELECT Name FROM DILF_Authors WHERE UserType<>'banned' AND Name LIKE '$match%'
		ORDER BY Name LIMIT 21");
	$count = 0;
	while (($line = DbFetchArray($result)) && $count++ < 20) {
		$name = "'".$line['Name']."'";
		print '<a href="javascript:setFieldEntry('.$id.', '.$name.')">'.htmlspecialchars($line['Name'], ENT_QUOTES)."</a> ";
		if ($count==20) print "...";
	}
	DbEndQuery($result);
}
else if (isset($_GET['Button']))
{
	//var_dump($_GET);
	$macros = new LimMacro('');
	$result = DbQuery("SELECT Text FROM DILF_Documents WHERE Title LIKE 'Buttons for %'");
	while ($line = DbFetchArray($result))
		$macros->ParseMacrosFrom($line['Text']);
	DbEndQuery($result);
	
	$macroText = $macros->FormatAjaxText($_GET['Button']);
	if (strpos($macroText, "@@")!==false) 
	{
		require_once "LimFormat.php";
		$result = DbQuery(sprintf("SELECT * FROM DILF_Settings WHERE 1"));
		$Configuration = DbFetchArray($result) ;
		DbEndQuery($result);
		
		$Letters = LastValidLetters($Configuration['AlphabetEnd']);    
		$authorId = $_GET['ButtonAuthor']+0;
		$macros->SetSubstitutions(array("@@AUTHOR@@", "@@LETTERS@@"), 
			array(GetAuthorHtml($authorId), $Letters));
		
		$macroText = $macros->TemplateSubstitute($macroText);
	}
	print $macroText;
}
LimDbDisconnect($dblink);
?>