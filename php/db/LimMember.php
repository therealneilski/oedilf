<?php
// Member information

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimMember {
	protected $Record;
  
	public function __construct($memberid=0, $passwordHash='')
	{
		if ($memberid) 
		{
            if ($passwordHash)
                $result = DbQuery("SELECT * FROM DILF_Authors WHERE AuthorId=".intval($memberid).
                    " AND Password='".DbEscapeString($passwordHash)."'");
            else
                $result = DbQuery("SELECT * FROM DILF_Authors WHERE AuthorId=".intval($memberid));
			if (DbQueryRows($result) > 0) 
			{
				$this->Record = DbFetchArray($result) ;
                if (!$this->Record['Token'])
                    $this->CreateToken();
			}
			DbEndQuery($result);
		}
		else $this->Record = array();
	}

    protected function CreateToken()
    {
        $this->Record['Token'] = md5(rand().time());
        LimGeneral::Log("Setting token to ".$this->Record['Token']);
        DbUpdate("DILF_Authors", array("Token"=>$this->Record['Token']),
            "AuthorId=".$this->GetMemberId(), 1);
    }

    public static function CheckLogin()
    {
        global $member;
        $loginError = '';

        if (LimSession::LoggedIn())
        {
            LimGeneral::Log("Connecting to existing session.");
            $member = new LimMember(LimSession::MemberId());
            $loginError = $member->CheckLoginState();
            LimGeneral::Log($loginError);
            if ($loginError<>'') $loginError = '<p>'.$loginError.'</p>';
        }
        else
            $member = new LimMember;

        if (!LimSession::LoggedIn())
        {
            list($id, $token) = LimSession::DecodeLoginCookieToken();
            if ($id)
            {
                LimGeneral::Log("Logging in from remembered login on this machine.");
                $member = new LimMember($id);
                if ($member->Record('Token')==$token) {
                    LimSession::SetLoggedIn($id, $member->Record('Password'),
                        LimSession::GetCookieRememberToken()) ;
                    $loginError = '';
                    $member->StampTime();
                    PostLoginActions();
                }
            }
        }
        return $loginError;
    }
	
	public function CheckLoginState()
	{
		$LoginError = '';

		if ($this->Record['Password']!=LimSession::GetHashedPassword())
			$LoginError .= 'Invalid username/password combination.';
		else if ($this->Record['UserType'] == 'banned')
			$LoginError .= 'Account inactive or suspended. If you wish to resume active membership of the OEDILF, contact the EiC via email.';
		// check for unapproved application
		else if ($this->Record['UserType'] == 'applicant')
		{
			$maskedmail = "<script type=\"text/javascript\">var first = '<a href=\"mailto&#58;virge';var second = '&#64;oedilf.com\">Virge</a>'; document.write(first+second); </script>";
			$LoginError .= "<p>Member application not yet approved.</p><p>An email was sent to your email account with instructions for confirming your registration. You may log in and post limericks once you have confirmed your registration using the information in the email. (If your email account has spam filtering enabled, you may need to check that it hasn't blocked the registration email or filed it as unsolicited junk mail.) If you didn't receive the email within half an hour of registration, please contact $maskedmail.</p>";
		}
        else
        {
			$this->StampTime();
            LimSession::RefreshLoginCookies($this->GetMemberId(), LimSession::GetCookieRememberToken());
		}
        if ($LoginError) LimSession::SetLoggedOut(false);
		return $LoginError;
	}

	public function GetRecord()
	{
		return $this->Record;
	}
	
	public function Record($entry)
	{
		return $this->Record[$entry];
	}

	protected $stats = false;
    protected $totalLims = 0;
	protected function GetStats()
	{
		if ($this->stats===false)
		{
			$id=$this->Record['AuthorId'];
			$result = DbQuery("SELECT State, SUM(SecondaryAuthorId=0) SoloCount, SUM(SecondaryAuthorId<>0) DuoCount
				FROM DILF_Limericks WHERE (PrimaryAuthorId=$id OR SecondaryAuthorId=$id)
				GROUP BY State");
			while ($line = DbFetchArray($result))
            {
				$this->stats[$line['State']] = $line ;
                if ($line['State']!='obsolete')
                    $this->totalLims += $line[SoloCount] + $line[DuoCount];
            }
			DbEndQuery($result);
		}		
	}

	public function GetTotalLimCount()
    {
		$this->GetStats();
        return $this->totalLims;
    }

	public function SoleAuthoredCount($state)
	{
		$this->GetStats();
		if (isset($this->stats[$state])) return $this->stats[$state]['SoloCount'];
		else return 0;		
	}
	
	public function CoauthoredCount($state)
	{
		$this->GetStats();
		if (isset($this->stats[$state])) return $this->stats[$state]['DuoCount'];
		else return 0;		
	}

	public function WeightedCount($state)
	{
		$this->GetStats();
		if (isset($this->stats[$state])) 
			return $this->stats[$state]['SoloCount'] + $this->stats[$state]['DuoCount']/2;
		else return 0;		
	}
	
	public function ShowcasedLimericks()
	{
		$id=$this->Record['AuthorId'];
		$result = DbQuery("SELECT SUM(Category='normal') Unfiltered, SUM(Category<>'normal') Filtered
			FROM DILF_Limericks WHERE PrimaryAuthorId=$id AND State='approved' AND ShowcasePriority>0
			GROUP BY State");
		$showcase = DbFetchArray($result); 
		DbEndQuery($result);
		return $showcase;
	}
	
	public function SetLocation($Lat, $Lng)
	{
		$this->Record[$Lat] = $Lat;
		$this->Record[$Lng] = $Lng;
		$result = DbQuery("UPDATE DILF_Authors SET Lat=$Lat, Lng=$Lng WHERE AuthorId=".
			$this->Record['AuthorId']." LIMIT 1");
	}
  
	public function StampTime() 
	{
		$result = DbQuery(sprintf("UPDATE DILF_Authors SET AccessTime = '%s' 
			WHERE AuthorId=%d AND Password='%s' LIMIT 1",
			time(), $this->Record['AuthorId'], $this->Record['Password']));
	}
  
	public function GetMemberId()
	{
		if (isset($this->Record['AuthorId'])) return $this->Record['AuthorId'];
		else return 0;
	}
	
	public function GetMemberName()
	{
		return $this->Record['Name'];
	}
  
	public function CanContribute() {
		return (LimSession::LoggedIn()) and in_array($this->Record['UserType'],
			array('member', 'workshopper', 'editor', 'administrator', 'senior workshopper'));
	}
	
	public function CanShowcase() {
		return (LimSession::LoggedIn()) and !in_array($this->Record['Rank'], array('R', 'C'));
	}
	
	public function CanSetLocation() {
		return (LimSession::LoggedIn()) and !in_array($this->Record['Rank'], array('R'));
	}
	
	public function CanChat() {
		return ($this->CanContribute() and ($this->Record['ApprovedCount']>=1));
	}
	
	public function CanWorkshop() {
		return (LimSession::LoggedIn()) and in_array($this->Record['UserType'], array('workshopper', 'editor', 'administrator', 'senior workshopper'));
	}
	
	public function CanWorkshopPlus() {
		return (LimSession::LoggedIn()) and in_array($this->Record['UserType'], array('editor', 'administrator', 'senior workshopper'));
	}
	
	public function CanEdit() {
		return (LimSession::LoggedIn()) and in_array($this->Record['UserType'], array('editor', 'administrator'));
	}
	
	public function CanAdministrate() {
		return (LimSession::LoggedIn()) and in_array($this->Record['UserType'], array('administrator'));
	}
	
	public function HasPrivilege($PrivilegeName) {
		// Administrators get all privileges whether listed on not
		return (LimSession::LoggedIn()) and ($this->CanAdministrate() or (strpos($this->Record['AddPrivileges'], '+'.$PrivilegeName)!==FALSE));
	}

	public function HasLimit($LimitName) {
		// Administrators don't automatically get limitations
		return (LimSession::LoggedIn()) and ((strpos($this->Record['AddPrivileges'], '+'.$LimitName)!==FALSE));
	}

    public function IsFiltered() {
        return (($this->Record('JuniorMode')=='On') or ($this->Record('CurtainFiltering')=='On'));
    }
	
	protected static $limitTable = array(
		10=>10,
		50=>11,
		100=>12,
		250=>13,
		500=>14,
		1000=>15,
		3000=>16,
		6000=>17,
		10000=>18,
		15000=>19,
		20000=>20
	);
	public static function ShowcaseLimitPercent($approved)
	{
		$limit = 0;
		foreach (self::$limitTable as $count=>$percent)
		{
			if ($approved>=$count) $limit = $percent;
		}
		return $limit;
	}
	
	public static function ShowcaseLimit($approved)
	{
		return floor($approved * self::ShowcaseLimitPercent($approved) / 100);
	}
}
?>
