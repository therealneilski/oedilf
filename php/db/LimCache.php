<?php

function CacheNewApprovedLims()
{
	$numLims = 10;
	$formatter = new LimTopicFormatter(LimSession::LoggedIn(), "class='widetable'");
	$NewLims = FormatLimerickList('',
		"SELECT * FROM DILF_Limericks WHERE State='approved'
		ORDER BY LimerickNumber DESC LIMIT ".$numLims, 
		0, $numLims, array("Show=Recent", "PageLen=$numLims"), 
		$NumLimericks, $formatter); 
	ReplaceDocument("CachedNewLims", $NewLims);
	
	$NewFilteredLims = FormatLimerickList('',
		"SELECT * FROM DILF_Limericks WHERE State='approved' AND Category='normal'
		ORDER BY LimerickNumber DESC LIMIT ".$numLims, 
		0, $numLims, array("Show=Recent", "PageLen=$numLims"), 
		$NumLimericks, $formatter); 
	ReplaceDocument("CachedNewFilteredLims", $NewFilteredLims);
}

function ReplaceDocument($name, $text)
{
	$old = GetDocument($name);
	if ($old !== false)
		DbUpdate("DILF_Documents", array('Text'=>DbEscapeString($text)), "Title='$name'", 1);
	else
		DbInsert("DILF_Documents", array('Title'=>$name, 'Text'=>DbEscapeString($text)));
}

function GetNewApprovedLims($curtained)
{
	if ($curtained) $title = "CachedNewFilteredLims";
	else $title = "CachedNewLims";
	
	$text = GetDocument($title);
	if ($text===false)
	{
		CacheNewApprovedLims();
		$text = GetDocument($title);
	}
	return $text;
}


?>