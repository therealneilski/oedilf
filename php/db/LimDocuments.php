<?php
class LimDocuments
{
	public function ParseInput($html)
	{
		global $PageTitle;
		
		StripMagicQuotes();
		
		if (($_GET['Documents']=='Show') or
			isset($_POST['EditDocumentCancelButton']))
		{
			$PageTitle = "Documents";
			$this->FormatDocumentTable($html);
		}
		else if ($_GET['Documents']=='Edit')
		{
			$PageTitle = "Edit Document";
			$this->FormatEditDocument($html);
		}
		else if (($_GET['Documents']=='Edited') and
			isset($_POST['EditDocumentUpdateButton']))
		{
			$PageTitle = "Edited Document";
			$this->ProcessEditedDocument($html);
		}
		else if (($_GET['Documents']=='New') and
			isset($_POST['CreateDocumentButton']))
		{
			$PageTitle = "New Document";
			$this->ProcessNewDocument($html);
		}
		else BugOrHack();
	}
	
	protected function FormatEditDocument($html)
	{
		$safeTitle = htmlspecialchars($_GET['Edit'], ENT_QUOTES);
		$html->Heading("Edit Document: ".$safeTitle, 3);
		$result = DbQuery(sprintf("SELECT * FROM DILF_Documents WHERE Title='%s'", 
			DbEscapeString($_GET['Edit'])));
		if ($line=DbFetchArray($result))
		{
			$form = $this->BuildDocumentEditForm();
			$form->AddHidden("Title", $safeTitle);
			$form->FormatHeader($html, "Documents=Edited");
			$form->FormatBody($html, $line);
			$form->FormatFooter($html);					
		}
		else
			$html->Paragraph("Couldn't find document.");		
	}
	
	protected function ProcessEditedDocument($html)
	{
		$form = $this->BuildDocumentEditForm();
    	$updates = $form->GetUpdateRecord($_POST);
		$updates = array_map(DbEscapeString, $updates);
		DbUpdate("DILF_Documents", $updates, "Title='".DbEscapeString($_POST['Title'])."'", 1);		
		
		AuditLog(0, 'Edit Document '.$_POST['Title']);
		SetReload('Documents=Show');
		$html->Paragraph('Document Updated.');
		$html->LinkSelf('Documents=Show', 'Back to Documents');
	}
	
	protected function BuildDocumentEditForm()
	{
		$form = new LimFormBuilder('EditDocument', true);
		$form->AddField(new LimFieldTextArea('Text', 'Text', 22, 80));
		return $form;
	}
	
	protected function ProcessNewDocument($html) 
	{
		if (($_POST['NewDocName']!="") and preg_match('#^[-\'A-Z0-9 ]+$#i', $_POST['NewDocName'])) {
			DbInsert("DILF_Documents", array("Title"=>DbEscapeString($_POST['NewDocName'])));
			SetReload('Documents=Show');
			$html->Paragraph('Document Created.');
		}
		else 
		{
			$html->Paragraph('"'.FormatWarning(htmlspecialchars($_POST['NewDocName'], ENT_QUOTES)).'" is not a valid document name.');
			$html->Paragraph('The document name you enter must comprise only alphanumeric, hyphen, quote and space characters.');
		}
		$html->LinkSelf('Documents=Show', 'Back to Documents');
	}
	
	protected function FormatDocumentTable($html)
	{
		$html->Heading('Edit Documents', 3);
		
		$html->BeginTable("Documents", "widetable");
		$html->BeginTableRow();
		$html->BeginTableCell('', 'darkpanel');
		
		$result = DbQuery(sprintf("SELECT * FROM DILF_Documents WHERE 1 ORDER BY Title"));
		$html->BeginTable("DocumentList", "");
		while ($line=DbFetchArray($result)) {
			$html->BeginTableRow();
			$html->TableCell($line['Title'], "", "lightpanel");
			$html->BeginTableCell("", "lightpanel");
			$html->LinkSelf(array("Documents=Edit", 'Edit='.$line['Title']), 'Edit');
			$html->EndTableCell();
			$html->BeginTableCell("", "lightpanel");
			$html->LinkSelf('View='.$line['Title'], 'View');
			$html->EndTableCell();
			$html->EndTableRow();
		}
		DbEndQuery($result);
		$html->EndTable();
		
		$html->EndTableCell();
		$html->EndTableRow();
		$html->BeginTableRow();
		$html->BeginTableCell('', 'darkpanel');
		
		$html->BeginForm("Documents=New", "NewDocument");
		$html->Text('New document name (alphanumeric, hyphen, quote and space characters)');
		$html->Text(' <input type=text name="NewDocName" value=""> ');
		$html->SubmitButton("CreateDocumentButton", "Create Document");
		$html->EndForm();
		
		$html->EndTableCell();
		$html->EndTableRow();
		$html->EndTable();
	}
}
?>