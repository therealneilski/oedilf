<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimMail {
    protected $sender, $subject, $message;

    public function __construct($sender, $subject, $message)
    {
        $this->sender = $sender;
        $this->subject = $subject;
        $this->message = preg_replace("#(?<!\r)\n#si", "\r\n", $message);
    }

    public function SendTo($recipient)
    {
        global $ShowTiming;
        $StartTime = LimGeneral::GetMicroTime();
        mail($recipient, $this->subject, $this->message,
            "From: ".$this->sender. "\r\nReply-To: ".$this->sender.
            "\r\nX-Mailer: OEDILF Admin". "\r\nReturn-Path: ".$this->sender,
            "-f".$this->sender // apache must be in /etc/mail/trusted-users to prevent header warning
        );
        if ($ShowTiming)
        {
            printf('Email took %.0f ms.', LimGeneral::MsElapsed($StartTime));
        }
    }
}
?>
