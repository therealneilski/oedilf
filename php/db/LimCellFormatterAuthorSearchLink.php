<?php
class LimCellFormatterAuthorSearchLink extends LimCellFormatterText
{
	public function FormatCell($cellValue)
	{
		return LinkSelf(array("searchstart=Search", "searchauthor=".$cellValue), GetAuthorHtml($cellValue));
	}
}
?>