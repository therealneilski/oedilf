
function showHint(field, hintId)
{
	var str = field.value;
	if (str.length == 0) {
		document.getElementById(hintId).innerHTML = "";
		return;
	}
	var xmlhttp = GetXmlHttpObject();
	if (xmlhttp == null) return;

	var url = "LimAjax.php";
	url = url + "?AuthorName=" + str + "&Target=" + field.id;
	url = url + "&rand=" + Math.random();
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4)
			document.getElementById(this.hintId).innerHTML = this.responseText;
	}
	xmlhttp.hintId = hintId;
	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);
}

function setFieldEntry(id, str)
{
	document.getElementById(id).value = str;
}

function macroButt(textAreaId, buttonName, authorId)
{
	var xmlhttp = GetXmlHttpObject();
	if (xmlhttp == null) return;
	xmlhttp.targetArea = textAreaId;
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4)
		{
			document.getElementById(this.targetArea).value += this.responseText;
			document.getElementById(this.targetArea).focus();
		}		
	}
	xmlhttp.open("GET", "LimAjax.php?Button="+buttonName+"&ButtonAuthor="+authorId, true);
	xmlhttp.send(null);	
}

function GetXmlHttpObject()
{
	var x=null;
	if (window.XMLHttpRequest)
		x = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
	else if (window.ActiveXObject)		
		x = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
	if (x==null)
		alert("Your browser does not support XMLHTTP!");
	return x;
}