<?php
class LimFieldMultiSelect extends LimField
{
	protected $options;
	
	public function __construct($fieldName, $screenLabel, $options) 
	{
		parent::__construct($fieldName, $screenLabel);
		$this->options = $options;
	}

	public function GetFormHtml($defaultValue)
	{
		$optionString = '';
		foreach ($this->options as $value => $string)
		{
			$optionString .= sprintf("<input type='checkbox' name='%s'%s>%s<br>", 
				$value, (strpos($defaultValue, '+'.$value)!==false) ? ' checked' : '', 
				htmlspecialchars($string, ENT_QUOTES));
		}
		return $optionString;
	}
	
	public function GetSqlUpdateValue($postValues)
	{
		$list = "";
		foreach ($this->options as $value => $string)
		{
			if (isset($postValues[$value]))
				$list .= '+'.$value;
		}
		return $list;
	}

	public function InvalidEntryMessage($postValues)
	{
		return "";
	}
	
	public function HasValue($postValues)
	{
		return true;
	}
}
?>