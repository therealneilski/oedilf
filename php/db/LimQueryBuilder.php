<?php

class LimQueryBuilder
{
    protected $command = '';
    protected $tables = array();
    protected $columns = array();
    protected $wheres = array();
    protected $groups = array();
    protected $orders = array();
    protected $limit = '';

    public function __construct($command)
    {
        $this->command = $command;
        return $this;
    }

    public static function Select()
    {
        return new LimQueryBuilder("SELECT");
    }

    public static function Delete()
    {
        return new LimQueryBuilder("DELETE");
    }

    public function Column($field, $as='')
    {
        $this->columns[] = $field . ($as ? " AS $as" : '');
        return $this;
    }

    public function From($table, $as='')
    {
        $this->tables[] = $table . ($as ? " AS $as" : '');
        return $this;
    }

    public function Where($condition)
    {
        $this->wheres[] = "($condition)";
        return $this;
    }

    public function Group($group)
    {
        $this->groups[] = $group;
        return $this;
    }

    public function Order($order, $dir='ASC')
    {
        $this->orders[] = $order." ".$dir;
        return $this;
    }

    public function Limit($count, $offset=0)
    {
        $start = ($offset>0 ? "$offset," : '');
        $this->limit = $start.$count;
        return $this;
    }

    public function ToSQL()
    {
        if ($this->command=="SELECT")
        {
            $sql = $this->command." ";

            if (count($this->columns))
                $sql .= implode(", ", $this->columns);
            else $sql .= "*";
        }
        else $sql = $this->command;

        $sql .= " FROM ";
        if (count($this->tables))
            $sql .= implode(", ", $this->tables);

        $sql .= " WHERE ";
        if (count($this->wheres))
            $sql .= implode(" AND ", $this->wheres);
        else $sql .= "1=1";

        if (count($this->groups))
        {
            $sql .= " GROUP BY ";
            $sql .= implode(", ", $this->groups);
        }

        if (count($this->orders))
        {
            $sql .= " ORDER BY ";
            $sql .= implode(", ", $this->orders);
        }

        if ($this->limit)
            $sql .= " LIMIT ".$this->limit;
            
        return $sql;
    }   
}

?>