<?php
// WEAP program manager

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimWeap {

	protected $weapers = array();
	protected $limList = array();
	protected $type = '';
	
	public function __construct($searchList, $type="WEAP") {
		$this->weapers = array();
		$this->type = $type;
		$result = DbQuery("SELECT W.AuthorId, A.Name, COUNT(*) Count 
			FROM DILF_WEAP W, DILF_Authors A 
			WHERE W.AuthorId=A.AuthorId AND W.Type='$type' 
			GROUP BY W.AuthorId ORDER BY A.Name");
		while ($weap = DbFetchArray($result)) {
			$this->weapers[$weap['AuthorId']] = array("name"=>$weap['Name'], "count"=>$weap['Count']);
		}
		$this->limList = explode(",", $searchList);
		DbEndQuery($result);
	} 

    public static function GetRandomLimerick($RFAs, $type="WEAP") {
        global $member;
        $memberid = $member->GetMemberId();
		$WordsDisplay = '';
		
		$excludeArray = array();
		// get exclusion list of all limericks you have grabbed in the last week
		$result = DbQuery(sprintf("SELECT VerseId AS OriginalId FROM DILF_Log 
			WHERE (DateTime>'%s' AND AuthorId=$memberid) AND 
				Action='Limerick Grabbed For $type'
			UNION 
			SELECT R.OriginalId AS OriginalId FROM DILF_RFAs R, DILF_WEAP W
			WHERE R.OriginalId=W.OriginalId AND
				R.AuthorId=$memberid", 
			LimTimeConverter::FormatGMDateFromNow(-7*24*3600)));
		while ($exclude = DbFetchArray($result)) 
			$excludeArray[] = $exclude['OriginalId'];
		$excludeSet = implode(",", $excludeArray);
		if ($excludeSet) $excluded = "AND (NOT (L.OriginalId IN ($excludeSet)))";
		else $excluded = "";
		
		if ($RFAs==-1) $RFACondition = "L.RFAs<".RFAS_NEEDED;
		else if ($RFAs==3) $RFACondition = "L.RFAs=3";
		else if ($RFAs==2) $RFACondition = "L.RFAs=2";
		else if ($RFAs==1) $RFACondition = "L.RFAs=1";
		else $RFACondition = "L.RFAs=0";
		
		$result = DbQuery("SELECT W.OriginalId 
			FROM DILF_WEAP W, DILF_Limericks L 
			WHERE W.OriginalId=L.OriginalId AND
				W.Type='$type' AND
				$RFACondition AND L.State IN ('tentative', 'revised') AND 
				L.PrimaryAuthorId<>$memberid AND
				L.SecondaryAuthorId<>$memberid 
				$excluded
			ORDER BY RAND() LIMIT 1");
		if ($lim = DbFetchArray($result)) {
			$limnum = $lim['OriginalId'];
			AuditLog($limnum, 'Limerick Grabbed For '.$type);
			SetRedirect("Workshop=".$limnum);
		}
		else {
			$WordsDisplay .= "<p>No $type limericks available at the moment.</p>";
		}
		DbEndQuery($result);
		
		return $WordsDisplay;
	}

	public function FormatConfigurationPage($html) {
		$html->Text(FormatJavaScriptSellAllFunction());
		$html->BeginTable();
		$html->BeginTableRow();
		$html->BeginTableCell();
		$html->Heading($this->type." Configuration", 3);
		$html->BeginForm("Utility=".$this->type."Remove", $this->type."Remove");
		$html->Text("Current ".$this->type." targets:");
		$html->LineBreak();
		$html->LineBreak();
		if (count($this->weapers)>0){
			foreach ($this->weapers as $id => $weaper) {
			 	$html->Checkbox($this->type."$id", false, $this->type."$id",
					sprintf("%s (%s)", htmlspecialchars($weaper['name'], ENT_QUOTES), $weaper['count']));
				$html->LineBreak();
			}			
			$html->LineBreak();
			$html->Link('javascript:selall(\''.$this->type.'Remove\', 1);', "Select All");
			$html->Text(" ");
			$html->Link('javascript:selall(\''.$this->type.'Remove\', 0);', "Deselect All");
			$html->LineBreak();
			$html->SubmitButton($this->type."RemoveSelected", 
				"Remove selected authors from ".$this->type." list");
		}
		else {
			$html->Text(" none found");
		}
		$html->EndForm();
		$html->LineBreak();
		
		$html->BeginForm("Utility=".$this->type."Add", $this->type."Add");
		if ($this->limList[0]=="")
			$candidates = "none available";
		else
			$candidates = count($this->limList);
		
		$html->SubmitButton($this->type."AddSelected", 
			"Add all current search results ($candidates) to ".$this->type." list");
		$html->LineBreak();
		$html->LineBreak();
		$html->BeginSelection($this->type."Author");
		$html->Text(AuthorSelectionList(-1, FALSE, TRUE));
		$html->EndSelection();
		$html->LineBreak();
		$html->SubmitButton($this->type."AddAuthor", 
			"Add author's tentative and revised to ".$this->type." list");
		$html->EndForm();
		$html->LineBreak();
		$html->EndTableCell();
		if ($this->type=="WEAP") {
			$html->BeginTableCell();
			$html->Text(FormatDocument("WEAP", TRUE, TRUE));
			$html->EndTableCell();
		}
		$html->EndTableRow();
		$html->EndTable();
		
		return $html->FormattedHtml();
	}
	
	public function ProcessWeapRemove() {
	 	$removeList = "";
	 	$delim = "";
		foreach ($_POST as $cbox => $value) {
			if ((substr($cbox, 0, 4)==$this->type) and ($value=='on')) {
				$id = substr($cbox, 4);
				$removeList .= $delim.$id;
				$delim = ",";
			}
		}
		if ($removeList!="") {
			DbQuery("DELETE FROM DILF_WEAP WHERE Type='".$this->type."' AND AuthorId IN ($removeList)");
		}
		SetRedirect('Utility='.$this->type);
	}

	public function ProcessWeapAdd() {
		if ($_POST[$this->type."AddSelected"]) 
		{
			if ($this->limList[0]!="") {
				$lims = implode(",", $this->limList);
				DbQuery("INSERT IGNORE INTO DILF_WEAP 
					SELECT PrimaryAuthorId AS AuthorId, OriginalId, '".$this->type."' AS Type
					FROM DILF_Limericks WHERE OriginalId IN ($lims)");
			}
		}
		else if ($_POST[$this->type."AddAuthor"])
		{
			$authorId = $_POST[$this->type.'Author'];
			DbQuery("INSERT IGNORE INTO DILF_WEAP 
				SELECT PrimaryAuthorId AS AuthorId, OriginalId, '".$this->type."' AS Type 
				FROM DILF_Limericks 
				WHERE PrimaryAuthorId=$authorId AND (State='tentative' OR State='revised')");			
		}
		SetRedirect('Utility='.$this->type);
	}
}


?>
