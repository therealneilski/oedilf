<?php
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

define('COOKIE_DURATION', ONE_YEAR);
define('COOKIE_PATH', "/");
define('COOKIE_NAME_REMEMBER', "LimDbRememberLogin");

class LimSession
{

    public static function Setup()
    {
        session_name("LimDb");
        session_set_cookie_params(COOKIE_DURATION, COOKIE_PATH);
        session_start();

        if (isset($_GET['Debug'])) $_SESSION['Debug'] = $_GET['Debug'];

        $debug = (isset($_SESSION['Debug']) ? $_SESSION['Debug'] : 0);
        LimGeneral::SetDebugMask($debug);
        LimGeneral::Log("-------------------------------------------------------");
    }

    public static function SetCookie($name, $value, $expire = -1)
    {
		if ( $expire == -1 ) {
			$expire = time() + COOKIE_DURATION;
		}
        LimGeneral::Log("Cookie:$name Value:$value Expiry:$expire Path:".COOKIE_PATH, LimGeneral::DEBUGLOG_COOKIES);
        setcookie($name, $value, $expire, COOKIE_PATH);
    }

    public static function ClearCookie($name)
    {
        self::SetCookie($name, '', time()-ONE_DAY);
    }

    public static function LoggedIn()
    {
        return isset($_SESSION['loggedin']) && ($_SESSION['loggedin']===true);
    }

    public static function MemberId()
    {
        return isset($_SESSION['memberid']) ? $_SESSION['memberid'] : 0;
    }

    public static function GetHashedPassword()
    {
        return isset($_SESSION['memberpassword']) ? $_SESSION['memberpassword'] : '';
    }

    public static function SetHashedPassword($hashedPassword)
    {
        $_SESSION['memberpassword'] = $hashedPassword;        
    }

    public static function GetCookieRememberToken()
    {
        return isset($_COOKIE[COOKIE_NAME_REMEMBER]) ? $_COOKIE[COOKIE_NAME_REMEMBER] : 0;
    }

    public static function SetLoggedIn($id, $hashedPassword, $rememberLoginToken)
    {
        $_SESSION['loggedin'] = true;
        $_SESSION['memberid'] = $id;
        $_SESSION['memberpassword'] = $hashedPassword;

        self::RefreshLoginCookies($id, $rememberLoginToken);
    }

    public static function SetLoggedOut($forgetToken = false)
    {
        $_SESSION['loggedin'] = false;
        $_SESSION['memberid'] = 0;
        $_SESSION['memberpassword'] = '';

        if ($forgetToken)
            self::ClearCookie(COOKIE_NAME_REMEMBER);
    }

    public static function RefreshLoginCookies($id, $rememberLoginToken)
    {
        $_COOKIE[COOKIE_NAME_REMEMBER] = $rememberLoginToken;
        if ($rememberLoginToken)
            self::SetCookie(COOKIE_NAME_REMEMBER, $rememberLoginToken);
        else
            self::ClearCookie(COOKIE_NAME_REMEMBER);
    }

    public static function MakeLoginCookieToken($id, $token)
    {
        return sprintf('%08x%s', $id, $token);
    }

    public static function DecodeLoginCookieToken()
    {
        if (isset($_COOKIE[COOKIE_NAME_REMEMBER]))
            return array(hexdec(substr($_COOKIE[COOKIE_NAME_REMEMBER], 0, 8)),
                substr($_COOKIE[COOKIE_NAME_REMEMBER], 8));
        else return array(0, '');
    }
}
?>
