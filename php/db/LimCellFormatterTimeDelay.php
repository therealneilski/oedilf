<?php
class LimCellFormatterTimeDelay extends LimCellFormatterDateTime
{
	public function __construct($timeZone, $secondsDelay)
	{
		parent::__construct($timeZone + $secondsDelay/3600);
	}
}
?>