<?php
// Limerick Data Formatting Functions
if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

$HelpSymbol = '<img src="YellowQuery.gif">';
$TagHelp = LinkPopup('View=HTML Tags', 'Help: Using tags to format your text', '', $HelpSymbol);
$EditHelpMsg = '<span class="hinttext">'.$TagHelp.'</span>';
$LimerickEditHelpMsg = '<span class="hinttext" style="margin-left:30px;">'.$TagHelp.
	' <a target="_blank" href="http://www.oedilf.com/forum/viewtopic.php?t=907">'.$HelpSymbol.'Help: Formatting Guidelines</a></span>';
$NewLimHelpMsg = '<span class="hinttext" style="margin-left:30px;">'.
	LinkPopup('View=First Limerick', 'Help: How to enter your first limerick', '', $HelpSymbol).'</span>';
$WordHintMsg = '<span class="hinttext">(One word per line. All lowercase except for proper nouns or acronyms. Just add the word, not the definition.)</span>';
$WSRevisionPromptMsg = '(replace this text with a description of your changes)';

function DelimitParamList($params, $delim="&amp;") {
  if (is_array($params)) {
	return implode($delim, $params);
  }
  else {
    return $params;
  }
}

function SubmitButton($name, $value, $title='')
{
	if ($title) $title = ' title="'.$title.'"';
	return '<input type="submit" class="btn" name="'.$name.'" value="'.$value.'"'.$title.' onClick="this.setAttribute(\'disabledStyle\', true)">';
}

function LinkSelf($params, $text, $title='', $symbol='') {
    return sprintf('<a title="%s" rel="nofollow" href="%s?%s">%s%s</a>',
        $title, PHPSELF, DelimitParamList($params), $symbol, str_replace(" ","&nbsp;",$text));
}
function NavLinkSelf($params, $text, $title='') {
  return sprintf('<a class="nav" title="%s" rel="nofollow" href="%s?%s">%s</a>', $title, PHPSELF, DelimitParamList($params), str_replace(" ","&nbsp;",$text));
}
function LightLinkSelf($params, $text, $title='') {
  return sprintf('<a class="macro" title="%s" rel="nofollow" href="%s?%s">%s</a>', $title, PHPSELF, DelimitParamList($params), str_replace(" ","&nbsp;",$text));
}
function LinkPopup($params, $text, $title='', $symbol='') {
  return sprintf('<a title="%s" rel="nofollow" href="%s?%s&amp;Popup=1" target="_blank">%s%s</a>', 
  	$title, PHPSELF, DelimitParamList($params), $symbol, str_replace(" ","&nbsp;",$text));
}
function DarkButton($params, $text, $title='') {
  return LightLinkSelf($params, $text, $title);
}

function LinkNewBrowser($site, $text, $title='') {
  return sprintf('<a title="%s" rel="nofollow" href="%s" target="_blank">%s</a>', $title, $site, $text);
}

function OneLookLink($word, $showImage, $showText) {
	if ($showImage) $image = "<img name='OneLook Search' src='OneLook.gif'>";
	else $image = "";
	$site = "http://onelook.com/?w=".rawurlencode($word);
	$hintWord = htmlspecialchars($word, ENT_QUOTES);
	if ($showText) $text = str_replace(" ","&nbsp;", $hintWord);
	else $text = "";
	return "<a title='Show OneLook definitions of $hintWord' rel='nofollow' href='$site' target='_blank'>". 
		$image.$text."</a>";
}

function RedXLink($params, $text='', $title='Delete') {
	return " ".sprintf('<a title="%s" rel="nofollow" href="%s?%s"><img src="RedX.gif">%s</a>', $title, PHPSELF, DelimitParamList($params), $text)." ";
}
function PurplePenLink($params, $text='', $title='Edit') {
	return " ".sprintf('<a title="%s" rel="nofollow" href="%s?%s"><img src="PurplePen.gif">%s</a>', $title, PHPSELF, DelimitParamList($params), $text)." ";
}

function strtolowerascii($txt){
	$txt=strtolower(strtr($txt,
		"������������������������������������������������������",
		"aaaaaaceeeeiiiinoooooouuuuyaaaaaaceeeeiiiinoooooouuuuy"));
	$txt = str_replace(array('�','�','�','�','�','�'), 
		array('ae','ae','th','th','th','th'), $txt);
	return($txt);
}

function SetReload($params, $timeout=3) {
  global $ReloadLine;
  $ReloadLine = sprintf('<meta http-equiv="refresh" content="%d;url=%s?%s">', 
  	$timeout, PHPSELF, DelimitParamList($params, "&")) ;
}

function SetRedirect($params) {
  global $RedirectLine;
  $RedirectLine = PHPSELF."?".DelimitParamList($params, "&");
}

$AlreadyStripped = false;
function StripMagicQuotes()
{
	global $AlreadyStripped;
	if (!$AlreadyStripped)
	{
		if (get_magic_quotes_gpc())
		{
			function stripslashes_deep(&$value)
			{
				$value = is_array($value) ?
					array_map("stripslashes_deep", $value) :
					stripslashes($value);
		
				return $value;
			}
			stripslashes_deep($_GET);  
			stripslashes_deep($_POST);  
		}
		$AlreadyStripped = true;
	}
}

function FormatWarning($text)
{
	return '<span class="warninghighlight">'.$text.'</span>';
}

function FormatFeedbackStyle($Style) {
	$index = LimFieldFeedbackStyle::PreferenceIndex($Style);
	if ($index<0) $index = 1;
	
	return sprintf('<div class="preferencehighlight%s">%s</div>', $index, 
    		htmlspecialchars(LimFieldFeedbackStyle::ExpandToText($Style), ENT_QUOTES));
}

function QueryWordList($VerseId) 
{
	$wordList = new LimWordList($VerseId);
	return $wordList->WordList();
}

function GetWordList($VerseId, $ShowLinks) 
{
	$html = new LimHtml();
	$wordList = new LimWordList($VerseId);
	if ($ShowLinks)
		$wordList->FormatWordList($html, ", ", "WordLinkFormatter");
	else $wordList->FormatWordList($html, ", ");
	return $html->FormattedHtml();
}

function FormatAuthor($AuthorId, $ShowLinks, $IncludeTwitter = FALSE) {
  $Author = GetAuthorHtml($AuthorId, $IncludeTwitter);

  if ($ShowLinks)
    return sprintf('<a rel="nofollow" href="http://%s?AuthorId=%d">%s</a>', THIS_APP, $AuthorId, $Author);
  else return '<b>'.$Author.'</b>';
}

function FormatRealAuthor($AuthorId, $ShowLinks) {
  $AuthorName = FormatAuthor($AuthorId, $ShowLinks);
  $result = DbQuery("SELECT * FROM DILF_Authors WHERE AuthorId=".$AuthorId);
  if ($line = DbFetchArray($result) and $line['RealName']) {
    $AuthorName .= " (".htmlspecialchars($line['RealName'], ENT_QUOTES).")";
  }
  DbEndQuery($result);

  return $AuthorName;
}

function GetLetterList($EndAlphabet) {
  $Letters = array();
  $word='Aa';
  $i=0;
  while ((strtolower($word)<$EndAlphabet) and ($i<1000)) {
    $Letters[] = $word;
    if ($word{1}=='z') {
      $FirstLetter = chr(ord($word{0})+1);
      $SecondLetter = 'a';
    }
    else {
      $FirstLetter = $word{0};
      $SecondLetter = chr(ord($word{1})+1);
    }
    $word = $FirstLetter.$SecondLetter;
    $i++;
  }
  return $Letters;
}

function LastValidLetters($EndAlphabet) {
    $Letters = $EndAlphabet;
    if (strlen($Letters)==2) { // take the preceding letter pair
      if ($Letters{1}=='a')
        $Letters = strtoupper(chr(ord($Letters{0})-1)).'z';
      else
        $Letters = strtoupper($Letters{0}).chr(ord($Letters{1})-1);
    }
    else $Letters = strtoupper($Letters{0}).$Letters{1};
    return $Letters;
}

function FormatLetterList($EndAlphabet, $command, $params) {
  $WordsDisplay = '';
  if ($params)
    $params = "&amp;".DelimitParamList($params);
	
  $Letters = GetLetterList($EndAlphabet);
  foreach ($Letters as $word) {
    if ($word{1} == 'a')
      $WordsDisplay .= LinkSelf("$command=[".$word{0}." -$word]$params", $word)." ";
    else if ($word{1} == 'z')
      $WordsDisplay .= LinkSelf("$command=[$word<".chr(ord($word{0})+1)." ]$params", $word)." ";
    else $WordsDisplay .= sprintf('<a href="%s?%s=%s*%s">%s</a> ', 
		PHPSELF, $command, $word, $params, $word);
  }
  return $WordsDisplay;
}

function SplitAN($AuthorNote) {
  $AN = explode("@@ENDPLAN@@", $AuthorNote, 2);
  if (count($AN)==1) {
    $AN[1] = $AN[0];
    $AN[0] = '';
  } else {
    // if the Pre-Lim Author Note ends with a line break, remove it.
    if (substr($AN[0], -2, 2) == "\r\n")
      $AN[0] = substr($AN[0], 0, -2);
    else if (substr($AN[0], -1, 1) == "\n")
      $AN[0] = substr($AN[0], 0, -1);
      
    // if the Author Note starts with a line break, remove it.
    if (substr($AN[1], 0, 2) == "\r\n")
      $AN[1] = substr($AN[1], 2);
    else if (substr($AN[1], 0, 1) == "\n")
      $AN[1] = substr($AN[1], 1);
  }
  return $AN;
}

function FormatLimerickQuote($OriginalId)
{
  global $CurtainFilter;
  $WordsDisplay = '';

  $result = DbQuery(sprintf('SELECT * FROM DILF_Limericks WHERE OriginalId="%d" '.$CurtainFilter.' AND (State="approved" OR State="confirming" OR State="tentative")', $OriginalId));
  $line = DbFetchArray($result) ;
  DbEndQuery($result);

  if ($line) {
    $WordsDisplay .= "<h3>Quote A Limerick</h3>";
    if ($line['State']<>"approved") $WordsDisplay .= FormatDocument('Quoting Unapproved Limericks', TRUE, TRUE);
    $WordsDisplay .= FormatDocument('Quoting Limericks', TRUE, TRUE);

    $a = FormatRealAuthor($line['PrimaryAuthorId'], TRUE);
    if ($line['SecondaryAuthorId']<>0) $a .= sprintf(' and %s', FormatRealAuthor($line['SecondaryAuthorId'], TRUE));
  
    $lim = sprintf("<p><b>%s</b> by %s</p>\n", GetWordList($line['VerseId'], TRUE), $a);
    $AN = SplitAN($line['AuthorNotes']);
    if ($AN[0])
      $lim .= "<p>".FormatQuickDocLinks(AddBreaks($AN[0]))."</p>\n";
      
    $lim .= "<p>".FormatQuickDocLinks(AddBreaks($line['Verse']))."</p>";

    if ($line['AuthorNotes']) {
      $lim .= "<p>".FormatQuickDocLinks(AddBreaks($AN[1]))."</p>\n";
      if ($line['EditorNotes'])
        $lim .= "<p>".FormatEditorNotes(FormatQuickDocLinks(AddBreaks($line['EditorNotes'])))."</p>\n";
    }
    $WordsDisplay .= 'Formatted Text:<br ><table class="widetable"><tr><td class="darkpanel">'.$lim.'</td></tr></table>';
  
    // create a text box version
    $WordsDisplay .= sprintf('<p>Raw HTML:<br ><TEXTAREA NAME="limericktext" COLS=80 ROWS=10>%s</TEXTAREA></p>', $lim);
  }
  else $WordsDisplay .= "<p>Limerick Unavailable</p>";
  return $WordsDisplay;
}

function FormatEditorNotes($Notes) {
  return "_________<br ><br ><b>Editor's Note:</b><br ><br >".$Notes ;
}

function FormatLinkMeLink($OriginalId)
{
  return LinkSelf('VerseId='.$OriginalId, 'Link', "Permanent link to this limerick", "<img src='Link.gif'>");
}

function FormatQuoteMeLink($OriginalId)
{
  return LinkPopup('Quote='.$OriginalId, 'Quote Me!', "How to quote this limerick", "<img src='Quote.gif'>");
}

function FormatStyledQuoteMeLine($OriginalId)
{
	return "<div class='quoteandshare'>".
        "<div class='quoteme'>".FormatQuoteMeLink($OriginalId)." ".FormatLinkMeLink($OriginalId).
        "</div>".
        FormatStyledSocialButtons($line).
        "</div>".
        '';
}

function FormatStyledSocialButtons($line)
{
	$html = new LimHtml();
	$html->BeginDiv('socialbuttons');
            $LimBigTitle = 'Limerick #'.
                $line['LimerickNumber'].
                " on ".
                GetWordList($line['VerseId'], false).
                " by ".
                rawurlencode(strip_tags(FormatAuthor($line['PrimaryAuthorId'], false, true))).
		$html->LineBreak();
		$html->LineBreak();
		$limUrl = 'https://oedilf.com/db/Lim.php?VerseId=A' . $line['LimerickNumber'];
    # 2017-12-31: Link buttons for fb and twitter temporarily  disabled as it was saying "Limerick on (undefined) and linking to lim 0
    #$html->Text('<a class="twitter-share-button" href="https://twitter.com/intent/tweet?text='.$LimBigTitle.'&url='.rawurlencode($limUrl).'&via=OEDILF">Tweet</a>');
		#$html->LineBreak();
    	#$html->Text('<div class="fb-share-button" href="https://www.oedilf.com/db/Lim.php?VerseId=A' . $line['LimerickNumber'] . ' layout="button" data-layout="button" >Share on Facebook</div>');
		#$html->LineBreak();
	$html->EndDiv();
	return $html->formattedHtml();
}

function FormatLimerickContent($line, $ShowNotes, $ShowQuoteLink=TRUE) {
  global $CurtainFilter;
  $WordsDisplay = '';
  if ($ShowQuoteLink and ($line['State']=="approved" or $line['State']=="confirming" or $line['State']=="tentative")) {
    $WordsDisplay .= FormatStyledQuoteMeLine($line['OriginalId']);
  }
  if ($ShowNotes) {
    $AN = SplitAN($line['AuthorNotes']);
    if ($AN[0])
      $WordsDisplay .= '<div class="limericknotes2" style="padding-bottom:10px;">'.FormatQuickDocLinks(AddBreaks($AN[0]))."</div>\n";
  }
  $WordsDisplay .= '<div class="limerickverse">';
  $WordsDisplay .= FormatQuickDocLinks(AddBreaks($line['Verse']));
  $WordsDisplay .= '</div>';
  if ($ShowNotes) {
    $WordsDisplay .= '<div class="limericknotes2">'.FormatQuickDocLinks(AddBreaks($AN[1])).'</div>';
    if ($line['EditorNotes']<>'')
      $WordsDisplay .= '<div class="limericknotes2">'.FormatEditorNotes(FormatQuickDocLinks(AddBreaks($line['EditorNotes']))).'</div>';
  }
  
  if ($CurtainFilter and ($line['Category']<>"normal") ) {
    $WordsDisplay = "<p>This limerick is listed as unsuitable for a general audience and will not be displayed.</p>";
  }
  return $WordsDisplay;
}

function FormatLimerickDetails($line, $ShowLinks, $WordDefLinks, $words) {
	global $member;
	$html = new LimHtml();
  
	$html->BeginDiv("limerickdetails");


	$html->Text(sprintf('By %s', FormatAuthor($line['PrimaryAuthorId'], $ShowLinks)));
	if ($line['SecondaryAuthorId']<>0) 
		$html->Text(sprintf(' and %s', FormatAuthor($line['SecondaryAuthorId'], $ShowLinks)));
	$html->LineBreak();
	
	if ($line['DateTime'])
	    $html->TextLine(sprintf('%s: %s', ($line['Version']>0) ? "Revised" : "Submitted",
			FormatDate($line['DateTime'])));
	
	if (!$ShowLinks and ($line['Version']>0))
		$html->TextLine(sprintf('Revision: %s', $line['Version']));
	else $html->LineBreak();

	
	if ($line['State']!="obsolete")
	{
		$html->TextLine('Defines:');
		$scrollworthyCount = 5;
		if (count($words)>$scrollworthyCount)
			$html->BeginDiv("wordscroller");
		else $html->BeginDiv("wordindent");
		$wordList = new LimWordList(0);
		$wordList->AddWordList($words);
		if ($WordDefLinks) $formatter = "WordDefLinkFormatter";
		else if ($ShowLinks) $formatter = "WordLinkFormatter";
		else $formatter = "WordBoldFormatter";
		$wordList->FormatWordList($html, "<br >", $formatter);
		$html->EndDiv();
		$html->LineBreak();
	}
	
	$html->Text('Status: ');
	if (($line['State']<>'held') and ($line['State']<>'revised') and ($line['State']<>'new'))
		$html->Text($line['State']);
	else $html->Text('<b>'.$line['State'].'</b>');
	if (LimSession::LoggedIn() and ($line['RFAs']>=0))
		$html->Text("; ".$line['RFAs'].(($line['RFAs']==1) ? " RFA":" RFAs"));
	$html->LineBreak();
	
	if ($line['State']!="obsolete")
	{
		if ($line['Category']=='curtained room')
			$html->TextLine("Filter: curtained room");
		
		$topics = LimerickTopicLinks($line['OriginalId']);
		if ($topics) $html->TextLine('Topics: '.$topics);
	}
  
	if ($ShowLinks and LimSession::LoggedIn())
	{
		$html->LineBreak();
		$html->LinkSelf("Workshop=".$line['VerseId'], "<b>Workshop</b>");
		$html->LineBreak();
    }
	
	if (LimSession::LoggedIn() and
		($line['PrimaryAuthorId']==$member->GetMemberId()) and
		($line['State']=='approved')) 
	{ // allow showcase selection
		if ($line['ShowcasePriority']>0)
			$html->LinkSelf(array("ShowcaseAction=Remove", "ShowcaseRemove=".$line['VerseId']), "Remove from Showcase"); 
		else if ($member->CanShowcase())
			$html->LinkSelf(array("ShowcaseAction=Add", "ShowcaseAdd=".$line['VerseId']), "Add to Showcase"); 
	}

        if ($line['State'] == 'approved') {
#            $LimBigTitle = 'Limerick #'.
#                $line['LimerickNumber'].
#                " on ".
#                GetWordList($line['VerseId'], false).
#                " by ".
#                rawurlencode(strip_tags(FormatAuthor($line['PrimaryAuthorId'], false, true))).
#		$html->LineBreak();
#		$html->LineBreak();
#		$limUrl = 'https://oedilf.com/db/Lim.php?VerseId=A' . $line['LimerickNumber'];
#    $html->Text('<a class="twitter-share-button" href="https://twitter.com/intent/tweet?text='.$LimBigTitle.'&url='.rawurlencode($limUrl).'&via=OEDILF">Tweet</a>');
#		$html->LineBreak();
#    	$html->Text('<div class="fb-share-button" href="https://www.oedilf.com/db/Lim.php?VerseId=A' . $line['LimerickNumber'] . ' layout="button" data-layout="button" >Share on Facebook</div>');
#		$html->LineBreak();
        }
	
	$html->EndDiv();
	return $html->FormattedHtml();
}

function TentativeWarning() {
  return '<b>The following limerick is in a "Tentative" state, which means the information contained within has not yet been verified. ' .
      LinkPopup('View=Tentative%20Limerick', '<i>more...</i>').'</b>';
}

function FormatLimerickBlock($line, $ShowLinks, $WordDefLinks=FALSE) {
  if ($line['State']!="obsolete")
  	$words = QueryWordList($line['VerseId']);
  else $words = array();
  $WordsDisplay = '';
  if (!LimSession::LoggedIn() and ($line['State']=='tentative'))
    $WordsDisplay .= '<p>'.TentativeWarning().'</p>';
  $WordsDisplay .= FormatLimBlock($line, $ShowLinks, $WordDefLinks, $words) ;
  return $WordsDisplay;
}

function FormatLimBlock($line, $ShowLinks, $WordDefLinks, $words) {
	$WordsDisplay = '';
	$WordsDisplay .= '<table class="widetable"><tr>';
	if ($line['State']=='approved') $Number=$line['LimerickNumber'];
	else $Number='T'.$line['OriginalId'];
	$WordsDisplay .= sprintf('<th>Limerick #%s ', $Number);
	$WordsDisplay .= '</th>';
	// use the first defined word as a title
	if (count($words)>0) $WordsDisplay .= sprintf('<th>%s</th>', $words[0]);
	$WordsDisplay .= '</tr><tr>';
	$WordsDisplay .= '<td class="darkpanel" style="width:25%;">'.
		FormatLimerickDetails($line, $ShowLinks, $WordDefLinks, $words).'</td>' ;
	$WordsDisplay .= '<td class="lightpanel" style="width:75%;">'.FormatLimerickContent($line, TRUE).'</td>';
	$WordsDisplay .= "</tr></table>\n";
	return $WordsDisplay;
}

function FormatOpenLimerickBlock($LimLine) {
	global $CurtainFilter;
	$Limerick = "";

	if ($LimLine['State']=='approved')
		$Number = LinkSelf("VerseId=A".$LimLine['LimerickNumber'], $LimLine['LimerickNumber']);
	else $Number = LinkSelf("VerseId=".$LimLine['OriginalId'], 'T'.$LimLine['OriginalId']);

	$a = FormatAuthor($LimLine['PrimaryAuthorId'], TRUE);
	if ($LimLine['SecondaryAuthorId']<>0) 
		$a .= sprintf(' and %s', FormatAuthor($LimLine['SecondaryAuthorId'], TRUE));

	$Limerick .= '<br><table width="100%">';
	if (!LimSession::LoggedIn() and ($LimLine['State']=='tentative'))
		$Limerick .= '<tr><td>'.TentativeWarning().'</td></tr>';
	$Limerick .= sprintf('<tr><td><b>%s</b> by %s (Limerick #%s)</td></tr>', 
	GetWordList($LimLine['VerseId'],TRUE), $a, $Number);

	$AN = SplitAN($LimLine['AuthorNotes']);
	if ($AN[0])
		$Limerick .= '<tr><td><div class="limericknotes" style="padding-bottom:10px;">'.
			FormatQuickDocLinks(AddBreaks($AN[0]))."</div></td></tr>";
	$Limerick .= '<tr><td>'.FormatStyledQuoteMeLine($LimLine['OriginalId']).
		'<div class="topiclimerickverse">'.FormatQuickDocLinks(AddBreaks($LimLine['Verse'])).
		'</div></td></tr>';
	if ($AN[1])
		$Limerick .= '<tr><td><div class="limericknotes">'.FormatQuickDocLinks(AddBreaks($AN[1])).'</div></td></tr>';
	if ($LimLine['EditorNotes']<>'')
		$Limerick .= '<tr><td><div class="limericknotes">'.
		FormatEditorNotes(FormatQuickDocLinks(AddBreaks($LimLine['EditorNotes']))).
		'</div></td></tr>';
	$Limerick .= '</table><br>';
	if ($CurtainFilter and ($LimLine['Category']<>"normal"))
		$Limerick = "<p>This limerick is listed as unsuitable for a general audience and will not be displayed.</p>";

	return $Limerick;
}

function FormatLimerickPage($limList, $formatter) {
	$WordsDisplay = '';
	
	$WordsDisplay .= $formatter->FormatPageTop();
	foreach ($limList as $line){
		$formatter->SetLimerick($line);
		$WordsDisplay .= $formatter->FormatLimerickPrelude();
		$WordsDisplay .= $formatter->FormatLimerickBlock();
		$WordsDisplay .= $formatter->FormatLimerickPostlude();
	}
	$WordsDisplay .= $formatter->FormatPageEnd();
	
	return $WordsDisplay;
}

function FormatLimerickList($heading, $query, $start, $pagesize, $linkparams, &$NumLimericks, $formatter=NULL) {
	$WordsDisplay = '';
	
	if ($formatter==NULL) {
		$formatter = new LimFormatter(LimSession::LoggedIn());
		$formatter->ShowLinks = TRUE;
	}

	if ($start<0) $start=0;
  
    $result = DbQuery($query);
    $NumLimericks = DbQueryRows($result);

    if ($pagesize<0) $pagesize=$NumLimericks;
    if ($NumLimericks>0) DbQuerySeek($result, $start);

	$pages = new LimPaginator($linkparams, $NumLimericks, $pagesize, $start);
	
	if ($heading)
		$WordsDisplay .= '<h3>'.$heading.' '.$pages->CurrRange().'</h3>';
	if ($NumLimericks>0)
		$WordsDisplay .= $pages->FormatNeatMultiplePageLinks();
	else
		$WordsDisplay .= '<p>None found.</p>';
	
	$count = $pagesize;
	if ($count>($NumLimericks-$start)) $count = $NumLimericks-$start;

	$limList = array();
	for ($i=0; $i<$count; $i++){
		$limList[] = DbFetchArray($result) ;
	}	
	$WordsDisplay .= FormatLimerickPage($limList, $formatter);
	
	DbEndQuery($result);
	
	$WordsDisplay .= $pages->FormatPaginationMenu();
	
	return $WordsDisplay;
}

function FormatLimericksFromList($heading, $list, $start, $pagesize, $linkparams, &$NumLimericks, $ShowHistory) {
  $WordsDisplay = '';

  $LimNums = explode(",",$list);

  $NumLimericks = count($LimNums);
  if (!$LimNums[0]) $NumLimericks = 0;
  
  if ($start<0) $start=0;
  if ($pagesize<0) $pagesize=$NumLimericks;
  $pages = new LimPaginator($linkparams, $NumLimericks, $pagesize, $start);

  $WordsDisplay .= '<h3>'.$heading.' '.$pages->CurrRange().'</h3>';
  if ($NumLimericks>0) {
    $WordsDisplay .= $pages->FormatNeatMultiplePageLinks();
  }
  else
    $WordsDisplay .= '<p>None found.</p>';

  for ($i=$start; ($i<$NumLimericks) and ($i<$start+$pagesize); $i++){

    $result = DbQuery(sprintf("SELECT * FROM DILF_Limericks WHERE State<>'obsolete' AND OriginalId=%d LIMIT 1", $LimNums[$i]));
    $line = DbFetchArray($result);
    DbEndQuery($result);
    if ($ShowHistory) 
    {
		$html = new LimHtml();
		$html->Heading("Limerick #T".$line['OriginalId'], 3, "expanded");
		$html->BeginDiv("boxedhistory");
		$html->LinkSelf("Workshop=".$line['OriginalId'], "Workshop");
		global $member;
    	$lim = new LimLimerick($line['OriginalId'], $member->GetMemberId());
		FormatWorkshopHistory($html, $lim, $member);
		$html->EndDiv();
		$WordsDisplay .= $html->FormattedHtml();
    }
    else $WordsDisplay .= FormatLimerickBlock($line, TRUE);
  }

  $WordsDisplay .= $pages->FormatPaginationMenu();

  return $WordsDisplay;
}

function CleanupHtml($text) {
  $text = htmlspecialchars($text, ENT_NOQUOTES);
  
  $text = preg_replace('@&amp;(#[0-9]+;)@', '&\1', $text); // allow special characters
  $text = str_replace(
    array('&amp;','--',     chr(145),chr(146),chr(147),chr(148),"&#8220;","&#8221;","&#8216;","&#8217;"),
  	array('&',    '&mdash;',"\\'",   "\\'",   '\\"',   '\\"',   '\\"',    '\\"',    "\\'",    "\\'"),
    $text); // allow ampersand, translate curly quotes and em-dash

  // create arrays for translatable tags
  $safe = array();
  $safereplace = array();
  $allowtags = array('i', 'b', 'u', 'em', 'strong', 's', 'ul', 'ol', 'li', 'sub', 'sup', 'tt', 'h1', 'h2', 'h3', 'h4', 'pre', 'small', 'big');
  foreach ($allowtags as $tag) {
    $safe[] = '['.$tag.']';
    $safereplace[] = '<'.$tag.'>';
    $safe[] = '['.strtoupper($tag).']';
    $safereplace[] = '<'.$tag.'>';
    $safe[] = '[/'.$tag.']';
    $safereplace[] = '</'.$tag.'>';
    $safe[] = '[/'.strtoupper($tag).']';
    $safereplace[] = '</'.$tag.'>';
    $safe[] = '&lt;'.$tag.'&gt;';
    $safereplace[] = '<'.$tag.'>';
    $safe[] = '&lt;'.strtoupper($tag).'&gt;';
    $safereplace[] = '<'.$tag.'>';
    $safe[] = '&lt;/'.$tag.'&gt;';
    $safereplace[] = '</'.$tag.'>';
    $safe[] = '&lt;/'.strtoupper($tag).'&gt;';
    $safereplace[] = '</'.$tag.'>';
  }
  $text = str_replace($safe, $safereplace, $text);

  $matcher = new LimTagMatcher($allowtags);
  return $matcher->Fix($text);
}

function AddBreaks($text) {
  $brString = str_replace("<br />", "<br>", nl2br($text));
  return preg_replace('/[ ]+(<br>)/', '$1', $brString); // strip trailing spaces
}

// callback function for preg_replace() to display limerick cross-references
// $matches filled from /\[\[#(T)?([-\'0-9]+)(:(.*))?\]\]/
// \0 => whole matched string
// \1 => 'T' or ''
// \2 => verse ID
// \3 => :<link text> or missing
// \4 => <link text> or missing
function FormatQuickDocLinksCallback($matches) {
    if (count($matches) >= 5) {
        list ($whole, $tentchar, $id, $hastext, $linktext) = $matches;
    }
    else {
        list ($whole, $tentchar, $id) = $matches;
	$hastext = '';
	$linktext = '#'.$tentchar.$id;
    }
 
    // <a href="http://www.oedilf.com/Lim.php?VerseId=123">some link text</a>
    // <a href="http://www.oedilf.com/Lim.php?VerseId=A100">#100</a>
    return sprintf('<a rel="nofollow" href="http://%s?VerseId=%s%s">%s</a>', THIS_APP, ($tentchar == '' ? 'A' : ''), $id, $linktext);
}

function FormatQuickDocLinks($text) {
  //[ [#T123:some link text]]
  // [[#456:other link text]]
  // [[#T789]]
  // [[#100]]
  // [curtain]...[/curtain]
  // [[*searchword]]

  $text = preg_replace(
    '/\[curtain\]((.| |\r|\n)*?)\[\/curtain\]/',
    '<div style="display:inline;color:#808;font-size:85%;" class="trigger">Curtained Comment</div><div style="color:#808;">\1</div> ',
    $text);

  $text = preg_replace_callback('/\[\[#(T)?([-\'0-9]+)(:(.*?))?\]\]/', "FormatQuickDocLinksCallback", $text);

  // recognize links (non ftp)
  $replacestring = '\1<a target="_blank" rel="nofollow" href="\2">\2</a>';
  // deliberate non-match of any url that is immediately preceded by "
  $text = preg_replace('@([^"])' . "((http|https)\://[-a-z0-9\.]+\.[a-z]{2,10}(:[a-z0-9]*)?/?([-a-z0-9\.\,_\?\'/\\\+&%\$#\=~;])*)@i",
      $replacestring, $text);

  $replacestring = sprintf('<a href="http://%s?Word=\1">\1</a>', THIS_APP);
  $text = preg_replace('#\[\[\*([-\'a-zA-Z0-9 ,.\-]+)\]\]#', $replacestring, $text);
  
  $replacestring = sprintf('<a href="http://%s?View=\1">\1</a>', THIS_APP);
  return preg_replace('#\[\[([-\'a-zA-Z0-9 ]+)\]\]#', $replacestring, $text);
}

function DecodeTime($DateTime) {
  preg_match("@([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})@", $DateTime, $regs);
  return mktime($regs[4], $regs[5], $regs[6], $regs[2], $regs[3], $regs[1]);
}

// pass a gmdate(...) to this
function FormatDateTime($DateTime) {
  global $member;
  return date("d M Y H:i", DecodeTime($DateTime) + ($member->Record('TimeZone') * 3600));
}

function FormatDate($DateTime) {
  global $member;
  return date("d M Y", DecodeTime($DateTime) + ($member->Record('TimeZone') * 3600));
}

function FormatRoughTime($seconds) {
    if ($seconds>=ONE_MONTH*2)
        return floor($seconds/ONE_MONTH)." months";
    else if ($seconds>=ONE_MONTH)
        return "a month";
    else if ($seconds>=ONE_WEEK*2)
        return floor($seconds/ONE_WEEK)." weeks";
    else if ($seconds>=ONE_WEEK)
        return "a week";
    else if ($seconds>=ONE_DAY*2)
        return floor($seconds/ONE_DAY)." days";
    else if ($seconds>=ONE_DAY)
        return "a day";
    else if ($seconds>=ONE_HOUR*2)
        return floor($seconds/ONE_HOUR)." hours";
    else if ($seconds>=ONE_HOUR)
        return "an hour";
    else if ($seconds>=ONE_MINUTE*2)
        return floor($seconds/ONE_MINUTE)." minutes";
    else if ($seconds>=ONE_MINUTE)
        return "a minute";
    else if ($seconds>=2)
        return floor($seconds)." seconds";
    else if ($seconds>=1)
        return "a second";
    else return "no time at all";
}

function FormatSQLTable($heading, $query, $format, $start, $pagesize, $linkparams, &$NumMatches) {
  $WordsDisplay = '';

  $result = DbQuery($query);
  $NumMatches = DbQueryRows($result);

  if ($start<0) $start=0;
  $pages = new LimPaginator($linkparams, $NumMatches, $pagesize, $start);

  if ($heading) $WordsDisplay .= '<h3>'.$heading.' '.$pages->CurrRange().'</h3>';
  if ($NumMatches>0) {
    if ($NumMatches>$pagesize)
      $WordsDisplay .= $pages->FormatNeatMultiplePageLinks();
    $WordsDisplay .= '<table class="widetable">';

    DbQuerySeek($result, $start);

    for ($i=$start; ($i<$NumMatches) and ($i<$start+$pagesize); $i++){
      $line = DbFetchArray($result) ;
      if ($i==$start) { // do headings
        $WordsDisplay .= '<tr>';
        foreach ($line as $heading => $value) {
          $WordsDisplay .= '<th>'.$heading.'</th>';
        }
        $WordsDisplay .= "</tr>\n";
      }

      $WordsDisplay .= '<tr>';
      foreach ($line as $heading => $value) {
        if (isset($format[$heading])) $formatter = $format[$heading];
        else $formatter = "";
        if ($formatter=='DateTime')
          $WordsDisplay .= FormatTableCell(FormatDateTime($value), 'randompanel');
        else if ($formatter=='DecodeTime')
          $WordsDisplay .= FormatTableCell(FormatDateTime(gmdate("Y-m-d H:i:s", $value)), 'randompanel');
        else if ($formatter=='WordLink')
          $WordsDisplay .= FormatTableCell(OneLookLink($value, true, false).' '.
          	LinkSelf("Word=".rawurlencode($value),$value),
          	'randompanel');
        else if ($formatter=='Raw')
          $WordsDisplay .= FormatTableCell($value, 'randompanel');
        else
          $WordsDisplay .= FormatTableCell(htmlspecialchars($value, ENT_QUOTES), 'randompanel');
      }
      $WordsDisplay .= "</tr>\n";
    }
    DbEndQuery($result);
    $WordsDisplay .= '</table>';
  }
  else {
    $WordsDisplay .= '<table class="widetable">';
    $WordsDisplay .= '<p>None found.</p>';
    $WordsDisplay .= '</table>';
  }

  $WordsDisplay .= $pages->FormatPaginationMenu();

  return $WordsDisplay;
}

function FormatLimerickLookup($IdNum, $Approved) {
  global $CurtainFilter ;
  $WordsDisplay = '';
  
  if ($Approved) {
    $result = DbQuery(sprintf('SELECT * FROM DILF_Limericks WHERE LimerickNumber=%d AND State="approved" '.$CurtainFilter, $IdNum));
  }
  else {
    if (LimSession::LoggedIn()) $OtherStates = 'OR State="new" OR State="revised" OR State="held" OR State="stored"';
    else $OtherStates = '';
    $result = DbQuery(sprintf('SELECT * FROM DILF_Limericks WHERE OriginalId=%d AND (State="tentative" OR State="confirming" OR State="approved" %s) '.$CurtainFilter,
      $IdNum, $OtherStates));
  }
  if ($line = DbFetchArray($result)) {

    $words = QueryWordList($line['VerseId']);
    $PageTitle = '';
    foreach ($words as $word)	$PageTitle .= $word.'; ';
    $PageTitle = trim($PageTitle, "; ");
    RSSInitializeAuthorFeedLink($line['PrimaryAuthorId']);

    $WordsDisplay .= FormatLimerickBlock($line, TRUE);

    # add metadata to the page header for Facebook and Twitter
    AddToHeader('og:url', '<meta property="og:url" content="https://www.oedilf.com/test/Lim.php?VerseId=A' . $line['LimerickNumber'] . '">');
    AddToHeader('og:type', '<meta name="og:type" content="website">');
    $Author = FormatAuthor($line['PrimaryAuthorId'], false);
    if ($line['SecondaryAuthorId']) {
        $Author .= ' and ' . FormatAuthor($line['SecondaryAuthorId'], false);
    }
    AddToHeader('og:title', '<meta name="og:title" content="Limerick #' . $line['LimerickNumber'] . ' on ' . GetWordList($line['VerseId'], false) . ' by ' . strip_tags($Author) . '">');
    AddToHeader('og-description', '<meta name="og:description" content="' . strip_tags($line['Verse']) . '">');
    AddToHeader('og:image', '<meta name="og:image" content="https://www.oedilf.com/db/oedilfi.jpg"><meta name="og:image:secure_url" content="https://www.oedilf.com/db/oedilfi.jpg">');
    AddToHeader('fb:app_id', '<meta name="fb:app_id" content="809601059091301">');
    AddToHeader('twitter:card', '<meta name="twitter:card" content="summary_large_image">');
    AddToHeader('twitter:site', '<meta name="twitter:site" content="@oedilf">');
    AddToHeader('twitter:creator', '<meta name="twitter:creator" content="@oedilf">');
    $LimBigTitle = 'Limerick #'.
	$line['LimerickNumber'].
	" on ".
	GetWordList($line['VerseId'], false).
	" by ".
	rawurlencode(strip_tags(FormatAuthor($line['PrimaryAuthorId'], false))).
    AddToHeader('twitter:title', '<meta name="twitter:title" content="' . GetWordList($line['VerseId'], false) . '">');
    AddToHeader('twitter:description', '<meta name="twitter:description" content="The Omnificent English Dictionary In Limerick Form">');
    AddToHeader('twitter:image', '<meta name="twitter:image" content="https://www.oedilf.com/test/Lim.php?LimImage=A' . $line['LimerickNumber'] . '">');
  }
  else {
    $WordsDisplay .= sprintf("<p>Limerick %s not available.</p>", $Approved ? '#'.$IdNum : '#T'.$IdNum);
  }
  DbEndQuery($result);

  return $WordsDisplay;
}

function UpdateStats() {
  $result = DbQuery("SELECT COUNT(*) Total,
SUM(State='approved') Approved,
SUM(State='approved' AND Category='normal') ApprovedGeneral,
SUM(State='approved' AND ShowcasePriority>0) ApprovedShowcase,
SUM(State='approved' AND Category='normal' AND ShowcasePriority>0) ApprovedGeneralShowcase
FROM DILF_Limericks WHERE (State='tentative' OR State='confirming' OR State='approved' OR State='revised')");
  if ($line = DbFetchArray($result)) {
    DbQuery(sprintf("UPDATE DILF_Stats SET Total=%d, Approved=%d, ApprovedGeneral=%d, ApprovedShowcase=%d, ApprovedGeneralShowcase=%d WHERE 1 LIMIT 1",
      $line['Total'], $line['Approved'], $line['ApprovedGeneral'], $line['ApprovedShowcase'], $line['ApprovedGeneralShowcase']));
  }
  DbEndQuery($result);
}

function UpdateRandomLimerick($cat) {
	global $CurtainFilter;
  
	$result = DbQuery("SELECT * FROM DILF_Stats WHERE 1 LIMIT 1");
	$line = DbFetchArray($result);
	if (LimSession::LoggedIn())
	{
		$ShowcaseFilter = '';
		if ($CurtainFilter) $Count = $line['ApprovedGeneral'];
		else $Count = $line['Approved'];
	}
	else
	{
		$ShowcaseFilter = " AND ShowcasePriority>0";
		if ($CurtainFilter) $Count = $line['ApprovedGeneralShowcase'];
		else $Count = $line['ApprovedShowcase'];
	}
	DbEndQuery($result);

    if (isset($_GET['RandomLim']))
    {
        $ShowcaseFilter = "AND OriginalId=".($_GET['RandomLim']+0);
        $CurtainFilter = "";
        $Count = 1;
    }

	$WordsDisplay = "";
	$RLim = rand(0,$Count-1);
	$result = DbQuery("SELECT * FROM DILF_Limericks WHERE State='approved'".$CurtainFilter.$ShowcaseFilter." LIMIT $RLim,1");
	if ($line = DbFetchArray($result))
	{
		$words = QueryWordList($line['VerseId']);
		$LimWords = $words[0];
		if (LimSession::LoggedIn()) $LinkParam='Workshop=';
		else $LinkParam='VerseId=';
		$WordsDisplay .= sprintf('<div style="float:right;"><table class="randomtable" style="height:135px;"><tr><th>%s %s</th></tr>',
			'Random Limerick:', LinkSelf($LinkParam.$line['OriginalId'], $LimWords));
		$WordsDisplay .= '<tr><td class="randompanel">';
		$WordsDisplay .= FormatLimerickContent($line, FALSE, FALSE);
		$WordsDisplay .= '<div class="limerickdetails">By '.FormatAuthor($line['PrimaryAuthorId'], TRUE);
		if ($line['SecondaryAuthorId']<>0) $WordsDisplay .= ' and '.FormatAuthor($line['SecondaryAuthorId'], TRUE);
		if (($line['AuthorNotes']<>'') or ($line['EditorNotes']<>''))
		$WordsDisplay .= ' '.LinkSelf('VerseId='.$line['OriginalId'], 'See Notes');
		$WordsDisplay .= ' '.FormatQuoteMeLink($line['OriginalId']);
		$WordsDisplay .= '</div></td></tr></table></div>';
	}
	DbEndQuery($result);
	
	DbUpdate("DILF_Random", array("TimeStamp"=>time(), "Text"=>DbEscapeString($WordsDisplay)), "CatId=$cat", 1);
	if (DbAffectedRows()==0)
		DbInsert("DILF_Random", array("CatId"=>$cat, "TimeStamp"=>time(), "Text"=>DbEscapeString($WordsDisplay)));
	return $WordsDisplay;
}

function FormatRandomLimerick() {
	global $CurtainFilter;
	$WordsDisplay = '';

	$cat = (LimSession::LoggedIn() ? 2:0) + ($CurtainFilter ? 1:0);
	$time = time()-10;
	$result = DbQuery("SELECT * FROM DILF_Random WHERE CatId=$cat AND TimeStamp>$time LIMIT 1");
	if ($line = DbFetchArray($result))
		$WordsDisplay .= $line['Text'];
	else $WordsDisplay .= UpdateRandomLimerick($cat);	
	DbEndQuery($result);

	$WordsDisplay = str_replace("/test/Lim.php", "/db/Lim.php", $WordsDisplay); // fix links
	return $WordsDisplay;
}

// get active author list - logged in in the last 10 minutes
function FormatAuthorsOnline() {
  global $member;
  $WordsDisplay = '';
  $AuthorList = '';
  $result = DbQuery(sprintf('SELECT * FROM DILF_Authors WHERE AccessTime>%d ORDER BY Name', time()-10*60));
  $sep='';
  while ($line = DbFetchArray($result)) {
      $name = htmlspecialchars($line['Name'], ENT_QUOTES);
    if ($member->CanChat())
      $AuthorList .= $sep.LinkPopup('SendMsg='.$line['AuthorId'], $name, "Send message to ".$name);
    else
      $AuthorList .= $sep.$name;
    $sep='; ';
  }
  if ($AuthorList) $WordsDisplay .= sprintf('<div class="smalltext">Members on-line in the past 10 minutes: %s</div>', $AuthorList);
  DbEndQuery($result);

  return $WordsDisplay;
}

$HeaderLines = array();
function AddToHeader($name, $text)
{
	global $HeaderLines;
	$HeaderLines[$name] = $text;
}

function FormatOEDILFDocHeader() {
  global $WorkshopFeed, $PageTitle, $HeaderLines, $ReloadLine, $AuthorFeed;
  $WordsDisplay = '';
  $WordsDisplay .= '<!DOCTYPE html>
<html lang="en">';
  $WordsDisplay .= "$AuthorFeed
$WorkshopFeed
<head><title>$PageTitle</title>
<link rel='alternate' type='application/rss+xml' title='RSS (includes limericks of an adult nature)' href='".BASE_DIR."feeds/rss.xml' >
<link rel='alternate' type='application/rss+xml' title='RSS (filtered view)' href='".BASE_DIR."feeds/rssfiltered.xml' >
<link rel='shortcut icon' href='favicon.ico'>
<link rel='search' type='application/opensearchdescription+xml' href='the-oedilf.xml' title='OEDILF Word Search'>
<meta name='verify-v1' content='Qq/UyQKzHEUCVq18OB8OtqYYLns9kmmcoS0tZ3Ddqbc=' />
<meta http-equiv='Content-Type' content='text/html;charset=ISO-8859-1' >";
  $WordsDisplay .= $ReloadLine; // force a reload of a different page if needed
  $WordsDisplay .= '
<link href="Lim'.$_SESSION['CSSFile'].'" rel="stylesheet" type="text/css" >
	<style type="text/css">
		@import "domcollapse.css";
	</style>
	<script type="text/javascript" src="domcollapse.js"></script>
	<script src="json3/3.3.2/json3.min.js"></script>
	<!--script src="//code.jquery.com/jquery-1.11.0.min.js"></script-->
	<script src="jquery/jquery-1.11.1.min.js"></script>
	<link href="select2-3.5.0/select2.css" rel="stylesheet"/>
	<script src="select2-3.5.0/select2.min.js"></script>
';
  $WordsDisplay .= MenuScript();
  $WordsDisplay .= implode("\n", $HeaderLines); // additions to the html header
  $WordsDisplay .= '
</head>';
  return $WordsDisplay;
}


// ------------------------------------------------------Menus
function FormatLoginJavaScriptCheck() {
    return '<script type="text/javascript">
  <!--
  document.write("<input type=hidden name=\"javascript_enabled_login\" value=\"1\">");
  //-->
  </script>';
}


// add a little javascript that causes an image to be refreshed on a regular basis
// this stops the session from timing out through lack of activity
function FormatHeartbeatImage() {
  return '<img name="keepUpdated" src="' . PHPSELF . '?Image=999" >
  <script type="text/javascript">
  <!--
  var updateEvery = 300;//Seconds
  function keepUpdated(timeDelay) {
     timer=setTimeout("updateImage()",1000*timeDelay)
  }
  function updateImage() {
    var randNum = Math.floor(Math.random()*999);
    document.images.keepUpdated.src ="' . PHPSELF . '?Image="+randNum;
    keepUpdated(updateEvery);
  }
  keepUpdated(1); // make the first time delay only 1 second - then we can use it to determine if javascript is enabled and working
  //-->
  </script>';
}

function FormatJavaScriptSellAllFunction() {
  global $AddedSellAllFunction;
  $WordsDisplay = '';
  if (!isset($AddedSellAllFunction)) {
    $WordsDisplay .= '<script language="Javascript" type="text/javascript">';
    $WordsDisplay .= 'function selall(container_id, par) { for (i = 0; i < document.getElementById(container_id).elements.length; i++) {';
    $WordsDisplay .= 'if (document.getElementById(container_id).elements[i]) { document.getElementById(container_id).elements[i].checked = par; }';
    $WordsDisplay .= '}}</script>';
	$AddedSellAllFunction = true;
  }
  return $WordsDisplay;
}

?>
