<?php
// Allow us to browse limericks by topic.

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

function TopicSelectionList($preselectid, $TopicList=false) {
  $selectedTopics = array();
  if ($TopicList) {
    foreach ($TopicList as $TopicLine) {
      $selectedTopics[$TopicLine['TopicId']] = $TopicLine['TopicId'];
    }
  }
  $WordsDisplay = '';
  $result = DbQuery("SELECT * FROM DILF_Topics WHERE 1 ORDER BY Name");
  $NumTopics = DbQueryRows($result);
## "(select a topic)" no longer needed with select2
#  if ($preselectid==0)
#    $WordsDisplay .= '<option value="0" selected>(select a topic)';
#  else
#    $WordsDisplay .= '<option value="0">(select a topic)';
  for ($i=0; $i<$NumTopics; $i++) {
    $line = DbFetchArray($result) ;
    if ($line['TopicId']==$preselectid
        || in_array($line['TopicId'], $selectedTopics)
       )
      $sel = 'selected';
    else
      $sel = '';
    $WordsDisplay .= sprintf('<option value="%s" %s>%s', $line['TopicId'], $sel, $line['Name']);
  }
  DbEndQuery($result);
  return $WordsDisplay;
}

function GetTopicQuery($TopicId, $selection, $extraStates="") {
	global $CurtainFilter;

	return sprintf("SELECT $selection, 
	(L.State='approved')*2+(L.ShowcasePriority>0) App, 
	(L.State='approved')*L.LimerickNumber+(L.State<>'approved')*L.OriginalId LOrder 
	FROM DILF_TopicLims T, DILF_Limericks L 
	WHERE T.TopicId=%d AND T.OriginalId=L.OriginalId AND (L.State='approved' OR L.State='tentative' OR L.State='revised' OR L.State='confirming' $extraStates) $CurtainFilter 
	ORDER BY App DESC, LOrder", 
	$TopicId);
}

function GetTopicLimericks($TopicId, $extraStates="") {

  global $CurtainFilter;
  $LimList = array();

  $result = DbQuery(GetTopicQuery($TopicId, "L.State, L.OriginalId, L.LimerickNumber", $extraStates));
  while ($limline=DbFetchArray($result)) {
    $LimList[] = $limline;
  }
  DbEndQuery($result);
  return $LimList;
}

function FormatTopic($TopicId, $start, $pagelength) {
  global $member;
  $WordsDisplay = '';

  $result = DbQuery(sprintf("SELECT * FROM DILF_Topics WHERE TopicId=%d", $TopicId));
  if ($line=DbFetchArray($result)) {
    $name = $line['Name'];
    $description = $line['Description'];

    $BackLine = '';
    $result2 = DbQuery(sprintf("SELECT T.Name, T.TopicId FROM DILF_SubTopics S, DILF_Topics T WHERE S.SubId=%d AND S.TopicId=T.TopicId ORDER BY T.Name", $TopicId));
    if (DbQueryRows($result2)>0) {
      $BackLine .= 'Back to: ';
      while ($subline=DbFetchArray($result2)) {
        $BackLine .= ' '.LinkSelf("Topic=".$subline['TopicId'], $subline['Name']);
      }
    }
    $BackLine .= ' '.LinkSelf("Show=Topics", 'Topic Map');
    DbEndQuery($result2);

    $WordsDisplay .= '<table class="widetable">';
    $WordsDisplay .= '<tr><th><span class="subheading">'.LinkSelf('Topic=1','Browse Limericks by Topic').': ';
    if ($member->CanWorkshop())
      $WordsDisplay .= LinkSelf(array("searchstart=Search", "searchauthor=0", "searchtopic=$TopicId"), $line['Name']);
    else $WordsDisplay .= '<b>'.$line['Name'].'</b>';
    $WordsDisplay .= '</span></th></tr>';
    if ($BackLine) $WordsDisplay .= '<tr><td>'.$BackLine.'</td></tr>';
    $WordsDisplay .= '<tr><td class="darkpanel">';
    $WordsDisplay .= '<p>'.FormatQuickDocLinks(AddBreaks($line['Description'])).'</p>';

    $SubTopics = '';
    $result2 = DbQuery(sprintf("SELECT T.Name, T.TopicId FROM DILF_SubTopics S, DILF_Topics T WHERE S.TopicId=%d AND S.SubId=T.TopicId ORDER BY T.Name", $TopicId));
    if (DbQueryRows($result2)>0) {
      $SubTopics .= '<ul class="topiclist">';
      while ($subline=DbFetchArray($result2)) {
        $SubTopics .= '<li>'.LinkSelf("Topic=".$subline['TopicId'], $subline['Name']);

        $SubSubTopics = '';
        $SubSubSep = '';
        // add a sublist as well to fatten it out
        $result3 = DbQuery(sprintf("SELECT T.Name, T.TopicId FROM DILF_SubTopics S, DILF_Topics T WHERE S.TopicId=%d AND S.SubId=T.TopicId ORDER BY T.Name", $subline['TopicId']));
        while ($subsubline=DbFetchArray($result3)) {
          $SubSubTopics .= $SubSubSep.LinkSelf("Topic=".$subsubline['TopicId'], $subsubline['Name']);
          $SubSubSep = '&nbsp; ';
        }
        DbEndQuery($result3);
        if ($SubSubTopics) $SubTopics .= '&nbsp;&nbsp;&nbsp;&nbsp; <span class="subtopictext">('.$SubSubTopics.')</span>';

      }
      $SubTopics .= '</ul>';
    }
    DbEndQuery($result2);

    $Limerick = '';
    $LimList = GetTopicLimericks($TopicId);

    if ($LimList) {
		$formatter = new LimTopicFormatter(LimSession::LoggedIn(), "style='width: 100%;'");
		$Limerick .= FormatLimerickList('Limericks on '.$line['Name'],
			GetTopicQuery($TopicId, "L.*"), $start, $pagelength, 
			array("Topic=$TopicId", "PageLen=$pagelength"), 
			$NumLimericks, $formatter);      
    }

    $WordsDisplay .= $SubTopics;
    $WordsDisplay .= $Limerick;

    if (!$SubTopics and !$Limerick) $WordsDisplay .= '<p><br><br><center>This topic has no links or limericks yet.</center><br><br><br></p>';

    $WordsDisplay .= '</td>';
    if ($BackLine) $WordsDisplay .= '<tr><td>'.$BackLine.'</td></tr>';
    $WordsDisplay .= '</tr></table>';

    if ($member->HasPrivilege('TopicEditor')) {
      $WordsDisplay .= '<table class="menutable"><tr><td class="menus">';

      if ($_SESSION['$ShowTopicEdit']=='On') 
	    $WordsDisplay .= LightLinkSelf(array("Topic=".$TopicId, "TopicEdit=Off"), "Hide Topic Editing");
      else $WordsDisplay .= LightLinkSelf(array("Topic=".$TopicId, "TopicEdit=On"), "Show Topic Editing");
      if ($_GET['TopicEdit']) {
        $_SESSION['$ShowTopicEdit'] = $_GET['TopicEdit'];
        SetRedirect("Topic=".$TopicId);
      }

      if ($_SESSION['$ShowTopicEdit']=='On') {
        $WordsDisplay .= ' '.LightLinkSelf(array("Action=AddTopic", "TopicParent=$TopicId"), "Create a new sub-topic");
        $WordsDisplay .= ' '.LightLinkSelf('TopicLinks='.$TopicId, "Edit topic links");
        if ($TopicId<>1)
          $WordsDisplay .= ' '.LightLinkSelf(array('DeleteTopic='.$TopicId, 'TopicName='.addslashes($name)), "Delete this topic");
      }
      $WordsDisplay .= ' <span class="hinttext">('.LinkPopup("View=Editing%20Topics", "Help: Editing Topics in the Topic Browser").')</span>';
      $WordsDisplay .= '</td></tr></table>';

      if ($_SESSION['$ShowTopicEdit']=='On') {
        $WordsDisplay .= sprintf('<form action="%s?UpdateTopic=%d" method="post" name="EditTopicForm">', PHPSELF, $TopicId);
        $WordsDisplay .= '<table class="widetable"><tr><th colspan=3>Modify Topic Name and Description</th></tr>';
		$LimList = GetTopicLimericks($TopicId, "OR L.State='new'");
        $WordsDisplay .= FormatTopicEditForm($name, $description, $LimList, 
        	SubmitButton("TopicButton", "Update Topic"));
        $WordsDisplay .= '</table>';
        $WordsDisplay .= '</form>';
      }
    }
  }
  else {
    $WordsDisplay .= sprintf("<p>Couldn't find topic #%s</p>", $TopicId);
    $WordsDisplay .= "<p>".LinkSelf("Topic=1", "BrowseTopics")."</p>";
  }
  DbEndQuery($result);

  return $WordsDisplay;
}

function ProcessDeleteTopic($TopicId, $Name, $Confirmed) {
  global $member;
  $WordsDisplay = '';

  if ($member->HasPrivilege('TopicEditor')) {
    if ($Confirmed) {
      $result = DbQuery("DELETE FROM DILF_Topics WHERE TopicId=".$TopicId);
      $result = DbQuery("DELETE FROM DILF_SubTopics WHERE SubId=".$TopicId." OR TopicId=".$TopicId);
      $links = DbAffectedRows();
      $result = DbQuery("DELETE FROM DILF_TopicLims WHERE TopicId=".$TopicId);
      $limlinks = DbAffectedRows();
      $WordsDisplay .= "<p>Topic deleted along with $links link(s) and $limlinks limerick reference(s).</p>";
      $WordsDisplay .= "<p>".LinkSelf("Topic=1", "BrowseTopics")."</p>";
    }
    else {
      $WordsDisplay .= "<p>Are you sure you want to delete topic #$TopicId, $Name?</p>";
      $WordsDisplay .= "<p>".LightLinkSelf(array('DeleteConfirmed=1', 'DeleteTopic='.$TopicId), "Yes, delete it.");
      $WordsDisplay .= ' '.LightLinkSelf('Topic='.$TopicId, "No.");
      $WordsDisplay .= "</p>";
    }
  }
  else $WordsDisplay .= '<p>You must be logged in with sufficient privilege to perform this action.</p>';

  return $WordsDisplay;
}

function ProcessNewTopic() {
  global $member;
  $WordsDisplay = '';

  if ($member->HasPrivilege('TopicEditor')) {
    $topicname = trim(CleanupHtml($_POST['topicname']), "\n\r ");
    if ($topicname<>'') {
      $topicdescription = trim(CleanupHtml($_POST['topicdescription']), "\n\r ");
      DbInsert("DILF_Topics", array("Name"=>$topicname, "Description"=>$topicdescription));
      $TopicId = DbLastInsert();
      // make it a child of the passed parent
      DbInsert("DILF_SubTopics", array("TopicId"=>$_POST['topicparent']+0, "SubId"=>$TopicId));
      // add limericks
      UpdateTopicLimericks($TopicId, $_POST['topiclimericks']);

      SetRedirect('Topic='.$TopicId);
    }
    else
      $WordsDisplay .= '<p>The topic name must contain at least one character.</p>';
  }
  else $WordsDisplay .= '<p>You must be logged in with sufficient privilege to view this page.</p>';

  $WordsDisplay .= LinkSelf('Action=AddTopic', 'Back to Add Topic');
  return $WordsDisplay;
}

function ProcessUpdateTopic($TopicId) {
  global $member;
  $WordsDisplay = '';

  if ($member->HasPrivilege('TopicEditor')) {
    $topicname = trim(CleanupHtml($_POST['topicname']), "\n\r ");
    if ($topicname<>'') {
      $topicdescription = trim(CleanupHtml($_POST['topicdescription']), "\n\r ");
      $result = DbQuery(sprintf("UPDATE DILF_Topics SET Name='%s', Description='%s' WHERE TopicId=%d", $topicname, $topicdescription, $TopicId));
      // add limericks
      UpdateTopicLimericks($TopicId, $_POST['topiclimericks']);

      SetRedirect('Topic='.$TopicId);
    }
    else
      $WordsDisplay .= '<p>The topic name must contain at least one character.</p>';
  }
  else $WordsDisplay .= '<p>You must be logged in with sufficient privilege to view this page.</p>';

  $WordsDisplay .= LinkSelf('Topic='.$TopicId, 'Back to Topic');
  return $WordsDisplay;
}

function FormatTopicEditForm($name, $description, $LimList, $Button, $EditParent=FALSE) {
  $WordsDisplay = '';

  $lims = '';
  if ($LimList) foreach($LimList as $LimLine) {
    if ($LimLine['State']=='approved')
      $lims .= $LimLine['LimerickNumber']."\n";
    else
      $lims .= 'T'.$LimLine['OriginalId']."\n";
  }

  $WordsDisplay .= '<tr>';
  $WordsDisplay .= FormatTableCell('Topic Name', 'darkpanel');
  $WordsDisplay .= FormatTableCell('<INPUT type=text name="topicname" VALUE="'.$name.'">', 'lightpanel');
  $WordsDisplay .= '<td rowspan=2 class="lightpanel">'.$Button.'</td>';
  $WordsDisplay .= '</tr>';
  $WordsDisplay .= '<tr>';
  $WordsDisplay .= FormatTableCell('Limerick Numbers', 'darkpanel');
  $WordsDisplay .= FormatTableCell('<TEXTAREA NAME="topiclimericks" COLS=30 ROWS=10>'.$lims.'</TEXTAREA>', 'lightpanel');
  $WordsDisplay .= '</tr>';
  $WordsDisplay .= '<tr>';
  $WordsDisplay .= FormatTableCell('Description (optional)', 'darkpanel');
  $WordsDisplay .= '<td colspan=2 class="lightpanel"><TEXTAREA NAME="topicdescription" COLS=80 ROWS=3>'.$description.'</TEXTAREA></td>';
  $WordsDisplay .= '</tr>';
  if ($EditParent) {
    $WordsDisplay .= '<tr>';
    $WordsDisplay .= FormatTableCell('Sub-topic of:', 'darkpanel');
    $WordsDisplay .= '<td colspan=2 class="lightpanel"><select name="topicparent">'.TopicSelectionList($_GET['TopicParent']+0).'</select></td>';
    $WordsDisplay .= '</tr>';
  }
  return $WordsDisplay;
}

function FormatNewTopic() {
  global $member;
  $WordsDisplay = '';

  if ($member->HasPrivilege('TopicEditor')) {
    $WordsDisplay .= sprintf('<form action="%s?Action=AddTopic" method="post" name="NewTopicForm">', PHPSELF);
    $WordsDisplay .= '<table class="widetable">';
    $WordsDisplay .= FormatTopicEditForm('', '', array(), 
    	SubmitButton("TopicButton", "Add Topic"), TRUE);
    $WordsDisplay .= '</table>';
    $WordsDisplay .= '</form>';
  }
  else $WordsDisplay .= '<p>You must be logged in with sufficient privilege to view this page.</p>';

  return $WordsDisplay;
}

function UpdateTopicLimericks($TopicId, $NumberString){
	$NumList = explode(" ", str_replace(array("\n","\r",";",",","#"), array(" "," "," "," "," "), $NumberString));
	$olist = "";
	$alist = "";
	if ($NumList) foreach($NumList as $LimNum) {
		if (($LimNum+0>0) or (($LimNum{0}=='T') and (substr($LimNum,1)+0>0))) {
			// find the original id first
			if ($LimNum{0}=='T')
				$olist .= substr($LimNum,1).",";
			else
				$alist .= $LimNum.",";
		}
	}
	
	$condition = "";
	if ($alist!="") {
		$alist = substr($alist, 0, -1);
		$condition .= "(LimerickNumber IN ($alist))";
	}
	
	if ($olist!="") {
		$olist = substr($olist, 0, -1);
		if ($condition != "") $condition .= " OR ";
		$condition .= "(OriginalId IN ($olist))";
	}
  
	// delete current
	$result = DbQuery("DELETE FROM DILF_TopicLims WHERE TopicId=".$TopicId );
	
    if ($condition != "") {
		$result = DbQuery("INSERT IGNORE INTO DILF_TopicLims 
			SELECT $TopicId AS TopicId, OriginalId 
			FROM DILF_Limericks WHERE State<>'obsolete' AND ($condition)" );
	}
}

function FormatTopicLinks($TopicId) {
  global $member;
  $WordsDisplay = '';

  if ($member->HasPrivilege('TopicEditor')) {
    if ($_POST['TopicButton']=='Set Topic Links') {
      // here we change the links for parentage
      // delete previous links
      $result = DbQuery("DELETE FROM DILF_SubTopics WHERE SubId=".$TopicId );
      // add new ones
      if ($_POST['topicparents'])
        foreach ($_POST['topicparents'] as $parent)
          if ($parent<>$TopicId) DbInsert("DILF_SubTopics", array("TopicId"=>$parent, "SubId"=>$TopicId));
          
      // here we change the links to children
      // delete previous links
      $result = DbQuery("DELETE FROM DILF_SubTopics WHERE TopicId=".$TopicId );
      // add new ones
      if ($_POST['topicchildren'])
        foreach ($_POST['topicchildren'] as $child)
          if ($child<>$TopicId) DbInsert("DILF_SubTopics", array("TopicId"=>$TopicId, "SubId"=>$child));
      SetRedirect('Topic='.$TopicId);
    }
    else if ($_POST['TopicButton']=='Cancel')
      SetRedirect('Topic='.$TopicId);
    else {
      $result = DbQuery(sprintf("SELECT * FROM DILF_Topics WHERE TopicId=%d", $TopicId));
      if ($line=DbFetchArray($result))
        $TopicName = $line['Name'];
      DbEndQuery($result);

      // Pull the topic name list into memory
      $TopicNames = array();
      $result = DbQuery("SELECT * FROM DILF_Topics WHERE 1 ORDER BY Name");
      while ($line=DbFetchArray($result))
        $TopicNames[$line['TopicId']] = $line['Name'];
      DbEndQuery($result);

      // Pull the sub-topic links into memory
      $IsParent = array();
      $result = DbQuery("SELECT * FROM DILF_SubTopics WHERE SubId=".$TopicId);
      while ($line=DbFetchArray($result))
        $IsParent[$line['TopicId']] = TRUE;
      DbEndQuery($result);
      $IsChild = array();
      $result = DbQuery("SELECT * FROM DILF_SubTopics WHERE TopicId=".$TopicId);
      while ($line=DbFetchArray($result))
        $IsChild[$line['SubId']] = TRUE;
      DbEndQuery($result);

      $WordsDisplay .= sprintf('<form action="%s?TopicLinks=%d" method="post" name="NewTopicForm">', PHPSELF, $TopicId);
      $WordsDisplay .= '<table class="widetable"><tr><th>Topics linking to '.$TopicName.'</th><th>Sub-topics of '.$TopicName.'</th></tr>';
      $WordsDisplay .= '<tr><td class="darkpanel" colspan=2><span class="hinttext">Help: Hold the <i>ctrl</i> key down while you click on topics in the lists if you want to select multiple topics or to deselect topics.</span></td></tr>';
      $WordsDisplay .= '<tr><td class="darkpanel" width=50%>';
      $WordsDisplay .= '<select name="topicparents[]" multiple size=12>';
      foreach($TopicNames as $id => $name)
        if ($id<>$TopicId)
          $WordsDisplay .= sprintf('<option value="%s" %s>%s', $id, ($IsParent[$id] ? 'selected' : ''), ($IsParent[$id] ? '###' : '').$name);
      $WordsDisplay .= '</select>';
      $WordsDisplay .= '</td>';
      $WordsDisplay .= '<td class="darkpanel" width=50%>';
      $WordsDisplay .= '<select name="topicchildren[]" multiple size=12>';
      foreach($TopicNames as $id => $name)
        if ($id<>$TopicId)
          $WordsDisplay .= sprintf('<option value="%s" %s>%s', $id, ($IsChild[$id] ? 'selected' : ''), ($IsChild[$id] ? '###' : '').$name);
      $WordsDisplay .= '</select>';
      $WordsDisplay .= '</td></tr>';
      $WordsDisplay .= '<tr>';
      $WordsDisplay .= '<td class="darkpanel" colspan=2>'.
      	SubmitButton("TopicButton", "Set Topic Links");
      $WordsDisplay .= ' '.SubmitButton("TopicButton", "Cancel").'</td>';
      $WordsDisplay .= '</tr>';
      $WordsDisplay .= '</table>';
      $WordsDisplay .= '</form>';
    }
  }
  else $WordsDisplay .= '<p>You must be logged in with sufficient privilege to view this page.</p>';

  return $WordsDisplay;
}

function FormatAddRemoveTopicLim($OriginalId) {
  global $member;
  $WordsDisplay = '';

  if ($member->CanContribute()) {
    $TopicList = array();
    $result = DbQuery(sprintf("SELECT * FROM DILF_TopicLims L, DILF_Topics T WHERE L.TopicId=T.TopicId AND L.OriginalId=%d ORDER BY T.Name", $OriginalId));
    while ($limline=DbFetchArray($result)) {
      $TopicList[] = $limline;
    }
    DbEndQuery($result);

    if ($_SESSION['JavascriptWorks'] and ($_SESSION['JavascriptTree']=='On')) {
      $WordsDisplay .= '<table class="widetable">';
      $WordsDisplay .= '<tr><td class="darkpanel">';
      $WordsDisplay .= FormatTopicTreeJS($OriginalId);
      $WordsDisplay .= '</td>';
      $WordsDisplay .= '<td width=120px>'.LightLinkSelf(array("JavascriptTree=Off", "Workshop=".$OriginalId), "Use Simple View").'</td>';
      $WordsDisplay .= '</tr>';
      $WordsDisplay .= '</table>';
    }
    else {  // old code for adding limericks to topics
      $WordsDisplay .= '<table class="menutable">';
      $WordsDisplay .= '<tr>';
      $WordsDisplay .= '<td class="menus">';
#      if ($TopicList) {
#        $WordsDisplay .= 'Remove this limerick from:';
#        foreach ($TopicList as $TopicLine)
#          $WordsDisplay .= ' '.LightLinkSelf(array('RemoveTopicLimerick='.$OriginalId, 'TargetTopic='.$TopicLine['TopicId']), $TopicLine['Name'], "Remove limerick from ".$TopicLine['Name']);
#      }
#      else $WordsDisplay .= 'This limerick is not currently listed in the topic browser.';
#
      $WordsDisplay .= sprintf('<form action="%s" method="get" name="TopicAddForm">', PHPSELF);
      $WordsDisplay .= 'Topics: ';
      #$WordsDisplay .= 'Topics: <select name="TargetTopic[]" id="TargetTopic" multiple="multiple">';
      $result = DbQuery("SELECT * FROM DILF_TopicLims WHERE OriginalId=".$OriginalId);
      $selected = '';
      while ($line=DbFetchArray($result)) {
        $selected .= $line['TopicId'] . ',';
      }
      if ($selected == '')
	$selected = '_empty';
      else
        $selected = rtrim($selected, ',');
      DbEndQuery($result);
      if ($selected != "_empty") {
        $WordsDisplay .= '<input name="TargetTopic[]" id="TargetTopic" type="hidden" style="width:500px" value="' . $selected . '" data-placeholder="(click here and type to select a topic)"></input>';
      } else {
        $WordsDisplay .= '<input name="TargetTopic[]" id="TargetTopic" type="hidden" style="width:500px" data-placeholder="(click here and type to select a topic)"></input>';
      }
      #$WordsDisplay .= "<option selected disabled></option>";
      #$WordsDisplay .= TopicSelectionList(0, $TopicList);
      #$WordsDisplay .= '</select>';
# the hidden field ensures useful stuff is still sent if all options are deselected
      $WordsDisplay .= sprintf('<input type=hidden name="AddTopicLimerick" value="%s">', $OriginalId);
      //$WordsDisplay .= '<a class="macro" href="javascript:window.document.TopicAddForm.submit();">Add</a>';
      $WordsDisplay .= SubmitButton("LimerickTopicButton", "Set topics");
      $WordsDisplay .= '</td>';
      $WordsDisplay .= '<td class="menusright">';
      $WordsDisplay .= ' '.LinkPopup('Show=Topics', 'Topic Map');
      $WordsDisplay .= ' '.LinkPopup('View=Topic%20Browser', 'Help');
      if ($_SESSION['JavascriptWorks'])
        $WordsDisplay .= ' '.LightLinkSelf(array("JavascriptTree=On", "Workshop=".$OriginalId), "Use Tree View");
      $WordsDisplay .= '</form>';
      $WordsDisplay .= '</td></tr>';
      $WordsDisplay .= '</table>';
      $WordsDisplay .= '<div id="idTreeJson" display="none"></div>';
      $WordsDisplay .= '<script>
	$(document).ready(function() {
';
      $WordsDisplay .= FormatTopicTreeJSON($OriginalId);
      $WordsDisplay .= <<<ENDSELECT2
	function formatResult(item, container, query, escapeMarkup) {
		var markup = [];
		window.Select2.util.markMatch(item.text, query.term, markup, escapeMarkup);
		var result = '<span class="topicSelectValue">' + markup.join("") + '</span>';
		if (item.container)
			result += ' <span class="topicSelectValueContainer">(' + item.container.substring(item.container.indexOf('/') + 1) + ")</span>";
		return result;
	}
	function sortByKey(array, key) {
	    return array.sort(function(a, b) {
		var x = a[key]; var y = b[key];
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	    });
	}
	$("#TargetTopic").select2({
		width: "500px",
		placeholder: "(click here and type to select a topic)",
		allowClear: true,
		containerCssClass: "topicSelectContainer",
		dropdownCssClass: "topicSelectDropdown",
		data: function(){return {results: sortByKey(topicJson, "text")};},
		multiple: true,
		formatResult: formatResult
	});
});
</script>
ENDSELECT2;
    }
  }
  return $WordsDisplay;
}

function RemoveLimerickFromTopic() {
  global $member;
  $WordsDisplay = '';

  if ($member->CanContribute()) {
    $result = DbQuery(sprintf("DELETE FROM DILF_TopicLims WHERE TopicId=%d AND OriginalId=%d", $_GET['TargetTopic'], $_GET['RemoveTopicLimerick']));
    AuditLog($_GET['RemoveTopicLimerick'], 'Removed limerick from topic '.$_GET['TargetTopic']);
    SetRedirect('Workshop='.$_GET['RemoveTopicLimerick']);
  }
  return $WordsDisplay;
}

function AddLimerickToTopic() {
  global $member;
  $WordsDisplay = '';

  if ($member->CanContribute()) {
    if ($_GET['LTopics']) { // called with a list of topics from the selection tree
      $result = DbQuery(sprintf("DELETE FROM DILF_TopicLims WHERE OriginalId=%d", $_GET['AddTopicLimerick'])); // remove limerick from all topics
      $TopicList = explode(";", $_GET['LTopics']);
      foreach ($TopicList as $TopicNum) {
        if ($TopicNum) DbInsert("DILF_TopicLims", array("TopicId"=>$TopicNum, "OriginalId"=>$_GET['AddTopicLimerick']));
      }
      AuditLog($_GET['AddTopicLimerick'], 'Added limerick to topics '.$_GET['LTopics']);
      SetRedirect('Workshop='.$_GET['AddTopicLimerick']);
    }
    else if (!$_GET['TargetTopic']) {
      $WordsDisplay .= '<p>You must select a topic for your limerick before adding.</p>';
      SetRedirect('Workshop='.$_GET['AddTopicLimerick']);
    }
    else {
      $result = DbQuery(sprintf('DELETE FROM DILF_TopicLims WHERE OriginalId=%d', $_GET['AddTopicLimerick']));
      if ($_GET['TargetTopic'] and $_GET['TargetTopic'] != '') {
        foreach ($_GET['TargetTopic'] as $newTopicArray) {
          foreach(explode(',', $newTopicArray) as $newTopic) {
	    if ($newTopic != "_empty" && $newTopic != '') {
              DbInsert("DILF_TopicLims", array("TopicId"=>$newTopic+0, "OriginalId"=>$_GET['AddTopicLimerick']+0));
              AuditLog($_GET['AddTopicLimerick'], 'Added limerick to topic '.$newTopic);
	    }
	  }
	}
      }
      SetRedirect('Workshop='.$_GET['AddTopicLimerick']);
    }
  }
  return $WordsDisplay;
}

function GenerateTopicMap(&$TopicNames, &$TopicMapped, &$TopicChildren, &$LimCounts) {
  // Pull the topic name list into memory
  $TopicNames = array();
  $TopicMapped = array();
  $TopicChildren = array();
  $result = DbQuery("SELECT * FROM DILF_Topics WHERE 1 ORDER BY Name");
  while ($line=DbFetchArray($result)) {
    $TopicId = $line['TopicId'];
    $TopicNames[$TopicId] = $line['Name'];
    $TopicMapped[$TopicId] = FALSE;
    $TopicChildren[$TopicId] = array();
  }
  DbEndQuery($result);

  // Get the limerick counts
  $LimCounts = array();
  $result = DbQuery("SELECT TopicId, COUNT(*) Count FROM DILF_TopicLims WHERE 1 GROUP BY TopicId");
  while ($line=DbFetchArray($result)) {
    $TopicId = $line['TopicId'];
    $LimCounts[$TopicId] = ' ('.$line['Count'].')';
  }
  DbEndQuery($result);

  $TopicMapped[1] = TRUE;
  // Pull the sub-topic links into memory and build a tree (preventing recursion)
  $result = DbQuery("SELECT S.TopicId, S.SubId FROM DILF_SubTopics S, DILF_Topics T WHERE S.SubId=T.TopicId ORDER BY T.Name");
  while ($line=DbFetchArray($result)) {
    $ChildId = $line['SubId'];
    $ParentId = $line['TopicId'];
    if (!$TopicMapped[$ChildId]) {
      $TopicMapped[$ChildId] = TRUE;
      $TopicChildren[$ParentId][] = $ChildId;
    }
  }
  DbEndQuery($result);
}

function FormatTopicMap() {
  $WordsDisplay = '';


  GenerateTopicMap($TopicNames, $TopicMapped, $TopicChildren, $LimCounts);

  // find and store the roots of all disconnected trees in a super node
  $LooseNodes = array();
  foreach ($TopicMapped as $TopicId => $mapped)
    if (!$mapped) $LooseNodes[] = $TopicId;

  $WordsDisplay .= '<table class="widetable"><tr><th><span class="subheading">'.LinkSelf('Topic=1','Browse Limericks by Topic').': Topic Map</span></th></tr>';
  $WordsDisplay .= '<tr><td class="darkpanel">';
  // get the display string
  $WordsDisplay .= "\n".LinkSelf("Topic=1", $TopicNames[1].$LimCounts[1]).RecurseTree(1, $TopicChildren, $TopicNames, $LimCounts);

  // show disconnected nodes
  if ($LooseNodes) {
    $WordsDisplay .= '<p>Disconnected nodes:</p><ul>';
    foreach ($LooseNodes as $node)
      $WordsDisplay .= "\n".LinkSelf("Topic=".$node, $TopicNames[$node].$LimCounts[$node]).RecurseTree($node, $TopicChildren, $TopicNames, $LimCounts);
    $WordsDisplay .= '</ul>';
  }

  $WordsDisplay .= '</td></tr>';
  $WordsDisplay .= '</table>';

  if ($_GET['GenMap']) $WordsDisplay .= FormatTopicTreeJS();

  return $WordsDisplay;
}

function RecurseTree($NodeId, $Children, $Names, $LimCounts) {
  $WordsDisplay = '';
  $WordsDisplay .= '<ul>';
  foreach ($Children[$NodeId] as $id) {
    $WordsDisplay .= '<li>'.LinkSelf("Topic=".$id, $Names[$id]).$LimCounts[$id];
    if (count($Children[$id])) $WordsDisplay .= RecurseTree($id, $Children, $Names, $LimCounts);
  }
  $WordsDisplay .= '</ul>';
  return $WordsDisplay;
}

function FormatTopicTreeJS($OriginalId) {
  global $OnLoadLines;
  $WordsDisplay = '';

  GenerateTopicMap($TopicNames, $TopicMapped, $TopicChildren, $LimCounts);

  AddToHeader("TopicTreeCss", '<link href="LimTopicTree.css" rel="stylesheet" type="text/css">');
  AddToHeader("TopicTreeJs", '<script type="text/javascript" src="LimTopicTree.js"></script>');
  $OnLoadLines .= "onload=\"doLoad();\"\n";

  $HelpLink .= LinkPopup('View=Topic%20Browser', 'Help');

  $WordsDisplay .= <<<INCLUDEJSTREE
Topics selected: <span id="idTopicList">(finding topics)</span>
<div id="idTopicSelectionTree" style="display:none">
<span style="float:right;text-align:right;">
<a class="nav" href='javascript:ExpandAll(document.getElementById("idTree"));'>Expand All</a>
<a class="nav" href='javascript:ContractAll(document.getElementById("idTree"));'>Contract All</a>
$HelpLink
</span>
<script type="text/javascript">
<!--
document.write("<div id=\"idTree\">");
OriginalId = $OriginalId;

INCLUDEJSTREE;

  // write out javascript to generate the tree
  $WordsDisplay .= RecurseJSTree(1, $TopicChildren, $TopicNames, $LimCounts);

  // write out javascript to pre-select the currently select topics
  $result = DbQuery("SELECT TopicId FROM DILF_TopicLims WHERE OriginalId=".$OriginalId );
  while ($line=DbFetchArray($result))
    $WordsDisplay .= "SelectedTopics[".$line['TopicId']."] = true;\n";
  DbEndQuery($result);

  $WordsDisplay .= <<<INCLUDEJSTREE
document.write("</div>");
-->
</script>
</div>
INCLUDEJSTREE;

  return $WordsDisplay;
}

function FormatTopicTreeJSON($OriginalId) {
  global $OnLoadLines;
  $WordsDisplay = '';
  GenerateTopicMap($TopicNames, $TopicMapped, $TopicChildren, $LimCounts);
  AddToHeader("TopicTreeCss", '<link href="LimTopicTree.css" rel="stylesheet" type="text/css">');
  AddToHeader("TopicTreeJs", '<script type="text/javascript" src="LimTopicTree.js"></script>');

  // write out javascript to pre-select the currently selected topics
  $result = DbQuery("SELECT L.TopicId, T.Name FROM DILF_TopicLims L, DILF_Topics T WHERE L.TopicId = T.TopicId AND OriginalId=".$OriginalId);
  $WordsDisplay .= "\ninitialTopics = [";
  while ($line = DbFetchArray($result)) {
    $WordsDisplay .= '{"id": "' . $line['TopicId'] . '", "text": "' . $line['Name'] . '"}, ';
  }
  DbEndQuery($result);
  $WordsDisplay .= "];\n";

  $WordsDisplay .= <<<INCLUDEJSONTREE
OriginalId = $OriginalId;
topicJson = '[';
container = '';

INCLUDEJSONTREE;
  // write out javascript to generate the tree
  $WordsDisplay .= RecurseJSONTree(1, $TopicChildren, $TopicNames, $LimCounts);
  $WordsDisplay .= <<<INCLUDEJSONTREE
topicJson += ']';
topicJson = topicJson.replace(/,\s*\]/g, "]").replace(/,\s*\}/g, "}").replace(/,\s*$/, "");
topicJson = JSON.parse(topicJson);
INCLUDEJSONTREE;


  return $WordsDisplay;
}

function RecurseJSTree($NodeId, $Children, $Names, $LimCounts) {
  $WordsDisplay = '';

  $WordsDisplay .= "AddBranchNode(".$NodeId.", \"".$Names[$NodeId]."\");\n" ;
  foreach ($Children[$NodeId] as $id) {
    if (count($Children[$id]))
      $WordsDisplay .= RecurseJSTree($id, $Children, $Names, $LimCounts);
    else
      $WordsDisplay .= "AddLeafNode(".$id.", \"".$Names[$id]."\");\n" ;
  }
  $WordsDisplay .= "FinishBranchNode();\n";

  return $WordsDisplay;
}

function RecurseJSONTree($NodeId, $Children, $Names, $LimCounts) {
  $WordsDisplay = '';

  $WordsDisplay .= "topicJson += AddBranchNodeJSON(".$NodeId.", \"".$Names[$NodeId]."\");\n" ;
  foreach ($Children[$NodeId] as $id) {
    if (count($Children[$id]))
      $WordsDisplay .= RecurseJSONTree($id, $Children, $Names, $LimCounts);
    else
      $WordsDisplay .= "topicJson += AddLeafNodeJSON(".$id.", \"".$Names[$id]."\");\n" ;
  }
  $WordsDisplay .= "topicJson += FinishBranchNodeJSON();\n";

  return $WordsDisplay;
}

function LimerickTopicLinks($OriginalId) {
  $WordsDisplay = '';

  $sep='';
  if ($OriginalId<>'?') {
    $result = DbQuery(sprintf("SELECT * FROM DILF_TopicLims L, DILF_Topics T WHERE L.TopicId=T.TopicId AND L.OriginalId=%d ORDER BY T.Name", $OriginalId));
    while ($limline=DbFetchArray($result)) {
      $WordsDisplay .= $sep.LinkSelf("Topic=".$limline['TopicId'], $limline['Name']);
      $sep = ', ';
    }
    DbEndQuery($result);
  }

  return $WordsDisplay;
}
?>
