#!/usr/local/bin/python

from tweetlim import make_image
import sys

if __name__ == '__main__':
    author = sys.argv[1]
    lim_id = sys.argv[2]
    verse_id = sys.argv[3]
    defined_words = sys.argv[4]
    lim = sys.argv[5]
    print make_image(
        author,
        lim_id,
        defined_words,
        lim
        )
