<?php
class LimLastVisit
{
	protected $originalId;
    protected $memberId;
    protected $sessionIndex;
    protected $sessionIndexExpires;
    protected $timeout = 300; // 5 minute timeout

	public function __construct($originalId, $memberId)
	{
        $this->originalId = $originalId;
		$this->memberId = $memberId;
        $this->sessionIndex = 'LastCommentOn'.$this->originalId;
        $this->sessionIndexExpires = $this->sessionIndex.'Expires';

        if (!isset($_SESSION[$this->sessionIndex]) or
           (time() > $_SESSION[$this->sessionIndexExpires]))
        {
            $result = DbQuery("SELECT * FROM DILF_LastVisit
                WHERE AuthorId=$this->memberId AND OriginalId=$this->originalId");
            $line = DbFetchArray($result) ;
            if ($line)
                $_SESSION[$this->sessionIndex] = $line['WorkshopId'];
            else $_SESSION[$this->sessionIndex] =  0;
            $_SESSION[$this->sessionIndexExpires] = time()+$this->timeout;
            DbEndQuery($result);
        }
    }

    public function SetMinCommentIndex($commentId)
    {
        if ($commentId>$_SESSION[$this->sessionIndex])
            $_SESSION[$this->sessionIndex] = $commentId;
    }

    public function GetLastVisitCommentId()
    {
        if (isset($_SESSION[$this->sessionIndex]))
            return $_SESSION[$this->sessionIndex];
        else return 0;
    }

    public function SaveLastViewed($commentId)
    {
        DbQuery("DELETE FROM DILF_LastVisit
            WHERE AuthorId=$this->memberId AND OriginalId=$this->originalId");
        DbInsert("DILF_LastVisit",
            array("AuthorId"=>$this->memberId,
                "OriginalId"=>$this->originalId,
                "WorkshopId"=>$commentId));
    }

    public function ClearSession()
    {
        unset($_SESSION[$this->sessionIndex]);
        unset($_SESSION[$this->sessionIndexExpires]);
    }
}
?>
