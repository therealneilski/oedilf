<?php
// Manage the words that go with a limerick

if (!defined('IN_LIM')) { die("Hacking attempt"); exit; }

class LimWordList {
	protected $WordList = array();
	//protected $PhraseWordList = array();
	
	public function __construct($verseId)
	{
		if ($verseId)
		{
			//$result = DbQuery("SELECT WI.WordText FROM DILF_LimWords LW, DILF_WordIndex WI
			//	WHERE LW.VerseId=$verseId AND WI.WordIndexId=LW.WordIndexId ORDER BY Sequence");
				
			$result = DbQuery("SELECT W.WordText, W.IsPhraseWord FROM DILF_Words W
				WHERE W.VerseId=$verseId AND W.IsPhraseWord=0 ORDER BY Sequence");
			while ($line = DbFetchArray($result))
			{
				/* if ($line['IsPhraseWord']>0)
					$this->PhraseWordList[] = $line['WordText'];
				else */ 
				$this->WordList[] = $line['WordText'];
			}
			DbEndQuery($result);	
		}
	}
	
	public function AddWordList($words)
	{
		$this->WordList = array_merge($this->WordList, $words);
	}
	
	public function WordList()
	{
		return $this->WordList;
	}

	public function FormatWordList($html, $separator, $wordFormatter=null)
	{
		$firstWord = true;
		foreach ($this->WordList as $word)
		{
			if (! $firstWord) $html->Text($separator);
			if ($wordFormatter) $wordFormatter($html, $word);
			else $html->Text($word);
			$firstWord = false;
		}
	}
}

// helpers

function WordLinkFormatter($html, $word)
{
	$html->LinkSelf("Word=".rawurlencode($word), $word, "Find limericks on ".$word);
}

function WordDefLinkFormatter($html, $word)
{
	WordLinkFormatter($html, $word);
	$html->Text("&nbsp;".OneLookLink($word, true, false));
}

function WordBoldFormatter($html, $word)
{
	$html->Text("<b>".$word."</b>");
}
	

?>